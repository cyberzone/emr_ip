﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="Invoice.aspx.cs" Inherits="EMR_IP.Billing.Invoice" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
     <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <link href="../Content/themes/base/jquery-ui.css" />

    <script src="../Validation.js" type="text/javascript"></script>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }


        #divDiagExt
        {
            width: 400px !important;
        }

            #divDiagExt div
            {
                width: 400px !important;
            }

        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }


        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }

         #divOrdClin
        {
            width: 400px !important;
        }

            #divOrdClin div
            {
                width: 400px !important;
            }

    </style>


    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function PatientPopup1(CtrlName, strValue) {
            var win = window.open("../Registration/Firstname_Zoom.aspx?PageName=Invoice&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "top=200,left=100,height=650,width=1100,toolbar=no,scrollbars==yes,menubar=no");
            win.focus();

            return true;

        }

        function PTPopup(PtId) {

            if (PtId != "") {
                var win = window.open("../Registration/Patient_Registration.aspx?PatientId=" + PtId, "newwin1", "top=200,left=100,height=700,width=950,toolbar=no,scrollbars=" + scroll + ",menubar=no");
                win.focus();
            }
        }



        function ServicePopup(CtrlName, strValue) {
            var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
            if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                var win = window.open("../Masters/ServiceMasterLookup.aspx?PageName=HomeCareReg&CtrlName=" + CtrlName + "&Value=" + strValue, "newwin", "height=570,width=950,toolbar=no,scrollbars==yes,menubar=no");
                win.focus();
            }

            return true;

        }
        function CheckInvType(msg1, msg2) {
            var isDelCompany = window.confirm('This patient is registered as ' + msg1 + ' Do you want Invoice this Patient as ' + msg2);

            return true

        }

        function CompIdSelected() {
            if (document.getElementById('<%=txtSubCompanyID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtSubCompanyID.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtSubCompanyID.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }


            }

            return true;
        }

        function CompNameSelected() {
            if (document.getElementById('<%=txtCompanyName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtCompanyName.ClientID%>').value;
                var Data1 = Data.split('~');

                var Code = Data1[0];
                var Name = Data1[1];

                if (Data1.length > 1) {
                    document.getElementById('<%=txtSubCompanyID.ClientID%>').value = Code.trim();
                    document.getElementById('<%=txtCompanyName.ClientID%>').value = Name.trim();
                }
            }

            return true;
        }

        function DRIdSelected() {
            if (document.getElementById('<%=txtDoctorID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorID.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DRNameSelected() {
            if (document.getElementById('<%=txtDoctorName.ClientID%>').value != "") {
                 var Data = document.getElementById('<%=txtDoctorName.ClientID%>').value;
                 var Data1 = Data.split('~');
                 if (Data1.length > 1) {
                     document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }



        function OrdClinIdSelected() {
            if (document.getElementById('<%=txtOrdClinCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtOrdClinCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtOrdClinCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtOrdClinName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function OrdClinNameSelected() {
            if (document.getElementById('<%=txtOrdClinName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtOrdClinName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtOrdClinCode.ClientID%>').value = Data1[0];
                     document.getElementById('<%=txtOrdClinName.ClientID%>').value = Data1[1];
                 }
             }

             return true;
         }


         function ServIdSelected() {
             if (document.getElementById('<%=txtServCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function ServNameSelected() {
            if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }


        function DiagIdSelected() {
            if (document.getElementById('<%=txtDiagCode.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDiagCode.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDiagCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDiagName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }


        function DiagNameSelected() {
            if (document.getElementById('<%=txtDiagName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDiagName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtDiagCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDiagName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function RefDRNameSelected() {
            if (document.getElementById('<%=txtRefDrName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtRefDrName.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtRefDrId.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtRefDrName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }
        function ConfirmYes() {

            document.getElementById('<%=hidMsgConfirm.ClientID%>').value = "true";

        }

        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Patient Policy is expired. So you cannot claim to this invoice. Do you want to invoice?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }
            document.forms[0].appendChild(confirm_value);
        }


        function ServiceAddVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strtxtServCode = document.getElementById('<%=txtServCode.ClientID%>').value
            if (/\S+/.test(strtxtServCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Service Code";
                document.getElementById('<%=txtServCode.ClientID%>').focus;
                return false;
            }


            var strtxtServName = document.getElementById('<%=txtServName.ClientID%>').value
            if (/\S+/.test(strtxtServName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Service Name";
                document.getElementById('<%=txtServName.ClientID%>').focus;
                return false;
            }



        }

        function DiagAddVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strtxtServCode = document.getElementById('<%=txtDiagCode.ClientID%>').value
            if (/\S+/.test(strtxtServCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Diagnosis  Code";
                document.getElementById('<%=txtDiagCode.ClientID%>').focus;
                return false;
            }

            var strtxtServName = document.getElementById('<%=txtDiagName.ClientID%>').value
            if (/\S+/.test(strtxtServName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Service Name";
                document.getElementById('<%=txtDiagName.ClientID%>').focus;
                return false;
            }

        }

        function DeleteInvoiceVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strCompCode = document.getElementById('<%=txtInvoiceNo.ClientID%>').value
            if (/\S+/.test(strCompCode) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Invoice No";
                document.getElementById('<%=txtInvoiceNo.ClientID%>').focus;
                return false;
            }

            var isDelCompany = window.confirm('Do you want to delete Invoice Information?');

            if (isDelCompany == true) {
                return true
            }
            else {
                return false;
            }
            return true
        }



        function InvSaveVal() {

            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strtxtInvoiceNo = document.getElementById('<%=txtInvoiceNo.ClientID%>').value
            if (/\S+/.test(strtxtInvoiceNo) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Invoice No";
                document.getElementById('<%=txtInvoiceNo.ClientID%>').focus;
                return false;
            }

            var strtxtInvDate = document.getElementById('<%=txtInvDate.ClientID%>').value
            if (/\S+/.test(strtxtInvDate) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Date";
                document.getElementById('<%=txtInvDate.ClientID%>').focus;
                return false;
            }


            var strtxtInvTime = document.getElementById('<%=txtInvTime.ClientID%>').value
            if (/\S+/.test(strtxtInvTime) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Time";
                document.getElementById('<%=txtInvTime.ClientID%>').focus;
                return false;
            }

            var strtxtFileNo = document.getElementById('<%=txtFileNo.ClientID%>').value
            if (/\S+/.test(strtxtFileNo) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter File No";
                document.getElementById('<%=txtFileNo.ClientID%>').focus;
                return false;
            }


            var strtxtName = document.getElementById('<%=txtName.ClientID%>').value
            if (/\S+/.test(strtxtName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Name";
                document.getElementById('<%=txtName.ClientID%>').focus;
                return false;
            }






            if (document.getElementById('<%=drpInvType.ClientID%>').value == "Credit") {


                var strtxtSubCompanyID = document.getElementById('<%=txtSubCompanyID.ClientID%>').value
                if (/\S+/.test(strtxtSubCompanyID) == false) {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Company ID";
                    document.getElementById('<%=txtSubCompanyID.ClientID%>').focus;
                    return false;
                }


                var strtxtCompanyName = document.getElementById('<%=txtCompanyName.ClientID%>').value
                if (/\S+/.test(strtxtCompanyName) == false) {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Company Name";
                    document.getElementById('<%=txtSubCompanyID.ClientID%>').focus;
                    return false;
                }

                var strtxtPolicyNo = document.getElementById('<%=txtPolicyNo.ClientID%>').value
                if (/\S+/.test(strtxtPolicyNo) == false) {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Policy No";
                    document.getElementById('<%=txtPolicyNo.ClientID%>').focus;
                    return false;
                }


                var strtxtPolicyExp = document.getElementById('<%=txtPolicyExp.ClientID%>').value
                if (/\S+/.test(strtxtPolicyExp) == false) {
                    document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Policy Expiry";
                    document.getElementById('<%=txtPolicyExp.ClientID%>').focus;
                    return false;
                }




            }


            var strtxtDoctorID = document.getElementById('<%=txtDoctorID.ClientID%>').value
            if (/\S+/.test(strtxtDoctorID) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Doctor Code";
                document.getElementById('<%=txtDoctorID.ClientID%>').focus;
                return false;
            }

            var strtxtDoctorName = document.getElementById('<%=txtDoctorName.ClientID%>').value
            if (/\S+/.test(strtxtDoctorName) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Doctor Name";
                document.getElementById('<%=txtDoctorName.ClientID%>').focus;
                return false;
            }

            var PaidAmount = document.getElementById('<%=TxtRcvdAmt.ClientID%>').value;
            var PaidAmtCheck = document.getElementById('<%=hidPaidAmtCheck.ClientID%>').value;
            var PaidAmtChange = '<%=Convert.ToString(ViewState["HSOM_INVOICE_PAIDAMT_CHANGE"]) %>';
            var NewFlag = '<%=Convert.ToString(ViewState["NewFlag"]) %>';


            if (PaidAmtChange == "Y" && NewFlag == "False") {
                if (PaidAmount != PaidAmtCheck) {

                    var isMsgConfirm = window.confirm('Paid Amount changed, Do you want to Continue? ');
                    document.getElementById('<%=hidMsgConfirm.ClientID%>').value = isMsgConfirm;
                }

            }

        }


        function BindServiceDtls(ServCode, ServName, HaadCode, CtrlName) {

            document.getElementById("<%=txtServCode.ClientID%>").value = ServCode;
            document.getElementById("<%=txtServName.ClientID%>").value = ServName;




        }
    </script>
    <script language="javascript" type="text/javascript">

        function ShowPrintCrBill(InvoiceNo, ReportName) {
            var BranchID = '<%=Convert.ToString(Session["Branch_ID"]) %>';

            var Criteria = " 1=1 "
            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BRANCH_ID}=\'' + BranchID + '\'  AND {HMS_INVOICE_MASTER.HIM_INVOICE_ID}=\'' + InvoiceNo + '\'';


            // var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + ReportName + '&SelectionFormula=' + Criteria, 'PrintCrBill', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            // win.focus();

        }

        function ShowPrintInvoice(BranchID, InvoiceNo, ReportName) {

            var Criteria = " 1=1 "
            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BRANCH_ID}=\'' + BranchID + '\'  AND {HMS_INVOICE_MASTER.HIM_INVOICE_ID}=\'' + InvoiceNo + '\'';


            //  var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + ReportName + '&SelectionFormula=' + Criteria, 'PrintInvoice', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            //  win.focus();

        }




        function ShowMergeReport(BranchID, InvoiceNo, InvDate, FileNo, DrId, SubCompId, PolicyNo, NoMergePrint) {

            var arrFromDate = InvDate.split('/');
            var Date1;
            if (arrFromDate.length > 1) {

                Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
            }



            var Report = "HmsMergeCreditBill.rpt";
            var Criteria = " 1=1 "
            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_PT_ID}= \'' + FileNo + '\''

            Criteria += ' AND {HMS_INVOICE_MASTER.HIM_BRANCH_ID}=\'' + BranchID + '\'';

            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DR_CODE}= \'' + DrId + '\''



            if (FileNo == "OPDCR" || FileNo == "OPD") {

                Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_POLICY_NO}= \'' + PolicyNo + '\''
            }

            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_SUB_INS_CODE}=\'' + SubCompId + '\''

            Criteria += ' AND  {HMS_INVOICE_MASTER.HIM_DATE}=date(\'' + Date1 + '\')'



            if (NoMergePrint == 'Y') {

                Criteria += '  AND {HMS_INVOICE_MASTER.HIM_INVOICE_ID}=\'' + InvoiceNo + '\''
            }


            //var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, 'PrintMergeInv', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            //win.focus();

        }

    </script>
      <script type="text/javascript">
          function ShowMessage() {
              $("#myMessage").show();
              setTimeout(function () {
                  var selectedEffect = 'blind';
                  var options = {};
                  $("#myMessage").hide();
              }, 2000);
              return true;
          }


        </script>

   
    
</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />
     <input type="hidden" runat="server" id="hidMsgConfirm" />
 
    <input type="hidden" id="hidCompleteProc" runat="server" />
   
    <table>
        <tr>
            <td class="PageHeader">Invoice
                <asp:Button ID="btnNew" runat="server" Style="padding-left: 2px; padding-right: 2px;  width: 50px;" CssClass="button orange small"
                                                                     OnClick="btnNew_Click" Text="New"   />
            </td>
            <td>
                &nbsp;&nbsp;<asp:label id="lblBalanceMsg" runat="server" forecolor="red" font-bold="true" style="letter-spacing: 1px;" cssclass="label"></asp:label>
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="850px">
        <tr>
            <td>
                <asp:label id="lblStatus" runat="server" forecolor="red" font-bold="true" style="letter-spacing: 1px;" cssclass="label"></asp:label>
            </td>
        </tr>

    </table>


    <div style="padding-top: 0px; width: 830px; height: 180px; border: thin; border-color: #cccccc; border-style: groove;">
        <input type="hidden" runat="server" id="hidVisitID" />
        <input type="hidden" runat="server" id="hidCompanyID" />
        <input type="hidden" runat="server" id="hidCmpRefCode" />
        <input type="hidden" runat="server" id="hidInsType" />
        <input type="hidden" runat="server" id="hidIDNo" />
        <input type="hidden" runat="server" id="hidInsTrtnt" />

        <input type="hidden" runat="server" id="hidPTVisitType" />
        <input type="hidden" runat="server" id="hidUpdateCodeType" />
        <input type="hidden" runat="server" id="hidCompAgrmtType" />
        <input type="hidden" runat="server" id="hidCoInsType" />
        <input type="hidden" runat="server" id="hidReceipt" />
        <input type="hidden" runat="server" id="hidType" />  

        <input type="hidden" runat="server" id="hidccText" />  
        <input type="hidden" runat="server" id="hidmasterinvoicenumber" />  
 
        <input type="hidden" runat="server" id="hidPaidAmtCheck" />  
 

        <table width="100%" border="0">
            <tr>
                <td class="lblCaption1" style="width: 70px">Invoice Type
                </td>
                <td style="width: 100px">
                    <asp:dropdownlist id="drpInvType" runat="server" cssclass="label" width="95%" borderwidth="1px" bordercolor="#cccccc" >
                                            <asp:ListItem Value="Cash">Cash</asp:ListItem>
                                            <asp:ListItem Value="Credit" selected="True">Credit</asp:ListItem>
                       </asp:dropdownlist>
                </td>
                <td class="lblCaption1" style="width: 70px;">Invoice No
                </td>
                <td style="width: 100px">
                    <asp:textbox id="txtInvoiceNo" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" Enabled="True"  width="95%" height="22PX" maxlength="10" autopostback="true" ontextchanged="txtInvoiceNo_TextChanged"></asp:textbox>

                </td>

                <td class="lblCaption1" style="width: 70px">Date & Time
                </td>
                <td style="width: 150px">
                    <asp:textbox id="txtInvDate" runat="server" width="48%" height="22px" cssclass="label" borderwidth="1px" bordercolor="#cccccc" maxlength="10" onkeypress="return OnlyNumeric(event);"  ></asp:textbox>
                    <asp:textbox id="txtInvTime" runat="server" readonly="true" width="40%" height="22px" cssclass="label" borderwidth="1px" bordercolor="#cccccc" maxlength="10"></asp:textbox>
                    <asp:calendarextender id="Calendarextender3" runat="server"
                        enabled="True" targetcontrolid="txtInvDate" format="dd/MM/yyyy">
                        </asp:calendarextender>


                </td>
                <td class="lblCaption1" style="width: 70px">
                     <asp:label id="lblInsCard" runat="server"    style="letter-spacing: 1px;" cssclass="lblCaption1" text="Ins.Card#"></asp:label>

                </td>
                <td style="width: 100px">
                    <asp:textbox id="txtIdNo" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="95%"></asp:textbox>
                     <asp:dropdownlist id="drpStatus" cssclass="label" runat="server" width="150px" visible="false">
                            <asp:ListItem Value="Open">Open</asp:ListItem>
                          <asp:ListItem Value="Void">Void</asp:ListItem>
                         <asp:ListItem Value="Hold">Hold</asp:ListItem>
                     </asp:dropdownlist>

                <asp:textbox id="txtJobnumber" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="50%" visible="false"></asp:textbox>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Token No
                </td>
                <td>
                    <asp:textbox id="txtTokenNo" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="95%" ></asp:textbox>

                </td>
                <td class="lblCaption1">File No
                </td>
                <td>
                    <asp:textbox id="txtFileNo" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="95%" Enabled="false"   autopostback="true" ontextchanged="txtFileNo_TextChanged" ondblclick="return PatientPopup1('FileNo',this.value);" ></asp:textbox>

                </td>
                <td class="lblCaption1">Name
                </td>
                <td>
                    <asp:textbox id="txtName" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="95%" ></asp:textbox>

                </td>
                <td class="lblCaption1">PT Comp.
                </td>
                <td>
                    <asp:textbox id="txtPTComp" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="95%" ></asp:textbox>

                </td>
            </tr>

            <tr>
                <td class="lblCaption1">Comp. ID 

                </td>
                <td>
                    <asp:textbox id="txtSubCompanyID" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="95%" Enabled="false"   ></asp:textbox>

                </td>
                <td class="lblCaption1">Ins. Name

                </td>
                <td colspan="3">
                    <asp:textbox id="txtSubCompanyName" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="48%" Enabled="false"  ></asp:textbox>

                    <asp:textbox id="txtCompanyName" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="48%"  Enabled="false" ></asp:textbox>


                </td>
                <td class="lblCaption1" >Policy Type
                </td>
                <td>


                    <asp:textbox id="txtPlanType" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="95%" ></asp:textbox>

                </td>

            </tr>
            <tr>
                <td class="lblCaption1">Policy No 

                </td>
                <td colspan="2" class="lblCaption1">
                    <asp:textbox id="txtPolicyNo" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="80%" Enabled="false" ></asp:textbox>

                    Exp.

                </td>
                <td>
                    <asp:textbox id="txtPolicyExp" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="95%" Enabled="false" ></asp:textbox>

                </td>
                <td class="lblCaption1"  >Treat. Type
                </td>
                <td>
                    <asp:dropdownlist id="drpTreatmentType" cssclass="label" runat="server" width="95%">
                          
                        </asp:dropdownlist>
                     <asp:textbox id="txtICdCode" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="50px" visible="false" ></asp:textbox>
                     <asp:textbox id="txtICDDesc" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="150px" visible="false" ></asp:textbox>

                </td>
                <td class="lblCaption1">Claim No 

                </td>
                <td>
                    <asp:textbox id="TxtVoucherNo" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="95%" ></asp:textbox>

                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Doctor 
                </td>
                <td class="auto-style28" colspan="3">

                    <div id="divDr" style="visibility: hidden;"></div>
                    <asp:textbox id="txtDoctorID" runat="server" cssclass="label" borderwidth="1px" bordercolor="#CCCCCC" width="20%" autopostback="true" ontextchanged="txtDoctorID_TextChanged" onblur="return DRIdSelected()"></asp:textbox>
                    <asp:textbox id="txtDoctorName" runat="server" cssclass="label" borderwidth="1px" bordercolor="#CCCCCC" width="75%" maxlength="50" onblur="return DRNameSelected()"></asp:textbox>
                    <asp:autocompleteextender id="AutoCompleteExtender3" runat="Server" targetcontrolid="txtDoctorID" minimumprefixlength="1" servicemethod="GetDoctorId"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" completionlistelementid="divDr"></asp:autocompleteextender>
                    <asp:autocompleteextender id="AutoCompleteExtender4" runat="Server" targetcontrolid="txtDoctorName" minimumprefixlength="1" servicemethod="GetDoctorName"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" completionlistelementid="divDr"></asp:autocompleteextender>

                </td>

                <td class="lblCaption1">Ref Dr.

                </td>
                <td colspan="3">
                    <asp:textbox id="txtRefDrId" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="20%" visible="true" ></asp:textbox>

                    <asp:textbox id="txtRefDrName" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="75%"  onblur="return RefDRNameSelected()" autopostback="true"    ontextchanged="txtRefDrId_TextChanged"></asp:textbox>
                   <asp:autocompleteextender id="AutoCompleteExtender1" runat="Server" targetcontrolid="txtRefDrName" minimumprefixlength="1" servicemethod="GetRefDoctorName"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" ></asp:autocompleteextender>

                </td>
               
            </tr>
            <tr>
                <td class="lblCaption1">Ord. Clinician 
                </td>
                <td class="auto-style28" colspan="3">

                    <div id="divOrdClin" style="visibility: hidden;"></div>
                    <asp:textbox id="txtOrdClinCode" runat="server" cssclass="label" borderwidth="1px" bordercolor="#CCCCCC" width="20%" autopostback="true" ontextchanged="txtOrdClinCode_TextChanged" onblur="return OrdClinIdSelected()"></asp:textbox>
                    <asp:textbox id="txtOrdClinName" runat="server" cssclass="label" borderwidth="1px" bordercolor="#CCCCCC" width="75%" maxlength="50" onblur="return OrdClinNameSelected()"></asp:textbox>
                    <asp:autocompleteextender id="AutoCompleteExtender2" runat="Server" targetcontrolid="txtOrdClinCode" minimumprefixlength="1" servicemethod="GetDoctorId"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" completionlistelementid="divOrdClin"></asp:autocompleteextender>
                    <asp:autocompleteextender id="AutoCompleteExtender9" runat="Server" targetcontrolid="txtOrdClinName" minimumprefixlength="1" servicemethod="GetDoctorName"
                        completionlistcssclass="AutoExtender" completionlistitemcssclass="AutoExtenderList" completionlisthighlighteditemcssclass="AutoExtenderHighlight" completionlistelementid="divOrdClin"></asp:autocompleteextender>

                </td>
               
                 <td class="lblCaption1">Remarks

                </td>
                <td>
                    <asp:textbox id="txtRemarks" runat="server" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="95%" visible="true" ></asp:textbox>
                      <asp:dropdownlist id="drpRemarks" cssclass="label" runat="server" width="150px" visible="false">
                          
                        </asp:dropdownlist>
                        <asp:dropdownlist id="drpCommissionType" cssclass="label" runat="server" width="150px" visible="false"></asp:dropdownlist>

                </td>
            </tr>

        </table>
    </div>
    <table width="100%">
        <tr>
            <td class="lblCaption1" align="center">Invoice Information
            </td>
        </tr>
    </table>
    <div style="padding: 5px; width: 820px;  border: thin; border-color: #cccccc; border-style: groove;">

        <asp:tabcontainer id="TabContainer1" runat="server" activetabindex="0" cssclass="ajax__tab_yuitabview-theme" width="100%">
          
 <asp:TabPanel runat="server" ID="tabService" HeaderText="Services" Width="100%">
                <contenttemplate>
                      <table>
                          <tr>
                              <td  class="lblCaption">
                                  Code <span style="color:red;">* </span>
                              </td>
                                <td class="lblCaption">
                                    Description
                              </td>
                           
                  
                 
                          </tr>
                          <tr>
                              <td style="width:105px;">
                                  <input type="hidden" id="hidHaadID" runat="server" />
                                  <input type="hidden" id="hidCatID" runat="server" />
                                   <input type="hidden" id="hidCatType" runat="server" />
                                  <asp:TextBox ID="txtServCode" runat="server" Width="100px" height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true"  OnTextChanged="txtServCode_TextChanged"    onblur="return ServIdSelected()" ondblclick="return ServicePopup('txtServCode',this.value);"></asp:TextBox>
                              </td>
                               <td style="width:405px;">
                                   <asp:TextBox ID="txtServName" runat="server" Width="400px" height="22px"  CssClass="label" BorderWidth="1px" BorderColor="#cccccc"      onblur="return ServNameSelected()" ondblclick="return ServicePopup('txtServName',this.value);"></asp:TextBox>
                     
                                    <div ID="divwidth" style="visibility:hidden;"></div>
                                    <asp:autocompleteextender id="AutoCompleteExtender5" runat="Server" targetcontrolid="txtServCode" minimumprefixlength="1" servicemethod="GetServiceID"
                                    CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divwidth"    ></asp:autocompleteextender>
                                    <asp:autocompleteextender id="AutoCompleteExtender6" runat="Server" targetcontrolid="txtServName" minimumprefixlength="1" servicemethod="GetServiceName"
                                    CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divwidth"    ></asp:autocompleteextender>
                      

                                 </td>
                             
                              <td>
                                   <asp:Button ID="btnServiceAdd" runat="server" Style="padding-left: 2px; padding-right: 2px;  width: 50px;" CssClass="button orange small"
                                                                     OnClick="btnServiceAdd_Click" Text="Add"  OnClientClick="return ServiceAddVal();"/>
                                  <asp:Button ID="btnImportSO" runat="server" OnClick="btnImportSO_Click" Text="Import Sales Order Trans." CssClass="button orange small"   />
                                 

                              </td>
                          </tr>
                        
                      </table>
                  
                       <div style="padding-top: 0px; width: 800px; height: 250px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                        <table width="100%">
                                            <tr>
                                                <td>
                                                     <asp:GridView ID="gvInvoiceTrans" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                            EnableModelValidation="True" Width="99%" gridline="none" OnRowDataBound="gvInvTran_RowDataBound">
                                                            <HeaderStyle CssClass="GridHeader" />
                                                            <RowStyle CssClass="GridRow" />
                                                
                                                            <Columns>

                                                                 <asp:TemplateField HeaderText="Trans. Date">
                                                                    <ItemTemplate>
                                                                          <asp:UpdatePanel runat="server" ID="UpdatePanelTransDate" 
                                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                                        <ContentTemplate>
                                                                             

                                                                            <asp:TextBox ID="txtTransDate" width="70px" CssClass="label"  style="border:none;" runat="server" Text='<%# Bind("HIT_TRANS_DATEDesc") %>'  Height="30px" AutoPostBack="true"  OnTextChanged="txtTransDate_TextChanged"    ></asp:TextBox>
                                                                         <asp:calendarextender id="CalendarextenderTransDate" runat="server"   enabled="True" targetcontrolid="txtTransDate" format="dd/MM/yyyy">
                                                                                </asp:calendarextender>  

                                                                         </ContentTemplate>
                                                                              <Triggers>
                                                                                <asp:PostBackTrigger ControlID="txtTransDate" />
                                                                            </Triggers>
                                                                     </asp:UpdatePanel>

                                                                      </ItemTemplate>
                                                                 </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px" >
                                                                    <ItemTemplate>
                                                                            <asp:Label ID="lblgvSOTransID" width="100px" CssClass="label" runat="server" Text='<%# Bind("SOTransID") %>'  visible="false" ></asp:Label>
                                                                            <asp:Label ID="lblgvServCost" width="100px" CssClass="label" runat="server" Text='<%# Bind("HIT_SERVCOST") %>'  visible="false" ></asp:Label>
                                                                            <asp:Label ID="lblgvAdjDedAmt" width="100px" CssClass="label" runat="server" Text='<%# Bind("HIT_ADJ_DEDUCT_AMOUNT") %>'  visible="false" ></asp:Label>
                                                                        <asp:UpdatePanel runat="server" ID="UpdatePanel6" 
                                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                                        <ContentTemplate>
                                                                             
                                                                             
                                                                               <asp:TextBox ID="txtgvServCode" width="100px" CssClass="label" style="border:none;" runat="server" Text='<%# Bind("HIT_SERV_CODE") %>'  Height="30px" Enabled="false"   autopostback="false" OnTextChanged="txtgvServCode_TextChanged"></asp:TextBox>

                                                                            
                                                                        </ContentTemplate>
                                                                              <Triggers>
                                                                                <asp:PostBackTrigger ControlID="txtgvServCode" />
                                                                            </Triggers>
                                                                     </asp:UpdatePanel>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Description">
                                                                    <ItemTemplate>
                                                                            <asp:TextBox ID="txtServDesc" width="300px" CssClass="label"  style="border:none;" runat="server" Text='<%# Bind("HIT_Description") %>'  Height="30px"  Enabled="false"   ></asp:TextBox>

                                                                           
                                      
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                               
                                                                <asp:TemplateField HeaderText="Fee" Visible="false">
                                                                    <ItemTemplate>
                                                                             <asp:UpdatePanel runat="server" ID="UpdatePanel12" 
                                                                                UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                                                <ContentTemplate>
                                                                                     <asp:TextBox ID="txtFee" runat="server" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Height="30px"    Text='<%# Bind("HIT_FEE") %>'  autopostback="true" OnTextChanged="gvInvoiceTrans_TextChanged"  onkeypress="return OnlyNumeric(event);" ></asp:TextBox>
                                                                                 </ContentTemplate>
                                                                              <Triggers>
                                                                                <asp:PostBackTrigger ControlID="txtFee" />
                                                                            </Triggers>
                                                                     </asp:UpdatePanel>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                   <asp:TemplateField HeaderText="D.%" HeaderStyle-Width="50px"  ItemStyle-VerticalAlign="Top"  Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDiscType" CssClass="GridRow"  runat="server" Text='<%# Bind("HIT_DISC_TYPE") %>' visible="false" ></asp:Label>
                                                                         


                                                                           <asp:UpdatePanel runat="server" ID="UpdatePanel4" 
                                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                                        <ContentTemplate>
                                                                          <asp:DropDownList ID="drpgvDiscType" CssClass="LabelStyle" style="border:none;" runat="server" Width="50px" height="30px" autopostback="true" onselectedindexchanged="drpgvDiscType_SelectedIndexChanged">
                                                                                <asp:ListItem Value="$">$</asp:ListItem>
                                                                                <asp:ListItem Value="%">%</asp:ListItem>
                                                                          </asp:DropDownList>

                                                                              </ContentTemplate>
                                                                              <Triggers>
                                                                                <asp:PostBackTrigger ControlID="drpgvDiscType" />
                                                                            </Triggers>
                                                                     </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="D.Amt" HeaderStyle-Width="70px"   Visible="false">
                                                                    <ItemTemplate>
                                                                        
                                                                           <asp:UpdatePanel runat="server" ID="UpdatePanel5" 
                                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                                        <ContentTemplate>
                                                                             
                                                                             <asp:TextBox ID="txtDiscAmt" runat="server" CssClass="label" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Height="30px"  Text='<%# Bind("HIT_DISC_AMT") %>' autopostback="true" OnTextChanged="gvInvoiceTrans_TextChanged"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                                            
                                                                        </ContentTemplate>
                                                                              <Triggers>
                                                                                <asp:PostBackTrigger ControlID="txtDiscAmt" />
                                                                            </Triggers>
                                                                     </asp:UpdatePanel>


                                                                    </ItemTemplate>
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Qty" HeaderStyle-Width="30px">
                                                                    <ItemTemplate>
                                                                     <asp:UpdatePanel runat="server" ID="UpId" 
                                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                                        <ContentTemplate>
                                                                            <asp:TextBox ID="txtgvQty" runat="server" CssClass="label" style="text-align:right;padding-right:5px;border:none;" Width="30px"  Height="30px"  Text='<%# Bind("HIT_QTY") %>'  autopostback="true" OnTextChanged="gvInvoiceTrans_TextChanged"  onkeypress="return OnlyNumeric(event);" ></asp:TextBox>
                                                                            
                                                                        </ContentTemplate>
                                                                              <Triggers>
                                                                                <asp:PostBackTrigger ControlID="txtgvQty" />
                                                                            </Triggers>
                                                                     </asp:UpdatePanel>
                                                                        
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Amount" HeaderStyle-Width="30px"  Visible="false">
                                                                    <ItemTemplate>
                                                                      
                                                                          <asp:TextBox ID="txtHitAmount" CssClass="label" runat="server" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Height="30px" ReadOnly="true"   Text='<%# Bind("HIT_AMOUNT") %>' ></asp:TextBox>
                                                                             
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="Ded." HeaderStyle-Width="70px"  Visible="false" >
                                                                    <ItemTemplate>
                                                                          <asp:UpdatePanel runat="server" ID="UpdatePanel2" 
                                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                                        <ContentTemplate>
                                                                             
                                                                             <asp:TextBox ID="txtDeduct" runat="server" CssClass="label" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Height="30px"  Text='<%# Bind("HIT_DEDUCT") %>' autopostback="true" OnTextChanged="gvInvoiceTrans_TextChanged"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                                            
                                                                        </ContentTemplate>
                                                                              <Triggers>
                                                                                <asp:PostBackTrigger ControlID="txtDeduct" />
                                                                            </Triggers>
                                                                     </asp:UpdatePanel>


                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Co-%" HeaderStyle-Width="50px"  ItemStyle-VerticalAlign="Top"  Visible="false">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCoInsType" CssClass="GridRow"  runat="server" Text='<%# Bind("HIT_CO_INS_TYPE") %>' visible="false" ></asp:Label>
                                                                           <asp:UpdatePanel runat="server" ID="UpdatePanel3" 
                                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                                        <ContentTemplate>
                                                                          <asp:DropDownList ID="drpCoInsType" CssClass="LabelStyle" runat="server" style="border:none;" Width="50px" height="30px" autopostback="true" onselectedindexchanged="drpCoInsType_SelectedIndexChanged">
                                                                                <asp:ListItem Value="$">$</asp:ListItem>
                                                                                <asp:ListItem Value="%">%</asp:ListItem>
                                                                          </asp:DropDownList>

                                                                              </ContentTemplate>
                                                                              <Triggers>
                                                                                <asp:PostBackTrigger ControlID="drpCoInsType" />
                                                                            </Triggers>
                                                                     </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Co-Ins" HeaderStyle-Width="70px" Visible="false" >
                                                                    <ItemTemplate>
                                                                         <asp:UpdatePanel runat="server" ID="UpdatePanel1" 
                                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                                        <ContentTemplate>
                                                                             <asp:TextBox ID="txtCoInsAmt" runat="server" CssClass="label" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Height="30px"  Text='<%# Bind("HIT_CO_INS_AMT")%>' autopostback="true" OnTextChanged="gvInvoiceTrans_TextChanged"  onkeypress="return OnlyNumeric(event);" ></asp:TextBox>

                                                                        </ContentTemplate>
                                                                              <Triggers>
                                                                                <asp:PostBackTrigger ControlID="txtCoInsAmt" />
                                                                            </Triggers>
                                                                     </asp:UpdatePanel>
                                                                        
                                                                    
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Co-Total" HeaderStyle-Width="70px"  Visible="false">
                                                                    <ItemTemplate>
                                                                       <asp:TextBox ID="txtgvCoInsTotal" runat="server" CssClass="label" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Height="30px" Enabled="false"  Text='<%# Bind("HIT_CO_TOTAL") %>'  ></asp:TextBox>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Comp. Type" HeaderStyle-Width="70px" >
                                                                    <ItemTemplate>
                                                                       <asp:TextBox ID="txtgvCompType" runat="server" CssClass="label" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Height="30px" Enabled="false"   Text='<%# Bind("HIT_COMP_TYPE") %>'  ></asp:TextBox>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Comp. Amount" HeaderStyle-Width="70px" >
                                                                    <ItemTemplate>
                                                                       <asp:TextBox ID="txtgvCompAmount" runat="server" CssClass="label" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Height="30px" Enabled="false"  Text='<%# Bind("HIT_COMP_AMT") %>'   ></asp:TextBox>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="Comp Total" HeaderStyle-Width="70px" >
                                                                    <ItemTemplate>
                                                                       <asp:TextBox ID="txtgvCompTotal" runat="server" CssClass="label" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Height="30px" Enabled="false"   Text='<%# Bind("HIT_COMP_TOTAL") %>'  ></asp:TextBox>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Dr.Code" HeaderStyle-Width="100">
                                                                    <ItemTemplate>
                                                                     <asp:UpdatePanel runat="server" ID="UpdatePanel7" 
                                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                                        <ContentTemplate>
                                                                            <asp:TextBox ID="txtgvDrCode" runat="server" CssClass="label" style="border:none;" Width="100px"  Height="30px"  Text='<%# Bind("HIT_DR_CODE") %>' autopostback="true" OnTextChanged="txtgvDrCode_TextChanged" ></asp:TextBox>
                                                                                
                                                                          </ContentTemplate>
                                                                              <Triggers>
                                                                                <asp:PostBackTrigger ControlID="txtgvDrCode" />
                                                                            </Triggers>
                                                                     </asp:UpdatePanel>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                   
                                                                 <asp:TemplateField HeaderText="Dr.Name" HeaderStyle-Width="150">
                                                                    <ItemTemplate>

                                                                       <asp:TextBox ID="txtgvDrName" runat="server" CssClass="label" style="border:none;" Width="150px"  Height="30px"  Text='<%# Bind("HIT_DR_NAME") %>' ></asp:TextBox>
                                                                       </ItemTemplate>
                                                                </asp:TemplateField>


                                                                 <asp:TemplateField HeaderText="Serv. Category"  >
                                                                    <ItemTemplate>
                                                                       <asp:TextBox ID="txtgvCatID" runat="server" CssClass="label" style="border:none;" Width="100px"  Height="30px"  Text='<%# Bind("HIT_CAT_ID") %>' ></asp:TextBox>
                                                                         <asp:Label ID="lblgvCatID" width="100px" CssClass="label" runat="server" Text='<%# Bind("HIT_CAT_ID") %>'    visible="false" ></asp:Label>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                  <asp:TemplateField HeaderText="Serv. Treatment Type" ShowHeader="false">
                                                                    <ItemTemplate>
                                                                      <asp:Label ID="lblServTreatType" width="100px" CssClass="label" runat="server"   visible="false" ></asp:Label>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                              <asp:TemplateField HeaderText="Covered" ShowHeader="false">
                                                                    <ItemTemplate>
                                                                       <asp:Label ID="lblCoverd" width="100px" CssClass="label" runat="server" Text='<%# Bind("HIT_COVERED") %>'  visible="false" ></asp:Label>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Serv. Type" ShowHeader="false">
                                                                    <ItemTemplate>
                                                                      <asp:Label ID="lblServType" width="100px" CssClass="label" runat="server" Text='<%# Bind("HIT_SERV_TYPE") %>'  visible="false" ></asp:Label>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="RefNo." ShowHeader="false">
                                                                    <ItemTemplate>
                                                                           <asp:Label ID="lblgvRefNo" width="100px" CssClass="label" runat="server" Text='<%# Bind("HIT_REFNO") %>'  visible="false" ></asp:Label>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>


                                                             

                                                                <asp:TemplateField HeaderText="TeethNo." ShowHeader="false">
                                                                    <ItemTemplate>
                                                                            <asp:Label ID="lblgvTeethNo" width="100px" CssClass="label" runat="server" Text='<%# Bind("HIT_TEETHNO") %>'  visible="false" ></asp:Label>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>

                                                                 <asp:TemplateField HeaderText="Haad Code">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtHaadCode" runat="server"  CssClass="label" style="border:none;" Width="70px"  Height="30px"  Text='<%# Bind("HIT_HAAD_CODE") %>' ></asp:TextBox>

                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Haad Desc">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtHaadDesc" runat="server" CssClass="label" style="border:none;" Width="150px"  Height="30px"  ></asp:TextBox>

                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Haad Type">
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtTypeValue" runat="server" CssClass="label" style="text-align:center;border:none;" Width="70px"  Height="30px"  ReadOnly="true"  Text='<%# Bind("HIT_TYPE_VALUE") %>' ></asp:TextBox>

                                                                    </ItemTemplate>

                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Cost Price" Visible="False"   >
                                                                    <ItemTemplate>
                                                                        <asp:TextBox ID="txtgvCostPrice" runat="server" CssClass="label" style="text-align:right;padding-right:5px;border:none;" Width="70px"  Height="30px" Enabled="false"  Text='<%# Bind("HIT_COSTPRICE") %>'   ></asp:TextBox>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Authorization ID" HeaderStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                       <asp:TextBox ID="txtgvAuthID" runat="server" CssClass="label" style="border:none;" Width="100px"  Height="30px"  Text='<%# Bind("HIT_AUTHORIZATIONID") %>' ></asp:TextBox>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Start Date" HeaderStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                       <asp:TextBox ID="txtgcStartDt" runat="server" CssClass="label" style="border:none;" Width="100px"  Height="30px"  Text='<%# Bind("HIT_STARTDATEDesc") %>' ></asp:TextBox>
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Observ.Type" HeaderStyle-Width="70px">
                                                                    <ItemTemplate>
                                                                       <asp:TextBox ID="txtObsType" runat="server" CssClass="label" style="border:none;" Width="100px"  Height="30px"   Text='<%# Bind("HIO_TYPE") %>'   ReadOnly="true"  ></asp:TextBox>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Observ.Code" HeaderStyle-Width="70px">
                                                                    <ItemTemplate>
                                                                         <asp:UpdatePanel runat="server" ID="UpdatePanel8" 
                                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                                        <ContentTemplate>
                                                                       <asp:TextBox ID="txtObsCode" runat="server" CssClass="label" style="border:none;" Width="70px"  Height="30px"  Text='<%# Bind("HIO_CODE") %>' autopostback="true" OnTextChanged="txtObsCode_TextChanged" ></asp:TextBox>
                                                                          </ContentTemplate>
                                                                              <Triggers>
                                                                                <asp:PostBackTrigger ControlID="txtObsCode" />
                                                                            </Triggers>
                                                                     </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Observ.Description" HeaderStyle-Width="70px">
                                                                    <ItemTemplate>
                                                                           <asp:UpdatePanel runat="server" ID="UpdatePanel9" 
                                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                                        <ContentTemplate>
                                                                       <asp:TextBox ID="txtObsDesc" runat="server" CssClass="label" style="border:none;" Width="120px"  Height="30px"  Text='<%# Bind("HIO_DESCRIPTION") %>' autopostback="true" OnTextChanged="txtObsCode_TextChanged" ></asp:TextBox>
                                                                          </ContentTemplate>
                                                                              <Triggers>
                                                                                <asp:PostBackTrigger ControlID="txtObsDesc" />
                                                                            </Triggers>
                                                                     </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Observ.Value" HeaderStyle-Width="70px">
                                                                    <ItemTemplate>
                                                                     <asp:UpdatePanel runat="server" ID="UpdatePanel10" 
                                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                                        <ContentTemplate>
                                                                       <asp:TextBox ID="txtObsValue" runat="server" CssClass="label" style="border:none;" Width="70px"  Height="30px"  Text='<%# Bind("HIO_VALUE") %>'  autopostback="true" OnTextChanged="txtObsCode_TextChanged"></asp:TextBox>
                                                                          </ContentTemplate>
                                                                              <Triggers>
                                                                                <asp:PostBackTrigger ControlID="txtObsValue" />
                                                                            </Triggers>
                                                                     </asp:UpdatePanel>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Observ.ValueType" HeaderStyle-Width="100px">
                                                                    <ItemTemplate>
                                                                         <asp:UpdatePanel runat="server" ID="UpdatePanel11" 
                                                                        UpdateMode="Conditional" ChildrenAsTriggers="true">
                                                                        <ContentTemplate>
                                                                       <asp:TextBox ID="txtObsValueType" runat="server" CssClass="label" style="border:none;" Width="100px"  Height="30px"  Text='<%# Bind("HIO_VALUETYPE") %>' autopostback="true" OnTextChanged="txtObsCode_TextChanged" ></asp:TextBox>
                                                                          </ContentTemplate>
                                                                              <Triggers>
                                                                                <asp:PostBackTrigger ControlID="txtObsValueType" />
                                                                            </Triggers>
                                                                     </asp:UpdatePanel>


                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Delete">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="DeleteDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Width="16px" Height="16px"
                                                                            OnClick="DeletegvInv_Click" />&nbsp;&nbsp;
                                                
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="50px" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                                </td>
                                            </tr>
                                           </table>
                        </div>

                      <div style="display:block;padding-top: 0px; width: 800px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                          <table width="100%" border="0" >
                            <tr>
                                <td class="lblCaption1" style="width: 90px;">Bill Amt(1)
                                </td>
                                <td>
                                    <asp:textbox id="txtBillAmt1" runat="server" height="22px" style="text-align:right;"  Enabled="false" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="80px" Text="0.00" maxlength="10" autopostback="true" OnTextChanged="txtInvoiceNo_TextChanged"></asp:textbox>

                                </td>
                                <td class="lblCaption1" style="width: 80px;">Bill Amt(2)
                                </td>
                                <td>
                                    <asp:textbox id="txtBillAmt2" runat="server" height="22px" style="text-align:right;"  Enabled="false"  cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="70px"  Text="0.00" maxlength="10" autopostback="true" OnTextChanged="txtInvoiceNo_TextChanged"></asp:textbox>

                                </td>
                                 <td class="lblCaption1" style="width: 80px;">Bill Amt(3)
                                </td>
                                <td>
                                    <asp:textbox id="txtBillAmt3" runat="server" height="22px" style="text-align:right;" Enabled="false"   cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="70px"  Text="0.00" maxlength="10" autopostback="true" OnTextChanged="txtInvoiceNo_TextChanged"></asp:textbox>

                                </td>
                                 <td class="lblCaption1" style="width: 60px;">Claim Amt
                                </td>
                                <td>
                                    <asp:textbox id="txtClaimAmt" runat="server" height="22px" style="text-align:right;" Enabled="false"   cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="70px"  Text="0.00" maxlength="10" autopostback="true" OnTextChanged="txtInvoiceNo_TextChanged"></asp:textbox>

                                </td>
                                 <td class="lblCaption1" style="width: 70px;">Bill Amt(4)
                                </td>
                                <td>
                                    <asp:textbox id="txtBillAmt4" runat="server" height="22px" style="text-align:right;"  Enabled="false"  cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="70px"  Text="0.00" maxlength="10" autopostback="true" OnTextChanged="txtInvoiceNo_TextChanged"></asp:textbox>

                                </td>
                                </tr>
                            <tr>
                                 <td class="lblCaption1" style="width: 90px;">Hosp.Disc
                                </td>
                                <td>
                                      <asp:DropDownList ID="drpHospDiscType" CssClass="LabelStyle" Font-Size="10px" runat="server" Width="35px" height="27px">
                                            <asp:ListItem Value="$">$</asp:ListItem>
                                            <asp:ListItem Value="%">%</asp:ListItem>
                                       </asp:DropDownList>
                                   <asp:textbox id="txtHospDisc" runat="server" height="22px" style="text-align:right;" cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="40px" maxlength="10"  Text="0.00" autopostback="true" OnTextChanged="txtHospDisc_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:textbox>
                                  <asp:textbox id="txtHospDiscAmt" runat="server" height="22px" style="text-align:right;" cssclass="label" borderwidth="1px" bordercolor="#cccccc"  maxlength="10"  Text="0.00"  visible="false" ></asp:textbox>

                                </td>
                                 <td class="lblCaption1" style="width: 80px;">Deductible
                                </td>
                                <td>
                                    <asp:textbox id="txtDeductible" runat="server" height="22px" Enabled="false"  style="text-align:right;"   cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="70px"  Text="0.00" maxlength="10"  ></asp:textbox>

                                </td>
                                 <td class="lblCaption1" style="width: 80px;">Co-Ins.Total
                                </td>
                                <td>
                                    <asp:textbox id="txtCoInsTotal" runat="server" height="22px" Enabled="false"  style="text-align:right;"  cssclass="label" borderwidth="1px" bordercolor="#cccccc"  Text="0.00" width="70px" maxlength="10" ></asp:textbox>

                                </td>
                                 <td class="lblCaption1" style="width: 60px;">Spl.Disc
                                </td>
                                <td>
                                    <asp:textbox id="txtSplDisc" runat="server" height="22px" Enabled="false" style="text-align:right;"  cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="70px" maxlength="10"  Text="0.00" autopostback="true" OnTextChanged="txtSplDisc_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:textbox>

                                </td>
                                 <td class="lblCaption1" style="width: 70px;">PT Credit
                                </td>
                                <td>
                                    <asp:textbox id="txtPTCredit" runat="server" height="22px" style="text-align:right;"  cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="70px" maxlength="10"  Text="0.00" autopostback="true" OnTextChanged="txtSplDisc_TextChanged" onkeypress="return OnlyNumeric(event);"></asp:textbox>

                                </td>
                            </tr>
                            <tr>
                                <td colspan="1">
                                    <asp:CheckBox ID="chkPrintCrBill" runat="server" CssClass="lblCaption1" Text="Print Cr. Bill" Checked="false"   /> 
                    
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkOnlySave" runat="server" CssClass="lblCaption1" Text="Only Save" Checked="true"   />

                                </td>
                                <td class="lblCaption1" style="width: 80px;">Print Type
                                </td>
                                <td>
                                    <asp:DropDownList ID="drpPrintType" CssClass="LabelStyle" runat="server" Width="70px"  >
                                         <asp:ListItem Value="" selected="True"></asp:ListItem>
                                            <asp:ListItem Value="Gross">Gross</asp:ListItem>
                                            <asp:ListItem Value="Net">Net</asp:ListItem>
                                            <asp:ListItem Value="Print1">Print1</asp:ListItem>
                                            <asp:ListItem Value="Print2">Print2</asp:ListItem>
                                        <asp:ListItem Value="Print3">Print3</asp:ListItem>
                                       </asp:DropDownList>


                                </td>
                
                                <td >
                                     <asp:Button ID="btnEditPTPopup" runat="server" style="padding-left:5px;padding-right:5px;width:70px;"  CssClass="button orange small"   Height="25px" Text="Edit Patient" OnClick="btnEditPTPopup_Click"   />
                                </td>
                                <td  colspan="2">
                                     &nbsp;<asp:Button ID="btnPrntDeductible" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 70px;" CssClass="button orange small" visible="true" OnClick="btnPrntDeductible_Click" Text="Deductible"  />

                                </td>
                                  <td >
                                     <asp:Button ID="btnPrint" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 70px;" CssClass="button orange small" visible="true" OnClick="btnPrint_Click" Text="Print"  OnClientClick="return InvSaveVal();" />

                                </td>
                                 <td class="lblCaption1" style="width: 70px;">Paid Amt
                                </td>
                                <td>
                                   <asp:textbox id="TxtRcvdAmt" runat="server" height="22px" style="text-align:right;"  cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="70px" maxlength="10" Text="0.00"  autopostback="false" OnTextChanged="TxtRcvdAmt_TextChanged"  onkeypress="return OnlyNumeric(event);"></asp:textbox>

                                     <asp:textbox id="txtPaidAmount" runat="server" height="22px" style="text-align:right;"  cssclass="label" borderwidth="1px" bordercolor="#cccccc" width="70px" maxlength="10" Text="0.00" visible="false"   onkeypress="return OnlyNumeric(event);"></asp:textbox>

                                </td>
                            </tr>
                              <tr>
                                  <td>
                                   <asp:CheckBox ID="chkNoMergePrint" runat="server" CssClass="lblCaption1" Text="No Merge Print" Checked="false" visible="false"   />&nbsp;&nbsp;

                                  </td>
                              </tr>
                           </table>
                     </div>
              <div style="display:block;padding-top: 0px; width: 800PX; height:auto; border: thin; border-color: #cccccc; border-style: groove;">
                
                    <asp:Label ID="Label1" runat="server" CssClass="lblCaption1" Text="Payment"  style="font-weight:bold;" ></asp:Label>  &nbsp;
                <table style="width:100%;">
                    <tr>
                        <td  class="lblCaption1" style="width:80px;">Pay.Type</td>
                        <td style="width:105px;">
                               <asp:dropdownlist id="drpPayType" runat="server" cssclass="label" width="100px" borderwidth="1px" bordercolor="#cccccc"  autopostback="true"   OnSelectedIndexChanged="drpPayType_SelectedIndexChanged" >
                                                     
                                   </asp:dropdownlist>
                        </td>
                        <td class="lblCaption1" style="width:70px;">
                            Cash Amt.
                        </td>
                        <td>
                        <asp:TextBox ID="txtCashAmt" runat="server" Width="70px"  height="22px"    CssClass="label" BorderWidth="1px" BorderColor="#cccccc"  text="0.00"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                        </td>
                    </tr>
                    </table>
                 <div id="divCC" runat="server" visible="false" >
                 <table style="width:100%;">
                    <tr>
                        <td  class="lblCaption1"  style="width:80px;">Type</td>
                        <td style="width:105px;">
                               <asp:dropdownlist id="drpccType" runat="server" cssclass="label" width="100px" borderwidth="1px" bordercolor="#cccccc"  >
                                         
                                   </asp:dropdownlist>
                        </td>
                        <td class="lblCaption1" style="width:70px;">
                            C.C Amt.
                        </td>
                        <td>
                             <asp:TextBox ID="txtCCAmt" runat="server" Width="70px" height="22px"    CssClass="label" BorderWidth="1px" BorderColor="#cccccc"  autopostback="true" OnTextChanged="txtCCAmt_TextChanged"   onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        </td>
                         <td class="lblCaption1">
                            C.C No.
                        </td>
                        <td>
                             <asp:TextBox ID="txtCCNo" runat="server" Width="70px" height="22px"     CssClass="label" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>
                        </td>
                        <td class="lblCaption1">
                            Holder
                        </td>
                        <td>
                             <asp:TextBox ID="txtCCHolder" runat="server" Width="100px"  height="22px"    CssClass="label" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>
                        </td>
                        <td class="lblCaption1">
                            Ref No.
                        </td>
                        <td>
                             <asp:TextBox ID="txtCCRefNo" runat="server" Width="70px"  height="22px"    CssClass="label" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>
                        </td>
                    </tr>
                </table>
                     </div>
                  <div id="divCheque" runat="server" visible="false" >
                <table style="width:100%;">
                    <tr>
                        <td  class="lblCaption1"  style="width:80px;">
                           Cheq. No.
                        </td>
                        <td>
                             <asp:TextBox ID="txtDcheqno" runat="server" Width="100px"  height="22px"   CssClass="label" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>
                        </td>
                         <td class="lblCaption1" style="width:70px;">
                          Cheq. Amt.
                        </td>
                        <td>
                             <asp:TextBox ID="txtChqAmt" runat="server" Width="70px"  height="22px"    CssClass="label" BorderWidth="1px" BorderColor="#cccccc" autopostback="true" OnTextChanged="txtChqAmt_TextChanged"  onkeypress="return OnlyNumeric(event);" ></asp:TextBox>
                        </td>
                        <td class="lblCaption1">
                            Cheq. Date
                        </td>
                        <td>
                             <asp:textbox id="txtDcheqdate" runat="server" width="70px" height="22px" cssclass="label" borderwidth="1px" bordercolor="#cccccc" maxlength="10" onkeypress="return OnlyNumeric(event);"></asp:textbox>
                            <asp:calendarextender id="Calendarextender4" runat="server"
                                enabled="True" targetcontrolid="txtDcheqdate" format="dd/MM/yyyy">
                                </asp:calendarextender>                     

                        </td>
                        <td class="lblCaption1">
                           Bank
                        </td>
                        <td>
                             <asp:DropDownList ID="drpBank" CssClass="label" runat="server" Width="250px"  ></asp:DropDownList>
                        </td>
                    </tr>
                </table>
                </div>


           </div>

          <div id="divAdvane" runat="server" visible="false" >
            <fieldset style="width:800px;">
                <legend class="lblCaption1">Advance Details</legend>
                <table style="width:100%;">
                    <tr>
                         <td class="lblCaption1">
                            <asp:ListBox ID="lstAdvDtls" runat="server" CssClass="label" Height="50px" Width="250px"  ></asp:ListBox>
                                Avail. Adv
                             <asp:TextBox ID="txtAvailAdv" runat="server" Width="100px"    CssClass="label" BorderWidth="1px" BorderColor="#cccccc"   visible="false"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                             Advance Amt.
                             <asp:TextBox ID="txtAdvAmt" runat="server" Width="100px"    CssClass="label" BorderWidth="1px" BorderColor="#cccccc"    visible="false"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                        </td>

                     </tr>
                    </table>
            </fieldset>
          </div> 
         <div id="divMergeInvoiceNo" runat="server" visible="false" >
               <table style="width:300px;">

                    <tr>
                         <td class="lblCaption1">
                             Merge Inv. No
                         </td>
                        <td>
                           <asp:TextBox ID="txtMergeInvoices" runat="server" Width="100px"    CssClass="label" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>

                        </td>
                    </tr>
               </table>

         </div>
  <div id="divMultiCurrency" runat="server" visible="false" >
            <fieldset style="width:800px;">
                <legend class="lblCaption1">Multi Currency</legend>
                <table style="width:100%;">
                    <tr>
                         <td class="lblCaption1">
                           
                              Cur. Type
                        </td>
                        <td>
                              <asp:dropdownlist id="drpCurType" runat="server" cssclass="label" width="120px" borderwidth="1px" bordercolor="#cccccc"  >
                                   </asp:dropdownlist>
                        </td>
                       <td class="lblCaption1">
                                Rcvd. Amt
                        </td>
                        <td>
                           <asp:TextBox ID="txtRcvdAmount" runat="server" Width="100px"    CssClass="label" BorderWidth="1px" BorderColor="#cccccc"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                        </td>
                          <td class="lblCaption1">
                            Conv. Rate
                        </td>
                        <td>
                            <asp:TextBox ID="txtConvRate" runat="server" Width="100px"    CssClass="label" BorderWidth="1px" BorderColor="#cccccc"  autopostback="true" OnTextChanged="txtConvRate_TextChanged" ></asp:TextBox>
                        </td>

                        <td class="lblCaption1">
                            Cur. Value
                        </td>
                        <td>
                            <asp:TextBox ID="txtcurValue" runat="server" Width="100px"    CssClass="label" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>
                        </td>
                        <td class="lblCaption1">
                           Bal. Amt.
                        </td>
                        <td>
                            <asp:label id="LblMBalAmt" runat="server" forecolor="red" font-bold="true" style="letter-spacing: 1px;" cssclass="label" onkeypress="return OnlyNumeric(event);"></asp:label>
                        </td>

                     </tr>
                    </table>
            </fieldset>
          </div>
   <div style="padding-top: 0px; width: 800PX; height: 20px; border: thin; border-color: #cccccc; border-style: groove;background-color: #efefef;">
        <table width="100%" >
                    <tr>
                        <td class="lblCaption1" style="width:500px;">
                           Last Invoice No - Date:
                            &nbsp;
                                    <asp:Label ID="lblLstInvNoDate" runat="server" CssClass="lblCaption1"  ></asp:Label>  &nbsp;
                           First Invoice Date:
                                   <asp:Label ID="lblFstInvDate" runat="server" CssClass="lblCaption1" ></asp:Label> 
                        </td>

                        <td class="lblCaption1">
                            Last Modified User:
                            &nbsp;
                                    <asp:Label ID="lblModifiedUser" runat="server" CssClass="lblCaption1" ></asp:Label>
                        </td>

                        <td class="lblCaption1">
                           Created User :
                            &nbsp;
                                    <asp:Label ID="lblCreatedUser" runat="server" CssClass="lblCaption1"  ></asp:Label>
                        </td>

                      

                    </tr>

                </table>
    </div>
    <table style="width:100%;">
                    <tr>
                        <td  class="lblCaption1" style="width:150px;height:40px;">PriorAuthorization ID</td>
                        <td style="width:200px;">
                             <asp:TextBox ID="txtPriorAuthorizationID" runat="server" Width="183px" height="22px"   CssClass="label" BorderWidth="1px" BorderColor="#cccccc"   ></asp:TextBox>

                         </td>
                        <td>
                             <asp:CheckBox ID="ChkTopupCard" runat="server" CssClass="lblCaption1" Text="Topup Card" Checked="false" Enabled="false" />
                             <asp:TextBox ID="TxtTopupAmt" runat="server" Width="100px"  height="22px"    CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Text="0.00"  Enabled="false"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                       </td>
                </tr>
        <tr>
            <td>
                 <asp:CheckBox ID="chkReadyforEclaim" runat="server" CssClass="lblCaption1" Text="Ready for Eclaim" Checked="true"  visible="false"/>
            </td>
            
        </tr>
        </table>


                 
                </contenttemplate>
                 
               </asp:TabPanel>

                 <asp:TabPanel runat="server" ID="TabPanel13" HeaderText="Diagnosis" Width="100%">
                <contenttemplate>
                    <table width="100%">
                        <tr>
                            <td style="width: 100px; height: 30px;" class="lblCaption1" >
                                 Type
                            </td>
                            <td style="width: 275px;">
                                <asp:updatepanel id="UpdatePanel37" runat="server">
                                    <contenttemplate>
                                <asp:DropDownList ID="drpClmType" runat="server" CssClass="label" Width="270px">
                                    <asp:ListItem Value="1" Selected="True">No Bed + No Emergency Room (OP)</asp:ListItem>
                                    <asp:ListItem Value="2">No Bed + Emergency Room (OP)</asp:ListItem>
                                    <asp:ListItem Value="3">InPatient Bed + No Emergency Room (IP)</asp:ListItem>
                                    <asp:ListItem Value="4">InPatient Bed + Emergency Room (IP)</asp:ListItem>
                                    <asp:ListItem Value="5">Daycase Bed + No Emergency Room (Day Care)</asp:ListItem>
                                    <asp:ListItem Value="6">Daycase Bed + Emergency Room (Day Care)</asp:ListItem>
                                    <asp:ListItem Value="7">National Screening</asp:ListItem>
                                </asp:DropDownList>
                                 </contenttemplate>
                                </asp:updatepanel>

                            </td>
                            <td style="height: 40px;width:70px;" class="lblCaption1">
                               Start Date  <span style="color:red;">* </span>
                            </td>
                            <td style="width: 130px;">
                                <asp:updatepanel id="UpdatePanel33" runat="server">
                                    <contenttemplate>
                                    <asp:TextBox ID="txtClmStartDate" runat="server" Width="75px" height="22PX" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:calendarextender id="CalendarExtender2" runat="server"
                                        enabled="True" targetcontrolid="txtClmStartDate" format="dd/MM/yyyy">
                                    </asp:calendarextender>
                                    <asp:maskededitextender id="MaskedEditExtender5" runat="server" enabled="true" targetcontrolid="txtClmStartDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>
                                    <asp:TextBox ID="txtClmFromTime" runat="server" Width="30px" height="22PX" MaxLength="5" BorderWidth="1px" BorderColor="#cccccc"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:maskededitextender id="MaskedEditExtender7" runat="server" enabled="true" targetcontrolid="txtClmFromTime" mask="99:99" masktype="Time"></asp:maskededitextender>

                 
                            </contenttemplate>
                                </asp:updatepanel>
                            </td>
                            <td  class="lblCaption1" style="width: 70px;">
                               End Date  <span style="color:red;">* </span>
                            </td>
                            <td style="width: 130px;">
                                <asp:updatepanel id="UpdatePanel34" runat="server">
                                    <contenttemplate>
                                <asp:TextBox ID="txtClmEndDate" runat="server" Width="75px" height="22PX" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                <asp:calendarextender id="CalendarExtender1" runat="server"
                                    enabled="True" targetcontrolid="txtClmEndDate" format="dd/MM/yyyy">
                                </asp:calendarextender>
                                <asp:maskededitextender id="MaskedEditExtender6" runat="server" enabled="true" targetcontrolid="txtClmEndDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>
                                 <asp:TextBox ID="txtClmToTime" runat="server" Width="30px" height="22PX" MaxLength="5" BorderWidth="1px" BorderColor="#cccccc"  onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:maskededitextender id="MaskedEditExtender8" runat="server" enabled="true" targetcontrolid="txtClmToTime" mask="99:99" masktype="Time"></asp:maskededitextender>
                                    </contenttemplate>
                                </asp:updatepanel>
                            </td>


                        </tr>
                        <tr>
                            <td style=" height: 30px;" class="lblCaption1" >
                               Start Type
                            </td>
                            <td>
                                <asp:updatepanel id="UpdatePanel35" runat="server">
                                    <contenttemplate>
                                <asp:DropDownList ID="drpClmStartType" runat="server" CssClass="label" Width="270px">
                                    <asp:ListItem Value="1" Selected="True">Elective</asp:ListItem>
                                    <asp:ListItem Value="2">Emergency</asp:ListItem>
                                    <asp:ListItem Value="3">Transfer</asp:ListItem>
                                    <asp:ListItem Value="4">Live Birth</asp:ListItem>
                                    <asp:ListItem Value="5">Still Birth</asp:ListItem>
                                    <asp:ListItem Value="6">Dead on Arrival</asp:ListItem>
                                </asp:DropDownList>
                                         </contenttemplate>
                                </asp:updatepanel>
                            </td>
                            <td   class="lblCaption1" >
                              End Type 
                            </td>
                            <td colspan="3">
                                <asp:updatepanel id="UpdatePanel36" runat="server">
                                    <contenttemplate>
                                <asp:DropDownList ID="drpClmEndType" runat="server" CssClass="label" Width="200px">
                                    <asp:ListItem Value="1" Selected="True">Discharged with approval</asp:ListItem>
                                    <asp:ListItem Value="2">Discharged against advice</asp:ListItem>
                                    <asp:ListItem Value="3">Discharged absent without leave</asp:ListItem>
                                    <asp:ListItem Value="4">Transfer to another facility</asp:ListItem>
                                    <asp:ListItem Value="5">Deceased</asp:ListItem>

                                </asp:DropDownList>
                                         </contenttemplate>
                                </asp:updatepanel>

                            </td>
                        </tr>
       
                    </table>
                     <table  width="700px">
                          <tr>
                              <td  class="lblCaption1"  style="width:105px;">
                                  Type <span style="color:red;">* </span>
                              </td>
                              <td  class="lblCaption1" style="width:105px;">
                                  Code <span style="color:red;">* </span>
                              </td>
                                <td  class="lblCaption1">
                                    Description
                              </td>
                              
                  
                 
                          </tr>
                          <tr>
                              <td>
                                    <asp:dropdownlist id="drpDiagType" runat="server" cssclass="label" width="100px" borderwidth="1px" bordercolor="#cccccc" >
                                            <asp:ListItem Value="Principal" selected="True">Principal</asp:ListItem>
                                            <asp:ListItem Value="Secondary">Secondary</asp:ListItem>
                                            <asp:ListItem Value="Admitting">Admitting</asp:ListItem>
                                           
                       </asp:dropdownlist>
                              </td>
                              <td>
                                 
                                  <asp:TextBox ID="txtDiagCode" runat="server" Width="100px" height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"     onblur="return DiagIdSelected()"></asp:TextBox>
                              </td>
                               <td>
                                   <asp:TextBox ID="txtDiagName" runat="server" Width="400px"  height="22px"  CssClass="label" BorderWidth="1px" BorderColor="#cccccc"      onblur="return DiagNameSelected()"></asp:TextBox>
                     
                                    <div ID="divDiagExt" style="visibility:hidden;"></div>
                                    <asp:autocompleteextender id="AutoCompleteExtender7" runat="Server" targetcontrolid="txtDiagCode" minimumprefixlength="1" servicemethod="GetDiagnosisID"
                                    CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divDiagExt"    ></asp:autocompleteextender>
                                    <asp:autocompleteextender id="AutoCompleteExtender8" runat="Server" targetcontrolid="txtDiagName" minimumprefixlength="1" servicemethod="GetDiagnosisName"
                                    CompletionListCssClass="AutoExtender"         CompletionListItemCssClass="AutoExtenderList"          CompletionListHighlightedItemCssClass="AutoExtenderHighlight"      CompletionListElementID="divDiagExt"    ></asp:autocompleteextender>
                      

                                 </td>
                               
                              <td>
                                   <asp:Button ID="btnInvDiagAdd" runat="server" Style="padding-left: 2px; padding-right: 2px;  width: 50px;" CssClass="button orange small"
                                                                     OnClick="btnInvDiagAdd_Click" Text="Add"  OnClientClick="return DiagAddVal();"/>

                              </td>
                          </tr>
                           
                      </table>
                        <table width="98%">
                                            <tr>
                                                <td>
                                                     <asp:GridView ID="gvInvoiceClaims" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                                            EnableModelValidation="True" Width="95%" gridline="none">
                                                            <HeaderStyle CssClass="GridHeader" />
                                                            <RowStyle CssClass="GridRow" />
                                                
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="110px">
                                                                    <ItemTemplate>
                                        
                                                                            <asp:Label ID="lblICDCode" width="100px" CssClass="label" runat="server" Text='<%# Bind("HIC_ICD_CODE") %>'  ></asp:Label>
                                        
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Description">
                                                                    <ItemTemplate>
                                        
                                                                            <asp:Label ID="lblICDDesc" width="400px" CssClass="label" runat="server" Text='<%# Bind("HIC_ICD_DESC") %>'></asp:Label>
                                      
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Type">
                                                                    <ItemTemplate>
                                                                          
                                                                            <asp:Label ID="lblICDType" width="70px" CssClass="label" runat="server" Text='<%# Bind("HIC_ICD_TYPE") %>'></asp:Label>
                                      
                                                                    </ItemTemplate>

                                                                </asp:TemplateField>
                                                                 <asp:TemplateField HeaderText="Delete">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="ImageButton1" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Width="16px" Height="16px"
                                                                            OnClick="DeletegvDiag_Click" />&nbsp;&nbsp;
                                                
                                                                    </ItemTemplate>
                                                                    <HeaderStyle Width="50px" />
                                                                    <ItemStyle HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                      
                                                                  
                                                                


                                                    </Columns>
                                                </asp:GridView>
                                                </td>
                                            </tr>
                                           </table>
                </contenttemplate>
                 </asp:TabPanel>
               
      
   </asp:tabcontainer>
         <asp:LinkButton ID="lnkMessage" runat="server" CssClass="LabelStyle" Text="" Enabled="false"
                                ForeColor="Blue"></asp:LinkButton>
                            <asp:ModalPopupExtender ID="MPExtMessage" runat="server" TargetControlID="lnkMessage"
                                PopupControlID="pnlPrevPrice" BackgroundCssClass="modalBackground" CancelControlID="btnClose"
                                PopupDragHandleControlID="pnlPrevPrice">
                            </asp:ModalPopupExtender>
                            <asp:Panel ID="pnlPrevPrice" runat="server" Height="250px" Width="550px" CssClass="modalPopup">
                                <table cellpadding="0" cellspacing="0" width="100%" style="background-color: maroon;">
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="btnClose" runat="server" Text=" X " Font-Bold="true" CssClass="ButtonStyle" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="height: 230px; color: #ffffff; font-weight: bold;">
                                            <%=strMessage %>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
            <div style="padding-left:10%;width:100%; ">
                    <div id="myMessage" style="display:none;border:groove;height:30px;width:200px;background-color:gray;color:#ffffff;font-family:arial,helvetica,clean,sans-serif;font-size:small;   border: 1px solid #fff;
                        padding: 20px;border-radius:10px;box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5);position:absolute;"> Saved Successfully </div>
           </div>
        <br />
        <table>
            <tr><td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                         <asp:Button ID="btnSave" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button orange small" OnClick="btnSave_Click" Text="Save" OnClientClick="return InvSaveVal();" />
                 <asp:Button ID="btnDelete" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button orange small" OnClick="btnDelete_Click" Text="Delete"  OnClientClick="return DeleteInvoiceVal();"  Visible="false" />
                <asp:Button ID="btnClear" runat="server" Style="padding-left: 5px; padding-right: 5px; width: 100px;" CssClass="button orange small" OnClick="btnClear_Click" Text="Clear"  />

                                </ContentTemplate>
                    </asp:UpdatePanel>
                </td></tr>
        </table>
    </div>
    <br />
    <br />
             
    <style type="text/css">
        .style1
        {
            width: 20px;
            height: 32px;
        }

        .modalBackground
        {
            background-color: black;
            filter: alpha(opacity=40);
            opacity: 0.5;
        }

        .modalPopup
        {
            background-color: #ffffff;
            border-width: 3px;
            border-style: solid;
            border-color: Gray;
            padding: 3px;
            width: 250px;
            top: -1000px;
            position: absolute;
            padding: 0px10px10px10px;
        }
    </style>

</asp:content>
