﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;
using System.Globalization;

namespace EMR_IP.Billing
{
    public partial class Invoice : System.Web.UI.Page
    {
        static string strSessionBranchId;


        IP_AdmissionSummary objAdmSumm = new IP_AdmissionSummary();
        CommonBAL objCom = new CommonBAL();
        IP_Invoice objInv = new IP_Invoice();
        DataSet DS;
        public string strMessage = "";
        string JourNo = "";

        string HospDiscType = "", Co_Ins_Type = "", Deductible_Type, HSOM_ADV_PAYMENT_SERV_CODE, mDef_Serv_Disc_Type, mDef_Hosp_Disc_Type;
        decimal HospDiscAmt, Deductible, Co_Ins_Amt;
        Boolean Co_Incld_Hosp, Ded_Max_Amt, Ded_CoIns, SplCode, CoInsGrossAmt, CoInsGrossAmtClaimNetAmt, HMS_DEDUCTIBLE_BENEFIT_TYPE, HMS_COINS_PTREG;
        Boolean blnMultipleReg, Phy_duplicatefound, HMS_ENABLE_ADVANCE, HMS_SEP_INV_NO, HMS_INV_PRINTED, mMinistryService;
        Int32 invoicePrintPreview;
        string ReportName, strPrintType;

        #region  Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void ProcessTimLog(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../ProcessTimLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='ECLAIM' AND SEGMENT='ECLAIM_TYPE'";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("CUST_VALUE") == false)
                {
                    ViewState["CUST_VALUE"] = DS.Tables[0].Rows[0]["CUST_VALUE"].ToString();
                }
                else
                {
                    ViewState["CUST_VALUE"] = "AUH";
                }
            }


        }

        void GetHaadName1(string HaadCode, out string strHaadName)
        {
            strHaadName = "";
            string Criteria = " 1=1 ";
            Criteria += " AND HHS_CODE='" + HaadCode + "'";
            objCom = new CommonBAL();
            DataSet DSHaad = new DataSet();
            DSHaad = objCom.HaadServiceGet(Criteria, "", "1");

            if (DSHaad.Tables[0].Rows.Count > 0)
            {
                strHaadName = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_DESCRIPTION"]);
            }

        }

        DataSet GetHaadName(string HaadCode)
        {

            string Criteria = " 1=1 ";
            Criteria += " AND HHS_CODE='" + HaadCode + "'";
            objCom = new CommonBAL();
            DataSet DSHaad = new DataSet();
            DSHaad = objCom.HaadServiceGet(Criteria, "", "1");


            return DSHaad;
        }


        DataSet GetServiceMasterName(string SearchType, string ServCode)
        {
            //ServID = "";
            //ServName = "";
            //HSM_TYPE = "";
            //CatID = "";
            //CatTyp = "";
            //Fee = 0;

            //obsCode="";
            //obsDesc="";
            //obsvalueType="";

            string Criteria = " 1=1 AND HSM_STATUS='A' ";
            if (SearchType == "HSM_SERV_ID")
            {
                Criteria += " AND  HSM_SERV_ID   ='" + ServCode + "'";
            }
            else
            {
                Criteria += " AND  HSM_HAAD_CODE   ='" + ServCode + "'";
            }

            objCom = new CommonBAL();
            DataSet DSServ = new DataSet();
            DSServ = objCom.ServiceMasterGet(Criteria);
            txtServName.Text = "";
            if (DSServ.Tables[0].Rows.Count > 0)
            {
                //ServID = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_SERV_ID"]);
                //ServName = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_NAME"]);
                //HSM_TYPE = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TYPE"]);
                //CatID = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CAT_ID"]);
                //CatTyp = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPE"]);
                //if (DSServ.Tables[0].Rows[0].IsNull("HSM_CODE_TYPE") == false && Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPE"]) != "")
                //    Fee = Convert.ToDecimal(DSServ.Tables[0].Rows[0]["HSM_FEE"]);

                //  obsCode = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_CODE"]);
                // obsDesc = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CAT_ID"]);
                // obsvalueType = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_VALUETYPE"]);
            }

            return DSServ;

        }

        string GetServiceMasterID(string SearchType, string ServCode)
        {
            string ServID = "";
            //ServName = "";
            //HSM_TYPE = "";
            //CatID = "";
            //CatTyp = "";
            //Fee = 0;

            //obsCode="";
            //obsDesc="";
            //obsvalueType="";

            string Criteria = " 1=1 AND HSM_STATUS='A' ";
            if (SearchType == "HSM_SERV_ID")
            {
                Criteria += " AND  HSM_SERV_ID   ='" + ServCode + "'";
            }
            else
            {
                Criteria += " AND  HSM_HAAD_CODE   ='" + ServCode + "'";
            }

            objCom = new CommonBAL();
            DataSet DSServ = new DataSet();
            DSServ = objCom.ServiceMasterGet(Criteria);
            // txtServName.Text = "";
            if (DSServ.Tables[0].Rows.Count > 0)
            {
                if (SearchType == "HSM_SERV_ID")
                {
                    ServID = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_HAAD_CODE"]);
                }
                else
                {
                    ServID = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_SERV_ID"]);
                }

            }

            return ServID;

        }

        void GetStaffName(string DrCode, out string DrName)
        {
            DrName = "";
            string Criteria = " 1=1 ";
            Criteria += " AND HSFM_STAFF_ID='" + DrCode + "'";
            objCom = new CommonBAL();
            DataSet DSHaad = new DataSet();
            DSHaad = objCom.GetStaffMaster(Criteria);

            if (DSHaad.Tables[0].Rows.Count > 0)
            {
                DrName = Convert.ToString(DSHaad.Tables[0].Rows[0]["FullName"]);
            }

        }

        void GetRefDrName(string DrCode, out string DrName)
        {
            DrName = "";
            string Criteria = " 1=1 ";
            Criteria += " AND HRDM_REF_ID = '" + DrCode + "'";
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.RefDoctorMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                DrName = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]);
            }

        }

        void GetCompanyAgrDtlsAll()
        {
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            if (txtSubCompanyID.Text.Trim() != "" && txtSubCompanyID.Text.Trim() != null)
            {
                string Criteria = " 1=1 ";
                Criteria += " AND   HAS_COMP_ID='" + txtSubCompanyID.Text.Trim() + "'";
                DS = objCom.AgrementServiceGet(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    ViewState["CompanyAgrDtls"] = DS;
                }
            }

        }
        /*
        DataRow[] GetCompanyAgrDtls(string ServCode, string CompanyID)
        {
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            DataRow[] result = null;
            string Criteria = " 1=1 ";
            Criteria += " AND HAS_SERV_ID='" + ServCode + "' AND HAS_COMP_ID='" + CompanyID + "'";
            if (ViewState["CompanyAgrDtls"] != "" && ViewState["CompanyAgrDtls"] != null)
            {
                DS = (DataSet)ViewState["CompanyAgrDtls"];

                if (DS.Tables[0].Rows.Count > 0)
                {
                    result = DS.Tables[0].Select(Criteria);

                }

            }
            return result;



        }
        */
        DataSet GetCompanyAgrDtls(string ServCode, string CompanyID)
        {



            DataSet DS = new DataSet();
            objCom = new CommonBAL();

            string Criteria = " 1=1 ";
            Criteria += " AND HAS_SERV_ID='" + ServCode + "' AND HAS_COMP_ID='" + CompanyID + "'";
            DS = objCom.AgrementServiceGetTop1(Criteria);
            //if (DS.Tables[0].Rows.Count > 0)
            //{
            //    if (DS.Tables[0].Rows[0].IsNull("HAS_NETAMOUNT") == false)
            //    {
            //        ServPrice = Convert.ToDecimal(DS.Tables[0].Rows[0]["HAS_NETAMOUNT"]);
            //    }
            //}

            return DS;



        }

        void fnSetdefaults()
        {
            if (GlobalValues.FileDescription == "SMCH")
            {
                txtHospDisc.Enabled = false;
                drpHospDiscType.Enabled = false;
                chkNoMergePrint.Visible = true;
            }

            Phy_duplicatefound = false;
            HMS_ENABLE_ADVANCE = Convert.ToString(ViewState["HSOM_ENABLE_ADV_PAYMENT"]) == "Y" ? true : false;
            invoicePrintPreview = Convert.ToString(ViewState["HSOM_ENABLE_ADV_PAYMENT"]) == "Y" ? 1 : 0;

            if (HMS_ENABLE_ADVANCE == true)
            {
                //  HMS_ADV_SERV_CODE = Convert.ToString(ViewState["HSOM_ADV_PAYMENT_SERV_CODE"]);
                if (Convert.ToString(ViewState["HSOM_ADV_PAYMENT_SERV_CODE"]) == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Please set Advance Payment Service Code in System Options.');", true);

                }
                // CmdAdvanceDtls.Visible = True
            }



            drpPayType.Items.Insert(0, "Cash");
            drpPayType.Items[0].Value = "Cash";

            drpPayType.Items.Insert(1, "Credit Card");
            drpPayType.Items[1].Value = "Credit Card";

            drpPayType.Items.Insert(2, "Cheque");
            drpPayType.Items[2].Value = "Cheque";

            drpPayType.Items.Insert(3, "Debit Card");
            drpPayType.Items[3].Value = "Debit Card";


            ViewState["NewFlag"] = true;

            if (HMS_ENABLE_ADVANCE == true && Convert.ToString(ViewState["HSOM_ADV_PAYMENT_SERV_CODE"]) != "")
            {

                drpPayType.Items.Insert(4, "Advance");
                drpPayType.Items[4].Value = "Advance";

            }

            if (GlobalValues.BLNALLOWADVANCEPAYMENT_INVOICE == true)
            {

                drpPayType.Items.Insert(5, "Utilise Advance");
                drpPayType.Items[5].Value = "Utilise Advance";
            }

            HMS_SEP_INV_NO = Convert.ToString(ViewState["HSOM_SEP_INV_NO"]) == "Y" ? true : false;

            HMS_INV_PRINTED = Convert.ToString(ViewState["HSOM_INV_PRINTED"]) == "Y" ? true : false;

            mDef_Serv_Disc_Type = "";
            mDef_Hosp_Disc_Type = "";


            mDef_Serv_Disc_Type = Convert.ToString(ViewState["HSOM_ENABLE_ADV_PAYMENT"]) == "" ? "%" : Convert.ToString(ViewState["HSOM_ENABLE_ADV_PAYMENT"]);// IIf(IsNull(Temprs.Fields(0).Value), "", IIf(Trim(Temprs.Fields(0).Value) = "", "%", Temprs.Fields(0).Value))
            mDef_Hosp_Disc_Type = Convert.ToString(ViewState["HSOM_ENABLE_ADV_PAYMENT"]) == "" ? "%" : Convert.ToString(ViewState["HSOM_ENABLE_ADV_PAYMENT"]); //IIf(IsNull(Temprs.Fields(1).Value), "", IIf(Trim(Temprs.Fields(1).Value) = "", "%", Temprs.Fields(1).Value))
            //lblhospdisctype.Caption = mDef_Hosp_Disc_Type

            objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND HSM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HSM_STATUS='A'";

            DataSet DSServ = new DataSet();
            DSServ = objCom.ServiceMasterGet(Criteria);

            if (DSServ.Tables[0].Rows.Count <= 0)
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Please set Advance Payment Service Code in System Options.');", true);

            }

            if (GlobalValues.FileDescription == "AEMC" || GlobalValues.FileDescription == "ALDAR" || GlobalValues.FileDescription == "KBR") mMinistryService = true; else mMinistryService = false;


            BIndCCType();
            TreatmentType();
            if (GlobalValues.FileDescription == "SMCH" || GlobalValues.FileDescription == "ALAMAL")
            {
                btnPrntDeductible.Visible = true;
                btnPrntDeductible.Text = "Merge Print";

                if (GlobalValues.FileDescription == "ALAMAL")
                {
                    drpRemarks.Visible = true;
                    BindRemarks();
                }
            }
            else if (GlobalValues.FileDescription == "FINECARE")
            {
                btnPrntDeductible.Visible = true;
                btnPrntDeductible.Text = "Expense";
            }
            else if (GlobalValues.FileDescription == "KQMC")
            {
                btnPrntDeductible.Visible = false;
            }
            else if (GlobalValues.FileDescription == "ALDAR")
            {
                drpRemarks.Visible = true;
            }
            else if (GlobalValues.FileDescription == "GRACE")
            {
                //Label20.Caption = "Trtmt Date"
            }
            else if (GlobalValues.FileDescription == "ALREEM")
            {
                drpCommissionType.Visible = true;

                lblInsCard.Text = "Comm.Type.";
                lblInsCard.Visible = true;
                // CmbCommissionType.Top = Me.CmbStatus.Top

            }

            if (GlobalValues.FileDescription == "BMC" || GlobalValues.FileDescription == "GMC")
            {
                //ChkPrintCreditBill.Visible = False
                //  ChkOnlySave.Visible = False
            }




        }

        void fnSetServices(string CompCategory, string CompID, string PolicyType)
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HCB_COMP_ID='" + CompID + "' AND HCB_CAT_TYPE='" + PolicyType + "'";
            objCom = new CommonBAL();
            DS = objCom.CompBenefitsGet(Criteria);


            if (DS.Tables[0].Rows.Count <= 0)
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Company Benefits are not Found for this company.');", true);
                HospDiscType = "";
                HospDiscAmt = 0;
                Deductible = 0;
                Co_Ins_Type = "%";
                Co_Ins_Amt = 100;
                Co_Incld_Hosp = false;
                Ded_Max_Amt = false;
                Ded_CoIns = false;
                CoInsGrossAmt = false;
                CoInsGrossAmtClaimNetAmt = false;
                Deductible_Type = "$";
                SplCode = false;
                HMS_DEDUCTIBLE_BENEFIT_TYPE = false;
            }
            else
            {
                HospDiscType = DS.Tables[0].Rows[0].IsNull("HCB_HOSP_DISC_TYPE") == true ? "" : Convert.ToString(DS.Tables[0].Rows[0]["HCB_HOSP_DISC_TYPE"]);//IIf(IsNull(RSCompBfts.Fields("HCB_HOSP_DISC_TYPE").Value), "", RSCompBfts.Fields("HCB_HOSP_DISC_TYPE").Value)
                HospDiscAmt = DS.Tables[0].Rows[0].IsNull("HCB_HOSP_DISC_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_HOSP_DISC_AMT"]);//IIf(IsNull(RSCompBfts.Fields("HCB_HOSP_DISC_AMT").Value), 0, RSCompBfts.Fields("HCB_HOSP_DISC_AMT").Value)
                Deductible_Type = DS.Tables[0].Rows[0].IsNull("HCB_DEDUCTIBLE_TYPE") == true ? "$" : Convert.ToString(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE_TYPE"]);// IIf(IsNull(RSCompBfts.Fields("HCB_DEDUCTIBLE_TYPE").Value), "$", RSCompBfts.Fields("HCB_DEDUCTIBLE_TYPE").Value)
                Deductible = DS.Tables[0].Rows[0].IsNull("HCB_DEDUCTIBLE") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE"]);//IIf(IsNull(RSCompBfts.Fields("HCB_DEDUCTIBLE").Value), 0, RSCompBfts.Fields("HCB_DEDUCTIBLE").Value)
                SplCode = DS.Tables[0].Rows[0].IsNull("HCB_SPL_CODE") == true ? false : Convert.ToString(DS.Tables[0].Rows[0]["HCB_SPL_CODE"]) == "Y" ? true : false;// IIf(IsNull(RSCompBfts.Fields("HCB_SPL_CODE").Value), False, IIf(RSCompBfts.Fields("HCB_SPL_CODE").Value = "Y", True, False))

                Co_Incld_Hosp = DS.Tables[0].Rows[0].IsNull("HCB_CONSLT_INCL_HOSP_DISC") == true ? false : Convert.ToString(DS.Tables[0].Rows[0]["HCB_CONSLT_INCL_HOSP_DISC"]) == "Y" ? true : false;//  IIf(IsNull(RSCompBfts.Fields("HCB_CONSLT_INCL_HOSP_DISC").Value), False, IIf(RSCompBfts.Fields("HCB_CONSLT_INCL_HOSP_DISC").Value = "Y", True, False))
                Ded_Max_Amt = DS.Tables[0].Rows[0].IsNull("HCB_DEDUCTIBLE_MAX_CONSLT_AMT") == true ? false : Convert.ToString(DS.Tables[0].Rows[0]["HCB_DEDUCTIBLE_MAX_CONSLT_AMT"]) == "Y" ? true : false;// IIf(IsNull(RSCompBfts.Fields("HCB_DEDUCTIBLE_MAX_CONSLT_AMT").Value), False, IIf(RSCompBfts.Fields("HCB_DEDUCTIBLE_MAX_CONSLT_AMT").Value = "Y", True, False))
                Ded_CoIns = DS.Tables[0].Rows[0].IsNull("HCB_DED_COINS_APPLICABLE") == true ? false : Convert.ToString(DS.Tables[0].Rows[0]["HCB_DED_COINS_APPLICABLE"]) == "Y" ? true : false;// IIf(IsNull(RSCompBfts.Fields("HCB_DED_COINS_APPLICABLE").Value), False, IIf(RSCompBfts.Fields("HCB_DED_COINS_APPLICABLE").Value = "Y", True, False))
                CoInsGrossAmt = DS.Tables[0].Rows[0].IsNull("HCB_COINS_FROM_GROSSAMT") == true ? false : Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINS_FROM_GROSSAMT"]) == "Y" ? true : false; ;// IIf(IsNull(RSCompBfts.Fields("HCB_COINS_FROM_GROSSAMT").Value), False, IIf(RSCompBfts.Fields("HCB_COINS_FROM_GROSSAMT").Value = "Y", True, False))
                CoInsGrossAmtClaimNetAmt = DS.Tables[0].Rows[0].IsNull("HCB_COINSFROMGROSS_CLAIMFROMNET") == true ? false : Convert.ToString(DS.Tables[0].Rows[0]["HCB_COINSFROMGROSS_CLAIMFROMNET"]) == "Y" ? true : false;// IIf(IsNull(RSCompBfts.Fields("HCB_COINSFROMGROSS_CLAIMFROMNET").Value), False, IIf(RSCompBfts.Fields("HCB_COINSFROMGROSS_CLAIMFROMNET").Value = "Y", True, False))
                hidInsTrtnt.Value = DS.Tables[0].Rows[0].IsNull("HCB_INS_TYPE_TREATMENT") == true ? "" : Convert.ToString(DS.Tables[0].Rows[0]["HCB_INS_TYPE_TREATMENT"]);// IIf(IsNull(RSCompBfts.Fields("HCB_INS_TYPE_TREATMENT").Value), "", RSCompBfts.Fields("HCB_INS_TYPE_TREATMENT").Value)
                HMS_DEDUCTIBLE_BENEFIT_TYPE = DS.Tables[0].Rows[0].IsNull("HCB_DED_INSURANCE_TYPE") == true ? false : Convert.ToString(DS.Tables[0].Rows[0]["HCB_DED_INSURANCE_TYPE"]) == "Y" ? true : false;//  IIf(IsNull(RSCompBfts.Fields("HCB_DED_INSURANCE_TYPE").Value), False, IIf(RSCompBfts.Fields("HCB_DED_INSURANCE_TYPE").Value = "Y", True, False))

            }


            //lblhospdisctype.Caption = IIf(HospDiscType = "", "", IIf(HospDiscType = "%", "%", "$"))
            //TxtHospDisc.Text = Format(HospDiscAmt, HMS_CURRENCY_FORMAT)
            //'lblCoInsType.Caption = IIf(Co_Ins_Type = "", "", IIf(Co_Ins_Type = "P", "%", "$"))
            //'txtCon_insAmt.Text = Format(Co_Ins_Amt, HMS_CURRENCY_FORMAT)
            //If txtInsTrtnt <> "Service Type" Then
            //    If RsCompBftsDetails.State = adStateOpen Then RsCompBftsDetails.Close
            //    RsCompBftsDetails.CursorLocation = adUseClient
            //    RsCompBftsDetails.Open "select * from HMS_COMP_BENEFITS_DETAILS WHERE HCBD_BRANCH_ID ='" & HMS_BRANCH & "'  AND HCBD_COMP_ID='" & Trim(CompID) & "'  AND HCBD_CAT_TYPE='" & Trim(PolicyType) & "' and HCBD_CATEGORY_ID='" & InsTrntCol(CmbTrtntType.ListIndex + 1) & "' AND HCBD_CATEGORY_TYPE='C'", CnInvoice, adOpenStatic, adLockOptimistic
            //    If RsCompBftsDetails.BOF And RsCompBftsDetails.EOF Then
            //        If App.Title = "0" Then
            //            BCoAmt = 0
            //            BCoType = "%"
            //            BCovered = True
            //            Blimit = 0
            //        Else
            //            BCoAmt = 100
            //            BCoType = "%"
            //            BCovered = False
            //            Blimit = 0
            //            If Not HMS_COINS_PTREG Then MsgBox "Company Benefits Informations are not found. So this invoice cannot be claimed!", vbInformation, HMS_ErrorCaption
            //        End If
            //    Else
            //        BCoAmt = IIf(IsNull(RsCompBftsDetails.Fields("HCBD_CO_INS_AMT").Value), 0, RsCompBftsDetails.Fields("HCBD_CO_INS_AMT").Value)
            //        BCoType = IIf(IsNull(RsCompBftsDetails.Fields("HCBD_CO_INS_TYPE").Value), "%", RsCompBftsDetails.Fields("HCBD_CO_INS_TYPE").Value)
            //        BCovered = IIf(IsNull(RsCompBftsDetails.Fields("HCBD_CATEGORY_TYPE").Value), "", IIf(RsCompBftsDetails.Fields("HCBD_CATEGORY_TYPE").Value = "C" Or RsCompBftsDetails.Fields("HCBD_CATEGORY_TYPE").Value = "P", True, False))
            //        Blimit = IIf(IsNull(RsCompBftsDetails.Fields("HCBD_LIMIT").Value), 0, RsCompBftsDetails.Fields("HCBD_LIMIT").Value)
            //    End If
            //Else
            //        BCoAmt = 100
            //        BCoType = "%"
            //        BCovered = False
            //        Blimit = 0
            //End If
            //Co_Ins_Type = BCoType
            //Co_Ins_Amt = BCoAmt
            hidCoInsType.Value = Co_Ins_Type != "" ? Co_Ins_Type : "%";
            //txtCon_insAmt.Text = Format(BCoAmt, HMS_CURRENCY_FORMAT)
        }

        void BIndCCType()
        {


            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            IP_Invoice objInv = new IP_Invoice();
            DS = objInv.CCTypeGet(Criteria);
            drpccType.Items.Clear();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpccType.DataSource = DS;
                drpccType.DataTextField = "HCT_CC_TYPE";
                drpccType.DataValueField = "HCT_CC_TYPE";
                drpccType.DataBind();
            }



        }

        void TreatmentType()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 AND HITT_STATUS=1 ";

            objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HITT_NAME,HITT_ID", "HMS_INS_TRTMT_TYPE", Criteria, "HITT_NAME");

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpTreatmentType.DataSource = DS;
                drpTreatmentType.DataTextField = "HITT_NAME";
                drpTreatmentType.DataValueField = "HITT_ID";
                drpTreatmentType.DataBind();


            }


        }

        void GetJobnumber()
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPV_PT_ID='" + txtFileNo.Text.Trim() + "' AND (HPV_DATE BETWEEN CONVERT(DATETIME, '" + txtInvDate.Text.Trim() + " 00:00:00', 103) AND CONVERT(DATETIME, '" + txtInvDate.Text.Trim() + " 23:59:59', 103))  And HPV_DR_ID='" + txtDoctorID.Text.Trim() + "'";


            objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("isnull(HPV_JOBNUMBER,'')+ '|' + isnull(HPV_BUSINESSUNITCODE,'') HPV_JOBNUMBER ", "HMS_PATIENT_VISIT ", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {
                string strJobNo = Convert.ToString(DS.Tables[0].Rows[0]["HPV_JOBNUMBER"]);
                string[] arrJobNo = strJobNo.Split('|');
                if (arrJobNo.Length > 1)
                {
                    txtJobnumber.Text = arrJobNo[0];
                    txtJobnumber.ToolTip = arrJobNo[1];
                }

            }
        }

        void BindBank()
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HBM_BANKNAME", "HMS_BANK_MASTER ", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpBank.DataSource = DS;
                drpBank.DataTextField = "HBM_BANKNAME";
                drpBank.DataValueField = "HBM_BANKNAME";
                drpBank.DataBind();
            }
        }


        void BindCurrency()
        {

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";


            objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HCRM_ID,HCRM_NAME", "HMS_CURRENCY_MASTER ", Criteria, "");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpCurType.DataSource = DS;
                drpCurType.DataTextField = "HCRM_NAME";
                drpCurType.DataValueField = "HCRM_ID";
                drpCurType.DataBind();
            }
        }


        void fnListAdvanceDtls()
        {
            DataSet DS = new DataSet();
            IP_Invoice objInv = new IP_Invoice();
            DS = objInv.AdvanceDtlsGet(Convert.ToString(Session["Branch_ID"]), txtFileNo.Text.Trim());

            Int32 intCount = 0;

            if (DS.Tables[0].Rows.Count > 0)
            {
                intCount = DS.Tables[0].Rows.Count;
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    string strData = "";

                    strData = Convert.ToString(DS.Tables[0].Rows[i]["HIO_REMARKS"]) + " - " + Convert.ToString(Convert.ToDecimal(DS.Tables[0].Rows[i]["Used"]) + Convert.ToDecimal(DS.Tables[0].Rows[i]["AdvAmt"]));
                    lstAdvDtls.Items.Insert(i, strData);
                    lstAdvDtls.Items[i].Value = strData;


                }

            }

            DataSet DS1 = new DataSet();

            DS1 = objInv.InvoiceAdvanceDtlsGet(Convert.ToString(Session["Branch_ID"]), txtFileNo.Text.Trim(), txtInvoiceNo.Text.Trim());


            if (DS.Tables[0].Rows.Count > 0)
            {
                for (int j = 0; j < DS.Tables[0].Rows.Count; j++)
                {
                    string strData = "";

                    strData = Convert.ToString(DS.Tables[0].Rows[j]["HIM_INVOICE_ID"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["HIM_DATE"]) + " - " + Convert.ToString(DS.Tables[0].Rows[j]["AdvAmt"]);
                    lstAdvDtls.Items.Insert(intCount, strData);
                    lstAdvDtls.Items[intCount].Value = strData;
                    intCount = intCount + 1;
                }

            }



        }

        void BindRemarks()
        {

            drpRemarks.Items.Insert(0, "OP");
            drpRemarks.Items[0].Value = "OP";

            drpRemarks.Items.Insert(1, "NV");
            drpRemarks.Items[1].Value = "NV";

            drpRemarks.Items.Insert(2, "WI");
            drpRemarks.Items[2].Value = "WI";

            drpRemarks.Items.Insert(3, "FU");
            drpRemarks.Items[3].Value = "FU";

            drpRemarks.Items.Insert(4, "RV");
            drpRemarks.Items[4].Value = "RV";

        }

        void BindPlanType()
        {
            //drpPlanType.Items.Clear();

            //DataSet DS = new DataSet();
            //string Criteria = " 1=1 ";
            ////if (drpSrcCompany.Items.Count > 0)
            ////{
            ////    if (drpSrcCompany.SelectedItem.Text != "")
            ////    {

            ////        if (drpNetworkName.SelectedIndex != 0)
            ////        {
            ////            Criteria += " AND HCB_COMP_ID = '" + drpNetworkName.SelectedValue + "'";
            ////        }
            ////        else
            ////        {
            //Criteria += " AND HCB_COMP_ID = '" + txtSubCompanyID.Text.Trim() + "'";
            //// }
            //DS = dbo.CompBenefitsGet(Criteria);

            //if (DS.Tables[0].Rows.Count > 0)
            //{
            //    drpPlanType.DataSource = DS;
            //    drpPlanType.DataTextField = "HCB_CAT_TYPE";
            //    drpPlanType.DataValueField = "HCB_CAT_TYPE";
            //    drpPlanType.DataBind();
            //}

            //drpPlanType.Items.Insert(0, "--- All ---");
            //drpPlanType.Items[0].Value = "0";
            ////}
            //// }

        }

        void BindSystemOption()
        {
            string Criteria = " 1=1 ";
            objCom = new CommonBAL();
            DataSet DS = new DataSet();
            DS = objCom.SystemOptionGet(Criteria);
            ViewState["HSOM_DR_MANUALSEELCT_INVOICE"] = "0";
            ViewState["HSOM_INV_PTWAITING_ONLY"] = "N";
            ViewState["HSOM_ENABLE_ADV_PAYMENT"] = "N";
            ViewState["HSOM_INVOICE_PREVIEW"] = "0";
            // ViewState["HSOM_ADV_PAYMENT_SERV_CODE"]
            ViewState["HSOM_SEP_INV_NO"] = "N";
            ViewState["HSOM_INV_PRINTED"] = "N";
            ViewState["HSOM_DEFAULT_SERV_DISC_TYPE"] = "$";
            ViewState["HSOM_DEFAULT_HOSP_DISC_TYPE"] = "$";
            ViewState["HSOM_COSTPRICE_BILLING"] = "N";
            ViewState["HSOM_INVOICE_PAIDAMT_CHANGE"] = "N";

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HSOM_INV_PTWAITING_ONLY") == false)
                {
                    ViewState["HSOM_INV_PTWAITING_ONLY"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_INV_PTWAITING_ONLY"]);

                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_DR_MANUALSEELCT_INVOICE") == false)
                {
                    ViewState["HSOM_DR_MANUALSEELCT_INVOICE"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DR_MANUALSEELCT_INVOICE"]);

                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_ENABLE_ADV_PAYMENT") == false)
                {
                    ViewState["HSOM_ENABLE_ADV_PAYMENT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ENABLE_ADV_PAYMENT"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_INVOICE_PREVIEW") == false)
                {
                    ViewState["HSOM_INVOICE_PREVIEW"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_INVOICE_PREVIEW"]);

                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_ADV_PAYMENT_SERV_CODE") == false)
                {
                    ViewState["HSOM_ADV_PAYMENT_SERV_CODE"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ADV_PAYMENT_SERV_CODE"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_SEP_INV_NO") == false)
                {
                    ViewState["HSOM_SEP_INV_NO"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_SEP_INV_NO"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_INV_PRINTED") == false)
                {
                    ViewState["HSOM_INV_PRINTED"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_INV_PRINTED"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEFAULT_SERV_DISC_TYPE") == false)
                {
                    ViewState["HSOM_DEFAULT_SERV_DISC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DEFAULT_SERV_DISC_TYPE"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEFAULT_HOSP_DISC_TYPE") == false)
                {
                    ViewState["HSOM_DEFAULT_HOSP_DISC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DEFAULT_HOSP_DISC_TYPE"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_COSTPRICE_BILLING") == false)
                {
                    ViewState["HSOM_COSTPRICE_BILLING"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_COSTPRICE_BILLING"]);

                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_INVOICE_PAIDAMT_CHANGE") == false)
                {
                    ViewState["HSOM_INVOICE_PAIDAMT_CHANGE"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_INVOICE_PAIDAMT_CHANGE"]);

                }




            }


        }

        string fnGetClaimFormNo()
        {
            objCom = new CommonBAL();
            string strClaimNo = "";
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPV_PT_ID = '" + txtFileNo.Text + "' AND HPV_DR_ID='" + txtDoctorID.Text.Trim() + "' AND convert(varchar(10),HPV_DATE,103)='" + objCom.fnGetDate("dd/MM/yyyy") + "'";
            Criteria += " AND HPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = objCom.PatientVisitGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strClaimNo = Convert.ToString(DS.Tables[0].Rows[0]["HPV_CLAIM_NO"]);

            }
            return strClaimNo;

        }

        Boolean TopUpCardChecking(string FileNo)
        {
            objCom = new CommonBAL();
            DataSet DSInv = new DataSet();
            IP_Invoice objInv = new IP_Invoice();
            string Criteria = "1=1 ";
            Criteria += " AND HSC_PT_ID='" + FileNo + "' AND  HSC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' And HSC_CARD_NAME ='Topup' and (HSC_ISDEFAULT=1)";
            DSInv = objCom.ScanCardImageGet(Criteria);
            if (DSInv.Tables[0].Rows.Count > 0)
            {

                if (DSInv.Tables[0].Rows[0].IsNull("HSC_INS_COMP_ID") == false)
                    ViewState["mTopupBillToCode"] = Convert.ToString(DSInv.Tables[0].Rows[0]["HSC_INS_COMP_ID"]);
                if (DSInv.Tables[0].Rows[0].IsNull("HSC_INS_COMP_ID") == false)
                    ViewState["mTopupInsCode"] = Convert.ToString(DSInv.Tables[0].Rows[0]["HSC_INS_COMP_ID"]);
                if (DSInv.Tables[0].Rows[0].IsNull("HSC_INS_COMP_NAME") == false)
                    ViewState["mTopupBillToCompName"] = Convert.ToString(DSInv.Tables[0].Rows[0]["HSC_INS_COMP_NAME"]);
                if (DSInv.Tables[0].Rows[0].IsNull("HSC_INS_COMP_NAME") == false)
                    ViewState["mTopupInsName"] = Convert.ToString(DSInv.Tables[0].Rows[0]["HSC_INS_COMP_NAME"]);
                if (DSInv.Tables[0].Rows[0].IsNull("HSC_EXPIRY_DATE") == false)
                    ViewState["mTopupExpiry"] = Convert.ToString(DSInv.Tables[0].Rows[0]["HSC_EXPIRY_DATE"]);
                if (DSInv.Tables[0].Rows[0].IsNull("HSC_POLICY_NO") == false)
                    ViewState["mTopupPolicyNo"] = Convert.ToString(DSInv.Tables[0].Rows[0]["HSC_POLICY_NO"]);
                if (DSInv.Tables[0].Rows[0].IsNull("HSC_CARD_NO") == false)
                    ViewState["mTopupIDNo"] = Convert.ToString(DSInv.Tables[0].Rows[0]["HSC_CARD_NO"]);

                return true;

            }
            return false;
        }

        void IsPatientBilledToday()
        {
            DataSet DSInv = new DataSet();
            IP_Invoice objInv = new IP_Invoice();
            string Criteria = "1=1 ";
            Criteria += " AND HIM_PT_ID='" + txtFileNo.Text.Trim() + "' AND  convert(varchar(10),HIM_DATE,103)  = '" + txtInvDate.Text.Trim() + "'";
            DSInv = objInv.InvoiceMasterGet(Criteria);
            //  System.DateTime.Now.Date.ToString("yyyy-MM-dd")

            if (DSInv.Tables[0].Rows.Count > 0)
            {
                string strInvNo = "";

                for (int i = 0; i < DSInv.Tables[0].Rows.Count; i++)
                {
                    strInvNo += Convert.ToString(DSInv.Tables[0].Rows[i]["HIM_INVOICE_ID"]) + ",";
                }
                if (strInvNo.Length > 1)
                {
                    strInvNo = strInvNo.Substring(0, strInvNo.Length - 1);
                }

                MPExtMessage.Show();
                strMessage = "This Patient is already billed today. Invoice Number(s) - " + strInvNo;

            }
        }

        Boolean IsPatientBilledSameDaySameDr(Boolean ShowMsg)
        {
            DataSet DSInv = new DataSet();
            IP_Invoice objInv = new IP_Invoice();
            string Criteria = "1=1 ";
            Criteria += " AND HIM_PT_ID='" + txtFileNo.Text.Trim() + "' AND  convert(varchar(10),HIM_DATE,103)  = '" + txtInvDate.Text.Trim() + "'";
            Criteria += " AND  HIM_DR_CODE='" + txtDoctorID.Text.Trim() + "'";
            Criteria += " AND  HIM_INVOICE_ID !='" + txtInvoiceNo.Text.Trim() + "'";
            Criteria += " AND  HIM_INVOICE_ID !='T" + txtInvoiceNo.Text.Trim() + "'";
            Criteria += " AND  HIM_INVOICE_TYPE ='" + drpInvType.SelectedValue + "'";

            DSInv = objInv.InvoiceMasterGet(Criteria);
            //  System.DateTime.Now.Date.ToString("yyyy-MM-dd")

            if (DSInv.Tables[0].Rows.Count > 0)
            {
                string strInvNo = "";
                strInvNo += Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_INVOICE_ID"]) + " - " + Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_DR_NAME"]) + ", ";
                if (ShowMsg == true)
                {
                    strMessage = "This Patient is already billed today for same Doctor . Invoice Number : " + strInvNo;
                    MPExtMessage.Show();
                }
                return true;


            }
            return false;
        }
        void BindCommissionType()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND HRDM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND HRDM_REF_ID='" + txtRefDrId.Text.Trim() + "'";
            DataSet DS = new DataSet();

            objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HRDM_COMMISSION_1,HRDM_COMMISSION_2,HRDM_COMMISSION_3,HRDM_COMMISSION_4", "HMS_REF_DOCTOR_MASTER", Criteria, "");


            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("HRDM_COMMISSION_1") == false && DS.Tables[0].Rows[0]["HRDM_COMMISSION_1"] != "")
                {
                    drpCommissionType.Items.Insert(0, Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_1"]));
                    drpCommissionType.Items[0].Value = Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_1"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HRDM_COMMISSION_2") == false && DS.Tables[0].Rows[0]["HRDM_COMMISSION_2"] != "")
                {
                    drpCommissionType.Items.Insert(1, Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_2"]));
                    drpCommissionType.Items[1].Value = Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_2"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HRDM_COMMISSION_3") == false && DS.Tables[0].Rows[0]["HRDM_COMMISSION_3"] != "")
                {
                    drpCommissionType.Items.Insert(2, Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_3"]));
                    drpCommissionType.Items[2].Value = Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_3"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HRDM_COMMISSION_4") == false && DS.Tables[0].Rows[0]["HRDM_COMMISSION_4"] != "")
                {
                    drpCommissionType.Items.Insert(3, Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_4"]));
                    drpCommissionType.Items[3].Value = Convert.ToString(DS.Tables[0].Rows[0]["HRDM_COMMISSION_4"]);
                }

            }

        }

        //void DisplaySecondaryInsuranceCompany()
        //{
        //    CommonDAL objCom = new CommonDAL();
        //    string Criteria=" 1=1 ";
        //    DataSet DS = new DataSet();

        //    Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HPM_PT_ID='" + txtFileNo.Text.Trim() + "'";
        //  DS =  objCom.fnGetFieldValue(" count(*) ", " HMS_PATIENT_SECONDARY_COMPANY", Criteria, "");

        //  if (DS.Tables[0].Rows.Count > 1)
        //  {
        //      DataSet DSComp = new DataSet();
        //      IP_Invoice objInv = new IP_Invoice();
        //      Criteria += " AND HPM_INS_COMP_ID
        //      objInv.Company_SecondayCompanyMasterGet(Criteria);



        //  }
        //}

        decimal fnCalculatePTAmount(string AmountType)
        {

            decimal decPTBalance = 0;

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            if (AmountType == "NPPAID")
            {
                Criteria += " AND HIT_SERV_CODE='2' AND HIM_PT_ID = '" + txtFileNo.Text + "'";
            }
            else
            {
                Criteria += " AND HPB_PT_ID = '" + txtFileNo.Text + "'";
            }

            IP_Invoice objInv = new IP_Invoice();
            DS = objInv.CalculatePTAmount(AmountType, Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("PTAmount") == false)
                {
                    decPTBalance = Convert.ToDecimal(DS.Tables[0].Rows[0]["PTAmount"]);
                }
            }

            return decPTBalance;
        }


        void SaveClaimDetail()
        {

            objCom = new CommonBAL();
            string Criteria = "1=1";

            Criteria = "HIC_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIC_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";
            objCom.fnDeleteTableData("HMS_INVOICE_CLAIMSDTLS", Criteria);

            IP_InvoiceClaims objInvClms = new IP_InvoiceClaims();
            objInvClms.HIC_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objInvClms.HIC_INVOICE_ID = txtInvoiceNo.Text.Trim();
            objInvClms.HIC_TYPE = drpClmType.SelectedValue;
            objInvClms.HIC_STARTDATE = txtClmStartDate.Text.Trim() + " " + txtClmFromTime.Text.Trim();
            objInvClms.HIC_ENDDATE = txtClmEndDate.Text.Trim() + " " + txtClmToTime.Text.Trim();
            objInvClms.HIC_STARTTYPE = drpClmStartType.SelectedValue;
            objInvClms.HIC_ENDTYPE = drpClmEndType.SelectedValue;

            DataTable DT = (DataTable)ViewState["InvoiceClaims"];


            //objrow["HIC_ICD_CODE"] = txtDiagCode.Text.Trim();
            //objrow["HIC_ICD_DESC"] = txtDiagName.Text.Trim();
            //objrow["HIC_ICD_TYPE"] = drpDiag

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                string strICDType = "", strICDCode = "";

                strICDCode = Convert.ToString(DT.Rows[i]["HIC_ICD_CODE"]);
                strICDType = Convert.ToString(DT.Rows[i]["HIC_ICD_TYPE"]);

                if (strICDType == "Principal")
                {
                    objInvClms.HIC_ICD_CODE_P = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Admitting")
                {
                    objInvClms.HIC_ICD_CODE_A = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S1 == null)
                {
                    objInvClms.HIC_ICD_CODE_S1 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S2 == null)
                {
                    objInvClms.HIC_ICD_CODE_S2 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S3 == null)
                {
                    objInvClms.HIC_ICD_CODE_S3 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S4 == null)
                {
                    objInvClms.HIC_ICD_CODE_S4 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S5 == null)
                {
                    objInvClms.HIC_ICD_CODE_S5 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S6 == null)
                {
                    objInvClms.HIC_ICD_CODE_S6 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S7 == null)
                {
                    objInvClms.HIC_ICD_CODE_S7 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S8 == null)
                {
                    objInvClms.HIC_ICD_CODE_S8 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S11 == null)
                {
                    objInvClms.HIC_ICD_CODE_S11 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S12 == null)
                {
                    objInvClms.HIC_ICD_CODE_S12 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S13 == null)
                {
                    objInvClms.HIC_ICD_CODE_S13 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S14 == null)
                {
                    objInvClms.HIC_ICD_CODE_S14 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S15 == null)
                {
                    objInvClms.HIC_ICD_CODE_S15 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S16 == null)
                {
                    objInvClms.HIC_ICD_CODE_S16 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S17 == null)
                {
                    objInvClms.HIC_ICD_CODE_S17 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S18 == null)
                {
                    objInvClms.HIC_ICD_CODE_S18 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S19 == null)
                {
                    objInvClms.HIC_ICD_CODE_S19 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S20 == null)
                {
                    objInvClms.HIC_ICD_CODE_S20 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S21 == null)
                {
                    objInvClms.HIC_ICD_CODE_S21 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S22 == null)
                {
                    objInvClms.HIC_ICD_CODE_S22 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S23 == null)
                {
                    objInvClms.HIC_ICD_CODE_S23 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S24 == null)
                {
                    objInvClms.HIC_ICD_CODE_S24 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S25 == null)
                {
                    objInvClms.HIC_ICD_CODE_S25 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S26 == null)
                {
                    objInvClms.HIC_ICD_CODE_S26 = strICDCode;
                    goto EndFor;
                }

                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S27 == null)
                {
                    objInvClms.HIC_ICD_CODE_S27 = strICDCode;
                    goto EndFor;
                }
                if (strICDType == "Secondary" && objInvClms.HIC_ICD_CODE_S28 == null)
                {
                    objInvClms.HIC_ICD_CODE_S28 = strICDCode;
                    goto EndFor;
                }

            EndFor: ;
            }

            if (DT.Rows.Count > 0)
            {
                objInvClms.InvoiceClaimDtlsAdd();
            }

            if (ChkTopupCard.Checked == true && Convert.ToBoolean(ViewState["NewFlag"]) == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
            {

                if (DT.Rows.Count > 0)
                {
                    Criteria = "HIC_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIC_INVOICE_ID='T" + txtInvoiceNo.Text.Trim() + "'";
                    objCom.fnDeleteTableData("HMS_INVOICE_CLAIMSDTLS", Criteria);

                    objInvClms.HIC_INVOICE_ID = "T" + txtInvoiceNo.Text.Trim();
                    objInvClms.InvoiceClaimDtlsAdd();
                }
            }

        }

        void fnSaveReceipt(decimal PrevPTCrPaid)
        {

            if (hidReceipt.Value == "" || hidReceipt.Value == null)
            {
                goto SaveReceiptEnd;
            }

            IP_Receipt objRec = new IP_Receipt();
            objRec.HRM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objRec.HRM_RECEIPTNO = hidReceipt.Value;
            objRec.HRM_DATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
            objRec.HRM_PT_ID = txtFileNo.Text.Trim();
            objRec.HRM_PT_NAME = txtName.Text.Trim();
            objRec.HRM_PT_TYPE = drpInvType.Text.Trim();
            objRec.HRM_COMP_ID = txtSubCompanyID.Text.Trim();
            objRec.HRM_COMP_NAME = txtCompanyName.Text.Trim();
            objRec.HRM_COMP_TYPE = hidInsType.Value;
            objRec.HRM_DR_ID = txtDoctorID.Text.Trim();
            objRec.HRM_DR_NAME = txtDoctorName.Text.Trim();
            objRec.HRM_REF_ID = txtRefDrId.Text.Trim();
            objRec.HRM_REF_NAME = txtRefDrName.Text.Trim();
            objRec.HRM_AMT_RCVD = Convert.ToString(Convert.ToDecimal(txtPaidAmount.Text) - PrevPTCrPaid);
            objRec.HRM_PAYMENT_TYPE = drpPayType.SelectedValue;
            objRec.HRM_CCNO = txtCCNo.Text.Trim();
            objRec.HRM_CCNAME = drpccType.SelectedValue;
            objRec.HRM_HOLDERNAME = txtCCHolder.Text.Trim();
            objRec.HRM_CCREFNO = txtCCRefNo.Text.Trim();
            objRec.HRM_CHEQUENO = txtCCNo.Text.Trim();
            objRec.HRM_BANKNAME = drpBank.SelectedValue;
            objRec.HRM_CHEQUEDATE = txtDcheqdate.Text.Trim();
            objRec.HRM_CASH_AMT = txtCashAmt.Text.Trim();
            objRec.HRM_CC_AMT = txtCCAmt.Text.Trim();
            objRec.HRM_CHEQUE_AMT = txtChqAmt.Text.Trim();
            objRec.HRM_CUR_TYP = drpCurType.SelectedValue;
            objRec.HRM_CUR_RATE = txtConvRate.Text.Trim();
            objRec.HRM_CUR_VALUE = txtcurValue.Text.Trim();
            objRec.HRM_CUR_RCVD_AMT = txtRcvdAmount.Text.Trim();

            if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
            {
                objRec.NewFlag = "A";
            }
            else
            {
                objRec.NewFlag = "M";
            }


            objRec.UserID = Convert.ToString(Session["User_ID"]);

            string srtReceptNo = "";
            srtReceptNo = objRec.ReceiptMasterAdd();
            hidReceipt.Value = srtReceptNo;

            objRec.HRT_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objRec.HRT_RECEIPTNO = hidReceipt.Value;
            objRec.HRT_DATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
            objRec.HRT_INVOICE_ID = txtInvoiceNo.Text.Trim();
            objRec.HRT_NET_AMOUNT = txtBillAmt4.Text.Trim();
            objRec.HRT_PAID_AMOUNT = txtPaidAmount.Text.Trim();
            objRec.HRT_DISCOUNT = txtSplDisc.Text.Trim();

            objRec.ReceiptTransAdd();

            if (ChkTopupCard.Checked == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
            {
                string srtReceptNo1 = "";
                objRec.HRM_RECEIPTNO = hidReceipt.Value;
                srtReceptNo1 = objRec.ReceiptMasterAdd();

                objRec.HRT_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objRec.HRT_RECEIPTNO = srtReceptNo1;
                objRec.HRT_DATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
                objRec.HRT_INVOICE_ID = "T" + txtInvoiceNo.Text.Trim();
                objRec.HRT_NET_AMOUNT = txtBillAmt4.Text.Trim();
                objRec.HRT_PAID_AMOUNT = txtPaidAmount.Text.Trim();
                objRec.HRT_DISCOUNT = txtSplDisc.Text.Trim();

                objRec.ReceiptTransAdd();
            }

        SaveReceiptEnd: ;

        }

        void PatientDeductibleBind()
        {
            objCom = new CommonBAL();

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = objCom.PatientMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (HMS_COINS_PTREG == false)
                {
                    ViewState["Deductible_Type"] = DS.Tables[0].Rows[0].IsNull("HPM_DED_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_DED_TYPE"]);// IIf(IsNull(.Fields("HPM_DED_TYPE").Value), "%", .Fields("HPM_DED_TYPE").Value)
                    ViewState["Deductible"] = DS.Tables[0].Rows[0].IsNull("HPM_DED_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_DED_AMT"]);//IIf(IsNull(.Fields("HPM_DED_AMT").Value), 0, .Fields("HPM_DED_AMT").Value)
                    ViewState["Co_Ins_Type"] = DS.Tables[0].Rows[0].IsNull("HPM_COINS_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_COINS_TYPE"]);//IIf(IsNull(.Fields("HPM_COINS_TYPE").Value), "%", .Fields("HPM_COINS_TYPE").Value)
                    ViewState["Co_Ins_Amt"] = DS.Tables[0].Rows[0].IsNull("HPM_COINS_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_COINS_AMT"]);//IIf(IsNull(.Fields("HPM_COINS_AMT").Value), 0, .Fields("HPM_COINS_AMT").Value)
                }

            }

        EndSub: ;
        }

        void PatientDataBind()
        {
            objCom = new CommonBAL();

            DateTime Today = System.DateTime.Now;

            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DS = objCom.PatientMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                if (GlobalValues.FileDescription != "SMCH") fnLastConsltDate();

                // drpInvType.SelectedValue = DS.Tables[0].Rows[0]["HPM_PT_TYPE"].ToString();

                /*
                string msg1, msg2;
                msg1 = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_TYPE"]) == "CA" ? "Cash" : "Credit";
                msg2 = drpInvType.SelectedValue == "Cash" ? "Cash" : "Credit";

                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    if (drpInvType.SelectedValue != msg1)
                    {


                        // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('This patient is registered as " + msg1 + " Do you want Invoice this Patient as" + msg2 + "');", true);
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "CheckInvType('" + msg1 + "','" + msg2 + "');", true);

                    }
                }
                8*/

                drpInvType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_TYPE"]) == "CA" ? "Cash" : "Credit";

                // txtTokenNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_TOKEN_NO"]);
                txtFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_ID"]);
                txtName.Text = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]);
                txtPTComp.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_PT_COMP_NAME"]);
                if (Convert.ToString(ViewState["HSOM_DR_MANUALSEELCT_INVOICE"]) == "0")
                {
                    txtDoctorID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_DR_ID"]);
                    txtDoctorName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_DR_NAME"]);

                    if (Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_ID"]) != "" && DS.Tables[0].Rows[0].IsNull("HPM_REF_DR_ID") == false)
                    {
                        txtOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_ID"]);
                        txtOrdClinName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_NAME"]);
                    }
                    else
                    {
                        txtOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_DR_ID"]);
                        txtOrdClinName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_DR_NAME"]);
                    }

                    txtRefDrName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_NAME"]);
                    txtRefDrId.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_REF_DR_ID"]);





                }

                if (drpInvType.SelectedValue == "Credit")
                    TxtVoucherNo.Text = fnGetClaimFormNo();
                if (drpTreatmentType.Items.Count > 0) drpTreatmentType.SelectedValue = "IP";
                if (hidPTVisitType.Value == "")
                {
                    DateTime dtLastVisit;
                    dtLastVisit = Convert.ToDateTime(DS.Tables[0].Rows[0]["HPM_PREV_VISIT"]);


                    TimeSpan Diff = Today.Date - dtLastVisit;

                    if (Diff.Days <= Convert.ToInt32(Session["RevisitDays"]))
                    {
                        hidPTVisitType.Value = "Revisit";
                    }
                    else if (Diff.Days >= Convert.ToInt32(Session["RevisitDays"]))
                    {
                        hidPTVisitType.Value = "Old";
                    }
                }

                Decimal CreditBalAmt = 0;

                CreditBalAmt = (fnCalculatePTAmount("INVOICE") - fnCalculatePTAmount("RETURN")) - (fnCalculatePTAmount("PAIDAMT") + fnCalculatePTAmount("RETURNPAIDAMT") + fnCalculatePTAmount("REJECTED")) - fnCalculatePTAmount("NPPAID");
                if (CreditBalAmt > 0)
                {
                    lblBalanceMsg.Text = "Balance Amount Due : " + Convert.ToString(CreditBalAmt);  //HMS_CURRENCY_FORMAT)
                }
                else
                {
                    lblBalanceMsg.Text = "";
                }




                //  ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "Confirm();", true);
                //string confirmValue = Request.Form["confirm_value"];
                //if (confirmValue == "Yes")
                //{
                //   // this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked YES!')", true);
                //    goto EndSub;
                //}
                //else
                //{
                //    //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('You clicked NO!')", true);
                //}



                //strMessage = "Patient Policy is expired. So you cannot claim to this invoice. Do you want to invoice.";

                //if (strMessage != "")
                //{
                //    MPExtMessage.Show();
                //}
                if (drpInvType.SelectedValue == "Credit")
                {
                    if (DS.Tables[0].Rows[0].IsNull("HPM_CONT_EXPDATE") == false && DS.Tables[0].Rows[0].IsNull("HPM_INS_COMP_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_ID"]) != "")
                    {
                        DateTime dtPolicyExp;
                        dtPolicyExp = Convert.ToDateTime(DS.Tables[0].Rows[0]["HPM_CONT_EXPDATE"]);


                        TimeSpan Diff1 = dtPolicyExp.Date - Today.Date;

                        if (Diff1.Days < 1)
                        {
                            //  this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Patient Policy is expired. So you cannot claim to this invoice. Do you want to invoice?')", true);
                            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Patient Policy is expired. So you cannot claim to this invoice. Do you want to invoice?')", true);

                            goto EndSub;
                        }

                        hidInsType.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_TYPE"]);
                        txtSubCompanyID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                        txtSubCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                        hidCompanyID.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPM_BILL_CODE"]);
                        hidCmpRefCode.Value = txtSubCompanyID.Text.Trim();
                        txtPlanType.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);



                        txtIdNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_ID_NO"]);
                        txtPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                        txtPolicyExp.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_CONT_EXPDATEDesc"]);
                        //  
                        //  TxtVoucherNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_VOUCHER_NO"]);



                        //  

                        // GetPatientVisit();

                        objCom = new CommonBAL();

                        if (hidCompanyID.Value.Trim() == txtSubCompanyID.Text.Trim())
                        {
                            txtCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                            txtSubCompanyName.Text = "";
                        }
                        else
                        {

                            objCom = new CommonBAL();
                            DataSet DSComp = new DataSet();
                            Criteria = " 1=1 ";
                            Criteria += "AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HCM_BILL_CODE='" + hidCompanyID.Value + "' AND HCM_COMP_ID='" + hidCompanyID.Value + "'";
                            DSComp = objCom.GetCompanyMaster(Criteria);
                            if (DSComp.Tables[0].Rows.Count > 0)
                            {
                                txtCompanyName.Text = Convert.ToString(DSComp.Tables[0].Rows[0]["HCM_NAME"]);
                                hidCompAgrmtType.Value = Convert.ToString(DSComp.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]);
                            }

                        }

                        DataSet DSComp1 = new DataSet();
                        Criteria = " 1=1 ";
                        Criteria += " AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HCM_COMP_ID='" + txtSubCompanyID.Text + "'";
                        DSComp1 = objCom.GetCompanyMaster(Criteria);
                        if (DSComp1.Tables[0].Rows.Count > 0)
                        {
                            hidUpdateCodeType.Value = Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_UPDATECODETYPE"]);
                            hidCompAgrmtType.Value = Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_AGREEMENT_TYPE"]);

                            fnSetServices(Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_UPDATECODETYPE"]), txtSubCompanyID.Text.Trim(), txtPlanType.Text.Trim());

                            if (Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_REM_RCPTN"]) != "" && DSComp1.Tables[0].Rows[0].IsNull("HCM_REM_RCPTN") == false)// App.FileDescription = "RASHIDIYA" Then
                            {
                                MPExtMessage.Show();
                                strMessage = Convert.ToString(DSComp1.Tables[0].Rows[0]["HCM_REM_RCPTN"]);//  temprec1.Fields("HCM_REM_RCPTN").Value;
                            }
                        }


                    }
                }
                if (HMS_COINS_PTREG == false)
                {
                    ViewState["Deductible_Type"] = DS.Tables[0].Rows[0].IsNull("HPM_DED_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_DED_TYPE"]);// IIf(IsNull(.Fields("HPM_DED_TYPE").Value), "%", .Fields("HPM_DED_TYPE").Value)
                    ViewState["Deductible"] = DS.Tables[0].Rows[0].IsNull("HPM_DED_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_DED_AMT"]);//IIf(IsNull(.Fields("HPM_DED_AMT").Value), 0, .Fields("HPM_DED_AMT").Value)
                    ViewState["Co_Ins_Type"] = DS.Tables[0].Rows[0].IsNull("HPM_COINS_TYPE") == true ? "%" : Convert.ToString(DS.Tables[0].Rows[0]["HPM_COINS_TYPE"]);//IIf(IsNull(.Fields("HPM_COINS_TYPE").Value), "%", .Fields("HPM_COINS_TYPE").Value)
                    ViewState["Co_Ins_Amt"] = DS.Tables[0].Rows[0].IsNull("HPM_COINS_AMT") == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0]["HPM_COINS_AMT"]);//IIf(IsNull(.Fields("HPM_COINS_AMT").Value), 0, .Fields("HPM_COINS_AMT").Value)
                }

                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    //blnMultipleReg = PatientVisitBind();

                    blnMultipleReg = AdmissionSummaryBind();

                }
                //      If CmbInvoiceType.Text = "Cash" Then
                //    fnFillCmpServInListBox
                //Else
                //    If Check_CmpService(TxtInsCode.Text) And mMinistryService Then
                //        fnFillCmpServInListBox "MINISTRY", mRefCode
                //    Else
                //        fnFillCmpServInListBox
                //    End If
                //End If

                if (txtRefDrId.Text.Trim() != "" && GlobalValues.FileDescription == "ALREEM")
                {
                    BindCommissionType();
                }
            }
            else
            {
                //txtName.Text = "";
                //txtInsType.Text = "";
                //TxtInsCode.Text = "";
                //TXTSubInsCode.Text = "";
                //txtinsName.Text = "";
                //txtSubins.Text = "";
                //txtptComp.Text = "";
                //TxtIDNo.Text = "";
                //TxtPolicyNo.Text = "";
                //TxtIDNo.Text = "";
                //txtpolicyExp.Text = "";
                //txtInsTrtnt.Text = "";
                //txtDrCode.Text = "";
                //TxtDRName.Text = "";
                //TxtRefDr.Text = "";
                //TxtRefID.Text = "";
                //CmbTrtntType.ListIndex = -1;
            }

           // GetCompanyAgrDtlsAll();
        EndSub: ;
        }

        Boolean IsPatientVisitAvailable()
        {
            string Criteria = " 1=1 ";

            string strStartDate = txtInvDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtInvDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) >= '" + strForStartDate + "'";
            }


            if (txtFileNo.Text.Trim() != "")
            {
                Criteria += " AND HPV_PT_ID   = '" + txtFileNo.Text.Trim() + "' ";
            }

            objCom = new CommonBAL();
            DataSet DS = new DataSet();
            DS = objCom.Patient_Master_VisitGet(Criteria);

            if (DS.Tables[0].Rows.Count > 1)
            {

                return true;
            }

            return false;


        }

        /*
        Boolean PatientVisitBind()
        {
            string Criteria = " 1=1 ";

            string strStartDate = txtInvDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtInvDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),HPV_DATE,101),101) = '" + strForStartDate + "'";
            }


            if (txtFileNo.Text.Trim() != "")
            {
                Criteria += " AND HPV_PT_ID   = '" + txtFileNo.Text.Trim() + "' ";
            }

            Criteria += " AND   HPV_DR_ID='" + Convert.ToString(Session["HPV_DR_ID"]) + "'";

            hidVisitID.Value = "";
            ViewState["IAS_EMR_ID"] = "";
            ModPopExtVisits.Hide();

            objCom = new CommonBAL();
            DataSet DS = new DataSet();
            DS = objCom.Patient_Master_VisitGet(Criteria);

            if (DS.Tables[0].Rows.Count > 1)
            {
                gvVisit.DataSource = DS;
                gvVisit.DataBind();
                ModPopExtVisits.Show();
                return true;
            }
            if (DS.Tables[0].Rows.Count == 1)
            {
                hidVisitID.Value = Convert.ToString(DS.Tables[0].Rows[0]["HPV_SEQNO"]);
                txtDoctorID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_ID"]);
                txtDoctorName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_NAME"]);

                if (Convert.ToString(DS.Tables[0].Rows[0]["HPV_REFERRAL_TO"]) != "" && DS.Tables[0].Rows[0].IsNull("HPV_REFERRAL_TO") == false)
                {
                    txtRefDrId.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_REFERRAL_TO"]);
                    txtOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_REFERRAL_TO"]);
                }
                else
                {
                    txtRefDrId.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_ID"]);
                    txtRefDrName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_NAME"]);

                    txtOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_ID"]);
                    txtOrdClinName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_DR_NAME"]);
                }

                ViewState["HPV_EMR_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["HPV_EMR_ID"]);

                if (IsPatientBilledSameDaySameDr(false) == false)
                {
                    BindSalesOrderServ();
                    BindSalesOrderDiag();
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["HPV_REFERRAL_TO"]) != "" && DS.Tables[0].Rows[0].IsNull("HPV_REFERRAL_TO") == false)
                {
                    txtRefDrId_TextChanged(txtRefDrId, new EventArgs());
                }

                ViewState["HPV_EMR_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["HPV_EMR_ID"]);

                //BindSalesOrderServ();
                //BindSalesOrderDiag();
                //txtRefDrId_TextChanged(txtRefDrId, new EventArgs());
                // txtOrdClinCode_TextChanged(txtOrdClinCode, new EventArgs());
                return true;
            }
            return false;


        }
        */


        Boolean AdmissionSummaryBind()
        {
            string Criteria = " 1=1 ";

            string strStartDate = txtInvDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtInvDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),IAS_DATE,101),101) = '" + strForStartDate + "'";
            }


            if (txtFileNo.Text.Trim() != "")
            {
                Criteria += " AND IAS_PT_ID   = '" + txtFileNo.Text.Trim() + "' ";
            }

            Criteria += " AND   IAS_DR_ID='" + Convert.ToString(Session["IAS_DR_ID"]) + "'";

            hidVisitID.Value = "";
            ViewState["IAS_EMR_ID"] = "";
            // ModPopExtVisits.Hide();

            objAdmSumm = new IP_AdmissionSummary();
            DataSet DS = new DataSet();
            DS = objAdmSumm.GetIPAdmissionSummary(Criteria);

            //if (DS.Tables[0].Rows.Count > 1)
            //{
            //    gvVisit.DataSource = DS;
            //    gvVisit.DataBind();
            //    ModPopExtVisits.Show();
            //    return true;
            //}
            if (DS.Tables[0].Rows.Count == 1)
            {
                //hidVisitID.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_SEQNO"]);
                txtDoctorID.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_ID"]);
                txtDoctorName.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_NAME"]);

                if (Convert.ToString(DS.Tables[0].Rows[0]["IAS_REFERRAL_PHYSICIAN"]) != "" && DS.Tables[0].Rows[0].IsNull("IAS_REFERRAL_PHYSICIAN") == false)
                {
                    txtRefDrId.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REFERRAL_PHYSICIAN"]);
                    txtOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_ID"]);
                    txtOrdClinName.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_NAME"]);
                }
                else
                {
                    txtRefDrId.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_ID"]);
                    txtRefDrName.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_NAME"]);

                    txtOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_ID"]);
                    txtOrdClinName.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_NAME"]);
                }

                ViewState["IAS_EMR_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["IAS_IP_ID"]);//IAS_EMR_ID

                //if (IsPatientBilledSameDaySameDr(false) == false)
                //{
                //    BindSalesOrderServ();
                //    BindSalesOrderDiag();
                //}
                if (Convert.ToString(DS.Tables[0].Rows[0]["IAS_REFERRAL_PHYSICIAN"]) != "" && DS.Tables[0].Rows[0].IsNull("IAS_REFERRAL_PHYSICIAN") == false)
                {
                    txtRefDrId_TextChanged(txtRefDrId, new EventArgs());
                }

              //  ViewState["IAS_EMR_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["IAS_EMR_ID"]);


                txtPriorAuthorizationID.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_AUTHORIZATION_ID"]);

                ViewState["IAS_DRG_CODE"] = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DRG_CODE"]);

               

                //BindSalesOrderServ();
                //BindSalesOrderDiag();
                //txtRefDrId_TextChanged(txtRefDrId, new EventArgs());
                // txtOrdClinCode_TextChanged(txtOrdClinCode, new EventArgs());
                return true;
            }
            return false;


        }

        Boolean BindInvoice()
        {
            string Criteria = " 1=1  ";

            // Criteria += " AND ( HIM_INVOICE_ID ='" + txtInvoiceNo.Text.Trim() + "' OR HIM_EMR_ID='" + Convert.ToString(Session["EMR_ID"]) + "')";
            Criteria += " AND  HIM_INVOICE_ID ='" + txtInvoiceNo.Text.Trim() + "' AND HIM_PT_ID='" + txtFileNo.Text + "' ";


            DataSet DS = new DataSet();
            IP_Invoice objInv = new IP_Invoice();
            DS = objInv.InvoiceMasterGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["NewFlag"] = false;

                drpInvType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INVOICE_TYPE"]);

                txtInvoiceNo.Text = DS.Tables[0].Rows[0]["HIM_INVOICE_ID"].ToString();
                txtTokenNo.Text = DS.Tables[0].Rows[0]["HIM_TOKEN_NO"].ToString();
                txtFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_ID"]);
                txtName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_NAME"]);

                if (DS.Tables[0].Rows[0].IsNull("HIM_STATUS") == false)
                    drpStatus.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_STATUS"]);

                txtInvDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_DATEDesc"]);

                DateTime dtInvoice = Convert.ToDateTime(DS.Tables[0].Rows[0]["HIM_DATE"]);
                txtInvTime.Text = dtInvoice.ToString("hh:mm:ss");



                if (txtFileNo.Text.Trim() != "OPDCR" && txtFileNo.Text.Trim() != "OPD")
                {
                    PatientDataBind();
                    if (txtFileNo.Text.Trim() == "ADAB") txtName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_NAME"]);
                }


                hidCompanyID.Value = Convert.ToString(DS.Tables[0].Rows[0]["HIM_BILLTOCODE"]);
                txtJobnumber.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_BUSINESSUNITCODE"]);
                txtJobnumber.Text = Convert.ToString(DS.Tables[0].Rows[0]["HPV_JOBNUMBER"]);

                txtSubCompanyID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_SUB_INS_CODE"]);
                txtCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INS_NAME"]);
                txtSubCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_SUB_INS_NAME"]);
                txtPlanType.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_POLICY_TYPE"]);



                txtPTComp.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_COMP"]);
                txtPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_POLICY_NO"]);
                txtIdNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ID_NO"]);
                txtPolicyExp.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_POLICY_EXPDesc"]);
                hidInsTrtnt.Value = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INS_TRTNT_TYPE"]);




                TxtVoucherNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_VOUCHER_NO"]);

                txtDoctorID.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_DR_CODE"]);
                txtDoctorName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_DR_NAME"]);


                txtOrdClinCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ORDERING_DR_CODE"]);
                txtOrdClinName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ORDERING_DR_NAME"]);

                drpTreatmentType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_TRTNT_TYPE"]);

                txtRefDrId.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_REF_DR_CODE"]);
                txtRefDrName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_REF_DR_NAME"]);
                txtRemarks.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_REMARKS"]);

                if (drpRemarks.Items.Count > 0)
                {
                    drpRemarks.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_REMARKS"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("HIM_CLAIM_AMOUNT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_CLAIM_AMOUNT"]) != "")
                {
                    decimal decClaimAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_CLAIM_AMOUNT"]);
                    txtClaimAmt.Text = decClaimAmt.ToString("#0.00");
                }


                if (DS.Tables[0].Rows[0].IsNull("HIM_GROSS_TOTAL") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_GROSS_TOTAL"]) != "")
                {
                    decimal decBillAmt1 = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_GROSS_TOTAL"]);
                    txtBillAmt1.Text = decBillAmt1.ToString("#0.00");
                }

                if (DS.Tables[0].Rows[0].IsNull("HIM_HOSP_DISC") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_HOSP_DISC"]) != "")
                {
                    decimal decHospDisc = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_HOSP_DISC"]);
                    txtHospDisc.Text = decHospDisc.ToString("#0.00");
                }


                if (txtBillAmt1.Text.Trim() != "" && txtHospDisc.Text != "")
                {
                    decimal decBillAmt2 = Convert.ToDecimal(txtBillAmt1.Text.Trim()) - Convert.ToDecimal(txtHospDisc.Text);
                    txtBillAmt2.Text = decBillAmt2.ToString("#0.00");
                }




                if (DS.Tables[0].Rows[0].IsNull("HIM_NET_AMOUNT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_NET_AMOUNT"]) != "")
                {
                    decimal decBillAmt3 = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_NET_AMOUNT"]);
                    txtBillAmt3.Text = decBillAmt3.ToString("#0.00");
                }




                if (DS.Tables[0].Rows[0].IsNull("HIM_PT_AMOUNT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_AMOUNT"]) != "")
                {
                    decimal decPTAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_PT_AMOUNT"]);
                    txtBillAmt4.Text = decPTAmt.ToString("#0.00");
                }



                drpHospDiscType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_HOSP_DISC_TYPE"]);


                if (DS.Tables[0].Rows[0].IsNull("HIM_HOSP_DISC_AMT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_HOSP_DISC_AMT"]) != "")
                {
                    decimal decHospDiscAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_HOSP_DISC_AMT"]);
                    txtHospDiscAmt.Text = decHospDiscAmt.ToString("#0.00");
                }


                if (DS.Tables[0].Rows[0].IsNull("HIM_DEDUCTIBLE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_DEDUCTIBLE"]) != "")
                {
                    decimal decDect = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_DEDUCTIBLE"]);
                    txtDeductible.Text = decDect.ToString("#0.00");
                }


                if (DS.Tables[0].Rows[0].IsNull("HIM_CO_INS_AMOUNT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_CO_INS_AMOUNT"]) != "")
                {
                    decimal decCoInsAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_CO_INS_AMOUNT"]);
                    txtCoInsTotal.Text = decCoInsAmt.ToString("#0.00");
                }

                hidCoInsType.Value = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CO_INS_TYPE"]);

                if (DS.Tables[0].Rows[0].IsNull("HIM_SPL_DISC") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_SPL_DISC"]) != "")
                {
                    decimal decSplDisc = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_SPL_DISC"]);
                    txtSplDisc.Text = decSplDisc.ToString("#0.00");
                }

                if (DS.Tables[0].Rows[0].IsNull("HIM_PT_CREDIT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_PT_CREDIT"]) != "")
                {
                    decimal decPTCredit = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_PT_CREDIT"]);
                    txtPTCredit.Text = decPTCredit.ToString("#0.00");
                }

                if (DS.Tables[0].Rows[0].IsNull("HIM_PAID_AMOUNT") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_PAID_AMOUNT"]) != "")
                {
                    decimal decPaidAmt = Convert.ToDecimal(DS.Tables[0].Rows[0]["HIM_PAID_AMOUNT"]);
                    txtPaidAmount.Text = decPaidAmt.ToString("#0.00");
                    hidPaidAmtCheck.Value = decPaidAmt.ToString("#0.00");
                }

                if (DS.Tables[0].Rows[0].IsNull("HIM_PAYMENT_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_PAYMENT_TYPE"]) != "")
                {
                    for (int intCount = 0; intCount < drpPayType.Items.Count; intCount++)
                    {
                        if (drpPayType.Items[intCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HIM_PAYMENT_TYPE"]))
                        {
                            drpPayType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_PAYMENT_TYPE"]);


                            goto fordrpPayType;
                        }

                    }
                }
            fordrpPayType: ;

                if (drpPayType.SelectedValue == "Credit Card")
                {
                    divCC.Visible = true;

                }
                if (drpPayType.SelectedValue == "Cheque")
                {
                    divCheque.Visible = true;

                }
                if (drpPayType.SelectedValue == "Advance")
                {
                    divAdvane.Visible = true;

                }

                if (DS.Tables[0].Rows[0].IsNull("HIM_CUR_TYP") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_CUR_TYP"]) != "")
                {
                    for (int intCount = 0; intCount < drpCurType.Items.Count; intCount++)
                    {
                        if (drpCurType.Items[intCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HIM_CUR_TYP"]))
                        {
                            drpCurType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CUR_TYP"]);
                            goto forCurType;
                        }

                    }
                }
            forCurType: ;

                txtConvRate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CUR_RATE"]);
                txtcurValue.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CUR_VALUE"]);

                txtRcvdAmount.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CUR_RCVD_AMT"]);
                hidReceipt.Value = Convert.ToString(DS.Tables[0].Rows[0]["HIM_RECEIPT_NO"]);
                hidType.Value = Convert.ToString(DS.Tables[0].Rows[0]["HIM_TYPE"]);


                if (DS.Tables[0].Rows[0].IsNull("HIM_COMMISSION_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_COMMISSION_TYPE"]) != "")
                {
                    for (int intCount = 0; intCount < drpCommissionType.Items.Count; intCount++)
                    {
                        if (drpCommissionType.Items[intCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HIM_COMMISSION_TYPE"]))
                        {
                            drpCommissionType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_COMMISSION_TYPE"]);
                            goto forCommissionType;
                        }

                    }
                }
            forCommissionType: ;

                txtAdvAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_ADV_AMT"]);

                hidccText.Value = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CC"]);

                if (DS.Tables[0].Rows[0].IsNull("HIM_TRTNT_TYPE_01") == false)
                    txtICDDesc.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_TRTNT_TYPE_01"]);

                if (DS.Tables[0].Rows[0].IsNull("HIM_TRTNT_Type_code") == false)
                    txtICdCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_TRTNT_Type_code"]);

                chkReadyforEclaim.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HIM_READYFORECLAIM"]);

                if (DS.Tables[0].Rows[0].IsNull("HIM_TOPUP") == false)
                    ChkTopupCard.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["HIM_TOPUP"]);
                TxtTopupAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_TOPUP_AMT"]);



                if (DS.Tables[0].Rows[0].IsNull("HIM_CC_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_CC_TYPE"]) != "")
                {
                    for (int intCount = 0; intCount < drpccType.Items.Count; intCount++)
                    {
                        if (drpccType.Items[intCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HIM_CC_TYPE"]))
                        {
                            drpccType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CC_TYPE"]);
                            goto forccType;
                        }

                    }
                }
            forccType: ;

                txtCCHolder.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CC_NAME"]);
                txtCCNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CC_NO"]);
                txtCCRefNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_REF_NO"]);

                txtDcheqno.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CHEQ_NO"]);
                txtCashAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CASH_AMT"]);
                txtCCAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CC_AMT"]);
                txtChqAmt.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CHEQUE_AMT"]);


                if (DS.Tables[0].Rows[0].IsNull("HIM_BANK_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HIM_BANK_NAME"]) != "")
                {
                    for (int intCount = 0; intCount < drpBank.Items.Count; intCount++)
                    {
                        if (drpBank.Items[intCount].Value == Convert.ToString(DS.Tables[0].Rows[0]["HIM_BANK_NAME"]))
                        {
                            drpBank.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["HIM_BANK_NAME"]);
                            goto forBank;
                        }

                    }
                }
            forBank: ;



                txtDcheqdate.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CHEQ_DATE"]);
                txtTokenNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_INVOICE_ORDER"]);


                lblModifiedUser.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_CREATED_USER"]);
                lblCreatedUser.Text = Convert.ToString(DS.Tables[0].Rows[0]["HIM_MODIFIED_USER"]);

                PatientDeductibleBind();

                return true;
            }
            else
            {
                return false;
            }

        }

        void BuildeInvoiceTrans()
        {
            string Criteria = "1=2";
            objInv = new IP_Invoice();
            DataSet DS = new DataSet();

            DS = objInv.IPInvoiceTransGet(Criteria);

            ViewState["InvoiceTrans"] = DS.Tables[0];
        }

        void BindInvoiceTrans()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HIT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND HIT_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";

            DataSet DS = new DataSet();
            objInv = new IP_Invoice();
            DS = objInv.IPInvoiceTransGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {


                ViewState["InvoiceTrans"] = DS.Tables[0];

            }

        }


        void BindTempInvoiceTrans()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["InvoiceTrans"];
            if (DT.Rows.Count > 0)
            {
                gvInvoiceTrans.DataSource = DT;
                gvInvoiceTrans.DataBind();

                /*

                gvInvoiceTrans.Columns[10].Visible = false; //Co-Total
                gvInvoiceTrans.Columns[11].Visible = false;    //'Comp. Type
                gvInvoiceTrans.Columns[12].Visible = false;    //Comp. Amount
                gvInvoiceTrans.Columns[13].Visible = false; //Comp Total

                gvInvoiceTrans.Columns[16].Visible = false; //Serv. Category
                gvInvoiceTrans.Columns[17].Visible = false;    //'Serv. Treatment Type
                gvInvoiceTrans.Columns[18].Visible = false;    //Covered
                gvInvoiceTrans.Columns[19].Visible = false; //Serv. Type
                gvInvoiceTrans.Columns[20].Visible = false;    //'RefNo.
                gvInvoiceTrans.Columns[21].Visible = false;    //'TeethNo.

                gvInvoiceTrans.Columns[25].Visible = false;// 'CostPrice.

                if (Convert.ToString(ViewState["HSOM_COSTPRICE_BILLING"]).ToUpper() == "Y")
                {
                    gvInvoiceTrans.Columns[25].Visible = true; // 'CostPrice.
                }

                */

            }
            else
            {
                gvInvoiceTrans.DataBind();
            }

        }




        void BuildeInvoiceClaims()
        {

            DataSet ds = new DataSet();

            DataTable dt = new DataTable();

            DataColumn HIC_ICD_CODE = new DataColumn();
            HIC_ICD_CODE.ColumnName = "HIC_ICD_CODE";

            DataColumn HIC_ICD_DESC = new DataColumn();
            HIC_ICD_DESC.ColumnName = "HIC_ICD_DESC";


            DataColumn HIC_ICD_TYPE = new DataColumn();
            HIC_ICD_TYPE.ColumnName = "HIC_ICD_TYPE";


            dt.Columns.Add(HIC_ICD_CODE);
            dt.Columns.Add(HIC_ICD_DESC);
            dt.Columns.Add(HIC_ICD_TYPE);

            ViewState["InvoiceClaims"] = dt;
        }

        void BindInvoiceClaimDtls()
        {
            string Criteria = " 1=1 ";


            Criteria += " AND  HIC_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";

            IP_InvoiceClaims objInvCla = new IP_InvoiceClaims();
            DataSet DS = new DataSet();
            DS = objInvCla.InvoiceClaimDtlsGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DT = (DataTable)ViewState["InvoiceClaims"];

                    DataRow objrow;
                    if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_P") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_P"]) != "")
                    {
                        objrow = DT.NewRow();
                        //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                        //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                        //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                        //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                        //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);

                        objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_P"]);
                        objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_PDesc"]);

                        objrow["HIC_ICD_TYPE"] = "Principal";


                        DT.Rows.Add(objrow);
                    }

                    if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_A") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_A"]) != "")
                    {
                        objrow = DT.NewRow();
                        //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                        //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                        //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                        //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                        //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);


                        objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_A"]);
                        objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_ADesc"]);
                        objrow["HIC_ICD_TYPE"] = "Admitting";
                        DT.Rows.Add(objrow);

                    }

                    for (int j = 1; j <= 30; j++)
                    {
                        if (DS.Tables[0].Columns.Contains("HIC_ICD_CODE_S" + j) == true && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j]) != "")
                        {

                            if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_S" + j) == false)
                            {
                                objrow = DT.NewRow();
                                //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                                //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                                //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                                //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                                //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);

                                objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j]);
                                objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j + "Desc"]);
                                objrow["HIC_ICD_TYPE"] = "Secondary";
                                DT.Rows.Add(objrow);

                            }
                        }
                    }




                }

                ViewState["InvoiceClaims"] = DT;

            }
        }

        void BindTempInvoiceClaims()
        {
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["InvoiceClaims"];
            if (DT.Rows.Count > 0)
            {
                gvInvoiceClaims.DataSource = DT;
                gvInvoiceClaims.DataBind();

            }
            else
            {
                gvInvoiceClaims.DataBind();
            }

        }

        void BindSalesOrderServ()
        {

            DataTable DT = new DataTable();
            DT.Columns.Add("SO_ID");
            DT.Columns.Add("HAAD_CODE");

            ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindSalesOrderServ() Start");

            ProcessTimLog(System.DateTime.Now.ToString() + "      IPSalesOrderTransGet() Start");

            string Criteria = " 1=1  AND IDSM_ADMISSION_NO='" + Convert.ToString(Session["IAS_ADMISSION_NO"]) + "'";

            ////string Criteria = " 1=1  AND (IDST_USED_STATUS IS NULL OR  IDST_USED_STATUS=0 ) ";
            ////Criteria += " AND IDSM_PT_ID='" + txtFileNo.Text.Trim() + "'  AND  IDSM_DR_CODE='" + txtDoctorID.Text.Trim() + "'";


            //Criteria += " AND IDSM_PT_ID='" + txtFileNo.Text.Trim() + "'  AND IDSM_DATE=CONVERT(DATETIME,'" + txtInvDate.Text.Trim() + "',103) AND IDSM_DR_CODE='" + txtDoctorID.Text.Trim() + "'";
            //Criteria += " AND  DSM_DOCTOR_TRANS_ID ='" + Convert.ToString(ViewState["HPV_EMR_ID"]) + "'";
            IP_Invoice objInv = new IP_Invoice();

            DataSet DS = new DataSet();
            //DS = objInv.DrSalesOrderTransGet(Criteria);
            DS = objInv.IPSalesOrderTransGet(Criteria);
            ProcessTimLog(System.DateTime.Now.ToString() + "      IPSalesOrderTransGet() End");

            IDictionary<string, string> Param = new Dictionary<string, string>();


            string strHaadIds = "", strServIds = "";
            if (DS.Tables[0].Rows.Count > 0)
            {


                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {


                    string strHaadCode = Convert.ToString(DS.Tables[0].Rows[i]["IDST_SERV_CODE"]);
                    string strSOTransID = Convert.ToString(DS.Tables[0].Rows[i]["IDST_TRANS_ID"]);
                    string strServCode = "";

                    strServCode = GetServiceMasterID("HSM_HAAD_CODE", strHaadCode);

                    if (strHaadIds != "")
                    {
                        strHaadIds += ",'" + strHaadCode + "'";
                        strServIds += ",'" + strServCode + "'";

                    }
                    else
                    {
                        strHaadIds = "'" + strHaadCode + "'";
                        strServIds = "'" + strServCode + "'";
                    }


                    DT.Rows.Add(strSOTransID, strServCode);


                    ////  DataSet DSServ = new DataSet();

                    ////  DSServ = GetServiceMasterName("HSM_HAAD_CODE", strServCode);


                    //// if (DSServ.Tables[0].Rows.Count > 0)
                    //// {
                    ////  fnServiceAdd(Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_SERV_ID"]), Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_NAME"]));


                    //// objrow["HIT_SERV_CODE"] = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_SERV_ID"]);
                    //// objrow["HIT_Description"] = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_NAME"]);
                    //HSM_TYPE = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TYPE"]);
                    //CatID = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CAT_ID"]);
                    //CatTyp = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPE"]);
                    //if (DSServ.Tables[0].Rows[0].IsNull("HSM_CODE_TYPE") == false && Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPE"]) != "")
                    //// objrow["HIT_FEE"] = Convert.ToDecimal(DSServ.Tables[0].Rows[0]["HSM_FEE"]);

                    //  obsCode = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_CODE"]);
                    // obsDesc = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CAT_ID"]);
                    // obsvalueType = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_VALUETYPE"]);

                    ////  }



                    // objrow["HIT_DISC_TYPE"] = CatTyp;

                    /*
                    if (Convert.ToString(DS.Tables[0].Rows[i]["DST_DISC_AMT"]) != "")
                        objrow["HIT_DISC_AMT"] = Convert.ToString(DS.Tables[0].Rows[i]["DST_DISC_AMT"]);
                    else
                        objrow["HIT_DISC_AMT"] = 0;
                    */


                    //// if (Convert.ToString(DS.Tables[0].Rows[i]["DST_QTY"]) != "")
                    ////      objrow["HIT_QTY"] = Convert.ToString(DS.Tables[0].Rows[i]["DST_QTY"]);
                    ////  else
                    ////      objrow["HIT_QTY"] = 1;
                    /*

                  if (Convert.ToString(DS.Tables[0].Rows[i]["DST_AMOUNT"]) != "")
                      objrow["HIT_AMOUNT"] = Convert.ToString(DS.Tables[0].Rows[i]["DST_AMOUNT"]);
                  else
                      objrow["HIT_AMOUNT"] = 0;


                  if (Convert.ToString(DS.Tables[0].Rows[i]["DST_DEDUCT"]) != "")
                      objrow["HIT_DEDUCT"] = Convert.ToString(DS.Tables[0].Rows[i]["DST_DEDUCT"]);
                  else
                      objrow["HIT_DEDUCT"] = 0;



                  objrow["HIT_CO_INS_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["DST_CO_INS_TYPE"]);

                  if (Convert.ToString(DS.Tables[0].Rows[i]["DST_CO_INS_AMT"]) != "")
                      objrow["HIT_CO_INS_AMT"] = Convert.ToString(DS.Tables[0].Rows[i]["DST_CO_INS_AMT"]);
                  else
                      objrow["HIT_CO_INS_AMT"] = 0;
                  */


                    //  txtHitAmount.Text = Convert.ToString(decFee * Convert.ToDecimal(txtgvQty.Text.Trim()));

                    // AmountCalculation();



                    //// DT.Rows.Add(objrow);

                }


                ProcessTimLog(System.DateTime.Now.ToString() + "      fnServiceAddFromSO() Start");
                ///  fnServiceAddFromSO(strServIds);
                fnServiceAddFromSO1(DT);

                ProcessTimLog(System.DateTime.Now.ToString() + "      fnServiceAddFromSO() End");
                //// //ViewState["InvoiceTrans"] = DT;
                //////BindTempInvoiceTrans();
                ProcessTimLog(System.DateTime.Now.ToString() + "      ---BindSalesOrderServ() End");

            }
        }

        void BindSalesOrderDiag()
        {
            if (string.IsNullOrEmpty(Convert.ToString(ViewState["IAS_EMR_ID"])) == true)
            {
                goto FunEnd;
            }
            DataSet DS = new DataSet();
            IP_PTDiagnosis objPTDiag = new IP_PTDiagnosis();
            string Criteria = " 1=1 ";
            Criteria += " and IPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPD_ID ='" + Convert.ToString(ViewState["IAS_EMR_ID"]) + "'";

            DS = objPTDiag.IPDiagnosisGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();

                Int32 i = 0;
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    DT = (DataTable)ViewState["InvoiceClaims"];
                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["HIC_ICD_CODE"] = DR["IPD_DIAG_CODE"];
                    objrow["HIC_ICD_DESC"] = DR["IPD_DIAG_NAME"];

                    if (i == 0)
                    {
                        objrow["HIC_ICD_TYPE"] = "Principal";
                    }
                    else
                    {
                        objrow["HIC_ICD_TYPE"] = "Secondary";

                    }
                    DT.Rows.Add(objrow);

                    i = i + 1;
                }

                ViewState["InvoiceClaims"] = DT;

                BindTempInvoiceClaims();

            }


        FunEnd: ;

        }

        void Clear()
        {
            //objCom = new CommonBAL();
            //string strDate = "", strTime = ""; ;
            //strDate = objCom.fnGetDate("dd/MM/yyyy");
            //strTime = objCom.fnGetDate("hh:mm:ss");

            //txtInvDate.Text = strDate;
            //txtInvTime.Text = strTime;

            //txtClmStartDate.Text = strDate;
            //txtClmFromTime.Text = strTime.Substring(0, 5);

            //txtClmEndDate.Text = strDate;
            //txtClmToTime.Text = strTime.Substring(0, 5);




            ViewState["NewFlag"] = true;
            if (Convert.ToString(Session["DefaultType"]).ToLower() == "true")
            {
                drpInvType.SelectedIndex = 1;
            }
            else
            {
                drpInvType.SelectedIndex = 0;
            }

            txtTokenNo.Text = "";
            // txtFileNo.Text = "";
            txtName.Text = "";

            if (drpStatus.Items.Count > 0)
                drpStatus.SelectedIndex = 0;


            txtJobnumber.Text = "";
            txtJobnumber.Text = "";

            txtSubCompanyID.Text = "";
            txtCompanyName.Text = "";
            txtSubCompanyName.Text = "";
            txtPlanType.Text = "";
            txtPTComp.Text = "";
            txtPolicyNo.Text = "";
            txtIdNo.Text = "";
            txtPolicyExp.Text = "";

            TxtVoucherNo.Text = "";

            txtDoctorID.Text = "";
            txtDoctorName.Text = "";

            txtOrdClinCode.Text = "";
            txtOrdClinName.Text = "";

            if (drpTreatmentType.Items.Count > 0)
                drpTreatmentType.SelectedIndex = 0;

            txtRefDrId.Text = "";
            txtRefDrName.Text = "";
            txtRemarks.Text = "";

            if (drpRemarks.Items.Count > 0)
                drpRemarks.SelectedIndex = 0;

            txtClaimAmt.Text = "0.00";
            txtBillAmt1.Text = "0.00";
            txtBillAmt2.Text = "0.00";
            txtBillAmt3.Text = "0.00";
            txtBillAmt4.Text = "0.00";
            if (drpHospDiscType.Items.Count > 0)
                drpHospDiscType.SelectedIndex = 0;

            txtHospDisc.Text = "0.00";
            txtHospDiscAmt.Text = "0.00";
            txtDeductible.Text = "0.00";
            txtCoInsTotal.Text = "0.00";

            txtSplDisc.Text = "0.00";
            txtPTCredit.Text = "0.00";
            txtPaidAmount.Text = "0.00";
            TxtRcvdAmt.Text = "0.00";

            if (drpPayType.Items.Count > 0)
                drpPayType.SelectedIndex = 0;

            if (drpCurType.Items.Count > 0)
                drpCurType.SelectedIndex = 0;

            txtConvRate.Text = "";
            txtcurValue.Text = "";

            txtRcvdAmount.Text = "";


            if (drpCommissionType.Items.Count > 0)
                drpCommissionType.SelectedIndex = 0;


            txtAdvAmt.Text = "";

            txtICDDesc.Text = "";
            txtICdCode.Text = "";

            ChkTopupCard.Checked = false;
            TxtTopupAmt.Text = "";

            if (drpccType.Items.Count > 0)
                drpccType.SelectedIndex = 0;


            txtCCHolder.Text = "";
            txtCCNo.Text = "";
            txtCCRefNo.Text = "";

            txtDcheqno.Text = "";
            txtCashAmt.Text = "";
            txtCCAmt.Text = "";
            txtChqAmt.Text = "";

            if (drpBank.Items.Count > 0)
                drpBank.SelectedIndex = 0;

            txtDcheqdate.Text = "";
            txtTokenNo.Text = "";



            BuildeInvoiceTrans();
            gvInvoiceTrans.DataBind();

            BuildeInvoiceClaims();
            gvInvoiceClaims.DataBind();

            ViewState["Deductible_Type"] = "";
            ViewState["Deductible"] = "";
            ViewState["Co_Ins_Type"] = "";
            ViewState["Co_Ins_Amt"] = "";



            hidVisitID.Value = "";
            hidCompanyID.Value = "";
            hidCmpRefCode.Value = "";
            hidInsType.Value = "";
            hidIDNo.Value = "";
            hidInsTrtnt.Value = "";

            hidPTVisitType.Value = "";
            hidUpdateCodeType.Value = "";
            hidCompAgrmtType.Value = "";
            hidCoInsType.Value = "";
            hidReceipt.Value = "";
            hidType.Value = "";
            hidccText.Value = "";
            hidmasterinvoicenumber.Value = "";

            hidHaadID.Value = "";
            hidCatID.Value = "";
            hidCatType.Value = "";
            hidPaidAmtCheck.Value = "";


            divCC.Visible = false;
            divCheque.Visible = false;
            divAdvane.Visible = false;
            divMergeInvoiceNo.Visible = false;
            divMultiCurrency.Visible = false;

            if (drpClmType.Items.Count > 0)
                drpClmType.SelectedIndex = 0;
            if (drpClmStartType.Items.Count > 0)
                drpClmStartType.SelectedIndex = 0;
            if (drpClmEndType.Items.Count > 0)
                drpClmEndType.SelectedIndex = 0;
            if (drpDiagType.Items.Count > 0)
                drpDiagType.SelectedIndex = 0;

            TabContainer1.ActiveTab = tabService;

            hidMsgConfirm.Value = "";


            lblLstInvNoDate.Text = "";
            lblFstInvDate.Text = "";
            lblModifiedUser.Text = "";
            lblCreatedUser.Text = "";

            chkOnlySave.Checked = false;
            chkPrintCrBill.Checked = false;
            drpPrintType.SelectedIndex = 0;
            txtPriorAuthorizationID.Text = "";
            lblBalanceMsg.Text = "";

            chkNoMergePrint.Checked = false;
        }

        void New()
        {
            txtFileNo.Text = "";
            ViewState["NewFlag"] = true;
            objCom = new CommonBAL();


            txtInvoiceNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "INVOICE");

            drpStatus.SelectedValue = "Open";


        }
        #endregion


        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetCompany(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";


            Criteria += " AND HCM_COMP_ID Like '%" + prefixText + "%'";


            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.GetCompanyMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetCompanyName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";

            Criteria += " AND HCM_STATUS = 'A' AND HCM_BRANCH_ID = '" + strSessionBranchId + "' ";
            Criteria += " AND HCM_NAME Like '%" + prefixText + "%'";


            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.GetCompanyMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["HCM_COMP_ID"].ToString() + "~" + ds.Tables[0].Rows[i]["HCM_NAME"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetDoctorId(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_STAFF_ID Like '%" + prefixText + "%'";
            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetServiceID(string prefixText)
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND HSM_STATUS='A' ";

            Criteria += " AND HSM_SERV_ID  like '" + prefixText + "%' ";
            DS = objCom.HMS_SP_ServiceMasterTopGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_SERV_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetServiceName(string prefixText)
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND HSM_STATUS='A' ";

            Criteria += " AND HSM_NAME   like '" + prefixText + "%' ";
            DS = objCom.HMS_SP_ServiceMasterTopGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HSM_SERV_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HSM_NAME"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }




        [System.Web.Services.WebMethod]
        public static string[] GetDiagnosisID(string prefixText)
        {

            CommonBAL dbo = new CommonBAL();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND DIM_STATUS='A' ";

            Criteria += " AND DIM_ICD_ID  like '" + prefixText + "%' ";
            DS = dbo.DrICDMasterGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDiagnosisName(string prefixText)
        {

            CommonBAL dbo = new CommonBAL();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND DIM_STATUS='A' ";

            Criteria += " AND DIM_ICD_DESCRIPTION   like '" + prefixText + "%' ";
            DS = dbo.DrICDMasterGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetRefDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            Criteria += " AND HRDM_FNAME   like '" + prefixText + "%' ";
            Criteria += " AND HRDM_BRANCH_ID ='" + strSessionBranchId + "'";
            DataSet DS = new DataSet();

            CommonBAL objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue("HRDM_REF_ID,HRDM_FNAME", "HMS_REF_DOCTOR_MASTER", Criteria, "");


            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["HRDM_REF_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["HRDM_FNAME"]);
                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion

        Boolean fnReVisitType()
        {

            string Criteria = " 1=1  ";

            Criteria += " AND   HIT_SERV_TYPE='C' and HIM_PT_ID='" + txtFileNo.Text.Trim() + "' AND  HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";


            DataSet DS = new DataSet();
            IP_Invoice objInv = new IP_Invoice();
            DS = objInv.IPInvoiceTransGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                string strLastVisit = Convert.ToString(DS.Tables[0].Rows[0]["HIM_DATE"]);

                int intRevisit = Convert.ToInt32(Session["RevisitDays"]);
                if (strLastVisit != "")
                {
                    DateTime dtLastVisit = Convert.ToDateTime(strLastVisit);
                    DateTime Today;
                    //if (Convert.ToString(ViewState["DATABASE_DATE"]) == "1")
                    //{
                    Today = System.DateTime.Now; //Convert.ToDateTime(hidTodayDBDate.Value);
                    //}
                    //else
                    //{
                    //    Today = Convert.ToDateTime(Convert.ToString(Session["LocalDate"]));
                    //}

                    TimeSpan Diff = Today.Date - dtLastVisit.Date;

                    if (Diff.Days <= intRevisit && Diff.Days >= 2)
                    {
                        return true;
                    }
                }


            }

            return false;
        }

        void fnMergeReport()
        {
            string Criteria = " 1=1 ";

            if (txtFileNo.Text.Trim() == "")
            {
                lblStatus.Text = "File No. should not be blank.";
                goto FunEnd;
            }

            string InvoiceNos = "";
            DataSet DSInv = new DataSet();
            CommonBAL objCom = new CommonBAL();

            string strStartDate = txtInvDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtInvDate.Text != "")
            {
                Criteria += " AND    HIM_DATE >= '" + strForStartDate + " 00:00:01'";
                Criteria += " AND    HIM_DATE <= '" + strForStartDate + " 23:59:59'";
            }

            Criteria += " AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HIM_PT_ID='" + txtFileNo.Text.Trim() + "'  AND  HIM_DR_CODE='" + txtDoctorID.Text.Trim() + "' AND HIM_SUB_INS_CODE='" + txtSubCompanyID.Text.Trim() + "'";

            if (txtFileNo.Text.Trim() == "OPDCR" || txtFileNo.Text.Trim() == "OPD")
            {
                Criteria += " and HIM_POLICY_NO='" + txtPolicyNo.Text.Trim() + "'";
            }


            DSInv = objCom.fnGetFieldValue(" distinct(HIM_INVOICE_ID) HIM_INVOICE_ID  ", "HMS_INVOICE_MASTER", Criteria, "HIM_INVOICE_ID");

            if (DSInv.Tables[0].Rows.Count > 0)
            {

                for (int i = 0; i < DSInv.Tables[0].Rows.Count; i++)
                {

                    if (InvoiceNos != "") InvoiceNos = InvoiceNos + @"\";

                    InvoiceNos = InvoiceNos + " " + Convert.ToString(DSInv.Tables[0].Rows[i]["HIM_INVOICE_ID"]);
                }

            }

            string InvDate = txtInvDate.Text.Trim();
            string InvoiceNo = txtInvoiceNo.Text.Trim();
            string FileNo = txtFileNo.Text.Trim();
            string DrId = txtDoctorID.Text.Trim();
            string SubCompId = txtSubCompanyID.Text.Trim();
            string PolicyNo = txtPolicyNo.Text.Trim();
            string NoMergePrint = "N";
            if (chkNoMergePrint.Checked == true)
            {
                NoMergePrint = "Y";
            }
            else
            {
                NoMergePrint = "Y";
            }





            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMergeReport('" + Convert.ToString(Session["Branch_ID"]) + "','" + InvoiceNo + "','" + InvDate + "','" + FileNo + "','" + DrId + "','" + SubCompId + "','" + PolicyNo + "','" + NoMergePrint + "');", true);


        FunEnd: ;

        }

        void fnPrint(string strInvoiceNo)
        {
            ReportName = "";
            strPrintType = "";

            if (chkOnlySave.Checked == true && chkPrintCrBill.Checked == false)
            {
                goto FunEnd;
            }

            if (GlobalValues.FileDescription == "RASHIDIYA")
            {
                goto pr1;
            }
            else
            {
                if (drpStatus.SelectedValue == " Hold" && drpStatus.SelectedValue == "Void")
                {
                    goto FunEnd;
                }

            }
        pr1: ;

            if (chkOnlySave.Checked == false)
            {

                if (drpPrintType.SelectedIndex != 0)
                {
                    strPrintType = drpPrintType.SelectedValue;
                    strPrintType = strPrintType.Substring(0, 5);

                }

                if (strPrintType == "Print")
                {
                    ReportName = "HmsInvoice" + drpPrintType.SelectedValue + ".rpt";
                }

                else if (drpInvType.SelectedValue == "Cash")
                {
                    if (GlobalValues.InvoiceType == "PERFORMA")
                    {
                        ReportName = "HmsInvoiceCa_PERFORMA.rpt";
                    }
                    else
                    {
                        ReportName = "HmsInvoiceCa.rpt";
                    }

                }
                else if (drpInvType.SelectedValue == "Credit")
                {
                    if (GlobalValues.InvoiceType == "PERFORMA")
                    {
                        ReportName = "HmsInvoiceCr_performa.rpt";
                    }
                    else
                    {
                        ReportName = "HmsInvoiceCr.rpt";
                    }
                }

                // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPrintInvoice('" + strInvoiceNo + "','" + ReportName + "');", true);

            }

            if (chkPrintCrBill.Checked == true && drpInvType.SelectedValue == "Credit")
            {
                if (GlobalValues.FileDescription == "SMCH")// And HMS_INSURANCE Then
                {
                    fnMergeReport();

                }
                else
                {
                    string strRpt = "";

                    if (GlobalValues.InvoiceType != "")
                    {
                        strRpt = "_" + GlobalValues.InvoiceType;
                    }

                    if (strPrintType == "Print")
                    {
                        ReportName = "HmsInvoice" + drpPrintType.SelectedValue + ".rpt";
                    }

                    else if (drpPrintType.SelectedValue == "Net")
                    {
                        ReportName = "HmsCreditBill1" + strRpt + ".rpt";
                    }
                    else if (drpPrintType.SelectedIndex == 3)// 'credit note
                    {
                        ReportName = "HmsCreditBill2" + strRpt + ".rpt";
                    }
                    else if (drpPrintType.SelectedIndex == 1) //gross
                    {
                        ReportName = "HmsCreditBill0" + strRpt + ".rpt";
                    }
                    else
                    {
                        //if( UCase(Trim(txttype.Text)) = "PHY" )
                        //      ReportName ="IVaissdetA5.rpt";
                        //else
                        //{
                        if (GlobalValues.InvoiceType == "PERFORMA")
                            ReportName = "msCreditBill_performa.rpt";
                        else
                        {
                            ReportName = "HmsCreditBill.rpt";

                            if (GlobalValues.FileDescription == "MASFOOT")
                            {
                                String RptName = "";
                                string Criteria = " 1=1 ";
                                Criteria += " AND HCM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HCM_COMP_ID ='" + txtSubCompanyID.Text.Trim() + "'";
                                DataSet DS = new DataSet();
                                objCom = new CommonBAL();
                                objCom.GetCompanyMaster(Criteria);

                                if (DS.Tables[0].Rows.Count > 0)
                                {
                                    if (DS.Tables[0].Rows[0].IsNull("INVOICERPT") == false)
                                        RptName = Convert.ToString(DS.Tables[0].Rows[0]["INVOICERPT"]);

                                }
                                if (RptName != "")
                                    ReportName = "HmsCreditBill" + RptName + ".rpt";
                                else
                                    ReportName = "HmsCreditBill.rpt";

                            }
                        }
                    }
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPrintInvoice('" + Convert.ToString(Session["Branch_ID"]) + "','" + strInvoiceNo + "','" + ReportName + "');", true);
                }


            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPrintInvoice('" + Convert.ToString(Session["Branch_ID"]) + "','" + strInvoiceNo + "','" + ReportName + "');", true);

            }



        FunEnd: ;
        }


        void PrintCrBill()
        {
            if (chkPrintCrBill.Checked == true && drpInvType.SelectedValue == "Credit")
            {

                if (txtInvoiceNo.Text.Trim() == "")
                {
                    goto FunEnd;
                }

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPrintCrBill('" + txtInvoiceNo.Text.Trim() + "');", true);
            }

        FunEnd: ;

        }

        void PrintInvoice()
        {
            if (chkOnlySave.Checked == false)
            {

                if (txtInvoiceNo.Text.Trim() == "")
                {
                    goto FunEnd;
                }

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPrintInvoice('" + txtInvoiceNo.Text.Trim() + "','" + drpInvType.SelectedValue + "');", true);
            }

        FunEnd: ;

        }

        void fnLastConsltDate()
        {
            lblLstInvNoDate.Text = "";
            lblFstInvDate.Text = "";
            IP_Invoice objInv = new IP_Invoice();
            string Criteria = "1=1 ";
            Criteria += " AND HIM_PT_ID='" + txtFileNo.Text.Trim() + "' AND  HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            DataSet DSVisitType = new DataSet();
            DSVisitType = objInv.ReVisitTypeGet(Criteria);
            if (DSVisitType.Tables[0].Rows.Count > 0)
            {
                lblLstInvNoDate.Text = Convert.ToString(DSVisitType.Tables[0].Rows[0]["HIM_INVOICE_ID"]) + " - " + Convert.ToString(DSVisitType.Tables[0].Rows[0]["HIM_DATEDesc"]);

                lblFstInvDate.Text = Convert.ToString(DSVisitType.Tables[0].Rows[DSVisitType.Tables[0].Rows.Count - 1]["HIM_DATEDesc"]);
            }
        }

        string GetLabValue(string strServCode, decimal nConversion)
        {

            string strLabValue = "";
            decimal decLabValue = 0;
            IP_Invoice objInv = new IP_Invoice();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND convert(varchar(10),dbo.LAB_TEST_REPORT_MASTER.LTRM_TEST_DATE ,103)='" + txtInvDate.Text.Trim() + "'" +
             "	 AND dbo.LAB_TEST_REPORT_MASTER.LTRM_PATIENT_ID = '" + txtFileNo.Text.Trim() + "'" +
             "   AND dbo.LAB_TEST_REPORT_MASTER.LTRM_DR_CODE = '" + txtDoctorID.Text.Trim() + "'" +
             "   AND dbo.LAB_TEST_PROFILE_MASTER.LTPM_SERV_ID ='" + strServCode + "'";

            DS = objInv.GetLabValues(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows.Count == 1)
                {
                    decLabValue = DS.Tables[0].Rows[0].IsNull(0) == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[0][0]);
                    if (nConversion != 0) decLabValue = decLabValue * nConversion;
                    strLabValue = Convert.ToString(decLabValue);
                }
                else if (DS.Tables[0].Rows.Count > 1)
                {
                    for (Int32 i = 0; i < DS.Tables[0].Rows.Count; i++)
                    {
                        if (strLabValue != "") strLabValue += "/";

                        if (nConversion != 0)
                        {
                            decLabValue = DS.Tables[0].Rows[i].IsNull(0) == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[i][0]) * nConversion;
                        }
                        else
                        {
                            decLabValue = DS.Tables[0].Rows[i].IsNull(0) == true ? 0 : Convert.ToDecimal(DS.Tables[0].Rows[i][0]);
                        }

                        strLabValue += decLabValue;
                    }
                }

            }


            return strLabValue;
        }

        string GetBPValue(string strServCode)
        {
            string strLabValue = "", strBPSys = "", strBPDia = "";
            string strEMRID = "0";
            string Criteria = " 1=1 ";
            Criteria += " AND convert(varchar(10),EPM_DATE ,103)='" + txtInvDate.Text.Trim() + "'" +
             "	 AND EPM_PT_ID = '" + txtFileNo.Text.Trim() + "'" +
             "   AND EPM_DR_CODe = '" + txtDoctorID.Text.Trim() + "'";

            EMR_PTMasterBAL objPTM = new EMR_PTMasterBAL();
            DS = objPTM.GetEMR_PTMaster(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                strEMRID = Convert.ToString(DS.Tables[0].Rows[0]["EPM_ID"]);

                Criteria = " 1=1 ";
                Criteria += " AND  EPV_ID = '" + strEMRID + "'";

                IP_PTVitalSign objPTVital = new IP_PTVitalSign();

                DataSet DS1 = new DataSet();
                DS1 = objPTVital.PTVitalSignGet(Criteria);

                if (DS1.Tables[0].Rows.Count > 0)
                {

                    strBPSys = Convert.ToString(DS1.Tables[0].Rows[0]["EPV_BP_SYSTOLICDesc"]);

                    strBPDia = Convert.ToString(DS1.Tables[0].Rows[0]["EPV_BP_DIASTOLICDesc"]);

                    strLabValue = strBPSys + "/" + strBPDia;

                }

            }



            return strLabValue;

        }

        void AmountCalculation()
        {
            decimal decDiscount = 0, decFees = 0, decHitAmt = 0, decTotalAmount = 0, decTotalDeduct = 0, decCoInsAmt = 0, decTotalCoInsAmt = 0;

            for (int intCurRow = 0; intCurRow < gvInvoiceTrans.Rows.Count; intCurRow++)
            {
                TextBox txtFee, txtDiscAmt, txtgvQty, txtHitAmount, txtDeduct, txtCoInsAmt, txtObsType;

                DropDownList drpgvDiscType, drpCoInsType;

                Label lblServType = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblServType");

                txtFee = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtFee");
                txtgvQty = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvQty");
                txtHitAmount = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtHitAmount");
                drpgvDiscType = (DropDownList)gvInvoiceTrans.Rows[intCurRow].FindControl("drpgvDiscType");
                txtDiscAmt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtDiscAmt");
                txtDeduct = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtDeduct");
                drpCoInsType = (DropDownList)gvInvoiceTrans.Rows[intCurRow].FindControl("drpCoInsType");
                txtCoInsAmt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtCoInsAmt");

                TextBox txtgvCoInsTotal = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCoInsTotal");
                TextBox txtgvCompType = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCompType");
                TextBox txtgvCompAmount = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCompAmount");
                TextBox txtgvCompTotal = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCompTotal");

                txtObsType = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsType");

                TextBox txtHaadCode = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtHaadCode");
                TextBox txtHaadDesc = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtHaadDesc");
                TextBox txtTypeValue = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtTypeValue");


                if (lblServType.Text == "C")
                {
                    drpCoInsType.Enabled = false;
                    txtCoInsAmt.ReadOnly = true;
                }

                if (txtFee.Text.Trim() == "")
                {
                    txtFee.Text = "0.00";
                }

                if (txtDiscAmt.Text.Trim() == "")
                {
                    txtDiscAmt.Text = "0.00";
                }
                if (txtgvQty.Text.Trim() == "")
                {
                    txtgvQty.Text = "1";
                }
                if (txtCoInsAmt.Text.Trim() == "")
                {
                    txtCoInsAmt.Text = "0.00";
                }
                if (txtHitAmount.Text.Trim() == "")
                {
                    txtHitAmount.Text = "0.00";
                }

                if (txtDeduct.Text.Trim() == "")
                {
                    txtDeduct.Text = "0.00";
                }
                if (txtHospDisc.Text.Trim() == "")
                {
                    txtHospDisc.Text = "0.00";
                }

                if (txtSplDisc.Text.Trim() == "")
                {
                    txtSplDisc.Text = "0.00";
                }

                if (txtPTCredit.Text.Trim() == "")
                {
                    txtPTCredit.Text = "0.00";
                }

                if (txtCoInsTotal.Text.Trim() == "")
                {
                    txtCoInsTotal.Text = "0.00";
                }


                if (txtBillAmt1.Text.Trim() == "")
                {
                    txtBillAmt1.Text = "0.00";
                }

                if (txtBillAmt2.Text.Trim() == "")
                {
                    txtBillAmt2.Text = "0.00";
                }

                if (txtBillAmt3.Text.Trim() == "")
                {
                    txtBillAmt3.Text = "0.00";
                }

                if (txtBillAmt4.Text.Trim() == "")
                {
                    txtBillAmt4.Text = "0.00";
                }

                if (TxtTopupAmt.Text.Trim() == "")
                {
                    TxtTopupAmt.Text = "0.00";
                }

                if (txtCCAmt.Text.Trim() == "")
                {
                    txtCCAmt.Text = "0.00";
                }

                if (txtChqAmt.Text.Trim() == "")
                {
                    txtChqAmt.Text = "0.00";
                }

                if (drpgvDiscType.SelectedIndex == 0)
                {
                    decDiscount = Convert.ToDecimal(txtDiscAmt.Text.Trim());

                }
                else
                {

                    decDiscount = ((Convert.ToDecimal(txtFee.Text.Trim()) / 100) * Convert.ToDecimal(txtDiscAmt.Text.Trim()));
                }


                decFees = Convert.ToDecimal(txtFee.Text.Trim()) - Convert.ToDecimal(decDiscount);

                decHitAmt = Convert.ToDecimal(txtgvQty.Text.Trim()) * decFees;

                txtHitAmount.Text = decHitAmt.ToString("#0.00");


                if (txtDeduct.Text.Trim() != "" && lblServType.Text == "C")
                {
                    // txtDeduct.Text = Convert.ToString(Convert.ToDecimal(txtDeduct.Text.Trim()) * Convert.ToDecimal(txtgvQty.Text.Trim()));
                    // txtCoInsAmt.Text = Convert.ToString(Convert.ToDecimal(txtDeduct.Text.Trim()) * Convert.ToDecimal(txtgvQty.Text.Trim()));
                    if (drpCoInsType.SelectedIndex == 0)
                    {
                        decCoInsAmt = Convert.ToDecimal(txtCoInsAmt.Text.Trim());

                    }
                    else
                    {

                        decCoInsAmt = ((Convert.ToDecimal(txtHitAmount.Text.Trim()) / 100) * Convert.ToDecimal(txtCoInsAmt.Text.Trim()));
                    }
                }


                decTotalAmount = decTotalAmount + Convert.ToDecimal(txtHitAmount.Text.Trim());



                decTotalDeduct = decTotalDeduct + Convert.ToDecimal(txtDeduct.Text.Trim());



                if (lblServType.Text != "C")//== "S")
                {
                    if (drpCoInsType.SelectedIndex == 0)
                    {
                        decCoInsAmt = Convert.ToDecimal(txtCoInsAmt.Text.Trim());

                    }
                    else
                    {

                        decCoInsAmt = ((Convert.ToDecimal(txtHitAmount.Text.Trim()) / 100) * Convert.ToDecimal(txtCoInsAmt.Text.Trim()));
                    }

                }
                decTotalCoInsAmt = decTotalCoInsAmt + decCoInsAmt;

                txtgvCoInsTotal.Text = Convert.ToString(decCoInsAmt);

                if (txtSubCompanyID.Text != "")
                {
                    txtgvCompType.Text = drpCoInsType.Text;
                    txtgvCompAmount.Text = Convert.ToString(Convert.ToDecimal(txtHitAmount.Text) - decCoInsAmt);
                    txtgvCompTotal.Text = Convert.ToString(Convert.ToDecimal(txtHitAmount.Text) - decCoInsAmt);

                }
                else
                {
                    txtgvCompType.Text = drpCoInsType.Text;
                    txtgvCompAmount.Text = "0.00";
                    txtgvCompTotal.Text = "0.00";
                }



                string strHaadName = "", strHaadType = "";
                if (txtHaadCode.Text.Trim() != "")
                {
                    DataSet DSHaad = new DataSet();
                    DSHaad = GetHaadName(txtHaadCode.Text.Trim());

                    if (DSHaad.Tables[0].Rows.Count > 0)
                    {
                        if (DSHaad.Tables[0].Rows[0].IsNull("HHS_DESCRIPTION") == false)
                        {
                            strHaadName = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_DESCRIPTION"]);
                        }
                        if (DSHaad.Tables[0].Rows[0].IsNull("HHS_TYPE_VALUE") == false)
                        {
                            strHaadType = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_TYPE_VALUE"]);
                        }
                    }
                    txtHaadDesc.Text = strHaadName;
                    txtTypeValue.Text = strHaadType;

                }


            }

            txtBillAmt1.Text = decTotalAmount.ToString("#0.00");
            txtDeductible.Text = decTotalDeduct.ToString("#0.00");



            //--------------------------

            decimal decBill2 = 0, decHosDisc = 0;
            if (txtBillAmt1.Text.Trim() != "" && txtHospDisc.Text.Trim() != "")
            {
                decHosDisc = Convert.ToDecimal(txtHospDisc.Text.Trim());

                txtHospDisc.Text = decHosDisc.ToString("#0.00");

                if (drpHospDiscType.SelectedIndex == 0)
                {
                    decBill2 = Convert.ToDecimal(txtBillAmt1.Text.Trim()) - Convert.ToDecimal(txtHospDisc.Text.Trim());

                }
                else
                {

                    decBill2 = Convert.ToDecimal(txtBillAmt1.Text.Trim()) - ((Convert.ToDecimal(txtBillAmt1.Text.Trim()) / 100) * Convert.ToDecimal(txtHospDisc.Text.Trim()));
                }

                txtBillAmt2.Text = decBill2.ToString("#0.00");
            }
            else
            {
                txtBillAmt2.Text = txtBillAmt1.Text.Trim();
            }
            //--------------------------

            decimal decBill3 = 0;
            if (txtBillAmt2.Text.Trim() != "" && txtDeductible.Text.Trim() != "")
            {

                decBill3 = Convert.ToDecimal(txtBillAmt2.Text.Trim()) - Convert.ToDecimal(txtDeductible.Text.Trim());

            }
            txtBillAmt3.Text = decBill3.ToString("#0.00");


            if (drpInvType.SelectedValue == "Cash")
            {
                decTotalCoInsAmt = decTotalCoInsAmt - decHosDisc;
                txtCoInsTotal.Text = decTotalCoInsAmt.ToString("#0.00");
            }
            else
            {
                txtCoInsTotal.Text = decTotalCoInsAmt.ToString("#0.00");
            }

            if (txtSubCompanyID.Text != "")
            {
                decimal decClaimAmt = 0;
                if (txtBillAmt2.Text.Trim() != "" && txtCoInsTotal.Text.Trim() != "")
                {
                    decClaimAmt = Convert.ToDecimal(txtBillAmt2.Text.Trim()) - Convert.ToDecimal(txtCoInsTotal.Text.Trim());
                }
                txtClaimAmt.Text = decClaimAmt.ToString("#0.00");

            }
            else
            {
                txtClaimAmt.Text = "0.00";
            }
            decimal decBill4 = 0;
            if (txtSubCompanyID.Text != "")
            {
                if (txtCoInsTotal.Text.Trim() != "" && txtSplDisc.Text.Trim() != "")
                {
                    decBill4 = Convert.ToDecimal(txtCoInsTotal.Text.Trim()) - Convert.ToDecimal(txtSplDisc.Text.Trim());

                }
            }
            else
            {
                decBill4 = Convert.ToDecimal(txtBillAmt3.Text.Trim()) - Convert.ToDecimal(txtSplDisc.Text.Trim());
            }
            txtBillAmt4.Text = decBill4.ToString("#0.00");



            decimal decPaidAmt = 0, decTopupAmt = 0, decRevAmt; //Convert.ToDecimal(txtCoInsTotal.Text.Trim());

            if (txtSplDisc.Text.Trim() != "")
            {
                decPaidAmt = decPaidAmt - Convert.ToDecimal(txtSplDisc.Text.Trim());
            }

            if (txtPTCredit.Text.Trim() != "")
            {
                decPaidAmt = decPaidAmt - Convert.ToDecimal(txtPTCredit.Text.Trim());
            }

            txtPaidAmount.Text = decPaidAmt.ToString("#0.00");


            if (GlobalValues.FileDescription == "ALDAR" && (Convert.ToBoolean(ViewState["NewFlag"]) == true || Convert.ToDecimal(txtPTCredit.Text.Trim()) > 0))
            {
                txtPTCredit.Text = Convert.ToString(txtBillAmt4.Text.Trim());
                decPaidAmt = Convert.ToDecimal(txtBillAmt4.Text.Trim()) - Convert.ToDecimal(txtPTCredit.Text.Trim());
            }
            else
            {
                txtPTCredit.Text = Convert.ToString(txtPTCredit.Text.Trim() != "" ? txtPTCredit.Text.Trim() : "0.00");

                if (ChkTopupCard.Checked == true)
                {
                    decTopupAmt = Convert.ToDecimal(txtBillAmt4.Text) - Convert.ToDecimal(txtPTCredit.Text.Trim());
                    decPaidAmt = Convert.ToDecimal(txtBillAmt4.Text) - Convert.ToDecimal(txtPTCredit.Text.Trim()) - decTopupAmt;
                }
                else
                {
                    if (txtPTCredit.Text.Trim() != "")
                    {
                        decPaidAmt = Convert.ToDecimal(txtBillAmt4.Text) - Convert.ToDecimal(txtPTCredit.Text.Trim());
                    }
                    else
                    {
                        decPaidAmt = Convert.ToDecimal(txtBillAmt4.Text);
                    }
                    decTopupAmt = 0;
                }


            }
            txtPaidAmount.Text = decPaidAmt.ToString("#0.00");
            TxtTopupAmt.Text = decTopupAmt.ToString("#0.00");


            decRevAmt = Convert.ToDecimal(txtBillAmt4.Text) - Convert.ToDecimal(txtPTCredit.Text) - Convert.ToDecimal(TxtTopupAmt.Text);
            TxtRcvdAmt.Text = decRevAmt.ToString("#0.00");

            decimal decCCAmt = 0, decChqAmt = 0, decCashAmt = 0;

            if (txtCCAmt.Text.Trim() != "")
            {
                decCCAmt = Convert.ToDecimal(txtCCAmt.Text.Trim());
            }

            if (txtChqAmt.Text.Trim() != "")
            {
                decChqAmt = Convert.ToDecimal(txtChqAmt.Text.Trim());
            }

            decCashAmt = decPaidAmt - decCCAmt - decChqAmt;

            txtCashAmt.Text = decCashAmt.ToString("#0.00");

        }

        Boolean fnSave()
        {
            //PrintCrBill();
            //PrintInvoice();

            Boolean isError = false;

            string PrevBilltoCode;
            decimal PrevClaim_Amt = 0, PrevClaimPaid = 0, PrevPTReject = 0, PrevCmpReject = 0, PrevPTCrPaid = 0;

            decimal CmpInvoiceNo = 0;

            PrevBilltoCode = hidCompanyID.Value;

            objCom = new CommonBAL();
            IP_Invoice objInv = new IP_Invoice();
            String Criteria = "1=1", FieldNameWithValues = "";



            TextFileWriting("Invoice.btnSave   1  ");

            if (GlobalValues.FileDescription == "ALREEM")
            {
                if (txtRefDrName.Text.Trim() != "")
                {
                    if (drpCommissionType.SelectedIndex == -1)
                    {
                        lblStatus.Text = "Please select commision type from the list";
                        isError = true;
                        goto FunEnd;
                    }

                }
            }
            TextFileWriting("Invoice.btnSave   2  ");


            if (hidMsgConfirm.Value == "false")
            {
                isError = true;
                goto FunEnd;
            }



            TextFileWriting("Invoice.btnSave   4  ");

            if (GlobalValues.FileDescription == "ANP" || GlobalValues.FileDescription == "ALNOORSATWA" && Convert.ToString(Session["User_ID"]).ToUpper() != "ADMIN" && drpInvType.SelectedValue == "Cash")
            {

                string dtInvDate = Convert.ToString(txtInvDate.Text.Trim());
                DateTime Today;
                Today = Convert.ToDateTime(objCom.fnGetDate("dd/MM/yyyy"));

                Int32 intDatediff = 0;
                intDatediff = objCom.fnGetDateDiff(Convert.ToString(Today), dtInvDate);

                if (intDatediff > 0)
                {
                    lblStatus.Text = "You cannot edit Cash Invoice. ";
                    isError = true;
                    goto FunEnd;
                }


            }
            TextFileWriting("Invoice.btnSave   5  ");

            if (GlobalValues.FileDescription == "ANP" || GlobalValues.FileDescription == "ALREEM" || GlobalValues.FileDescription == "ALNOORSATWA")
            {
                if (txtRemarks.Text == "")
                {
                    lblStatus.Text = "Remarks field should be non-empty for editing Invoice. ";
                    isError = true;
                    goto FunEnd;
                }
            }
            Boolean boolPrincipal = false;

            if (Convert.ToString(Session["HSOM_ECLAIM"]).ToUpper() == "Y")
            {
                DataTable DT = (DataTable)ViewState["InvoiceClaims"];

                //if (DT.Rows.Count <= 0)
                //{

                //    lblStatus.Text = "Please Add the Diagnosis ";
                //    isError = true;
                //    goto FunEnd;
                //}

                if (DT.Rows.Count > 0)
                {
                    for (int i = 0; i < DT.Rows.Count; i++)
                    {
                        string strICDType = "";
                        strICDType = Convert.ToString(DT.Rows[i]["HIC_ICD_TYPE"]);

                        if (strICDType == "Principal")
                        {
                            boolPrincipal = true;
                            goto EndForDiag;
                        }


                    }
                EndForDiag: ;
                    if (boolPrincipal == false)
                    {
                        lblStatus.Text = "Please Add the Principal Diagnosis";
                        isError = true;
                        goto FunEnd;
                    }
                }

            }

            TextFileWriting("Invoice.btnSave   6  ");
            if (GlobalValues.FileDescription == "PHY") // And App.LegalCopyright <> "1" Then
            {
                FieldNameWithValues = " HIM_PT_COMP='" + txtPTComp.Text.Trim() + "' ";
                Criteria = "HIM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_MASTER", Criteria);


                if (ChkTopupCard.Checked == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
                {
                    FieldNameWithValues = " HIM_PT_COMP='" + txtPTComp.Text.Trim() + "' ";
                    Criteria = "HIM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIM_INVOICE_ID='T" + txtInvoiceNo.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_MASTER", Criteria);

                }

                //fnclearfields
                goto FunEnd;

            }
            TextFileWriting("Invoice.btnSave   7  ");
            // objCom.AudittrailAdd(Convert.ToString(Session["Branch_ID"]), "Invoice", "MASTER", "Validated Invoice Master entry at " + objCom.fnGetDate("dd/MM/yyyy hh:mm:ss"), Convert.ToString(Session["User_ID"]));

            TextFileWriting("Invoice.btnSave   8  ");

            if (drpPayType.SelectedItem.Text == "Multi Currency")
            {
                txtDcheqdate.Text = "";
                txtDcheqno.Text = "";
                drpBank.SelectedIndex = -1;
                drpccType.SelectedIndex = -1;
                txtCCNo.Text = "";
                txtCCRefNo.Text = "";
                txtCCAmt.Text = "0";
                txtChqAmt.Text = "0";
            }
            else if (drpPayType.SelectedItem.Text == "Credit Card" || drpPayType.SelectedItem.Text == "Debit Card")
            {
                txtDcheqdate.Text = "";
                txtDcheqno.Text = "";
                drpBank.SelectedIndex = -1;
                txtConvRate.Text = "";
                txtcurValue.Text = "";
                txtChqAmt.Text = "0";
                txtRcvdAmount.Text = "0";
                drpCurType.SelectedIndex = -1;
                decimal decPaidAmt = 0, decCCAmt = 0;

                if (txtPaidAmount.Text != "")
                {
                    decPaidAmt = Convert.ToDecimal(txtPaidAmount.Text);
                }

                if (txtCCAmt.Text != "")
                {
                    decCCAmt = Convert.ToDecimal(txtCCAmt.Text);
                }

                txtCashAmt.Text = Convert.ToString(decPaidAmt - decCCAmt);

            }
            else if (drpPayType.SelectedItem.Text.ToUpper() == "UTILISE ADVANCE")
            {
                txtDcheqdate.Text = "";
                txtDcheqno.Text = "";
                drpBank.SelectedIndex = -1;
                txtConvRate.Text = "";
                txtcurValue.Text = "";
                txtChqAmt.Text = "0";
                txtRcvdAmount.Text = "0";
                drpCurType.SelectedIndex = -1;
                txtCashAmt.Text = "0";

            }
            else if (drpPayType.SelectedItem.Text == "Cheque")
            {
                txtCCNo.Text = "";
                txtCCRefNo.Text = "";
                txtConvRate.Text = "";
                txtcurValue.Text = "";
                drpCurType.SelectedIndex = -1;
                txtRcvdAmount.Text = "0";
            }
            else if (drpPayType.SelectedItem.Text == "Advance")
            {
                txtDcheqdate.Text = "";
                txtDcheqno.Text = "";
                drpBank.SelectedIndex = -1;
                txtConvRate.Text = "";
                txtcurValue.Text = "";
                txtChqAmt.Text = "0";
                txtRcvdAmount.Text = "0";
                drpCurType.SelectedIndex = -1;

                decimal decPaidAmt = 0, decCCAmt = 0;

                if (txtPaidAmount.Text != "")
                {
                    decPaidAmt = Convert.ToDecimal(txtPaidAmount.Text);
                }

                if (txtCCAmt.Text != "")
                {
                    decCCAmt = Convert.ToDecimal(txtCCAmt.Text);
                }

                txtCashAmt.Text = Convert.ToString(decPaidAmt - decCCAmt);

            }
            else
            {
                txtConvRate.Text = "";
                txtcurValue.Text = "";
                txtCCNo.Text = "";
                txtCCRefNo.Text = "";
                txtDcheqdate.Text = "";
                txtDcheqno.Text = "";
                txtCCHolder.Text = "";
                drpCurType.SelectedIndex = -1;
                drpBank.SelectedIndex = -1;
                drpccType.SelectedIndex = -1;
                txtCCAmt.Text = "0";
                txtChqAmt.Text = "0";
                txtRcvdAmount.Text = "0";

                decimal decPaidAmt = 0;

                if (txtPaidAmount.Text != "")
                {
                    decPaidAmt = Convert.ToDecimal(txtPaidAmount.Text);
                }


                txtCashAmt.Text = Convert.ToString(decPaidAmt);

            }
            TextFileWriting("Invoice.btnSave   9  ");

            hidReceipt.Value = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "RECEIPT");

            if (GlobalValues.FileDescription != "HOLISTIC")
            {
                TextFileWriting("Invoice.btnSave   10  ");

                Criteria = "1=1 AND HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                if (objCom.fnCheckduplicate("HMS_INVOICE_MASTER", Criteria) == true && Convert.ToBoolean(ViewState["NewFlag"]) == false)
                {

                    IP_Invoice objInvMastEdit = new IP_Invoice();
                    objInvMastEdit.HIM_INVOICE_ID = txtInvoiceNo.Text.Trim();
                    objInvMastEdit.HIM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objInvMastEdit.InvoiceMasterEditAdd();

                    TextFileWriting("Invoice.btnSave   11  ");


                    if (GlobalValues.FileDescription == "KMC" && drpInvType.SelectedValue == "Credit")
                    {
                        DataSet DSInvNo = new DataSet();
                        DSInvNo = objInv.GetLastCompanyInvoiceNo(Convert.ToString(Session["Branch_ID"]), txtSubCompanyID.Text.Trim());
                        if (DSInvNo.Tables[0].Rows.Count > 0)
                        {
                            if (DSInvNo.Tables[0].Rows[0].IsNull(0) == false)
                                CmpInvoiceNo = Convert.ToDecimal(DSInvNo.Tables[0].Rows[0][0]) + 1;
                        }
                    }








                    DataSet DSInv = new DataSet();
                    Criteria = "1=1 AND HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                    DSInv = objInv.InvoiceMasterGet(Criteria);
                    if (DSInv.Tables[0].Rows.Count > 0)
                    {
                        ViewState["NewFlag"] = false;
                        TextFileWriting("Invoice.btnSave   12  ");
                        IP_Invoice objInvTransEdit = new IP_Invoice();
                        objInvTransEdit.HIT_INVOICE_ID = txtInvoiceNo.Text.Trim();
                        objInvTransEdit.HIT_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);

                        objInvTransEdit.InvoiceTransEditAdd();
                        TextFileWriting("Invoice.btnSave   13  ");


                        TextFileWriting("Invoice.btnSave   14  ");
                        hidReceipt.Value = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_RECEIPT_NO"]);
                        PrevBilltoCode = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_BILLTOCODE"]);
                        PrevClaim_Amt = Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_CLAIM_AMOUNT"]);
                        PrevClaimPaid = Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_CLAIM_AMOUNT_PAID"]);

                        PrevPTReject = DSInv.Tables[0].Rows[0].IsNull("HIM_PT_REJECTED") == false ? 0 : Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_PT_REJECTED"]);
                        PrevCmpReject = DSInv.Tables[0].Rows[0].IsNull("HIM_CLAIM_REJECTED") == false ? 0 : Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_CLAIM_REJECTED"]);
                        PrevPTCrPaid = DSInv.Tables[0].Rows[0].IsNull("HIM_PT_CREDIT_PAID") == false ? 0 : Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_PT_CREDIT_PAID"]);
                        JourNo = DSInv.Tables[0].Rows[0].IsNull("HIM_JOUR_NO") == false ? "" : Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_JOUR_NO"]);
                        CmpInvoiceNo = DSInv.Tables[0].Rows[0].IsNull("HIM_CMP_INVOICE_NO") == false ? 0 : Convert.ToDecimal(DSInv.Tables[0].Rows[0]["HIM_CMP_INVOICE_NO"]);
                        TextFileWriting("Invoice.btnSave   15  ");

                        if (Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_PAYMENT_TYPE"]) == "Advance")
                        {

                            DataSet DSInvOther = new DataSet();
                            Criteria = "1=1 AND HIO_TYPE='Invoice' AND HIO_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIO_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                            DSInvOther = objInv.InvoiceOtherGet(Criteria);
                            if (DSInv.Tables[0].Rows.Count > 0)
                            {
                                decimal AdvAmt = 0;
                                Criteria = "1=1 AND HIO_TYPE='Advance' AND HIO_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND  HIO_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                                AdvAmt = DSInvOther.Tables[0].Rows[0].IsNull("HIO_ADVANCE_AMT") == false ? 0 : Convert.ToDecimal(DSInvOther.Tables[0].Rows[0]["HIO_ADVANCE_AMT"]);
                                objInv.InvoiceOtherUpdateAdvAmt(AdvAmt, Criteria);
                            }


                            Criteria = "1=1 AND HIO_TYPE='Invoice' AND HIO_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND  HIO_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                            objCom.fnDeleteTableData("HMS_INVOICE_OTHERS", Criteria);
                        }
                        TextFileWriting("Invoice.btnSave   16  ");

                        TextFileWriting("Invoice.btnSave   17  ");
                        // if( StrInvoiceType <> "PERFORMA" Then
                        Criteria = "1=1 AND HPB_TRANS_ID='" + txtInvoiceNo.Text.Trim() + "' AND  HPB_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                        objCom.fnDeleteTableData("HMS_PATIENT_BALANCE_HISTORY", Criteria);

                        Criteria = "1=1 AND hrt_receiptno='" + hidReceipt.Value + "'  AND HRT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                        objCom.fnDeleteTableData("HMS_RECEIPT_TRANSACTION", Criteria);

                        Criteria = "1=1 AND hrm_receiptno='" + hidReceipt.Value + "' AND  HRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                        objCom.fnDeleteTableData("HMS_RECEIPT_MASTER", Criteria);

                        IP_Invoice objInvUpdate = new IP_Invoice();
                        objInvUpdate.HIM_GROSS_TOTAL = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_GROSS_TOTAL"]);
                        objInvUpdate.HIM_HOSP_DISC_AMT = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_HOSP_DISC_AMT"]);
                        objInvUpdate.HIM_PT_AMOUNT = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_PT_AMOUNT"]);
                        objInvUpdate.HIM_CLAIM_AMOUNT = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_CLAIM_AMOUNT"]);
                        objInvUpdate.HIM_PAID_AMOUNT = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_PAID_AMOUNT"]);
                        objInvUpdate.HIM_PT_CREDIT = Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_PT_CREDIT"]);
                        Criteria = "1=1 and HPV_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HPV_PT_ID='" + Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_PT_ID"]) + "'  and HPV_DATE>='" + objCom.fnGetDate("MM/dd/yyyy 00:00:00") + "'  and HPV_DATE<='" + objCom.fnGetDate("MM/dd/yyyy 23:59:00") + "' AND  HPV_DR_ID='" + txtDoctorID.Text.Trim() + "'";
                        objInvUpdate.PatientVIsitUpdateInvoiceAmt(Criteria);


                        Criteria = "1=1  and HCB_TRANS_TYPE='INVOICE' AND HCB_TRANS_ID='" + txtInvoiceNo.Text.Trim() + "' AND  HCB_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                        if (hidCompanyID.Value != PrevBilltoCode && (PrevClaimPaid > 0 || PrevCmpReject > 0))
                        {
                            objInvUpdate.CompanyBalHistUpdate(Criteria);

                        }
                        else
                        {
                            Criteria = "1=1  and HCB_TRANS_TYPE='INVOICE' AND HCB_TRANS_ID='" + txtInvoiceNo.Text.Trim() + "' AND  HCB_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                            objCom.fnDeleteTableData("HMS_COMPANY_BALANCE_HISTORY", Criteria);

                        }

                        TextFileWriting("Invoice.btnSave   18  ");
                        if (Convert.ToString(Session["HSOM_ECLAIM"]).ToUpper() == "Y")
                        {

                            FieldNameWithValues = "HET_INVOICED='N'";
                            Criteria = "HET_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HET_CLAIM_ID='" + Convert.ToString(DSInv.Tables[0].Rows[0]["HIM_VOUCHER_NO"]) + "'";
                            objCom.fnUpdateTableData(FieldNameWithValues, "HMS_ECLAIM_TRANSACTION", Criteria);
                        }


                        Criteria = "1=1 AND HIT_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                        objCom.fnDeleteTableData("HMS_INVOICE_TRANSACTION", Criteria);


                        Criteria = "1=1 AND HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                        objCom.fnDeleteTableData("HMS_INVOICE_MASTER", Criteria);



                    }



                }


            }


            objInv.HIM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objInv.HIM_INVOICE_ID = txtInvoiceNo.Text.Trim(); //'T" & Trim(txtinvoiceno.Text) 
            objInv.HIM_INVOICE_TYPE = drpInvType.SelectedValue;
            objInv.HIM_DATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
            objInv.HIM_STATUS = drpStatus.SelectedValue;
            objInv.HIM_MASTER_INVOICE_NUMBER = txtInvoiceNo.Text.Trim();//txtinvoiceno.ToolTipText
            objInv.HIM_INVOICE_ORDER = txtTokenNo.Text.Trim();//txttokenNo.ToolTipText
            if (txtTokenNo.Text.Trim() != "")
                objInv.HIM_TOKEN_NO = txtTokenNo.Text.Trim();
            objInv.HIM_PT_ID = txtFileNo.Text.Trim();
            objInv.HIM_PT_NAME = txtName.Text.Trim();
            objInv.HIM_BILLTOCODE = hidCompanyID.Value;
            objInv.HPV_BUSINESSUNITCODE = txtJobnumber.Text;
            objInv.HPV_JOBNUMBER = txtJobnumber.ToolTip;

            objInv.HIM_SUB_INS_CODE = txtSubCompanyID.Text.Trim();
            objInv.HIM_INS_NAME = txtCompanyName.Text.Trim();
            objInv.HIM_SUB_INS_NAME = txtSubCompanyName.Text.Trim();
            objInv.HIM_PT_COMP = txtPTComp.Text.Trim();
            objInv.HIM_POLICY_NO = txtPolicyNo.Text.Trim();
            objInv.HIM_ID_NO = txtIdNo.Text;
            objInv.HIM_POLICY_EXP = txtPolicyExp.Text.Trim();
            objInv.HIM_INS_TRTNT_TYPE = hidInsTrtnt.Value; // "Service Type";
            objInv.HIM_DR_CODE = txtDoctorID.Text.Trim();
            objInv.HIM_DR_NAME = txtDoctorName.Text.Trim();

            objInv.HIM_ORDERING_DR_CODE = txtOrdClinCode.Text.Trim();
            objInv.HIM_ORDERING_DR_NAME = txtOrdClinName.Text.Trim();

            objInv.HIM_TRTNT_TYPE = drpTreatmentType.SelectedValue;
            objInv.HIM_REF_DR_CODE = txtRefDrId.Text;
            objInv.HIM_REF_DR_NAME = txtRefDrName.Text;

            objInv.HIM_GROSS_TOTAL = txtBillAmt1.Text.Trim();
            objInv.HIM_HOSP_DISC_TYPE = drpHospDiscType.SelectedValue;
            objInv.HIM_HOSP_DISC = txtHospDisc.Text.Trim();
            objInv.HIM_HOSP_DISC_AMT = txtHospDiscAmt.Text.Trim();
            objInv.HIM_CO_INS_TYPE = hidCoInsType.Value;
            objInv.HIM_CO_INS_AMOUNT = "0";// txtCoInsTotal.Text.Trim();
            objInv.HIM_DEDUCTIBLE = txtDeductible.Text.Trim();
            objInv.HIM_NET_AMOUNT = txtBillAmt3.Text.Trim();
            objInv.HIM_CLAIM_AMOUNT = txtClaimAmt.Text.Trim();
            objInv.HIM_CLAIM_AMOUNT_PAID = Convert.ToString(PrevClaimPaid);
            objInv.HIM_CLAIM_REJECTED = Convert.ToString(PrevCmpReject);
            objInv.HIM_PT_AMOUNT = txtBillAmt4.Text.Trim();
            objInv.HIM_PT_CREDIT = txtPTCredit.Text.Trim();
            objInv.HIM_PT_CREDIT_PAID = Convert.ToString(PrevPTCrPaid);
            // objInv.HIM_PT_REJECTED =
            objInv.HIM_SPL_DISC = txtSplDisc.Text.Trim();
            objInv.HIM_PAID_AMOUNT = txtPaidAmount.Text.Trim();
            objInv.HIM_PAYMENT_TYPE = drpPayType.SelectedValue;
            if (drpPayType.SelectedItem.Text == "Credit Card" || drpPayType.SelectedItem.Text == "Debit Card")
            {
                objInv.HIM_CC_TYPE = drpccType.SelectedValue;
            }

            objInv.HIM_CC_NAME = txtCCHolder.Text.Trim();
            objInv.HIM_CC_NO = txtCCNo.Text.Trim();
            objInv.HIM_REF_NO = txtCCRefNo.Text.Trim();
            objInv.HIM_CHEQ_NO = txtDcheqno.Text.Trim();
            objInv.HIM_CASH_AMT = txtCashAmt.Text.Trim();
            objInv.HIM_CC_AMT = txtCCAmt.Text.Trim();
            objInv.HIM_CHEQUE_AMT = txtChqAmt.Text.Trim();

            objInv.PHASECODE = "";
            if (drpPayType.SelectedItem.Text == "Cheque")
            {
                objInv.HIM_BANK_NAME = drpBank.SelectedValue;
            }
            else
            {
                objInv.HIM_BANK_NAME = "";
            }
            objInv.HIM_CHEQ_DATE = txtDcheqdate.Text.Trim();
            if (drpPayType.SelectedItem.Text == "Multi Currency")
            {
                objInv.HIM_CUR_TYP = drpCurType.SelectedValue;
            }
            else
            {
                objInv.HIM_CUR_TYP = "";
            }
            objInv.HIM_CUR_RATE = txtConvRate.Text.Trim(); ;
            objInv.HIM_CUR_VALUE = Convert.ToString(Convert.ToDecimal(txtcurValue.Text.Trim() != "" ? txtcurValue.Text.Trim() : "0") - Convert.ToDecimal(LblMBalAmt.Text.Trim() != "" ? LblMBalAmt.Text.Trim() : "0"));
            objInv.HIM_CUR_RCVD_AMT = txtRcvdAmount.Text.Trim();
            objInv.HIM_RECEIPT_NO = hidReceipt.Value;
            objInv.HIM_COMMIT_FLAG = "N";
            objInv.HIM_VOUCHER_NO = TxtVoucherNo.Text.Trim();
            objInv.HIM_POLICY_TYPE = txtPlanType.Text.Trim();
            objInv.HIM_REMARKS = txtRemarks.Text.Trim();
            objInv.HIM_TYPE = hidType.Value != "PHY" ? "INV" : hidType.Value;
            objInv.HIM_COMMISSION_TYPE = drpCommissionType.SelectedValue;
            objInv.HIM_CMP_INVOICE_NO = Convert.ToString(CmpInvoiceNo);
            objInv.HIM_MERGEINVOICES = txtMergeInvoices.Text.Trim();
            objInv.HIM_ADV_AMT = txtAdvAmt.Text.Trim();
            objInv.HIM_TRTNT_TYPE_01 = txtICDDesc.Text.Trim();
            objInv.HIM_TRTNT_Type_code = txtICdCode.Text.Trim();

            if (chkReadyforEclaim.Checked == true)
            {
                objInv.HIM_READYFORECLAIM = "1";
            }
            else
            {
                objInv.HIM_READYFORECLAIM = "0";
            }
            objInv.HIM_EMR_ID = Convert.ToString(Session["EMR_ID"]);
            objInv.HIM_AUTHID = txtPriorAuthorizationID.Text.Trim();
            objInv.HIM_CC = hidccText.Value;


            if (ChkTopupCard.Checked == true)
            {
                objInv.HIM_TOPUP = "1";
            }
            else
            {
                objInv.HIM_TOPUP = "0";
            }
            objInv.HIM_TOPUP_AMT = TxtTopupAmt.Text.Trim();

            if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
            {
                objInv.NewFlag = "A";
            }
            else
            {
                objInv.NewFlag = "M";
            }
            objInv.IsTopupInv = "N";
            objInv.UserID = Convert.ToString(Session["User_ID"]);



            string strInvoiceID = "0";
            strInvoiceID = objInv.IPInvoiceMasterAdd();
            txtInvoiceNo.Text = strInvoiceID;


            if (ChkTopupCard.Checked == true && Convert.ToBoolean(ViewState["NewFlag"]) == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
            {

                objInv.HIM_INVOICE_ID = "T" + txtInvoiceNo.Text.Trim();
                objInv.HIM_BILLTOCODE = Convert.ToString(ViewState["mTopupBillToCode"]);
                objInv.HIM_SUB_INS_CODE = Convert.ToString(ViewState["mTopupInsCode"]);
                objInv.HIM_INS_NAME = Convert.ToString(ViewState["mTopupBillToCompName"]);
                objInv.HIM_SUB_INS_NAME = Convert.ToString(ViewState["mTopupInsName"]);
                objInv.HIM_POLICY_NO = Convert.ToString(ViewState["mTopupPolicyNo"]);
                objInv.HIM_ID_NO = Convert.ToString(ViewState["mTopupIDNo"]);
                objInv.HIM_POLICY_EXP = Convert.ToString(ViewState["mTopupExpiry"]);
                objInv.IsTopupInv = "Y";

                string strTInvoiceID1 = "0";
                strTInvoiceID1 = objInv.IPInvoiceMasterAdd();
            }



            TextFileWriting("Invoice.btnSave   31  ");


            Criteria = "1=1 AND  HIO_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIO_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            objCom.fnDeleteTableData("HMS_INVOICE_OBSERVATION", Criteria);
            TextFileWriting("Invoice.btnSave   32  ");


            Boolean mAdvOnly = false;
            for (int intCurRow = 0; intCurRow < gvInvoiceTrans.Rows.Count; intCurRow++)
            {
                Label lblgvSOTransID = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvSOTransID");
                TextBox txtTransDate = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtTransDate");

                Label lblgvCatID = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvCatID");
                TextBox txtgvServCode = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvServCode");
                TextBox txtServDesc = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtServDesc");
                TextBox txtFee = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtFee");

                Label lblDiscType = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblDiscType");

                DropDownList drpgvDiscType = (DropDownList)gvInvoiceTrans.Rows[intCurRow].FindControl("drpgvDiscType");

                TextBox txtDiscAmt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtDiscAmt");
                TextBox txtgvQty = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvQty");
                TextBox txtHitAmount = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtHitAmount");
                TextBox txtDeduct = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtDeduct");
                Label lblCoInsType = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblCoInsType");
                TextBox txtCoInsAmt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtCoInsAmt");
                TextBox txtgvCoInsTotal = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCoInsTotal");

                TextBox txtgvCompType = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCompType");
                TextBox txtgvCompAmount = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCompAmount");
                TextBox txtgvCompTotal = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCompTotal");

                TextBox txtgvDrCode = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvDrCode");
                TextBox txtgvDrName = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvDrName");
                Label lblServType = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblServType");

                Label lblCoverd = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblCoverd");
                Label lblgvRefNo = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvRefNo");
                Label lblgvTeethNo = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvTeethNo");


                TextBox txtHaadCode = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtHaadCode");
                TextBox txtHaadDesc = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtHaadDesc");

                TextBox txtTypeValue = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtTypeValue");

                TextBox txtgvCostPrice = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvCostPrice");


                TextBox txtgvAuthID = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvAuthID");

                Label lblgvServCost = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvServCost");
                Label lblgvAdjDedAmt = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblgvAdjDedAmt");

                TextBox txtgcStartDt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgcStartDt");


                TextBox txtObsType = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsType");
                TextBox txtObsCode = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsCode");
                TextBox txtObsDesc = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsDesc");
                TextBox txtObsValue = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsValue");
                TextBox txtObsValueType = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtObsValueType");

                objInv.HIT_TRANS_DATE = txtTransDate.Text.Trim();
                objInv.HIT_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objInv.HIT_INVOICE_ID = strInvoiceID;
                objInv.HIT_CAT_ID = lblgvCatID.Text.Trim();
                objInv.HIT_SERV_CODE = txtgvServCode.Text.Trim();
                objInv.HIT_DESCRIPTION = txtServDesc.Text.Trim();
                objInv.HIT_FEE = txtFee.Text.Trim() != "" ? txtFee.Text.Trim() : "0"; ;
                objInv.HIT_DISC_TYPE = drpgvDiscType.SelectedValue;//lblDiscType.Text.Trim();
                objInv.HIT_DISC_AMT = txtDiscAmt.Text.Trim();// 
                objInv.HIT_QTY = txtgvQty.Text.Trim() != "" ? txtgvQty.Text.Trim() : "1"; ;
                objInv.HIT_AMOUNT = txtHitAmount.Text.Trim() != "" ? txtHitAmount.Text.Trim() : "0";
                objInv.HIT_DEDUCT = txtDeduct.Text.Trim() != "" ? txtDeduct.Text.Trim() : "0";
                objInv.HIT_CO_INS_TYPE = lblCoInsType.Text.Trim();
                objInv.HIT_CO_INS_AMT = txtCoInsAmt.Text.Trim() != "" ? txtCoInsAmt.Text.Trim() : "0";
                objInv.HIT_CO_TOTAL = txtgvCoInsTotal.Text.Trim() != "" ? txtgvCoInsTotal.Text.Trim() : "0";

                objInv.HIT_COMP_TYPE = txtgvCompType.Text.Trim();
                objInv.HIT_COMP_AMT = txtgvCompAmount.Text.Trim() != "" ? txtgvCompAmount.Text.Trim() : "0";
                objInv.HIT_COMP_TOTAL = txtgvCompTotal.Text.Trim();

                objInv.HIT_DR_CODE = txtgvDrCode.Text.Trim() != "" ? txtgvDrCode.Text.Trim() : txtDoctorID.Text.Trim();
                objInv.HIT_DR_NAME = txtgvDrName.Text.Trim() != "" ? txtgvDrName.Text.Trim() : txtDoctorName.Text.Trim();
                objInv.HIT_COVERED = lblCoverd.Text.Trim();
                objInv.HIT_SERV_TYPE = lblServType.Text.Trim();


                //objInv.HIT_PT_AMOUNT=
                //objInv.HIT_DISC_AMOUNT=
                //objInv.HIT_NET_AMOUNT=

                objInv.HIT_SLNO = Convert.ToString(intCurRow + 1);
                objInv.HIT_REFNO = lblgvRefNo.Text;
                objInv.HIT_TEETHNO = lblgvTeethNo.Text;
                objInv.HIT_HAAD_CODE = txtHaadCode.Text.Trim();
                objInv.HIT_TYPE_VALUE = txtTypeValue.Text.Trim();
                objInv.HIT_COMMISSION_TYPE = drpCommissionType.SelectedValue;
                objInv.HIT_COSTPRICE = txtgvCostPrice.Text;
                objInv.HIT_AUTHORIZATIONID = txtgvAuthID.Text.Trim();
                objInv.HIT_SERVCOST = lblgvServCost.Text;
                objInv.HIT_ADJ_DEDUCT_AMOUNT = lblgvAdjDedAmt.Text;

                if (txtgcStartDt.Text.Trim() == "")
                {
                    objInv.HIT_STARTDATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
                }
                else
                {
                    objInv.HIT_STARTDATE = txtgcStartDt.Text.Trim();
                }
                //objInv.HIT_PHARMACY_CODE=
                //objInv.HIT_TREATMENT_ID=
                //objInv.HIT_RAD_NUMBER=
                //objInv.UTIL_EMNUPDATE=
                //objInv.HIT_addSrno=
                if (txtgvCoInsTotal.Text.Trim() != "")
                {
                    if (ChkTopupCard.Checked == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
                    {
                        objInv.HIT_TOPUP_AMT = txtgvCoInsTotal.Text.Trim();
                    }
                    else
                    {
                        objInv.HIT_TOPUP_AMT = "0";
                    }
                }
                //objInv.HIT_CLAIM_BALANCE=
                objInv.IPInvoiceTransAdd();

                if (ChkTopupCard.Checked == true && Convert.ToBoolean(ViewState["NewFlag"]) == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
                {
                    objInv.HIT_INVOICE_ID = "T" + txtInvoiceNo.Text.Trim();
                    objInv.HIT_CO_TOTAL = txtgvCompTotal.Text.Trim();


                    objInv.HIT_COMP_AMT = txtCoInsAmt.Text.Trim() != "" ? txtCoInsAmt.Text.Trim() : "0";
                    objInv.HIT_COMP_TOTAL = txtgvCoInsTotal.Text.Trim() != "" ? txtgvCoInsTotal.Text.Trim() : "0";
                    objInv.HIT_TOPUP_AMT = Convert.ToString(txtHitAmount.Text.Trim() != "" ? Convert.ToDecimal(txtHitAmount.Text.Trim()) : 0 - Convert.ToDecimal(txtgvCoInsTotal.Text.Trim()));


                    objInv.IPInvoiceTransAdd();

                }

                objCom = new CommonBAL();


                objCom.fnUpdateTableData(" IDST_USED_STATUS=1 ", "IP_DR_SALESORDER_TRANSACTION", " IDST_TRANS_ID=" + lblgvSOTransID.Text);

                TextFileWriting("Invoice.btnSave   33  ");
                if (txtObsType.Text.Trim() == "LOINC" || txtObsType.Text.Trim() == "Universal Dental" || txtObsType.Text.Trim() == "Result")
                {
                    if (txtObsCode.Text.Trim() != "")
                    {
                        objInv.HIO_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                        objInv.HIO_INVOICE_ID = txtInvoiceNo.Text.Trim();
                        objInv.HIO_SERV_CODE = txtgvServCode.Text.Trim();
                        objInv.HIO_TYPE = txtObsType.Text.Trim();
                        objInv.HIO_CODE = txtObsCode.Text.Trim();
                        objInv.HIO_SERV_SLNO = Convert.ToString(intCurRow + 1);
                        objInv.HIO_DESCRIPTION = txtObsDesc.Text.Trim();
                        objInv.HIO_VALUE = txtObsValue.Text.Trim();
                        objInv.HIO_VALUETYPE = txtObsValueType.Text.Trim();
                        objInv.InvoiceObserAdd();


                        if (ChkTopupCard.Checked == true && Convert.ToBoolean(ViewState["NewFlag"]) == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
                        {
                            objInv.HIO_INVOICE_ID = "T" + txtInvoiceNo.Text.Trim();
                            objInv.InvoiceObserAdd();

                        }


                    }
                }
                if (Convert.ToString(Session["HSOM_ECLAIM"]).ToUpper() == "Y")
                {

                    FieldNameWithValues = " HET_INVOICED='Y' ";
                    Criteria = " HET_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HET_CLAIM_ID='" + TxtVoucherNo.Text.Trim() + "' and HET_CODE='" + txtHaadCode.Text.Trim() + "' AND HET_TYPE='" + txtTypeValue.Text.Trim() + "'";
                    objCom.fnUpdateTableData(FieldNameWithValues, "HMS_ECLAIM_TRANSACTION", Criteria);

                }



                if (txtgvServCode.Text.Trim() == Convert.ToString(ViewState["HSOM_ADV_PAYMENT_SERV_CODE"])) mAdvOnly = true;

            }


            TextFileWriting("Invoice.btnSave   34  ");
            if (ChkTopupCard.Checked == false)
            {
                fnSaveReceipt(PrevPTCrPaid);
            }

            if (Convert.ToString(Session["HSOM_ECLAIM"]).ToUpper() == "Y")
            {
                SaveClaimDetail();
            }

            TextFileWriting("Invoice.btnSave   35  ");



            if (mAdvOnly == true)
            {
                //Criteria = "1=1 AND  HIO_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND  HIO_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HIO_TYPE='Advance'";
                // objCom.fnDeleteTableData("hms_invoice_others", Criteria);
                //fnSaveOtherDetails "Advance", txtinvoiceno.Text, txtPaidAmount.Text, 0
            }
            else if (drpPayType.SelectedValue == "Advance")
            {
                // fnSaveOtherDetails "Invoice", txtinvoiceno.Text, TxtAdvAmt.Text, 0  
            }

            TextFileWriting("Invoice.btnSave   36  ");

            decimal decTopUpAmt = 0;
            if (TxtTopupAmt.Text.Trim() != "")
            {
                decTopUpAmt = Convert.ToDecimal(TxtTopupAmt.Text.Trim());
            }
            objInv.HPB_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objInv.HPB_TRANS_ID = txtInvoiceNo.Text.Trim();
            objInv.HPB_DATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
            objInv.HPB_PT_ID = txtFileNo.Text.Trim();
            objInv.HPB_PATIENT_TYPE = drpInvType.SelectedValue;
            objInv.HPB_INV_AMOUNT = txtBillAmt4.Text.Trim();
            objInv.HPB_PAID_AMT = txtPaidAmount.Text.Trim();
            objInv.HPB_REJECTED_AMT = Convert.ToString(PrevPTReject);
            objInv.HPB_BALANCE_AMT = Convert.ToString((Convert.ToDecimal(txtBillAmt4.Text.Trim()) - Convert.ToDecimal(txtPaidAmount.Text.Trim()) - PrevPTReject));// - decTopUpAmt
            objInv.UserID = Convert.ToString(Session["User_ID"]);
            objInv.PatientBalHisAdd();

            if (ChkTopupCard.Checked == true && Convert.ToBoolean(ViewState["NewFlag"]) == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
            {
                objInv.HPB_TRANS_ID = "T" + txtInvoiceNo.Text.Trim();
                objInv.HPB_INV_AMOUNT = TxtTopupAmt.Text.Trim();
                objInv.HPB_PAID_AMT = "0";
                objInv.HPB_REJECTED_AMT = "0";
                objInv.HPB_BALANCE_AMT = TxtTopupAmt.Text.Trim();//"0";//
                objInv.PatientBalHisAdd();
            }

            TextFileWriting("Invoice.btnSave   37  ");

            TextFileWriting("Invoice.btnSave   38  ");
            if (drpInvType.SelectedValue == "Credit" && txtClaimAmt.Text != "" && txtClaimAmt.Text != "0")
            {


                objInv.HCB_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objInv.HCB_COMP_TYPE = hidInsType.Value;
                objInv.HCB_COMP_ID = hidCompanyID.Value;
                objInv.HCB_TRANS_ID = txtInvoiceNo.Text.Trim();
                objInv.HCB_DATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
                objInv.HCB_AMT = txtClaimAmt.Text.Trim();
                objInv.HCB_PAID_AMT = Convert.ToString(PrevClaimPaid);
                objInv.HCB_REJECT_AMT = Convert.ToString(PrevCmpReject);
                objInv.HCB_BALANCE_AMT = Convert.ToString(Convert.ToDecimal(txtClaimAmt.Text.Trim()) - (PrevClaimPaid + PrevCmpReject));
                objInv.UserID = Convert.ToString(Session["User_ID"]);
                objInv.CompanytBalHisAdd();

                TextFileWriting("Invoice.btnSave   39  ");


                if (ChkTopupCard.Checked == true && Convert.ToBoolean(ViewState["NewFlag"]) == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
                {


                    objInv.HCB_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objInv.HCB_COMP_TYPE = hidInsType.Value;
                    objInv.HCB_COMP_ID = hidCompanyID.Value;
                    objInv.HCB_TRANS_ID = "T" + txtInvoiceNo.Text.Trim();
                    objInv.HCB_DATE = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();
                    objInv.HCB_AMT = TxtTopupAmt.Text.Trim();
                    objInv.HCB_PAID_AMT = "0";
                    objInv.HCB_REJECT_AMT = "0";
                    objInv.HCB_BALANCE_AMT = TxtTopupAmt.Text.Trim();
                    objInv.UserID = Convert.ToString(Session["User_ID"]);
                    objInv.CompanytBalHisAdd();

                }

            }
            TextFileWriting("Invoice.btnSave   40  ");

            FieldNameWithValues = "IPM_INVOICENO='" + txtInvoiceNo.Text.Trim() + "'";
            Criteria = "IPM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND IPM_PT_ID='" + txtFileNo.Text.Trim() + "'  AND CONVERT(VARCHAR(10),IPM_DATE,103)='" + txtInvDate.Text.Trim() + "' AND IPM_DR_CODE = '" + txtDoctorID.Text.Trim() + "'";
            objCom.fnUpdateTableData(FieldNameWithValues, "IP_PT_MASTER", Criteria);



            FieldNameWithValues = "IAS_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";
            Criteria = "IAS_BRANCHID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND IAS_PT_ID='" + txtFileNo.Text.Trim() + "'  AND IAS_ADMISSION_NO = '" + Convert.ToString(Session["IAS_ADMISSION_NO"]) + "'";
            objCom.fnUpdateTableData(FieldNameWithValues, "IP_ADMISSION_SUMMARY", Criteria);


            TextFileWriting("Invoice.btnSave   41  ");


            FieldNameWithValues = " HIM_SERVCOST_TOTAL =  (SELECT     SUM(HIT_COSTPRICE * HIT_QTY) AS TotalCost From HMS_INVOICE_TRANSACTION    WHERE      (HIT_INVOICE_ID = HMS_INVOICE_MASTER.HIM_INVOICE_ID) AND (HIT_BRANCH_ID = HMS_INVOICE_MASTER.HIM_BRANCH_ID)) ";
            Criteria = "HIM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";
            objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_MASTER", Criteria);


            TextFileWriting("Invoice.btnSave   42  ");

            TextFileWriting("Invoice.btnSave   43  ");
            if (ChkTopupCard.Checked == true && Convert.ToBoolean(ViewState["NewFlag"]) == true && Convert.ToDecimal(TxtTopupAmt.Text.Trim()) > 0)
            {
                FieldNameWithValues = " HIM_SERVCOST_TOTAL =  (SELECT     SUM(HIT_COSTPRICE * ISNULL(HIT_QTY,1)) AS TotalCost From HMS_INVOICE_TRANSACTION    WHERE      (HIT_INVOICE_ID = HMS_INVOICE_MASTER.HIM_INVOICE_ID) AND (HIT_BRANCH_ID = HMS_INVOICE_MASTER.HIM_BRANCH_ID)) ";
                Criteria = "HIM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND HIM_INVOICE_ID='T" + txtInvoiceNo.Text.Trim() + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "HMS_INVOICE_MASTER", Criteria);
            }

            TextFileWriting("Invoice.btnSave   44 ");


            fnPrint(txtInvoiceNo.Text.Trim());

        FunEnd: ;
            return isError;
        }

        void ChekcProcessCompleted()
        {

            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";    //BAL.GlobalValues.HomeCare_ID  + "' ";
            //Criteria += " AND EPM_STATUS = 'Y' ";

            DataSet ds = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            ds = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["EPM_STATUS"] = Convert.ToString(ds.Tables[0].Rows[0]["EPM_STATUS"]);
                ViewState["EPM_VIP"] = Convert.ToString(ds.Tables[0].Rows[0]["EPM_VIP"]);
                ViewState["EPM_ROYAL"] = Convert.ToString(ds.Tables[0].Rows[0]["EPM_ROYAL"]);
                ViewState["EPM_START_DATE"] = Convert.ToString(ds.Tables[0].Rows[0]["EPM_START_DATEDesc"]);

            }


        }

        void ProcessCompleted()
        {
            string strDate = "", strTime = ""; ;
            strDate = objCom.fnGetDateddMMyyyy();
            strTime = objCom.fnGetDate("hh:mm:ss");

            ChekcProcessCompleted();

            if (Convert.ToString(ViewState["EPM_STATUS"]).ToUpper() != "Y" && hidCompleteProc.Value == "true")
            {
                EMR_PTMasterBAL objPTMaster = new EMR_PTMasterBAL();
                objPTMaster.EPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPTMaster.EPM_ID = Convert.ToString(Session["EMR_ID"]);

                objPTMaster.EPM_CRITICAL_NOTES = "";
                objPTMaster.ProcessCompleted = "Completed";
                objPTMaster.EPM_VIP = Convert.ToString(ViewState["EPM_VIP"]);// chkVIP.Checked == true ? "1" : "0";
                objPTMaster.EPM_ROYAL = Convert.ToString(ViewState["EPM_ROYAL"]);//chkRoyal.Checked == true ? "1" : "0";
                objPTMaster.EPM_START_DATE = Convert.ToString(ViewState["EPM_START_DATE"]);// lblBeginDt.Text;

                //DateTime strEndmDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                //string Time = DateTime.Now.ToString("HH:mm:ss tt");
                // objPTMaster.EPM_END_DATE = strEndmDate.ToString("dd/MM/yyyy") + " " + Time;
                objPTMaster.EPM_END_DATE = strDate + " " + strTime;


                objPTMaster.EPM_DR_CODE = Convert.ToString(Session["HPV_DR_ID"]);
                objPTMaster.EPM_DATE = Convert.ToString(Session["HPV_DATE"]);//  lblEMRDate.Text ;

                objPTMaster.AddPatientProcess();
            }
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_INVOICE' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {
                btnNew.Visible = false;
                btnSave.Visible = false;
                btnDelete.Visible = false;
                btnClear.Visible = false;

                btnServiceAdd.Visible = false;
                btnImportSO.Visible = false;
                btnInvDiagAdd.Visible = false;

                btnPrint.Visible = false;

            }

            if (strPermission == "5")
            {
                btnDelete.Visible = false;

            }

            if (strPermission == "7")
            {
                btnNew.Visible = false;
                btnSave.Visible = false;

                btnServiceAdd.Visible = false;
                btnImportSO.Visible = false;
                btnInvDiagAdd.Visible = false;

                btnPrint.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }

                hidCompleteProc.Value = Convert.ToString(Request.QueryString["CompleteProc"]);

                //if (hidCompleteProc.Value == "true")
                //{
                //    ProcessCompleted();
                //}

                try
                {


                    strSessionBranchId = Convert.ToString(Session["Branch_ID"]);
                    BindSystemOption();
                    BindScreenCustomization();
                    fnSetdefaults();

                    if (Convert.ToString(Session["DefaultType"]).ToLower() == "true")
                    {
                        drpInvType.SelectedIndex = 1;
                    }
                    else
                    {
                        drpInvType.SelectedIndex = 0;
                    }

                    /// string PatientId = "";
                    // PatientId = Convert.ToString(Request.QueryString["PatientId"]);

                    //if (PatientId != " " && PatientId != null)
                    //{
                    //    txtSrcFileNo.Text = Convert.ToString(PatientId);
                    //    PatientDataBind();
                    //}
                    //BindDoctor();
                    objCom = new CommonBAL();
                    string strDate = "", strTime = ""; ;
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("hh:mm:ss");

                    txtInvDate.Text = Convert.ToString(Session["IAS_DATE"]);// strDate;
                    txtInvTime.Text = strTime;

                    txtClmStartDate.Text = Convert.ToString(Session["IAS_DATE"]);// strDate;
                    txtClmFromTime.Text = strTime.Substring(0, 5);

                    txtClmEndDate.Text = Convert.ToString(Session["IAS_DATE"]);// strDate;
                    txtClmToTime.Text = strTime.Substring(0, 5);

                    if (GlobalValues.FileDescription == "SYNERGY")
                    {
                        // PICTrtntType01.Visible = True
                        txtICdCode.Visible = true;
                        txtICDDesc.Visible = true;
                        drpTreatmentType.Visible = false;

                    }

                    txtInvoiceNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "INVOICE");
                    BindBank();
                    BindCurrency();
                    BindPlanType();
                    BuildeInvoiceClaims();
                    BuildeInvoiceTrans();



                    string PatientId = "";
                    PatientId = Convert.ToString(Request.QueryString["PatientId"]);
                    if (PatientId != " " && PatientId != null)
                    {

                        txtFileNo.Text = Convert.ToString(PatientId);
                        PatientDataBind();

                    }
                    txtFileNo.Text = Convert.ToString(Session["EMR_PT_ID"]);
                    txtFileNo_TextChanged("txtFileNo", new EventArgs());


                    //txtInvoiceNo_TextChanged("txtInvoiceNo", new EventArgs());

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.Page_Load");
                    TextFileWriting(ex.Message.ToString());

                }
            }
        }

        protected void txtInvoiceNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (BindInvoice() == true)
                {
                    TextFileWriting(" BindInvoice() completed");
                    BindInvoiceTrans();
                    TextFileWriting(" BindInvoiceTrans() completed");
                    BindTempInvoiceTrans();
                    TextFileWriting(" BindTempInvoiceTrans() completed");
                    BindInvoiceClaimDtls();
                    BindTempInvoiceClaims();

                    AmountCalculation();
                    TextFileWriting(" AmountCalculation() completed");
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtInvoiceNo_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();
                if (TopUpCardChecking(txtFileNo.Text.Trim()) == true)
                {
                    ChkTopupCard.Checked = true;

                }
                else
                {
                    ChkTopupCard.Checked = false;

                    TxtTopupAmt.Text = "0";
                }

                PatientDataBind();
                /*
                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    IsPatientBilledToday();
                }
                 * 
                 * */
                Boolean bolVisitAvail = false;

                //bolVisitAvail = PatientVisitBind();

                if (Convert.ToString(ViewState["HSOM_INV_PTWAITING_ONLY"]).ToUpper() == "Y" && Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    bolVisitAvail = IsPatientVisitAvailable();
                    if (bolVisitAvail == false)
                    {
                        Clear();
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('This Patient is not found in PT Waiting List.');", true);

                    }

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }


        }


        protected void txtInvDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();

                if (TopUpCardChecking(txtFileNo.Text.Trim()) == true)
                {
                    ChkTopupCard.Checked = true;

                }
                else
                {
                    ChkTopupCard.Checked = false;

                    TxtTopupAmt.Text = "0";
                }

                PatientDataBind();
                /*
                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    IsPatientBilledToday();
                }
                 * */
                Boolean bolVisitAvail = false;

                //bolVisitAvail = PatientVisitBind();

                if (Convert.ToString(ViewState["HSOM_INV_PTWAITING_ONLY"]).ToUpper() == "Y" && Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    bolVisitAvail = IsPatientVisitAvailable();
                    if (bolVisitAvail == false)
                    {
                        Clear();
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('This Patient is not found in PT Waiting List.');", true);

                    }

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }


        }



        protected void txtSubCompanyID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                objCom = new CommonBAL();
                string Criteria = " 1=1 ";
                DS = new DataSet();

                string strName = txtSubCompanyID.Text.Trim();
                string[] arrName = strName.Split('~');

                if (arrName.Length > 1)
                {
                    txtSubCompanyID.Text = arrName[0].Trim();
                    txtCompanyName.Text = arrName[1].Trim();
                    Criteria += " and HCM_COMP_ID='" + arrName[0].Trim() + "' ";
                }
                else
                {
                    Criteria += " and HCM_COMP_ID='" + txtSubCompanyID.Text.Trim() + "'";
                }

                DS = objCom.GetCompanyMaster(Criteria);

                if (DS.Tables[0].Rows.Count > 0)
                {
                    txtCompanyName.Text = Convert.ToString(DS.Tables[0].Rows[0]["HCM_NAME"]).Trim();
                }
                else
                {
                    txtCompanyName.Text = "";
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtSubCompanyID_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }

        }

        protected void txtDoctorID_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string strDrCode = "", strDrName = "";
                strDrCode = txtDoctorID.Text.Trim();

                GetStaffName(strDrCode, out strDrName);
                txtDoctorName.Text = strDrName;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtDoctorID_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }

        }

        protected void txtOrdClinCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string strDrCode = "", strDrName = "";
                strDrCode = txtOrdClinCode.Text.Trim();

                GetRefDrName(strDrCode, out strDrName);
                txtOrdClinName.Text = strDrName;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtDoctorID_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }

        }


        protected void btnInvDiagAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable DT = new DataTable();

                DT = (DataTable)ViewState["InvoiceClaims"];

                DataRow objrow;
                objrow = DT.NewRow();
                objrow["HIC_ICD_CODE"] = txtDiagCode.Text.Trim();
                objrow["HIC_ICD_DESC"] = txtDiagName.Text.Trim();
                objrow["HIC_ICD_TYPE"] = drpDiagType.SelectedValue;


                DT.Rows.Add(objrow);

                ViewState["InvoiceClaims"] = DT;
                BindTempInvoiceClaims();

                txtDiagCode.Text = "";
                txtDiagName.Text = "";
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnInvDiagAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtServCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                /*
                Boolean IsConsultExist = false;

                for (int i = 0; i < gvInvoiceTrans.Rows.Count; i++)
                {

                    Label lblServType1 = (Label)gvInvoiceTrans.Rows[i].FindControl("lblServType");

                    if (lblServType1.Text == "C")
                    {
                        IsConsultExist = true;
                    }

                }

                */
                string ServCode = txtServCode.Text.Trim();

                DataSet DSServ = new DataSet();
                string strServType = "";

                DSServ = GetServiceMasterName("HSM_SERV_ID", ServCode);
                if (DSServ.Tables[0].Rows.Count > 0)
                {
                    //ServID = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_SERV_ID"]);
                    //ServName = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_NAME"]);
                    //HSM_TYPE = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TYPE"]);
                    //CatID = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CAT_ID"]);
                    //CatTyp = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPE"]);
                    //if (DSServ.Tables[0].Rows[0].IsNull("HSM_CODE_TYPE") == false && Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPE"]) != "")
                    //    Fee = Convert.ToDecimal(DSServ.Tables[0].Rows[0]["HSM_FEE"]);

                    //  obsCode = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_CODE"]);
                    // obsDesc = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CAT_ID"]);
                    // obsvalueType = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_VALUETYPE"]);

                    hidHaadID.Value = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_SERV_ID"]);
                    txtServName.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_NAME"]);
                    hidCatID.Value = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CAT_ID"]);
                    hidCatType.Value = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPE"]);

                    strServType = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TYPE"]);
                    /*
                    if (IsConsultExist == true && strServType == "C" && drpInvType.SelectedValue == "Credit")
                    {
                        txtServCode.Text = "";
                        txtServName.Text = "";
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Sorry, You can invoice only one Consultation for Credit Patients.');", true);
                    }
                     * */

                }
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtServCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        void fnServiceAdd(string ServCode, string ServName)
        {
            objCom = new CommonBAL();
            IP_Invoice objInv = new IP_Invoice();
            string Criteria = " 1=1 ";
            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["InvoiceTrans"];
            DataRow objrow;
            objrow = DT.NewRow();
            objrow["SOTransID"] = "0";
            objrow["HIT_TRANS_DATEDesc"] = txtInvDate.Text;
            objrow["HIT_SERV_CODE"] = ServCode;
            objrow["HIT_Description"] = ServName;
            objrow["HIT_QTY"] = 1;
            objrow["HIT_DISC_AMT"] = 0;
            objrow["HIT_DEDUCT"] = 0;
            objrow["HIT_CO_INS_AMT"] = 0;
            objrow["HIT_DR_CODE"] = txtDoctorID.Text;
            objrow["HIT_DR_NAME"] = txtDoctorName.Text;

            Int32 intCurRow = 0;
            intCurRow = gvInvoiceTrans.Rows.Count - 1;

            string DiscType = "$", Co_Type = "%", ServCat = "", ServTrnt = "", Covered = "", HaadCode = "", ObsNeeded = "N", ObsType = "", ObsCode = "", ObsValue = "", ObsValueType = "";
            decimal DiscAmount = 0, ServFee = 0, Co_Ins = 0, ServiceCost = 0, ObsConversion = 0;
            DataSet DSServ = new DataSet();

            DSServ = GetServiceMasterName("HSM_SERV_ID", ServCode);


            if (DSServ.Tables[0].Rows.Count > 0)
            {

                ServFee = Convert.ToDecimal(DSServ.Tables[0].Rows[0]["HSM_FEE"]);
                ServCat = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CAT_ID"]);
                ServTrnt = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TYPE"]);

                ServiceCost = Convert.ToDecimal(Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_COST"]) != "" ? Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_COST"]) : "0");


                HaadCode = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_HAAD_CODE"]);
                ObsNeeded = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBSERVATION_NEEDED"]);
                if (ObsNeeded == "Y")
                {
                    ObsCode = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_CODE"]);//txtObsCode.Text = 
                    ObsValueType = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_VALUETYPE"]);//txtObsValue.Text = 
                    ObsConversion = DSServ.Tables[0].Rows[0].IsNull("HSM_OBS_CONVERSION") == true || Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_CONVERSION"]) == "" ? 0 : Convert.ToDecimal(DSServ.Tables[0].Rows[0]["HSM_OBS_CONVERSION"]);

                    if (ViewState["CUST_VALUE"].ToString() == "DXB")
                    {
                        ObsType = "Result";
                    }
                    else
                    {
                        if (ServTrnt == "L")
                        {
                            ObsType = "LOINC";
                        }
                        else
                        {
                            ObsType = "Universal Dental";
                        }
                    }

                    if (ObsValue == "" && ViewState["CUST_VALUE"].ToString() == "DXB" && GlobalValues.FileDescription == "AMMC")
                    {
                        ObsValue = GetLabValue(ServCode, ObsConversion);
                        if (ServTrnt == "C")
                        {
                            ObsValue = GetBPValue(ServCode);
                        }

                    }


                }


            }

            if (ServTrnt == "C" && Convert.ToBoolean(ViewState["NewFlag"]) == true && txtFileNo.Text.Trim() != "OPDCR" || txtFileNo.Text.Trim() != "ADAB")
            {
                Criteria = "  HIT_SERV_TYPE='C' and   HIM_PT_ID='" + txtFileNo.Text.Trim() + "' AND  HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'  and HIM_DR_CODE='" + txtDoctorID.Text.Trim() + "'";
                DataSet DSVisitType = new DataSet();
                DSVisitType = objInv.ReVisitTypeGet(Criteria);
                Boolean boolVisitType = false;
                if (DSVisitType.Tables[0].Rows.Count > 0)
                {
                    /*
                      DateTime dtLastInvDate, dtInvDate;
                      dtLastInvDate = Convert.ToDateTime(DSVisitType.Tables[0].Rows[0]["HIM_DATE"]);

                      //dtInvDate =  Convert.ToDateTime(Convert.ToDateTime(txtInvDate.Text).ToString("MM/dd/yyyy"));           
                     
                      dtLastInvDate = Convert.ToDateTime(txtInvDate.Text.Trim());

                        
                      string strStartDate = txtInvDate.Text;
                      string[] arrDate = strStartDate.Split('/');
                      string strForStartDate = "";

                      if (arrDate.Length > 1)
                      {
                          strForStartDate = arrDate[1] + "/" + arrDate[0] + "/" + arrDate[2];
                      }
                        

                     dtInvDate = Convert.ToDateTime(strForStartDate);
                       
                      TimeSpan Diff = dtInvDate - dtLastInvDate;

                      if (Diff.Days >= 2 && Diff.Days < Convert.ToInt32(Session["RevisitDays"]))
                      {
                          boolVisitType = true;
                      }
                      */

                    string dtLastInvDate, dtInvDate;
                    TextFileWriting("1");
                    dtLastInvDate = Convert.ToString(DSVisitType.Tables[0].Rows[0]["HIM_DATEDESC"]);
                    TextFileWriting("2");
                    dtInvDate = Convert.ToString(txtInvDate.Text.Trim());
                    TextFileWriting("3");
                    // TimeSpan Diff = dtInvDate - dtLastInvDate;
                    Int32 intDatediff;
                    intDatediff = objCom.fnGetDateDiff(dtInvDate, dtLastInvDate);


                    if (intDatediff >= 2 && intDatediff < Convert.ToInt32(Session["RevisitDays"]))
                    {
                        boolVisitType = true;
                    }

                }

                if (boolVisitType == true && GlobalValues.FileDescription != "RASHIDIYA" && GlobalValues.FileDescription != "KQMC")
                {
                    lblStatus.Text = "This patient is no need to charge consultation. Do you want to invoice.";


                }
            }



            if (drpInvType.SelectedValue == "Cash")
            {
                Co_Type = "%";
                Co_Ins = 100;
                Covered = "U";// lblCoverd.Text = "U";

            }
            else if (drpInvType.SelectedValue == "Credit")
            {
                if (hidInsTrtnt.Value != "Service Type")
                {
                    if (hidCompAgrmtType.Value != "S")
                    {






                    }
                }

                Covered = "U";

                if (txtSubCompanyID.Text != "")
                {


                    //  DataRow[] DRCompAgr = GetCompanyAgrDtls(ServCode, txtSubCompanyID.Text.Trim());
                    DataSet DSCompAgr = new DataSet();

                    DSCompAgr = GetCompanyAgrDtls(ServCode, txtSubCompanyID.Text.Trim());

                    foreach (DataRow DR in DSCompAgr.Tables[0].Rows)
                    {
                        if (DR.IsNull("HAS_NETAMOUNT") == false)
                        {
                            ServFee = Convert.ToDecimal(DR["HAS_COMP_FEE"]);
                            Covered = Convert.ToString(DR["HAS_COVERING_TYPE"]);//lblCoverd.Text = 

                            Co_Type = Convert.ToString(DR["HAS_DISC_TYPE"]);
                            Co_Ins = Convert.ToDecimal(DR["HAS_DISC_AMT"]);

                            ServCat = Convert.ToString(DR["HAS_CATEGORY_ID"]);//lblgvCatID.Text = Convert.ToString(DSCompAgr.Tables[0].Rows[0]["HAS_CATEGORY_ID"]);
                            ServiceCost = Convert.ToDecimal(Convert.ToString(DR["HAS_SERV_COST"]) != "" ? Convert.ToString(DR["HAS_SERV_COST"]) : "0");


                            DiscType = Convert.ToString(DR["HAS_DISC_TYPE"]);
                            DiscAmount = Convert.ToDecimal(Convert.ToString(DR["HAS_DISC_AMT"]) != "" ? Convert.ToString(DR["HAS_DISC_AMT"]) : "0");

                            // ServFee = Val(RsCompAgrmt.Fields("HAS_COMP_FEE").Value)

                            //  ServTrnt = Trim(RsCompAgrmt.Fields("HAS_TREATMENT_TYPE").Value)
                            //  ServCat = Trim(RsCompAgrmt.Fields("HAS_CATEGORY_ID").Value)


                            //  Agrmt_Serv_Desc = Trim(RsCompAgrmt.Fields("HAS_SERV_NAME").Value)
                            //  Covered = "C"



                        }

                        else
                        {
                            Co_Type = "%";
                            Co_Ins = 100;
                            Covered = "U";
                        }
                    }
                }


            }


            string strHaadName = "", strHaadType = "0";
            if (HaadCode != "")
            {
                DataSet DSHaad = new DataSet();
                DSHaad = GetHaadName(HaadCode);

                if (DSHaad.Tables[0].Rows.Count > 0)
                {
                    if (DSHaad.Tables[0].Rows[0].IsNull("HHS_DESCRIPTION") == false)
                    {
                        strHaadName = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_DESCRIPTION"]);
                    }
                    if (DSHaad.Tables[0].Rows[0].IsNull("HHS_TYPE_VALUE") == false)
                    {
                        strHaadType = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_TYPE_VALUE"]);
                    }
                }

            }

            objrow["HIT_SERV_TYPE"] = ServTrnt;
            objrow["HIT_CAT_ID"] = ServCat;
            objrow["HIT_COVERED"] = Covered;// lblCoverd.Text.Trim();
            // objrow["HIT_REFNO"] = lblgvRefNo.Text.Trim();

            objrow["HIT_COSTPRICE"] = ServiceCost.ToString("#0.00"); ;// lblgvCostPrice.Text.Trim();
            objrow["HIT_SERVCOST"] = ServiceCost.ToString("#0.00"); ;
            //   objrow["HIT_ADJ_DEDUCT_AMOUNT"] = lblgvAdjDedAmt.Text.Trim();

            objrow["HIT_FEE"] = ServFee.ToString("#0.00"); ;

            objrow["HIT_DISC_TYPE"] = DiscType;
            objrow["HIT_DISC_AMT"] = DiscAmount.ToString("#0.00"); ;

            decimal decDiscount = 0;


            if (DiscType == "%")
            {
                decDiscount = ((ServFee / 100) * DiscAmount);
            }
            else
            {
                decDiscount = DiscAmount;

            }

            objrow["HIT_QTY"] = "1";
            //objrow["HIT_AMOUNT"] = Convert.ToString(ServFee * 1);
            objrow["HIT_AMOUNT"] = ServFee - decDiscount;


            ServFee = Convert.ToDecimal(objrow["HIT_AMOUNT"]);

            objrow["HIT_HAAD_CODE"] = HaadCode;
            objrow["HIT_TYPE_VALUE"] = strHaadType;

            objrow["HIT_STARTDATEDesc"] = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();

            objrow["HIO_TYPE"] = ObsType;

            //objrow["HIO_SERV_CODE"] = ObsCode;
            objrow["HIO_CODE"] = ObsCode;

            objrow["HIO_VALUE"] = ObsValue;
            objrow["HIO_VALUETYPE"] = ObsValueType;


            if (drpInvType.SelectedValue == "Cash")
            {
                objrow["HIT_CO_INS_TYPE"] = "%";
                objrow["HIT_CO_INS_AMT"] = "100";
            }


            decimal decDedAmt = 0, decConInsAmt = 0;
            decDedAmt = Convert.ToDecimal(ViewState["Deductible"]);
            decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_Amt"]);
            if (ServTrnt == "C")
            {
                if (Convert.ToString(ViewState["Deductible_Type"]) == "$")
                {
                    objrow["HIT_DEDUCT"] = decDedAmt.ToString("#0.00"); ;

                    if (txtSubCompanyID.Text != "")
                    {
                        objrow["HIT_CO_INS_TYPE"] = "$";
                        objrow["HIT_CO_INS_AMT"] = decDedAmt.ToString("#0.00");
                    }
                }
                else
                {
                    objrow["HIT_DEDUCT"] = Convert.ToDecimal((ServFee * decDedAmt) / 100).ToString("#0.00");// Convert.ToString(ViewState["Co_Ins_Amt"]);
                    if (txtSubCompanyID.Text != "")
                    {
                        objrow["HIT_CO_INS_TYPE"] = "$";
                        objrow["HIT_CO_INS_AMT"] = Convert.ToDecimal((ServFee * decDedAmt) / 100).ToString("#0.00");// Convert.ToString(ViewState["Co_Ins_Amt"]);
                    }



                }


            }
            else
            {
                objrow["HIT_DEDUCT"] = "0";
                if (txtSubCompanyID.Text != "")
                {
                    objrow["HIT_CO_INS_TYPE"] = Convert.ToString(ViewState["Co_Ins_Type"]);
                }


                if (Convert.ToString(ViewState["Deductible_Type"]) == "$")
                {
                    if (txtSubCompanyID.Text != "")
                    {
                        objrow["HIT_CO_INS_AMT"] = decConInsAmt.ToString("#0.00"); ;
                    }
                }
                else
                {
                    if (txtSubCompanyID.Text != "")
                    {
                        objrow["HIT_CO_INS_AMT"] = decConInsAmt.ToString("#0.00"); ; // Convert.ToString(ViewState["Co_Ins_Amt"]);
                    }
                }
            }



            //objrow["HIT_CO_TOTAL"] =
            //objrow["HIT_COMP_TYPE"] =
            //objrow["HIT_COMP_AMT"] =
            //objrow["HIT_COMP_TOTAL"] = 
            //txtgvCoInsTotal.Text = Convert.ToString(decCoInsAmt);
            //txtgvCompType.Text = drpCoInsType.Text;
            //txtgvCompAmount.Text = Convert.ToString(Convert.ToDecimal(txtHitAmount.Text) - decCoInsAmt);
            //txtgvCompTotal.Text = Convert.ToString(Convert.ToDecimal(txtHitAmount.Text) - decCoInsAmt);



            DT.Rows.Add(objrow);

            ViewState["InvoiceTrans"] = DT;
            BindTempInvoiceTrans();

            AmountCalculation();



        }

        void fnServiceAddFromSO(string strServIds)
        {
            DataSet DSServ = new DataSet();
            CommonBAL objDBO = new CommonBAL();
            string ServCode, ServName;

            string Criteria1 = " 1=1 AND HSM_STATUS='A' ";

            Criteria1 += " AND  HSM_SERV_ID  in (" + strServIds + ")";



            DSServ = objDBO.ServiceMasterGet(Criteria1);

            DataSet DSCompAgr = new DataSet();

            string CriteriaAgr = " 1=1 ";
            CriteriaAgr += " AND HAS_SERV_ID in (" + strServIds + ") AND HAS_COMP_ID='" + txtSubCompanyID.Text.Trim() + "'";
            DSCompAgr = objDBO.AgrementServiceGet(CriteriaAgr);


            foreach (DataRow DR in DSServ.Tables[0].Rows)
            {
                ServCode = Convert.ToString(DR["HSM_SERV_ID"]);
                ServName = Convert.ToString(DR["HSM_NAME"]);


                IP_Invoice objInv = new IP_Invoice();
                string Criteria = " 1=1 ";
                DataTable DT = new DataTable();

                DT = (DataTable)ViewState["InvoiceTrans"];
                DataRow objrow;
                objrow = DT.NewRow();
                objrow["HIT_SERV_CODE"] = ServCode;
                objrow["HIT_Description"] = ServName;
                objrow["HIT_QTY"] = 1;
                objrow["HIT_DISC_AMT"] = 0;
                objrow["HIT_DEDUCT"] = 0;
                objrow["HIT_CO_INS_AMT"] = 0;
                objrow["HIT_DR_CODE"] = txtDoctorID.Text;
                objrow["HIT_DR_NAME"] = txtDoctorName.Text;

                Int32 intCurRow = 0;
                intCurRow = gvInvoiceTrans.Rows.Count - 1;

                string DiscType = "$", Co_Type = "%", ServCat = "", ServTrnt = "", Covered = "", HaadCode = "", ObsNeeded = "N", ObsType = "", ObsCode = "", ObsValue = "", ObsValueType = "";
                decimal DiscAmount = 0, ServFee = 0, Co_Ins = 0, ServiceCost = 0, ObsConversion = 0;
                //DataSet DSServ = new DataSet();

                //DSServ = GetServiceMasterName("HSM_SERV_ID", ServCode);


                //if (DSServ.Tables[0].Rows.Count > 0)
                //{

                ServFee = Convert.ToDecimal(DR["HSM_FEE"]);
                ServCat = Convert.ToString(DR["HSM_CAT_ID"]);
                ServTrnt = Convert.ToString(DR["HSM_TYPE"]);

                ServiceCost = Convert.ToDecimal(Convert.ToString(DR["HSM_COST"]) != "" ? Convert.ToString(DR["HSM_COST"]) : "0");


                HaadCode = Convert.ToString(DR["HSM_HAAD_CODE"]);
                ObsNeeded = Convert.ToString(DR["HSM_OBSERVATION_NEEDED"]);
                if (ObsNeeded == "Y")
                {
                    ObsCode = Convert.ToString(DR["HSM_OBS_CODE"]);//txtObsCode.Text = 
                    ObsValueType = Convert.ToString(DR["HSM_OBS_VALUETYPE"]);//txtObsValue.Text = 
                    ObsConversion = DR.IsNull("HSM_OBS_CONVERSION") == true || Convert.ToString(DR["HSM_OBS_CONVERSION"]) == "" ? 0 : Convert.ToDecimal(DR["HSM_OBS_CONVERSION"]);

                    if (ViewState["CUST_VALUE"].ToString() == "DXB")
                    {
                        ObsType = "Result";
                    }
                    else
                    {
                        if (ServTrnt == "L")
                        {
                            ObsType = "LOINC";
                        }
                        else
                        {
                            ObsType = "Universal Dental";
                        }
                    }

                    if (ObsValue == "" && ViewState["CUST_VALUE"].ToString() == "DXB" && GlobalValues.FileDescription == "AMMC")
                    {
                        ObsValue = GetLabValue(ServCode, ObsConversion);
                        if (ServTrnt == "C")
                        {
                            ObsValue = GetBPValue(ServCode);
                        }

                    }


                }


                //}

                if (ServTrnt == "C" && Convert.ToBoolean(ViewState["NewFlag"]) == true && txtFileNo.Text.Trim() != "OPDCR" || txtFileNo.Text.Trim() != "ADAB")
                {
                    Criteria = "  HIT_SERV_TYPE='C' and   HIM_PT_ID='" + txtFileNo.Text.Trim() + "' AND  HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'  and HIM_DR_CODE='" + txtDoctorID.Text.Trim() + "'";
                    DataSet DSVisitType = new DataSet();
                    DSVisitType = objInv.ReVisitTypeGet(Criteria);
                    Boolean boolVisitType = false;
                    if (DSVisitType.Tables[0].Rows.Count > 0)
                    {
                        /*
                          DateTime dtLastInvDate, dtInvDate;
                          dtLastInvDate = Convert.ToDateTime(DSVisitType.Tables[0].Rows[0]["HIM_DATE"]);

                          //dtInvDate =  Convert.ToDateTime(Convert.ToDateTime(txtInvDate.Text).ToString("MM/dd/yyyy"));           
                     
                          dtLastInvDate = Convert.ToDateTime(txtInvDate.Text.Trim());

                        
                          string strStartDate = txtInvDate.Text;
                          string[] arrDate = strStartDate.Split('/');
                          string strForStartDate = "";

                          if (arrDate.Length > 1)
                          {
                              strForStartDate = arrDate[1] + "/" + arrDate[0] + "/" + arrDate[2];
                          }
                        

                         dtInvDate = Convert.ToDateTime(strForStartDate);
                       
                          TimeSpan Diff = dtInvDate - dtLastInvDate;

                          if (Diff.Days >= 2 && Diff.Days < Convert.ToInt32(Session["RevisitDays"]))
                          {
                              boolVisitType = true;
                          }
                          */

                        string dtLastInvDate, dtInvDate;
                        TextFileWriting("1");
                        dtLastInvDate = Convert.ToString(DSVisitType.Tables[0].Rows[0]["HIM_DATEDESC"]);
                        TextFileWriting("2");
                        dtInvDate = Convert.ToString(txtInvDate.Text.Trim());
                        TextFileWriting("3");
                        // TimeSpan Diff = dtInvDate - dtLastInvDate;
                        Int32 intDatediff;
                        intDatediff = objCom.fnGetDateDiff(dtInvDate, dtLastInvDate);


                        if (intDatediff >= 2 && intDatediff < Convert.ToInt32(Session["RevisitDays"]))
                        {
                            boolVisitType = true;
                        }

                    }

                    if (boolVisitType == true && GlobalValues.FileDescription != "RASHIDIYA" && GlobalValues.FileDescription != "KQMC")
                    {
                        lblStatus.Text = "This patient is no need to charge consultation. Do you want to invoice.";


                    }
                }



                if (drpInvType.SelectedValue == "Cash")
                {
                    Co_Type = "%";
                    Co_Ins = 100;
                    Covered = "U";// lblCoverd.Text = "U";

                }
                else if (drpInvType.SelectedValue == "Credit")
                {
                    /*
                    if (hidInsTrtnt.Value != "Service Type")
                    {
                        if (hidCompAgrmtType.Value != "S")
                        {

                            string CriteriaCompBenDtls = " 1=1 ";

                            if (txtSubCompanyID.Text == "")
                            {
                                CriteriaCompBenDtls += " and  HCBD_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND  hcbd_comp_id='" + hidCompanyID.Value + "' and hcbd_category_id='" + ServTrnt + "' and  hcbd_cat_type='" + drpPayType.SelectedValue + "'";
                            }
                            else
                            {
                                CriteriaCompBenDtls += " and  HCBD_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND  hcbd_comp_id='" + txtSubCompanyID.Text.Trim() + "' and hcbd_category_id='" + ServTrnt + "' and  hcbd_cat_type='" + drpPayType.SelectedValue + "'";

                            }

                            dboperations dbo = new dboperations();
                            DataSet DSCompBenDtls = new DataSet();
                            DSCompBenDtls = dbo.CompBenefitsDtlsGet(CriteriaCompBenDtls);

                            if( Convert.ToString(DSCompBenDtls.Tables[0].Rows[0]["HCBD_CATEGORY_TYPE"]) == "C" || Convert.ToString(DSCompBenDtls.Tables[0].Rows[0]["HCBD_CATEGORY_TYPE"]) == "P" )
                            {
                                if(hidCompAgrmtType.Value == "S")
                                {

                                }
                            }


                        }
                    }
                    else
                    {


                    }
                    */
                    Covered = "U";

                    if (txtSubCompanyID.Text != "")
                    {


                        DataTable DTCompAgr;

                        DTCompAgr = DSCompAgr.Tables[0];
                        DataRow[] TempRow;
                        TempRow = DTCompAgr.Select("HAS_SERV_ID ='" + ServCode + "'");

                        if (TempRow.Length > 0)
                        {
                            if (TempRow[0].IsNull("HAS_NETAMOUNT") == false)
                            {
                                ServFee = Convert.ToDecimal(TempRow[0]["HAS_COMP_FEE"]);
                                Covered = Convert.ToString(TempRow[0]["HAS_COVERING_TYPE"]);//lblCoverd.Text = 

                                Co_Type = Convert.ToString(TempRow[0]["HAS_DISC_TYPE"]);
                                Co_Ins = Convert.ToDecimal(TempRow[0]["HAS_DISC_AMT"]);

                                ServCat = Convert.ToString(TempRow[0]["HAS_CATEGORY_ID"]);//lblgvCatID.Text = Convert.ToString(DSCompAgr.Tables[0].Rows[0]["HAS_CATEGORY_ID"]);
                                ServiceCost = Convert.ToDecimal(Convert.ToString(TempRow[0]["HAS_SERV_COST"]) != "" ? Convert.ToString(TempRow[0]["HAS_SERV_COST"]) : "0");


                                DiscType = Convert.ToString(TempRow[0]["HAS_DISC_TYPE"]);
                                DiscAmount = Convert.ToDecimal(Convert.ToString(TempRow[0]["HAS_DISC_AMT"]) != "" ? Convert.ToString(TempRow[0]["HAS_DISC_AMT"]) : "0");

                                // ServFee = Val(RsCompAgrmt.Fields("HAS_COMP_FEE").Value)

                                //  ServTrnt = Trim(RsCompAgrmt.Fields("HAS_TREATMENT_TYPE").Value)
                                //  ServCat = Trim(RsCompAgrmt.Fields("HAS_CATEGORY_ID").Value)


                                //  Agrmt_Serv_Desc = Trim(RsCompAgrmt.Fields("HAS_SERV_NAME").Value)
                                //  Covered = "C"



                            }

                        }
                        else
                        {
                            Co_Type = "%";
                            Co_Ins = 100;
                            Covered = "U";
                        }

                    }


                }


                string strHaadName = "", strHaadType = "0";
                if (HaadCode != "")
                {
                    DataSet DSHaad = new DataSet();
                    DSHaad = GetHaadName(HaadCode);

                    if (DSHaad.Tables[0].Rows.Count > 0)
                    {
                        if (DSHaad.Tables[0].Rows[0].IsNull("HHS_DESCRIPTION") == false)
                        {
                            strHaadName = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_DESCRIPTION"]);
                        }
                        if (DSHaad.Tables[0].Rows[0].IsNull("HHS_TYPE_VALUE") == false)
                        {
                            strHaadType = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_TYPE_VALUE"]);
                        }
                    }

                }

                objrow["HIT_SERV_TYPE"] = ServTrnt;
                objrow["HIT_CAT_ID"] = ServCat;
                objrow["HIT_COVERED"] = Covered;// lblCoverd.Text.Trim();
                // objrow["HIT_REFNO"] = lblgvRefNo.Text.Trim();

                objrow["HIT_COSTPRICE"] = ServiceCost.ToString("#0.00"); ;// lblgvCostPrice.Text.Trim();
                objrow["HIT_SERVCOST"] = ServiceCost.ToString("#0.00"); ;
                //   objrow["HIT_ADJ_DEDUCT_AMOUNT"] = lblgvAdjDedAmt.Text.Trim();

                objrow["HIT_FEE"] = ServFee.ToString("#0.00"); ;

                objrow["HIT_DISC_TYPE"] = DiscType;
                objrow["HIT_DISC_AMT"] = DiscAmount.ToString("#0.00"); ;

                decimal decDiscount = 0;


                if (DiscType == "%")
                {
                    decDiscount = ((ServFee / 100) * DiscAmount);
                }
                else
                {
                    decDiscount = DiscAmount;

                }

                objrow["HIT_QTY"] = "1";
                //objrow["HIT_AMOUNT"] = Convert.ToString(ServFee * 1);
                objrow["HIT_AMOUNT"] = ServFee - decDiscount;


                ServFee = Convert.ToDecimal(objrow["HIT_AMOUNT"]);

                objrow["HIT_HAAD_CODE"] = HaadCode;
                objrow["HIT_TYPE_VALUE"] = strHaadType;

                objrow["HIT_STARTDATEDesc"] = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();

                objrow["HIO_TYPE"] = ObsType;

                //objrow["HIO_SERV_CODE"] = ObsCode;
                objrow["HIO_CODE"] = ObsCode;

                objrow["HIO_VALUE"] = ObsValue;
                objrow["HIO_VALUETYPE"] = ObsValueType;


                if (drpInvType.SelectedValue == "Cash")
                {
                    objrow["HIT_CO_INS_TYPE"] = "%";
                    objrow["HIT_CO_INS_AMT"] = "100";
                }


                decimal decDedAmt = 0, decConInsAmt = 0;
                decDedAmt = Convert.ToDecimal(ViewState["Deductible"]);
                decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_Amt"]);
                if (ServTrnt == "C")
                {
                    if (Convert.ToString(ViewState["Deductible_Type"]) == "$")
                    {
                        objrow["HIT_DEDUCT"] = decDedAmt.ToString("#0.00"); ;

                        if (txtSubCompanyID.Text != "")
                        {
                            objrow["HIT_CO_INS_TYPE"] = "$";
                            objrow["HIT_CO_INS_AMT"] = decDedAmt.ToString("#0.00");
                        }
                    }
                    else
                    {
                        objrow["HIT_DEDUCT"] = Convert.ToDecimal((ServFee * decDedAmt) / 100).ToString("#0.00");// Convert.ToString(ViewState["Co_Ins_Amt"]);
                        if (txtSubCompanyID.Text != "")
                        {
                            objrow["HIT_CO_INS_TYPE"] = "$";
                            objrow["HIT_CO_INS_AMT"] = Convert.ToDecimal((ServFee * decDedAmt) / 100).ToString("#0.00");// Convert.ToString(ViewState["Co_Ins_Amt"]);
                        }



                    }


                }
                else
                {
                    objrow["HIT_DEDUCT"] = "0";
                    if (txtSubCompanyID.Text != "")
                    {
                        objrow["HIT_CO_INS_TYPE"] = Convert.ToString(ViewState["Co_Ins_Type"]);
                    }


                    if (Convert.ToString(ViewState["Deductible_Type"]) == "$")
                    {
                        if (txtSubCompanyID.Text != "")
                        {
                            objrow["HIT_CO_INS_AMT"] = decConInsAmt.ToString("#0.00"); ;
                        }
                    }
                    else
                    {
                        if (txtSubCompanyID.Text != "")
                        {
                            objrow["HIT_CO_INS_AMT"] = decConInsAmt.ToString("#0.00"); ; // Convert.ToString(ViewState["Co_Ins_Amt"]);
                        }
                    }
                }



                //objrow["HIT_CO_TOTAL"] =
                //objrow["HIT_COMP_TYPE"] =
                //objrow["HIT_COMP_AMT"] =
                //objrow["HIT_COMP_TOTAL"] = 
                //txtgvCoInsTotal.Text = Convert.ToString(decCoInsAmt);
                //txtgvCompType.Text = drpCoInsType.Text;
                //txtgvCompAmount.Text = Convert.ToString(Convert.ToDecimal(txtHitAmount.Text) - decCoInsAmt);
                //txtgvCompTotal.Text = Convert.ToString(Convert.ToDecimal(txtHitAmount.Text) - decCoInsAmt);

                ViewState["InvoiceTrans"] = DT;

                DT.Rows.Add(objrow);



            }

            ProcessTimLog(System.DateTime.Now.ToString() + "      BindTempInvoiceTrans() Start");
            BindTempInvoiceTrans();
            ProcessTimLog(System.DateTime.Now.ToString() + "      BindTempInvoiceTrans() End");

            ProcessTimLog(System.DateTime.Now.ToString() + "      AmountCalculation() Start");
            AmountCalculation();
            ProcessTimLog(System.DateTime.Now.ToString() + "      AmountCalculation() End");

        }

        void fnServiceAddFromSO1(DataTable DTSO)
        {
            string strServIds = "";
            foreach (DataRow DR_SO in DTSO.Rows)
            {
                if (strServIds != "")
                {

                    strServIds += ",'" + Convert.ToString(DR_SO["HAAD_CODE"]) + "'";

                }
                else
                {

                    strServIds = "'" + Convert.ToString(DR_SO["HAAD_CODE"]) + "'";
                }
            }

            DataSet DSServ = new DataSet();
            CommonBAL objDBO = new CommonBAL();
            string ServCode, ServName;

            string Criteria1 = " 1=1 AND HSM_STATUS='A' ";

            Criteria1 += " AND  HSM_SERV_ID  in (" + strServIds + ")";


            DSServ = objDBO.ServiceMasterGet(Criteria1);

            DataSet DSCompAgr = new DataSet();

            string CriteriaAgr = " 1=1 ";
            CriteriaAgr += " AND HAS_SERV_ID in (" + strServIds + ") AND HAS_COMP_ID='" + txtSubCompanyID.Text.Trim() + "'";
            DSCompAgr = objDBO.AgrementServiceGet(CriteriaAgr);

            foreach (DataRow DR_SO1 in DTSO.Rows)
            {
                DataRow[] TempRow1;
                TempRow1 = DSServ.Tables[0].Select("HSM_SERV_ID ='" + Convert.ToString(DR_SO1["HAAD_CODE"]) + "'");


                foreach (DataRow DR in TempRow1)
                {

                    ServCode = Convert.ToString(DR["HSM_SERV_ID"]);
                    ServName = Convert.ToString(DR["HSM_NAME"]);


                    IP_Invoice objInv = new IP_Invoice();
                    string Criteria = " 1=1 ";
                    DataTable DT = new DataTable();

                    DT = (DataTable)ViewState["InvoiceTrans"];
                    DataRow objrow;
                    objrow = DT.NewRow();
                    objrow["SOTransID"] = Convert.ToString(DR_SO1["SO_ID"]);
                    objrow["HIT_TRANS_DATEDesc"] = txtInvDate.Text;
                    objrow["HIT_SERV_CODE"] = ServCode;
                    objrow["HIT_Description"] = ServName;
                    objrow["HIT_QTY"] = 1;
                    objrow["HIT_DISC_AMT"] = 0;
                    objrow["HIT_DEDUCT"] = 0;
                    objrow["HIT_CO_INS_AMT"] = 0;
                    objrow["HIT_DR_CODE"] = txtDoctorID.Text;
                    objrow["HIT_DR_NAME"] = txtDoctorName.Text;

                    Int32 intCurRow = 0;
                    intCurRow = gvInvoiceTrans.Rows.Count - 1;

                    string DiscType = "$", Co_Type = "%", ServCat = "", ServTrnt = "", Covered = "", HaadCode = "", ObsNeeded = "N", ObsType = "", ObsCode = "", ObsValue = "", ObsValueType = "";
                    decimal DiscAmount = 0, ServFee = 0, Co_Ins = 0, ServiceCost = 0, ObsConversion = 0;
                    //DataSet DSServ = new DataSet();

                    //DSServ = GetServiceMasterName("HSM_SERV_ID", ServCode);


                    //if (DSServ.Tables[0].Rows.Count > 0)
                    //{

                    ServFee = Convert.ToDecimal(DR["HSM_FEE"]);
                    ServCat = Convert.ToString(DR["HSM_CAT_ID"]);
                    ServTrnt = Convert.ToString(DR["HSM_TYPE"]);

                    ServiceCost = Convert.ToDecimal(Convert.ToString(DR["HSM_COST"]) != "" ? Convert.ToString(DR["HSM_COST"]) : "0");


                    HaadCode = Convert.ToString(DR["HSM_HAAD_CODE"]);
                    ObsNeeded = Convert.ToString(DR["HSM_OBSERVATION_NEEDED"]);
                    if (ObsNeeded == "Y")
                    {
                        ObsCode = Convert.ToString(DR["HSM_OBS_CODE"]);//txtObsCode.Text = 
                        ObsValueType = Convert.ToString(DR["HSM_OBS_VALUETYPE"]);//txtObsValue.Text = 
                        ObsConversion = DR.IsNull("HSM_OBS_CONVERSION") == true || Convert.ToString(DR["HSM_OBS_CONVERSION"]) == "" ? 0 : Convert.ToDecimal(DR["HSM_OBS_CONVERSION"]);

                        if (ViewState["CUST_VALUE"].ToString() == "DXB")
                        {
                            ObsType = "Result";
                        }
                        else
                        {
                            if (ServTrnt == "L")
                            {
                                ObsType = "LOINC";
                            }
                            else
                            {
                                ObsType = "Universal Dental";
                            }
                        }

                        if (ObsValue == "" && ViewState["CUST_VALUE"].ToString() == "DXB" && GlobalValues.FileDescription == "AMMC")
                        {
                            ObsValue = GetLabValue(ServCode, ObsConversion);
                            if (ServTrnt == "C")
                            {
                                ObsValue = GetBPValue(ServCode);
                            }

                        }


                    }


                    //}

                    if (ServTrnt == "C" && Convert.ToBoolean(ViewState["NewFlag"]) == true && txtFileNo.Text.Trim() != "OPDCR" || txtFileNo.Text.Trim() != "ADAB")
                    {
                        Criteria = "  HIT_SERV_TYPE='C' and   HIM_PT_ID='" + txtFileNo.Text.Trim() + "' AND  HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'  and HIM_DR_CODE='" + txtDoctorID.Text.Trim() + "'";
                        DataSet DSVisitType = new DataSet();
                        DSVisitType = objInv.ReVisitTypeGet(Criteria);
                        Boolean boolVisitType = false;
                        if (DSVisitType.Tables[0].Rows.Count > 0)
                        {
                            /*
                              DateTime dtLastInvDate, dtInvDate;
                              dtLastInvDate = Convert.ToDateTime(DSVisitType.Tables[0].Rows[0]["HIM_DATE"]);

                              //dtInvDate =  Convert.ToDateTime(Convert.ToDateTime(txtInvDate.Text).ToString("MM/dd/yyyy"));           
                     
                              dtLastInvDate = Convert.ToDateTime(txtInvDate.Text.Trim());

                        
                              string strStartDate = txtInvDate.Text;
                              string[] arrDate = strStartDate.Split('/');
                              string strForStartDate = "";

                              if (arrDate.Length > 1)
                              {
                                  strForStartDate = arrDate[1] + "/" + arrDate[0] + "/" + arrDate[2];
                              }
                        

                             dtInvDate = Convert.ToDateTime(strForStartDate);
                       
                              TimeSpan Diff = dtInvDate - dtLastInvDate;

                              if (Diff.Days >= 2 && Diff.Days < Convert.ToInt32(Session["RevisitDays"]))
                              {
                                  boolVisitType = true;
                              }
                              */

                            string dtLastInvDate, dtInvDate;
                            TextFileWriting("1");
                            dtLastInvDate = Convert.ToString(DSVisitType.Tables[0].Rows[0]["HIM_DATEDESC"]);
                            TextFileWriting("2");
                            dtInvDate = Convert.ToString(txtInvDate.Text.Trim());
                            TextFileWriting("3");
                            // TimeSpan Diff = dtInvDate - dtLastInvDate;
                            Int32 intDatediff;
                            intDatediff = objCom.fnGetDateDiff(dtInvDate, dtLastInvDate);


                            if (intDatediff >= 2 && intDatediff < Convert.ToInt32(Session["RevisitDays"]))
                            {
                                boolVisitType = true;
                            }

                        }

                        if (boolVisitType == true && GlobalValues.FileDescription != "RASHIDIYA" && GlobalValues.FileDescription != "KQMC")
                        {
                            lblStatus.Text = "This patient is no need to charge consultation. Do you want to invoice.";


                        }
                    }



                    if (drpInvType.SelectedValue == "Cash")
                    {
                        Co_Type = "%";
                        Co_Ins = 100;
                        Covered = "U";// lblCoverd.Text = "U";

                    }
                    else if (drpInvType.SelectedValue == "Credit")
                    {
                        /*
                        if (hidInsTrtnt.Value != "Service Type")
                        {
                            if (hidCompAgrmtType.Value != "S")
                            {

                                string CriteriaCompBenDtls = " 1=1 ";

                                if (txtSubCompanyID.Text == "")
                                {
                                    CriteriaCompBenDtls += " and  HCBD_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND  hcbd_comp_id='" + hidCompanyID.Value + "' and hcbd_category_id='" + ServTrnt + "' and  hcbd_cat_type='" + drpPayType.SelectedValue + "'";
                                }
                                else
                                {
                                    CriteriaCompBenDtls += " and  HCBD_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'  AND  hcbd_comp_id='" + txtSubCompanyID.Text.Trim() + "' and hcbd_category_id='" + ServTrnt + "' and  hcbd_cat_type='" + drpPayType.SelectedValue + "'";

                                }

                                dboperations dbo = new dboperations();
                                DataSet DSCompBenDtls = new DataSet();
                                DSCompBenDtls = dbo.CompBenefitsDtlsGet(CriteriaCompBenDtls);

                                if( Convert.ToString(DSCompBenDtls.Tables[0].Rows[0]["HCBD_CATEGORY_TYPE"]) == "C" || Convert.ToString(DSCompBenDtls.Tables[0].Rows[0]["HCBD_CATEGORY_TYPE"]) == "P" )
                                {
                                    if(hidCompAgrmtType.Value == "S")
                                    {

                                    }
                                }


                            }
                        }
                        else
                        {


                        }
                        */
                        Covered = "U";

                        if (txtSubCompanyID.Text != "")
                        {


                            DataTable DTCompAgr;

                            DTCompAgr = DSCompAgr.Tables[0];
                            DataRow[] TempRow;
                            TempRow = DTCompAgr.Select("HAS_SERV_ID ='" + ServCode + "'");

                            if (TempRow.Length > 0)
                            {
                                if (TempRow[0].IsNull("HAS_NETAMOUNT") == false)
                                {
                                    ServFee = Convert.ToDecimal(TempRow[0]["HAS_COMP_FEE"]);
                                    Covered = Convert.ToString(TempRow[0]["HAS_COVERING_TYPE"]);//lblCoverd.Text = 

                                    Co_Type = Convert.ToString(TempRow[0]["HAS_DISC_TYPE"]);
                                    Co_Ins = Convert.ToDecimal(TempRow[0]["HAS_DISC_AMT"]);

                                    ServCat = Convert.ToString(TempRow[0]["HAS_CATEGORY_ID"]);//lblgvCatID.Text = Convert.ToString(DSCompAgr.Tables[0].Rows[0]["HAS_CATEGORY_ID"]);
                                    ServiceCost = Convert.ToDecimal(Convert.ToString(TempRow[0]["HAS_SERV_COST"]) != "" ? Convert.ToString(TempRow[0]["HAS_SERV_COST"]) : "0");


                                    DiscType = Convert.ToString(TempRow[0]["HAS_DISC_TYPE"]);
                                    DiscAmount = Convert.ToDecimal(Convert.ToString(TempRow[0]["HAS_DISC_AMT"]) != "" ? Convert.ToString(TempRow[0]["HAS_DISC_AMT"]) : "0");

                                    // ServFee = Val(RsCompAgrmt.Fields("HAS_COMP_FEE").Value)

                                    //  ServTrnt = Trim(RsCompAgrmt.Fields("HAS_TREATMENT_TYPE").Value)
                                    //  ServCat = Trim(RsCompAgrmt.Fields("HAS_CATEGORY_ID").Value)


                                    //  Agrmt_Serv_Desc = Trim(RsCompAgrmt.Fields("HAS_SERV_NAME").Value)
                                    //  Covered = "C"



                                }

                            }
                            else
                            {
                                Co_Type = "%";
                                Co_Ins = 100;
                                Covered = "U";
                            }

                        }


                    }


                    string strHaadName = "", strHaadType = "0";
                    if (HaadCode != "")
                    {
                        DataSet DSHaad = new DataSet();
                        DSHaad = GetHaadName(HaadCode);

                        if (DSHaad.Tables[0].Rows.Count > 0)
                        {
                            if (DSHaad.Tables[0].Rows[0].IsNull("HHS_DESCRIPTION") == false)
                            {
                                strHaadName = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_DESCRIPTION"]);
                            }
                            if (DSHaad.Tables[0].Rows[0].IsNull("HHS_TYPE_VALUE") == false)
                            {
                                strHaadType = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_TYPE_VALUE"]);
                            }
                        }

                    }

                    objrow["HIT_SERV_TYPE"] = ServTrnt;
                    objrow["HIT_CAT_ID"] = ServCat;
                    objrow["HIT_COVERED"] = Covered;// lblCoverd.Text.Trim();
                    // objrow["HIT_REFNO"] = lblgvRefNo.Text.Trim();

                    objrow["HIT_COSTPRICE"] = ServiceCost.ToString("#0.00"); ;// lblgvCostPrice.Text.Trim();
                    objrow["HIT_SERVCOST"] = ServiceCost.ToString("#0.00"); ;
                    //   objrow["HIT_ADJ_DEDUCT_AMOUNT"] = lblgvAdjDedAmt.Text.Trim();

                    objrow["HIT_FEE"] = ServFee.ToString("#0.00"); ;

                    objrow["HIT_DISC_TYPE"] = DiscType;
                    objrow["HIT_DISC_AMT"] = DiscAmount.ToString("#0.00"); ;

                    decimal decDiscount = 0;


                    if (DiscType == "%")
                    {
                        decDiscount = ((ServFee / 100) * DiscAmount);
                    }
                    else
                    {
                        decDiscount = DiscAmount;

                    }

                    objrow["HIT_QTY"] = "1";
                    //objrow["HIT_AMOUNT"] = Convert.ToString(ServFee * 1);
                    objrow["HIT_AMOUNT"] = ServFee - decDiscount;


                    ServFee = Convert.ToDecimal(objrow["HIT_AMOUNT"]);

                    objrow["HIT_HAAD_CODE"] = HaadCode;
                    objrow["HIT_TYPE_VALUE"] = strHaadType;

                    objrow["HIT_STARTDATEDesc"] = txtInvDate.Text.Trim() + " " + txtInvTime.Text.Trim();

                    objrow["HIO_TYPE"] = ObsType;

                    //objrow["HIO_SERV_CODE"] = ObsCode;
                    objrow["HIO_CODE"] = ObsCode;

                    objrow["HIO_VALUE"] = ObsValue;
                    objrow["HIO_VALUETYPE"] = ObsValueType;


                    if (drpInvType.SelectedValue == "Cash")
                    {
                        objrow["HIT_CO_INS_TYPE"] = "%";
                        objrow["HIT_CO_INS_AMT"] = "100";
                    }


                    decimal decDedAmt = 0, decConInsAmt = 0;
                    decDedAmt = Convert.ToDecimal(ViewState["Deductible"]);
                    decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_Amt"]);
                    if (ServTrnt == "C")
                    {
                        if (Convert.ToString(ViewState["Deductible_Type"]) == "$")
                        {
                            objrow["HIT_DEDUCT"] = decDedAmt.ToString("#0.00"); ;

                            if (txtSubCompanyID.Text != "")
                            {
                                objrow["HIT_CO_INS_TYPE"] = "$";
                                objrow["HIT_CO_INS_AMT"] = decDedAmt.ToString("#0.00");
                            }
                        }
                        else
                        {
                            objrow["HIT_DEDUCT"] = Convert.ToDecimal((ServFee * decDedAmt) / 100).ToString("#0.00");// Convert.ToString(ViewState["Co_Ins_Amt"]);
                            if (txtSubCompanyID.Text != "")
                            {
                                objrow["HIT_CO_INS_TYPE"] = "$";
                                objrow["HIT_CO_INS_AMT"] = Convert.ToDecimal((ServFee * decDedAmt) / 100).ToString("#0.00");// Convert.ToString(ViewState["Co_Ins_Amt"]);
                            }



                        }


                    }
                    else
                    {
                        objrow["HIT_DEDUCT"] = "0";
                        if (txtSubCompanyID.Text != "")
                        {
                            objrow["HIT_CO_INS_TYPE"] = Convert.ToString(ViewState["Co_Ins_Type"]);
                        }


                        if (Convert.ToString(ViewState["Deductible_Type"]) == "$")
                        {
                            if (txtSubCompanyID.Text != "")
                            {
                                objrow["HIT_CO_INS_AMT"] = decConInsAmt.ToString("#0.00"); ;
                            }
                        }
                        else
                        {
                            if (txtSubCompanyID.Text != "")
                            {
                                objrow["HIT_CO_INS_AMT"] = decConInsAmt.ToString("#0.00"); ; // Convert.ToString(ViewState["Co_Ins_Amt"]);
                            }
                        }
                    }



                    //objrow["HIT_CO_TOTAL"] =
                    //objrow["HIT_COMP_TYPE"] =
                    //objrow["HIT_COMP_AMT"] =
                    //objrow["HIT_COMP_TOTAL"] = 
                    //txtgvCoInsTotal.Text = Convert.ToString(decCoInsAmt);
                    //txtgvCompType.Text = drpCoInsType.Text;
                    //txtgvCompAmount.Text = Convert.ToString(Convert.ToDecimal(txtHitAmount.Text) - decCoInsAmt);
                    //txtgvCompTotal.Text = Convert.ToString(Convert.ToDecimal(txtHitAmount.Text) - decCoInsAmt);

                    ViewState["InvoiceTrans"] = DT;

                    DT.Rows.Add(objrow);



                }
            }
            ProcessTimLog(System.DateTime.Now.ToString() + "      BindTempInvoiceTrans() Start");
            BindTempInvoiceTrans();
            ProcessTimLog(System.DateTime.Now.ToString() + "      BindTempInvoiceTrans() End");

            ProcessTimLog(System.DateTime.Now.ToString() + "      AmountCalculation() Start");
            AmountCalculation();
            ProcessTimLog(System.DateTime.Now.ToString() + "      AmountCalculation() End");

        }

        protected void btnServiceAdd_Click(object sender, EventArgs e)
        {
            try
            {
                fnServiceAdd(txtServCode.Text.Trim(), txtServName.Text.Trim());

                txtServCode.Text = "";
                txtServName.Text = "";
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnServiceAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvInvTran_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    DropDownList drpgvDiscType, drpCoInsType;
                    CheckBox chkSubmit;
                    Label lblDiscType, lblCoInsType;


                    drpgvDiscType = (DropDownList)e.Row.Cells[0].FindControl("drpgvDiscType");
                    drpCoInsType = (DropDownList)e.Row.Cells[0].FindControl("drpCoInsType");


                    chkSubmit = (CheckBox)e.Row.Cells[0].FindControl("chkSubmit");


                    lblDiscType = (Label)e.Row.Cells[0].FindControl("lblDiscType");
                    lblCoInsType = (Label)e.Row.Cells[0].FindControl("lblCoInsType");


                    drpgvDiscType.SelectedValue = lblDiscType.Text;
                    drpCoInsType.SelectedValue = lblCoInsType.Text;


                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.gvInvTran_RowDataBound");
                TextFileWriting(ex.Message.ToString());

            }


        }

        protected void DeletegvInv_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                    if (Convert.ToString(ViewState["InvoiceTrans"]) != null)
                    {
                        DataTable DT = new DataTable();
                        DT = (DataTable)ViewState["InvoiceTrans"];
                        DT.Rows.RemoveAt(gvScanCard.RowIndex);
                        DT.AcceptChanges();
                        ViewState["InvoiceTrans"] = DT;

                    }


                    BindTempInvoiceTrans();

                    AmountCalculation();
                }
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.DeleteDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void txtgvServCode_TextChanged(object sender, EventArgs e)
        {
            //GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent.Parent.Parent;
            //TextBox txtgvServCode = (TextBox)currentRow.FindControl("txtgvServCode");
            //TextBox txtServDesc = (TextBox)currentRow.FindControl("txtServDesc");
            //TextBox txtFee = (TextBox)currentRow.FindControl("txtFee");
            //TextBox txtgvQty = (TextBox)currentRow.FindControl("txtgvQty");
            //TextBox txtHitAmount = (TextBox)currentRow.FindControl("txtHitAmount");

            //Label lblCoverd = (Label)currentRow.FindControl("lblCoverd");
            //Label lblgvCatID = (Label)currentRow.FindControl("lblgvCatID");
            //Label lblServType = (Label)currentRow.FindControl("lblServType");

            //TextBox txtHaadCode = (TextBox)currentRow.FindControl("txtHaadCode");
            //TextBox txtHaadDesc = (TextBox)currentRow.FindControl("txtHaadDesc");


            //TextBox txtTypeValue = (TextBox)currentRow.FindControl("txtTypeValue");

            //TextBox txtObsType = (TextBox)currentRow.FindControl("txtObsType");
            //TextBox txtObsCode = (TextBox)currentRow.FindControl("txtObsCode");
            //TextBox txtObsValue = (TextBox)currentRow.FindControl("txtObsValue");



            //DataSet DSServ = new DataSet();

            //DSServ = GetServiceMasterName("HSM_SERV_ID", txtgvServCode.Text.Trim());

            //string ObsNeeded = "N";
            //if (DSServ.Tables[0].Rows.Count > 0)
            //{
            //    lblgvCatID.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CAT_ID"]);
            //    txtServDesc.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_NAME"]);
            //    lblServType.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_TYPE"]);

            //    txtFee.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_FEE"]);

            //    txtHaadCode.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_HAAD_CODE"]);
            //    //  txtTypeValue.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_CODE_TYPE"]);

            //    ObsNeeded = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBSERVATION_NEEDED"]);
            //    if (ObsNeeded == "Y")
            //    {
            //        txtObsCode.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_CODE"]);
            //        txtObsValue.Text = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_OBS_VALUETYPE"]);

            //        if (lblServType.Text == "L")
            //        {
            //            txtObsType.Text = "LOINC";
            //        }
            //        else
            //        {
            //            txtObsType.Text = "Universal Dental";
            //        }

            //    }


            //}

            //decimal ServPrice = 0;
            //lblCoverd.Text = "U";

            //if (txtSubCompanyID.Text != "")
            //{
            //    DataSet DSCompAgr = new DataSet();

            //    DSCompAgr = GetCompanyAgrDtls(txtgvServCode.Text.Trim(), txtSubCompanyID.Text.Trim());

            //    if (DS.Tables[0].Rows.Count > 0)
            //    {
            //        if (DS.Tables[0].Rows[0].IsNull("HAS_NETAMOUNT") == false)
            //        {
            //            ServPrice = Convert.ToDecimal(DS.Tables[0].Rows[0]["HAS_NETAMOUNT"]);
            //            lblCoverd.Text = Convert.ToString(DS.Tables[0].Rows[0]["HAS_COVERING_TYPE"]);


            //            txtFee.Text = Convert.ToString(ServPrice);

            //        }

            //    }

            //}


            //string strHaadName = "", strHaadType = "";
            //if (txtHaadCode.Text.Trim() != "")
            //{
            //    DataSet DSHaad = new DataSet();
            //    DSHaad = GetHaadName(txtHaadCode.Text.Trim());

            //    if (DSHaad.Tables[0].Rows.Count > 0)
            //    {
            //        strHaadName = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_DESCRIPTION"]);
            //        strHaadType = Convert.ToString(DSHaad.Tables[0].Rows[0]["HHS_TYPE_VALUE"]);
            //    }
            //    txtHaadDesc.Text = strHaadName;
            //    txtTypeValue.Text = strHaadType;

            //}

            //txtgvQty.Text = "1";

            //txtHitAmount.Text = Convert.ToString(Convert.ToDecimal(txtFee.Text.Trim()) * Convert.ToDecimal(txtgvQty.Text.Trim()));

            //AmountCalculation();

        }


        void GridRowRefresh(Int32 intCurRow)
        {


            Label lblServType = (Label)gvInvoiceTrans.Rows[intCurRow].FindControl("lblServType");


            DropDownList drpgvDiscType = (DropDownList)gvInvoiceTrans.Rows[intCurRow].FindControl("drpgvDiscType");

            TextBox txtDiscAmt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtDiscAmt");

            TextBox txtFee = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtFee");
            TextBox txtgvQty = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtgvQty");
            TextBox txtHitAmount = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtHitAmount");
            TextBox txtDeduct = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtDeduct");

            DropDownList drpCoInsType = (DropDownList)gvInvoiceTrans.Rows[intCurRow].FindControl("drpCoInsType");
            TextBox txtCoInsAmt = (TextBox)gvInvoiceTrans.Rows[intCurRow].FindControl("txtCoInsAmt");

            txtHitAmount.Text = Convert.ToString(Convert.ToDecimal(txtFee.Text.Trim()) * Convert.ToDecimal(txtgvQty.Text.Trim()));

            DataTable DT = new DataTable();
            DT = (DataTable)ViewState["InvoiceTrans"];

            string ServTrnt = "", strDeductType = "$";
            ServTrnt = lblServType.Text.Trim();

            decimal decDiscount, decDedAmt = 0, decConInsAmt = 0, ServFee = 0;


            if (drpgvDiscType.SelectedIndex == 0)
            {
                decDiscount = Convert.ToDecimal(txtDiscAmt.Text.Trim());

            }
            else
            {

                decDiscount = ((Convert.ToDecimal(txtFee.Text.Trim()) / 100) * Convert.ToDecimal(txtDiscAmt.Text.Trim()));
            }
            DT.Rows[intCurRow]["HIT_FEE"] = txtFee.Text.Trim();
            DT.Rows[intCurRow]["HIT_DISC_TYPE"] = drpgvDiscType.SelectedValue;
            DT.Rows[intCurRow]["HIT_DISC_AMT"] = txtDiscAmt.Text.Trim();

            DT.Rows[intCurRow]["HIT_QTY"] = txtgvQty.Text.Trim();
            DT.Rows[intCurRow]["HIT_AMOUNT"] = Convert.ToDecimal(txtHitAmount.Text) - (Convert.ToDecimal(txtgvQty.Text.Trim()) * decDiscount);


            if (drpInvType.SelectedValue == "Cash")
            {
                DT.Rows[intCurRow]["HIT_CO_INS_TYPE"] = "%";
                DT.Rows[intCurRow]["HIT_CO_INS_AMT"] = "100";
            }


            //decDedAmt = Convert.ToDecimal(txtDeduct.Text.Trim());
            //decConInsAmt = Convert.ToDecimal(txtCoInsAmt.Text.Trim());
            if (Convert.ToDecimal(DT.Rows[intCurRow]["HIT_DEDUCT"]) == Convert.ToDecimal(txtDeduct.Text))
            {
                strDeductType = Convert.ToString(ViewState["Deductible_Type"]);
                decDedAmt = Convert.ToDecimal(ViewState["Deductible"]);
            }
            else
            {
                decDedAmt = Convert.ToDecimal(txtDeduct.Text);
            }

            if (Convert.ToDecimal(DT.Rows[intCurRow]["HIT_CO_INS_AMT"]) == Convert.ToDecimal(txtCoInsAmt.Text))
            {
                decConInsAmt = Convert.ToDecimal(ViewState["Co_Ins_Amt"]);
            }
            else
            {
                decConInsAmt = Convert.ToDecimal(txtCoInsAmt.Text);
            }

            ServFee = Convert.ToDecimal(DT.Rows[intCurRow]["HIT_AMOUNT"]);// Convert.ToDecimal(txtFee.Text.Trim());


            if (ServTrnt == "C")
            {
                if (strDeductType == "$")
                {
                    DT.Rows[intCurRow]["HIT_DEDUCT"] = decDedAmt.ToString("#0.00"); ;

                    if (txtSubCompanyID.Text != "")
                    {
                        DT.Rows[intCurRow]["HIT_CO_INS_TYPE"] = "$";
                        DT.Rows[intCurRow]["HIT_CO_INS_AMT"] = decDedAmt.ToString("#0.00");
                    }
                }
                else
                {
                    DT.Rows[intCurRow]["HIT_DEDUCT"] = Convert.ToDecimal((ServFee * decDedAmt) / 100).ToString("#0.00");// Convert.ToString(ViewState["Co_Ins_Amt"]);
                    if (txtSubCompanyID.Text != "")
                    {
                        DT.Rows[intCurRow]["HIT_CO_INS_TYPE"] = "$";
                        DT.Rows[intCurRow]["HIT_CO_INS_AMT"] = Convert.ToDecimal((ServFee * decDedAmt) / 100).ToString("#0.00");// Convert.ToString(ViewState["Co_Ins_Amt"]);
                    }



                }


            }
            else
            {
                DT.Rows[intCurRow]["HIT_DEDUCT"] = "0";
                if (txtSubCompanyID.Text != "")
                {
                    DT.Rows[intCurRow]["HIT_CO_INS_TYPE"] = drpCoInsType.SelectedValue;// Convert.ToString(ViewState["Co_Ins_Type"]);
                }


                if (strDeductType == "$")
                {
                    if (txtSubCompanyID.Text != "")
                    {
                        DT.Rows[intCurRow]["HIT_CO_INS_AMT"] = decConInsAmt.ToString("#0.00");
                    }
                }
                else
                {
                    if (txtSubCompanyID.Text != "")
                    {
                        DT.Rows[intCurRow]["HIT_CO_INS_AMT"] = decConInsAmt.ToString("#0.00"); ; // Convert.ToString(ViewState["Co_Ins_Amt"]);
                    }
                }
            }

            //DT.Rows[intCurRow]["HIT_DEDUCT"] = Convert.ToDecimal(DT.Rows[intCurRow]["HIT_DEDUCT"]) * Convert.ToDecimal(DT.Rows[intCurRow]["HIT_QTY"]);

            //if (Convert.ToString(DT.Rows[intCurRow]["HIT_CO_INS_TYPE"]) == "$")
            //{
            //    DT.Rows[intCurRow]["HIT_CO_INS_AMT"] = Convert.ToDecimal(DT.Rows[intCurRow]["HIT_CO_INS_AMT"]) * Convert.ToDecimal(DT.Rows[intCurRow]["HIT_QTY"]);

            //}

            DT.AcceptChanges();
            ViewState["InvoiceTrans"] = DT;
            BindTempInvoiceTrans();
            AmountCalculation();

        }

        protected void gvInvoiceTrans_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent.Parent.Parent;
                Int32 R = currentRow.RowIndex;
                GridRowRefresh(R);
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.gvInvoiceTrans_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void drpgvDiscType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow currentRow = (GridViewRow)((DropDownList)sender).Parent.Parent.Parent.Parent;
                Int32 R = currentRow.RowIndex;
                GridRowRefresh(R);
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      eAuthApproval.drpgvDiscType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpCoInsType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow currentRow = (GridViewRow)((DropDownList)sender).Parent.Parent.Parent.Parent;
                Int32 R = currentRow.RowIndex;
                GridRowRefresh(R);
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.drpCoInsType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtgvDrCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent.Parent.Parent;
                Int32 intCurRow = currentRow.RowIndex;

                TextBox txtgvDrCode = (TextBox)currentRow.FindControl("txtgvDrCode");
                TextBox txtgvDrName = (TextBox)currentRow.FindControl("txtgvDrName");
                string strDrCode = "", strDrName = "";
                strDrCode = txtgvDrCode.Text.Trim();

                GetStaffName(strDrCode, out strDrName);
                txtgvDrName.Text = strDrName;

                //  GetJobnumber();

                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["InvoiceTrans"];
                DT.Rows[intCurRow]["HIT_DR_CODE"] = txtgvDrCode.Text;
                DT.Rows[intCurRow]["HIT_DR_NAME"] = txtgvDrName.Text;

                DT.AcceptChanges();
                ViewState["InvoiceTrans"] = DT;
                BindTempInvoiceTrans();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtgvDrCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }



        protected void txtObsCode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent.Parent.Parent;
                Int32 intCurRow = currentRow.RowIndex;
                TextBox txtObsCode = (TextBox)currentRow.FindControl("txtObsCode");
                TextBox txtObsDesc = (TextBox)currentRow.FindControl("txtObsDesc");
                TextBox txtObsValue = (TextBox)currentRow.FindControl("txtObsValue");
                TextBox txtObsValueType = (TextBox)currentRow.FindControl("txtObsValueType");


                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["InvoiceTrans"];
                DT.Rows[intCurRow]["HIO_SERV_CODE"] = txtObsCode.Text;
                DT.Rows[intCurRow]["HIO_DESCRIPTION"] = txtObsDesc.Text;
                DT.Rows[intCurRow]["HIO_VALUE"] = txtObsValue.Text;
                DT.Rows[intCurRow]["HIO_VALUETYPE"] = txtObsValueType.Text;

                DT.AcceptChanges();
                ViewState["InvoiceTrans"] = DT;
                BindTempInvoiceTrans();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtObsCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }



        protected void txtHospDisc_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //decimal decBill2 = 0;



                //if (txtBillAmt1.Text.Trim() != "" && txtHospDisc.Text.Trim() != "")
                //{
                //    decimal decHosDisc = Convert.ToDecimal(txtHospDisc.Text.Trim());

                //    txtHospDisc.Text = decHosDisc.ToString("#0.00");

                //    if (drpHospDiscType.SelectedIndex == 0)
                //    {
                //        decBill2 = Convert.ToDecimal(txtBillAmt1.Text.Trim()) - Convert.ToDecimal(txtHospDisc.Text.Trim());

                //    }
                //    else
                //    {

                //        decBill2 = Convert.ToDecimal(txtBillAmt1.Text.Trim()) - ((Convert.ToDecimal(txtBillAmt1.Text.Trim()) / 100) * Convert.ToDecimal(txtHospDisc.Text.Trim()));
                //    }

                //    txtBillAmt2.Text = decBill2.ToString("#0.00");

                //    decimal decBill3 = 0;
                //    if (txtBillAmt2.Text.Trim() != "" && txtDeductible.Text.Trim() != "")
                //    {
                //        decBill3 = Convert.ToDecimal(txtBillAmt2.Text.Trim()) - Convert.ToDecimal(txtDeductible.Text.Trim());
                //    }
                //    txtBillAmt3.Text = decBill3.ToString("#0.00");

                //}

                AmountCalculation();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtHospDisc_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtSplDisc_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //decimal decPaidAmt = Convert.ToDecimal(txtCoInsTotal.Text.Trim());

                //if (txtSplDisc.Text.Trim() != "")
                //{
                //    decPaidAmt = decPaidAmt - Convert.ToDecimal(txtSplDisc.Text.Trim());
                //}

                //if (txtPTCredit.Text.Trim() != "")
                //{
                //    decPaidAmt = decPaidAmt - Convert.ToDecimal(txtPTCredit.Text.Trim());
                //}

                //txtPaidAmount.Text = decPaidAmt.ToString("#0.00");

                AmountCalculation();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtSplDisc_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void TxtRcvdAmt_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal decPTCredit = 0, decRcvdAmt = 0;

                if (txtPTCredit.Text.Trim() != "")
                    decPTCredit = Convert.ToDecimal(txtPTCredit.Text.Trim());

                if (TxtRcvdAmt.Text.Trim() != "")
                    decRcvdAmt = Convert.ToDecimal(TxtRcvdAmt.Text.Trim());

                decPTCredit = decPTCredit - decRcvdAmt;

                txtPTCredit.Text = decPTCredit.ToString("#0.00");

                //  AmountCalculation();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.TxtRcvdAmt_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {

                Clear();

                New();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnNew_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //if (IsPatientBilledSameDaySameDr(true) == true)
                //{
                //    goto FunEnd;
                //}

                Boolean isError = false;
                isError = fnSave();

                if (isError == false)
                {
                    Clear();
                    New();

                    // MPExtMessage.Show();
                    // strMessage = "Details Saved !";
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
                }
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnSave");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {

                fnSave();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnSave");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToBoolean(ViewState["NewFlag"]) == true)
                {
                    goto FunEnd;
                }
                objCom = new CommonBAL();
                if ((GlobalValues.FileDescription == "ANP" || GlobalValues.FileDescription == "ALNOORSATWA") && Convert.ToString(Session["User_ID"]).ToUpper() != "ADMIN" && drpInvType.SelectedValue == "Cash")
                {
                    string dtInvDate = Convert.ToString(txtInvDate.Text.Trim());
                    DateTime Today;

                    Today = Convert.ToDateTime(objCom.fnGetDate("dd/MM/yyyy"));


                    Int32 intDatediff = 0;
                    intDatediff = objCom.fnGetDateDiff(Convert.ToString(Today), dtInvDate);


                    if (intDatediff > 0)
                    {
                        lblStatus.Text = "You cannot delete Cash Invoice";
                        goto FunEnd;

                    }
                }

                if (GlobalValues.FileDescription == "ALREEM" && Convert.ToBoolean("NewFlag") == false)
                {
                    if (txtRemarks.Text.Trim() == "")
                    {
                        lblStatus.Text = "Remarks field should be non-empty for deleting Invoice. ";
                        goto FunEnd;
                    }

                }

                IP_Invoice objInv = new IP_Invoice();

                DataSet DS = new DataSet();

                string Criteria = "1=1";
                Criteria += " AND HIM_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "' AND HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                DS = objInv.InvoiceMasterGet(Criteria);
                if (DS.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToString(DS.Tables[0].Rows[0]["HIM_COMMIT_FLAG"]).ToUpper() == "Y" && Convert.ToString(Session["User_ID"]).ToUpper() != "ADMIN")
                    {
                        lblStatus.Text = "This Invoice is Commited. So you cannot delete this invoice.";
                        goto FunEnd;
                    }

                    objInv.HIM_INVOICE_ID = txtInvoiceNo.Text.Trim();
                    objInv.HIM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objInv.HIM_RECEIPT_NO = hidReceipt.Value;
                    objInv.InvoiceMasterDelete();

                }
                Clear();
                New();
                lblStatus.Text = "Invoice Deleted.";
                lblStatus.ForeColor = System.Drawing.Color.Green;

            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnDelete_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {

            Clear();

            New();
        }

        protected void btnPrntDeductible_Click(object sender, EventArgs e)
        {
            try
            {
                fnPrint(txtInvoiceNo.Text.Trim());
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnPrntDeductible_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void txtRefDrId_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtRefDrId.Text.Trim() != "" && GlobalValues.FileDescription == "ALREEM")
                {
                    BindCommissionType();
                }
                string strDrCode = "", strDrName = "";
                strDrCode = txtRefDrId.Text.Trim();

                GetRefDrName(strDrCode, out strDrName);
                txtRefDrName.Text = strDrName;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtRefDrId_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void drpPayType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                divCC.Visible = false;
                divCheque.Visible = false;
                divAdvane.Visible = false;
                if (drpPayType.SelectedItem.Text == "Multi Currency")
                {

                }
                else if (drpPayType.SelectedItem.Text == "Credit Card")
                {
                    divCC.Visible = true;
                    BIndCCType();
                    if (txtCCAmt.Text.Trim() == "0")
                    {
                        txtCCAmt.Text = txtPaidAmount.Text;

                    }
                }
                else if (drpPayType.SelectedItem.Text == "Cheque" || drpPayType.SelectedItem.Text == "PDC")
                {
                    divCheque.Visible = true;
                    if (txtChqAmt.Text.Trim() == "0")
                    {
                        txtChqAmt.Text = txtPaidAmount.Text;

                    }

                }
                else if (drpPayType.SelectedItem.Text == "Debit Card")
                {
                    drpccType.Items.Clear();
                    divCC.Visible = true;


                    drpccType.Items.Insert(0, "Salary Card");
                    drpccType.Items[0].Value = "Salary Card";

                    drpccType.Items.Insert(1, "Knet");
                    drpccType.Items[1].Value = "Knet";

                    if (txtCCAmt.Text.Trim() == "0")
                    {
                        txtCCAmt.Text = txtPaidAmount.Text;

                    }
                }
                else if (drpPayType.SelectedItem.Text == "Advance")
                {
                    divAdvane.Visible = true;
                    fnListAdvanceDtls();
                }
                else if (drpPayType.SelectedItem.Text.ToUpper() == "UTILISE ADVANCE")
                {

                }
                else
                {
                    decimal decPaidAmt = 0, decccAmt = 0, decChqAmt = 0, decCashAmt = 0;

                    if (txtPaidAmount.Text.Trim() != "")
                    {
                        decPaidAmt = Convert.ToDecimal(txtPaidAmount.Text.Trim());
                    }

                    if (txtCCAmt.Text.Trim() != "")
                    {
                        decccAmt = Convert.ToDecimal(txtCCAmt.Text.Trim());
                    }

                    if (txtChqAmt.Text.Trim() != "")
                    {
                        decChqAmt = Convert.ToDecimal(txtChqAmt.Text.Trim());
                    }

                    decCashAmt = decPaidAmt - decccAmt - decChqAmt;

                    txtCashAmt.Text = decCashAmt.ToString("#0.00");
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.drpPayType_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void txtConvRate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (drpPayType.SelectedValue == "Multi Currency")
                {
                    txtcurValue.Text = Convert.ToString(Convert.ToDecimal(txtRcvdAmount.Text) * Convert.ToDecimal(txtConvRate.Text));

                    if (Convert.ToDecimal(txtRcvdAmount.Text) > 0)
                    {
                        LblMBalAmt.Text = Convert.ToString(Convert.ToDecimal(txtcurValue.Text) - Convert.ToDecimal(txtPaidAmount.Text));
                        txtCashAmt.Text = Convert.ToString(Convert.ToDecimal(txtPaidAmount.Text) - Convert.ToDecimal(txtcurValue.Text) + Convert.ToDecimal(LblMBalAmt.Text));
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtConvRate_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void txtCCAmt_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal decPaidAmt = 0, decCCAmt = 0, decChqAmt = 0, decCashAmt = 0;

                if (txtPaidAmount.Text.Trim() != "")
                {
                    decPaidAmt = Convert.ToDecimal(txtPaidAmount.Text.Trim());
                }



                if (txtCCAmt.Text.Trim() != "")
                {
                    decCCAmt = Convert.ToDecimal(txtCCAmt.Text.Trim());
                }



                if (txtChqAmt.Text.Trim() != "")
                {
                    decChqAmt = Convert.ToDecimal(txtChqAmt.Text.Trim());
                }

                decCashAmt = decPaidAmt - decCCAmt - decChqAmt;

                txtCashAmt.Text = decCashAmt.ToString("#0.00");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtCCAmt_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }


        protected void txtChqAmt_TextChanged(object sender, EventArgs e)
        {
            try
            {
                decimal decPaidAmt = 0, decChqAmt = 0, decCashAmt = 0;

                if (txtPaidAmount.Text.Trim() != "")
                {
                    decPaidAmt = Convert.ToDecimal(txtPaidAmount.Text.Trim());
                }

                if (txtChqAmt.Text.Trim() != "")
                {
                    decChqAmt = Convert.ToDecimal(txtChqAmt.Text.Trim());
                }

                decCashAmt = decPaidAmt - decChqAmt;

                txtCashAmt.Text = decCashAmt.ToString("#0.00");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtChqAmt_TextChanged");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void btnEditPTPopup_Click(object sender, EventArgs e)
        {
            try
            {
                objCom = new CommonBAL();
                string Criteria = " 1=1 ";
                DataSet ds1 = new DataSet();
                Criteria = " HPM_PT_ID  ='" + txtFileNo.Text.Trim() + "'";
                ds1 = objCom.PatientMasterGet(Criteria);
                if (ds1.Tables[0].Rows.Count <= 0)
                {
                    goto FunEnd;

                }

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "PTPopup('" + txtFileNo.Text.Trim() + "');", true);


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnEditPTPopup_Click");
                TextFileWriting(ex.Message.ToString());

            }
        }

        protected void DeletegvDiag_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                    if (Convert.ToString(ViewState["InvoiceClaims"]) != null)
                    {
                        DataTable DT = new DataTable();
                        DT = (DataTable)ViewState["InvoiceClaims"];
                        DT.Rows.RemoveAt(gvScanCard.RowIndex);
                        DT.AcceptChanges();
                        ViewState["InvoiceClaims"] = DT;

                    }


                    BindTempInvoiceClaims();


                }
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.DeletegvDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void btnImportSO_Click(object sender, EventArgs e)
        {
            try
            {

                BuildeInvoiceTrans();
                BuildeInvoiceClaims();


                if (Convert.ToString(ViewState["IAS_DRG_CODE"]) != "")
                {
                    DataSet DSServ = new DataSet();
                    DSServ = GetServiceMasterName("HSM_SERV_ID", Convert.ToString(ViewState["IAS_DRG_CODE"]));

                    if (DSServ.Tables[0].Rows.Count > 0)
                    {
                        string ServID = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_SERV_ID"]);
                        string ServName = Convert.ToString(DSServ.Tables[0].Rows[0]["HSM_NAME"]);

                        fnServiceAdd(ServID, ServName);
                    }

                }

                BindSalesOrderServ();
                BindSalesOrderDiag();
                //  System.Threading.Thread.Sleep(5000);
                // Hide image
                // divLoading.Style.Add("display", "none");
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.btnImportSO_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void txtTransDate_TextChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow currentRow = (GridViewRow)((TextBox)sender).Parent.Parent.Parent.Parent;
                Int32 intCurRow = currentRow.RowIndex;

                TextBox txtTransDate = (TextBox)currentRow.FindControl("txtTransDate");

                DataTable DT = new DataTable();
                DT = (DataTable)ViewState["InvoiceTrans"];
                DT.Rows[intCurRow]["HIT_TRANS_DATEDesc"] = txtTransDate.Text;

                DT.AcceptChanges();
                ViewState["InvoiceTrans"] = DT;
                BindTempInvoiceTrans();
            }
            catch (Exception ex)
            {

                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Invoice.txtgvDrCode_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


    }
}