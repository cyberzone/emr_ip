﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using EMR_IP_BAL;
using System.IO;


namespace EMR_IP.CReports
{
    public partial class ReportViewer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            ReportDocument crystalReport = new ReportDocument();
            string ReportName = (String)Request.Params["ReportName"] == null ? "" : Request.Params["ReportName"];
            crystalReport.Load(@GlobalValues.REPORT_PATH + ReportName);
            crystalReport.SetDatabaseLogon(GlobalValues.DB_USERNAME, GlobalValues.DB_PASSWORD, @GlobalValues.DB_SERVER1, GlobalValues.DB_DATABASE);
            String SelectionFormula = (String)Request.Params["SelectionFormula"] == null ? "" : Request.Params["SelectionFormula"];
            crystalReport.RecordSelectionFormula = SelectionFormula;
            //ReportViewer.ReportSource = crystalReport;

            // convert rpt to pdf binary content.
            byte[] pdfContent = null;
            using (MemoryStream ms = (MemoryStream)crystalReport.ExportToStream(ExportFormatType.PortableDocFormat))
            {
                pdfContent = ms.ToArray();
                ms.Close();

            }
            crystalReport.Close();
            crystalReport.Dispose();


            Response.ClearHeaders();
            Response.ClearContent();
            Response.AppendHeader("Content-Type", "application/pdf");
            Response.BinaryWrite(pdfContent);
        }
    }
}