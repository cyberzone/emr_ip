﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP
{
    public partial class CommonPageLoader : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();
        void BindStaffData()
        {
            DataSet DS = new DataSet();

        
            string Criteria = " 1=1  ";
            Criteria += " AND HSFM_STAFF_ID='" + Convert.ToString(Session["User_Code"]) + "'";

            DS = objCom.GetStaffMaster(Criteria);
            DataRow[] result = DS.Tables[0].Select(Criteria);
            foreach (DataRow DR in result)
            {

                Session["FullName"] = Convert.ToString(DR["FullName"]);
                Session["User_DeptID"] = Convert.ToString(DR["HSFM_DEPT_ID"]);
                Session["User_Category"] = Convert.ToString(DR["HSFM_CATEGORY"]);
                //txtStaffName.Text = Convert.ToString(DR["FullName"]);
            }

        }

        void BindUserData()
        {

             string Criteria = " 1=1 ";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"] )+ "' AND HUM_USER_ID ='" + Convert.ToString(Session["User_ID"]) + "'";

            DataSet ds = new DataSet();
            //ds = dbo.UserMasterGet(drpBranch.SelectedValue, drpUsers.SelectedValue , txtPassword.Text);
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                
                Session["User_ID"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_USER_ID"]);
                Session["User_Code"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_REMARK"]);
                Session["User_Name"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_USER_NAME"]);
                Session["User_Active"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_STATUS"]);
                Session["Roll_Id"] = Convert.ToString(ds.Tables[0].Rows[0]["HGP_ROLL_ID"]);

              
                if (ds.Tables[0].Columns.Contains("HUM_REFRESH_INTERVAL") == true)
                {
                    if (ds.Tables[0].Rows[0].IsNull("HUM_REFRESH_INTERVAL") == false && Convert.ToString(ds.Tables[0].Rows[0]["HUM_REFRESH_INTERVAL"]) != "")
                    {
                        Session["HUM_REFRESH_INTERVAL"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_REFRESH_INTERVAL"]);
                    }
                    else
                    {
                        Session["HUM_REFRESH_INTERVAL"] = "0";
                    }
                }

                if (ds.Tables[0].Columns.Contains("HUM_ADDNEW_RECORD_FROMEMR") == true)
                {
                    if (ds.Tables[0].Rows[0].IsNull("HUM_ADDNEW_RECORD_FROMEMR") == false && Convert.ToString(ds.Tables[0].Rows[0]["HUM_ADDNEW_RECORD_FROMEMR"]) != "")
                    {
                        Session["HUM_ADDNEW_RECORD_FROMEMR"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_ADDNEW_RECORD_FROMEMR"]);
                    }
                    else
                    {
                        Session["HUM_ADDNEW_RECORD_FROMEMR"] = "False";
                    }
                }

            }
        }
        void BindSystemOption()
        {
            string Criteria = " 1=1 ";

            DataSet DS = new DataSet();
            DS = objCom.SystemOptionGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEF_CUR_ID") == false)
                {
                    Session["HSOM_DEF_CUR_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DEF_CUR_ID"]);

                }
                // 
                if (DS.Tables[0].Rows[0].IsNull("HSOM_AUTO_SL_NO") == false)
                {
                    Session["AutoSlNo"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_AUTO_SL_NO"]);

                }


                //PATIENT VISIT STATUS IS 'F' IF IT IS Y ELSE IT WILL BE 'W'
                if (DS.Tables[0].Rows[0].IsNull("HSOM_FILING_REQUEST") == false)
                {
                    Session["FilingRequest"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_FILING_REQUEST"]);

                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_TOKEN_PRINT") == false)
                {
                    Session["TokenPrint"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_TOKEN_PRINT"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_ID_PRINT") == false)
                {
                    Session["IdPrint"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ID_PRINT"]);

                }
                //CLEAR THE DATA AFTER SAVING
                if (DS.Tables[0].Rows[0].IsNull("HSOM_CLEAR_AFT_SAVE") == false)
                {
                    Session["ClearAfterSave"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_CLEAR_AFT_SAVE"]);

                }

                // 
                if (DS.Tables[0].Rows[0].IsNull("HSOM_SCAN_SAVE") == false)
                {
                    Session["ScanSave"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_SCAN_SAVE"]);

                }
                else
                {
                    Session["ScanSave"] = "0";
                }

                //REVISIT DAYS 
                if (DS.Tables[0].Rows[0].IsNull("HSOM_REVISIT_DAYS") == false)
                {
                    Session["RevisitDays"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_REVISIT_DAYS"]);

                }


                //NETWORK NAME  DROPDWON IS MANDATORY OR NOT
                if (DS.Tables[0].Rows[0].IsNull("HSOM_PACKAGE_MANDITORY_PTREG") == false)
                {
                    Session["PackageManditory"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_PACKAGE_MANDITORY_PTREG"]);


                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_MULTIPLEVISITADAY") == false)
                {
                    Session["MultipleVisitADay"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_MULTIPLEVISITADAY"]);


                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_PTNAME_MULTIPLE") == false)
                {
                    Session["PTNameMultiple"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_PTNAME_MULTIPLE"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("hsom_label_copies") == false)
                {
                    Session["LabelCopies"] = Convert.ToString(DS.Tables[0].Rows[0]["hsom_label_copies"]);

                }
                else
                {
                    Session["LabelCopies"] = 1;
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEFAULT_TYPE") == false)
                {
                    Session["DefaultType"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DEFAULT_TYPE"]);

                }
                else
                {
                    Session["DefaultType"] = "false";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_SEARCH_OPT") == false)
                {
                    Session["SearchOption"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_SEARCH_OPT"]);

                }
                else
                {
                    Session["SearchOption"] = "AN";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_FILENO_MONTHYEAR") == false)
                {
                    Session["FileNoMonthYear"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_FILENO_MONTHYEAR"]);

                }
                else
                {
                    Session["FileNoMonthYear"] = "N";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_REPORT_PATH") == false)
                {
                    Session["ReportPath"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_REPORT_PATH"]);

                }
                else
                {
                    Session["ReportPath"] = "D:\\HMS\\Reports";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_TOKEN_PRINT") == false)
                {
                    Session["TokenPrint"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_TOKEN_PRINT"]);

                }
                else
                {
                    Session["TokenPrint"] = "";
                }



                if (DS.Tables[0].Rows[0].IsNull("HSOM_ID_PRINT") == false)
                {
                    Session["IDPrint"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ID_PRINT"]);

                }
                else
                {
                    Session["IDPrint"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_COINS_PTREG") == false)
                {
                    Session["ConInsPTReg"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_COINS_PTREG"]);

                }
                else
                {
                    Session["ConInsPTReg"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_SHOW_TOKEN") == false)
                {
                    Session["ShowToken"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_SHOW_TOKEN"]);

                }
                else
                {
                    Session["ShowToken"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_DSPLY_DR_PTREG") == false)
                {
                    Session["DrNameDisplay"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DSPLY_DR_PTREG"]);

                }
                else
                {
                    Session["DrNameDisplay"] = "";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_PRIORITY_OPT_FILING") == false)
                {
                    Session["PriorityDisplay"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_PRIORITY_OPT_FILING"]);

                }
                else
                {
                    Session["PriorityDisplay"] = "";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_KNOW_FROM") == false)
                {
                    Session["KnowFromDisplay"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_KNOW_FROM"]);

                }
                else
                {
                    Session["KnowFromDisplay"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEP_WISE_PTID") == false)
                {
                    Session["DeptWisePTId"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DEP_WISE_PTID"]);

                }
                else
                {
                    Session["DeptWisePTId"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_INTERVAL") == false)
                {
                    Session["AppointmentInterval"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_INTERVAL"]);

                }
                else
                {
                    Session["AppointmentInterval"] = "30";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_START") == false)
                {
                    Session["AppointmentStart"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_START"]);

                }
                else
                {
                    Session["AppointmentStart"] = "9";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_END") == false)
                {
                    Session["AppointmentEnd"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_END"]);

                }
                else
                {
                    Session["AppointmentEnd"] = "21";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_HEIGHT") == false)
                {
                    Session["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_HEIGHT"]);

                }
                else
                {
                    Session["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = "30";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DR_DSLY_WIDTH") == false)
                {
                    Session["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]);

                }
                else
                {
                    Session["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = "1900";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_FORMAT") == false)
                {
                    Session["HSOM_APPOINTMENT_DSPLY_FORMAT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_FORMAT"]);

                }
                else
                {
                    Session["HSOM_APPOINTMENT_DSPLY_FORMAT"] = "12";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_ECLAIM") == false)
                {
                    Session["HSOM_ECLAIM"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ECLAIM"]);

                }
                else
                {
                    Session["HSOM_ECLAIM"] = "";
                }






            }

        }
        
        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_IP' ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            Session["EMR_IP_DISC_LAMA_FILE_PATH"] = "0";
            Session["EMR_IP_ANES_CHART_FILE_PATH"] = "0";
            Session["HMS_TIME_INTERVAL"] = "5";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_IP_DISC_LAMA_FILE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_IP_DISC_LAMA_FILE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_IP_ANES_CHART_FILE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_IP_ANES_CHART_FILE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "HMS_TIME_INTERVAL")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["HMS_TIME_INTERVAL"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                }
            }



        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindSystemOption();
            BindScreenCustomization();


            ViewState["BranchID"] = "0";
            ViewState["PageName"] = "0";
            ViewState["DrId"] = "0";
            ViewState["User_Name"] = "0";
            ViewState["Roll_Id"] = "0";
           


            //LoginFrom=CommonPage
            ViewState["LoginFrom"] = "EMR";


            if (Convert.ToString(Request.QueryString["BranchID"]) != "0")
            {
                ViewState["BranchID"] = Convert.ToString(Request.QueryString["BranchID"]);
            }

            if (Convert.ToString(Request.QueryString["User_ID"]) != "0")
            {
                ViewState["User_ID"] = Convert.ToString(Request.QueryString["User_ID"]);
            }


            if (Convert.ToString(Request.QueryString["PageName"]) != "0")
            {
                ViewState["PageName"] = Convert.ToString(Request.QueryString["PageName"]);
            }

            //if (Convert.ToString(Request.QueryString["User_Code"]) != "0")
            //{
            //    ViewState["User_Code"] = Convert.ToString(Request.QueryString["User_Code"]);
            //}

            //if (Convert.ToString(Request.QueryString["User_Name"]) != "0")
            //{
            //    ViewState["User_Name"] = Convert.ToString(Request.QueryString["User_Name"]);
            //}
            //if (Convert.ToString(Request.QueryString["Roll_Id"]) != "0")
            //{
            //    ViewState["Roll_Id"] = Convert.ToString(Request.QueryString["Roll_Id"]);
            //}
            //if (Convert.ToString(Request.QueryString["User_DeptID"]) != "0")
            //{
            //    ViewState["User_DeptID"] = Convert.ToString(Request.QueryString["User_DeptID"]);
            //}



            if (Convert.ToString(Request.QueryString["LoginFrom"]) != "")
            {
                ViewState["LoginFrom"] = Convert.ToString(Request.QueryString["LoginFrom"]);
            }



            Session["Branch_ID"] = Convert.ToString(ViewState["BranchID"]);
            Session["User_ID"] = Convert.ToString(ViewState["User_ID"]);
            Session["LoginFrom"] = Convert.ToString(ViewState["LoginFrom"]);

          string  EMR_ID     = Convert.ToString(Request.QueryString["EMR_ID"]);
          string EMR_PT_ID = Convert.ToString(Request.QueryString["EMR_PT_ID"]);
          string DR_ID = Convert.ToString(Request.QueryString["DR_ID"]);
          string IAS_ADMISSION_NO  = Convert.ToString(Request.QueryString["IAS_ADMISSION_NO"]);


          BindUserData();
          BindStaffData();
          
            if (Convert.ToString(ViewState["PageName"]) == "MRDAuditIP")
            {
                Response.Redirect("WebReports/IPClinicalSummary.aspx?PageName=LabInvestProfileMaster&EMR_ID=" + EMR_ID + "&EMR_PT_ID=" + EMR_PT_ID + "&DR_ID=" + DR_ID + "&ADMISSION_NO=" + IAS_ADMISSION_NO);
            }
            else
            {

                Response.Redirect("Registration/PatientWaitingList.aspx");
            }


        }
    }
}