﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Login.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="EMR_IP.Default" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="LoginContent" runat="server">
  
        <table class="table" cellpadding="5" cellspacing="5">
            <tr>
                <td>
                    <label for="BranchID">Branch Name</label></td>
                <td>
                    <asp:DropDownList ID="drpBranch" runat="server" Width="250px">
                   
                </asp:DropDownList>
                </td>
            </tr>
           
            <tr>
                <td>
                    <label for="UserID">User Name</label></td>
                <td>
                    <asp:DropDownList ID="drpUsers" runat="server"  Width="250px" AutoPostBack="false"  >
                          <asp:ListItem Selected="True">Select</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
         
            <tr>
                <td>
                    <label for="Password">Password</label></td>
                <td>
                    <asp:TextBox id="txtPassword" runat="server" TextMode="Password" Width="245px" MaxLength="15"   ></asp:TextBox>


                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                   
                       <asp:Button ID="btnLogin" runat="server" CssClass="button blue"  Height="30px"   Width="150px" 
                                    onclick="btnLogin_Click" Text="Login" />

                </td>
            </tr>
        </table>
    
</asp:Content>
