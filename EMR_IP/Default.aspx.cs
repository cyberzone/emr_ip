﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;



namespace EMR_IP
{
    public partial class Default : System.Web.UI.Page
    {

        CommonBAL objCom = new CommonBAL();

        #region Methods
        void BindBranch()
        {

            DataSet ds = new DataSet();
            ds = objCom.BranchMasterGet();
            if (ds.Tables[0].Rows.Count > 0)
            {
                drpBranch.DataSource = ds;
                drpBranch.DataValueField = "HBM_ID";
                drpBranch.DataTextField = "HBM_ID";
                drpBranch.DataBind();

            }

        }

        void GetBranchDtls()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " HBM_STATUS='A' AND HBM_ID='" + drpBranch.SelectedValue + "'";
            DS = objCom.fnGetFieldValue("*", "HMS_BRANCH_MASTER", Criteria, "");


            if (DS.Tables[0].Rows.Count > 0)
            {
                Session["Branch_ProviderID"] = Convert.ToString(DS.Tables[0].Rows[0]["HBM_COMM_LIC_NO"]);
                Session["Branch_Name"] = Convert.ToString(DS.Tables[0].Rows[0]["HBM_NAME"]);


            }

        }

        void BindUsers()
        {
            string Criteria = " 1=1 AND HUM_STATUS='A' ";
            Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "'";
            Criteria += " AND ( HUM_REMARK IN (SELECT HSFM_STAFF_ID FROM HMS_STAFF_MASTER WHERE HSFM_CATEGORY IN ('Doctors','Nurse') OR HUM_USER_ID ='ADMIN')) ";


            DataSet ds = new DataSet();
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpUsers.DataSource = ds;
                drpUsers.DataValueField = "HUM_USER_ID";
                drpUsers.DataTextField = "HUM_USER_NAME";
                drpUsers.DataBind();

            }
            //drpUsers.Items.Insert(0, "Select Username");
            //drpUsers.Items[0].Value = "0";


        }


        void BindSystemOption()
        {
            string Criteria = " 1=1 ";

            DataSet DS = new DataSet();
            DS = objCom.SystemOptionGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEF_CUR_ID") == false)
                {
                    Session["HSOM_DEF_CUR_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DEF_CUR_ID"]);

                }
                // 
                if (DS.Tables[0].Rows[0].IsNull("HSOM_AUTO_SL_NO") == false)
                {
                    Session["AutoSlNo"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_AUTO_SL_NO"]);

                }


                //PATIENT VISIT STATUS IS 'F' IF IT IS Y ELSE IT WILL BE 'W'
                if (DS.Tables[0].Rows[0].IsNull("HSOM_FILING_REQUEST") == false)
                {
                    Session["FilingRequest"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_FILING_REQUEST"]);

                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_TOKEN_PRINT") == false)
                {
                    Session["TokenPrint"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_TOKEN_PRINT"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_ID_PRINT") == false)
                {
                    Session["IdPrint"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ID_PRINT"]);

                }
                //CLEAR THE DATA AFTER SAVING
                if (DS.Tables[0].Rows[0].IsNull("HSOM_CLEAR_AFT_SAVE") == false)
                {
                    Session["ClearAfterSave"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_CLEAR_AFT_SAVE"]);

                }

                // 
                if (DS.Tables[0].Rows[0].IsNull("HSOM_SCAN_SAVE") == false)
                {
                    Session["ScanSave"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_SCAN_SAVE"]);

                }
                else
                {
                    Session["ScanSave"] = "0";
                }

                //REVISIT DAYS 
                if (DS.Tables[0].Rows[0].IsNull("HSOM_REVISIT_DAYS") == false)
                {
                    Session["RevisitDays"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_REVISIT_DAYS"]);

                }


                //NETWORK NAME  DROPDWON IS MANDATORY OR NOT
                if (DS.Tables[0].Rows[0].IsNull("HSOM_PACKAGE_MANDITORY_PTREG") == false)
                {
                    Session["PackageManditory"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_PACKAGE_MANDITORY_PTREG"]);


                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_MULTIPLEVISITADAY") == false)
                {
                    Session["MultipleVisitADay"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_MULTIPLEVISITADAY"]);


                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_PTNAME_MULTIPLE") == false)
                {
                    Session["PTNameMultiple"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_PTNAME_MULTIPLE"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("hsom_label_copies") == false)
                {
                    Session["LabelCopies"] = Convert.ToString(DS.Tables[0].Rows[0]["hsom_label_copies"]);

                }
                else
                {
                    Session["LabelCopies"] = 1;
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEFAULT_TYPE") == false)
                {
                    Session["DefaultType"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DEFAULT_TYPE"]);

                }
                else
                {
                    Session["DefaultType"] = "false";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_SEARCH_OPT") == false)
                {
                    Session["SearchOption"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_SEARCH_OPT"]);

                }
                else
                {
                    Session["SearchOption"] = "AN";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_FILENO_MONTHYEAR") == false)
                {
                    Session["FileNoMonthYear"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_FILENO_MONTHYEAR"]);

                }
                else
                {
                    Session["FileNoMonthYear"] = "N";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_REPORT_PATH") == false)
                {
                    Session["ReportPath"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_REPORT_PATH"]);

                }
                else
                {
                    Session["ReportPath"] = "D:\\HMS\\Reports";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_TOKEN_PRINT") == false)
                {
                    Session["TokenPrint"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_TOKEN_PRINT"]);

                }
                else
                {
                    Session["TokenPrint"] = "";
                }



                if (DS.Tables[0].Rows[0].IsNull("HSOM_ID_PRINT") == false)
                {
                    Session["IDPrint"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ID_PRINT"]);

                }
                else
                {
                    Session["IDPrint"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_COINS_PTREG") == false)
                {
                    Session["ConInsPTReg"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_COINS_PTREG"]);

                }
                else
                {
                    Session["ConInsPTReg"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_SHOW_TOKEN") == false)
                {
                    Session["ShowToken"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_SHOW_TOKEN"]);

                }
                else
                {
                    Session["ShowToken"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_DSPLY_DR_PTREG") == false)
                {
                    Session["DrNameDisplay"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DSPLY_DR_PTREG"]);

                }
                else
                {
                    Session["DrNameDisplay"] = "";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_PRIORITY_OPT_FILING") == false)
                {
                    Session["PriorityDisplay"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_PRIORITY_OPT_FILING"]);

                }
                else
                {
                    Session["PriorityDisplay"] = "";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_KNOW_FROM") == false)
                {
                    Session["KnowFromDisplay"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_KNOW_FROM"]);

                }
                else
                {
                    Session["KnowFromDisplay"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_DEP_WISE_PTID") == false)
                {
                    Session["DeptWisePTId"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_DEP_WISE_PTID"]);

                }
                else
                {
                    Session["DeptWisePTId"] = "";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_INTERVAL") == false)
                {
                    Session["AppointmentInterval"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_INTERVAL"]);

                }
                else
                {
                    Session["AppointmentInterval"] = "30";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_START") == false)
                {
                    Session["AppointmentStart"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_START"]);

                }
                else
                {
                    Session["AppointmentStart"] = "9";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPNMNT_END") == false)
                {
                    Session["AppointmentEnd"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPNMNT_END"]);

                }
                else
                {
                    Session["AppointmentEnd"] = "21";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_HEIGHT") == false)
                {
                    Session["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_HEIGHT"]);

                }
                else
                {
                    Session["HSOM_APPOINTMENT_DSPLY_HEIGHT"] = "30";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DR_DSLY_WIDTH") == false)
                {
                    Session["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DR_DSLY_WIDTH"]);

                }
                else
                {
                    Session["HSOM_APPOINTMENT_DR_DSLY_WIDTH"] = "1900";
                }


                if (DS.Tables[0].Rows[0].IsNull("HSOM_APPOINTMENT_DSPLY_FORMAT") == false)
                {
                    Session["HSOM_APPOINTMENT_DSPLY_FORMAT"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_APPOINTMENT_DSPLY_FORMAT"]);

                }
                else
                {
                    Session["HSOM_APPOINTMENT_DSPLY_FORMAT"] = "12";
                }

                if (DS.Tables[0].Rows[0].IsNull("HSOM_ECLAIM") == false)
                {
                    Session["HSOM_ECLAIM"] = Convert.ToString(DS.Tables[0].Rows[0]["HSOM_ECLAIM"]);

                }
                else
                {
                    Session["HSOM_ECLAIM"] = "";
                }






            }

        }

        void GetUsersRemarks()
        {
            string Criteria = " 1=1 AND HUM_STATUS='A' and  HUM_USER_ID='" + drpUsers.SelectedValue + "'";
            DataSet ds = new DataSet();
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["HUM_REMARK"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_REMARK"]);
            }
        }


        void BindStaffData()
        {
            DataSet DS = new DataSet();

            DS = (DataSet)ViewState["StaffData"];
            string Criteria = " 1=1  ";
            Criteria += " AND HSFM_STAFF_ID='" + Convert.ToString(Session["User_Code"]) + "'";

            DS = (DataSet)ViewState["StaffData"];
            DataRow[] result = DS.Tables[0].Select(Criteria);
            foreach (DataRow DR in result)
            {
                Session["FullName"] = Convert.ToString(DR["FullName"]);
                Session["User_DeptID"] = Convert.ToString(DR["HSFM_DEPT_ID"]);
                Session["User_Category"] = Convert.ToString(DR["HSFM_CATEGORY"]);
                //txtStaffName.Text = Convert.ToString(DR["FullName"]);
            }

        }

        void BindAllStaffData()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1  ";
            Criteria += "  AND HSFM_BRANCH_ID ='" + drpBranch.SelectedValue + "'";

            DS = objCom.GetStaffMaster(Criteria);

            ViewState["StaffData"] = DS;
        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME IN ('EMR_IP','HMS_TIME') ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            Session["EMR_IP_DISC_LAMA_FILE_PATH"] = "0";
            Session["EMR_IP_ANES_CHART_FILE_PATH"] = "0";
            Session["HMS_TIME_INTERVAL"] = "5";
           


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_IP_DISC_LAMA_FILE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_IP_DISC_LAMA_FILE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_IP_ANES_CHART_FILE_PATH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_IP_ANES_CHART_FILE_PATH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }
                    if (Convert.ToString(DR["SEGMENT"]) == "HMS_TIME_INTERVAL")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["HMS_TIME_INTERVAL"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                }
            }



        }
     
        void BindScreenCustomizationEMR()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND (   SCREENNAME='EMR' )";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);

            Session["EMR_ENABLE_MALAFFI"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "EMR_ENABLE_MALAFFI")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["EMR_ENABLE_MALAFFI"] = Convert.ToString(DR["CUST_VALUE"]).Trim();
                        }

                    }




                }
            }



        }

        void BindScreenCustomizationHMS()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND (   SCREENNAME='HMS' )";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
 
            Session["PWD_EXPIRY_DAYS"] = "0";
            Session["TRIAGE_OPTION"] = "0";
            Session["PWD_ENCRYPT"] = "0";
            Session["PWD_MINIMUM_AGE"] = "0";


            Session["PWD_FORMAT_REQUIRED"] = "0";

            Session["PWD_MIN_LENGTH"] = "0";
            Session["PWD_MAX_LENGTH"] = "0";
            Session["PWD_NUM_MIN_LENGTH"] = "0";
            Session["PWD_LETTER_MIN_LENGTH"] = "0";


            Session["PWD_PREV_NOTREPEAT"] = "0";



            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_FORMAT_REQUIRED")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_FORMAT_REQUIRED"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                   

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_EXPIRY_DAYS")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_EXPIRY_DAYS"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "TRIAGE_OPTION")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["TRIAGE_OPTION"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_ENCRYPT")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_ENCRYPT"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_MINIMUM_AGE")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_MINIMUM_AGE"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_MIN_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_MIN_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_MAX_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_MAX_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_NUM_MIN_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_NUM_MIN_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_LETTER_MIN_LENGTH")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_LETTER_MIN_LENGTH"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PWD_PREV_NOTREPEAT")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            Session["PWD_PREV_NOTREPEAT"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }



                }
            }



        }

        Boolean CheckPasswordExpiry()
        {

            string Criteria = " 1=1 ";

            //if (GlobalValues.FileDescription == "SMCH")  //if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
            //{
            //    Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND  HUM_USER_ID='" + txtUserID.Text.Trim() + "'";

            //}
            //else
            //{
                Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND HUM_USER_ID ='" + drpUsers.SelectedValue + "'";
          //  }




            DataSet DS = new DataSet();

            DS = objCom.fnGetFieldValue("DATEDIFF(DD,CONVERT(DATETIME,HUM_CREATED_DATE,101),CONVERT(DATETIME,GETDATE(),101)) AS UserCreatedDateDiff", "HMS_USER_MASTER", Criteria, "");
            if (DS.Tables[0].Rows.Count > 0)
            {

                Int64 intUserDateDiff = 0, intExpDays = 0;

                if (DS.Tables[0].Rows[0].IsNull("UserCreatedDateDiff") == false && Convert.ToString(DS.Tables[0].Rows[0]["UserCreatedDateDiff"]) != "")
                {

                    intUserDateDiff = Convert.ToInt64(DS.Tables[0].Rows[0]["UserCreatedDateDiff"]);

                    Session["UserCreatedDateDiff"] = Convert.ToInt64(DS.Tables[0].Rows[0]["UserCreatedDateDiff"]);

                }

                if (DS.Tables[0].Rows[0].IsNull("UserCreatedDateDiff") == false && Convert.ToString(DS.Tables[0].Rows[0]["UserCreatedDateDiff"]) != "")
                {
                    intExpDays = Convert.ToInt64(Session["PWD_EXPIRY_DAYS"]);

                }


                Int64 intUserPasswordExpire = intExpDays - intUserDateDiff;



                Session["UserPasswordExpire"] = intUserPasswordExpire;


                if (intUserDateDiff > intExpDays)
                {
                    return true;
                }
            }

            return false;
        }
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            BindSystemOption();

            if (!IsPostBack)
            {
                Session["Branch_ID"] = null;
                Session["User_ID"] = null;
                Session["User_Code"] = null;
                Session["User_Name"] = null;
                Session["User_Active"] = null;
                Session["Roll_Id"] = null;
          


                Session["IAS_ADMISSION_NO"] = null;
                Session["EMR_PT_ID"] = null;
                Session["IAS_DR_ID"] = null;
                Session["IAS_DR_NAME"] = null;
                Session["IAS_DEP_NAME"] = null;
                Session["IAS_DEP_ID"] = null;
                Session["IAS_COMP_ID"] = null;
                Session["IAS_DATE"] = null;
                Session["EMR_ID"] = null;
                Session["SO_ID"] = null;

                Session["WaitingListFromDate"] = null;
                Session["WaitingListToDate"] = null;
                Session["WaitingListStatus"] = null;

                drpUsers.Focus();
                BindBranch();
                BindUsers();
                BindAllStaffData();
                BindScreenCustomization();
                BindScreenCustomizationHMS();
                BindScreenCustomizationEMR();
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {

            string Criteria = " 1=1 ";
            CommonBAL objCom = new CommonBAL();
            string EncryPassword = "";
            if (Convert.ToString(Session["PWD_ENCRYPT"]) == "1")
            {
                // EncryPassword =  objCom.SimpleEncrypt(Convert.ToString(txtPassword.Text.Trim()));
                EncryPassword = objCom.SimpleEncrypt(Convert.ToString(txtPassword.Text.Trim()));
                //if (GlobalValues.FileDescription == "SMCH")
                //{
                //    Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND HUM_USER_ID ='" + txtUserID.Text.Trim() + "' AND   HUM_USER_PASS= '" + EncryPassword + "'";
                //}
                //else
                //{
                    Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND HUM_USER_ID ='" + drpUsers.SelectedValue + "' AND   HUM_USER_PASS= '" + EncryPassword + "'";

               // }

            }
            else
            {
                Criteria += " AND HUM_BRANCH_ID='" + drpBranch.SelectedValue + "' AND HUM_USER_ID ='" + drpUsers.SelectedValue + "' AND   HUM_USER_PASS= '" + txtPassword.Text.Trim() + "'";

            }


 
            DataSet ds = new DataSet();
            //ds = dbo.UserMasterGet(drpBranch.SelectedValue, drpUsers.SelectedValue , txtPassword.Text);
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Session["Branch_ID"] = drpBranch.SelectedValue;
                Session["User_ID"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_USER_ID"]);
                Session["User_Code"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_REMARK"]);
                Session["User_Name"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_USER_NAME"]);
                Session["User_Active"] = Convert.ToString(ds.Tables[0].Rows[0]["HUM_STATUS"]);
                Session["Roll_Id"] = Convert.ToString(ds.Tables[0].Rows[0]["HGP_ROLL_ID"]);

                BindStaffData();
                GetBranchDtls();
                if (Session["User_Active"].ToString() == "A")
                {
                    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Login Sucess');", true);
                    // Response.Redirect("Welcome.aspx");
                    Response.Redirect("Registration/PatientWaitingList.aspx");
                    ds.Clear();
                    ds.Dispose();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('User Inactive');", true);
                    ds.Clear();
                    ds.Dispose();
                }

            }
            else
            {
                txtPassword.Focus();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('Login Failed');", true);
                ds.Clear();
                ds.Dispose();
            }

        }

        //protected void drpUsers_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    GetUsersRemarks();
        //    BindStaffData();
        //}
    }
}