﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;



namespace EMR_IP
{
    public partial class DisplayCardImage : System.Web.UI.Page
    {
        void BindPatientPhoto()
        {
             CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND HPP_BRANCH_ID='" + Convert.ToString( Session["Branch_ID"] ) + "' AND  HPP_PT_ID='" + Convert.ToString( Session["EMR_PT_ID"] )+ "'";
            DataSet DS = new DataSet();
            DS = objCom.PatientPhotoGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HPP_INS_CARD") == false)
                {
                    
                    ViewState["HTI_IMAGE1"] = (byte[])DS.Tables[0].Rows[0]["HPP_INS_CARD"];
                    
                }

            }



        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["HTI_IMAGE1"] = "";
            Byte[] bytImage;

            BindPatientPhoto();
            if (Convert.ToString(ViewState["HTI_IMAGE1"]) != "")
            {
                bytImage = (Byte[])ViewState["HTI_IMAGE1"];
                

                Response.BinaryWrite(bytImage);

            }
        }
    }
}