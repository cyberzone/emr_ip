﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;



namespace EMR_IP
{
    public partial class DisplayUserProfileImage : System.Web.UI.Page
    {
        void BindPhoto()
        {
            Byte[] bytImage;

            DataSet DS = new DataSet();
            CommonBAL dbo = new CommonBAL();
            string Criteria = " 1=1  ";
            Criteria += " AND HSP_STAFF_ID='" + Convert.ToString(Session["User_Code"]) + "'";

            DS = dbo.StaffPhotoGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HSP_PHOTO") == false)
                {
                    bytImage = (Byte[])DS.Tables[0].Rows[0]["HSP_PHOTO"];
                    Response.BinaryWrite(bytImage);
                }

            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            BindPhoto();
        }
    }
}