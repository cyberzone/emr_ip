﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="EMR_IP.EducationalForm.Index" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/EducationalForm/EducationalFormControl.ascx" TagPrefix="UC1" TagName="EducationalFormControl" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
      <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <link href="../Content/themes/base/jquery-ui.css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

            .box-title
            {
                padding-bottom: 5px;
                border-bottom: 4px solid #92c500;
                float: left;
                font-size: 1.5em;
                color: #2078c0;
            }
            h1, h2, h3, h4, h5, h6 {
                font-size: 100%;
                font-weight: normal;
                font-family: "Segoe UI", Arial, Helvetica, sans-serif;
                }
    </style>

      <script type="text/javascript">
          function ShowMessage() {
              $("#myMessage").show();
              setTimeout(function () {
                  var selectedEffect = 'blind';
                  var options = {};
                  $("#myMessage").hide();
              }, 2000);
              return true;
          }

        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />

     <div class="box">
        <div class="box-header">
            <h3 class="box-title">Educational Form </h3>
        </div>
    </div>
    <div style="padding-left:60%;width:100%; ">
            <div id="myMessage" style="display:none;border:groove;height:30px;width:200px;background-color:gray;color:#ffffff;font-family:arial,helvetica,clean,sans-serif;font-size:small;   border: 1px solid #fff;
                padding: 20px;border-radius:10px;box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5);position:absolute;"> Saved Successfully </div>
        </div>
      <table style="width:100%" >
          <tr>
              <td align="right">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                         <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Width="100px" OnClick="btnSave_Click" Text="Save Record" />
                    </ContentTemplate>
                </asp:UpdatePanel>

              </td>
          </tr>
      </table>
 
    <br />

     <table class="table spacy">                
                <tr>
                    <td><label for="Language" class="lblCaption1" >Patient's language</label>
                        <input type="text" name="Language" id="Language"  runat="server" />
                    </td>
                    <td><label class="lblCaption1" >Patient's literacy</label>
                        <input type="checkbox" name="Read" id="Read" value="1"   runat="server" class="lblCaption1"  /><span class="lblCaption1" > Read </span>
                        <input type="checkbox" name="Write" id="Write" value="1"  runat="server"  class="lblCaption1" /> <span class="lblCaption1" >  Write </span>
                        <input type="checkbox" name="Speak" id="Speak" value="1"  runat="server" class="lblCaption1"  /> <span class="lblCaption1" >  Speak </span>
                    </td>
                    <td><label for="ReligionBelief" class="lblCaption1" >Religion/Belief</label>
                        <input type="text" name="ReligionBelief" id="ReligionBelief"  runat="server" />
                    </td>
                </tr>
            </table>
     <UC1:EducationalFormControl ID="EducationalFormControlPart1" runat="server"  SegmentType="EDU_PART1" SegmentHeader="Part I"  />
     <UC1:EducationalFormControl ID="EducationalFormControlPart2" runat="server"  SegmentType="EDU_PART2"  SegmentHeader="Part II" />
     <UC1:EducationalFormControl ID="EducationalFormControlPart3" runat="server"  SegmentType="EDU_PART3" SegmentHeader="Part III"  />
    <br />
<br />
</asp:Content>

