﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;

namespace EMR_IP.EducationalForm
{
    public partial class Index : System.Web.UI.Page
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindEducationalForm()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria1 = " 1=1 ";
            // Criteria1 += " AND EFM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria1 += " AND EFM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            DS = objCom.EMR_EDUCATIONALFORMGet(Criteria1);
            if (DS.Tables[0].Rows.Count > 0)
            {

                Language.Value = Convert.ToString(DS.Tables[0].Rows[0]["EFM_LANGUAGE"]);

                if (Convert.ToString(DS.Tables[0].Rows[0]["EFM_READ"]) == "1")
                {
                    Read.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EFM_WRITE"]) == "1")
                {
                    Write.Checked = true;
                }
                if (Convert.ToString(DS.Tables[0].Rows[0]["EFM_SPEAK"]) == "1")
                {
                    Speak.Checked = true;
                }


                ReligionBelief.Value = Convert.ToString(DS.Tables[0].Rows[0]["EFM_RELEGION"]);
            }
        }

        void SaveEducationalForm()
        {
            EMR_EducationalForm objEduFrm = new EMR_EducationalForm();

            objEduFrm.branchid = Convert.ToString(Session["Branch_ID"]);
            objEduFrm.patientmasterid = Convert.ToString(Session["EMR_ID"]);

            objEduFrm.language = Language.Value;
            objEduFrm.read = Read.Checked == true ? "1" : "0";

            objEduFrm.write = Write.Checked == true ? "1" : "0";
            objEduFrm.speak = Speak.Checked == true ? "1" : "0";
            objEduFrm.religion = ReligionBelief.Value;

            objEduFrm.WEMR_spI_SaveEducationalForm();
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_EDU_FRM' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Visible = false;
                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSave.Visible = false;
                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }

                try
                {

                    BindEducationalForm();
                }

                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "       EducationalForm.Index.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }


        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveEducationalForm();
                EducationalFormControlPart1.SaveEducationalForm();
                EducationalFormControlPart2.SaveEducationalForm();
                EducationalFormControlPart3.SaveEducationalForm();

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "       EducationalForm.Index.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}