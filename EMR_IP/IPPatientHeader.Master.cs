﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP
{
    public partial class IPPatientHeader : System.Web.UI.MasterPage
    {
        CommonBAL objCom = new CommonBAL();
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("IPPatientHeader."+strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        Boolean BindAdmissionSummary()
        {
            DataSet DS = new DataSet();
            IP_AdmissionSummary obj = new IP_AdmissionSummary();
            string Criteria = " 1=1 ";
            Criteria += " and IAS_ADMISSION_NO='" + Convert.ToString(Session["IAS_ADMISSION_NO"]) + "'";

            DS = obj.GetIPAdmissionSummary(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                // txtInpatientNo.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_NO"]);

                //  AdmissionDateTime.Value = Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateTime"]);
               
                if (DS.Tables[0].Rows[0].IsNull("IAS_PT_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_NAME"]) != "")
                {
                    string str = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_NAME"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblPTName.Text = str;
                    lblPTName.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_NAME"]);

                }



                lblAdmissionNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_NO"]);
                lblAdmissionType.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_TYPE"]);
                lblAddmissionMode.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMN_MODE"]);


                
                if (DS.Tables[0].Rows[0].IsNull("IAS_NATIONALITY") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_NATIONALITY"]) != "")
                {
                    string str = Convert.ToString(DS.Tables[0].Rows[0]["IAS_NATIONALITY"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblNationality.Text = str;
                    lblNationality.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["IAS_NATIONALITY"]);
                }


                lblPTID.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_ID"]);
                lblDoctor.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_ID"]) + "-" + Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_NAME"]);
                lblPolicyType.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_TYPE"]);


                lblAge.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_AGE"]);
                lblSex.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_SEX"]);
                lblAdmissionDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateDesc"]) + " " + Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateTimeDesc"]);
                lblPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_NO"]);

                lblMobile.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_MOBILE"]);
                lblDOB.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DOBDesc"]);
                lblProviderName.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_INSURANCE_COMPANY"]);


                //lblRoom.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_MOBILE"]);
                //lblBead.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DOB"]);
                //lblWard.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_INSURANCE_COMPANY"]);
              
                return true;
            }
            else
            {

                return false;
            }
        }

        void BindPTVisitDetails()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HPV_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HPV_PT_ID = '" + lblPTID.Text + "' ";
            Criteria += " AND HPV_EMR_ID = '" + Convert.ToString(Session["EMR_ID"]) + "' ";

            DataSet DS = new DataSet();
            DS = objCom.PatientVisitGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HPV_COMP_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_NAME"]) != "")
                {

                    string str = Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_NAME"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblProviderName.Text = str;
                    lblProviderName.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["HPV_COMP_NAME"]);
                }

             
                //if (DS.Tables[0].Rows[0].IsNull("HPM_POLICY_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_NO"]) != "")
                //{
                //    string str = Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                //    if (str.Length > 15)
                //    {
                //        str = str.Substring(0, 15);
                //    }
                //    lblPolicyNo.Text = str;
                //    lblPolicyNo.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                //}

                //if (DS.Tables[0].Rows[0].IsNull("HPM_POLICY_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]) != "")
                //{
                //    string str = Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
                //    if (str.Length > 15)
                //    {
                //        str = str.Substring(0, 15);
                //    }
                //    lblPolicyType.Text = str;
                //    lblPolicyType.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
                //}
            }



        }

        void BindPTDetails()
        {
            string Criteria = " 1=1 AND HPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND HPM_PT_ID = '" + lblPTID.Text + "' ";
            DataSet ds = new DataSet();
            objCom = new CommonBAL();
            ds = objCom.PatientMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {


                if (ds.Tables[0].Rows[0].IsNull("FullName") == false && Convert.ToString(ds.Tables[0].Rows[0]["FullName"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblPTName.Text = str;
                    lblPTName.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["FullName"]);

                }


                if (ds.Tables[0].Rows[0].IsNull("HPM_NATIONALITY") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblNationality.Text = str;
                    lblNationality.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_NATIONALITY"]);
                }


                //if (ds.Tables[0].Rows[0].IsNull("HPM_INS_COMP_NAME") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]) != "")
                //{

                //    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                //    if (str.Length > 15)
                //    {
                //        str = str.Substring(0, 15);
                //    }
                //    lblProviderName.Text = str;
                //    lblProviderName.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                //}

                if (ds.Tables[0].Rows[0].IsNull("HPM_SEX") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }

                    lblSex.Text = str;
                    lblSex.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);
                    GlobalValues.EMR_PT_SEX = str;
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_MOBILE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblMobile.Text = str;
                    lblMobile.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_MOBILE"]);
                }
                if (ds.Tables[0].Rows[0].IsNull("HPM_AGE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblAge.Text = str;
                    lblAge.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE"]);
                }

               
 

                if (ds.Tables[0].Rows[0].IsNull("HPM_POLICY_NO") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblPolicyNo.Text = str;
                    lblPolicyNo.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                }

                if (ds.Tables[0].Rows[0].IsNull("HPM_POLICY_TYPE") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]) != "")
                {
                    string str = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
                    if (str.Length > 15)
                    {
                        str = str.Substring(0, 15);
                    }
                    lblPolicyType.Text = str;
                    lblPolicyType.ToolTip = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
                }


            }

        }

        void BindPatientPhoto()
        {
            imgFront.ImageUrl = "~/DisplayCardImage.aspx";

            //objCom = new CommonBAL();
            //string Criteria = " 1=1 ";
            //Criteria += " AND HPP_BRANCH_ID='" + Session["Branch_ID"] + "' AND  HPP_PT_ID='" + lblPTID.Text + "'";
            //DataSet DS = new DataSet();
            //DS = objCom.PatientPhotoGet(Criteria);


            //if (DS.Tables[0].Rows.Count > 0)
            //{
            //    if (DS.Tables[0].Rows[0].IsNull("HPP_INS_CARD") == false)
            //    {
            //        imgFront.Visible = true;
            //        Session["HTI_IMAGE1"] = (byte[])DS.Tables[0].Rows[0]["HPP_INS_CARD"];
            //        imgFront.ImageUrl = "~/DisplayCardImage.aspx";
            //    }

            //}



        }

        string BindRefDrData(string StaffID)
        {
            string RefDrName = "";

            DataSet DS = new DataSet();
            string Criteria = " 1=1  ";
            Criteria += " AND HRDM_REF_ID='" + StaffID + "'";

            DS = objCom.RefDoctorMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                RefDrName = Convert.ToString(DS.Tables[0].Rows[0]["FullName"]);
            }

            return RefDrName;
        }

        void Clear()
        {

            lblPTID.Text = "";
            

            lblPTName.Text = "";

            lblNationality.Text = "";

            lblProviderName.Text = "";
            lblSex.Text = "";
            lblMobile.Text = "";
            lblAge.Text = "";
         
           
            lblPolicyNo.Text = "";
            lblPolicyType.Text = "";
            imgFront.Visible = false;


        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {

                if (Convert.ToString(Session["IAS_ADMISSION_NO"]) == "" || Convert.ToString(Session["IAS_ADMISSION_NO"]) == null)
                {
                    goto FunEnd;
                }

                try
                {
                    BindAdmissionSummary();
                   // BindPTVisitDetails();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        FunEnd: ;
        }

        protected void chkProcUpdate_CheckedChanged(object sender, EventArgs e)
        {


              try
                {
 
                    IP_PTMaster objPTMaster = new IP_PTMaster();
                    objPTMaster.IPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPTMaster.IPM_ID = Convert.ToString(Session["EMR_ID"]);
                    objPTMaster.IPM_DR_CODE = Convert.ToString(Session["IAS_DR_ID"]);
                    objPTMaster.IPM_DATE = Convert.ToString(Session["IAS_DATE"]);
                    objPTMaster.IPSalesOrderAdd();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "chkProcUpdate_CheckedChanged");
                    TextFileWriting(ex.Message.ToString());
                }

        }
        
    }
}