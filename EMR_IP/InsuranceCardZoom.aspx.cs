﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using EMR_IP_BAL;



namespace EMR_IP
{
    public partial class InsuranceCardZoom : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                imgFront.ImageUrl = "~/DisplayCardImage.aspx";
                imgBack.ImageUrl = "~/DisplayCardImageBack.aspx";
            }
        }
    }
}