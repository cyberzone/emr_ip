﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BedMaster.ascx.cs" Inherits="EMR_IP.Masters.BedMaster" %>

<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

<script language="javascript" type="text/javascript">

    function OnlyNumeric(evt) {
        var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
        if (chCode >= 48 && chCode <= 57 ||
             chCode == 46) {
            return true;
        }
        else

            return false;
    }
</script>
 <input type="hidden" id="hidPermission" runat="server" value="9" />

<table style="width: 100%">
    <tr>
        <td class="lblCaption1">Bed Code
        </td>
        <td>
            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <asp:TextBox ID="txtCode" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
        <td class="lblCaption1">Charge
        </td>
        <td>
            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <asp:TextBox ID="txtCharge" runat="server" CssClass="label" Width="200px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1">Ward 
        </td>
        <td>
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <asp:DropDownList ID="drpWardNo" runat="server" CssClass="label" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="drpWardNo_SelectedIndexChanged"></asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>

        <td class="lblCaption1">Room
        </td>
        <td>
            <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                <ContentTemplate>
                    <asp:DropDownList ID="drpRoomNo" runat="server" CssClass="label" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="drpRoomNo_SelectedIndexChanged"></asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>

    </tr>

    <tr>
        
        <td>
            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <asp:CheckBox ID="chkActive" runat="server" Text="Active" Checked="true" CssClass="lblCaption1" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1">Remarks
        </td>
        <td colspan="3">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="label" Width="90%" Height="50px" TextMode="MultiLine"></asp:TextBox>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>

    </tr>


</table>
<table>
    <tr>
        <td>
            <asp:Button ID="btnAdd" runat="server" Text="Add Ward" CssClass="button orange small" OnClick="btnAdd_Click" Width="100px" />
        </td>
    </tr>
</table>

<div>
    <asp:UpdatePanel ID="UpdatePanel12" runat="server">
        <ContentTemplate>
            <asp:GridView ID="gvBed" runat="server" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="100%">
                <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
                <RowStyle CssClass="GridRow" />
                <Columns>
                    

                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:ImageButton ID="DeleteDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Width="16px" Height="16px"
                                OnClick="Deletegv_Click" />&nbsp;&nbsp;
                                                
                        </ItemTemplate>
                        <HeaderStyle Width="50px" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Code">
                        <ItemTemplate>
                             
                            <asp:LinkButton ID="lnkCode" runat="server" OnClick="Edit_Click">


                                <asp:Label ID="lblCode" CssClass="GridRow" runat="server" Text='<%# Bind("HBD_ID") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Ward">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkWrad" runat="server" OnClick="Edit_Click">
                                <asp:Label ID="lblWard" CssClass="GridRow" runat="server" Text='<%# Bind("HBD_WARD_ID") %>'></asp:Label>
                            </asp:LinkButton>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Room">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkRoom" runat="server" OnClick="Edit_Click">
                                <asp:Label ID="lblRoom" CssClass="GridRow" runat="server" Text='<%# Bind("HBD_ROOM_ID") %>'></asp:Label>
                            </asp:LinkButton>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Rent">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkRent" runat="server" OnClick="Edit_Click">
                                <asp:Label ID="lblRent" CssClass="GridRow" runat="server" Text='<%# Bind("HBD_RENT") %>'></asp:Label>
                            </asp:LinkButton>

                        </ItemTemplate>
                    </asp:TemplateField>



                    <asp:TemplateField HeaderText="Active">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkActive" runat="server" OnClick="Edit_Click">
                                <asp:Label ID="lblActive" CssClass="GridRow" runat="server" Text='<%# Bind("HBD_STATUS") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkRemarks" runat="server" OnClick="Edit_Click">
                                <asp:Label ID="lblRemarks" CssClass="GridRow" runat="server" Text='<%# Bind("HBD_REMARK") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>


                </Columns>


            </asp:GridView>

        </ContentTemplate>
    </asp:UpdatePanel>
</div>
