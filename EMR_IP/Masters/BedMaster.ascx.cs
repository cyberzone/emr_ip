﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;
using Newtonsoft.Json;

namespace EMR_IP.Masters
{
    public partial class BedMaster : System.Web.UI.UserControl
    {
        CommonBAL objCom = new CommonBAL();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        /*
        public DataTable DerializeDataTable(string json)
        {
            // const string json = @"[{""Name"":""AAA"",""Age"":""22"",""Job"":""PPP""},"
            //                  + @"{""Name"":""BBB"",""Age"":""25"",""Job"":""QQQ""},"
            //                  + @"{""Name"":""CCC"",""Age"":""38"",""Job"":""RRR""}]";
            var table = JsonConvert.DeserializeObject<DataTable>(json);
            return table;
        }
        void BindWardNo()
        {


            string strjson;
            DataSet DS = new DataSet();
            DataSet DS1 = new DataSet();
            DataTable DT = new DataTable();

            IP_ResourceDatas objRD = new IP_ResourceDatas();
            string Criteria = " 1=1 ";
            Criteria += " and RD_TYPE='WARD_MASTER' and RD_IS_DELETED = 0 ";
            DS = objRD.IPResourceDatasGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strjson = "[";
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    strjson += DR["RD_DATA"] + ",";

                }
                strjson += "]";

                DT = DerializeDataTable(strjson);

                if (DT.Rows.Count > 0)
                {
                    drpWardNo.DataSource = DT;
                    drpWardNo.DataTextField = "Code";
                    drpWardNo.DataValueField = "Code";
                    drpWardNo.DataBind();
                }

                drpWardNo.Items.Insert(0, "Select Code");
                drpWardNo.Items[0].Value = "";
            }



        }

        void BindRoom()
        {


            string strjson;
            DataSet DS = new DataSet();
            DataSet DS1 = new DataSet();
            DataTable DT = new DataTable();

            IP_ResourceDatas objRD = new IP_ResourceDatas();
            string Criteria = " 1=1 ";
            Criteria += " and RD_TYPE='ROOM_MASTER' and RD_IS_DELETED = 0 ";
            DS = objRD.IPResourceDatasGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strjson = "[";
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    string Data = "";
                    Data = Convert.ToString(DR["RD_DATA"]);
                    Data = Data.Replace("{\"", "{\"RD_ID\":\"" + Convert.ToString(DR["RD_ID"]) + "\"" + ",\"");
                    strjson += Data + ",";

                }
                strjson += "]";

                DT = DerializeDataTable(strjson);
                ViewState["Room_Data"] = DT;

                drpRoomNo.Items.Clear();
                if (drpRoomNo.Items.Count == 0)
                {
                    drpRoomNo.Items.Insert(0, "Select Code");
                    drpRoomNo.Items[0].Value = "";

                }
                DataRow[] result = DT.Select("ward='" + drpWardNo.SelectedValue + "'");
                Int32 i = 0;
                foreach (DataRow DR in result)
                {

                    drpRoomNo.Items.Insert(i + 1, Convert.ToString(DR["Code"]));
                    drpRoomNo.Items[i + 1].Value = Convert.ToString(DR["Code"]);


                }


            }



        }

        void BindCharge()
        {

            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["Room_Data"];
            DataRow[] result = DT.Select("ward='" + drpWardNo.SelectedValue + "' and Code='" + drpRoomNo.SelectedValue + "'");
            Int32 i = 0;
            foreach (DataRow DR in result)
            {
                txtCharge.Text = Convert.ToString(DR["Rent"]);
            }


        }


        void BindBed()
        {


            string strjson;
            DataSet DS = new DataSet();
            DataSet DS1 = new DataSet();
            DataTable DT = new DataTable();

            IP_ResourceDatas objRD = new IP_ResourceDatas();
            string Criteria = " 1=1 ";
            Criteria += " and RD_TYPE='BED_MASTER' and RD_IS_DELETED = 0 ";
            DS = objRD.IPResourceDatasGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strjson = "[";
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    string Data = "";
                    Data = Convert.ToString(DR["RD_DATA"]);
                    Data = Data.Replace("{\"", "{\"RD_ID\":\"" + Convert.ToString(DR["RD_ID"]) + "\"" + ",\"");
                    strjson += Data + ",";

                }
                strjson += "]";

                DT = DerializeDataTable(strjson);

                if (DT.Rows.Count > 0)
                {
                    gvRoom.DataSource = DT;

                    gvRoom.DataBind();
                }


            }



        }

        */

        void BindWardNo()
        {

            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " HWM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            DS = objCom.fnGetFieldValue("*", "HMS_WARD_MASTER", Criteria, "HWM_ID");

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpWardNo.DataSource = DS;
                drpWardNo.DataTextField = "HWM_NAME";
                drpWardNo.DataValueField = "HWM_ID";
                drpWardNo.DataBind();
            }

            drpWardNo.Items.Insert(0, "Select Code");
            drpWardNo.Items[0].Value = "";

        }

        void BindRoom()
        {

            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " HRM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            DS = objCom.fnGetFieldValue("*", "HMS_ROOM_MASTER", Criteria, "HRM_ID");
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpRoomNo.DataSource = DS;
                drpRoomNo.DataTextField = "HRM_NAME";
                drpRoomNo.DataValueField = "HRM_ID";
                drpRoomNo.DataBind();
            }

            drpRoomNo.Items.Insert(0, "Select Code");
            drpRoomNo.Items[0].Value = "";



        }

        void BindBed()
        {

            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " HBD_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            DS = objCom.fnGetFieldValue("*", "HMS_BED_MASTER", Criteria, "HBD_ID");

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvBed.DataSource = DS;
                gvBed.DataBind();
                string strPermission = hidPermission.Value;
                if (strPermission == "1" || strPermission == "7")
                {
                    gvBed.Columns[0].Visible = false;
                }
            }
            else
            {
                gvBed.DataBind();
            }

        }

        void Clear()
        {
            txtCode.Text = "";
            if (drpWardNo.Items.Count > 0)
                drpWardNo.SelectedIndex = 0;

            if (drpRoomNo.Items.Count > 0)
                drpRoomNo.SelectedIndex = 0;
            txtCharge.Text = "";
            txtRemarks.Text = "";
           

        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_BED_MASTER' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnAdd.Visible = false;
                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnAdd.Visible = false;
                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                    {
                        SetPermission();
                    }

                    
                    CommonBAL objCom = new CommonBAL();
                    string strDate = "", strTime = ""; ;
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("hh:mm:ss");

                  
                    BindWardNo();
                    BindBed();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      WardMaster.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }

        protected void Edit_Click(object sender, EventArgs e)
        {

            try
            {

                //if (Convert.ToString(ViewState["SelectIndex"]) != "")
                //{
                //    gvBedTrans.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                //}

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                // ViewState["SelectIndex"] = gvScanCard.RowIndex;
                //  gvBedTrans.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

               
                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblWard = (Label)gvScanCard.Cells[0].FindControl("lblWard");
                Label lblRoom = (Label)gvScanCard.Cells[0].FindControl("lblRoom");
                Label lblRent = (Label)gvScanCard.Cells[0].FindControl("lblRent");


                Label lblRemarks = (Label)gvScanCard.Cells[0].FindControl("lblRemarks");
                Label lblActive = (Label)gvScanCard.Cells[0].FindControl("lblActive");


             

                txtCode.Text = lblCode.Text;
                drpWardNo.SelectedValue = lblWard.Text;

               
                
                txtCharge.Text = lblRent.Text;


                txtRemarks.Text = lblRemarks.Text;
                if (lblActive.Text == "A")
                {
                    chkActive.Checked = true;
                }
                else
                {
                    chkActive.Checked = false;
                }

                drpWardNo_SelectedIndexChanged(drpWardNo, new EventArgs());
                drpRoomNo.SelectedValue = lblRoom.Text;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      BedTransfer.Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                //{"Code":"43","Ward":"345","MultyBed":"Yes","Rent":"897","Active":true,"Remarks":"fdgfsdg","Date":"4/12/2013","Time":"11:18"}
                //{"Code":"34","Ward":"123","Room":"45","Rent":"452","Active":true,"Status":"Availability","Remarks":"dfsgsdfg","Date":"4/12/2013","Time":"11:19"}


                objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.WardID = drpWardNo.SelectedValue;
                objCom.RoomID = drpRoomNo.SelectedValue;
                objCom.Code = txtCode.Text.Trim();
                
                objCom.Rent = txtCharge.Text.Trim();
                objCom.Remarks = txtRemarks.Text.Trim();

                if (chkActive.Checked == true)
                {
                    objCom.Status = "A";
                }
                else
                {
                    objCom.Status = "I";
                }


                objCom.UserID = Convert.ToString(Session["User_ID"]);
                objCom.BedMasterAdd();

 

                BindBed();
                Clear();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void Deletegv_Click(object sender, EventArgs e)
        {

            try
            {


                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");



                objCom = new CommonBAL();

                string Criteria = " HBD_BRANCH_ID	='" + Convert.ToString(Session["Branch_ID"]) + "' AND  HBD_ID ='" + lblCode.Text.Trim() + "'";

                objCom.fnDeleteTableData("HMS_BED_MASTER", Criteria);


                BindBed();
                Clear();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Deletegv_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpWardNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindRoom();
        }

        protected void drpRoomNo_SelectedIndexChanged(object sender, EventArgs e)
        {
           // BindCharge();
        }
    }
}