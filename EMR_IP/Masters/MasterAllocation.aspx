﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MasterAllocation.aspx.cs" Inherits="EMR_IP.Masters.MasterAllocation" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<%@ Register Src="~/Masters/WardMaster.ascx" TagPrefix="UC1" TagName="WardMaster" %>
<%@ Register Src="~/Masters/RoomMaster.ascx" TagPrefix="UC1" TagName="RoomMaster" %>
<%@ Register Src="~/Masters/BedMaster.ascx" TagPrefix="UC1" TagName="BedMaster" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

     <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

            .box-title
            {
                padding-bottom: 5px;
                border-bottom: 4px solid #92c500;
                float: left;
                font-size: 1.2em;
                color: #2078c0;
            }
            h1, h2, h3, h4, h5, h6 {
                font-size: 100%;
                font-weight: normal;
                font-family: "Segoe UI", Arial, Helvetica, sans-serif;
                }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
        <div class="box">
        <div class="box-header">
            <h3 class="box-title">Master Allocation </h3>
        </div>
    </div>

     <br />
         <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Ward Master" Width="100%">
            <ContentTemplate>
                <UC1:WardMaster ID="pnlWardMaster" runat="server"></UC1:WardMaster>

            </ContentTemplate>
        </asp:TabPanel>
             <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Room Master" Width="100%">
            <ContentTemplate>
                <UC1:RoomMaster ID="pnlRoomMaster" runat="server"></UC1:RoomMaster>

            </ContentTemplate>
        </asp:TabPanel>
               <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="Bed Master" Width="100%">
            <ContentTemplate>
                <UC1:BedMaster ID="pnlBedMaster" runat="server"></UC1:BedMaster>

            </ContentTemplate>
        </asp:TabPanel>
        </asp:TabContainer>
    </div>
    </form>
</body>
</html>
