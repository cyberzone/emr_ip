﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;
using Newtonsoft.Json;
using System.Web.UI.HtmlControls;
namespace EMR_IP.Patient
{
    public partial class AdmissionNursingAssessment : System.Web.UI.Page
    {
        static string strSessionDeptId;

        CommonBAL objCom = new CommonBAL();
        IP_AdmissionNursingAssessment objAdmNurAss = new IP_AdmissionNursingAssessment();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        Boolean BindAdmissionSummary()
        {
            DataSet DS = new DataSet();
            IP_AdmissionSummary obj = new IP_AdmissionSummary();
            string Criteria = " 1=1 ";
            Criteria += " and IAS_ADMISSION_NO='" + Convert.ToString(Session["IAS_ADMISSION_NO"]) + "'";

            DS = obj.GetIPAdmissionSummary(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                

                drpWardNo.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["IAS_WARD_NO"]);

                if (DS.Tables[0].Rows[0].IsNull("IAS_ROOM_NO") == false)
                {
                    drpWardNo_SelectedIndexChanged(drpWardNo, new EventArgs());
                    drpRoomNo.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ROOM_NO"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("IAS_BED_NO") == false)
                {
                    drpRoomNo_SelectedIndexChanged(drpRoomNo,new EventArgs());
                    drpBed.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["IAS_BED_NO"]);
                }

 
                return true;
            }
            else
            {

                return false;
            }
        }

        void BindTime()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpArrivalHour.DataSource = DS;
                drpArrivalHour.DataTextField = "Name";
                drpArrivalHour.DataValueField = "Code";
                drpArrivalHour.DataBind();


                drpAdmiHour.DataSource = DS;
                drpAdmiHour.DataTextField = "Name";
                drpAdmiHour.DataValueField = "Code";
                drpAdmiHour.DataBind();

                drpLastDoseHour.DataSource = DS;
                drpLastDoseHour.DataTextField = "Name";
                drpLastDoseHour.DataValueField = "Code";
                drpLastDoseHour.DataBind();
               

            }

            drpArrivalHour.Items.Insert(0, "00");
            drpArrivalHour.Items[0].Value = "00";

            drpAdmiHour.Items.Insert(0, "00");
            drpAdmiHour.Items[0].Value = "00";

            drpLastDoseHour.Items.Insert(0, "00");
            drpLastDoseHour.Items[0].Value = "00";

            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpArrivalMin.DataSource = DS;
                drpArrivalMin.DataTextField = "Name";
                drpArrivalMin.DataValueField = "Code";
                drpArrivalMin.DataBind();

                drpAdmiMin.DataSource = DS;
                drpAdmiMin.DataTextField = "Name";
                drpAdmiMin.DataValueField = "Code";
                drpAdmiMin.DataBind();

                drpLastDosemin.DataSource = DS;
                drpLastDosemin.DataTextField = "Name";
                drpLastDosemin.DataValueField = "Code";
                drpLastDosemin.DataBind();

            }

            drpArrivalMin.Items.Insert(0, "00");
            drpArrivalMin.Items[0].Value = "00";

            drpAdmiMin.Items.Insert(0, "00");
            drpAdmiMin.Items[0].Value = "00";

            drpLastDosemin.Items.Insert(0, "00");
            drpLastDosemin.Items[0].Value = "00";
             

        }

        public DataTable DerializeDataTable(string json)
        {
            // const string json = @"[{""Name"":""AAA"",""Age"":""22"",""Job"":""PPP""},"
            //                  + @"{""Name"":""BBB"",""Age"":""25"",""Job"":""QQQ""},"
            //                  + @"{""Name"":""CCC"",""Age"":""38"",""Job"":""RRR""}]";
            var table = JsonConvert.DeserializeObject<DataTable>(json);
            return table;
        }

        void BindWardNo()
        {

            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " HWM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND HWM_STATUS='A' ";

            DS = objCom.fnGetFieldValue("HWM_ID,HWM_NAME", "HMS_WARD_MASTER", Criteria, "HWM_ID");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpWardNo.DataSource = DS;
                drpWardNo.DataTextField = "HWM_NAME";
                drpWardNo.DataValueField = "HWM_ID";
                drpWardNo.DataBind();


            }
            drpWardNo.Items.Insert(0, "--- Select ---");
            drpWardNo.Items[0].Value = "";

        }

        void BindRoom()
        {
            drpRoomNo.Items.Clear();
            drpBed.Items.Clear();

            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " HRM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND HRM_STATUS='A' ";
            Criteria += " AND HRM_WARD_ID='" + drpWardNo.SelectedValue + "'";


            DS = objCom.fnGetFieldValue("HRM_ID,HRM_NAME", "HMS_ROOM_MASTER", Criteria, "HRM_ID");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpRoomNo.DataSource = DS;
                drpRoomNo.DataTextField = "HRM_NAME";
                drpRoomNo.DataValueField = "HRM_ID";
                drpRoomNo.DataBind();


            }
            drpRoomNo.Items.Insert(0, "--- Select ---");
            drpRoomNo.Items[0].Value = "";



        }


        void BindBed()
        {
            drpBed.Items.Clear();
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " HBD_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND HBD_STATUS='A' ";
            Criteria += " AND HBD_WARD_ID='" + drpWardNo.SelectedValue + "' and  HBD_ROOM_ID='" + drpRoomNo.SelectedValue + "'";


            DS = objCom.fnGetFieldValue("HBD_ID", "HMS_BED_MASTER", Criteria, "HBD_ID");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpBed.DataSource = DS;
                drpBed.DataTextField = "HBD_ID";
                drpBed.DataValueField = "HBD_ID";
                drpBed.DataBind();


            }
            drpBed.Items.Insert(0, "--- Select ---");
            drpBed.Items[0].Value = "";



        }
        void BindOrientationUnit()
        {
            chkOrientationUnit.Items.Clear();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='ORIENTATION_UNIT' ";

            DataSet DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                chkOrientationUnit.DataSource = DS;
                chkOrientationUnit.DataTextField = "EM_NAME";
                chkOrientationUnit.DataValueField = "EM_CODE";
                chkOrientationUnit.DataBind();
            }


        }

        void BindPresPastHealthProb()
        {
            chkPresPastHealthProb.Items.Clear();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='PRES_PAST_HEALTH_PROB' ";

            DataSet DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                chkPresPastHealthProb.DataSource = DS;
                chkPresPastHealthProb.DataTextField = "EM_NAME";
                chkPresPastHealthProb.DataValueField = "EM_CODE";
                chkPresPastHealthProb.DataBind();
            }


        }

        void BindData()
        {
            objAdmNurAss = new IP_AdmissionNursingAssessment();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 and IANA_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IANA_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DS = objAdmNurAss.AdmiNursingAssessmentGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    txtAdmissRoute.Text = Convert.ToString(DR["IANA_ADMISSION_ROUTE"]);
                    drpWardNo.SelectedValue = Convert.ToString(DR["IANA_ADMISSION_WARD"]);
                    drpRoomNo.SelectedValue = Convert.ToString(DR["IANA_ROOM_NO"]);
                    drpBed.SelectedValue = Convert.ToString(DR["IANA_BED_NO"]);


                    txtArrivalDate.Text = Convert.ToString(DR["IANA_WARD_ARRIVAL_DateDesc"]);
                    string strArrivalDate = Convert.ToString(DR["IANA_WARD_ARRIVAL_TIMEDesc"]);
                    string[] arrArrivalDater = strArrivalDate.Split(':');
                    if (arrArrivalDater.Length > 1)
                    {
                        drpArrivalHour.SelectedValue = arrArrivalDater[0];
                        drpArrivalMin.SelectedValue = arrArrivalDater[1];

                    }

                    txtAdmiDate.Text = Convert.ToString(DR["IANA_ADMISSION_DATE_DateDesc"]);
                    string strAdmiDate = Convert.ToString(DR["IANA_ADMISSION_DATE_TIMEDesc"]);
                    string[] arrAdmiDate = strAdmiDate.Split(':');
                    if (arrArrivalDater.Length > 1)
                    {
                        drpAdmiHour.SelectedValue = arrAdmiDate[0];
                        drpAdmiMin.SelectedValue = arrAdmiDate[1];

                    }

                    drpAdmissionMode.SelectedValue = Convert.ToString(DR["IANA_ADMISSION_MODE"]);
                    drpInfObtFrom.SelectedValue = Convert.ToString(DR["IANA_INFO_OBTAINED_FROM"]);
                    txtDiag.Text = Convert.ToString(DR["IANA_DIAGNOSIS"]);
                    txtWeight.Text = Convert.ToString(DR["IANA_WEIGHT"]);
                    txtHeight.Text = Convert.ToString(DR["IANA_HEIGHT"]);
                    txtPTPainScore.Text = Convert.ToString(DR["IANA_PT_PAIN_SCORE"]);


                    string strOrientationUnit = Convert.ToString(DR["IANA_ORIENTATION_UNIT"]);

                    string[] arrOrientationUnit = strOrientationUnit.Split('|');

                    if (arrOrientationUnit.Length > 0)
                    {
                        for (Int32 i = 0; i < chkOrientationUnit.Items.Count - 1; i++)
                        {
                            for (Int32 j = 0; j < arrOrientationUnit.Length; j++)
                            {

                                if (chkOrientationUnit.Items[i].Value == arrOrientationUnit[j])
                                {
                                    chkOrientationUnit.Items[i].Selected = true;
                                    goto OrientForEnd;
                                }

                            }
                        OrientForEnd: ;
                        }

                    }

                    radValuables.SelectedValue = Convert.ToString(DR["IANA_BROUGHT_VALUABLE"]);
                    txtPTSenthWith.Text = Convert.ToString(DR["IANA_SENT_HOME_WITH"]);
                    txtProGivenBy.Text = Convert.ToString(DR["IANA_PRO_GIVEN_BY"]);
                    drpLivingWith.SelectedValue = Convert.ToString(DR["IANA_LIVING_SITUATION"]);
                    drpProfession.SelectedValue = Convert.ToString(DR["IANA_PROFESSION"]);
                    textLivingYears.Text = Convert.ToString(DR["IANA_LIVING_UAE"]);
                    radMarried.SelectedValue = Convert.ToString(DR["IANA_MARRIED"]);

                    if (Convert.ToString(DR["IANA_SMOKE"]) == radSmokeYes.Value)
                    {
                        radSmokeYes.Checked = true;
                    }
                    else if (Convert.ToString(DR["IANA_SMOKE"]) == radSmokeNo.Value)
                    {
                        radSmokeNo.Checked = true;
                    }


                    CigsCount.Text = Convert.ToString(DR["IANA_SMOKE_COUNT"]);

                    string strSmokeQuit = Convert.ToString(DR["IANA_QUIT_SMOKE"]);
                    string[] arrSmokeQuit = strSmokeQuit.Split('|');
                    if (arrSmokeQuit.Length > 0)
                    {
                        txtSmokeQuitYears.Text = arrSmokeQuit[0];
                        if (arrSmokeQuit.Length > 1)
                        {
                            txtSmokeQuitMonth.Text = arrSmokeQuit[1];
                        }

                    }


                    if (Convert.ToString(DR["IANA_ALCOHOL"]) == radAlcoholYes.Value)
                    {
                        radAlcoholYes.Checked = true;
                    }
                    else if (Convert.ToString(DR["IANA_ALCOHOL"]) == radAlcoholNo.Value)
                    {
                        radAlcoholNo.Checked = true;
                    }

                    string strAlcoholQuit = Convert.ToString(DR["IANA_QUIT_ALCOHOL"]);
                    string[] arrAlcoholQuit = strAlcoholQuit.Split('|');
                    if (arrAlcoholQuit.Length > 0)
                    {
                        txtDrinkQuitYears.Text = arrAlcoholQuit[0];
                        if (arrSmokeQuit.Length > 1)
                        {
                            txtDrinkQuitMonth.Text = arrAlcoholQuit[1];
                        }

                    }

                    for (Int32 i = 0; i < chkPresPastHealthProb.Items.Count - 1; i++)
                    {

                        if (chkPresPastHealthProb.Items[i].Selected == true)
                        {
                            objAdmNurAss.IANA_PRES_HEALTH_PROB += chkPresPastHealthProb.Items[i].Value + "|";
                        }
                    }

                    string strPresPastHealthProb = Convert.ToString(DR["IANA_PRES_HEALTH_PROB"]);

                    string[] arrPresPastHealthProb = strPresPastHealthProb.Split('|');

                    if (arrPresPastHealthProb.Length > 0)
                    {
                        for (Int32 i = 0; i < chkPresPastHealthProb.Items.Count - 1; i++)
                        {
                            for (Int32 j = 0; j < arrPresPastHealthProb.Length; j++)
                            {

                                if (chkPresPastHealthProb.Items[i].Value == arrPresPastHealthProb[j])
                                {
                                    chkPresPastHealthProb.Items[i].Selected = true;
                                    goto PresPastProbtForEnd;
                                }

                            }
                        PresPastProbtForEnd: ;
                        }

                    }


                    txtProbIdentified.Text = Convert.ToString(DR["IANA_PROB_IDENTIFIED"]);
                    txtPrevHostSurgeries.Text = Convert.ToString(DR["IANA_PREV_HOSP_SURG"]);



                    if (Convert.ToString(DR["IANA_ALLERGY"]) == radAlergiesYes.Value)
                    {
                        radAlergiesYes.Checked = true;
                    }
                    else if (Convert.ToString(DR["IANA_ALLERGY"]) == radAlergiesNo.Value)
                    {
                        radAlergiesNo.Checked = true;
                    }



                    txtAlergies.Text = Convert.ToString(DR["IANA_ALLERGY_DESC"]);
                    txtReactionType.Text = Convert.ToString(DR["IANA_REACTION_TYPE"]);

                    if (Convert.ToString(DR["IANA_MEDICINE_BROUGHT"]) == "True")
                    {
                        chkMedicineBrought.Checked = true;
                    }
                    else
                    {
                        chkMedicineBrought.Checked = false;
                    }

                    if (Convert.ToString(DR["IANA_PHYSICIAN_VARIFIED"]) == "True")
                    {
                        chkVarifiedPhysician.Checked = true;
                    }
                    else
                    {
                        chkVarifiedPhysician.Checked = false;
                    }

                    txtSentHomeWith.Text = Convert.ToString(DR["IANA_MED_SENT_HOME_WITH"]);
                    txtPainScore.Text = Convert.ToString(DR["IANA_PAIN_SCORE"]);
                    PainScoreType.SelectedValue = Convert.ToString(DR["IANA_PAIN_SCORE_TYPE"]);
                    txtPainLocation.Text = Convert.ToString(DR["IANA_PAIN_LOCATION"]);
                    txtPainOnset.Text = Convert.ToString(DR["IANA_PAIN_ONSET"]);
                    txtPainVariations.Text = Convert.ToString(DR["IANA_PAIN_VARIATIONS"]);
                    drpPainAggravates.SelectedValue = Convert.ToString(DR["IANA_PAIN_AGGRAVATES"]);
                    drpPainRelieves.SelectedValue = Convert.ToString(DR["IANA_PAIN_RELIEVES"]);
                    txtMedications.Text = Convert.ToString(DR["IANA_PAIN_MEDICATIONS"]);
                    drpPainEffects.SelectedValue = Convert.ToString(DR["IANA_PAIN_EFFECTS"]);

                    lblLAMAFileName.Text = Convert.ToString(DR["IANA_LAMA_FILE_NAME"]);

                }
            }
            else
            {
                BindAdmissionSummary();
            }

        }

        #endregion

        #region MedicationMethods


        void BindPharmacyGrid()
        {

            DataSet ds = new DataSet();
            objAdmNurAss = new IP_AdmissionNursingAssessment();

            string Criteria = " 1=1 ";
            Criteria += " AND IANM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IANM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            ds = objAdmNurAss.AdmiNurseMedicHistoryGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvPharmacy.DataSource = ds;
                gvPharmacy.DataBind();


                string strPermission = hidPermission.Value;
                if (strPermission == "1" || strPermission == "7")
                {
                    gvPharmacy.Columns[0].Visible = false;
                }

            }
            else
            {
                gvPharmacy.DataBind();
            }
        }

        void BindRoute()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND EPR_ACTIVE=1 ";
            Criteria += " AND EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.EMRRouteGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpRoute.DataSource = DS;
                drpRoute.DataTextField = "EPR_NAME";
                drpRoute.DataValueField = "EPR_CODE";
                drpRoute.DataBind();
            }
            drpRoute.Items.Insert(0, "Select Route");
            drpRoute.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void ClearPhar()
        {
            txtServCode.Text = "";
            txtServName.Text = "";
            txtDossage1.Text = "";

            if (drpRoute.Items.Count > 0)
                drpRoute.SelectedIndex = 0;

            txtFreqyency.Text = "";
            drpFreqType.SelectedIndex = 0;


            drpLastDoseHour.SelectedIndex = 0;
            drpLastDosemin.SelectedIndex = 0;

            if (Convert.ToString(ViewState["PharmacyIndex"]) != "")
            {
                gvPharmacy.Rows[Convert.ToInt32(ViewState["PharmacyIndex"])].BackColor = System.Drawing.Color.White;
            }
            ViewState["PharmacyIndex"] = "";
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_ADMI_NURSE_ASS' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Visible = false;
                btnAddPhar.Visible = false;
                btnUpload.Visible = false;
                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSave.Visible = false;
                btnAddPhar.Visible = false;
                btnUpload.Visible = false;
                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }
        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetServicessList(string prefixText)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string[] Data;


            ds = dbo.HaadServicessListGet("Pharmacy", prefixText, "", strSessionDeptId);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["Code"].ToString() + "~" + ds.Tables[0].Rows[i]["Description"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion


        DataSet GetSegmentData(string strType, string Position)
        {
            objAdmNurAss = new IP_AdmissionNursingAssessment();
            string Criteria = " 1=1 AND INAS_ACTIVE=1 AND INAS_BRANCH='" + Convert.ToString(Session["Branch_ID"]) + "' AND INAS_TYPE='" + strType + "'";
            Criteria += " AND  INAS_POSITION='" + Position + "'";

            DataSet DS = new DataSet();
            DS = objAdmNurAss.AdminNurseAssSegmentGet(Criteria);


            return DS;

        }

        DataSet GetSegmentMaster(string strType)
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 AND INASM_STATUS=1 AND INASM_TYPE='" + strType + "'";


            DataSet DS = new DataSet();
            DS = objCom.fnGetFieldValue(" * ", "IP_ADMIN_NURSE_ASS_SEGMENT_MASTER", Criteria, "");


            return DS;
        }

        public void CreateSegCtrls(string strType, string Position)
        {
            DataSet DS = new DataSet();

            DS = GetSegmentData(strType, Position);

            foreach (DataRow DR in DS.Tables[0].Rows)
            {


                Panel myFieldSet = new Panel();

                myFieldSet.GroupingText = Convert.ToString(DR["INAS_NAME"]);
                myFieldSet.Style.Add("text-align", "justify");
                myFieldSet.CssClass = "lblCaption1";
                myFieldSet.Width = 500;
                Int32 i = 1;

                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["INAS_CODE"]));

                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {

                    HtmlGenericControl trHe1 = new HtmlGenericControl("tr");

                    HtmlGenericControl tdHe1 = new HtmlGenericControl("td");


                    Boolean _checked = false;
                    string strComment = "";
                    objAdmNurAss = new IP_AdmissionNursingAssessment();
                    string Criteria2 = "1=1 ";
                    Criteria2 += " AND INASD_TYPE='" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "' AND INASD_FIELD_ID='" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim() + "' ";
                    Criteria2 += " AND INASD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND INASD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

                    DataSet DS2 = new DataSet();
                    DS2 = objAdmNurAss.AdminNurseAssSegmentDtlsGet(Criteria2);

                    if (DS2.Tables[0].Rows.Count > 0)
                    {
                        _checked = true;

                        strComment = Convert.ToString(DS2.Tables[0].Rows[0]["INASD_COMMENT"]).Trim();
                    }


                    if (Convert.ToString(DR1["INASM_CONTROL_TYPE"]).ToUpper() == "CheckBox".ToUpper())
                    {
                        CheckBox chk = new CheckBox();
                        chk.ID = "chk" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim();
                        chk.Text = Convert.ToString(DR1["INASM_FIELD_NAME"]).Trim();
                        chk.CssClass = "lblCaption1";

                        chk.Checked = _checked;

                        //tdHe1.Controls.Add(chk);
                        //trHe1.Controls.Add(tdHe1);
                        //myFieldSet.Controls.Add(trHe1);

                        myFieldSet.Controls.Add(chk);
                    }


                    if (Convert.ToString(DR1["INASM_CONTROL_TYPE"]).ToUpper() == "TextBox".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["INASM_FIELD_NAME"]).Trim() + ":";
                        myFieldSet.Controls.Add(lbl);

                        Literal lit1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                        myFieldSet.Controls.Add(lit1);

                        TextBox txt = new TextBox();
                        txt.ID = "txt" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["INASM_FIELD_NAME"]);
                        txt.CssClass = "Label";
                        txt.Style.Add("width", "99%");

                        txt.Text = strComment;

                        myFieldSet.Controls.Add(txt);


                    }

                    if (Convert.ToString(DR1["INASM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Width = 150;
                        lbl.Text = Convert.ToString(DR1["INASM_FIELD_NAME"]).Trim() + ":";
                        myFieldSet.Controls.Add(lbl);

                        TextBox txt = new TextBox();
                        txt.ID = "txt" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["INASM_FIELD_NAME"]);
                        txt.CssClass = "Label";
                        txt.Style.Add("width", "75px");

                        AjaxControlToolkit.CalendarExtender CalendarExtender = new AjaxControlToolkit.CalendarExtender();
                        CalendarExtender.TargetControlID = txt.ID;
                        CalendarExtender.Format = "dd/MM/yyyy";


                        txt.Text = strComment;

                        myFieldSet.Controls.Add(txt);
                        myFieldSet.Controls.Add(CalendarExtender);
                        //tdHe1.Controls.Add(txt);
                        //trHe1.Controls.Add(tdHe1);
                        //myFieldSet.Controls.Add(trHe1);

                    }




                    if (Convert.ToString(DR1["INASM_CONTROL_TYPE"]).ToUpper() == "Label".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["INASM_FIELD_NAME"]).Trim();
                        myFieldSet.Controls.Add(lbl);

                        //tdHe1.Controls.Add(lbl);
                        //trHe1.Controls.Add(tdHe1);
                        //myFieldSet.Controls.Add(trHe1);

                        //HtmlGenericControl trHe2 = new HtmlGenericControl("tr");
                        //HtmlGenericControl tdHe21 = new HtmlGenericControl("td");
                        //tdHe21.Controls.Add(lbl);
                        //trHe2.Controls.Add(tdHe21);
                        //myFieldSet.Controls.Add(trHe2);

                    }

                    Literal lit = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                    myFieldSet.Controls.Add(lit);

                }

                //if (Convert.ToString(DR["IASM_CONTROL_TYPE"]) == "RadioButtonList")
                //{

                //    intIndex = intIndex + 1;
                //    RadList.Items.Add(new ListItem(Convert.ToString(DR["IASM_FIELD_NAME"]), Convert.ToString(DR["IASM_FIELD_ID"])));
                //    RadList.CssClass = "lblCaption1";
                //    if (_checked == true)
                //    {
                //        RadList.SelectedValue = Convert.ToString(DR["IASM_FIELD_ID"]);
                //    }
                //    myFieldSet.Controls.Add(RadList);

                //}
                if (strType == "IP_NURSE_ASSESSMENT_PHY")
                {
                    if (Position == "LEFT")
                    {
                        plhoPhyAssessmentLeft.Controls.Add(myFieldSet);
                    }
                    else
                    {
                        plhoPhyAssessmentRight.Controls.Add(myFieldSet);
                    }
                }

                if (strType == "IP_NURSE_ASSESSMENT_RSA")
                {
                    if (Position == "LEFT")
                    {
                        plhoRiskSafetyAssessmentLeft.Controls.Add(myFieldSet);
                    }
                    else
                    {

                    }
                }

                if (strType == "IP_NURSE_ASSESSMENT_EDU")
                {
                    if (Position == "LEFT")
                    {
                        plhoEducationalAssessmentLeft.Controls.Add(myFieldSet);
                    }
                    else
                    {

                    }
                }


                if (strType == "IP_NURSE_ASSESSMENT_FUN")
                {
                    if (Position == "LEFT")
                    {
                        plhoFunctionalAssessmentLeft.Controls.Add(myFieldSet);
                    }
                    else
                    {

                    }
                }


                if (strType == "IP_NURSE_ASSESSMENT_BRSKIN")
                {
                    if (Position == "RIGHT")
                    {
                        plhoBraSkinRiskAssessmentRight.Controls.Add(myFieldSet);
                    }
                    else
                    {

                    }
                }

                if (strType == "IP_NURSE_ASSESSMENT_DISCH")
                {
                    if (Position == "LEFT")
                    {
                        plhoPatientDischAssessmentLeft.Controls.Add(myFieldSet);
                    }
                    else
                    {
                        plhoPatientDischAssessmentRight.Controls.Add(myFieldSet);
                    }
                }

                if (strType == "IP_NURSE_ASSESSMENT_DISCH_SUM")
                {
                    if (Position == "LEFT")
                    {
                        plhoPatientDischSummaryLeft.Controls.Add(myFieldSet);
                    }
                    else
                    {
                         
                    }
                }

                i = i + 1;



            }
        }

        public void SaveSegmentDtls(string strType, string FieldID, string FieName, string Selected, string Comment)
        {
            objAdmNurAss = new IP_AdmissionNursingAssessment();

            objAdmNurAss.BranchID = Convert.ToString(Session["Branch_ID"]);
            objAdmNurAss.EMRID = Convert.ToString(Session["EMR_ID"]);
            objAdmNurAss.PTID = Convert.ToString(Session["EMR_PT_ID"]);
            objAdmNurAss.INASD_TYPE = strType;
            objAdmNurAss.INASD_FIELD_ID = FieldID;
            objAdmNurAss.INASD_FIELD_NAME = FieName;
            objAdmNurAss.INASD_VALUE = Selected;
            objAdmNurAss.INASD_COMMENT = Comment;
            objAdmNurAss.AdminNurseAssSegmentDtlsAdd();
        }


        public void SaveSegment(string strType, string Position)
        {
            DataSet DS = new DataSet();

            DS = GetSegmentData(strType, Position);

            foreach (DataRow DR in DS.Tables[0].Rows)
            {

                objCom = new CommonBAL();

                string CriteriaDel = " 1=1 AND INASD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND INASD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                CriteriaDel += " AND INASD_TYPE='" + Convert.ToString(DR["INAS_CODE"]).Trim() + "'";
                objCom.fnDeleteTableData("IP_ADMIN_NURSE_ASS_SEGMENT_DTLS", CriteriaDel);


                Int32 i = 1;
                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["INAS_CODE"]).Trim());



                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {
                    string strValueYes = "", _Comment = "";
                    if (Convert.ToString(DR1["INASM_CONTROL_TYPE"]).ToUpper() == "CheckBox".ToUpper())
                    {
                        CheckBox chk1 = new CheckBox();
                        if (strType == "IP_NURSE_ASSESSMENT_PHY")
                        {
                            if (Position == "LEFT")
                            {
                                chk1 = (CheckBox)plhoPhyAssessmentLeft.FindControl("chk" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());

                            }
                            else
                            {
                                chk1 = (CheckBox)plhoPhyAssessmentRight.FindControl("chk" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());

                            }
                        }

                        if (strType == "IP_NURSE_ASSESSMENT_RSA")
                        {
                            if (Position == "LEFT")
                            {
                                chk1 = (CheckBox)plhoRiskSafetyAssessmentLeft.FindControl("chk" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());

                            }
                            else
                            {

                            }
                        }



                        if (strType == "IP_NURSE_ASSESSMENT_EDU")
                        {
                            if (Position == "LEFT")
                            {
                                chk1 = (CheckBox)plhoEducationalAssessmentLeft.FindControl("chk" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());

                            }
                            else
                            {

                            }
                        }

                        if (strType == "IP_NURSE_ASSESSMENT_FUN")
                        {
                            if (Position == "LEFT")
                            {
                                chk1 = (CheckBox)plhoFunctionalAssessmentLeft.FindControl("chk" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());

                            }
                            else
                            {

                            }
                        }


                        if (strType == "IP_NURSE_ASSESSMENT_BRSKIN")
                        {
                            if (Position == "RIGHT")
                            {
                                chk1 = (CheckBox)plhoBraSkinRiskAssessmentRight.FindControl("chk" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());

                            }
                            else
                            {

                            }
                        }
                        if (strType == "IP_NURSE_ASSESSMENT_DISCH")
                        {
                            if (Position == "LEFT")
                            {
                                chk1 = (CheckBox)plhoPatientDischAssessmentLeft.FindControl("chk" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());

                            }
                            else
                            {
                                chk1 = (CheckBox)plhoPatientDischAssessmentRight.FindControl("chk" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());

                            }
                        }
                        if (strType == "IP_NURSE_ASSESSMENT_DISCH_SUM")
                        {
                            if (Position == "LEFT")
                            {
                                chk1 = (CheckBox)plhoPatientDischSummaryLeft.FindControl("chk" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());

                            }
                            else
                            {

                            }
                        }




                        if (chk1 != null)
                        {
                            if (chk1.Checked == true)
                            {
                                strValueYes = "Y";
                            }
                        }

                    }
                    if (Convert.ToString(DR1["INASM_CONTROL_TYPE"]).ToUpper() == "TextBox".ToUpper() || Convert.ToString(DR1["INASM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                    {
                        TextBox txt = new TextBox();
                        if (strType == "IP_NURSE_ASSESSMENT_PHY")
                        {
                            if (Position == "LEFT")
                            {
                                txt = (TextBox)plhoPhyAssessmentLeft.FindControl("txt" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());
                            }
                            else
                            {
                                txt = (TextBox)plhoPhyAssessmentRight.FindControl("txt" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());

                            }
                        }

                        if (strType == "IP_NURSE_ASSESSMENT_RSA")
                        {
                            if (Position == "LEFT")
                            {
                                txt = (TextBox)plhoRiskSafetyAssessmentLeft.FindControl("txt" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());
                            }
                            else
                            {


                            }
                        }

                        if (strType == "IP_NURSE_ASSESSMENT_FUN")
                        {
                            if (Position == "LEFT")
                            {
                                txt = (TextBox)plhoFunctionalAssessmentLeft.FindControl("txt" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());
                            }
                            else
                            {


                            }
                        }


                        if (strType == "IP_NURSE_ASSESSMENT_EDU")
                        {
                            if (Position == "LEFT")
                            {
                                txt = (TextBox)plhoEducationalAssessmentLeft.FindControl("txt" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());
                            }
                            else
                            {


                            }
                        }



                        if (strType == "IP_NURSE_ASSESSMENT_BRSKIN")
                        {
                            if (Position == "RIGHT")
                            {
                                txt = (TextBox)plhoBraSkinRiskAssessmentRight.FindControl("txt" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());
                            }
                            else
                            {


                            }
                        }


                        if (strType == "IP_NURSE_ASSESSMENT_DISCH")
                        {
                            if (Position == "LEFT")
                            {
                                txt = (TextBox)plhoPatientDischAssessmentLeft.FindControl("txt" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());
                            }
                            else
                            {
                                txt = (TextBox)plhoPatientDischAssessmentRight.FindControl("txt" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());


                            }
                        }


                        if (strType == "IP_NURSE_ASSESSMENT_DISCH_SUM")
                        {
                            if (Position == "LEFT")
                            {
                                txt = (TextBox)plhoPatientDischSummaryLeft.FindControl("txt" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim());
                            }
                            else
                            {


                            }
                        }

                        if (txt.Text != "")
                        {
                            strValueYes = "Y";
                            _Comment = txt.Text;
                        }
                    }



                    if (strValueYes != "")
                    {
                        SaveSegmentDtls(Convert.ToString(DR["INAS_CODE"]).Trim(), Convert.ToString(DR1["INASM_FIELD_ID"]).Trim(), Convert.ToString(DR1["INASM_FIELD_NAME"]).Trim(), strValueYes, _Comment);

                    }

                }
                i = i + 1;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {

                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                    {
                        SetPermission();
                    }


                    this.Page.Title = "AdmissionNursingAssessment";

                    lblLAMAFileName.Text = "";

                    strSessionDeptId = Convert.ToString(Session["User_DeptID"]);

                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());

                    txtArrivalDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtAdmiDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    txtLastDoseDate.Text = strFromDate.ToString("dd/MM/yyyy");
                  

                    BindTime();
                    
                    BindWardNo();
                    BindOrientationUnit();
                    BindPresPastHealthProb();
                    BindRoute();

                    BindData();
                    BindPharmacyGrid();

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }


        public void generateDynamicControls()
        {

            CreateSegCtrls("IP_NURSE_ASSESSMENT_PHY", "LEFT");
            CreateSegCtrls("IP_NURSE_ASSESSMENT_PHY", "RIGHT");

            CreateSegCtrls("IP_NURSE_ASSESSMENT_RSA", "LEFT");
            CreateSegCtrls("IP_NURSE_ASSESSMENT_EDU", "LEFT");
            CreateSegCtrls("IP_NURSE_ASSESSMENT_FUN", "LEFT");
            CreateSegCtrls("IP_NURSE_ASSESSMENT_BRSKIN", "RIGHT");

            CreateSegCtrls("IP_NURSE_ASSESSMENT_DISCH", "LEFT");
            CreateSegCtrls("IP_NURSE_ASSESSMENT_DISCH_SUM", "LEFT");
            CreateSegCtrls("IP_NURSE_ASSESSMENT_DISCH", "RIGHT");

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                 

                objAdmNurAss = new IP_AdmissionNursingAssessment();
                objAdmNurAss.BranchID = Convert.ToString(Session["Branch_ID"]);
                objAdmNurAss.EMRID = Convert.ToString(Session["EMR_ID"]);
                objAdmNurAss.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                objAdmNurAss.IANA_ADMISSION_ROUTE = txtAdmissRoute.Text;
                objAdmNurAss.IANA_ADMISSION_WARD = drpWardNo.SelectedValue;
                objAdmNurAss.IANA_ROOM_NO = drpRoomNo.SelectedValue;
                objAdmNurAss.IANA_BED_NO = drpBed.SelectedValue;
                objAdmNurAss.IANA_WARD_ARRIVAL_TIME = txtArrivalDate.Text.Trim() + " " + drpArrivalHour.SelectedValue + ":" + drpArrivalMin.SelectedValue + ":00";
                objAdmNurAss.IANA_ADMISSION_DATE = txtAdmiDate.Text.Trim() + " " + drpAdmiHour.SelectedValue + ":" + drpAdmiMin.SelectedValue + ":00";
                objAdmNurAss.IANA_ADMISSION_MODE = drpAdmissionMode.SelectedValue;
                objAdmNurAss.IANA_INFO_OBTAINED_FROM = drpInfObtFrom.SelectedValue;
                objAdmNurAss.IANA_DIAGNOSIS = txtDiag.Text;
                objAdmNurAss.IANA_WEIGHT = txtWeight.Text;
                objAdmNurAss.IANA_HEIGHT = txtHeight.Text;
                objAdmNurAss.IANA_PT_PAIN_SCORE = txtPTPainScore.Text;
                for (Int32 i = 0; i < chkPresPastHealthProb.Items.Count - 1; i++)
                {

                    if (chkOrientationUnit.Items[i].Selected == true)
                    {
                        objAdmNurAss.IANA_ORIENTATION_UNIT += chkOrientationUnit.Items[i].Value + "|";
                    }
                }
                objAdmNurAss.IANA_BROUGHT_VALUABLE = radValuables.SelectedValue;
                objAdmNurAss.IANA_SENT_HOME_WITH = txtPTSenthWith.Text;
                objAdmNurAss.IANA_PRO_GIVEN_BY = txtProGivenBy.Text;
                objAdmNurAss.IANA_LIVING_SITUATION = drpLivingWith.SelectedValue;
                objAdmNurAss.IANA_PROFESSION = drpProfession.SelectedValue;
                objAdmNurAss.IANA_LIVING_UAE = textLivingYears.Text;
                objAdmNurAss.IANA_MARRIED = radMarried.SelectedValue;
                if (radSmokeYes.Checked == true)
                {
                    objAdmNurAss.IANA_SMOKE = radSmokeYes.Value;
                }
                else if (radSmokeNo.Checked == true)
                {
                    objAdmNurAss.IANA_SMOKE = radSmokeNo.Value;
                }

                objAdmNurAss.IANA_SMOKE_COUNT = CigsCount.Text;
                objAdmNurAss.IANA_QUIT_SMOKE = txtSmokeQuitYears.Text + "|" + txtSmokeQuitMonth.Text;

                if (radAlcoholYes.Checked == true)
                {
                    objAdmNurAss.IANA_ALCOHOL = radAlcoholYes.Value;
                }
                else if (radAlcoholNo.Checked == true)
                {
                    objAdmNurAss.IANA_ALCOHOL = radAlcoholNo.Value;
                }
                objAdmNurAss.IANA_QUIT_ALCOHOL = txtDrinkQuitYears.Text + "|" + txtDrinkQuitMonth.Text;

                for (Int32 i = 0; i < chkPresPastHealthProb.Items.Count - 1; i++)
                {

                    if (chkPresPastHealthProb.Items[i].Selected == true)
                    {
                        objAdmNurAss.IANA_PRES_HEALTH_PROB += chkPresPastHealthProb.Items[i].Value + "|";
                    }
                }
                objAdmNurAss.IANA_PROB_IDENTIFIED = txtProbIdentified.Text;
                objAdmNurAss.IANA_PREV_HOSP_SURG = txtPrevHostSurgeries.Text;
                if (radAlergiesYes.Checked == true)
                {
                    objAdmNurAss.IANA_ALLERGY = radAlergiesYes.Value;
                }
                else if (radAlergiesNo.Checked == true)
                {
                    objAdmNurAss.IANA_ALLERGY = radAlergiesNo.Value;
                }


                objAdmNurAss.IANA_ALLERGY_DESC = txtAlergies.Text;
                objAdmNurAss.IANA_REACTION_TYPE = txtReactionType.Text;

                if (chkMedicineBrought.Checked == true)
                {
                    objAdmNurAss.IANA_MEDICINE_BROUGHT = "True";
                }
                else
                {
                    objAdmNurAss.IANA_MEDICINE_BROUGHT = "False";
                }

                if (chkVarifiedPhysician.Checked == true)
                {
                    objAdmNurAss.IANA_PHYSICIAN_VARIFIED = "True";
                }
                else
                {
                    objAdmNurAss.IANA_PHYSICIAN_VARIFIED = "False";
                }


                objAdmNurAss.IANA_MED_SENT_HOME_WITH = txtSentHomeWith.Text;
                objAdmNurAss.IANA_PAIN_SCORE = txtPainScore.Text;
                objAdmNurAss.IANA_PAIN_SCORE_TYPE = PainScoreType.SelectedValue;
                objAdmNurAss.IANA_PAIN_LOCATION = txtPainLocation.Text;
                objAdmNurAss.IANA_PAIN_ONSET = txtPainOnset.Text;
                objAdmNurAss.IANA_PAIN_VARIATIONS = txtPainVariations.Text;
                objAdmNurAss.IANA_PAIN_AGGRAVATES = drpPainAggravates.SelectedValue;
                objAdmNurAss.IANA_PAIN_RELIEVES = drpPainRelieves.SelectedValue;
                objAdmNurAss.IANA_PAIN_MEDICATIONS = txtMedications.Text;
                objAdmNurAss.IANA_PAIN_EFFECTS = drpPainEffects.SelectedValue;
                objAdmNurAss.IANA_LAMA_FILE_NAME = lblLAMAFileName.Text;
                objAdmNurAss.AdmiNursingAssessmentAdd();

                SaveSegment("IP_NURSE_ASSESSMENT_PHY", "LEFT");
                SaveSegment("IP_NURSE_ASSESSMENT_PHY", "RIGHT");

                SaveSegment("IP_NURSE_ASSESSMENT_RSA", "LEFT");
                SaveSegment("IP_NURSE_ASSESSMENT_EDU", "LEFT");
                SaveSegment("IP_NURSE_ASSESSMENT_FUN", "LEFT");
                SaveSegment("IP_NURSE_ASSESSMENT_BRSKIN", "RIGHT");



                SaveSegment("IP_NURSE_ASSESSMENT_DISCH", "LEFT");
                SaveSegment("IP_NURSE_ASSESSMENT_DISCH_SUM", "LEFT");
                SaveSegment("IP_NURSE_ASSESSMENT_DISCH", "RIGHT");


              

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpWardNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindRoom();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "drpWardNo_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpRoomNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindBed();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "drpRoomNo_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnAddPhar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtServCode.Text.Trim() == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Enter Medication Details')", true);
                    goto FunEnd;
                }
                objAdmNurAss = new IP_AdmissionNursingAssessment();
                objAdmNurAss.BranchID = Convert.ToString(Session["Branch_ID"]);
                objAdmNurAss.EMRID = Convert.ToString(Session["EMR_ID"]);
                objAdmNurAss.IANM_PHY_CODE = txtServCode.Text;
                objAdmNurAss.IANM_PHY_NAME = txtServName.Text.Replace("'", "''");

                objAdmNurAss.IANM_DOSAGE = txtDossage1.Text;
                objAdmNurAss.IANM_ROUTE = drpRoute.SelectedValue;
                objAdmNurAss.IANM_FREQUENCY = txtFreqyency.Text.Trim();
                objAdmNurAss.IANM_FREQUENCYTYPE = drpFreqType.SelectedValue;
                objAdmNurAss.IANM_LAST_DOSAGE = txtLastDoseDate.Text.Trim() + " " + drpLastDoseHour.SelectedValue + ":" + drpLastDosemin.SelectedValue + ":00";
                objAdmNurAss.AdmiNurseMedicHistoryAdd();

                BindPharmacyGrid();
                ClearPhar();
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAddPhar_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteeDiag_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblPhyCode = (Label)gvScanCard.Cells[0].FindControl("lblPhyCode");



                string Criteria = " 1=1 ";
                Criteria += " AND IANM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND IANM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                Criteria += " AND IANM_PHY_CODE='" + lblPhyCode.Text + "'";



                objCom = new CommonBAL();
                objCom.fnDeleteTableData("IP_ADMISSION_NURSE_MEDIC_HISTORY", Criteria);

                BindPharmacyGrid();
                ClearPhar();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "DeletePhar_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void PhySelect_Click(object sender, EventArgs e)
        {
            try
            {


                if (Convert.ToString(ViewState["PharmacyIndex"]) != "")
                {
                    gvPharmacy.Rows[Convert.ToInt32(ViewState["PharmacyIndex"])].BackColor = System.Drawing.Color.White;
                }

                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                ViewState["PharmacyIndex"] = gvScanCard.RowIndex;

                gvPharmacy.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");


                Label lblPhyCode = (Label)gvScanCard.Cells[0].FindControl("lblPhyCode");
                Label lblPhyDesc = (Label)gvScanCard.Cells[0].FindControl("lblPhyDesc");


                Label lblPhyDosage = (Label)gvScanCard.Cells[0].FindControl("lblPhyDosage");


                Label lblPhyRoute = (Label)gvScanCard.Cells[0].FindControl("lblPhyRoute");



                Label lblPhyFrequency = (Label)gvScanCard.Cells[0].FindControl("lblPhyFrequency");
                Label lblPhyFrequencyType = (Label)gvScanCard.Cells[0].FindControl("lblPhyFrequencyType");

                Label lblLastDoseDate = (Label)gvScanCard.Cells[0].FindControl("lblLastDoseDate");
                Label lblLastDoseTime = (Label)gvScanCard.Cells[0].FindControl("lblLastDoseTime");


                txtServCode.Text = lblPhyCode.Text;
                txtServName.Text = lblPhyDesc.Text;


                txtDossage1.Text = lblPhyDosage.Text;

                //  drpDossage.SelectedValue = lblPhyDosage.Text;


                drpRoute.SelectedValue = lblPhyRoute.Text;


                txtFreqyency.Text = lblPhyFrequency.Text;
                drpFreqType.SelectedValue = lblPhyFrequencyType.Text;


                // txtRefil.Text = lblPhyRefil.Text;
                // txtFromDate.Text = lblPhyStDate.Text;
                // txtToDate.Text = lblPhyEndDate.Text;

                //  chkLifeLong.Checked = Convert.ToBoolean(lblPhyLifeLong.Text);
                txtLastDoseDate.Text = lblLastDoseDate.Text;

                string strlblLastDoseTime = lblLastDoseTime.Text;
                string[] arrlblLastDoseTime = strlblLastDoseTime.Split(':');
                if (arrlblLastDoseTime.Length > 0)
                {

                    drpLastDoseHour.SelectedValue = arrlblLastDoseTime[0];
                    if (arrlblLastDoseTime.Length > 1)
                    {
                        drpLastDosemin.SelectedValue = arrlblLastDoseTime[1];
                    }
                }





            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "PhySelect_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            lblLAMAFileName.Text = "";

            string SCanPath, FileExt = "";

            SCanPath = @Convert.ToString(Session["EMR_IP_DISC_LAMA_FILE_PATH"]);

            if (!Directory.Exists(@SCanPath))
            {
                Directory.CreateDirectory(@SCanPath);
            }
            string strTimeStamp = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;

            FileExt = Path.GetExtension(fileLogo.FileName);

            string LAMAFileName = Convert.ToString(Session["IAS_ADMISSION_NO"]) + "_" + strTimeStamp + FileExt;

            fileLogo.SaveAs(SCanPath + "\\" + LAMAFileName );

            lblLAMAFileName.Text = LAMAFileName;
        }

    }
}