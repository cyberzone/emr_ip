﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;
using Newtonsoft.Json;

namespace EMR_IP.Patient
{
    public partial class BedTransfer : System.Web.UI.Page
    {

       CommonBAL objCom = new CommonBAL();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {

                drpTransHour.DataSource = DS;
                drpTransHour.DataTextField = "Name";
                drpTransHour.DataValueField = "Code";
                drpTransHour.DataBind();

         

            }

            drpTransHour.Items.Insert(0, "00");
            drpTransHour.Items[0].Value = "00";

            

            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {


                drpTransMin.DataSource = DS;
                drpTransMin.DataTextField = "Name";
                drpTransMin.DataValueField = "Code";
                drpTransMin.DataBind();

                
            }

            drpTransMin.Items.Insert(0, "00");
            drpTransMin.Items[0].Value = "00";

            


        }

        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        public static string[] GetDoctorDtls(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND ( HSFM_STAFF_ID Like '%" + prefixText + "%'  OR  HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%'  ) ";
            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        public DataTable DerializeDataTable(string json)
        {
            // const string json = @"[{""Name"":""AAA"",""Age"":""22"",""Job"":""PPP""},"
            //                  + @"{""Name"":""BBB"",""Age"":""25"",""Job"":""QQQ""},"
            //                  + @"{""Name"":""CCC"",""Age"":""38"",""Job"":""RRR""}]";
            var table = JsonConvert.DeserializeObject<DataTable>(json);
            return table;
        }

        /*
        void BindWardNo()
        {


            string strjson;
            DataSet DS = new DataSet();
            DataSet DS1 = new DataSet();
            DataTable DT = new DataTable();

            IP_ResourceDatas objRD = new IP_ResourceDatas();
            string Criteria = " 1=1 ";
            Criteria += " and RD_TYPE='WARD_MASTER' and RD_IS_DELETED = 0 ";
            DS = objRD.IPResourceDatasGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strjson = "[";
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    strjson += DR["RD_DATA"] + ",";

                }
                strjson += "]";

                DT = DerializeDataTable(strjson);

                if (DT.Rows.Count > 0)
                {
                    drpWardNo.DataSource = DT;
                    drpWardNo.DataTextField = "Code";
                    drpWardNo.DataValueField = "Code";
                    drpWardNo.DataBind();
                }

                drpWardNo.Items.Insert(0, "Select Code");
                drpWardNo.Items[0].Value = "";
            }



        }

        void BindRoom()
        {
            drpRoomNo.Items.Clear();
            drpRoomNo.Items.Insert(0, "Select Code");
            drpRoomNo.Items[0].Value = "";

            drpBed.Items.Clear();
            drpBed.Items.Insert(0, "Select Code");
            drpBed.Items[0].Value = "";

            string strjson;
            DataSet DS = new DataSet();
            DataSet DS1 = new DataSet();
            DataTable DT = new DataTable();

            IP_ResourceDatas objRD = new IP_ResourceDatas();
            string Criteria = " 1=1 ";
            Criteria += " and RD_TYPE='ROOM_MASTER' and RD_IS_DELETED = 0 ";
            DS = objRD.IPResourceDatasGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strjson = "[";
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    strjson += DR["RD_DATA"] + ",";

                }
                strjson += "]";

                DT = DerializeDataTable(strjson);
                if (DT.Rows.Count > 0)
                {
                    DataRow[] result = DT.Select("Ward='" + drpWardNo.SelectedValue + "'");

                    foreach (DataRow DR in result)
                    {
                        drpRoomNo.Items.Insert(0 + 1, Convert.ToString(DR[0]));
                        drpRoomNo.Items[0 + 1].Value = Convert.ToString(DR[0]);

                    }

                }

            }

            ;

        }

        void BindBed()
        {



            drpBed.Items.Clear();
            drpBed.Items.Insert(0, "Select Code");
            drpBed.Items[0].Value = "";

            string strjson;
            DataSet DS = new DataSet();
            DataSet DS1 = new DataSet();
            DataTable DT = new DataTable();

            IP_ResourceDatas objRD = new IP_ResourceDatas();
            string Criteria = " 1=1 ";
            Criteria += " and RD_TYPE='BED_MASTER' and RD_IS_DELETED = 0 ";
            DS = objRD.IPResourceDatasGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strjson = "[";
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    strjson += DR["RD_DATA"] + ",";

                }
                strjson += "]";

                DT = DerializeDataTable(strjson);

                if (DT.Rows.Count > 0)
                {
                    DataRow[] result = DT.Select("Ward='" + drpWardNo.SelectedValue + "' and Room='" + drpRoomNo.SelectedValue + "'");

                    foreach (DataRow DR in result)
                    {
                        drpBed.Items.Insert(0 + 1, Convert.ToString(DR[0]));
                        drpBed.Items[0 + 1].Value = Convert.ToString(DR[0]);

                    }

                }

            }



        }

        */

        void BindWardNo()
        {

            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " HWM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND HWM_STATUS='A' ";

            DS = objCom.fnGetFieldValue("HWM_ID,HWM_NAME", "HMS_WARD_MASTER", Criteria, "HWM_ID");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpWardNo.DataSource = DS;
                drpWardNo.DataTextField = "HWM_NAME";
                drpWardNo.DataValueField = "HWM_ID";
                drpWardNo.DataBind();


            }
            drpWardNo.Items.Insert(0, "--- Select ---");
            drpWardNo.Items[0].Value = "";

        }

        void BindRoom()
        {
            drpRoomNo.Items.Clear();
            drpBed.Items.Clear();

            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " HRM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND HRM_STATUS='A' ";
            Criteria += " AND HRM_WARD_ID='" + drpWardNo.SelectedValue + "'";


            DS = objCom.fnGetFieldValue("HRM_ID,HRM_NAME", "HMS_ROOM_MASTER", Criteria, "HRM_ID");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpRoomNo.DataSource = DS;
                drpRoomNo.DataTextField = "HRM_NAME";
                drpRoomNo.DataValueField = "HRM_ID";
                drpRoomNo.DataBind();


            }
            drpRoomNo.Items.Insert(0, "--- Select ---");
            drpRoomNo.Items[0].Value = "";



        }


        void BindBed()
        {
            drpBed.Items.Clear();
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " HBD_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND HBD_STATUS='A' ";
            Criteria += " AND HBD_WARD_ID='" + drpWardNo.SelectedValue + "' and  HBD_ROOM_ID='" + drpRoomNo.SelectedValue + "'";


            DS = objCom.fnGetFieldValue("HBD_ID", "HMS_BED_MASTER", Criteria, "HBD_ID");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpBed.DataSource = DS;
                drpBed.DataTextField = "HBD_ID";
                drpBed.DataValueField = "HBD_ID";
                drpBed.DataBind();


            }
            drpBed.Items.Insert(0, "--- Select ---");
            drpBed.Items[0].Value = "";



        }

        void BindBedTrasnfer()
        {
            string strjson;
            DataSet DS = new DataSet();
            DataSet DS1 = new DataSet();
            DataTable DT = new DataTable();

            IP_ResourceDatas objRD = new IP_ResourceDatas();
            string Criteria = " 1=1 AND RD_IS_DELETED=0 ";
            Criteria += " and RD_TYPE='BED_TRANSFER' and  RD_TYPE_ID='" + Convert.ToString(Session["IAS_ADMISSION_NO"]) + "'";
            DS = objRD.IPResourceDatasGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                strjson = "[";
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    string Data = "";
                    Data = Convert.ToString(DR["RD_DATA"]);
                    Data =Data.Replace("{\"","{\"RD_ID\":\""+Convert.ToString(DR["RD_ID"]) +"\"" +",\"");
                    strjson += Data + ",";

                }
                strjson += "]";

                DT = DerializeDataTable(strjson);

                DataColumn RD_ID = new DataColumn();
                RD_ID.ColumnName = "RD_ID";

                gvBedTrans.Visible = false;
                if (DT.Rows.Count > 0)
                {
                    gvBedTrans.Visible = true;
                    gvBedTrans.DataSource = DT;
                    gvBedTrans.DataBind();

                }
                else
                {
                    gvBedTrans.DataBind();
                }

            }
        }

        void Clear()
        {
            if (drpWardNo.Items.Count > 0)
                drpWardNo.SelectedIndex = 0;

            if (drpRoomNo.Items.Count > 0)
                drpRoomNo.SelectedIndex = 0;

            if (drpBed.Items.Count > 0)
                drpBed.SelectedIndex = 0;

            txtFee.Text = "";
            txtDoctorID.Text = "";
            txtDoctorName.Text = "";

            if (drpTransHour.Items.Count > 0)
                drpTransHour.SelectedIndex = 0;


            if (drpTransMin.Items.Count > 0)
                drpTransMin.SelectedIndex = 0;

            txtDate.Text = "";

            if (drpStatus.Items.Count > 0)
                drpStatus.SelectedIndex = 0;
            txtAdmittedfor.Text = "";
            txtRemarks.Text = "";

            ViewState["RD_ID"] = "0";
            //if (Convert.ToString(ViewState["SelectIndex"]) != "")
            //{
            //    if (gvBedTrans.Rows.Count > 0)
            //        gvBedTrans.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
            //}
            //ViewState["SelectIndex"] = "";
        }


        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_BED_TRANSFER' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Visible = false;
                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSave.Visible = false;
                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }


                try
                {
                    BindTime();

                    ViewState["RD_ID"] = "0";
                    ViewState["SelectIndex"] = "";
                    CommonBAL objCom = new CommonBAL();
                    string strDate = "", strTime = ""; ;
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("hh:mm:ss");

                    txtDate.Text = strDate;
                 

                    BindWardNo();

                    BindBedTrasnfer();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      BedTransfer.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            //{"WardNo":"123","RoomNo":"45","BedNo":"34","Admittedfor":"dasd","RecomDrID":"DR.HUSSAMELDEEN","RecomDrName":"DR.HUSSAMELDEEN",
            //"Status":"Active","Date":"4/12/2013","Time":"11:19","Fee":"452","Remarks":"asdasddsfgdsfgdsfgdsfg"}

            try
            {

                IDictionary<string, string> Param = new Dictionary<string, string>();
                Param.Add("WardNo", drpWardNo.SelectedValue);
                Param.Add("RoomNo", drpRoomNo.SelectedValue);
                Param.Add("BedNo", drpBed.SelectedValue);
                Param.Add("Admittedfor", txtAdmittedfor.Text.Trim());
                Param.Add("RecomDrID", txtDoctorID.Text.Trim());
                Param.Add("RecomDrName", txtDoctorName.Text.Trim());
                Param.Add("Status", drpStatus.SelectedValue);

                Param.Add("Date", txtDate.Text.Trim());
                Param.Add("Time", txtDate.Text + " " + drpTransHour.SelectedValue + ":" + drpTransMin.SelectedValue + ":00");
                Param.Add("Fee", txtFee.Text.Trim());
                Param.Add("Remarks", txtRemarks.Text.Trim());



                IP_ResourceDatas objRD = new IP_ResourceDatas();
                objRD.RD_ID = Convert.ToString(ViewState["RD_ID"]);
                objRD.RD_TYPE = "BED_TRANSFER";
                objRD.RD_TYPE_ID = Convert.ToString(Session["IAS_ADMISSION_NO"]);
                objRD.RD_DATA = CommonBAL.ConvertToJson(Param);
                objRD.RD_IS_DELETED = "0";
                objRD.UserID = Convert.ToString(Session["User_ID"]);
                objRD.IPResourceDatasAdd();


                BindBedTrasnfer();
                Clear();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      BedTransfer.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpWardNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindRoom();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      BedTransfer.drpWardNo_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpRoomNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindBed();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      BedTransfer.drpRoomNo_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void Edit_Click(object sender, EventArgs e)
        {

            try
            {

                //if (Convert.ToString(ViewState["SelectIndex"]) != "")
                //{
                //    gvBedTrans.Rows[Convert.ToInt32(ViewState["SelectIndex"])].BackColor = System.Drawing.Color.White;
                //}

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
               // ViewState["SelectIndex"] = gvScanCard.RowIndex;
              //  gvBedTrans.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblRdId = (Label)gvScanCard.Cells[0].FindControl("lblRdId");
                Label lblRdTypeID = (Label)gvScanCard.Cells[0].FindControl("lblRdTypeID");
                Label lblWardNo = (Label)gvScanCard.Cells[0].FindControl("lblWardNo");
                Label lblRoomNo = (Label)gvScanCard.Cells[0].FindControl("lblRoomNo");
                Label lblBedNo = (Label)gvScanCard.Cells[0].FindControl("lblBedNo");
                Label lblFee = (Label)gvScanCard.Cells[0].FindControl("lblFee");
                Label lblRecomDrID = (Label)gvScanCard.Cells[0].FindControl("lblRecomDrID");
                Label lblRecomDrName = (Label)gvScanCard.Cells[0].FindControl("lblRecomDrName");
                Label lblStatus = (Label)gvScanCard.Cells[0].FindControl("lblStatus");
                Label lblDate = (Label)gvScanCard.Cells[0].FindControl("lblDate");
                Label lblTime = (Label)gvScanCard.Cells[0].FindControl("lblTime");
                Label lblAdmittedfor = (Label)gvScanCard.Cells[0].FindControl("lblAdmittedfor");
                Label lblRemarks = (Label)gvScanCard.Cells[0].FindControl("lblRemarks");



                ViewState["RD_ID"] = lblRdId.Text;

                drpWardNo.SelectedValue = lblWardNo.Text;
                drpWardNo_SelectedIndexChanged(drpWardNo, new EventArgs());

                drpRoomNo.SelectedValue = lblRoomNo.Text;
                drpRoomNo_SelectedIndexChanged(drpRoomNo, new EventArgs());

                drpBed.SelectedValue = lblBedNo.Text;

                txtFee.Text = lblFee.Text;
                txtDoctorID.Text = lblRecomDrID.Text;
                txtDoctorName.Text = lblRecomDrName.Text;

                drpStatus.Text = lblStatus.Text;
                txtDate.Text = lblDate.Text;



                string strTime = lblTime.Text;

                string[] arrTransTime = strTime.Split(':');
                if (arrTransTime.Length > 1)
                {
                    drpTransHour.SelectedValue = arrTransTime[0];
                    drpTransMin.SelectedValue = arrTransTime[1];

                }


                txtAdmittedfor.Text = lblAdmittedfor.Text;
                txtRemarks.Text = lblRemarks.Text;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      BedTransfer.Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeletegvBedTrans_Click(object sender, EventArgs e)
        {

            try
            {


                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
               

                Label lblRdId = (Label)gvScanCard.Cells[0].FindControl("lblRdId");
                Label lblRdTypeID = (Label)gvScanCard.Cells[0].FindControl("lblRdTypeID");

                IP_ResourceDatas objRD = new IP_ResourceDatas();
                objRD.RD_ID = lblRdId.Text;
                objRD.RD_TYPE = "BED_TRANSFER";
                objRD.RD_TYPE_ID = Convert.ToString(Session["IAS_ADMISSION_NO"]);
                objRD.RD_DATA = "";
                objRD.RD_IS_DELETED = "1";
                objRD.UserID = Convert.ToString(Session["User_ID"]);
                objRD.IPResourceDatasAdd();


                BindBedTrasnfer();
                Clear();
 

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      BedTransfer.Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

    }
}