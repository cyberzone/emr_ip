﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;
namespace EMR_IP.Patient
{

    public partial class CongenitaHeartDiseaseScreening : System.Web.UI.Page
    {

        IP_CongenitaHeartDiseaseScreening objCong = new IP_CongenitaHeartDiseaseScreening();
        DataSet DS = new DataSet();


        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("CongenitaHeartDiseaseScreening." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpIniHour.DataSource = DS;
                drpIniHour.DataTextField = "Name";
                drpIniHour.DataValueField = "Code";
                drpIniHour.DataBind();

                drpSecHour.DataSource = DS;
                drpSecHour.DataTextField = "Name";
                drpSecHour.DataValueField = "Code";
                drpSecHour.DataBind();

                drpThiHour.DataSource = DS;
                drpThiHour.DataTextField = "Name";
                drpThiHour.DataValueField = "Code";
                drpThiHour.DataBind();

            }

            drpIniHour.Items.Insert(0, "00");
            drpIniHour.Items[0].Value = "00";

            drpSecHour.Items.Insert(0, "00");
            drpSecHour.Items[0].Value = "00";


            drpThiHour.Items.Insert(0, "00");
            drpThiHour.Items[0].Value = "00";

            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {

                drpIniMin.DataSource = DS;
                drpIniMin.DataTextField = "Name";
                drpIniMin.DataValueField = "Code";
                drpIniMin.DataBind();

                drpSecMin.DataSource = DS;
                drpSecMin.DataTextField = "Name";
                drpSecMin.DataValueField = "Code";
                drpSecMin.DataBind();


                drpThiMin.DataSource = DS;
                drpThiMin.DataTextField = "Name";
                drpThiMin.DataValueField = "Code";
                drpThiMin.DataBind();

            }

            drpIniMin.Items.Insert(0, "00");
            drpIniMin.Items[0].Value = "00";


            drpSecMin.Items.Insert(0, "00");
            drpSecMin.Items[0].Value = "00";

            drpThiMin.Items.Insert(0, "00");
            drpThiMin.Items[0].Value = "00";


        }

        void BindData()
        {
            objCong = new IP_CongenitaHeartDiseaseScreening();
            DS = new DataSet();

            string Criteria = " 1=1 and ICHDS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND ICHDS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objCong.CongHearDiseaseScreeGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    txtAge.Text = Convert.ToString(DR["ICHDS_AGE"]);
                    txtIniDate.Text = Convert.ToString(DR["ICHDS_INI_TIME_DateDesc"]);

                    string strIniHour = Convert.ToString(DR["ICHDS_INI_TIMEDesc"]);
                    string[] arrIniHour = strIniHour.Split(':');
                    if (arrIniHour.Length > 1)
                    {
                        drpIniHour.SelectedValue = arrIniHour[0];
                        drpIniMin.SelectedValue = arrIniHour[1];

                    }


                    txtINI_PULSE_OX_SATU_RHAND.Text = Convert.ToString(DR["ICHDS_INI_PULSE_OX_SATU_RHAND"]);
                    txtINI_PULSE_OX_SATU_FOOT.Text = Convert.ToString(DR["ICHDS_INI_PULSE_OX_SATU_FOOT"]);
                    txtINI_DIFF_RHAND_FOOT.Text = Convert.ToString(DR["ICHDS_INI_DIFF_RHAND_FOOT"]);


                    string strIniResult = Convert.ToString(DR["ICHDS_INI_DIFF_RESULT"]);
                    if (radIniPass.Value == strIniResult)
                    {
                        radIniPass.Checked = true;
                    }
                    if (radIniFail.Value == strIniResult)
                    {
                        radIniFail.Checked = true;
                    }



                    txtSecDate.Text = Convert.ToString(DR["ICHDS_SEC_TIME_DateDesc"]);

                    string strSecHour = Convert.ToString(DR["ICHDS_SEC_TIMEDesc"]);
                    string[] arrSecHour = strSecHour.Split(':');
                    if (arrSecHour.Length > 1)
                    {
                        drpSecHour.SelectedValue = arrSecHour[0];
                        drpSecMin.SelectedValue = arrSecHour[1];

                    }


                    txtSEC_PULSE_OX_SATU_RHAND.Text = Convert.ToString(DR["ICHDS_SEC_PULSE_OX_SATU_RHAND"]);
                    txtSEC_PULSE_OX_SATU_FOOT.Text = Convert.ToString(DR["ICHDS_SEC_PULSE_OX_SATU_FOOT"]);
                    txtSEC_DIFF_RHAND_FOOT.Text = Convert.ToString(DR["ICHDS_SEC_DIFF_RHAND_FOOT"]);

                    string strSecResult = Convert.ToString(DR["ICHDS_SEC_DIFF_RESULT"]);
                    if (radSecPass.Value == strSecResult)
                    {
                        radSecPass.Checked = true;
                    }
                    if (radSecFail.Value == strSecResult)
                    {
                        radSecFail.Checked = true;
                    }


                    txtThiDate.Text = Convert.ToString(DR["ICHDS_THI_TIME_DateDesc"]);

                    string strThiHour = Convert.ToString(DR["ICHDS_THI_TIMEDesc"]);
                    string[] arrThiHour = strThiHour.Split(':');
                    if (arrThiHour.Length > 1)
                    {
                        drpThiHour.SelectedValue = arrThiHour[0];
                        drpThiMin.SelectedValue = arrThiHour[1];

                    }



                    txtTHI_PULSE_OX_SATU_RHAND.Text = Convert.ToString(DR["ICHDS_THI_PULSE_OX_SATU_RHAND"]);
                    txtTHI_PULSE_OX_SATU_FOOT.Text = Convert.ToString(DR["ICHDS_THI_PULSE_OX_SATU_FOOT"]);
                    txtTHI_DIFF_RHAND_FOOT.Text = Convert.ToString(DR["ICHDS_THI_DIFF_RHAND_FOOT"]);


                    string strThiResult = Convert.ToString(DR["ICHDS_THI_DIFF_RESULT"]);
                    if (radThiPass.Value == strThiResult)
                    {
                        radThiPass.Checked = true;
                    }
                    if (radThiFail.Value == strThiResult)
                    {
                        radThiFail.Checked = true;
                    }

                }
            }


        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_CCHD' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Visible = false;
                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSave.Visible = false;
                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                try
                {
                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                    {
                        SetPermission();
                    }

                    this.Page.Title = "CCHD";
                    CommonBAL objCom = new CommonBAL();
                    string strDate = "", strTime = ""; ;
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("hh:mm:ss");

                    txtIniDate.Text = strDate;
                    txtSecDate.Text = strDate;
                    txtThiDate.Text = strDate;

                    BindTime();
                    BindData();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                objCong = new IP_CongenitaHeartDiseaseScreening();
                objCong.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCong.EMRID = Convert.ToString(Session["EMR_ID"]);
                objCong.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                objCong.ICHDS_AGE = txtAge.Text;
                objCong.ICHDS_INI_TIME = txtIniDate.Text.Trim() + " " + drpIniHour.SelectedValue + ":" + drpIniMin.SelectedValue + ":00";
                objCong.ICHDS_INI_PULSE_OX_SATU_RHAND = txtINI_PULSE_OX_SATU_RHAND.Text;
                objCong.ICHDS_INI_PULSE_OX_SATU_FOOT = txtINI_PULSE_OX_SATU_FOOT.Text;
                objCong.ICHDS_INI_DIFF_RHAND_FOOT = txtINI_DIFF_RHAND_FOOT.Text;

                if (radIniPass.Checked == true)
                {
                    objCong.ICHDS_INI_DIFF_RESULT = radIniPass.Value;
                }
                if (radIniFail.Checked == true)
                {
                    objCong.ICHDS_INI_DIFF_RESULT = radIniFail.Value;
                }




                objCong.ICHDS_SEC_TIME = txtSecDate.Text.Trim() + " " + drpSecHour.SelectedValue + ":" + drpSecMin.SelectedValue + ":00";
                objCong.ICHDS_SEC_PULSE_OX_SATU_RHAND = txtSEC_PULSE_OX_SATU_RHAND.Text;
                objCong.ICHDS_SEC_PULSE_OX_SATU_FOOT = txtSEC_PULSE_OX_SATU_FOOT.Text;
                objCong.ICHDS_SEC_DIFF_RHAND_FOOT = txtSEC_DIFF_RHAND_FOOT.Text;


                if (radSecPass.Checked == true)
                {
                    objCong.ICHDS_SEC_DIFF_RESULT = radSecPass.Value;
                }
                if (radIniFail.Checked == true)
                {
                    objCong.ICHDS_SEC_DIFF_RESULT = radSecFail.Value;
                }

                objCong.ICHDS_THI_TIME = txtThiDate.Text.Trim() + " " + drpThiHour.SelectedValue + ":" + drpThiMin.SelectedValue + ":00";
                objCong.ICHDS_THI_PULSE_OX_SATU_RHAND = txtTHI_PULSE_OX_SATU_RHAND.Text;
                objCong.ICHDS_THI_PULSE_OX_SATU_FOOT = txtTHI_PULSE_OX_SATU_FOOT.Text;
                objCong.ICHDS_THI_DIFF_RHAND_FOOT = txtTHI_DIFF_RHAND_FOOT.Text;

                if (radThiPass.Checked == true)
                {
                    objCong.ICHDS_THI_DIFF_RESULT = radThiPass.Value;
                }
                if (radThiFail.Checked == true)
                {
                    objCong.ICHDS_THI_DIFF_RESULT = radThiFail.Value;
                }

                objCong.UserID = Convert.ToString(Session["User_ID"]);
                objCong.CongHearDiseaseScreeAdd();

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}