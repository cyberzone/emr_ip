﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;
namespace EMR_IP.Patient
{
    public partial class DischargeSummary : System.Web.UI.Page
    {
        static string strSessionDeptId;

        IP_DischargeSummary objDis = new IP_DischargeSummary();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpFrmHour.DataSource = DS;
                drpFrmHour.DataTextField = "Name";
                drpFrmHour.DataValueField = "Code";
                drpFrmHour.DataBind();
            }

            drpFrmHour.Items.Insert(0, "00");
            drpFrmHour.Items[0].Value = "00";


            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpFrmlMin.DataSource = DS;
                drpFrmlMin.DataTextField = "Name";
                drpFrmlMin.DataValueField = "Code";
                drpFrmlMin.DataBind();
            }

            drpFrmlMin.Items.Insert(0, "00");
            drpFrmlMin.Items[0].Value = "00";

        }

        void BindData()
        {
            objDis = new IP_DischargeSummary();
            string Criteria = " 1=1 and IDS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IDS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DataSet DS = new DataSet();

            DS = objDis.DischargeSummaryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    txtDisDate.Text = Convert.ToString(DR["IDS_DATEEDesc"]);

                    string strArrivalDate = Convert.ToString(DR["IDS_DATE_TIMEDesc"]);
                    string[] arrArrivalDater = strArrivalDate.Split(':');
                    if (arrArrivalDater.Length > 1)
                    {
                        drpFrmHour.SelectedValue = arrArrivalDater[0];
                        drpFrmlMin.SelectedValue = arrArrivalDater[1];

                    }



                    txtCondition.Text = Convert.ToString(DR["IDS_CONDITION"]);
                    txtInstructions.Text = Convert.ToString(DR["IDS_INSTRUCTIONS"]);
                    txtPhyActivity.Text = Convert.ToString(DR["IDS_PHY_ACTIVITY"]);
                    txtDiet.Text = Convert.ToString(DR["IDS_DIET"]);
                    txtFollowUpCare.Text = Convert.ToString(DR["IDS_FOLLOWUPCARE"]);
                    txtMedications.Text = Convert.ToString(DR["IDS_MEDICATIONS"]);

                    radInstrMedication.SelectedValue = Convert.ToString(DR["IDS_INSTR_MEDICATION"]);
                    radInfoMedicFood.SelectedValue = Convert.ToString(DR["IDS_INFO_MEDIC_FOOD"]);

                    if (radInstrMedication.SelectedValue == "True")
                    {
                        txtInstrMedication.Visible = true;
                    }

                    if (radInfoMedicFood.SelectedValue == "True")
                    {
                        txtInfoMedicFood.Visible = true;
                    }
                    txtInstrMedication.Text = Convert.ToString(DR["IDS_INSTR_MEDICATION_REMARKS"]);
                    txtInfoMedicFood.Text = Convert.ToString(DR["IDS_INFO_MEDIC_FOOD_REMARKS"]);

                    txtTransType.Text = Convert.ToString(DR["IDS_TRANSPOR_TYPE"]);

                }

            }

        }

        Boolean BindAdmissionSummary()
        {
            DataSet DS = new DataSet();
            IP_AdmissionSummary obj = new IP_AdmissionSummary();
            string Criteria = " 1=1 ";
            Criteria += " and IAS_ADMISSION_NO='" + Convert.ToString(Session["IAS_ADMISSION_NO"]) + "'";

            DS = obj.GetIPAdmissionSummary(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                // txtInpatientNo.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_NO"]);

                //  AdmissionDateTime.Value = Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateTime"]);
                //  ProvisionalDiagnosis.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PROVISIONAL_DIAGNOSIS"]);
                // AttendingPhysician.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ATTENDING_PHYSICIAN"]);
                //Laboratory.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_LABORATORY"]);
                // Radiological.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_RADIOLOGICAL"]);
                // OtherAdmissionSummary.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_OTHERS"]);
                // ProcedurePlanned.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PROCEDURE_PLANNED"]);
                // Anesthesia.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ANESTHESIA"]);
                //  Treatment.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_TREATMENT"]);
                //ReferralPhysician.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REFERRAL_PHYSICIAN"]);

                // radAdmissionType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_TYPE"]);
                ReasonforAdmission.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REASON_FOR_ADMISSION"]);
                // Remarks.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REMARKS"]);
                return true;
            }
            else
            {

                return false;
            }
        }

        void BindDiagnosis()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND IPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";


            DataSet DS = new DataSet();
            IP_PTDiagnosis objDiag = new IP_PTDiagnosis();
            DS = objDiag.IPDiagnosisGet(Criteria);

            //if (DS.Tables[0].Rows.Count > 0)
            //{
            //    foreach (DataRow DR in DS.Tables[0].Rows)
            //    {

            //        if (Convert.ToString(DR["IPD_TYPE"]).ToUpper() == "Principal".ToUpper())
            //        {
            //            txtPrincDiag.Text = Convert.ToString(DR["IPD_DIAG_CODE"]).Trim() + " ~ " + Convert.ToString(DR["IPD_DIAG_NAME"]).Trim();
            //        }
            //        else
            //        {
            //            if (txtAdditDiag.Text == "")
            //            {
            //                txtAdditDiag.Text = Convert.ToString(DR["IPD_DIAG_CODE"]).Trim() + " ~ " + Convert.ToString(DR["IPD_DIAG_NAME"]).Trim();
            //            }
            //            else
            //            {
            //                txtAdditDiag.Text += "\n" + Convert.ToString(DR["IPD_DIAG_CODE"]).Trim() + "~" + Convert.ToString(DR["IPD_DIAG_NAME"]).Trim();
            //            }

            //        }

            //    }


            //}

        }

        void BindProcedure()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND IPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";



            DataSet DS = new DataSet();
            IP_PTProcedure objPro = new IP_PTProcedure();
            DS = objPro.IPProceduresGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {


                    if (txtProcedute.Value == "")
                    {
                        txtProcedute.Value = Convert.ToString(DR["IPP_PRO_CODE"]).Trim() + " ~ " + Convert.ToString(DR["IPP_PRO_NAME"]).Trim();
                    }
                    else
                    {
                        txtProcedute.Value += "\n" + Convert.ToString(DR["IPP_PRO_CODE"]).Trim() + " ~ " + Convert.ToString(DR["IPP_PRO_NAME"]).Trim();
                    }



                }
            }


        }

        void BindMedication()
        {
            string strMedication = "";
            DataSet DS = new DataSet();
            IP_PTPharmacy objPro = new IP_PTPharmacy();

            string Criteria = " 1=1 ";
            Criteria += " AND IPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objPro.IPPharmacyGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    strMedication += Convert.ToString(DR["IPP_PHY_CODE"]) + " - " + Convert.ToString(DR["IPP_PHY_NAME"]);
                    strMedication += "\n";

                }
            }


            DS = new DataSet();

            IP_PTIVFluid objIV = new IP_PTIVFluid();
            Criteria = " 1=1 ";
            Criteria += " AND IPI_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPI_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objIV.IPPTIVFluidGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    strMedication += Convert.ToString(DR["IPI_IV_CODE"]) + " - " + Convert.ToString(DR["IPI_IV_NAME"]);
                    strMedication += "\n";
                }
            }
            txtTreatmentMedic.InnerText = strMedication;
        }



        Boolean CheckPrincipalDiagnosis()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND IPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND IPD_TYPE='Principal'";
            Criteria += " AND IPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DataSet DS = new DataSet();
            IP_PTDiagnosis objDiag = new IP_PTDiagnosis();
            DS = objDiag.IPDiscDiagnosisGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }


        void BindDiagnosisGrid()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND IPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            IP_PTDiagnosis objDiag = new IP_PTDiagnosis();
            DS = objDiag.IPDiscDiagnosisGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvDiagnosis.DataSource = DS;
                gvDiagnosis.DataBind();

                string strPermission = hidPermission.Value;
                if (strPermission == "1" || strPermission == "7")
                {
                    gvDiagnosis.Columns[0].Visible = false;
                }

            }
            else
            {
                gvDiagnosis.DataBind();
            }



        FunEnd: ;
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_DISCHARGE_SUMM' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Visible = false;
                btnAddDiag.Visible = false;

                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSave.Visible = false;
                btnAddDiag.Visible = false;
                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }

        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetDiagnosis(string prefixText)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string[] Data;

            ds = dbo.HaadServicessListGet("Diagnosis", prefixText, "", strSessionDeptId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["Code"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["Description"]).Trim();

                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            try
            {
                if (!IsPostBack)
                {

                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                    {
                        SetPermission();
                    }

                    strSessionDeptId = Convert.ToString(Session["User_DeptID"]);

                    this.Page.Title = "DischargeSummary";
                    CommonBAL objCom = new CommonBAL();
                    string strDate = "", strTime = ""; ;
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("hh:mm:ss");

                    txtDisDate.Text = strDate;
                    BindTime();
                    BindData();

                    BindAdmissionSummary();
                    BindDiagnosis();
                    BindProcedure();
                    BindMedication();

                    BindDiagnosisGrid();
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtCondition.Text == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Enter Condition at Discharge..')", true);
                    goto FunEnd;
                }

                if (txtInstructions.Text == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Enter Discharge Instructions')", true);
                    goto FunEnd;
                }

                if (txtPhyActivity.Text == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Enter Physical Activity')", true);
                    goto FunEnd;
                }

                if (txtDiet.Text == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Enter Diet')", true);
                    goto FunEnd;
                }


                if (txtFollowUpCare.Text == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Enter Follow Up Care')", true);
                    goto FunEnd;
                }

                if (txtMedications.Text == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Enter Discharge Medications')", true);
                    goto FunEnd;
                }


                if (txtTransType.Text == "")
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Enter Type of Transportation')", true);
                    goto FunEnd;
                }

                objDis = new IP_DischargeSummary();


                objDis.BranchID = Convert.ToString(Session["Branch_ID"]);
                objDis.EMRID = Convert.ToString(Session["EMR_ID"]);
                objDis.PTID = Convert.ToString(Session["EMR_PT_ID"]);


                objDis.IDS_PRINC_DIAG = "";// txtPrincDiag.Text.Trim();
                objDis.IDS_ADD_DIAG = "";// txtAdditDiag.Text.Trim();
                objDis.IDS_ADMIN_REASON = ReasonforAdmission.Value;
                objDis.IDS_SIGNIFICANT_FINDINGS = txtSignificantFindings.Value;
                objDis.IDS_PROCEDURES = txtProcedute.Value;
                objDis.IDS_TREAT_MEDIC = txtTreatmentMedic.Value;


                objDis.IDS_DATE = txtDisDate.Text + " " + drpFrmHour.SelectedValue + ":" + drpFrmlMin.SelectedValue + ":00";
                objDis.IDS_CONDITION = txtCondition.Text;
                objDis.IDS_INSTRUCTIONS = txtInstructions.Text;
                objDis.IDS_PHY_ACTIVITY = txtPhyActivity.Text;
                objDis.IDS_DIET = txtDiet.Text;
                objDis.IDS_FOLLOWUPCARE = txtFollowUpCare.Text;
                objDis.IDS_MEDICATIONS = txtMedications.Text;


                objDis.IDS_INSTR_MEDICATION = radInstrMedication.SelectedValue;
                objDis.IDS_INFO_MEDIC_FOOD = radInfoMedicFood.SelectedValue;

                objDis.IDS_INSTR_MEDICATION_REMARKS = txtInstrMedication.Text;
                objDis.IDS_INFO_MEDIC_FOOD_REMARKS = txtInfoMedicFood.Text;

                objDis.IDS_TRANSPOR_TYPE = txtTransType.Text;
                objDis.AdmissionNO = Convert.ToString(Session["IAS_ADMISSION_NO"]);
                objDis.UserID = Convert.ToString(Session["User_ID"]);
                objDis.DischargeSummaryAdd();


                objDis = new IP_DischargeSummary();


                objDis.BranchID = Convert.ToString(Session["Branch_ID"]);
                objDis.EMRID = Convert.ToString(Session["EMR_ID"]);
                objDis.PTDiagnosisAddFromDisDiag();


                CommonBAL objCom = new CommonBAL();
                string Criteria = " 1=1 ";
                Criteria += " AND IAS_ADMISSION_NO='" + Convert.ToString(Session["IAS_ADMISSION_NO"]) + "' AND IAS_BRANCHID='" + Convert.ToString(Session["Branch_ID"]) + "'";

                string FieldNameWithValues = " IAS_STATUS='D' , IAS_DISCHARGE_DATE= CONVERT(DATETIME,'" + txtDisDate.Text + " " + drpFrmHour.SelectedValue + ":" + drpFrmlMin.SelectedValue + ":00',103) ";
                objCom.fnUpdateTableData(FieldNameWithValues, "IP_ADMISSION_SUMMARY", Criteria);
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void radInstrMedication_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radInstrMedication.SelectedValue == "True")
            {

                txtInstrMedication.Visible = true;
            }
            else
            {
                txtInstrMedication.Visible = false;
            }

        }

        protected void radInfoMedicFood_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (radInfoMedicFood.SelectedValue == "True")
            {

                txtInfoMedicFood.Visible = true;
            }
            else
            {
                txtInfoMedicFood.Visible = false;
            }
        }

        protected void btnAddDiag_Click(object sender, EventArgs e)
        {
            try
            {
                if (gvDiagnosis.Rows.Count > 0 && drpDiagType.SelectedValue == "Principal")
                {
                    if (CheckPrincipalDiagnosis() == true)
                    {
                        lblMessage.Text = "Principal diagnosis already added";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }
                }


                Int32 intPos = 1;
                if (gvDiagnosis.Rows.Count >= 1)
                {
                    intPos = Convert.ToInt32(gvDiagnosis.Rows.Count) + 1;

                }

                string strDiagnosis = txtDiagnosis.Text.Trim();
                string[] arrDiagnosis = strDiagnosis.Split('~');


                string strDiagCode = "";
                string strDiagDesc = "";

                if (arrDiagnosis.Length > 0)
                {
                    strDiagCode = arrDiagnosis[0];

                    if (arrDiagnosis.Length > 1)
                    {
                        strDiagDesc = arrDiagnosis[1];

                    }
                }


                IP_PTDiagnosis objDiag = new IP_PTDiagnosis();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.IPD_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.IPD_DIAG_ID = "";
                objDiag.IPD_DIAG_CODE = strDiagCode;
                objDiag.IPD_DIAG_NAME = strDiagDesc;

                objDiag.IPD_TYPE = drpDiagType.SelectedValue;
                objDiag.IPD_POSITION = Convert.ToString(intPos);
                objDiag.IPDiscDiagnosisAdd();


                BindDiagnosisGrid();

                drpDiagType.SelectedValue = "Secondary";
                txtDiagnosis.Text = "";

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Add_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void DeleteeDiag_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblDiagID = (Label)gvScanCard.Cells[0].FindControl("lblDiagID");


                IP_PTDiagnosis objDiag = new IP_PTDiagnosis();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.IPD_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.IPD_DIAG_ID = lblDiagID.Text;

                objDiag.IPDiscDiagnosisDelete();


                BindDiagnosisGrid();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void gvDiagnosis_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblDiagType = (Label)e.Row.FindControl("lblDiagType");
                    Button btnColor = (Button)e.Row.FindControl("btnColor");

                    if (lblDiagType.Text == "Principal")
                    {
                        btnColor.BackColor = System.Drawing.ColorTranslator.FromHtml("#02a0e0");

                    }
                    else if (lblDiagType.Text == "Secondary")
                    {
                        btnColor.BackColor = System.Drawing.ColorTranslator.FromHtml("#9ACD32");
                    }
                    else if (lblDiagType.Text == "Admitting")
                    {
                        btnColor.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF00");
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      gvDiagnosis_RowDataBound");
                TextFileWriting(ex.Message.ToString());
            }
        }


    }
}