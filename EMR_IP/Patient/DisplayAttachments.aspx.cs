﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

namespace EMR_IP.Patient
{
    public partial class DisplayAttachments : System.Web.UI.Page
    {
        void BindScanFileFromPhysical()
        {
            string ScanFileName = Convert.ToString(ViewState["FileName"]);
            //   string ScanPath = @"\" + Convert.ToString(ViewState["PT_ID"]) + @"\" + Convert.ToString(ViewState["Parent1"]) + @"\" + Convert.ToString(ViewState["Parent2"]) + @"\";
            string FileExtension = ScanFileName.Substring(ScanFileName.LastIndexOf('.'), ScanFileName.Length - ScanFileName.LastIndexOf('.'));


            string ScanSourcePath = "";

            if (Convert.ToString(ViewState["FileType"]) == "Template")
            {
                ScanSourcePath = Server.MapPath(@"..\Downloads\Template_AnesthesiaChart.xlsx");
            }
            else
            {
                ScanSourcePath = @Convert.ToString(Session["EMR_IP_ANES_CHART_FILE_PATH"]) + ScanFileName;
            }

            
            // if (System.IO.File.Exists(ScanSourcePath) == true)
            // {
            if (FileExtension.ToLower() == ".pdf")
            {
                Response.ContentType = "Application/pdf";
            }
            else if (FileExtension.ToLower() == ".docx" || FileExtension.ToLower() == ".doc")
            {
                Response.ContentType = "application/msword";// "application/ms-word";
            }
            else if (FileExtension.ToLower() == ".xlsx" || FileExtension.ToLower() == ".xls")
            {
                Response.AddHeader("content-disposition", "attachment; filename=" + ScanFileName);
                Response.AddHeader("Content-Type", "application/Excel");
                Response.ContentType = "application/vnd.xls";
            }
            else if (FileExtension.ToLower() == ".jpg")
            {
                Response.ContentType = "image/JPEG";
            }
            else if (FileExtension.ToLower() == ".png")
            {
                Response.ContentType = "image/png";
            }
            else if (FileExtension.ToLower() == ".bmp")
            {
                Response.ContentType = "image/bmp";
            }
            else if (FileExtension.ToLower() == ".gif")
            {
                Response.ContentType = "image/gif";
            }



            Response.WriteFile(ScanSourcePath);
            Response.End();



            //}


        }


        void BindScanFileFromBinary()
        {
            string ScanFileName = Convert.ToString(ViewState["FileName"]);
            //   string ScanPath = @"\" + Convert.ToString(ViewState["PT_ID"]) + @"\" + Convert.ToString(ViewState["Parent1"]) + @"\" + Convert.ToString(ViewState["Parent2"]) + @"\";
            string FileExtension = ScanFileName.Substring(ScanFileName.LastIndexOf('.'), ScanFileName.Length - ScanFileName.LastIndexOf('.'));


            string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            string ScanSourcePath = pathUser + @"\Downloads\" + ScanFileName;



            if (System.IO.File.Exists(ScanSourcePath) == true)
            {
                if (FileExtension.ToLower() == ".pdf")
                {
                    Response.ContentType = "Application/pdf";
                }
                else if (FileExtension.ToLower() == ".docx" || FileExtension.ToLower() == ".doc")
                {
                    Response.ContentType = "application/msword";// "application/ms-word";
                }
                else if (FileExtension.ToLower() == ".xlsx" || FileExtension.ToLower() == ".xls")
                {
                    Response.AddHeader("content-disposition", "attachment; filename=" + ScanFileName);
                    Response.AddHeader("Content-Type", "application/Excel");
                    Response.ContentType = "application/vnd.xls";
                }
                else if (FileExtension.ToLower() == ".jpg")
                {
                    Response.ContentType = "image/JPEG";
                }
                else if (FileExtension.ToLower() == ".png")
                {
                    Response.ContentType = "image/png";
                }
                else if (FileExtension.ToLower() == ".bmp")
                {
                    Response.ContentType = "image/bmp";
                }
                else if (FileExtension.ToLower() == ".gif")
                {
                    Response.ContentType = "image/gif";
                }



                Response.WriteFile(ScanSourcePath);
                Response.End();



            }


        }

        void DisplayBinaryFile()
        {
            string strFileName = System.DateTime.Today.Year.ToString() + System.DateTime.Today.Month.ToString() + System.DateTime.Today.Day.ToString() + System.DateTime.Now.TimeOfDay.Hours + System.DateTime.Now.TimeOfDay.Minutes + System.DateTime.Now.TimeOfDay.Seconds;
            strFileName = "AnesthesiaChart_" + strFileName + ".xlsx";
            string pathUser = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);
            string pathDownload = pathUser + @"\Downloads\" + strFileName;

            Byte[] bytes = Convert.FromBase64String(Convert.ToString(Session["IA_CHART_ATTACHMENT"]));
            File.WriteAllBytes(pathDownload, bytes);
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            ViewState["FileName"] = Convert.ToString(Request.QueryString["FileName"]);
            ViewState["FileType"] = Convert.ToString(Request.QueryString["FileType"]);

            //if (Convert.ToString( ViewState["FileType"]) == "Binary")
            //{
            //    BindScanFileFromBinary();
            //}
            //else
            //{
            BindScanFileFromPhysical();
            // }

        }
    }
}