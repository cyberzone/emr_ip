﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.Patient
{
    public partial class InitialAssessmentForm : System.Web.UI.Page
    {
        IP_InitialAssessmentForm objIniAssFrm = new IP_InitialAssessmentForm();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpArrHour.DataSource = DS;
                drpArrHour.DataTextField = "Name";
                drpArrHour.DataValueField = "Code";
                drpArrHour.DataBind();

                drpVisitHour.DataSource = DS;
                drpVisitHour.DataTextField = "Name";
                drpVisitHour.DataValueField = "Code";
                drpVisitHour.DataBind();
            }

            drpArrHour.Items.Insert(0, "00");
            drpArrHour.Items[0].Value = "00";

            drpVisitHour.Items.Insert(0, "00");
            drpVisitHour.Items[0].Value = "00";


            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpArrMin.DataSource = DS;
                drpArrMin.DataTextField = "Name";
                drpArrMin.DataValueField = "Code";
                drpArrMin.DataBind();

                drpVisiMin.DataSource = DS;
                drpVisiMin.DataTextField = "Name";
                drpVisiMin.DataValueField = "Code";
                drpVisiMin.DataBind();
            }

            drpArrMin.Items.Insert(0, "00");
            drpArrMin.Items[0].Value = "00";

            drpVisiMin.Items.Insert(0, "00");
            drpVisiMin.Items[0].Value = "00";
             

        }

        void BindEncounter()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 and IPE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPE_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            IP_InitialAssessmentForm objPTEnc = new IP_InitialAssessmentForm();
            ds = objPTEnc.PTEncounterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                txtArea.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_AREA"]);
                txtVisitDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_VISIT_DATEDesc"]);
                txtArrivalDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_ARRIVAL_TIMEDesc"]);
                txtProvisionalDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_DIAGNOSIS"]);

                radConditionUponVisit.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["IPE_CONDITION"]);
                txtConditionOther.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_CONDITION_OTH"]);

                string strSTHour = Convert.ToString(ds.Tables[0].Rows[0]["ArrivalTime"]);

                string[] arrSTHour = strSTHour.Split(':');
                if (arrSTHour.Length > 1)
                {
                    drpArrHour.SelectedValue = arrSTHour[0];
                    drpArrMin.SelectedValue = arrSTHour[1];

                }

                string strVisitHour = Convert.ToString(ds.Tables[0].Rows[0]["VisitTime"]);
                string[] arrVisitHour = strVisitHour.Split(':');
                if (arrVisitHour.Length > 1)
                {
                    drpVisitHour.SelectedValue = arrVisitHour[0];
                    drpVisiMin.SelectedValue = arrVisitHour[1];

                }

                radInfoObtainedFrom.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["IPE_INFOBTAINDFROM"]);
                txtInfoObtainedOther.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_INFOBTAINDFROM_OTH"]);

                radExplanationGiven.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["IPE_EXPLI_GIVEN"]);
                txtExplanationGivenOther.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_EXPLI_GIVEN_OTH"]);

                if (ds.Tables[0].Rows[0].IsNull("IPE_ASSESSMENT_DONE_CODE") == false)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[0]["IPE_ASSESSMENT_DONE_CODE"]) != "")
                    {
                        txtAssessmentDoneBy.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_ASSESSMENT_DONE_CODE"]) + " ~ " + Convert.ToString(ds.Tables[0].Rows[0]["IPE_ASSESSMENT_DONE_NAME"]);

                    }
                }

                txtPresentComplaient.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_PRESENT_COMPLAINT"]);
                txtPastMedicalHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_PAST_MEDICAL_HISTORY"]);
                txtSurgicallHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_PAST_SURGICAL_HISTORY"]);
                txtPsychoSocEcoHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_PSY_SOC_ECONOMIC_HISTORY"]);
                txtFamilyHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_FAMILY_HISTORY"]);
                txtPhysicalAssessment.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_PHYSICAL_ASSESSMENT"]);
                txtReviewOfSystem.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_REVIEW_OF_SYSTEMS"]);
                txtInvestigations.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_INVESTIGATIONS"]);
                txtPrincipalDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_PRINCIPAL_DIAGNOSIS"]);
                txtSecondaryDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_SECONDARY_DIAGNOSIS"]);
                txtTreatmentPlan.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_TREATMENT_PLAN"]);
                txtProcedure.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_PROCEDURE"]);
                txtTreatment.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_TREATMENT"]);
                txtFollowUpNotes.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_FOLLOWUPNOTES"]);

            }

        }

        void Clear()
        {

            txtArea.Text = "";
            txtArrivalDate.Text = "";
            drpArrHour.SelectedIndex = 0;
            drpArrMin.SelectedIndex = 0;
            txtVisitDate.Text = "";


            txtProvisionalDiagnosis.Text = "";
            radConditionUponVisit.SelectedIndex = -1;
            txtConditionOther.Text = "";
            radInfoObtainedFrom.SelectedIndex = -1;
            txtInfoObtainedOther.Text = "";

            radExplanationGiven.SelectedIndex = -1;
            txtExplanationGivenOther.Text = "";
            txtAssessmentDoneBy.Text = "";

            txtPresentComplaient.Text = "";
            txtPastMedicalHistory.Text = "";
            txtSurgicallHistory.Text = "";
            txtPsychoSocEcoHistory.Text = "";
            txtFamilyHistory.Text = "";
            txtPhysicalAssessment.Text = "";
            txtReviewOfSystem.Text = "";
            txtInvestigations.Text = "";
            txtPrincipalDiagnosis.Text = "";
            txtSecondaryDiagnosis.Text = "";
            txtTreatmentPlan.Text = "";
            txtProcedure.Text = "";
            txtTreatment.Text = "";
            txtFollowUpNotes.Text = "";

        }

        void SaveEncounter()
        {

            try
            {
                IP_InitialAssessmentForm objPTEnc = new IP_InitialAssessmentForm();
                objPTEnc.BranchID = Convert.ToString(Session["Branch_ID"]);
                objPTEnc.EMRID = Convert.ToString(Session["EMR_ID"]);
                objPTEnc.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                objPTEnc.IPE_AREA = txtArea.Text.Trim();
                if (txtArrivalDate.Text.Trim() != "")
                {
                    objPTEnc.IPE_ARRIVAL_TIME = txtArrivalDate.Text.Trim() + " " + drpArrHour.SelectedValue + ":" + drpArrMin.SelectedValue + ":00";
                }
                else
                {
                    objPTEnc.IPE_ARRIVAL_TIME = "";
                }
                if (txtVisitDate.Text.Trim() != "")
                {
                    objPTEnc.IPE_VISIT_DATE = txtVisitDate.Text.Trim() + " " + drpVisitHour.SelectedValue + ":" + drpVisiMin.SelectedValue + ":00";
                }
                else
                {
                    objPTEnc.IPE_VISIT_DATE = "";
                }
                objPTEnc.IPE_DIAGNOSIS = txtProvisionalDiagnosis.Text.Trim();
                objPTEnc.IPE_CONDITION = Convert.ToString(radConditionUponVisit.SelectedValue);
                objPTEnc.IPE_CONDITION_OTH = txtConditionOther.Text.Trim();
                objPTEnc.IPE_INFOBTAINDFROM = Convert.ToString(radInfoObtainedFrom.SelectedValue);
                objPTEnc.IPE_INFOBTAINDFROM_OTH = txtInfoObtainedOther.Text.Trim();

                objPTEnc.IPE_EXPLI_GIVEN = Convert.ToString(radExplanationGiven.SelectedValue);
                objPTEnc.IPE_EXPLI_GIVEN_OTH = txtExplanationGivenOther.Text.Trim();

                string strAssessmentDoneby = txtAssessmentDoneBy.Text.Trim();
                string[] arrAssessmentDoneby = strAssessmentDoneby.Split('~');

                if (arrAssessmentDoneby.Length > 1)
                {
                    objPTEnc.IPE_ASSESSMENT_DONE_CODE = arrAssessmentDoneby[0].Trim();
                    objPTEnc.IPE_ASSESSMENT_DONE_NAME = arrAssessmentDoneby[1].Trim();
                }
                else
                {
                    objPTEnc.IPE_ASSESSMENT_DONE_CODE = "";
                    objPTEnc.IPE_ASSESSMENT_DONE_NAME = "";
                }

                objPTEnc.IPE_PRESENT_COMPLAINT = txtPresentComplaient.Text.Trim();
                objPTEnc.IPE_PAST_MEDICAL_HISTORY = txtPastMedicalHistory.Text.Trim();
                objPTEnc.IPE_PAST_SURGICAL_HISTORY = txtSurgicallHistory.Text.Trim();
                objPTEnc.IPE_PSY_SOC_ECONOMIC_HISTORY = txtPsychoSocEcoHistory.Text.Trim();
                objPTEnc.IPE_FAMILY_HISTORY = txtFamilyHistory.Text.Trim();
                objPTEnc.IPE_PHYSICAL_ASSESSMENT = txtPhysicalAssessment.Text.Trim();
                objPTEnc.IPE_REVIEW_OF_SYSTEMS = txtReviewOfSystem.Text.Trim();
                objPTEnc.IPE_INVESTIGATIONS = txtInvestigations.Text.Trim();
                objPTEnc.IPE_PRINCIPAL_DIAGNOSIS = txtPrincipalDiagnosis.Text.Trim();
                objPTEnc.IPE_SECONDARY_DIAGNOSIS = txtSecondaryDiagnosis.Text.Trim();
                objPTEnc.IPE_TREATMENT_PLAN = txtTreatmentPlan.Text.Trim();
                objPTEnc.IPE_PROCEDURE = txtProcedure.Text.Trim();
                objPTEnc.IPE_TREATMENT = txtTreatment.Text.Trim();
                objPTEnc.IPE_FOLLOWUPNOTES = txtFollowUpNotes.Text.Trim();
                objPTEnc.UserID = Convert.ToString(Session["User_Code"]);

                objPTEnc.PTEncounterAdd();



                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientEncounter.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }
        }

        void BindAssessment()
        {
            IP_InitialAssessmentForm obj = new IP_InitialAssessmentForm();

            DataSet DS = new DataSet();
            string Criteria = " 1=1 and IPA_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPA_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DS = obj.PTAssessmentGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                txtHC_ASS_Health_PresPastHist.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_PRESPASTHIST"]);
                txtPresPastProblem.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_PRESPAS_PROB"]);
                txtPresPastSurgeries.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_PRESPAS_PREVPROB"]);
                txtAlergiesMedication.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_ALLERGIES_MEDIC"]);
                txtAlergiesFood.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_ALLERGIES_FOOD"]);
                txtAlergiesOther.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_ALLERGIES_OTHER"]);
                txtHC_ASS_Health_MedicHist_Medicine.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_MEDICHIST_MEDICINE"]);
                txtHC_ASS_Health_MedicHist_Sleep.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_MEDICHIST_SLEEP"]);
                txtHC_ASS_Health_MedicHist_PschoyAss.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_MEDICHIST_PSCHOYASS"]);
                radPainAssYes.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_PAIN_EXPRESSES"]);
                //txtHC_ASS_Phy_Gast_BowelSounds.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_GAST_BOWELSOUNDS"]);
                //txtHC_ASS_Phy_Gast_Elimination.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_GAST_ELIMINATION"]);
                //txtHC_ASS_Phy_Gast_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_GAST_GENERAL"]);
                //txtHC_ASS_Phy_Repro_Male.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_REPRO_MALE"]);
                //txtHC_ASS_Phy_Repro_Female.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_REPRO_FEMALE"]);
                //txtHC_ASS_Phy_Repro_Breasts.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_REPRO_BREASTS"]);
                //txtHC_ASS_Phy_Genit_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_GENIT_GENERAL"]);
                //txtHC_ASS_Phy_Skin_Color.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_SKIN_COLOR"]);
                //txtHC_ASS_Phy_Skin_Temperature.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_SKIN_TEMPERATURE"]);
                //txtHC_ASS_Phy_Skin_Lesions.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_SKIN_LESIONS"]);
                //txtHC_ASS_Phy_Neuro_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NEURO_GENERAL"]);
                //txtHC_ASS_Phy_Neuro_Conscio.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NEURO_CONSCIO"]);
                //txtHC_ASS_Phy_Neuro_Oriented.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NEURO_ORIENTED"]);
                //txtHC_ASS_Phy_Neuro_TimeResp.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NEURO_TIMERESP"]);
                //txtHC_ASS_Phy_Cardio_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_CARDIO_GENERAL"]);
                //txtHC_ASS_Phy_Cardio_Pulse.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_CARDIO_PULSE"]);
                //txtHC_ASS_Phy_Cardio_PedalPulse.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_CARDIO_PEDALPULSE"]);
                //txtHC_ASS_Phy_Cardio_Edema.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_CARDIO_EDEMA"]);
                //txtHC_ASS_Phy_Cardio_NailBeds.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_CARDIO_NAILBEDS"]);
                //txtHC_ASS_Phy_Cardio_Capillary.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_CARDIO_CAPILLARY"]);
                //txtHC_ASS_Phy_Ent_Eyes.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_ENT_EYES"]);
                //txtHC_ASS_Phy_Ent_Ears.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_ENT_EARS"]);
                //txtHC_ASS_Phy_Ent_Nose.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_ENT_NOSE"]);
                //txtHC_ASS_Phy_Ent_Throat.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_ENT_THROAT"]);
                //txtHC_ASS_Phy_Resp_Chest.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_RESP_CHEST"]);
                //txtHC_ASS_Phy_Resp_BreathPatt.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_RESP_BREATHPATT"]);
                //txtHC_ASS_Phy_Resp_BreathSound.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_RESP_BREATHSOUND"]);
                //txtHC_ASS_Phy_Resp_BreathCough.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RESP_BREATHCOUGH"]);
                //txtHC_ASS_Phy_Nutri_Diet.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NUTRI_DIET"]);
                //txtHC_ASS_Phy_Nutri_Appetite.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NUTRI_APPETITE"]);
                //txtHC_ASS_Phy_Nutri_NutriSupport.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NUTRI_NUTRISUPPORT"]);
                //txtHC_ASS_Phy_Nutri_FeedingDif.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NUTRI_FEEDINGDIF"]);
                //txtHC_ASS_Phy_Nutri_WeightStatus.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NUTRI_WEIGHTSTATUS"]);
                //txtHC_ASS_Phy_Nutri_Diag.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NUTRI_DIAG"]);
                txtHC_ASS_Risk_SkinRisk_Sensory.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SKINRISK_SENSORY"]);
                txtHC_ASS_Risk_SkinRisk_Moisture.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SKINRISK_MOISTURE"]);
                txtHC_ASS_Risk_SkinRisk_Activity.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SKINRISK_ACTIVITY"]);
                txtHC_ASS_Risk_SkinRisk_Mobility.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SKINRISK_MOBILITY"]);
                txtHC_ASS_Risk_SkinRisk_Nutrition.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SKINRISK_NUTRITION"]);
                txtHC_ASS_Risk_SkinRisk_Friction.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SKINRISK_FRICTION"]);
                txtHC_ASS_Risk_Socio_Living.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SOCIO_LIVING"]);
                txtHC_ASS_Risk_Safety_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SAFETY_GENERAL"]);
                txtHC_ASS_Risk_Fun_SelfCaring.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_FUN_SELFCARING"]);
                txtHC_ASS_Risk_Fun_Musculos.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_FUN_MUSCULOS"]);
                txtHC_ASS_Risk_Fun_Equipment.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_FUN_EQUIPMENT"]);
                txtHC_ASS_Risk_Edu_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_EDU_GENERAL"]);

            }


        }

        void ClearAssessment()
        {

            txtHC_ASS_Health_PresPastHist.Text = "";
            txtPresPastProblem.Text = "";
            txtPresPastSurgeries.Text = "";
            txtAlergiesMedication.Text = "";
            txtAlergiesFood.Text = "";
            txtAlergiesOther.Text = "";
            txtHC_ASS_Health_MedicHist_Medicine.Text = "";
            txtHC_ASS_Health_MedicHist_Sleep.Text = "";
            txtHC_ASS_Health_MedicHist_PschoyAss.Text = "";
            radPainAssYes.SelectedIndex = -1;
            //txtHC_ASS_Phy_Gast_BowelSounds.Text = "";
            //txtHC_ASS_Phy_Gast_Elimination.Text = "";
            //txtHC_ASS_Phy_Gast_General.Text = "";
            //txtHC_ASS_Phy_Repro_Male.Text = "";
            //txtHC_ASS_Phy_Repro_Female.Text = "";
            //txtHC_ASS_Phy_Repro_Breasts.Text = "";
            //txtHC_ASS_Phy_Genit_General.Text = "";
            //txtHC_ASS_Phy_Skin_Color.Text = "";
            //txtHC_ASS_Phy_Skin_Temperature.Text = "";
            //txtHC_ASS_Phy_Skin_Lesions.Text = "";
            //txtHC_ASS_Phy_Neuro_General.Text = "";
            //txtHC_ASS_Phy_Neuro_Conscio.Text = "";
            //txtHC_ASS_Phy_Neuro_Oriented.Text = "";
            //txtHC_ASS_Phy_Neuro_TimeResp.Text = "";
            //txtHC_ASS_Phy_Cardio_General.Text = "";
            //txtHC_ASS_Phy_Cardio_Pulse.Text = "";
            //txtHC_ASS_Phy_Cardio_PedalPulse.Text = "";
            //txtHC_ASS_Phy_Cardio_Edema.Text = "";
            //txtHC_ASS_Phy_Cardio_NailBeds.Text = "";
            //txtHC_ASS_Phy_Cardio_Capillary.Text = "";
            //txtHC_ASS_Phy_Ent_Eyes.Text = "";
            //txtHC_ASS_Phy_Ent_Ears.Text = "";
            //txtHC_ASS_Phy_Ent_Nose.Text = "";
            //txtHC_ASS_Phy_Ent_Throat.Text = "";
            //txtHC_ASS_Phy_Resp_Chest.Text = "";
            //txtHC_ASS_Phy_Resp_BreathPatt.Text = "";
            //txtHC_ASS_Phy_Resp_BreathSound.Text = "";
            //txtHC_ASS_Phy_Resp_BreathCough.Text = "";
            //txtHC_ASS_Phy_Nutri_Diet.Text = "";
            //txtHC_ASS_Phy_Nutri_Appetite.Text = "";
            //txtHC_ASS_Phy_Nutri_FeedingDif.Text = "";
            //txtHC_ASS_Phy_Nutri_WeightStatus.Text = "";
            //txtHC_ASS_Phy_Nutri_Diag.Text = "";
            txtHC_ASS_Risk_SkinRisk_Sensory.Text = "";
            txtHC_ASS_Risk_SkinRisk_Moisture.Text = "";
            txtHC_ASS_Risk_SkinRisk_Activity.Text = "";
            txtHC_ASS_Risk_SkinRisk_Mobility.Text = "";
            txtHC_ASS_Risk_SkinRisk_Nutrition.Text = "";
            txtHC_ASS_Risk_SkinRisk_Friction.Text = "";
            txtHC_ASS_Risk_Socio_Living.Text = "";
            txtHC_ASS_Risk_Safety_General.Text = "";
            txtHC_ASS_Risk_Fun_SelfCaring.Text = "";
            txtHC_ASS_Risk_Fun_Musculos.Text = "";
            txtHC_ASS_Risk_Fun_Equipment.Text = "";
            txtHC_ASS_Risk_Edu_General.Text = "";
        }

        void SaveAssessment()
        {

            IP_InitialAssessmentForm obj = new IP_InitialAssessmentForm();


            obj.BranchID = Convert.ToString(Session["Branch_ID"]);
            obj.EMRID = Convert.ToString(Session["EMR_ID"]);
            obj.PTID = Convert.ToString(Session["EMR_PT_ID"]);

            obj.IPA_HEALTH_PRESPASTHIST = txtHC_ASS_Health_PresPastHist.Text.Trim();
            obj.IPA_HEALTH_PRESPAS_PROB = txtPresPastProblem.Text.Trim();
            obj.IPA_HEALTH_PRESPAS_PREVPROB = txtPresPastSurgeries.Text.Trim();
            obj.IPA_HEALTH_ALLERGIES_MEDIC = txtAlergiesMedication.Text.Trim();
            obj.IPA_HEALTH_ALLERGIES_FOOD = txtAlergiesFood.Text.Trim();
            obj.IPA_HEALTH_ALLERGIES_OTHER = txtAlergiesOther.Text.Trim();
            obj.IPA_HEALTH_MEDICHIST_MEDICINE = txtHC_ASS_Health_MedicHist_Medicine.Text.Trim();
            obj.IPA_HEALTH_MEDICHIST_SLEEP = txtHC_ASS_Health_MedicHist_Sleep.Text.Trim();
            obj.IPA_HEALTH_MEDICHIST_PSCHOYASS = txtHC_ASS_Health_MedicHist_PschoyAss.Text.Trim();
            obj.IPA_HEALTH_PAIN_EXPRESSES = Convert.ToString(radPainAssYes.SelectedValue);
            //obj.IPA_PHY_GAST_BOWELSOUNDS = txtHC_ASS_Phy_Gast_BowelSounds.Text.Trim();
            //obj.IPA_PHY_GAST_ELIMINATION = txtHC_ASS_Phy_Gast_Elimination.Text.Trim();
            //obj.IPA_PHY_GAST_GENERAL = txtHC_ASS_Phy_Gast_General.Text.Trim();
            //obj.IPA_PHY_REPRO_MALE = txtHC_ASS_Phy_Repro_Male.Text.Trim();
            //obj.IPA_PHY_REPRO_FEMALE = txtHC_ASS_Phy_Repro_Female.Text.Trim();
            //obj.IPA_PHY_REPRO_BREASTS = txtHC_ASS_Phy_Repro_Breasts.Text.Trim();
            //obj.IPA_PHY_GENIT_GENERAL = txtHC_ASS_Phy_Genit_General.Text.Trim();
            //obj.IPA_PHY_SKIN_COLOR = txtHC_ASS_Phy_Skin_Color.Text.Trim();
            //obj.IPA_PHY_SKIN_TEMPERATURE = txtHC_ASS_Phy_Skin_Temperature.Text.Trim();
            //obj.IPA_PHY_SKIN_LESIONS = txtHC_ASS_Phy_Skin_Lesions.Text.Trim();
            //obj.IPA_PHY_NEURO_GENERAL = txtHC_ASS_Phy_Neuro_General.Text.Trim();
            //obj.IPA_PHY_NEURO_CONSCIO = txtHC_ASS_Phy_Neuro_Conscio.Text.Trim();
            //obj.IPA_PHY_NEURO_ORIENTED = txtHC_ASS_Phy_Neuro_Oriented.Text.Trim();
            //obj.IPA_PHY_NEURO_TIMERESP = txtHC_ASS_Phy_Neuro_TimeResp.Text.Trim();
            //obj.IPA_PHY_CARDIO_GENERAL = txtHC_ASS_Phy_Cardio_General.Text.Trim();
            //obj.IPA_PHY_CARDIO_PULSE = txtHC_ASS_Phy_Cardio_Pulse.Text.Trim();
            //obj.IPA_PHY_CARDIO_PEDALPULSE = txtHC_ASS_Phy_Cardio_PedalPulse.Text.Trim();
            //obj.IPA_PHY_CARDIO_EDEMA = txtHC_ASS_Phy_Cardio_Edema.Text.Trim();
            //obj.IPA_PHY_CARDIO_NAILBEDS = txtHC_ASS_Phy_Cardio_NailBeds.Text.Trim();
            //obj.IPA_PHY_CARDIO_CAPILLARY = txtHC_ASS_Phy_Cardio_Capillary.Text.Trim();
            //obj.IPA_PHY_ENT_EYES = txtHC_ASS_Phy_Ent_Eyes.Text.Trim();
            //obj.IPA_PHY_ENT_EARS = txtHC_ASS_Phy_Ent_Ears.Text.Trim();
            //obj.IPA_PHY_ENT_NOSE = txtHC_ASS_Phy_Ent_Nose.Text.Trim();
            //obj.IPA_PHY_ENT_THROAT = txtHC_ASS_Phy_Ent_Throat.Text.Trim();
            //obj.IPA_PHY_RESP_CHEST = txtHC_ASS_Phy_Resp_Chest.Text.Trim();
            //obj.IPA_PHY_RESP_BREATHPATT = txtHC_ASS_Phy_Resp_BreathPatt.Text.Trim();
            //obj.IPA_PHY_RESP_BREATHSOUND = txtHC_ASS_Phy_Resp_BreathSound.Text.Trim();
            //obj.IPA_RESP_BREATHCOUGH = txtHC_ASS_Phy_Resp_BreathCough.Text.Trim();
            //obj.IPA_PHY_NUTRI_DIET = txtHC_ASS_Phy_Nutri_Diet.Text.Trim();
            //obj.IPA_PHY_NUTRI_APPETITE = txtHC_ASS_Phy_Nutri_Appetite.Text.Trim();
            //obj.IPA_PHY_NUTRI_NUTRISUPPORT = txtHC_ASS_Phy_Nutri_NutriSupport.Text.Trim();
            //obj.IPA_PHY_NUTRI_FEEDINGDIF = txtHC_ASS_Phy_Nutri_FeedingDif.Text.Trim();
            //obj.IPA_PHY_NUTRI_WEIGHTSTATUS = txtHC_ASS_Phy_Nutri_WeightStatus.Text.Trim();
            //obj.IPA_PHY_NUTRI_DIAG = txtHC_ASS_Phy_Nutri_Diag.Text.Trim();
            obj.IPA_RISK_SKINRISK_SENSORY = txtHC_ASS_Risk_SkinRisk_Sensory.Text.Trim();
            obj.IPA_RISK_SKINRISK_MOISTURE = txtHC_ASS_Risk_SkinRisk_Moisture.Text.Trim();
            obj.IPA_RISK_SKINRISK_ACTIVITY = txtHC_ASS_Risk_SkinRisk_Activity.Text.Trim();
            obj.IPA_RISK_SKINRISK_MOBILITY = txtHC_ASS_Risk_SkinRisk_Mobility.Text.Trim();
            obj.IPA_RISK_SKINRISK_NUTRITION = txtHC_ASS_Risk_SkinRisk_Nutrition.Text.Trim();
            obj.IPA_RISK_SKINRISK_FRICTION = txtHC_ASS_Risk_SkinRisk_Friction.Text.Trim();
            obj.IPA_RISK_SOCIO_LIVING = txtHC_ASS_Risk_Socio_Living.Text.Trim();
            obj.IPA_RISK_SAFETY_GENERAL = txtHC_ASS_Risk_Safety_General.Text.Trim();
            obj.IPA_RISK_FUN_SELFCARING = txtHC_ASS_Risk_Fun_SelfCaring.Text.Trim();
            obj.IPA_RISK_FUN_MUSCULOS = txtHC_ASS_Risk_Fun_Musculos.Text.Trim();
            obj.IPA_RISK_FUN_EQUIPMENT = txtHC_ASS_Risk_Fun_Equipment.Text.Trim();
            obj.IPA_RISK_EDU_GENERAL = txtHC_ASS_Risk_Edu_General.Text.Trim();
            obj.UserID = Convert.ToString(Session["User_Code"]);

            obj.PTAssessmentAdd();

        }



        #endregion


        #region Segment

        DataTable BindHour()
        {
            int AppointmentStart = 0;
            int AppointmentEnd = 23;
            int index = 0;

            DataTable DT = new DataTable();

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";

            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";

            DT.Columns.Add(Name);
            DT.Columns.Add(Code);

            DataRow objrow;
            for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            {
                string strHour = Convert.ToString(index);
                if (index <= 9)
                {
                    strHour = "0" + Convert.ToString(index);
                }


                objrow = DT.NewRow();

                objrow["Name"] = strHour;
                objrow["Code"] = strHour;

                DT.Rows.Add(objrow);

                index = index + 1;

            }
            DT.AcceptChanges();

            return DT;
        }

        DataTable BindMinutes()
        {
            int AppointmentInterval = 15;
            int index = 1;
            int intCount = 0;
            DataTable DT = new DataTable();

            DataColumn Name = new DataColumn();
            Name.ColumnName = "Name";

            DataColumn Code = new DataColumn();
            Code.ColumnName = "Code";

            DT.Columns.Add(Name);
            DT.Columns.Add(Code);



            DataRow objrow;
            objrow = DT.NewRow();

            objrow["Name"] = "00";
            objrow["Code"] = "00";
            DT.Rows.Add(objrow);

            for (int j = AppointmentInterval; j < 60; j++)
            {

                objrow = DT.NewRow();

                objrow["Name"] = Convert.ToString(j - intCount);
                objrow["Code"] = Convert.ToString(j - intCount);

                DT.Rows.Add(objrow);

                j = j + AppointmentInterval;
                index = index + 1;
                intCount = intCount + 1;

            }

            DT.AcceptChanges();

            return DT;
        }


        DataSet GetSegmentData(string SegmentType)
        {
            objIniAssFrm = new IP_InitialAssessmentForm();
            string Criteria = " 1=1 AND IAS_ACTIVE=1 AND IAS_BRANCH='" + Convert.ToString(Session["Branch_ID"]) + "' AND IAS_TYPE='" + SegmentType + "'";
            // Criteria += " AND  IAS_POSITION='" + Position + "'";

            DataSet DS = new DataSet();

            DS = objIniAssFrm.PTAssessmentSegmentGet(Criteria);



            return DS;

        }

        public DataSet GetSegmentMaster(string SegmentType)
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND IASM_STATUS=1 ";
            Criteria += " and IASM_TYPE='" + SegmentType + "'";


            objIniAssFrm = new IP_InitialAssessmentForm();
            DS = objIniAssFrm.PTAssessmentSegmentMasterGet(Criteria);


            return DS;

        }

        public void SaveSegmentDtls(string strType, string FieldID, string FieName, string Selected, string Comment)
        {
            objIniAssFrm = new IP_InitialAssessmentForm();

            objIniAssFrm.BranchID = Convert.ToString(Session["Branch_ID"]);
            objIniAssFrm.EMRID = Convert.ToString(Session["EMR_ID"]);
            objIniAssFrm.PTID = Convert.ToString(Session["EMR_PT_ID"]);
            objIniAssFrm.IASD_TYPE = strType;
            objIniAssFrm.IASD_FIELD_ID = FieldID;
            objIniAssFrm.IASD_FIELD_NAME = FieName;
            objIniAssFrm.IASD_VALUE = Selected;
            objIniAssFrm.IASD_COMMENT = Comment;
            objIniAssFrm.PTAssessmentSegmentDtlsAdd();
        }

        public void SaveSegment(string SegmentType)
        {
            string Position = "";
            DataSet DS = new DataSet();

            DS = GetSegmentData(SegmentType);

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                Position = Convert.ToString(DR["IAS_POSITION"]);
                objIniAssFrm = new IP_InitialAssessmentForm();

                string CriteriaDel = " 1=1 AND IASD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IASD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                CriteriaDel += " AND IASD_TYPE='" + Convert.ToString(DR["IAS_CODE"]).Trim() + "'";
                objIniAssFrm.PTAssessmentSegmentDtlsGet(CriteriaDel);


                Int32 i = 1;
                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["IAS_CODE"]).Trim());



                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {
                    string strValueYes = "", _Comment = "";
                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "CheckBox".ToUpper())
                    {
                        CheckBox chk1 = new CheckBox();

                        if (Position == "LEFT")
                        {
                            if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY")
                            {
                                chk1 = (CheckBox)plhoPhyAssessmentLeft.FindControl("chk" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());
                            }
                            else if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY_ROS")
                            {

                            }
                            else if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY_PE")
                            {

                            }


                        }
                        else
                        {
                            if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY")
                            {
                                chk1 = (CheckBox)plhoPhyAssessmentRight.FindControl("chk" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());
                            }
                            else if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY_ROS")
                            {
                                chk1 = (CheckBox)plhoPhyROSRight.FindControl("chk" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());

                            }
                            else if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY_PE")
                            {
                                chk1 = (CheckBox)plhoPhyPESRight.FindControl("chk" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());

                            }


                        }







                        if (chk1 != null)
                        {
                            if (chk1.Checked == true)
                            {
                                strValueYes = "Y";
                            }
                        }

                    }
                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "TextBox".ToUpper() || Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "AutoTextBox".ToUpper() || Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                    {
                        TextBox txt = new TextBox();

                        if (Position == "LEFT")
                        {
                            txt = (TextBox)plhoPhyAssessmentLeft.FindControl("txt" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());
                        }
                        else
                        {
                            if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY")
                            {
                                txt = (TextBox)plhoPhyAssessmentRight.FindControl("txt" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());
                            }
                            else if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY_ROS")
                            {
                                txt = (TextBox)plhoPhyROSRight.FindControl("txt" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());
                            }
                            else if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY_PE")
                            {
                                txt = (TextBox)plhoPhyPESRight.FindControl("txt" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());
                            }

                        }

                        DropDownList drpHour = new DropDownList();
                        DropDownList drpMin = new DropDownList();

                        if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                        {
                            if (Position == "LEFT")
                            {
                                drpHour = (DropDownList)plhoPhyAssessmentLeft.FindControl("drpHour" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());
                                drpMin = (DropDownList)plhoPhyAssessmentLeft.FindControl("drpMin" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());
                            }
                            else
                            {
                                if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY")
                                {
                                    drpHour = (DropDownList)plhoPhyAssessmentRight.FindControl("drpHour" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());
                                    drpMin = (DropDownList)plhoPhyAssessmentRight.FindControl("drpMin" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());
                                }
                                else if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY_ROS")
                                {
                                    drpHour = (DropDownList)plhoPhyROSRight.FindControl("drpHour" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());
                                    drpMin = (DropDownList)plhoPhyROSRight.FindControl("drpMin" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());

                                }
                                else if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY_PE")
                                {
                                    drpHour = (DropDownList)plhoPhyPESRight.FindControl("drpHour" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());
                                    drpMin = (DropDownList)plhoPhyPESRight.FindControl("drpMin" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());

                                }
                            }

                        }


                        if (txt.Text != "")
                        {
                            strValueYes = "Y";
                            _Comment = txt.Text;
                        }

                        if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                        {
                            _Comment = txt.Text.Trim() + " " + drpHour.SelectedValue + ":" + drpMin.SelectedValue + ":00";
                        }
                    }




                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "RadioButtonList".ToUpper())
                    {

                        RadioButtonList RadList = new RadioButtonList();



                        if (Position == "LEFT")
                        {

                            RadList = (RadioButtonList)plhoPhyAssessmentLeft.FindControl("radl" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());

                        }
                        else
                        {
                            RadList = (RadioButtonList)plhoPhyAssessmentRight.FindControl("radl" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());

                        }






                        if (RadList != null)
                        {
                            if (RadList.SelectedValue != "")
                            {
                                strValueYes = "Y";
                                _Comment = RadList.SelectedValue;
                            }
                        }



                    }





                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "DropDownList".ToUpper())
                    {

                        DropDownList drpList = new DropDownList();



                        if (Position == "LEFT")
                        {

                            drpList = (DropDownList)plhoPhyAssessmentLeft.FindControl("drp" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());

                        }
                        else
                        {
                            drpList = (DropDownList)plhoPhyAssessmentRight.FindControl("drp" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim());

                        }







                        if (drpList != null)
                        {
                            if (drpList.SelectedValue != "" && drpList.SelectedValue != "---Select---")
                            {
                                strValueYes = "Y";
                                _Comment = drpList.SelectedValue;
                            }
                        }



                    }



                    if (strValueYes != "")
                    {
                        SaveSegmentDtls(Convert.ToString(DR["IAS_CODE"]).Trim(), Convert.ToString(DR1["IASM_FIELD_ID"]).Trim(), Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim(), strValueYes, _Comment);

                    }

                }
                i = i + 1;
            }
        }


        public void CreateSegCtrls(string SegmentType)
        {
            string Position = "";
            DataSet DS = new DataSet();

            DS = GetSegmentData(SegmentType);

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                Position = Convert.ToString(DR["IAS_POSITION"]);


                Panel pnlHeader = new Panel();
                pnlHeader.ID = "pnl" + Convert.ToString(DR["IAS_CODE"]);
                pnlHeader.BorderStyle = BorderStyle.Groove;
                pnlHeader.BorderWidth = 0;
                //  pnlHeader.Style.Add("display", "none");
                pnlHeader.Style.Add("width", "99.5%");
                pnlHeader.Style.Add("border", "thin");
                pnlHeader.Style.Add("border-color", "#F7F2F2");//F7F2F2
                pnlHeader.Style.Add("border-style", "groove");
                // pnlHeader.Style.Add("padding-bottom", " 20px");
                pnlHeader.Style.Add("border-radius", "5px");
                pnlHeader.Style.Add("heigh", "10px");

                Literal lit2 = new Literal();
                lit2.Text = "&nbsp;";

                Label lblHeader = new Label();
                lblHeader.Text = Convert.ToString(DR["IAS_NAME"]);
                lblHeader.CssClass = "lblCaption1";
                lblHeader.Style.Add("font-family", "Segoe UI,arial");
                lblHeader.Style.Add("font-size", "13px");
                lblHeader.Style.Add("width", "95%");
                // lblHeader.ReadOnly = true;
                lblHeader.BorderStyle = BorderStyle.None;

                HtmlAnchor Anc1 = new HtmlAnchor();
                Anc1.ID = "anc" + Convert.ToString(DR["IAS_CODE"]);
                Anc1.Title = "+";
                Anc1.InnerText = "+";
                Anc1.Style.Add("color", "#005c7b");
                Anc1.Style.Add("font-weight", "bold");
                Anc1.Style.Add("font-family", "Arial Black");
                Anc1.Style.Add("font-size", "18px");
                // Anc1.Attributes.Add("onclick", "DivShow('" + pnlHeader.ID + "','" + Anc1.ID + "')");
                Anc1.Style.Add("width", "5%");

                if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY_ROS")
                {
                    lblHeader.Style.Add("color", "#ffffff");
                    Anc1.Style.Add("color", "#ffffff");
                    pnlHeader.Style.Add("background-image", "url(../Images/ui-bg_flat_75_02a0e0_40x100.PNG)");
                }
                else if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY_PE")
                {
                    lblHeader.Style.Add("color", "#ffffff");
                    Anc1.Style.Add("color", "#ffffff");
                    pnlHeader.Style.Add("background-image", "url(../Images/ui-bg_flat_75_02a0e0_40x100.PNG)");
                }

                pnlHeader.Controls.Add(lit2);
                pnlHeader.Controls.Add(lblHeader);
                pnlHeader.Controls.Add(Anc1);

                Panel myFieldSet = new Panel();

                // myFieldSet.GroupingText = Convert.ToString(DR["IAS_NAME"]);
                myFieldSet.ID = "myFieldSet" + Convert.ToString(DR["IAS_CODE"]);
                myFieldSet.Style.Add("text-align", "justify");
                myFieldSet.CssClass = "lblCaption1";
                myFieldSet.Style.Add("display", "none");

                myFieldSet.Style.Add("width", "99.5%");
                myFieldSet.Style.Add("border", "thin");
                myFieldSet.Style.Add("border-color", "#F7F2F2");//F7F2F2
                myFieldSet.Style.Add("border-style", "groove");
                myFieldSet.Style.Add("padding-bottom", " 20px");
                myFieldSet.Style.Add("border-radius", "5px");
                myFieldSet.Style.Add("heigh", "30px");


                myFieldSet.Width = 500;
                Int32 i = 1;

                pnlHeader.Attributes.Add("onclick", "DivShow('" + myFieldSet.ID + "','" + Anc1.ID + "')");

                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["IAS_CODE"]));

                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {


                    Boolean _checked = false;
                    string strComment = "";
                    objIniAssFrm = new IP_InitialAssessmentForm();
                    string Criteria2 = "1=1 ";
                    Criteria2 += " AND IASD_TYPE='" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "' AND IASD_FIELD_ID='" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim() + "' ";
                    Criteria2 += " AND IASD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IASD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

                    DataSet DS2 = new DataSet();
                    DS2 = objIniAssFrm.PTAssessmentSegmentDtlsGet(Criteria2);

                    if (DS2.Tables[0].Rows.Count > 0)
                    {
                        _checked = true;

                        strComment = Convert.ToString(DS2.Tables[0].Rows[0]["IASD_COMMENT"]).Trim();
                    }


                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "CheckBox".ToUpper())
                    {
                        CheckBox chk = new CheckBox();
                        chk.ID = "chk" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();
                        chk.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim();
                        chk.CssClass = "lblCaption1";

                        chk.Checked = _checked;

                        //tdHe1.Controls.Add(chk);
                        //trHe1.Controls.Add(tdHe1);
                        //myFieldSet.Controls.Add(trHe1);

                        myFieldSet.Controls.Add(chk);
                    }


                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "TextBox".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim() + ":";
                        myFieldSet.Controls.Add(lbl);

                        Literal lit1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                        myFieldSet.Controls.Add(lit1);

                        TextBox txt = new TextBox();
                        txt.ID = "txt" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]);
                        txt.CssClass = "Label";
                        txt.Style.Add("width", "99%");

                        txt.Text = strComment;

                        if (DR1.IsNull("IASM_TARGET_CTRL") == false)
                        {
                            if (Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim() == "Height" || Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim() == "Weight")
                            {
                                string strCtrlID = Convert.ToString(DR1["IASM_TARGET_CTRL"]).Trim();
                                txt.Attributes.Add("onkeyup", "return BindBMI('txt" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_5','txt" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_6','txt" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + strCtrlID + "')");

                            }
                        }
                        myFieldSet.Controls.Add(txt);


                    }


                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "AutoTextBox".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim() + ":";
                        myFieldSet.Controls.Add(lbl);

                        Literal lit1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                        myFieldSet.Controls.Add(lit1);

                        TextBox txt = new TextBox();
                        txt.ID = "txt" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();
                        txt.Style.Add("width", "99%");
                        txt.Style.Add("height", "20px");
                        txt.TextMode = TextBoxMode.MultiLine;
                        txt.Height = 20;
                        txt.Style.Add("resize", "none");
                        txt.Attributes.Add("autocomplete", "off");
                        txt.CssClass = "Label";

                        if (DR1.IsNull("IASM_HEIGHT") == false)
                        {
                            txt.Height = Convert.ToInt32(DR1["IASM_HEIGHT"]);
                            txt.Style.Add("height", Convert.ToString(DR1["IASM_HEIGHT"] + "px"));
                        }
                        else
                        {
                            txt.Style.Add("height", "50px");
                        }

                        AjaxControlToolkit.AutoCompleteExtender autoCompleteExtender = new AjaxControlToolkit.AutoCompleteExtender();
                        autoCompleteExtender.TargetControlID = txt.ID;


                        if (DR1.IsNull("IASM_DATA_LOAD_FROM") == false && Convert.ToString(DR1["IASM_DATA_LOAD_FROM"]) != "")
                        {
                            if (Convert.ToString(DR1["IASM_DATA_LOAD_FROM"]).Trim().ToUpper() == "DIAGNOSIS")
                            {
                                autoCompleteExtender.ServiceMethod = "GetDiagnosisName";
                            }
                            if (Convert.ToString(DR1["IASM_DATA_LOAD_FROM"]).Trim().ToUpper() == "PROCEDURE")
                            {
                                autoCompleteExtender.ServiceMethod = "GetProcedure";
                            }

                            if (Convert.ToString(DR1["IASM_DATA_LOAD_FROM"]).Trim().ToUpper() == "DOCTOR")
                            {
                                autoCompleteExtender.ServiceMethod = "GetDoctorName";
                            }

                            if (Convert.ToString(DR1["IASM_DATA_LOAD_FROM"]).Trim().ToUpper() == "HISTORY")
                            {
                                autoCompleteExtender.ServiceMethod = "GetSegmentValue";
                            }
                        }
                        else
                        {
                            autoCompleteExtender.ServiceMethod = "GetSegmentValue";
                        }

                        autoCompleteExtender.ID = "aut" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();
                        autoCompleteExtender.CompletionListCssClass = "AutoExtender";
                        autoCompleteExtender.CompletionListItemCssClass = "AutoExtenderList";
                        autoCompleteExtender.CompletionListHighlightedItemCssClass = "AutoExtenderHighlight";
                        // autoCompleteExtender.CompletionListElementID = "divwidth";
                        autoCompleteExtender.ContextKey = Convert.ToString(DR1["IASM_TYPE"]).Trim() + "~" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();
                        autoCompleteExtender.UseContextKey = true;

                        // autoCompleteExtender.ServicePath = "YourAutoCompleteWebService.asmx";
                        autoCompleteExtender.CompletionInterval = 10;
                        autoCompleteExtender.CompletionSetCount = 15;
                        autoCompleteExtender.EnableCaching = true;
                        autoCompleteExtender.MinimumPrefixLength = 1;



                        txt.Text = strComment;

                        if (DR1.IsNull("IASM_TARGET_CTRL") == false)
                        {
                            string strCtrlID = Convert.ToString(DR1["IASM_TARGET_CTRL"]).Trim();
                            txt.Attributes.Add("onkeypress", "return BindCC(event,'" + txt.ID + "','txt" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + strCtrlID + "')");

                            autoCompleteExtender.ContextKey = Convert.ToString(DR1["IASM_TYPE"]).Trim() + "~" + strCtrlID;
                        }


                        myFieldSet.Controls.Add(txt);
                        myFieldSet.Controls.Add(autoCompleteExtender);



                    }

                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim() + ":";
                        lbl.Width = 150;
                        myFieldSet.Controls.Add(lbl);

                        TextBox txt = new TextBox();
                        txt.ID = "txt" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]);
                        txt.CssClass = "lblCaption1";
                        txt.Style.Add("width", "75px");

                        AjaxControlToolkit.CalendarExtender CalendarExtender = new AjaxControlToolkit.CalendarExtender();
                        CalendarExtender.TargetControlID = txt.ID;
                        CalendarExtender.Format = "dd/MM/yyyy";


                        DropDownList drpHour = new DropDownList();
                        drpHour.ID = "drpHour" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();
                        drpHour.CssClass = "lblCaption1";
                        drpHour.Style.Add("width", "48px");

                        drpHour.DataSource = BindHour();
                        drpHour.DataTextField = "Name";
                        drpHour.DataValueField = "Code";
                        drpHour.DataBind();

                        DropDownList drpMin = new DropDownList();
                        drpMin.ID = "drpMin" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();
                        drpMin.CssClass = "lblCaption1";
                        drpMin.Style.Add("width", "48px");

                        drpMin.DataSource = BindMinutes();
                        drpMin.DataTextField = "Name";
                        drpMin.DataValueField = "Code";
                        drpMin.DataBind();





                        string strDateTime = strComment.Trim();
                        string[] arrDateTime = strDateTime.Split(' ');

                        if (arrDateTime.Length > 0)
                        {
                            txt.Text = arrDateTime[0];

                            if (arrDateTime.Length > 1)
                            {
                                string strIA_TIME_IN = arrDateTime[1];

                                string[] arrIA_TIME_IN = strIA_TIME_IN.Split(':');
                                if (arrIA_TIME_IN.Length > 1)
                                {
                                    drpHour.SelectedValue = arrIA_TIME_IN[0];
                                    drpMin.SelectedValue = arrIA_TIME_IN[1];

                                }
                            }

                        }

                        myFieldSet.Controls.Add(txt);
                        myFieldSet.Controls.Add(CalendarExtender);
                        myFieldSet.Controls.Add(drpHour);
                        myFieldSet.Controls.Add(drpMin);


                    }




                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "Label".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim();
                        myFieldSet.Controls.Add(lbl);



                    }

                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]) == "DropDownList")
                    {

                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim() + ":    ";
                        lbl.Width = 150;
                        myFieldSet.Controls.Add(lbl);

                        DropDownList DrpList = new DropDownList();
                        DrpList.ID = "drp" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();
                        DrpList.Width = 200;

                        string strRadFieldName = Convert.ToString(DR1["IASM_DATA"]);
                        string[] arrRadFieldName = strRadFieldName.Split('|');


                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            DrpList.Items.Add(new ListItem(arrRadFieldName[intRad], arrRadFieldName[intRad]));

                        }

                        DrpList.CssClass = "lblCaption1";

                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            if (DrpList.Items[intRad].Value == strComment)
                            {
                                DrpList.SelectedValue = strComment;
                            }

                        }

                        myFieldSet.Controls.Add(DrpList);

                    }




                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]) == "RadioButtonList")
                    {

                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim() + ":    ";
                        lbl.Width = 150;
                        myFieldSet.Controls.Add(lbl);

                        RadioButtonList RadList = new RadioButtonList();
                        RadList.ID = "radl" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();

                        RadList.RepeatDirection = RepeatDirection.Horizontal;
                        RadList.Width = Convert.ToInt32(DR1["IASM_WIDTH"]);
                        string strRadFieldName = Convert.ToString(DR1["IASM_DATA"]);
                        string[] arrRadFieldName = strRadFieldName.Split('|');


                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            RadList.Items.Add(new ListItem(arrRadFieldName[intRad], arrRadFieldName[intRad]));

                        }

                        RadList.CssClass = "lblCaption1";

                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            if (RadList.Items[intRad].Value == strComment)
                            {
                                RadList.SelectedValue = strComment;
                            }

                        }

                        myFieldSet.Controls.Add(RadList);

                    }

                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]) == "Image")
                    {

                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim() + ":    ";
                        lbl.Width = 150;
                        myFieldSet.Controls.Add(lbl);

                        Image img = new Image();
                        img.ID = "img" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();

                        img.ImageUrl = "../Images/" + Convert.ToString(DR1["IASM_DATA"]).Trim();

                        if (DR1.IsNull("IASM_WIDTH") == false && Convert.ToString(DR1["IASM_WIDTH"]) != "")
                        {
                            img.Width = Convert.ToInt32(DR1["IASM_WIDTH"]);
                        }
                        else
                        {
                            img.Width = 50;
                        }



                        if (DR1.IsNull("IASM_TARGET_CTRL") == false)
                        {
                            string strCtrlID = Convert.ToString(DR1["IASM_TARGET_CTRL"]).Trim();
                            img.Attributes.Add("onClick", "return BindValue('" + Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim() + "','txt" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + strCtrlID + "')");


                        }

                        myFieldSet.Controls.Add(img);

                    }

                    Literal lit = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                    myFieldSet.Controls.Add(lit);

                }

                Literal litGap1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };

                if (Position == "LEFT")
                {
                    if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY")
                    {
                        plhoPhyAssessmentLeft.Controls.Add(pnlHeader);
                        plhoPhyAssessmentLeft.Controls.Add(myFieldSet);
                        plhoPhyAssessmentLeft.Controls.Add(litGap1);
                    }



                }
                else
                {
                    if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY")
                    {
                        plhoPhyAssessmentRight.Controls.Add(pnlHeader);
                        plhoPhyAssessmentRight.Controls.Add(myFieldSet);
                        plhoPhyAssessmentRight.Controls.Add(litGap1);
                    }
                    else if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY_ROS")
                    {
                        plhoPhyROSRight.Controls.Add(pnlHeader);
                        plhoPhyROSRight.Controls.Add(myFieldSet);
                        plhoPhyROSRight.Controls.Add(litGap1);
                    }

                    else if (SegmentType == "IP_INITIAL_ASSESSMENT_PHY_PE")
                    {
                        plhoPhyPESRight.Controls.Add(pnlHeader);
                        plhoPhyPESRight.Controls.Add(myFieldSet);
                        plhoPhyPESRight.Controls.Add(litGap1);
                    }


                }






                i = i + 1;



            }
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_INITIAL_DR_ASS' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Visible = false;
                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSave.Visible = false;
                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }

        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetStaffName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 ";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND HSFM_STAFF_ID +' '  + HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ";

            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]).Trim();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetProcedure(string prefixText)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string[] Data;

            ds = dbo.HaadServicessListGet("Procedure", prefixText, "", "");

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["Code"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["Description"]).Trim();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS='Present'";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND ( HSFM_STAFF_ID Like '%" + prefixText + "%' OR HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ) ";

            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetSegmentValue(string prefixText, int count, string contextKey)
        {
            string SegType = "", FieldID = "";
            string[] strContKey = contextKey.Split('~');

            if (strContKey.Length > 0)
            {
                SegType = strContKey[0];
                FieldID = strContKey[1];
                //SegFieldName = strContKey[2];
            }

            string[] Data;
            string Criteria = " 1=1  ";

            IP_InitialAssessmentForm objIniAssFrm = new IP_InitialAssessmentForm();

            Criteria += " AND IASD_TYPE='" + SegType + "' AND IASD_FIELD_ID='" + FieldID + "' ";
            Criteria += " AND IASD_COMMENT Like '%" + prefixText + "%'";

            DataSet DS = new DataSet();
            DS = objIniAssFrm.PTAssessmentSegmentDtlsGet(Criteria);




            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = DS.Tables[0].Rows[i]["IASD_COMMENT"].ToString();
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }


        [System.Web.Services.WebMethod]
        public static string[] GetDiagnosisName(string prefixText)
        {

            CommonBAL dbo = new CommonBAL();
            DataSet DS = new DataSet();

            string[] Data;

            string Criteria = " 1=1 AND DIM_STATUS='A' ";

            Criteria += " AND ( DIM_ICD_ID LIKE '" + prefixText + "' OR DIM_ICD_DESCRIPTION   LIKE '" + prefixText + "%')";
            DS = dbo.DrICDMasterGet(Criteria, "30");

            if (DS.Tables[0].Rows.Count > 0)
            {
                Data = new string[DS.Tables[0].Rows.Count];
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_ID"]) + "~" + Convert.ToString(DS.Tables[0].Rows[i]["DIM_ICD_DESCRIPTION"]);

                }


                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }


                this.Page.Title = "InitialAssessmentForm";
                try
                {



                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtVisitDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtArrivalDate.Text = strFromDate.ToString("dd/MM/yyyy");


                    BindTime();

                    BindEncounter();
                    BindAssessment();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PatientEncounter.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }



        public void generateDynamicControls()
        {

            CreateSegCtrls("IP_INITIAL_ASSESSMENT_PHY");
            CreateSegCtrls("IP_INITIAL_ASSESSMENT_PHY_ROS");
            CreateSegCtrls("IP_INITIAL_ASSESSMENT_PHY_PE");



        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveEncounter();
                SaveAssessment();

                CommonBAL objCom = new CommonBAL();
                string Criteria = " IASD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IASD_ID=" + Convert.ToString(Session["EMR_ID"]) + " AND  IASD_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
                objCom.fnDeleteTableData("IP_PT_ASSESSMENT_SEGMENT_DTLS", Criteria);

                //  SaveSegment();

                SaveSegment("IP_INITIAL_ASSESSMENT_PHY");
                SaveSegment("IP_INITIAL_ASSESSMENT_PHY_ROS");
                SaveSegment("IP_INITIAL_ASSESSMENT_PHY_PE");

                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientEncounter.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }



        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientEncounter.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }




        #endregion
    }
}