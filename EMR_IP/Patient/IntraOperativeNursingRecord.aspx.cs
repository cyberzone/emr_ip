﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;


namespace EMR_IP.Patient
{
    public partial class IntraOperativeNursingRecord : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpInterOperHour.DataSource = DS;
                drpInterOperHour.DataTextField = "Name";
                drpInterOperHour.DataValueField = "Code";
                drpInterOperHour.DataBind();

                drpourniTimeUpHour.DataSource = DS;
                drpourniTimeUpHour.DataTextField = "Name";
                drpourniTimeUpHour.DataValueField = "Code";
                drpourniTimeUpHour.DataBind();

                drpourniTimeDownHour.DataSource = DS;
                drpourniTimeDownHour.DataTextField = "Name";
                drpourniTimeDownHour.DataValueField = "Code";
                drpourniTimeDownHour.DataBind();

                drpScrubNurseHour.DataSource = DS;
                drpScrubNurseHour.DataTextField = "Name";
                drpScrubNurseHour.DataValueField = "Code";
                drpScrubNurseHour.DataBind();


                drpCirculNurseHour.DataSource = DS;
                drpCirculNurseHour.DataTextField = "Name";
                drpCirculNurseHour.DataValueField = "Code";
                drpCirculNurseHour.DataBind();


                drpReScrubNurseHour.DataSource = DS;
                drpReScrubNurseHour.DataTextField = "Name";
                drpReScrubNurseHour.DataValueField = "Code";
                drpReScrubNurseHour.DataBind();


                drpReCirculNurseHour.DataSource = DS;
                drpReCirculNurseHour.DataTextField = "Name";
                drpReCirculNurseHour.DataValueField = "Code";
                drpReCirculNurseHour.DataBind();

                 

            }

            drpInterOperHour.Items.Insert(0, "00");
            drpInterOperHour.Items[0].Value = "00";


           

            drpourniTimeUpHour.Items.Insert(0, "00");
            drpourniTimeUpHour.Items[0].Value = "00";

            drpourniTimeDownHour.Items.Insert(0, "00");
            drpourniTimeDownHour.Items[0].Value = "00";

            drpScrubNurseHour.Items.Insert(0, "00");
            drpScrubNurseHour.Items[0].Value = "00";


            drpCirculNurseHour.Items.Insert(0, "00");
            drpCirculNurseHour.Items[0].Value = "00";


            drpReScrubNurseHour.Items.Insert(0, "00");
            drpReScrubNurseHour.Items[0].Value = "00";

            drpReCirculNurseHour.Items.Insert(0, "00");
            drpReCirculNurseHour.Items[0].Value = "00";


            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpInterOperMin.DataSource = DS;
                drpInterOperMin.DataTextField = "Name";
                drpInterOperMin.DataValueField = "Code";
                drpInterOperMin.DataBind();


                drpourniTimeUpMin.DataSource = DS;
                drpourniTimeUpMin.DataTextField = "Name";
                drpourniTimeUpMin.DataValueField = "Code";
                drpourniTimeUpMin.DataBind();

                drpourniTimeDownMin.DataSource = DS;
                drpourniTimeDownMin.DataTextField = "Name";
                drpourniTimeDownMin.DataValueField = "Code";
                drpourniTimeDownMin.DataBind();


                drpScrubNurseMin.DataSource = DS;
                drpScrubNurseMin.DataTextField = "Name";
                drpScrubNurseMin.DataValueField = "Code";
                drpScrubNurseMin.DataBind();


                drpCirculNurseMin.DataSource = DS;
                drpCirculNurseMin.DataTextField = "Name";
                drpCirculNurseMin.DataValueField = "Code";
                drpCirculNurseMin.DataBind();

                drpReScrubNurseMin.DataSource = DS;
                drpReScrubNurseMin.DataTextField = "Name";
                drpReScrubNurseMin.DataValueField = "Code";
                drpReScrubNurseMin.DataBind();



                drpReCirculNurseMin.DataSource = DS;
                drpReCirculNurseMin.DataTextField = "Name";
                drpReCirculNurseMin.DataValueField = "Code";
                drpReCirculNurseMin.DataBind();


               
            }

            drpInterOperMin.Items.Insert(0, "00");
            drpInterOperMin.Items[0].Value = "00";

            drpourniTimeUpMin.Items.Insert(0, "00");
            drpourniTimeUpMin.Items[0].Value = "00";

            drpourniTimeDownMin.Items.Insert(0, "00");
            drpourniTimeDownMin.Items[0].Value = "00";

            drpScrubNurseMin.Items.Insert(0, "00");
            drpScrubNurseMin.Items[0].Value = "00";

            drpCirculNurseMin.Items.Insert(0, "00");
            drpCirculNurseMin.Items[0].Value = "00";

            drpReScrubNurseMin.Items.Insert(0, "00");
            drpReScrubNurseMin.Items[0].Value = "00";


            drpReCirculNurseMin.Items.Insert(0, "00");
            drpReCirculNurseMin.Items[0].Value = "00";
            
        }

        void BindStaff()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 "; // AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.GetStaffMaster(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpSurgeon.DataSource = DS;
                drpSurgeon.DataTextField = "FullName";
                drpSurgeon.DataValueField = "HSFM_STAFF_ID";
                drpSurgeon.DataBind();

                drpAnaesthetist.DataSource = DS;
                drpAnaesthetist.DataTextField = "FullName";
                drpAnaesthetist.DataValueField = "HSFM_STAFF_ID";
                drpAnaesthetist.DataBind();

                drpSafetyStrapPositionedBy.DataSource = DS;
                drpSafetyStrapPositionedBy.DataTextField = "FullName";
                drpSafetyStrapPositionedBy.DataValueField = "HSFM_STAFF_ID";
                drpSafetyStrapPositionedBy.DataBind();

                drpElectroUnitAppliedBy.DataSource = DS;
                drpElectroUnitAppliedBy.DataTextField = "FullName";
                drpElectroUnitAppliedBy.DataValueField = "HSFM_STAFF_ID";
                drpElectroUnitAppliedBy.DataBind();

                drpUriCatheterInsertedBy.DataSource = DS;
                drpUriCatheterInsertedBy.DataTextField = "FullName";
                drpUriCatheterInsertedBy.DataValueField = "HSFM_STAFF_ID";
                drpUriCatheterInsertedBy.DataBind();


                drpPreAnesthesiologist.DataSource = DS;
                drpPreAnesthesiologist.DataTextField = "FullName";
                drpPreAnesthesiologist.DataValueField = "HSFM_STAFF_ID";
                drpPreAnesthesiologist.DataBind();


                drpSkinAppliedBy.DataSource = DS;
                drpSkinAppliedBy.DataTextField = "FullName";
                drpSkinAppliedBy.DataValueField = "HSFM_STAFF_ID";
                drpSkinAppliedBy.DataBind();

            }


            drpSurgeon.Items.Insert(0, "--- Select ---");
            drpSurgeon.Items[0].Value = "";
            drpAnaesthetist.Items.Insert(0, "--- Select ---");
            drpAnaesthetist.Items[0].Value = "";
            drpSafetyStrapPositionedBy.Items.Insert(0, "--- Select ---");
            drpSafetyStrapPositionedBy.Items[0].Value = "";
            drpElectroUnitAppliedBy.Items.Insert(0, "--- Select ---");
            drpElectroUnitAppliedBy.Items[0].Value = "";
            drpUriCatheterInsertedBy.Items.Insert(0, "--- Select ---");
            drpUriCatheterInsertedBy.Items[0].Value = "";
            drpPreAnesthesiologist.Items.Insert(0, "--- Select ---");
            drpPreAnesthesiologist.Items[0].Value = "";

            drpSkinAppliedBy.Items.Insert(0, "--- Select ---");
            drpSkinAppliedBy.Items[0].Value = "";






            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindNurse()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  AND HSFM_CATEGORY='Nurse'";
            Criteria += " AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.GetStaffMaster(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpScrubNurse.DataSource = DS;
                drpScrubNurse.DataTextField = "FullName";
                drpScrubNurse.DataValueField = "HSFM_STAFF_ID";
                drpScrubNurse.DataBind();

                drpCirculNurse.DataSource = DS;
                drpCirculNurse.DataTextField = "FullName";
                drpCirculNurse.DataValueField = "HSFM_STAFF_ID";
                drpCirculNurse.DataBind();


                drpReScrubNurse.DataSource = DS;
                drpReScrubNurse.DataTextField = "FullName";
                drpReScrubNurse.DataValueField = "HSFM_STAFF_ID";
                drpReScrubNurse.DataBind();

                drpReCirculNurse.DataSource = DS;
                drpReCirculNurse.DataTextField = "FullName";
                drpReCirculNurse.DataValueField = "HSFM_STAFF_ID";
                drpReCirculNurse.DataBind();


            }


            drpScrubNurse.Items.Insert(0, "--- Select ---");
            drpScrubNurse.Items[0].Value = "";
            drpCirculNurse.Items.Insert(0, "--- Select ---");
            drpCirculNurse.Items[0].Value = "";


            drpReScrubNurse.Items.Insert(0, "--- Select ---");
            drpReScrubNurse.Items[0].Value = "";
            drpReCirculNurse.Items.Insert(0, "--- Select ---");
            drpReCirculNurse.Items[0].Value = "";






            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindData()
        {
            IP_IntraOperativeNursingRecord objIONS = new IP_IntraOperativeNursingRecord();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " and  IONR_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' AND IONR_ID=" + Convert.ToString(Session["EMR_ID"]);
            DS = objIONS.InterOperNursRecordGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    txtInterOperDate.Text = Convert.ToString(DR["IONR_DATEDesc"]);

                    string strInterOperHour = Convert.ToString(DR["IONR_DATETime"]);

                    string[] arrInterOperHour = strInterOperHour.Split(':');
                    if (arrInterOperHour.Length > 0)
                    {
                        drpInterOperHour.SelectedValue = arrInterOperHour[0];
                        if (arrInterOperHour.Length > 1)
                        {
                            drpInterOperMin.SelectedValue = arrInterOperHour[1];
                        }
                    }

                    txtPreOperDiag.Text = Convert.ToString(DR["IONR_PRE_OPER_DIAG"]);
                    txtPostOperDiag.Text = Convert.ToString(DR["IONR_POST_OPER_DIAG"]);
                    txtOperProcPerf.Text = Convert.ToString(DR["IONR_OPER_PROCEDURE"]);


                    drpSurgeon.SelectedValue = Convert.ToString(DR["IONR_SURGEN"]);
                    drpAnaesthetist.SelectedValue = Convert.ToString(DR["IONR_ANAESTHETIST"]);

                    drpAnesType.SelectedValue = Convert.ToString(DR["IONR_ANESTHESIA_TYPE"]);
                    drpAnesClassifi.SelectedValue = Convert.ToString(DR["IONR_ANESTHESIA_CLASS"]);


                    radInfoMedicFood.SelectedValue = Convert.ToString(DR["IONR_PT_IDENTIFI"]);

                    string strAllergies = Convert.ToString(DR["IONR_ALLERGIES"]);

                    string[] arrAllergies = strAllergies.Split('|');

                    if (arrAllergies.Length > 0)
                    {

                        if (arrAllergies[0] == radAllergiesYes.Value)
                        {
                            radAllergiesYes.Checked = true;
                        }
                        else if (arrAllergies[0] == radAllergiesNo.Value)
                        {
                            radAllergiesNo.Checked = true;
                        }
                        txtAlergies.Value = arrAllergies[1];
                    }


                    string strIONR_NPO = Convert.ToString(DR["IONR_NPO"]);

                    string[] arrIONR_NPO = strIONR_NPO.Split('|');

                    if (arrIONR_NPO.Length > 0)
                    {

                        if (arrIONR_NPO[0] == radNPOYes.Value)
                        {
                            radNPOYes.Checked = true;
                        }
                        else if (arrIONR_NPO[0] == radNPONo.Value)
                        {
                            radNPONo.Checked = true;
                        }
                        if (arrIONR_NPO.Length > 1)
                        {
                            txtNPO.Value = arrIONR_NPO[1];
                        }
                    }




                    string strIONR_OPER_CONSENT = Convert.ToString(DR["IONR_OPER_CONSENT"]);

                    string[] arrIONR_OPER_CONSENT = strIONR_OPER_CONSENT.Split('|');

                    if (arrIONR_OPER_CONSENT.Length > 0)
                    {

                        if (arrIONR_OPER_CONSENT[0] == radOperConYes.Value)
                        {
                            radOperConYes.Checked = true;
                        }
                        else if (arrIONR_OPER_CONSENT[0] == radOperConNo.Value)
                        {
                            radOperConNo.Checked = true;
                        }
                        if (arrIONR_OPER_CONSENT.Length > 1)
                        {
                            txtOperCon.Value = arrIONR_OPER_CONSENT[1];
                        }
                    }





                    string strIONR_TIME_OUT = Convert.ToString(DR["IONR_TIME_OUT"]);

                    string[] arrIONR_TIME_OUT = strIONR_TIME_OUT.Split('|');

                    if (arrIONR_TIME_OUT.Length > 0)
                    {

                        if (arrIONR_TIME_OUT[0] == radTimeOutYes.Value)
                        {
                            radTimeOutYes.Checked = true;
                        }
                        else if (arrIONR_TIME_OUT[0] == radTimeOutNo.Value)
                        {
                            radTimeOutNo.Checked = true;
                        }
                        if (arrIONR_TIME_OUT.Length > 1)
                        {
                            txtTimeOut.Value = arrIONR_TIME_OUT[1];
                        }
                    }

                    string strIONR_VARIFI_OPER_CHECKLIST = Convert.ToString(DR["IONR_VARIFI_OPER_CHECKLIST"]);
                    if (strIONR_VARIFI_OPER_CHECKLIST == radVarifOperCheckList.Value)
                    {
                        radVarifOperCheckList.Checked = true;
                    }

                    txtAssComment.Text = Convert.ToString(DR["IONR_ASSESS_COMMENT"]);

                    drpCaseClasification.SelectedValue = Convert.ToString(DR["IONR_CASE_CLASSIFI"]);

                    drpCaseType.SelectedValue = Convert.ToString(DR["IONR_CASE_TYPE"]);

                    txtIntubationTime.Text = Convert.ToString(DR["IONR_INTUBATION_TIME"]);

                    txtIncisionTime.Text = Convert.ToString(DR["IONR_NCISION_TIME"]);
                    txtExtubationTime.Text = Convert.ToString(DR["IONR_EXTUBATION_TIME"]);


                    string strIONR_HAIR_REMOVAL = Convert.ToString(DR["IONR_HAIR_REMOVAL"]);

                    if (strIONR_HAIR_REMOVAL == radHairRemovalYes.Value)
                    {
                        radHairRemovalYes.Checked = true;
                    }
                    else if (strIONR_HAIR_REMOVAL == radHairRemovalNo.Value)
                    {
                        radHairRemovalNo.Checked = true;
                    }

                    else if (strIONR_HAIR_REMOVAL == radClipped.Value)
                    {
                        radClipped.Checked = true;
                    }

                    txtDrainspacking.Text = Convert.ToString(DR["IONR_DRAINS_PACKING"]);

                    txtDrainsLocation.Text = Convert.ToString(DR["IONR_DRAINS_LOCATION"]);



                    string strIONR_CLIPPER = Convert.ToString(DR["IONR_CLIPPER"]);

                    if (strIONR_CLIPPER == radClipperYes.Value)
                    {
                        radClipperYes.Checked = true;
                    }
                    else if (strIONR_CLIPPER == radClipperNo.Value)
                    {
                        radClipperNo.Checked = true;
                    }


                    drpSkinCondition.SelectedValue = Convert.ToString(DR["IONR_SKIN_CONDITION"]).Replace("?", "");

                    txtDressing.Text = Convert.ToString(DR["IONR_DRESSING"]);



                    string strIONR_SPONGE_COUNT = Convert.ToString(DR["IONR_SPONGE_COUNT"]);

                    string[] arrIONR_SPONGE_COUNT = strIONR_SPONGE_COUNT.Split('|');

                    if (arrIONR_SPONGE_COUNT.Length > 0)
                    {

                        txtSponge1.Text = arrIONR_SPONGE_COUNT[0];
                        if (arrIONR_SPONGE_COUNT.Length > 1)
                        {
                            txtSponge2.Text = arrIONR_SPONGE_COUNT[1];
                        }

                    }

                    string strIONR_GAUZE_COUNT = Convert.ToString(DR["IONR_GAUZE_COUNT"]);

                    string[] arrIONR_GAUZE_COUNT = strIONR_GAUZE_COUNT.Split('|');

                    if (arrIONR_GAUZE_COUNT.Length > 0)
                    {

                        txtGauze1.Text = arrIONR_GAUZE_COUNT[0];
                        if (arrIONR_GAUZE_COUNT.Length > 1)
                        {
                            txtGauze2.Text = arrIONR_GAUZE_COUNT[1];
                        }

                    }

                    string strIONR_PAENUT_COUNT = Convert.ToString(DR["IONR_PAENUT_COUNT"]);

                    string[] arrIONR_PAENUT_COUNT = strIONR_PAENUT_COUNT.Split('|');

                    if (arrIONR_PAENUT_COUNT.Length > 0)
                    {

                        txtPaenut1.Text = arrIONR_PAENUT_COUNT[0];
                        if (arrIONR_PAENUT_COUNT.Length > 0)
                        {
                            txtPaenut2.Text = arrIONR_PAENUT_COUNT[1];
                        }

                    }


                    string strIONR_NEEDLE_COUNT = Convert.ToString(DR["IONR_NEEDLE_COUNT"]);

                    string[] arrIONR_NEEDLE_COUNT = strIONR_NEEDLE_COUNT.Split('|');

                    if (arrIONR_NEEDLE_COUNT.Length > 0)
                    {

                        txtNeedle1.Text = arrIONR_NEEDLE_COUNT[0];
                        if (arrIONR_NEEDLE_COUNT.Length > 1)
                        {
                            txtNeedle2.Text = arrIONR_NEEDLE_COUNT[1];
                        }

                    }

                    string strIONR_INSTRUMENTS_COUNT = Convert.ToString(DR["IONR_INSTRUMENTS_COUNT"]);

                    string[] arrIONR_INSTRUMENTS_COUNT = strIONR_INSTRUMENTS_COUNT.Split('|');

                    if (arrIONR_INSTRUMENTS_COUNT.Length > 0)
                    {

                        txtInstruments1.Text = arrIONR_INSTRUMENTS_COUNT[0];
                        if (arrIONR_INSTRUMENTS_COUNT.Length > 1)
                        {
                            txtInstruments2.Text = arrIONR_INSTRUMENTS_COUNT[1];
                        }

                    }

                    string strIONR_COTTON_BALLS_COUNT = Convert.ToString(DR["IONR_COTTON_BALLS_COUNT"]);

                    string[] arrIONR_COTTON_BALLS_COUNT = strIONR_COTTON_BALLS_COUNT.Split('|');

                    if (arrIONR_COTTON_BALLS_COUNT.Length > 0)
                    {

                        txtCottonBalls1.Text = arrIONR_COTTON_BALLS_COUNT[0];
                        if (arrIONR_COTTON_BALLS_COUNT.Length > 1)
                        {
                            txtCottonBalls2.Text = arrIONR_COTTON_BALLS_COUNT[1];
                        }

                    }



                    string strIONR_COUNT_CORRECT = Convert.ToString(DR["IONR_COUNT_CORRECT"]);
                    if (strIONR_COUNT_CORRECT == chkCountCorYes.Value)
                    {
                        chkCountCorYes.Checked = true;
                    }
                    string strIONR_COUNT_CORRECT_SURG_INF = Convert.ToString(DR["IONR_COUNT_CORRECT_SURG_INF"]);
                    if (strIONR_COUNT_CORRECT_SURG_INF == chkCountCorSurgeonInformed.Value)
                    {
                        chkCountCorSurgeonInformed.Checked = true;
                    }


                    string strIONR_COUNT_INCORRECT = Convert.ToString(DR["IONR_COUNT_INCORRECT"]);
                    if (strIONR_COUNT_INCORRECT == chkCountincorYes.Value)
                    {
                        chkCountincorYes.Checked = true;
                    }
                    string strIONR_COUNT_INCORRECT_SURG_INF = Convert.ToString(DR["IONR_COUNT_CORRECT_SURG_INF"]);
                    if (strIONR_COUNT_INCORRECT_SURG_INF == chkCountincorSurgeonInformed.Value)
                    {
                        chkCountincorSurgeonInformed.Checked = true;
                    }


                    string strIONR_CORRECTIVE_ACTION = Convert.ToString(DR["IONR_CORRECTIVE_ACTION"]);
                    if (strIONR_CORRECTIVE_ACTION == radCorrectiveActionTaken.Value)
                    {
                        radCorrectiveActionTaken.Checked = true;
                    }

                    txtCorrectiveComment.Text = Convert.ToString(DR["IONR_CORRECTIVE_COMMENT"]);


                    string strIONR_SPECIMEN = Convert.ToString(DR["IONR_SPECIMEN"]);

                    if (strIONR_SPECIMEN == radpecimenYes.Value)
                    {
                        radpecimenYes.Checked = true;
                    }
                    else if (strIONR_SPECIMEN == radSpecimenNo.Value)
                    {
                        radSpecimenNo.Checked = true;
                    }


                    drpSpecimenType.SelectedValue = Convert.ToString(DR["IONR_SPECIMEN_TYPE"]);
                    drpPatientTransTo.SelectedValue = Convert.ToString(DR["IONR_PATIENT_TRANS_TO"]);


                    drpPosition.SelectedValue = Convert.ToString(DR["IONR_POSITION"]);
                    drpPositionalAide.SelectedValue = Convert.ToString(DR["IONR_POSITIONAL_AIDE"]);


                    string strIONR_SAFETY_STRAP_ON = Convert.ToString(DR["IONR_SAFETY_STRAP_ON"]);

                    if (strIONR_SAFETY_STRAP_ON == radSafetyStrapYes.Value)
                    {
                        radSafetyStrapYes.Checked = true;
                    }
                    else if (strIONR_SAFETY_STRAP_ON == radSafetyStrapNo.Value)
                    {
                        radSafetyStrapNo.Checked = true;
                    }

                    string strIONR_SAFETY_STRAP_DESC = Convert.ToString(DR["IONR_SAFETY_STRAP_DESC"]);

                    string[] arrIONR_SAFETY_STRAP_DESC = strIONR_SAFETY_STRAP_DESC.Split('|');

                    if (arrIONR_SAFETY_STRAP_DESC.Length > 0)
                    {
                        if (arrIONR_SAFETY_STRAP_DESC[0] == chkLeftArmStraps.Value)
                        {
                            chkLeftArmStraps.Checked = true;
                        }
                        if (arrIONR_SAFETY_STRAP_DESC[0] == chkRightArmStraps.Value)
                        {
                            chkRightArmStraps.Checked = true;
                        }
                        if (arrIONR_SAFETY_STRAP_DESC.Length > 1)
                        {
                            if (arrIONR_SAFETY_STRAP_DESC[1] == chkRightArmStraps.Value)
                            {
                                chkRightArmStraps.Checked = true;
                            }
                        }
                    }



                    drpSafetyStrapPositionedBy.SelectedValue = Convert.ToString(DR["IONR_POSITIONED_BY"]);

                    string strIONR_ELECTROSURGICAL_UNIT = Convert.ToString(DR["IONR_ELECTROSURGICAL_UNIT"]);

                    if (strIONR_ELECTROSURGICAL_UNIT == radElectroUnitYes.Value)
                    {
                        radElectroUnitYes.Checked = true;
                    }
                    else if (strIONR_ELECTROSURGICAL_UNIT == radElectroUnitNo.Value)
                    {
                        radElectroUnitNo.Checked = true;
                    }




                    txtRetElectroLocation.Text = Convert.ToString(DR["IONR_RET_ELECTRODE_LOCATION"]);
                    txtSettingsCut.Text = Convert.ToString(DR["IONR_ELECT_SURG_CUT"]);
                    txtSettingsCoag.Text = Convert.ToString(DR["IONR_ELECT_SURG_COAG"]);
                    txtBipolar.Text = Convert.ToString(DR["IONR_ELECT_BIPOLAR"]);
                    drpElectroUnitAppliedBy.SelectedValue = Convert.ToString(DR["IONR_ELECT_APPLIEDBY"]);


                    txtSkinCondition.Text = Convert.ToString(DR["IONR_SKIN_COND_UNDER_PAD"]);


                    txtTourniTimeUpDate.Text = Convert.ToString(DR["IONR_TOURNIQUETS_TIME_UP_Desc"]);

                    string strTimeUp = Convert.ToString(DR["IONR_TOURNIQUETS_TIME_UPTime"]);

                    string[] arrTimeUp = strTimeUp.Split(':');
                    if (arrTimeUp.Length > 0)
                    {
                        drpourniTimeUpHour.SelectedValue = arrTimeUp[0];
                        if (arrTimeUp.Length > 0)
                        {
                            drpourniTimeUpMin.SelectedValue = arrTimeUp[1];
                        }

                    }

                    txtTourniTimeDownDate.Text = Convert.ToString(DR["IONR_TOURNIQUETS_TIME_DOWNDesc"]);

                    string strTimeDown = Convert.ToString(DR["IONR_TOURNIQUETS_TIME_DOWNTime"]);

                    string[] arrTimeDown = strTimeDown.Split(':');
                    if (arrTimeDown.Length > 0)
                    {
                        drpourniTimeDownHour.SelectedValue = arrTimeDown[0];
                        if (arrTimeDown.Length > 1)
                        {
                            drpourniTimeDownMin.SelectedValue = arrTimeDown[1];
                        }

                    }


                    txtSkinStatusBeforePlacement.Text = Convert.ToString(DR["IONR_SKIN_STATUS_BE_PLACEMENT"]);
                    txtSkinStatusAfterPlacement.Text = Convert.ToString(DR["IONR_SKIN_STATUS_AF_PLACEMENT"]);
                    txtSiteLocation.Text = Convert.ToString(DR["IONR_SKIN_SITE_LOCATION"]);
                    drpSkinAppliedBy.SelectedValue = Convert.ToString(DR["IONR_SKIN_APPLIED_BY"]);




                    txtUriCatheterFolyCathno.Text = Convert.ToString(DR["IONR_FOLY_CATH_NO"]);
                    drpUriCatheterInsertedBy.SelectedValue = Convert.ToString(DR["IONR_URIN_CAT_INSERTEDBY"]);
                    drpUrineReturned.SelectedValue = Convert.ToString(DR["IONR_URINE_RETURNED"]);
                    drpPreAnesthesiologist.SelectedValue = Convert.ToString(DR["IONR_PER_ANESTHESIOLOGIST"]);
                    txtTotalBloodProd.Text = Convert.ToString(DR["IONR_TOTAL_BLOOD_PROD"]);
                    txtTotalIVFluids.Text = Convert.ToString(DR["IONR_TOTAL_IV_FLUIDS"]);
                    txtUrinaryDrainage.Text = Convert.ToString(DR["IONR_URINARY_DRAINAGE"]);
                    txtEstBloodLoss.Text = Convert.ToString(DR["IONR_ESTI_BLOOD_LOSS"]);
                    txtOutPutOthers.Text = Convert.ToString(DR["IONR_OUTPUT_OTHERS"]);


                    txtProsthesisLocation.Text = Convert.ToString(DR["IONR_PROSTHESIS_LOCA"]);
                    txtProsthesisSize.Text = Convert.ToString(DR["IONR_PROSTHESIS_SIZE"]);
                    txtProsthesisSlNo.Text = Convert.ToString(DR["IONR_PROSTHESIS_SL_NO"]);
                    txtCarmManufacturer.Text = Convert.ToString(DR["IONR_PROSTHESIS_MANUFA"]);

                    string strIONR_XRAY_CARAM = Convert.ToString(DR["IONR_XRAY_CARAM"]);


                    if (strIONR_XRAY_CARAM == radCarmYes.Value)
                    {
                        radCarmYes.Checked = true;
                    }
                    else if (strIONR_XRAY_CARAM == radCarmNo.Value)
                    {
                        radCarmNo.Checked = true;
                    }
                    else if (strIONR_XRAY_CARAM == radCarmNA.Value)
                    {
                        radCarmNA.Checked = true;
                    }


                    txtLaser.Text = Convert.ToString(DR["IONR_XRAY_LASER"]);
                    txtOthersMicroscope.Text = Convert.ToString(DR["IONR_MICROSCOPE"]);
                    txtOthersOthers.Text = Convert.ToString(DR["IONR_OTHER"]);
                    txtComment.Text = Convert.ToString(DR["IONR_COMMENT"]);

                    drpScrubNurse.SelectedValue = Convert.ToString(DR["IONR_SCRUB_NURSE"]);


                    txtScrubNurseDate.Text = Convert.ToString(DR["IONR_SCRUB_NURSE_DATEDesc"]);
                    string strScrubNurseDate = Convert.ToString(DR["IONR_SCRUB_NURSE_DATETime"]);

                    string[] arrScrubNurseDate = strScrubNurseDate.Split(':');
                    if (arrScrubNurseDate.Length > 1)
                    {
                        drpScrubNurseHour.SelectedValue = arrScrubNurseDate[0];
                        drpScrubNurseMin.SelectedValue = arrScrubNurseDate[1];

                    }




                    drpCirculNurse.SelectedValue = Convert.ToString(DR["IONR_CIRCUL_NURSE"]);

                    txtCirculNurseDate.Text = Convert.ToString(DR["IONR_CIRCUL_NURSE_DATEDesc"]);
                    string strCirculNurseDate = Convert.ToString(DR["IONR_CIRCUL_NURSE_DATETime"]);

                    string[] arrCirculNurseDate = strCirculNurseDate.Split(':');
                    if (arrCirculNurseDate.Length > 1)
                    {
                        drpCirculNurseHour.SelectedValue = arrCirculNurseDate[0];
                        drpCirculNurseMin.SelectedValue = arrCirculNurseDate[1];

                    }




                    drpReScrubNurse.SelectedValue = Convert.ToString(DR["IONR_REL_SCRUB_NURSE"]);

                    txtReScrubNurseDate.Text = Convert.ToString(DR["IONR_REL_SCRUB_NURSE_DATEDesc"]);
                    string strReScrubNurseDate = Convert.ToString(DR["IONR_REL_SCRUB_NURSE_DATETime"]);

                    string[] arrReScrubNurseDate = strReScrubNurseDate.Split(':');
                    if (arrReScrubNurseDate.Length > 1)
                    {
                        drpReScrubNurseHour.SelectedValue = arrReScrubNurseDate[0];
                        drpReScrubNurseMin.SelectedValue = arrReScrubNurseDate[1];

                    }


                    drpReCirculNurse.SelectedValue = Convert.ToString(DR["IONR_REL_CIRCUL_NURSE"]);

                    txtReCirculNurseDate.Text = Convert.ToString(DR["IONR_REL_CIRCUL_NURSE_DATEDesc"]);
                    string strReCirculNurseDate = Convert.ToString(DR["IONR_REL_CIRCUL_NURSE_DATETime"]);

                    string[] arrReCirculNurseDate = strReCirculNurseDate.Split(':');
                    if (arrReCirculNurseDate.Length > 1)
                    {
                        drpReCirculNurseHour.SelectedValue = arrReCirculNurseDate[0];
                        drpReCirculNurseMin.SelectedValue = arrReCirculNurseDate[1];

                    }


                }
            }

        }

        void BindCaseClasification()
        {
            drpCaseClasification.Items.Clear();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='CASE_CLASIFICATION' ";

            DataSet DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpCaseClasification.DataSource = DS;
                drpCaseClasification.DataTextField = "EM_NAME";
                drpCaseClasification.DataValueField = "EM_CODE";
                drpCaseClasification.DataBind();
            }

            drpCaseClasification.Items.Insert(0, "--- Select ---");
            drpCaseClasification.Items[0].Value = "";
        }

        void BindCaseType()
        {
            drpCaseType.Items.Clear();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='CASE_TYPE' ";

            DataSet DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpCaseType.DataSource = DS;
                drpCaseType.DataTextField = "EM_NAME";
                drpCaseType.DataValueField = "EM_CODE";
                drpCaseType.DataBind();
            }

            drpCaseType.Items.Insert(0, "--- Select ---");
            drpCaseType.Items[0].Value = "";
        }

        void BindSkinCondition()
        {
            drpSkinCondition.Items.Clear();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='SKIN_CONDITION' ";

            DataSet DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpSkinCondition.DataSource = DS;
                drpSkinCondition.DataTextField = "EM_NAME";
                drpSkinCondition.DataValueField = "EM_CODE";
                drpSkinCondition.DataBind();
            }

            drpSkinCondition.Items.Insert(0, "--- Select ---");
            drpSkinCondition.Items[0].Value = "";
        }

        void BindSpecimentType()
        {
            drpSpecimenType.Items.Clear();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='SPECIMEN_TYPE' ";

            DataSet DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpSpecimenType.DataSource = DS;
                drpSpecimenType.DataTextField = "EM_NAME";
                drpSpecimenType.DataValueField = "EM_CODE";
                drpSpecimenType.DataBind();
            }

            drpSpecimenType.Items.Insert(0, "--- Select ---");
            drpSpecimenType.Items[0].Value = "";
        }

        void BindPTTransferTo()
        {
            drpPatientTransTo.Items.Clear();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='PT_TRANSFER_TO' ";

            DataSet DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpPatientTransTo.DataSource = DS;
                drpPatientTransTo.DataTextField = "EM_NAME";
                drpPatientTransTo.DataValueField = "EM_CODE";
                drpPatientTransTo.DataBind();
            }

            drpPatientTransTo.Items.Insert(0, "--- Select ---");
            drpPatientTransTo.Items[0].Value = "";
        }
        void BindInjuryPosition()
        {
            drpPosition.Items.Clear();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='INJURY_POSITION' ";

            DataSet DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpPosition.DataSource = DS;
                drpPosition.DataTextField = "EM_NAME";
                drpPosition.DataValueField = "EM_CODE";
                drpPosition.DataBind();
            }

            drpPosition.Items.Insert(0, "--- Select ---");
            drpPosition.Items[0].Value = "";
        }

        void BindPositionAids()
        {
            drpPositionalAide.Items.Clear();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='POSITIONAL_AIDE' ";

            DataSet DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpPositionalAide.DataSource = DS;
                drpPositionalAide.DataTextField = "EM_NAME";
                drpPositionalAide.DataValueField = "EM_CODE";
                drpPositionalAide.DataBind();
            }

            drpPositionalAide.Items.Insert(0, "--- Select ---");
            drpPositionalAide.Items[0].Value = "";
        }

        void BindUrineReturnedType()
        {
            drpUrineReturned.Items.Clear();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='URINE_RETURNED' ";

            DataSet DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpUrineReturned.DataSource = DS;
                drpUrineReturned.DataTextField = "EM_NAME";
                drpUrineReturned.DataValueField = "EM_CODE";
                drpUrineReturned.DataBind();
            }

            drpUrineReturned.Items.Insert(0, "--- Select ---");
            drpUrineReturned.Items[0].Value = "";
        }

        Boolean CheckPassword(string UserID, string Password)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_REMARK ='" + UserID + "' AND   HUM_USER_PASS= '" + Password + "'";

            DataSet ds = new DataSet();
            //ds = dbo.UserMasterGet(drpBranch.SelectedValue, drpUsers.SelectedValue , txtPassword.Text);
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_INT_OPR_NURSE_REC' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Visible = false;
                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSave.Visible = false;
                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }


        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }
                try
                {
                    this.Page.Title = "IntraOperativeNursingRecord";

                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtInterOperDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtTourniTimeUpDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtTourniTimeDownDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    txtScrubNurseDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtCirculNurseDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    txtReScrubNurseDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtReCirculNurseDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    BindTime();
                    BindStaff();
                    BindNurse();

                    BindCaseClasification();
                    BindCaseType();
                    BindSkinCondition();
                    BindSpecimentType();
                    BindPTTransferTo();
                    BindInjuryPosition();
                    BindPositionAids();
                    BindUrineReturnedType();

                    BindData();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }

        }


        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (drpScrubNurse.SelectedIndex != 0)
                {
                    if (CheckPassword(drpScrubNurse.SelectedValue, txtScrubNursePass.Text.Trim()) == false)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Scrub Nurse Password wrong')", true);
                        goto FunEnd;
                    }
                }
                if (drpCirculNurse.SelectedIndex != 0)
                {
                    if (CheckPassword(drpCirculNurse.SelectedValue, txtCirculNursePass.Text.Trim()) == false)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Circulatory Nurse Password wrong')", true);
                        goto FunEnd;
                    }
                }

                if (drpReScrubNurse.SelectedIndex != 0)
                {
                    if (CheckPassword(drpReScrubNurse.SelectedValue, txtReScrubNursePass.Text.Trim()) == false)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage(' ReleiverScrub Nurse Password wrong')", true);
                        goto FunEnd;
                    }
                }

                if (drpReCirculNurse.SelectedIndex != 0)
                {
                    if (CheckPassword(drpReCirculNurse.SelectedValue, txtReCirculNursePass.Text.Trim()) == false)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage(' Releiver Circulatory Nurse Password wrong')", true);
                        goto FunEnd;
                    }
                }


                IP_IntraOperativeNursingRecord objIONS = new IP_IntraOperativeNursingRecord();

                objIONS.BranchID = Convert.ToString(Session["Branch_ID"]);
                objIONS.EMRID = Convert.ToString(Session["EMR_ID"]);
                objIONS.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                objIONS.IONR_DATE = txtInterOperDate.Text.Trim() + " " + drpInterOperHour.SelectedValue + ":" + drpInterOperMin.SelectedValue + ":00";
                objIONS.IONR_PRE_OPER_DIAG = txtPreOperDiag.Text.Trim();
                objIONS.IONR_POST_OPER_DIAG = txtPostOperDiag.Text.Trim();
                objIONS.IONR_OPER_PROCEDURE = txtOperProcPerf.Text.Trim();

                objIONS.IONR_SURGEN = drpSurgeon.SelectedValue;
                objIONS.IONR_ANAESTHETIST = drpAnaesthetist.SelectedValue;
                objIONS.IONR_ANESTHESIA_TYPE = drpAnesType.SelectedValue;
                objIONS.IONR_ANESTHESIA_CLASS = drpAnesClassifi.SelectedValue;


                objIONS.IONR_PT_IDENTIFI = radInfoMedicFood.SelectedValue;
                if (radAllergiesYes.Checked == true)
                {
                    objIONS.IONR_ALLERGIES = radAllergiesYes.Value + "|" + txtAlergies.Value;

                }
                else if (radAllergiesNo.Checked == true)
                {
                    objIONS.IONR_ALLERGIES = radAllergiesNo.Value + "|" + txtAlergies.Value;

                }


                if (radNPOYes.Checked == true)
                {
                    objIONS.IONR_NPO = radNPOYes.Value + "|" + txtNPO.Value;

                }
                else if (radNPONo.Checked == true)
                {
                    objIONS.IONR_NPO = radNPONo.Value + "|" + txtNPO.Value;

                }
                if (radOperConYes.Checked == true)
                {
                    objIONS.IONR_OPER_CONSENT = radOperConYes.Value + "|" + txtOperCon.Value;

                }
                else if (radOperConNo.Checked == true)
                {
                    objIONS.IONR_OPER_CONSENT = radOperConNo.Value + "|" + txtOperCon.Value;

                }

                if (radTimeOutYes.Checked == true)
                {
                    objIONS.IONR_TIME_OUT = radTimeOutYes.Value + "|" + txtTimeOut.Value;

                }
                else if (radTimeOutNo.Checked == true)
                {
                    objIONS.IONR_TIME_OUT = radTimeOutNo.Value + "|" + txtTimeOut.Value;

                }


                if (radVarifOperCheckList.Checked == true)
                {
                    objIONS.IONR_VARIFI_OPER_CHECKLIST = radVarifOperCheckList.Value;

                }



                objIONS.IONR_ASSESS_COMMENT = txtAssComment.Text;
                objIONS.IONR_CASE_CLASSIFI = drpCaseClasification.SelectedValue;

                objIONS.IONR_CASE_TYPE = drpCaseType.SelectedValue;
                objIONS.IONR_INTUBATION_TIME = txtIntubationTime.Text;
                objIONS.IONR_NCISION_TIME = txtIncisionTime.Text;
                objIONS.IONR_EXTUBATION_TIME = txtExtubationTime.Text;


                if (radHairRemovalYes.Checked == true)
                {
                    objIONS.IONR_HAIR_REMOVAL = radHairRemovalYes.Value;

                }
                else if (radHairRemovalNo.Checked == true)
                {
                    objIONS.IONR_HAIR_REMOVAL = radHairRemovalNo.Value;

                }
                else if (radClipped.Checked == true)
                {
                    objIONS.IONR_HAIR_REMOVAL = radClipped.Value;

                }

                objIONS.IONR_DRAINS_PACKING = txtDrainspacking.Text;
                objIONS.IONR_DRAINS_LOCATION = txtDrainsLocation.Text;


                if (radClipperYes.Checked == true)
                {
                    objIONS.IONR_CLIPPER = radClipperYes.Value;

                }
                else if (radClipperNo.Checked == true)
                {
                    objIONS.IONR_CLIPPER = radClipperNo.Value;

                }


                objIONS.IONR_SKIN_CONDITION = drpSkinCondition.SelectedValue;
                objIONS.IONR_DRESSING = txtDressing.Text;
                objIONS.IONR_SPONGE_COUNT = txtSponge1.Text + "|" + txtSponge2.Text;
                objIONS.IONR_GAUZE_COUNT = txtGauze1.Text + "|" + txtGauze2.Text;

                objIONS.IONR_PAENUT_COUNT = txtPaenut1.Text + "|" + txtPaenut2.Text;
                objIONS.IONR_NEEDLE_COUNT = txtNeedle1.Text + "|" + txtNeedle2.Text;
                objIONS.IONR_INSTRUMENTS_COUNT = txtInstruments1.Text + "|" + txtInstruments2.Text;
                objIONS.IONR_COTTON_BALLS_COUNT = txtCottonBalls1.Text + "|" + txtCottonBalls2.Text;


                if (chkCountCorYes.Checked == true)
                {
                    objIONS.IONR_COUNT_CORRECT = chkCountCorYes.Value;

                }
                if (chkCountCorSurgeonInformed.Checked == true)
                {
                    objIONS.IONR_COUNT_CORRECT_SURG_INF = chkCountCorSurgeonInformed.Value;

                }



                if (chkCountincorYes.Checked == true)
                {
                    objIONS.IONR_COUNT_INCORRECT = chkCountincorYes.Value;

                }
                if (chkCountincorSurgeonInformed.Checked == true)
                {
                    objIONS.IONR_COUNT_INCORRECT_SURG_INF = chkCountincorSurgeonInformed.Value;

                }



                objIONS.IONR_CORRECTIVE_ACTION = radCorrectiveActionTaken.Value;
                objIONS.IONR_CORRECTIVE_COMMENT = txtCorrectiveComment.Text;

                if (radpecimenYes.Checked == true)
                {
                    objIONS.IONR_SPECIMEN = radpecimenYes.Value;
                }
                else if (radSpecimenNo.Checked == true)
                {
                    objIONS.IONR_SPECIMEN = radSpecimenNo.Value;
                }
                objIONS.IONR_SPECIMEN_TYPE = drpSpecimenType.SelectedValue;
                objIONS.IONR_PATIENT_TRANS_TO = drpPatientTransTo.SelectedValue;



                objIONS.IONR_POSITION = drpPosition.SelectedValue;
                objIONS.IONR_POSITIONAL_AIDE = drpPositionalAide.SelectedValue;
                if (radSafetyStrapYes.Checked == true)
                {
                    objIONS.IONR_SAFETY_STRAP_ON = radSafetyStrapYes.Value;
                }
                else if (radSafetyStrapNo.Checked == true)
                {
                    objIONS.IONR_SAFETY_STRAP_ON = radSafetyStrapNo.Value;
                }


                if (chkLeftArmStraps.Checked == true)
                {
                    objIONS.IONR_SAFETY_STRAP_DESC = chkLeftArmStraps.Value;
                }
                if (chkRightArmStraps.Checked == true)
                {
                    objIONS.IONR_SAFETY_STRAP_DESC = chkRightArmStraps.Value;
                }
                if (chkLeftArmStraps.Checked == true && chkRightArmStraps.Checked == true)
                {

                    objIONS.IONR_SAFETY_STRAP_DESC = chkLeftArmStraps.Value + "|" + chkRightArmStraps.Value;
                }

                objIONS.IONR_POSITIONED_BY = drpSafetyStrapPositionedBy.SelectedValue;


                if (radElectroUnitYes.Checked == true)
                {
                    objIONS.IONR_ELECTROSURGICAL_UNIT = radElectroUnitYes.Value;
                }
                else if (radElectroUnitNo.Checked == true)
                {
                    objIONS.IONR_ELECTROSURGICAL_UNIT = radElectroUnitNo.Value;
                }


                objIONS.IONR_RET_ELECTRODE_LOCATION = txtRetElectroLocation.Text;
                objIONS.IONR_ELECT_SURG_CUT = txtSettingsCut.Text;
                objIONS.IONR_ELECT_SURG_COAG = txtSettingsCoag.Text;
                objIONS.IONR_ELECT_BIPOLAR = txtBipolar.Text;
                objIONS.IONR_ELECT_APPLIEDBY = drpElectroUnitAppliedBy.SelectedValue;



                objIONS.IONR_SKIN_COND_UNDER_PAD = txtSkinCondition.Text;
                objIONS.IONR_TOURNIQUETS_TIME_UP = txtTourniTimeUpDate.Text + " " + drpourniTimeUpHour.SelectedValue + ":" + drpourniTimeUpMin.SelectedValue + ":00";
                objIONS.IONR_TOURNIQUETS_TIME_DOWN = txtTourniTimeDownDate.Text + " " + drpourniTimeDownHour.SelectedValue + ":" + drpourniTimeDownMin.SelectedValue + ":00";
                objIONS.IONR_SKIN_STATUS_BE_PLACEMENT = txtSkinStatusBeforePlacement.Text;
                objIONS.IONR_SKIN_STATUS_AF_PLACEMENT = txtSkinStatusAfterPlacement.Text;
                objIONS.IONR_SKIN_SITE_LOCATION = txtSiteLocation.Text;
                objIONS.IONR_SKIN_APPLIED_BY = drpSkinAppliedBy.SelectedValue;




                objIONS.IONR_FOLY_CATH_NO = txtUriCatheterFolyCathno.Text;
                objIONS.IONR_URIN_CAT_INSERTEDBY = drpUriCatheterInsertedBy.SelectedValue;
                objIONS.IONR_URINE_RETURNED = drpUrineReturned.SelectedValue;
                objIONS.IONR_PER_ANESTHESIOLOGIST = drpPreAnesthesiologist.SelectedValue;
                objIONS.IONR_TOTAL_BLOOD_PROD = txtTotalBloodProd.Text;
                objIONS.IONR_TOTAL_IV_FLUIDS = txtTotalIVFluids.Text;
                objIONS.IONR_URINARY_DRAINAGE = txtUrinaryDrainage.Text;
                objIONS.IONR_ESTI_BLOOD_LOSS = txtEstBloodLoss.Text;
                objIONS.IONR_OUTPUT_OTHERS = txtOutPutOthers.Text;




                objIONS.IONR_PROSTHESIS_LOCA = txtProsthesisLocation.Text;
                objIONS.IONR_PROSTHESIS_SIZE = txtProsthesisSize.Text;
                objIONS.IONR_PROSTHESIS_SL_NO = txtProsthesisSlNo.Text;
                objIONS.IONR_PROSTHESIS_MANUFA = txtCarmManufacturer.Text;

                if (radCarmYes.Checked == true)
                {
                    objIONS.IONR_XRAY_CARAM = radCarmYes.Value;
                }
                else if (radCarmNo.Checked == true)
                {
                    objIONS.IONR_XRAY_CARAM = radCarmNo.Value;
                }
                else if (radCarmNA.Checked == true)
                {
                    objIONS.IONR_XRAY_CARAM = radCarmNA.Value;
                }
                objIONS.IONR_XRAY_LASER = txtLaser.Text;
                objIONS.IONR_MICROSCOPE = txtOthersMicroscope.Text;
                objIONS.IONR_OTHER = txtOthersOthers.Text;
                objIONS.IONR_COMMENT = txtComment.Text;



                objIONS.IONR_SCRUB_NURSE = drpScrubNurse.SelectedValue;
                objIONS.IONR_SCRUB_NURSE_DATE = txtScrubNurseDate.Text + " " + drpScrubNurseHour.SelectedValue + ":" + drpScrubNurseMin.SelectedValue + ":00";
                objIONS.IONR_CIRCUL_NURSE = drpCirculNurse.SelectedValue;
                objIONS.IONR_CIRCUL_NURSE_DATE = txtCirculNurseDate.Text + " " + drpCirculNurseHour.SelectedValue + ":" + drpCirculNurseMin.SelectedValue + ":00";
                objIONS.IONR_REL_SCRUB_NURSE = drpReScrubNurse.SelectedValue;
                objIONS.IONR_REL_SCRUB_NURSE_DATE = txtReScrubNurseDate.Text + " " + drpReScrubNurseHour.SelectedValue + ":" + drpReScrubNurseMin.SelectedValue + ":00";
                objIONS.IONR_REL_CIRCUL_NURSE = drpReCirculNurse.SelectedValue;
                objIONS.IONR_REL_CIRCUL_NURSE_DATE = txtReCirculNurseDate.Text + " " + drpReCirculNurseHour.SelectedValue + ":" + drpReCirculNurseMin.SelectedValue + ":00";
                objIONS.UserID = Convert.ToString(Session["User_ID"]);


                objIONS.InterOperNursRecordAdd();

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

    }
}