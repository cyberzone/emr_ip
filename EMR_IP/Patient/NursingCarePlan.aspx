﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="NursingCarePlan.aspx.cs" Inherits="EMR_IP.Patient.NursingCarePlan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }


    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />

    <h3 class="box-title">Nursing Care Plan</h3>

    <br />
    <br />
    <br />
    <br />
    <br />
    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
             Saved Successfully 
                
        </div>
    </div>

    <table style="width: 100%">
        <tr>
            <td class="lblCaption1" style="width:120px;">Date & Time
            </td>
            <td colspan="3">
                <asp:UpdatePanel ID="updatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtAssDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender1" runat="server"
                            Enabled="True" TargetControlID="txtAssDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:DropDownList ID="drpFrmHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                        <asp:DropDownList ID="drpFrmlMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
            </tr>
        <tr>
            <td class="lblCaption1" style="width:120px;">Assessment
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtAssessment" runat="server" Width="99%" Height="50px" CssClass="label" TextMode="MultiLine" style="resize:none;"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        
            <td class="lblCaption1" style="width:120px;">Nursing Diagnosis
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel4" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtDiagnosis" runat="server" Width="99%" Height="50px" CssClass="label" TextMode="MultiLine" style="resize:none;"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
              </tr>
        <tr>
            <td class="lblCaption1">Implementation
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel5" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtImplementation" runat="server" Width="99%" Height="50px" CssClass="label" TextMode="MultiLine" style="resize:none;"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>

       
            <td class="lblCaption1">Evaluation
            </td>
            <td>
                <asp:UpdatePanel ID="updatePanel6" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtEvaluation" runat="server" Width="99%" Height="50px" CssClass="label" TextMode="MultiLine" style="resize:none;"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:UpdatePanel runat="server" ID="updatePanel7">
                    <ContentTemplate>
                        <asp:Button ID="btnAdd" runat="server" CssClass="orange" Width="50px" Text="Add" OnClick="btnAdd_Click" Style="width: 50px; border-radius: 5px 5px 5px 5px" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <div style="padding-top: 0px; width: 100%; height: 330px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
        <asp:UpdatePanel ID="updatePanel24" runat="server">
            <ContentTemplate>

                <asp:GridView ID="gvNursing" runat="server" AutoGenerateColumns="False"
                    EnableModelValidation="True" Width="100%" GridLines="none">
                    <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                    <RowStyle CssClass="GridRow" />
                    <Columns>
                        <asp:TemplateField HeaderText="Date & TIme">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkCode" runat="server" OnClick="Nursing_Click">
                                    <asp:Label ID="lblNursingID" CssClass="GridRow" runat="server" Text='<%# Bind("INCP_NURC_ID") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblgvAssDate" CssClass="GridRow" runat="server" Text='<%# Bind("INCP_DATEDesc") %>'></asp:Label>
                                    <asp:Label ID="lblgvAssTime" CssClass="GridRow" runat="server" Text='<%# Bind("INCP_DATE_TIMEDesc") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Assessment">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnksys" runat="server" OnClick="Nursing_Click">
                                    <asp:Label ID="lblgvAssessment" CssClass="GridRow" runat="server" Text='<%# Bind("INCP_ASSESSMENT") %>'></asp:Label>

                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Nursing Diagnosis">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkdis" runat="server" OnClick="Nursing_Click">
                                    <asp:Label ID="lblgvDiagnosis" CssClass="GridRow" runat="server" Text='<%# Bind("INCP_DIAGNOSIS") %>'></asp:Label>

                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Implementation">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPul" runat="server" OnClick="Nursing_Click">
                                    <asp:Label ID="lblgvImplementation" CssClass="GridRow" runat="server" Text='<%# Bind("INCP_IMPLEMENTATION") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Evaluation">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkspo" runat="server" OnClick="Nursing_Click">
                                    <asp:Label ID="lblgvEvaluation" CssClass="GridRow" runat="server" Text='<%# Bind("INCP_EVALUATION") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>


                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
</asp:Content>
