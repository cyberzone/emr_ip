﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.Patient
{
    public partial class NursingCarePlan : System.Web.UI.Page
    {

        IP_NursingCarePlan objNurs = new IP_NursingCarePlan();


        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpFrmHour.DataSource = DS;
                drpFrmHour.DataTextField = "Name";
                drpFrmHour.DataValueField = "Code";
                drpFrmHour.DataBind();
            }

            drpFrmHour.Items.Insert(0, "00");
            drpFrmHour.Items[0].Value = "00";


            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpFrmlMin.DataSource = DS;
                drpFrmlMin.DataTextField = "Name";
                drpFrmlMin.DataValueField = "Code";
                drpFrmlMin.DataBind();
            }

            drpFrmlMin.Items.Insert(0, "00");
            drpFrmlMin.Items[0].Value = "00";

             

        }

        void BindGrid()
        {
            DataSet DS = new DataSet();
            objNurs = new IP_NursingCarePlan();



            string Criteria = " 1=1 and INCP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND INCP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objNurs.NurseCarePlanGet(Criteria);


            gvNursing.DataBind();
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvNursing.DataSource = DS;
                gvNursing.DataBind();

            }

        }

        void Clear()
        {
            Int32 R = 0;
            if (Convert.ToString(ViewState["gvNursingSelectIndex"]) != "" && Convert.ToString(ViewState["gvNursingSelectIndex"]) != null)
            {
                R = Convert.ToInt32(ViewState["gvNursingSelectIndex"]);
                gvNursing.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
            }

            ViewState["INCP_NURC_ID"] = "";
            ViewState["gvNursingSelectIndex"] = "";


            CommonBAL objCom = new CommonBAL();
            string strDate = "", strTime = ""; ;
            strDate = objCom.fnGetDate("dd/MM/yyyy");
            strTime = objCom.fnGetDate("hh:mm:ss");
            txtAssDate.Text = strDate;

            drpFrmHour.SelectedIndex = 0;
            drpFrmlMin.SelectedIndex = 0;

            txtAssessment.Text = "";
            txtDiagnosis.Text = "";
            txtImplementation.Text = "";
            txtEvaluation.Text = "";
            ViewState["INCP_NURC_ID"] = "";
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_NURSE_CAREPLAN' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnAdd.Visible = false;
                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnAdd.Visible = false;
                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }

                try
                {
                    this.Page.Title = "NursingCarePlan";
                    CommonBAL objCom = new CommonBAL();
                    string strDate = "", strTime = ""; ;
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("hh:mm:ss");

                    txtAssDate.Text = strDate;
                    BindTime();
                    BindGrid();

                    ViewState["INCP_NURC_ID"] = "";
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                objNurs = new IP_NursingCarePlan();
                objNurs.BranchID = Convert.ToString(Session["Branch_ID"]);
                objNurs.EMRID = Convert.ToString(Session["EMR_ID"]);
                objNurs.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                if (Convert.ToString(ViewState["INCP_NURC_ID"]) == "")
                {
                    objNurs.INCP_NURC_ID = "0";
                }
                else
                {
                    objNurs.INCP_NURC_ID = Convert.ToString(ViewState["INCP_NURC_ID"]);
                }
                objNurs.INCP_DATE = txtAssDate.Text.Trim() + " " + drpFrmHour.SelectedValue + ":" + drpFrmlMin.SelectedValue + ":00";
                objNurs.INCP_ASSESSMENT = txtAssessment.Text;
                objNurs.INCP_DIAGNOSIS = txtDiagnosis.Text;
                objNurs.INCP_IMPLEMENTATION = txtImplementation.Text;
                objNurs.INCP_EVALUATION = txtEvaluation.Text;
                objNurs.UserID = Convert.ToString(Session["User_ID"]);
                objNurs.NurseCarePlanAdd();
                Clear();
                BindGrid();
                //   ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);



            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void Nursing_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvNursingSelectIndex"] = gvScanCard.RowIndex;
                gvNursing.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblNursingID = (Label)gvScanCard.Cells[0].FindControl("lblNursingID");

                Label lblgvAssDate = (Label)gvScanCard.Cells[0].FindControl("lblgvAssDate");
                Label lblgvAssTime = (Label)gvScanCard.Cells[0].FindControl("lblgvAssTime");

                Label lblgvAssessment = (Label)gvScanCard.Cells[0].FindControl("lblgvAssessment");
                Label lblgvDiagnosis = (Label)gvScanCard.Cells[0].FindControl("lblgvDiagnosis");
                Label lblgvImplementation = (Label)gvScanCard.Cells[0].FindControl("lblgvImplementation");
                Label lblgvEvaluation = (Label)gvScanCard.Cells[0].FindControl("lblgvEvaluation");

                ViewState["INCP_NURC_ID"] = lblNursingID.Text;


                txtAssDate.Text = lblgvAssDate.Text;


                string strSTHour = lblgvAssTime.Text;

                string[] arrSTHour = strSTHour.Split(':');
                if (arrSTHour.Length > 1)
                {
                    drpFrmHour.SelectedValue = arrSTHour[0];
                    drpFrmlMin.SelectedValue = arrSTHour[1];

                }

                txtAssessment.Text = lblgvAssessment.Text;
                txtDiagnosis.Text = lblgvDiagnosis.Text;
                txtImplementation.Text = lblgvImplementation.Text;
                txtEvaluation.Text = lblgvEvaluation.Text;






            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "ReAssessment_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }
    }
}