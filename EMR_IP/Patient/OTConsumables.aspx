﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="OTConsumables.aspx.cs" Inherits="EMR_IP.Patient.OTConsumables" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>


    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }


    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }
    </script>

    <script language="javascript" type="text/javascript">



        function ServNameSelected() {
            if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }

        function DeleteConfirm() {
            var isDelCompany = window.confirm('Do you want to Delete This Data ?');

            if (isDelCompany == true) {
                return true
            }
            else {
                return false;
            }
            return true
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />
    <h3 class="box-title">OT Consumables</h3>
    <br />
    <br />
    <br />

    <table width="100%">
        <tr>
            <td class="lblCaption1">Description
            </td>
            <td colspan="3">
                <asp:UpdatePanel runat="server" ID="updatePanel19">
                    <ContentTemplate>

                        <asp:TextBox ID="txtServCode" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Enabled="false"></asp:TextBox>
                        <asp:TextBox ID="txtServName" runat="server" Width="99%" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()"></asp:TextBox>
                        <div id="divwidth" style="visibility: hidden;"></div>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtServCode" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                        </asp:AutoCompleteExtender>
                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtServName" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                        </asp:AutoCompleteExtender>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Quantity
            </td>
            <td>
                <asp:UpdatePanel runat="server" ID="updatePanel7">
                    <ContentTemplate>
                        <asp:TextBox ID="txtQty" runat="server" CssClass="label" Width="100px" Text="1" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
            <td class="lblCaption1">Price
            </td>
            <td>
                <asp:UpdatePanel runat="server" ID="updatePanel8">
                    <ContentTemplate>
                        <asp:TextBox ID="txtPrice" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>

 <td class="lblCaption1">Type
            </td>
            <td>
                <asp:UpdatePanel runat="server" ID="updatePanel9">
                    <ContentTemplate>
                        <asp:DropDownList ID="drpConsumablesType"  runat="server" CssClass="lblCaption1" Width="200px">
                            <asp:ListItem  Text="--- Select ---" Value=""></asp:ListItem>
                            <asp:ListItem  Text="Operation Fee" Value="Operation Fee"></asp:ListItem>
                            <asp:ListItem  Text="Anesthesia" Value="Anesthesia"></asp:ListItem>
                            <asp:ListItem  Text="OT Charges" Value="OT Charges"></asp:ListItem>
                            <asp:ListItem  Text="Consumables" Value="Consumables"></asp:ListItem>
                        </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>

        </tr>
        <tr>
            <td colspan="4">
                <asp:UpdatePanel runat="server" ID="updatePanel4">
                    <ContentTemplate>
                        <asp:Button ID="btnAdd" runat="server" CssClass="orange" Width="50px" Text="Add" OnClick="btnAdd_Click" Style="width: 50px; border-radius: 5px 5px 5px 5px" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <table style="width: 100%">
        <tr>
            <td>
                <div style="padding-top: 0px; width: 100%; height: 330px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                    <asp:UpdatePanel runat="server" ID="updatePanel5">
                        <ContentTemplate>
                            <asp:GridView ID="gvOTConsum" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                EnableModelValidation="True" GridLines="None" Width="100%">
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />


                                <Columns>
                                    <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="18" Width="18"
                                                OnClick="Deletee_Click" OnClientClick="return DeleteConfirm();" />&nbsp;&nbsp;
                                                
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkgvDesc" runat="server" OnClick="OTConSelect_Click">

                                                 <asp:Label ID="lblSOTransID" CssClass="label" Font-Size="11px"  runat="server" Text='<%# Bind("IPOTC_SO_TRANS_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblgvOTID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPOTC_OT_ID") %>' Visible="false"></asp:Label>

                                                <asp:Label ID="lblgvCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPOTC_OT_CODE") %>'></asp:Label>
                                                <br />
                                                <asp:Label ID="lblgvDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPOTC_OT_NAME") %>'></asp:Label>
                                            </asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkgvConsumableType" runat="server" OnClick="OTConSelect_Click">
                                                <asp:Label ID="lblgvConsumableType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPOTC_OT_TYPE") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Qty" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkgvQTY" runat="server" OnClick="OTConSelect_Click">
                                                <asp:Label ID="lblgvQTY" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPOTC_QTY") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Price" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" >
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkgvPrice" runat="server" OnClick="OTConSelect_Click">
                                                <asp:Label ID="lblgvPrice" CssClass="label" Font-Size="11px" style="text-align:right;padding-right:5px;width:100px;" runat="server" Text='<%# Bind("IPOTC_PRICE") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" >
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkgvAmount" runat="server" OnClick="OTConSelect_Click">
                                                <asp:Label ID="lblgvAmount" CssClass="label" Font-Size="11px" style="text-align:right;padding-right:5px;width:100px;" runat="server" Text='<%# Bind("IPOTC_AMOUNT") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </td>

        </tr>
        <tr>
            <td align="right" class="lblCaption1"  style="font-weight:bold;">
                  <asp:UpdatePanel runat="server" ID="updatePanel1">
                      <ContentTemplate>
                          Total Amount:&nbsp;
                          <asp:TextBox ID="txtTotalAmount" runat="server" CssClass="label" style="text-align:right;padding-right:5px;"  Width="100px" Font-Bold="true" onkeypress="return OnlyNumeric(event);" ReadOnly="true"></asp:TextBox>
                      </ContentTemplate>
                  </asp:UpdatePanel>
            </td>
           

        </tr>
    </table>
</asp:Content>
