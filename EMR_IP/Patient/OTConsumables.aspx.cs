﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;
namespace EMR_IP.Patient
{
    public partial class OTConsumables : System.Web.UI.Page
    {
        static string strSessionDeptId, strUserCode;

        IP_OTConsumables objOT = new IP_OTConsumables();

        DataSet DS = new DataSet();
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("OTConsumables." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindGrid()
        {

            objOT = new IP_OTConsumables();

            string Criteria = " 1=1 ";
            Criteria += " AND IPOTC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPOTC_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }

            DS = new DataSet();


            DS = objOT.OTConsumablesGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvOTConsum.DataSource = DS;
                gvOTConsum.DataBind();


                string strPermission = hidPermission.Value;
                if (strPermission == "1" || strPermission == "7")
                {
                    gvOTConsum.Columns[0].Visible = false;
                }
            }
            else
            {
                gvOTConsum.DataBind();
            }

            CalculateTotalAmount();
        FunEnd: ;


        }

        void Clear()
        {

            Int32 R = 0;
            if (Convert.ToString(ViewState["OTSelectIndex"]) != "" && Convert.ToString(ViewState["OTSelectIndex"]) != null)
            {
                R = Convert.ToInt32(ViewState["OTSelectIndex"]);
                gvOTConsum.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
            }
            ViewState["IPOTC_OT_ID"] = "";
            ViewState["OTSelectIndex"] = "";

            txtServCode.Text = "";
            txtServName.Text = "";
            if (drpConsumablesType.Items.Count > 0)
            {
                drpConsumablesType.SelectedIndex = 0;
            }

            txtQty.Text = "1";
            txtPrice.Text = "";
        }


        void CalculateTotalAmount()
        {
            Decimal decTotalAmount = 0;
            for (int intCurRow = 0; intCurRow < gvOTConsum.Rows.Count; intCurRow++)
            {
                Label lblgvAmount = (Label)gvOTConsum.Rows[intCurRow].FindControl("lblgvAmount");

                if (lblgvAmount.Text.Trim() != "")
                {
                    decTotalAmount += Convert.ToDecimal(lblgvAmount.Text);
                }
            }


            txtTotalAmount.Text = Convert.ToString(decTotalAmount);


        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_OT_CONSUMABLES' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnAdd.Visible = false;
                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnAdd.Visible = false;
                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }


        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetServicessList(string prefixText)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string[] Data;

            ds = dbo.HaadServicessListGet("OT", prefixText, "", strSessionDeptId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //  Data[i] = ds.Tables[0].Rows[i]["Code"].ToString() + "~" + ds.Tables[0].Rows[i]["ProductName"].ToString() + '~' + ds.Tables[0].Rows[i]["Description"].ToString();
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["Code"]).Trim() + "~" + Convert.ToString(ds.Tables[0].Rows[i]["Description"]).Trim();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion


        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            try
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }

                if (!IsPostBack)
                {
                    strSessionDeptId = Convert.ToString(Session["User_DeptID"]);
                    strUserCode = Convert.ToString(Session["User_Code"]);

                    ViewState["IPOTC_OT_ID"] = "";
                    ViewState["OTSelectIndex"] = "";

                    BindGrid();

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                IP_PTMaster objPTM = new IP_PTMaster();
                string SO_TRANS_ID = "";

                try
                {

                    objPTM.IPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPTM.IPM_ID = Convert.ToString(Session["EMR_ID"]);
                    objPTM.IPM_DR_CODE = Convert.ToString(Session["IAS_DR_ID"]);
                    objPTM.DR_Name = Convert.ToString(Session["IAS_DR_NAME"]);
                    objPTM.SO_ID = Convert.ToString(Session["SO_ID"]);

                    objPTM.ServCode = txtServCode.Text;
                    objPTM.ServDesc = txtServName.Text.Replace("'", "''");
                    objPTM.ServQty = txtQty.Text.Trim();
                    objPTM.ServType = "OT"; //"HCPCS";
                    objPTM.IPSalesOrderTransAdd(out SO_TRANS_ID);


                }
                catch (Exception ex)
                {
                    goto FunEnd;
                }



                objOT = new IP_OTConsumables();

                objOT.BranchID = Convert.ToString(Session["Branch_ID"]);
                objOT.EMRID = Convert.ToString(Session["EMR_ID"]);
                objOT.PTID = Convert.ToString(Session["EMR_PT_ID"]);
                objOT.IPOTC_OT_ID = Convert.ToString(ViewState["IPOTC_OT_ID"]);

                objOT.IPOTC_OT_CODE = txtServCode.Text;
                objOT.IPOTC_OT_NAME = txtServName.Text.Replace("'", "''");

                objOT.IPOTC_OT_TYPE = drpConsumablesType.SelectedValue;
                objOT.IPOTC_QTY = txtQty.Text;
                objOT.IPOTC_PRICE = txtPrice.Text;
                objOT.IPOTC_SO_TRANS_ID = SO_TRANS_ID;
                objOT.OTConsumablesAdd();
                BindGrid();
                Clear();

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAddPhar_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void OTConSelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;
                ViewState["OTSelectIndex"] = gvScanCard.RowIndex;
                gvOTConsum.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");



                Label lblgvOTID = (Label)gvScanCard.Cells[0].FindControl("lblgvOTID");

                Label lblgvCode = (Label)gvScanCard.Cells[0].FindControl("lblgvCode");
                Label lblgvDesc = (Label)gvScanCard.Cells[0].FindControl("lblgvDesc");
                Label lblgvConsumableType = (Label)gvScanCard.Cells[0].FindControl("lblgvConsumableType");

                Label lblgvQTY = (Label)gvScanCard.Cells[0].FindControl("lblgvQTY");
                Label lblgvPrice = (Label)gvScanCard.Cells[0].FindControl("lblgvPrice");




                ViewState["IPOTC_OT_ID"] = lblgvOTID.Text;

                txtServCode.Text = lblgvCode.Text;
                txtServName.Text = lblgvDesc.Text;


                drpConsumablesType.SelectedValue = lblgvConsumableType.Text;

                txtQty.Text = lblgvQTY.Text;

                txtPrice.Text = lblgvPrice.Text;




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "PhySelect_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void Deletee_Click(object sender, EventArgs e)
        {
            try
            {

                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblgvOTID = (Label)gvScanCard.Cells[0].FindControl("lblgvOTID");
                Label lblSOTransID = (Label)gvScanCard.Cells[0].FindControl("lblSOTransID");

                objOT = new IP_OTConsumables();

                objOT.BranchID = Convert.ToString(Session["Branch_ID"]);
                objOT.EMRID = Convert.ToString(Session["EMR_ID"]);
                objOT.IPOTC_OT_ID = lblgvOTID.Text;
                objOT.IPOTC_SO_TRANS_ID = lblSOTransID.Text;

                objOT.OTConsumablesDelete();
                BindGrid();
                Clear();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Deletee_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        #endregion

    }
}