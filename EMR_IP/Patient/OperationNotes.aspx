﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="OperationNotes.aspx.cs" Inherits="EMR_IP.Patient.OperationNotes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidthProc
        {
            width: 400px !important;
        }

            #divwidthProc div
            {
                width: 400px !important;
            }
    </style>

    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

        function ShowErrorMessage(vMessage) {

            document.getElementById("divMessage").style.display = 'block';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = vMessage;
        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = '';
        }
    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function BindProcedure() {



            var ChiefComp = document.getElementById("<%=txtProcedureFindings.ClientID%>").value + document.getElementById("<%=txtProc.ClientID%>").value + "\n";
            if (document.getElementById("<%=txtProc.ClientID%>").value != "") {
                document.getElementById("<%=txtProcedureFindings.ClientID%>").value = ChiefComp;
            }
            document.getElementById("<%=txtProc.ClientID%>").value = '';


            return false;

        }

        function BindNurse() {



            var ChiefComp = document.getElementById("<%=txtNurseDtls.ClientID%>").value + document.getElementById("<%=txtNurseName.ClientID%>").value + "\n";

            if (document.getElementById("<%=txtNurseName.ClientID%>").value != "") {
                document.getElementById("<%=txtNurseDtls.ClientID%>").value = ChiefComp;
            }
            document.getElementById("<%=txtNurseName.ClientID%>").value = '';


            return false;

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />
    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully 
                
        </div>
    </div>
    <div style="padding-left: 60%; width: 100%;">
        <div id="divMessage" style="display: none; border: groove; height: 50px; width: 300px; background-color: brown; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right" valign="top">

                        <input type="button" id="btnMsgClose" class="ButtonStyle" style="background-color: White; color: black; border: none;" value=" X " onclick="HideErrorMessage()" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMessage" runat="server" CssClass="label"></asp:Label>
                    </td>
                </tr>
            </table>

        </div>
    </div>
        <table width="100%"  >
            <tr>
                <td style="text-align:left;width:50%;" >
                    <h3 class="box-title">Operation Notes</h3>
                </td>
                <td style="text-align:right;;width:50%;">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Saved Record" OnClick="btnSave_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>


    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Operation Notes" Width="100%">
            <ContentTemplate>
                <table style="width: 100%" cellpadding="3" cellspacing="3">
                    <tr>
                        <td class="lblCaption1" style="width: 100px;">Date 
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel ID="updatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtORDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                        Enabled="True" TargetControlID="txtORDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1">Name of Surgeon
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel5">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpSurgeon" runat="server" CssClass="label" Width="200px"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Assistants
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel6">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpAssistants" runat="server" CssClass="label" Width="200px"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>


                    </tr>
                    <tr>
                        <td class="lblCaption1">PreOperative Diagnosis
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel19" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtPreOperDiag" runat="server" CssClass="label" TextMode="MultiLine" Width="100%" Height="50px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>

                        <td class="lblCaption1">PostOperative Diagnosis
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtPostOperDiag" runat="server" CssClass="label" TextMode="MultiLine" Width="100%" Height="50px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Operative procedure Performed
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtOperProcPerf" runat="server" CssClass="label" TextMode="MultiLine" Width="100%" Height="50px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Name of Anaesthetist
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel9">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpAnaesthetist" runat="server" CssClass="label" Width="200px"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                        <td class="lblCaption1">Anesthesia Type 
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel69" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpAnesType" runat="server" CssClass="label" Width="200px">
                                        <asp:ListItem Text="--- Select ---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
                                        <asp:ListItem Text="SA" Value="SA"></asp:ListItem>
                                        <asp:ListItem Text="LA" Value="LA"></asp:ListItem>
                                        <asp:ListItem Text="Epidural" Value="Epidural"></asp:ListItem>
                                        <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">OR No.
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel7" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpORRooms" runat="server" CssClass="label" Width="200px">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                        <td class="lblCaption1">SWAB & Instrument Check
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel11" runat="server">
                                <ContentTemplate>
                                    <asp:CheckBox ID="chkSWABInstrumentCheck" runat="server" CssClass="lblCaption1" Width="200px" Text="SWAB & Instrument Check" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Theatre Sisters 
                        </td>
                        <td class="auto-style28" colspan="3">
                            <asp:UpdatePanel runat="server" ID="updatePanel8">
                                <ContentTemplate>
                                    <div id="divDr" style="visibility: hidden;"></div>
                                    <asp:TextBox ID="txtNurseName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" Width="60%" MaxLength="50"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtNurseName" MinimumPrefixLength="1" ServiceMethod="GetDoctorName"
                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDr">
                                    </asp:AutoCompleteExtender>
                                    <input type="button" id="btnNurseAdd" value="Add" class="button orange small" onclick="BindNurse()" />
                                    <asp:TextBox ID="txtNurseDtls" runat="server" Width="70%" Height="100px" CssClass="label" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>



                    </tr>

                    <tr>
                        <td class="lblCaption1">Specimens to Pathology 
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel10" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtSpecimensPathology" runat="server" CssClass="label" TextMode="MultiLine" Width="100%" Height="50px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                        <td class="lblCaption1" style="width: 150px;">Time Started
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel12" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpSTHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                    <asp:DropDownList ID="drpSTMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                    Finished Time  
                        <asp:DropDownList ID="drpFinHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                    <asp:DropDownList ID="drpFinMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">I. V. Transfusion/lnfusion
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel14" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtIVTransFusion" runat="server" CssClass="label" TextMode="MultiLine" Width="100%" Height="50px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                        <td class="lblCaption1">Drains
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel15" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtDrains" runat="server" CssClass="label" TextMode="MultiLine" Width="100%" Height="50px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Sedation/Antibiolics/Drugs
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel16" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtSedation" runat="server" CssClass="label" TextMode="MultiLine" Width="100%" Height="50px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                        <td class="lblCaption1">Catheters
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel17" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCatheters" runat="server" CssClass="label" TextMode="MultiLine" Width="100%" Height="50px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>


            </ContentTemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Procedure and Findings" Width="100%">
            <ContentTemplate>
                <table width="100%">

                    <tr>
                        <td class="lblCaption1" valign="top">Procedure and Findings
                        </td>
                        <td valign="top">
                            <asp:UpdatePanel ID="updatePanel18" runat="server">
                                <ContentTemplate>
                                    <div id="divwidthProc" style="visibility: hidden;"></div>
                                    <asp:TextBox ID="txtProc" runat="server" CssClass="label" Width="60%"></asp:TextBox>
                                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtProc" MinimumPrefixLength="1" ServiceMethod="GetProcedure"
                                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidthPDiag">
                                    </asp:AutoCompleteExtender>
                                    <input type="button" id="btnProcAdd" value="Add" class="button orange small" onclick="BindProcedure()" />
                                    <asp:TextBox ID="txtProcedureFindings" runat="server" Width="70%" Height="150px" CssClass="label" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>


                    </tr>

                </table>
            </ContentTemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server" ID="TabPanel5" HeaderText="Signature" Width="100%">
            <ContentTemplate>
                <table width="100%">

                    <tr>
                        <td>
                            <fieldset>
                                <legend class="lblCaption1">Surgeon Signature </legend>
                                <table width="100%">
                                    <tr>

                                        <td class="lblCaption1" style="width: 150px">Name  
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpSigSurgeon" runat="server" CssClass="label" Width="205px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="width: 150px">Password
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSigSurgeonPass" runat="server" CssClass="label" TextMode="Password" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Date & Time
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel54" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtSigSurgeonDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="Calendarextender5" runat="server"
                                                        Enabled="True" TargetControlID="txtSigSurgeonDate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:DropDownList ID="drpSigSurgeonHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                    <asp:DropDownList ID="drpSigSurgeonMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>


                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>

                                </table>


                            </fieldset>
                        </td>
                        <td>
                            <fieldset>
                                <legend class="lblCaption1">Assistant Signature</legend>
                                <table width="100%">
                                    <tr>

                                        <td class="lblCaption1" style="width: 150px">Name  
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpSigAssistant" runat="server" CssClass="label" Width="205px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="width: 150px">Password
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSigAssistantPass" runat="server" CssClass="label" TextMode="Password" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Date & Time
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel49" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtSigAssistantDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="Calendarextender4" runat="server"
                                                        Enabled="True" TargetControlID="txtSigAssistantDate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>

                                                    <asp:DropDownList ID="drpSigAssistantHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                    <asp:DropDownList ID="drpSigAssistantMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>

                                </table>
                            </fieldset>
                        </td>

                    </tr>



                </table>
            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>
</asp:Content>
