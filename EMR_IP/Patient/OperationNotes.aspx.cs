﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;

namespace EMR_IP.Patient
{
    public partial class OperationNotes : System.Web.UI.Page
    {
        static string strSessionDeptId;

        DataSet DS = new DataSet();

        CommonBAL objCom = new CommonBAL();
        IP_OperationNotes objOper = new IP_OperationNotes();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("OperationNotes." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {
                 
                drpSTHour.DataSource = DS;
                drpSTHour.DataTextField = "Name";
                drpSTHour.DataValueField = "Code";
                drpSTHour.DataBind();

                drpFinHour.DataSource = DS;
                drpFinHour.DataTextField = "Name";
                drpFinHour.DataValueField = "Code";
                drpFinHour.DataBind();

                drpSigSurgeonHour.DataSource = DS;
                drpSigSurgeonHour.DataTextField = "Name";
                drpSigSurgeonHour.DataValueField = "Code";
                drpSigSurgeonHour.DataBind();

                drpSigAssistantHour.DataSource = DS;
                drpSigAssistantHour.DataTextField = "Name";
                drpSigAssistantHour.DataValueField = "Code";
                drpSigAssistantHour.DataBind();
            }

            drpSTHour.Items.Insert(0, "00");
            drpSTHour.Items[0].Value = "00";

            drpFinHour.Items.Insert(0, "00");
            drpFinHour.Items[0].Value = "00";

            drpSigSurgeonHour.Items.Insert(0, "00");
            drpSigSurgeonHour.Items[0].Value = "00";

            drpSigAssistantHour.Items.Insert(0, "00");
            drpSigAssistantHour.Items[0].Value = "00";


            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {
 
                drpSTMin.DataSource = DS;
                drpSTMin.DataTextField = "Name";
                drpSTMin.DataValueField = "Code";
                drpSTMin.DataBind();

                drpFinMin.DataSource = DS;
                drpFinMin.DataTextField = "Name";
                drpFinMin.DataValueField = "Code";
                drpFinMin.DataBind();

                drpSigSurgeonMin.DataSource = DS;
                drpSigSurgeonMin.DataTextField = "Name";
                drpSigSurgeonMin.DataValueField = "Code";
                drpSigSurgeonMin.DataBind();


                drpSigAssistantMin.DataSource = DS;
                drpSigAssistantMin.DataTextField = "Name";
                drpSigAssistantMin.DataValueField = "Code";
                drpSigAssistantMin.DataBind();
            }

            drpSTMin.Items.Insert(0, "00");
            drpSTMin.Items[0].Value = "00";

            drpFinMin.Items.Insert(0, "00");
            drpFinMin.Items[0].Value = "00";

            drpSigSurgeonMin.Items.Insert(0, "00");
            drpSigSurgeonMin.Items[0].Value = "00";


            drpSigAssistantMin.Items.Insert(0, "00");
            drpSigAssistantMin.Items[0].Value = "00";

             
             
        }

        void BindStaff()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS = 'Present'  ";
            Criteria += " AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.GetStaffMaster(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpSurgeon.DataSource = DS;
                drpSurgeon.DataTextField = "FullName";
                drpSurgeon.DataValueField = "HSFM_STAFF_ID";
                drpSurgeon.DataBind();

                drpAssistants.DataSource = DS;
                drpAssistants.DataTextField = "FullName";
                drpAssistants.DataValueField = "HSFM_STAFF_ID";
                drpAssistants.DataBind();



                drpAnaesthetist.DataSource = DS;
                drpAnaesthetist.DataTextField = "FullName";
                drpAnaesthetist.DataValueField = "HSFM_STAFF_ID";
                drpAnaesthetist.DataBind();




            }


            drpSurgeon.Items.Insert(0, "--- Select ---");
            drpSurgeon.Items[0].Value = "";

            drpAssistants.Items.Insert(0, "--- Select ---");
            drpAssistants.Items[0].Value = "";

            drpAnaesthetist.Items.Insert(0, "--- Select ---");
            drpAnaesthetist.Items[0].Value = "";



        }


        void BindORRoom()
        {
            DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " HORM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND HORM_STATUS='A' ";

            DS = objCom.fnGetFieldValue("HORM_ID", "HMS_OPERATION_THEATRE_MASTER", Criteria, "HORM_ID");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpORRooms.DataSource = DS;
                drpORRooms.DataTextField = "HORM_ID";
                drpORRooms.DataValueField = "HORM_ID";
                drpORRooms.DataBind();


            }
            drpORRooms.Items.Insert(0, "--- Select ---");
            drpORRooms.Items[0].Value = "";
        }

        void BindData()
        {
            DS = new DataSet();
            objOper = new IP_OperationNotes();
            string Criteria = " ION_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  ION_ID ='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objOper.OperationNotesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    txtORDate.Text = Convert.ToString(DR["ION_DATEDesc"]);
                    drpSurgeon.SelectedValue = Convert.ToString(DR["ION_SURGEON_ID"]);
                    drpAssistants.SelectedValue = Convert.ToString(DR["ION_ASSISTANTS"]);


                    txtPreOperDiag.Text = Convert.ToString(DR["ION_PREOPERATIVE_DIAGNOSIS"]);
                    txtPostOperDiag.Text = Convert.ToString(DR["ION_POSTOPERATIVE_DIAGNOSIS"]);
                    txtOperProcPerf.Text = Convert.ToString(DR["ION_OPERATION_PERFORMED"]);
                    drpAnaesthetist.SelectedValue = Convert.ToString(DR["ION_ANAESTHETIST_ID"]);
                    drpAnesType.SelectedValue = Convert.ToString(DR["ION_ANAESTHETIST_TYPE"]);
                    drpORRooms.SelectedValue = Convert.ToString(DR["ION_OR_NO"]);


                    txtNurseDtls.Text = Convert.ToString(DR["ION_THEATRE_SISTERS"]);
                    txtProcedureFindings.Text = Convert.ToString(DR["ION_PROCEDURE_FINDINGS"]);
                    txtSpecimensPathology.Text = Convert.ToString(DR["ION_SPECIMENTS_PATHOLOGY"]);
                    chkSWABInstrumentCheck.Checked = Convert.ToBoolean(DR["ION_SWAB_INSTRUMENT_CHECK"]);

                    string strTimeUp = Convert.ToString(DR["ION_STARTED_TIMEDesc"]);

                    string[] arrTimeUp = strTimeUp.Split(':');
                    if (arrTimeUp.Length > 1)
                    {
                        drpSTHour.SelectedValue = arrTimeUp[0];
                        drpSTMin.SelectedValue = arrTimeUp[1];

                    }


                    string strFinished = Convert.ToString(DR["ION_FINISHED_TIMEDesc"]);

                    string[] arrFinished = strFinished.Split(':');
                    if (arrFinished.Length > 1)
                    {
                        drpFinHour.SelectedValue = arrFinished[0];
                        drpFinMin.SelectedValue = arrFinished[1];

                    }


                    txtIVTransFusion.Text = Convert.ToString(DR["ION_IV_TRANSFUSION_INFUSION"]);
                    txtDrains.Text = Convert.ToString(DR["ION_DRAINS"]);
                    txtSedation.Text = Convert.ToString(DR["ION_SEDATION_ANTIBIOTICS_DRUGS"]);
                    txtCatheters.Text = Convert.ToString(DR["ION_CATHETERS"]);
                }
            }
        }

        void BindUsers()
        {
            string Criteria = " 1=1 AND HUM_STATUS='A' ";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND ( HUM_REMARK IN (SELECT HSFM_STAFF_ID FROM HMS_STAFF_MASTER WHERE HSFM_CATEGORY IN ('Doctors','Nurse') OR HUM_USER_ID ='ADMIN')) ";


            DataSet ds = new DataSet();
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {



                drpSigSurgeon.DataSource = ds;
                drpSigSurgeon.DataValueField = "HUM_USER_ID";
                drpSigSurgeon.DataTextField = "HUM_USER_NAME";
                drpSigSurgeon.DataBind();

                drpSigAssistant.DataSource = ds;
                drpSigAssistant.DataValueField = "HUM_USER_ID";
                drpSigAssistant.DataTextField = "HUM_USER_NAME";
                drpSigAssistant.DataBind();




            }
            drpSigSurgeon.Items.Insert(0, "--- Select ---");
            drpSigSurgeon.Items[0].Value = "";

            drpSigAssistant.Items.Insert(0, "--- Select ---");
            drpSigAssistant.Items[0].Value = "";


        }

        Boolean CheckPassword(string UserID, string Password)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID ='" + UserID + "' AND   HUM_USER_PASS= '" + Password + "'";

            DataSet ds = new DataSet();
            //ds = dbo.UserMasterGet(drpBranch.SelectedValue, drpUsers.SelectedValue , txtPassword.Text);
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_OPERATION_NOTES' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Visible = false;
                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSave.Visible = false;
                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }


        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetProcedure(string prefixText)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string[] Data;

            ds = dbo.HaadServicessListGet("Procedure", prefixText, "", strSessionDeptId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["Code"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["Description"]).Trim();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        [System.Web.Services.WebMethod]
        public static string[] GetDoctorName(string prefixText)
        {
            string[] Data;
            string Criteria = " 1=1 AND HSFM_CATEGORY='Nurse'";
            // Criteria += " AND HSFM_FNAME Like '%" + prefixText + "%'";
            Criteria += " AND ( HSFM_STAFF_ID Like '%" + prefixText + "%' OR HSFM_FNAME + ' ' +isnull(HSFM_MNAME,'') + ' '  + isnull(HSFM_LNAME,'')   like '%" + prefixText + "%' ) ";

            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();
            ds = objCom.GetStaffMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["HSFM_STAFF_ID"]) + "~" + Convert.ToString(ds.Tables[0].Rows[i]["FullName"]);
                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }



            if (!IsPostBack)
            {

                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }

                try
                {
                    strSessionDeptId = Convert.ToString(Session["User_DeptID"]);

                    CommonBAL objCom = new CommonBAL();
                    string strDate = "";
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    txtORDate.Text = strDate;
                    txtSigSurgeonDate.Text = strDate;
                    txtSigAssistantDate.Text = strDate;

                    BindTime();
                    BindStaff();
                    BindUsers();
                    BindORRoom();
                    BindData();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (drpSigSurgeon.SelectedIndex == 0 && drpSigAssistant.SelectedIndex == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Please enter Signature Details')", true);
                    goto FunEnd;
                }
                if (drpSigSurgeon.SelectedIndex != 0)
                {
                    if (CheckPassword(drpSigSurgeon.SelectedValue, txtSigSurgeonPass.Text.Trim()) == false)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Surgeon Password wrong')", true);
                        goto FunEnd;
                    }
                }

                if (drpSigAssistant.SelectedIndex != 0)
                {
                    if (CheckPassword(drpSigAssistant.SelectedValue, txtSigAssistantPass.Text.Trim()) == false)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Assistant Password wrong')", true);
                        goto FunEnd;
                    }
                }

                objOper = new IP_OperationNotes();
                objOper.BranchID = Convert.ToString(Session["Branch_ID"]);
                objOper.EMRID = Convert.ToString(Session["EMR_ID"]);
                objOper.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                objOper.ION_DATE = txtORDate.Text;
                objOper.ION_SURGEON_ID = drpSurgeon.SelectedValue;
                objOper.ION_SURGEON_NAME = drpSurgeon.SelectedItem.Text;
                objOper.ION_ASSISTANTS = drpAssistants.SelectedValue;
                objOper.ION_PREOPERATIVE_DIAGNOSIS = txtPreOperDiag.Text;
                objOper.ION_POSTOPERATIVE_DIAGNOSIS = txtPostOperDiag.Text;
                objOper.ION_OPERATION_PERFORMED = txtOperProcPerf.Text;
                objOper.ION_ANAESTHETIST_ID = drpAnaesthetist.SelectedValue;
                objOper.ION_ANAESTHETIST_TYPE = drpAnesType.SelectedValue;
                objOper.ION_OR_NO = drpORRooms.SelectedValue;
                objOper.ION_THEATRE_SISTERS = txtNurseDtls.Text;
                objOper.ION_PROCEDURE_FINDINGS = txtProcedureFindings.Text;
                objOper.ION_SPECIMENTS_PATHOLOGY = txtSpecimensPathology.Text;
                objOper.ION_SWAB_INSTRUMENT_CHECK = chkSWABInstrumentCheck.Checked == true ? "true" : "false";
                objOper.ION_STARTED_TIME = txtORDate.Text.Trim() + " " + drpSTHour.SelectedValue + ":" + drpSTMin.SelectedValue + ":00";
                objOper.ION_FINISHED_TIME = txtORDate.Text.Trim() + " " + drpFinHour.SelectedValue + ":" + drpFinMin.SelectedValue + ":00";
                objOper.ION_IV_TRANSFUSION_INFUSION = txtIVTransFusion.Text;
                objOper.ION_DRAINS = txtDrains.Text;
                objOper.ION_SEDATION_ANTIBIOTICS_DRUGS = txtSedation.Text;
                objOper.ION_CATHETERS = txtCatheters.Text;

                objOper.UserID = Convert.ToString(Session["User_ID"]);
                objOper.OperationNotesAdd();


                if (drpSigSurgeon.SelectedIndex != 0)
                {
                    string strSurgeonDate = txtSigSurgeonDate.Text.Trim() + " " + drpSigSurgeonHour.SelectedValue + ":" + drpSigSurgeonMin.SelectedValue + ":00";

                    objCom = new CommonBAL();
                    string FieldNameWithValues = " ION_SURGEON_SIGN='" + drpSigSurgeon.SelectedValue + "'";
                    FieldNameWithValues += ",ION_SURGEON_SIGN_DATE=CONVERT(datetime,'" + strSurgeonDate + "',103) ";

                    string Criteria = " ION_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  ION_ID ='" + Convert.ToString(Session["EMR_ID"]) + "'";

                    objCom.fnUpdateTableData(FieldNameWithValues, "IP_OPERATION_NOTES", Criteria);

                }

                if (drpSigAssistant.SelectedIndex != 0)
                {
                    string strSurgeonDate = txtSigAssistantDate.Text.Trim() + " " + drpSigAssistantHour.SelectedValue + ":" + drpSigAssistantMin.SelectedValue + ":00";

                    objCom = new CommonBAL();
                    string FieldNameWithValues = " ION_ASSISTANTS_SIGN='" + drpSigAssistant.SelectedValue + "'";
                    FieldNameWithValues += ",ION_ASSISTANTS_SIGN_DATE=CONVERT(datetime,'" + strSurgeonDate + "',103) ";

                    string Criteria = " ION_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND  ION_ID ='" + Convert.ToString(Session["EMR_ID"]) + "'";

                    objCom.fnUpdateTableData(FieldNameWithValues, "IP_OPERATION_NOTES", Criteria);

                }
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}