﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="PainAssessment.aspx.cs" Inherits="EMR_IP.Patient.PainAssessment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }


    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />
    <h3 class="box-title">Pain Assessment</h3>

    <br />
    <br />
    <br />
    <br />
    <br />
    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully 
                
        </div>
    </div>

    <div style="float: right;">

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Saved Record" OnClick="btnSave_Click" />


            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <br />
    <br />
    <br />
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Pain Assessment1" Width="100%">
            <ContentTemplate>
                <fieldset>
                    <legend class="lblCaption1">Ongoing Pain Assessment </legend>
                    <span class="lblCaption1">PAIN ASSESSMENT: (when there is no change in Pain Character, Location, Frequency & Duration, enter NC “No Change”) </span>
                    <table style="width: 100%">
                        <tr>
                            <td class="lblCaption1">Date & Time
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel3" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAssDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                            Enabled="True" TargetControlID="txtAssDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>

                                    <asp:dropdownlist id="drpAssHour" style="font-size:11px;"  cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"> </asp:dropdownlist>
                                    <asp:dropdownlist id="drpAssMin" style="font-size:11px;"  cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"></asp:dropdownlist>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Location
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel4" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAssLocation" runat="server" Width="200px" CssClass="label"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Pain intensity
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel5" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAssPainIntensity" runat="server" Width="200px" CssClass="label"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Character code
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel6" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpAssCharacterCode" runat="server"  Width="200px" CssClass="label" >
                                            <asp:ListItem Text="--- Select ---" Value ="" ></asp:ListItem>
                                            <asp:ListItem Text="Sharp" Value ="Sharp" ></asp:ListItem>
                                             <asp:ListItem Text="Dull" Value ="Dull" ></asp:ListItem>
                                             <asp:ListItem Text="Stabbing" Value ="Stabbing" ></asp:ListItem>
                                             <asp:ListItem Text="Burning" Value ="Burning" ></asp:ListItem>
                                             <asp:ListItem Text="Crushing" Value ="Crushing" ></asp:ListItem>
                                             <asp:ListItem Text="Deep" Value ="Deep" ></asp:ListItem>
                                             <asp:ListItem Text="Colic" Value ="Colic" ></asp:ListItem>
                                             <asp:ListItem Text="Throbbing" Value ="Throbbing" ></asp:ListItem>
                                             <asp:ListItem Text="Numb" Value ="Numb" ></asp:ListItem>
                                             <asp:ListItem Text="Shooting" Value ="Shooting" ></asp:ListItem>
                                             <asp:ListItem Text="Pressing" Value ="Pressing" ></asp:ListItem>
                                             <asp:ListItem Text="Tight" Value ="Tight" ></asp:ListItem>
                                             <asp:ListItem Text="Pulling" Value ="Pulling" ></asp:ListItem>
                                             <asp:ListItem Text="Sousing" Value ="Sousing" ></asp:ListItem>
                                             <asp:ListItem Text="Sore" Value ="Sore" ></asp:ListItem>
                                             <asp:ListItem Text="No pain" Value ="No pain" ></asp:ListItem>

                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Frequency
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel7" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAssFrequency" runat="server" Width="200px" CssClass="label"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Duration
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel8" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAssDuration" runat="server" Width="200px" CssClass="label"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>

                    </table>
                    <table>
                        <tr>
                            <td colspan="2" class="lblCaption1">INTERVENTIONS
                                <asp:UpdatePanel ID="updatePanel9" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBox ID="chkContinueCurTreat" runat="server" Text="Continue current treatment" CssClass="lblCaption1" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>

                        <tr>
                            <td class="lblCaption1" style="width: 200px;">Medication (Type, dose, frequency): 
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel10" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAssMedication" runat="server" CssClass="label" Width="100%" Height="30px" TextMode="MultiLine"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Non Medication 
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel11" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBoxList ID="chkAssNonMedication" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="800px">
                                            <asp:ListItem Text="Heat packs" Value="Heat packs"></asp:ListItem>
                                            <asp:ListItem Text="Cold packs" Value="Cold packs"></asp:ListItem>
                                            <asp:ListItem Text="Repositioning / turning" Value="Repositioning / turning"></asp:ListItem>
                                            <asp:ListItem Text="Ambulation" Value="Ambulation"></asp:ListItem>
                                            <asp:ListItem Text="Relaxation exercises" Value="Relaxation exercises"></asp:ListItem>
                                            <asp:ListItem Text="Deep Breathing" Value="Deep Breathing"></asp:ListItem>
                                            <asp:ListItem Text="Rhythmic Breathing" Value="Rhythmic Breathing"></asp:ListItem>
                                        </asp:CheckBoxList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                        <tr>
                            <td class="lblCaption1">Other Interventions
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel12" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtAssOtherInterventions" runat="server" CssClass="label" Width="100%" Height="30px" TextMode="MultiLine"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                
                            <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="button orange small" OnClick="btnAdd_Click" />
                     

                    <asp:UpdatePanel ID="updatePanel24" runat="server">
                        <ContentTemplate>

                            <asp:GridView ID="gvAssessment" runat="server" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="100%" GridLines="none">
                                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Date & Tome">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCode" runat="server" OnClick="Assessment_Click">
                                                <asp:Label ID="lblAssessID" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_ASS_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblgvAssDate" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_DATEDesc") %>'></asp:Label>
                                                <asp:Label ID="lblgvAssTime" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_DATE_TIMEDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Location">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnksys" runat="server" OnClick="Assessment_Click">
                                                <asp:Label ID="lblgvLocation" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_LOCATION") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Pain intensity">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkdis" runat="server" OnClick="Assessment_Click">
                                                <asp:Label ID="lblgvPainIntensity" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_PAIN_INTENSITY") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Character code">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPul" runat="server" OnClick="Assessment_Click">
                                                <asp:Label ID="lblgvCharacterCode" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_CHARACTER_CODE") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Frequency">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkspo" runat="server" OnClick="Assessment_Click">
                                                <asp:Label ID="lblgvFrequency" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_FREQUENCY") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Duration">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkRes" runat="server" OnClick="Assessment_Click">
                                                <asp:Label ID="lblgvDuration" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_DURATION") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Continue treat.">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkTemp" runat="server" OnClick="Assessment_Click">
                                                <asp:Label ID="lblgvContinuetreat" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_CONT_CURR_MED") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>




                                    <asp:TemplateField HeaderText="Medication">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPain" runat="server" OnClick="Assessment_Click">
                                                <asp:Label ID="lblgvMedication" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_MEDICATION") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Non Medication">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkMot" runat="server" OnClick="Assessment_Click">
                                                <asp:Label ID="lblgvNonMedication" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_NONMEDICATION") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Other Interventions">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCome" runat="server" OnClick="Assessment_Click">
                                                <asp:Label ID="lblgvOtherInter" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_OTHER_INTERVENTIONS") %>'></asp:Label>
                                            </asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>


                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </fieldset>
            </ContentTemplate>
        </asp:TabPanel>


        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Pain Assessment2" Width="100%">
            <ContentTemplate>

                <fieldset>
                    <legend class="lblCaption1">OTHER QUESTIONS TO ASK  </legend>
                <table style="width: 100%">
                  
                    <tr>
                        <td class="lblCaption1" style="width: 200px;">Pain Radiation: 
                        </td>
                        <td class="lblCaption1" colspan="3">

                            <input type="radio" id="radPainRadiationNo" name="radWeightLoss" runat="server" class="lblCaption1" value="No" />No 
                       <input type="radio" id="radPainRadiationYes" name="radWeightLoss" runat="server" class="lblCaption1" value="Yes" />Yes
                    , specify (where?)
                  <asp:TextBox ID="txtCurrentMedications" runat="server" CssClass="label" Width="100%" Height="30px" TextMode="MultiLine"></asp:TextBox>

                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Pain Pattern
                        </td>
                        <td>
                            <asp:RadioButtonList ID="radPainPattern" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="200px">
                                <asp:ListItem Text="Constant" Value="Constant"></asp:ListItem>
                                <asp:ListItem Text="Intermittent" Value="Intermittent"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td class="lblCaption1">Pain onset
                        </td>
                        <td>
                            <asp:RadioButtonList ID="radPainOnset" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="200px">
                                <asp:ListItem Text="Constant" Value="Constant"></asp:ListItem>
                                <asp:ListItem Text="Intermittent" Value="Intermittent"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">What causes your pain?
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtPainCauses" runat="server" CssClass="label" Width="100%" Height="30px" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>

                    <tr>
                        <td class="lblCaption1" colspan="4">What relieves your pain? (Heat, cold, certain position, medication, etc…) 
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4">
                            <asp:TextBox ID="txtPainRelieves" runat="server" CssClass="label" Width="100%" Height="30px" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" colspan="4">What effects does pain have on Activities of Daily Living, relationships with others and emotions?
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:TextBox ID="txtEffectsOnActivities" runat="server" CssClass="label" Width="100%" Height="30px" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                </fieldset>

                  <fieldset>
                    <legend class="lblCaption1">Interventions  </legend>
                <table width="100%">
                    
                    <tr>
                        <td class="lblCaption1" style="width: 200px;">Medication (Type, dose, frequency): 
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="txtMedication" runat="server" CssClass="label" Width="100%" Height="30px" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Non Medication 
                        </td>
                        <td>
                            <asp:CheckBoxList ID="chkNonMedication" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="800px">
                                <asp:ListItem Text="Heat packs" Value="Heat packs"></asp:ListItem>
                                <asp:ListItem Text="Cold packs" Value="Cold packs"></asp:ListItem>
                                <asp:ListItem Text="Repositioning / turning" Value="Repositioning / turning"></asp:ListItem>
                                <asp:ListItem Text="Ambulation" Value="Ambulation"></asp:ListItem>
                                <asp:ListItem Text="Relaxation exercises" Value="Relaxation exercises"></asp:ListItem>
                                <asp:ListItem Text="Deep Breathing" Value="Deep Breathing"></asp:ListItem>
                                <asp:ListItem Text="Rhythmic Breathing" Value="Rhythmic Breathing"></asp:ListItem>
                            </asp:CheckBoxList>
                        </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1">Other Interventions
                        </td>
                        <td>
                            <asp:TextBox ID="txtOtherInterventions" runat="server" CssClass="label" Width="100%" Height="30px" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Intervention Time
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtInterventionDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                     <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                        Enabled="True" TargetControlID="txtInterventionDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:dropdownlist id="drpInterHour" style="font-size:11px;"  cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"> </asp:dropdownlist>
                                    <asp:dropdownlist id="drpInterMin" style="font-size:11px;"  cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"></asp:dropdownlist>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>

                </table>
                </fieldset>
                  <fieldset>
                    <legend class="lblCaption1">Please check appropriate scale used to assess pain intensity </legend>

                <table width="100%">
                    <tr>

                        <td class="lblCaption1">
                            <input type="checkbox" id="chkVerbal" name="chkScale" runat="server" class="lblCaption1" value="For verbal patients explain the Faces Pain Scale and ask thepatient to rate his/her pain" />For verbal patients explain the Faces Pain Scale and ask thepatient to rate his/her pain
                        </td>
                        <td colspan="2" class="lblCaption1">
                            <input type="checkbox" id="chkNonVerbal" name="chkScale" runat="server" class="lblCaption1" value="For non-verbal / pre-verbal rate the patients` pain using the Faces Pain Scale" />For non-verbal / pre-verbal rate the patients` pain using the Faces Pain Scale
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="lblCaption1">
                            <input type="checkbox" id="chkNeonates" name="chkScale" runat="server" class="lblCaption1" value="For Neonates use NIPS pain scoring scale" />For Neonates use NIPS pain scoring scale
                        </td>
                    </tr>

                </table>
                      </fieldset>
            </ContentTemplate>
        </asp:TabPanel>
         <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="Pain ReAssessment" Width="100%">
            <ContentTemplate>
                  <fieldset>
                    <legend class="lblCaption1">Pain ReAssessment </legend>
                    <span class="lblCaption1">(when there is no change in Pain Character, Location, Frequency & Duration, enter NC “No Change”) </span>
                    <table style="width: 100%">
                        <tr>
                            <td class="lblCaption1">Date & Time
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel13" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtReAssDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                            Enabled="True" TargetControlID="txtReAssDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:dropdownlist id="drpReAssHour" style="font-size:11px;"  cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"> </asp:dropdownlist>
                                        <asp:dropdownlist id="drpReAssMin" style="font-size:11px;"  cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"></asp:dropdownlist>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Location
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel14" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtReAssPainIntensity" runat="server" Width="200px" CssClass="label"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                              <td class="lblCaption1">Changes in Pain 
                            </td>
                             <td>
                                <asp:UpdatePanel ID="updatePanel15" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtReAssPaintChanges" runat="server" Width="200px" CssClass="label"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                              <td class="lblCaption1">Interventions
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel16" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtReAssInterventions" runat="server" Width="200px" CssClass="label"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                        </table>

                       <asp:Button ID="btnReAssAdd" runat="server" Text="Add" CssClass="button orange small" OnClick="btnReAssAdd_Click"/>

                        <asp:UpdatePanel ID="updatePanel17" runat="server">
                        <ContentTemplate>
                             <asp:GridView ID="gvReAssessment" runat="server" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="100%" GridLines="none">
                                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Date & Time">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCode" runat="server" OnClick="ReAssessment_Click">
                                                <asp:Label ID="lblReAssessID" CssClass="GridRow" runat="server" Text='<%# Bind("IPNAD_ASS_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblgvReAssDate" CssClass="GridRow" runat="server" Text='<%# Bind("IPNAD_DATEDesc") %>'></asp:Label>
                                                <asp:Label ID="lblgvReAssTime" CssClass="GridRow" runat="server" Text='<%# Bind("IPNAD_DATE_TIMEDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  
                                    <asp:TemplateField HeaderText="Pain intensity">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkdis" runat="server" OnClick="ReAssessment_Click">
                                                <asp:Label ID="lblgvReAssPainIntensity" CssClass="GridRow" runat="server" Text='<%# Bind("IPNAD_PAIN_INTENSITY") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Changes in Pain">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPul" runat="server" OnClick="ReAssessment_Click">
                                                <asp:Label ID="lblgvReAssCharacterCode" CssClass="GridRow" runat="server" Text='<%# Bind("IPNAD_PAIN_CHANGES") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Interventions">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkspo" runat="server" OnClick="ReAssessment_Click">
                                                <asp:Label ID="lblgvReAssInterventions" CssClass="GridRow" runat="server" Text='<%# Bind("IPNAD_INTERVENTIONS") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                            

                                     
                                </Columns>


                            </asp:GridView>
                             </ContentTemplate>
                    </asp:UpdatePanel>
                   </fieldset>
            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>
</asp:Content>
