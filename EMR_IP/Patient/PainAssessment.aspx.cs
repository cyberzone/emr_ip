﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;


namespace EMR_IP.Patient
{
    public partial class PainAssessment : System.Web.UI.Page
    {

        IP_PainAssessment objPainAss = new IP_PainAssessment();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {
                 


                drpAssHour.DataSource = DS;
                drpAssHour.DataTextField = "Name";
                drpAssHour.DataValueField = "Code";
                drpAssHour.DataBind();

                drpInterHour.DataSource = DS;
                drpInterHour.DataTextField = "Name";
                drpInterHour.DataValueField = "Code";
                drpInterHour.DataBind();


                drpReAssHour.DataSource = DS;
                drpReAssHour.DataTextField = "Name";
                drpReAssHour.DataValueField = "Code";
                drpReAssHour.DataBind();

                 
            }

            drpAssHour.Items.Insert(0, "00");
            drpAssHour.Items[0].Value = "00";

            drpInterHour.Items.Insert(0, "00");
            drpInterHour.Items[0].Value = "00";

            drpReAssHour.Items.Insert(0, "00");
            drpReAssHour.Items[0].Value = "00";
 

            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {

                drpAssMin.DataSource = DS;
                drpAssMin.DataTextField = "Name";
                drpAssMin.DataValueField = "Code";
                drpAssMin.DataBind();


                drpInterMin.DataSource = DS;
                drpInterMin.DataTextField = "Name";
                drpInterMin.DataValueField = "Code";
                drpInterMin.DataBind();


                drpReAssMin.DataSource = DS;
                drpReAssMin.DataTextField = "Name";
                drpReAssMin.DataValueField = "Code";
                drpReAssMin.DataBind();
            }

            drpAssMin.Items.Insert(0, "00");
            drpAssMin.Items[0].Value = "00";

            drpInterMin.Items.Insert(0, "00");
            drpInterMin.Items[0].Value = "00";


            drpReAssMin.Items.Insert(0, "00");
            drpReAssMin.Items[0].Value = "00";



             

        }

        void BindData()
        {
            DataSet DS = new DataSet();
            objPainAss = new IP_PainAssessment();


            string Criteria = " 1=1 and IPA_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPA_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objPainAss.PainAssessmentGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    string strPainRadiationNo = Convert.ToString(DR["IPA_PAIN_RADIATION"]);
                    string[] arrPainRadiationNo = strPainRadiationNo.Split('|');

                    if (arrPainRadiationNo.Length >= 0)
                    {

                        if (arrPainRadiationNo[0] == "No")
                        {
                            radPainRadiationNo.Checked = true;
                        }
                        else if (arrPainRadiationNo[0] == "Yes")
                        {
                            radPainRadiationYes.Checked = true;
                        }
                        if (arrPainRadiationNo.Length > 0)
                        {
                            txtCurrentMedications.Text = arrPainRadiationNo[1];
                        }

                    }
                    radPainPattern.SelectedValue = Convert.ToString(DR["IPA_PAIN_PATTERN"]);
                    radPainOnset.SelectedValue = Convert.ToString(DR["IPA_PAIN_ONSET"]);

                    txtPainCauses.Text = Convert.ToString(DR["IPA_PAIN_CAUSES"]);
                    txtPainRelieves.Text = Convert.ToString(DR["IPA_PAIN_RELIEVES"]);
                    txtEffectsOnActivities.Text = Convert.ToString(DR["IPA_EFFECTS_ONACTIVITIES"]);
                    txtMedication.Text = Convert.ToString(DR["IPA_MEDICATION"]);




                    string strNonMedication = Convert.ToString(DR["IPA_NONMEDICATION"]);
                    string[] arrNonMedication = strNonMedication.Split('|');



                    for (Int32 i = 0; i <= chkNonMedication.Items.Count - 1; i++)
                    {
                        for (Int32 j = 0; j <= arrNonMedication.Length - 1; j++)
                        {
                            if (arrNonMedication[j] == chkNonMedication.Items[i].Value)
                            {
                                chkNonMedication.Items[i].Selected = true;
                                goto EndArrLoop;
                            }

                        }

                    EndArrLoop: ;

                    }

                    txtOtherInterventions.Text = Convert.ToString(DR["IPA_OTHER_INTERVENTIONS"]);
                    txtInterventionDate.Text = Convert.ToString(DR["IPA_INTERVENTION_DateDesc"]);
                  

                    string strInterventionTime = Convert.ToString(DR["IPA_INTERVENTION_TIMEDesc"]);

                    string[] arrInterventionTime = strInterventionTime.Split(':');
                    if (arrInterventionTime.Length > 1)
                    {
                        drpInterHour.SelectedValue = arrInterventionTime[0];
                        drpInterMin.SelectedValue = arrInterventionTime[1];

                    }



                    string strPAIN_ASSESS_SCALE = Convert.ToString(DR["IPA_PAIN_ASSESS_SCALE"]);
                    string[] arrPAIN_ASSESS_SCALE = strPAIN_ASSESS_SCALE.Split('|');


                    if (arrPAIN_ASSESS_SCALE.Length >= 0)
                    {
                        for (Int32 j = 0; j <= arrPAIN_ASSESS_SCALE.Length - 1; j++)
                        {
                            if (arrPAIN_ASSESS_SCALE[j] == "For verbal patients explain the Faces Pain Scale and ask thepatient to rate his/her pain")
                            {
                                chkVerbal.Checked = true;
                            }

                            if (arrPAIN_ASSESS_SCALE[j] == "For non-verbal / pre-verbal rate the patients` pain using the Faces Pain Scale")
                            {
                                chkNonVerbal.Checked = true;
                            }

                            if (arrPAIN_ASSESS_SCALE[j] == "For Neonates use NIPS pain scoring scale")
                            {
                                chkNeonates.Checked = true;
                            }

                        }

                    }


                }

            }

        }

        void BindAssessmentGrid()
        {
            DataSet DS = new DataSet();
            objPainAss = new IP_PainAssessment();


            string Criteria = " 1=1 and IPAD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPAD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objPainAss.PainAssessmentDtlsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                gvAssessment.DataBind();
                if (DS.Tables[0].Rows.Count > 0)
                {
                    gvAssessment.DataSource = DS;
                    gvAssessment.DataBind();

                }
            }
        }

        void AssessmentClear()
        {
            Int32 R = 0;
            if (Convert.ToString(ViewState["gvAssessmentSelectIndex"]) != "" && Convert.ToString(ViewState["gvAssessmentSelectIndex"]) != null)
            {
                R = Convert.ToInt32(ViewState["gvAssessmentSelectIndex"]);
                gvAssessment.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
            }
            ViewState["IPAD_ASS_ID"] = "";
            ViewState["gvAssessmentSelectIndex"] = "";

            CommonBAL objCom = new CommonBAL();
            string strDate = "", strTime = ""; ;
            strDate = objCom.fnGetDate("dd/MM/yyyy");
            strTime = objCom.fnGetDate("hh:mm:ss");
            txtAssDate.Text = strDate;

            drpAssHour.SelectedIndex = 0;
            drpAssMin.SelectedIndex = 0;

            txtAssLocation.Text = "";
            txtAssPainIntensity.Text = "";

            drpAssCharacterCode.SelectedIndex = 0;
            txtAssFrequency.Text = "";
            txtAssDuration.Text = "";

            chkContinueCurTreat.Checked = false;

            txtAssMedication.Text = "";
            //chkAssNonMedication.
            for (Int32 i = 0; i <= chkAssNonMedication.Items.Count - 1; i++)
            {
                chkAssNonMedication.Items[i].Selected = false;
            }
            txtAssOtherInterventions.Text = "";

        }


        void BindReAssessmentGrid()
        {
            DataSet DS = new DataSet();
            objPainAss = new IP_PainAssessment();


            string Criteria = " 1=1 and IPNAD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPNAD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objPainAss.PainReAssessmentDtlsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                gvReAssessment.DataBind();
                if (DS.Tables[0].Rows.Count > 0)
                {
                    gvReAssessment.DataSource = DS;
                    gvReAssessment.DataBind();

                }
            }
        }

        void ReAssessmentClear()
        {
            Int32 R = 0;
            if (Convert.ToString(ViewState["gvReAssessmentSelectIndex"]) != "" && Convert.ToString(ViewState["gvReAssessmentSelectIndex"]) != null)
            {
                R = Convert.ToInt32(ViewState["gvReAssessmentSelectIndex"]);
                gvReAssessment.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
            }
            ViewState["IPNAD_ASS_ID"] = "";
            ViewState["gvReAssessmentSelectIndex"] = "";

            CommonBAL objCom = new CommonBAL();
            string strDate = "", strTime = ""; ;
            strDate = objCom.fnGetDate("dd/MM/yyyy");
            strTime = objCom.fnGetDate("hh:mm:ss");
            txtReAssDate.Text = strDate;

            drpReAssHour.SelectedIndex = 0;
            drpReAssMin.SelectedIndex = 0;
            txtReAssPainIntensity.Text = "";
            txtReAssPaintChanges.Text = "";
            txtReAssInterventions.Text = "";


        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_PAIN_ASS' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Visible = false;
                btnAdd.Visible = false;
                btnReAssAdd.Visible = false;


                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSave.Visible = false;
                btnAdd.Visible = false;
                btnReAssAdd.Visible = false;

                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {

                    if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                    {
                        SetPermission();
                    }

                    BindTime();
                    this.Page.Title = "PainAssessment";
                    CommonBAL objCom = new CommonBAL();
                    string strDate = "", strTime = ""; ;
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("hh:mm:ss");

                    txtInterventionDate.Text = strDate;
                   

                    txtAssDate.Text = strDate;
                    
                    txtReAssDate.Text = strDate;
                  

                    BindData();
                    BindAssessmentGrid();
                    BindReAssessmentGrid();
                    ViewState["IPAD_ASS_ID"] = "";
                    ViewState["IPNAD_ASS_ID"] = "";
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                objPainAss = new IP_PainAssessment();
                objPainAss.BranchID = Convert.ToString(Session["Branch_ID"]);
                objPainAss.EMRID = Convert.ToString(Session["EMR_ID"]);
                objPainAss.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                string strPainRadiationNo = "";
                if (radPainRadiationNo.Checked == true)
                {
                    strPainRadiationNo = radPainRadiationNo.Value;
                }
                else if (radPainRadiationYes.Checked == true)
                {
                    strPainRadiationNo = radPainRadiationYes.Value;
                }

                objPainAss.IPA_PAIN_RADIATION = strPainRadiationNo + "|" + txtCurrentMedications.Text;
                objPainAss.IPA_PAIN_PATTERN = radPainPattern.SelectedValue;
                objPainAss.IPA_PAIN_ONSET = radPainOnset.SelectedValue;
                objPainAss.IPA_PAIN_CAUSES = txtPainCauses.Text;
                objPainAss.IPA_PAIN_RELIEVES = txtPainRelieves.Text;
                objPainAss.IPA_EFFECTS_ONACTIVITIES = txtEffectsOnActivities.Text;

                objPainAss.IPA_MEDICATION = txtMedication.Text;

                string strNonMedication = "";

                for (Int32 i = 0; i <= chkNonMedication.Items.Count - 1; i++)
                {

                    if (chkNonMedication.Items[i].Selected == true)
                    {
                        strNonMedication += chkNonMedication.Items[i].Value + "|";
                    }

                }


                objPainAss.IPA_NONMEDICATION = strNonMedication;
                objPainAss.IPA_OTHER_INTERVENTIONS = txtOtherInterventions.Text;
                objPainAss.IPA_INTERVENTION_TIME = txtInterventionDate.Text.Trim() + " " + drpInterHour.SelectedValue + ":" + drpInterMin.SelectedValue + ":00";

                string strPAIN_ASSESS_SCALE = "";
                if (chkVerbal.Checked == true)
                {
                    strPAIN_ASSESS_SCALE += chkVerbal.Value + "|";
                }
                if (chkNonVerbal.Checked == true)
                {
                    strPAIN_ASSESS_SCALE += chkNonVerbal.Value + "|";
                }
                if (chkNeonates.Checked == true)
                {
                    strPAIN_ASSESS_SCALE += chkNeonates.Value + "|";
                }

                objPainAss.IPA_PAIN_ASSESS_SCALE = strPAIN_ASSESS_SCALE;
                objPainAss.UserID = Convert.ToString(Session["User_ID"]);

                objPainAss.PainAssessmentAdd();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                objPainAss = new IP_PainAssessment();
                objPainAss.BranchID = Convert.ToString(Session["Branch_ID"]);
                objPainAss.EMRID = Convert.ToString(Session["EMR_ID"]);
                objPainAss.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                if (ViewState["IPAD_ASS_ID"] == "")
                {
                    objPainAss.IPAD_ASS_ID = "0";
                }
                else
                {
                    objPainAss.IPAD_ASS_ID = Convert.ToString(ViewState["IPAD_ASS_ID"]);
                }

                objPainAss.IPAD_DATE = txtAssDate.Text.Trim() + " " + drpAssHour.SelectedValue + ":" + drpAssMin.SelectedValue + ":00";
                objPainAss.IPAD_LOCATION = txtAssLocation.Text;
                objPainAss.IPAD_PAIN_INTENSITY = txtAssPainIntensity.Text;
                objPainAss.IPAD_CHARACTER_CODE = drpAssCharacterCode.SelectedValue ;
                objPainAss.IPAD_FREQUENCY = txtAssFrequency.Text;
                objPainAss.IPAD_DURATION = txtAssDuration.Text;


                if (chkContinueCurTreat.Checked == true)
                {
                    objPainAss.IPAD_CONT_CURR_MED = "true";
                }
                else
                {
                    objPainAss.IPAD_CONT_CURR_MED = "false";
                }


                objPainAss.IPAD_MEDICATION = txtAssMedication.Text;

                string strNonMedication = "";
                for (Int32 i = 0; i <= chkAssNonMedication.Items.Count - 1; i++)
                {

                    if (chkAssNonMedication.Items[i].Selected == true)
                    {
                        strNonMedication += chkAssNonMedication.Items[i].Value + "|";
                    }

                }

                objPainAss.IPAD_NONMEDICATION = strNonMedication;
                objPainAss.IPAD_OTHER_INTERVENTIONS = txtAssOtherInterventions.Text;

                objPainAss.PainAssessmentDtlsAdd();

                AssessmentClear();
                BindAssessmentGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void Assessment_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvAssessmentSelectIndex"] = gvScanCard.RowIndex;
                gvAssessment.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblAssessID = (Label)gvScanCard.Cells[0].FindControl("lblAssessID");

                Label lblgvAssDate = (Label)gvScanCard.Cells[0].FindControl("lblgvAssDate");
                Label lblgvAssTime = (Label)gvScanCard.Cells[0].FindControl("lblgvAssTime");
                Label lblgvLocation = (Label)gvScanCard.Cells[0].FindControl("lblgvLocation");

                Label lblgvPainIntensity = (Label)gvScanCard.Cells[0].FindControl("lblgvPainIntensity");
                Label lblgvCharacterCode = (Label)gvScanCard.Cells[0].FindControl("lblgvCharacterCode");
                Label lblgvFrequency = (Label)gvScanCard.Cells[0].FindControl("lblgvFrequency");
                Label lblgvDuration = (Label)gvScanCard.Cells[0].FindControl("lblgvDuration");
                Label lblgvContinuetreat = (Label)gvScanCard.Cells[0].FindControl("lblgvContinuetreat");


                Label lblgvMedication = (Label)gvScanCard.Cells[0].FindControl("lblgvMedication");
                Label lblgvNonMedication = (Label)gvScanCard.Cells[0].FindControl("lblgvNonMedication");
                Label lblgvOtherInter = (Label)gvScanCard.Cells[0].FindControl("lblgvOtherInter");

                ViewState["IPAD_ASS_ID"] = lblAssessID.Text;


                txtAssDate.Text = lblgvAssDate.Text;


                string strAssTime = Convert.ToString(lblgvAssTime.Text);

                string[] arrAssTime = strAssTime.Split(':');
                if (arrAssTime.Length > 1)
                {
                    drpAssHour.SelectedValue = arrAssTime[0];
                    drpAssMin.SelectedValue = arrAssTime[1];

                }

                txtAssLocation.Text = lblgvLocation.Text;

                txtAssPainIntensity.Text = lblgvPainIntensity.Text;
                drpAssCharacterCode.SelectedValue  = lblgvCharacterCode.Text;
                txtAssFrequency.Text = lblgvFrequency.Text;
                txtAssDuration.Text = lblgvDuration.Text;

                if (lblgvContinuetreat.Text == "True")
                {
                    chkContinueCurTreat.Checked = true;
                }
                else
                {
                    chkContinueCurTreat.Checked = false;
                }


                txtAssMedication.Text = lblgvMedication.Text;


                string strNonMedication = lblgvNonMedication.Text;
                string[] arrNonMedication = strNonMedication.Split('|');



                for (Int32 i = 0; i <= chkAssNonMedication.Items.Count - 1; i++)
                {
                    for (Int32 j = 0; j <= arrNonMedication.Length - 1; j++)
                    {
                        if (arrNonMedication[j] == chkAssNonMedication.Items[i].Value)
                        {
                            chkAssNonMedication.Items[i].Selected = true;
                            goto EndArrLoop;
                        }

                    }

                EndArrLoop: ;

                }



                txtAssOtherInterventions.Text = lblgvOtherInter.Text;




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "ReAssessment_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void btnReAssAdd_Click(object sender, EventArgs e)
        {
            try
            {
                objPainAss = new IP_PainAssessment();
                objPainAss.BranchID = Convert.ToString(Session["Branch_ID"]);
                objPainAss.EMRID = Convert.ToString(Session["EMR_ID"]);
                objPainAss.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                if (ViewState["IPNAD_ASS_ID"] == "")
                {
                    objPainAss.IPNAD_ASS_ID = "0";
                }
                else
                {
                    objPainAss.IPNAD_ASS_ID = Convert.ToString(ViewState["IPNAD_ASS_ID"]);
                }

                objPainAss.IPNAD_DATE = txtReAssDate.Text.Trim() + " " + drpReAssHour.SelectedValue + ":" + drpReAssMin.SelectedValue + ":00";
                objPainAss.IPNAD_PAIN_INTENSITY = txtReAssPainIntensity.Text;
                objPainAss.IPNAD_PAIN_CHANGES = txtReAssPaintChanges.Text;
                objPainAss.IPNAD_INTERVENTIONS = txtReAssInterventions.Text;
                objPainAss.PainReAssessmentDtlsAdd();

                ReAssessmentClear();
                BindReAssessmentGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnReAssAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void ReAssessment_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvReAssessmentSelectIndex"] = gvScanCard.RowIndex;
                gvReAssessment.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblReAssessID = (Label)gvScanCard.Cells[0].FindControl("lblReAssessID");

                Label lblgvReAssDate = (Label)gvScanCard.Cells[0].FindControl("lblgvReAssDate");
                Label lblgvReAssTime = (Label)gvScanCard.Cells[0].FindControl("lblgvReAssTime");
                Label lblgvReAssPainIntensity = (Label)gvScanCard.Cells[0].FindControl("lblgvReAssPainIntensity");

                Label lblgvReAssCharacterCode = (Label)gvScanCard.Cells[0].FindControl("lblgvReAssCharacterCode");
                Label lblgvReAssInterventions = (Label)gvScanCard.Cells[0].FindControl("lblgvReAssInterventions");


                ViewState["IPNAD_ASS_ID"] = lblReAssessID.Text;


                txtReAssDate.Text = lblgvReAssDate.Text;
               string strReAssTime = lblgvReAssTime.Text;

               string[] arrReAssTime = strReAssTime.Split(':');
               if (arrReAssTime.Length > 1)
                {
                    drpReAssHour.SelectedValue = arrReAssTime[0];
                    drpReAssMin.SelectedValue = arrReAssTime[1];

                }



                txtReAssPainIntensity.Text = lblgvReAssPainIntensity.Text;

                txtReAssPaintChanges.Text = lblgvReAssCharacterCode.Text;
                txtReAssInterventions.Text = lblgvReAssInterventions.Text;





            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "ReAssessment_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

    }
}