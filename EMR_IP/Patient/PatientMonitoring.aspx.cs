﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.Patient
{
    public partial class PatientMonitoring : System.Web.UI.Page
    {

        # region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void HomeCareNotesBind(string strType)
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND IPN_TYPE = '" + strType + "'";
            Criteria += " AND IPN_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";


            IP_Monitoring objPT = new IP_Monitoring();
            ds = objPT.PTMonitoringGet(Criteria);

            if (strType == "DR_PROGRESS")
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvProgNotes.DataSource = ds;
                    gvProgNotes.DataBind();

                    string strPermission = hidPermission.Value;
                    if (strPermission == "1" || strPermission == "7")
                    {
                        gvProgNotes.Columns[0].Visible = false;
                    }
                }
                else
                {
                    gvProgNotes.DataBind();
                }

            }

            if (strType == "NU_PROGRESS")
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvEmerNotes.DataSource = ds;
                    gvEmerNotes.DataBind();

                    string strPermission = hidPermission1.Value;
                    if (strPermission == "1" || strPermission == "7")
                    {
                        gvEmerNotes.Columns[0].Visible = false;
                    }
                }
                else
                {
                    gvEmerNotes.DataBind();
                }

            }




        }

        void BindTime()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {

                drpProgHour.DataSource = DS;
                drpProgHour.DataTextField = "Name";
                drpProgHour.DataValueField = "Code";
                drpProgHour.DataBind();

                drpEmerHour.DataSource = DS;
                drpEmerHour.DataTextField = "Name";
                drpEmerHour.DataValueField = "Code";
                drpEmerHour.DataBind();
            }

            drpProgHour.Items.Insert(0, "00");
            drpProgHour.Items[0].Value = "00";

            drpEmerHour.Items.Insert(0, "00");
            drpEmerHour.Items[0].Value = "00";


            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {
               
                drpProgMin.DataSource = DS;
                drpProgMin.DataTextField = "Name";
                drpProgMin.DataValueField = "Code";
                drpProgMin.DataBind();

                drpEmerMin.DataSource = DS;
                drpEmerMin.DataTextField = "Name";
                drpEmerMin.DataValueField = "Code";
                drpEmerMin.DataBind();
            }

            drpProgMin.Items.Insert(0, "00");
            drpProgMin.Items[0].Value = "00";


            drpEmerMin.Items.Insert(0, "00");
            drpEmerMin.Items[0].Value = "00";

        }

        void BindStaff()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  AND HSFM_CATEGORY='Doctors' AND HSFM_SF_STATUS = 'Present' ";
            Criteria += " AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.GetStaffMaster(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpDoctor.DataSource = DS;
                drpDoctor.DataTextField = "FullName";
                drpDoctor.DataValueField = "HSFM_STAFF_ID";
                drpDoctor.DataBind();



            }


            drpDoctor.Items.Insert(0, "--- Select ---");
            drpDoctor.Items[0].Value = "";







            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindNurse()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1  AND HSFM_CATEGORY='Nurse' AND HSFM_SF_STATUS = 'Present' ";
            Criteria += " AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.GetStaffMaster(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpNurse.DataSource = DS;
                drpNurse.DataTextField = "FullName";
                drpNurse.DataValueField = "HSFM_STAFF_ID";
                drpNurse.DataBind();



            }


            drpNurse.Items.Insert(0, "--- Select ---");
            drpNurse.Items[0].Value = "";







            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void ClearProgNotes()
        {
            txtProgressNotes.Text = "";
            txtProgressNotes.Enabled = true;
            txtProgDate.Text = "";
            drpProgHour.SelectedIndex = 0;
            drpProgMin.SelectedIndex = 0;
            if (Convert.ToString(ViewState["ProgNotesSelectIndex"]) != "")
            {
                gvProgNotes.Rows[Convert.ToInt32(ViewState["ProgNotesSelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            ViewState["ProgNotesSelectIndex"] = "";
            ViewState["IPN_MON_ID"] = "";

            drpDoctor.SelectedIndex = 0;
        }

        void ClearEmerNotes()
        {
            txtEmergencyNotes.Text = "";
            txtEmergencyNotes.Enabled = true;
            txtEmerDate.Text = "";
            drpEmerHour.SelectedIndex = 0;
            drpEmerMin.SelectedIndex = 0;

            if (Convert.ToString(ViewState["EmerNotesSelectIndex"]) != "")
            {
                gvEmerNotes.Rows[Convert.ToInt32(ViewState["EmerNotesSelectIndex"])].BackColor = System.Drawing.Color.White;
            }
            ViewState["EmerNotesSelectIndex"] = "";
            ViewState["IPN_MON_ID_1"] = "";

            drpNurse.SelectedIndex = 0;
        }




        Boolean CheckPassword(string UserID, string Password)
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_REMARK ='" + UserID + "' AND   HUM_USER_PASS= '" + Password + "'";

            DataSet ds = new DataSet();
            //ds = dbo.UserMasterGet(drpBranch.SelectedValue, drpUsers.SelectedValue , txtPassword.Text);
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_DOCTOR_PROG_NOTE' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSaveProgNotes.Visible = false;


                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSaveProgNotes.Visible = false;

                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            //if (strPermission == "0")
            //{
            //    Response.Redirect("../Common/PermissionDenied.aspx");
            //}
        }

        void SetPermission1()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_NURSE_PROG_NOTE' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSaveEmerNotes.Visible = false;


                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSaveEmerNotes.Visible = false;

                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission1.Value = strPermission;
            //if (strPermission == "0")
            //{
            //    Response.Redirect("../Common/PermissionDenied.aspx");
            //}
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                    SetPermission1();
                }

                this.Page.Title = "PatientMonitoring";


                try
                {
                    ViewState["IPN_MON_ID"] = "";
                    ViewState["IPN_MON_ID_1"] = "";



                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtProgDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtEmerDate.Text = strFromDate.ToString("dd/MM/yyyy");



                    BindTime();
                    BindStaff();
                    BindNurse();
                    HomeCareNotesBind("DR_PROGRESS");
                    HomeCareNotesBind("NU_PROGRESS");


                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }

        protected void SelectProgNotes_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear();
                txtProgressNotes.Enabled = false;

                if (Convert.ToString(ViewState["ProgNotesSelectIndex"]) != "")
                {
                    gvProgNotes.Rows[Convert.ToInt32(ViewState["ProgNotesSelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ProgNotesSelectIndex"] = gvScanCard.RowIndex;

                gvProgNotes.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");



                Label lblProgId, lblProgressNotes, lblProgDate, lblProgTime, lblDoctorID;

                lblProgId = (Label)gvScanCard.Cells[0].FindControl("lblProgId");
                lblProgressNotes = (Label)gvScanCard.Cells[0].FindControl("lblProgressNotes");
                lblProgDate = (Label)gvScanCard.Cells[0].FindControl("lblProgDate");
                lblProgTime = (Label)gvScanCard.Cells[0].FindControl("lblProgTime");
                lblDoctorID = (Label)gvScanCard.Cells[0].FindControl("lblDoctorID");

                ViewState["IPN_MON_ID"] = lblProgId.Text;
                txtProgressNotes.Text = lblProgressNotes.Text;
                txtProgDate.Text = lblProgDate.Text;


                string strSTHour = Convert.ToString(lblProgTime.Text);

                string[] arrSTHour = strSTHour.Split(':');
                if (arrSTHour.Length > 1)
                {
                    drpProgHour.SelectedValue = arrSTHour[0];
                    drpProgMin.SelectedValue = arrSTHour[1];

                }
                drpDoctor.SelectedValue = lblDoctorID.Text;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "SelectProgNotes_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSaveProgNotes_Click(object sender, EventArgs e)
        {
            try
            {

                if (drpDoctor.SelectedIndex == 0)
                {
                    lblStatus.Text = "Select Doctor Name";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                if (txtDRPass.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Password";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                if (CheckPassword(drpDoctor.SelectedValue, txtDRPass.Text.Trim()) == false)
                {
                    lblStatus.Text = "Password  Wrong";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                IP_Monitoring objPT = new IP_Monitoring();
                objPT.BranchID = Convert.ToString(Session["Branch_ID"]);
                objPT.EMRID = Convert.ToString(Session["EMR_ID"]);
                objPT.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                objPT.IPN_MON_ID = Convert.ToString(ViewState["IPN_MON_ID"]);

                objPT.IPN_NOTES = txtProgressNotes.Text.Trim();
                objPT.IPN_TYPE = "DR_PROGRESS";
                objPT.IPN_MONITORING_DATE = txtProgDate.Text.Trim() + " " + drpProgHour.SelectedValue + ":" + drpProgMin.SelectedValue + ":00";
                objPT.IPN_STAFF_ID = drpDoctor.SelectedValue;
                objPT.IPN_STAFF_NAME = drpDoctor.SelectedItem.Text;
                objPT.UserID = Convert.ToString(Session["User_Code"]);
                objPT.PTMonitoringAdd();


                ClearProgNotes();
                HomeCareNotesBind("DR_PROGRESS");
                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;


            FunEnd: ;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSaveProgNotes_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteProgNotes_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblProgId;
                lblProgId = (Label)gvScanCard.Cells[0].FindControl("lblProgId");

                string FieldNameWithValues = " IPN_STATUS ='InActive'  ";
                string Criteria = " IPN_MON_ID = " + lblProgId.Text;
                CommonBAL objCom = new CommonBAL();
                objCom.fnUpdateTableData(FieldNameWithValues, "IP_PT_MONITORING", Criteria);

                lblStatus.Text = "Notes Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                ClearProgNotes();
                HomeCareNotesBind("DR_PROGRESS");

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "DeleteProgNotes_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnClearProgNotes_Click(object sender, EventArgs e)
        {
            ClearProgNotes();
        }

        protected void gvProgNotes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                lblSerial.Text = ((gvProgNotes.PageIndex * gvProgNotes.PageSize) + e.Row.RowIndex + 1).ToString();

                //BELOW CODING FOR STRIKE THROUGH OPTION
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                ImageButton DeleteProgNotes;
                DeleteProgNotes = (ImageButton)e.Row.FindControl("DeleteProgNotes");
                if (lblStatus.Text == "InActive")
                {
                    e.Row.Cells[0].Font.Strikeout = true;
                    e.Row.Cells[1].Font.Strikeout = true;
                    e.Row.Cells[2].Font.Strikeout = true;
                    e.Row.Cells[3].Font.Strikeout = true;
                    DeleteProgNotes.Visible = false;
                }


            }
        }


        protected void SelectgvEmerNotes_Click(object sender, EventArgs e)
        {
            try
            {
                //Clear();
                txtEmergencyNotes.Enabled = false;
                if (Convert.ToString(ViewState["EmerNotesSelectIndex"]) != "")
                {
                    gvEmerNotes.Rows[Convert.ToInt32(ViewState["EmerNotesSelectIndex"])].BackColor = System.Drawing.Color.White;
                }
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["EmerNotesSelectIndex"] = gvScanCard.RowIndex;


                gvEmerNotes.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");


                Label lblEmerId, lblEmergencyNotes, lblEmerDate, lblEmerTime, lblNurseID;

                lblEmerId = (Label)gvScanCard.Cells[0].FindControl("lblEmerId");
                lblEmergencyNotes = (Label)gvScanCard.Cells[0].FindControl("lblEmergencyNotes");
                lblEmerDate = (Label)gvScanCard.Cells[0].FindControl("lblEmerDate");
                lblEmerTime = (Label)gvScanCard.Cells[0].FindControl("lblEmerTime");
                lblNurseID = (Label)gvScanCard.Cells[0].FindControl("lblNurseID");


                ViewState["IPN_MON_ID_1"] = lblEmerId.Text;
                txtEmergencyNotes.Text = lblEmergencyNotes.Text;
                txtEmerDate.Text = lblEmerDate.Text;

                string strSTHour = Convert.ToString(lblEmerTime.Text);

                string[] arrSTHour = strSTHour.Split(':');
                if (arrSTHour.Length > 1)
                {
                    drpEmerHour.SelectedValue = arrSTHour[0];
                    drpEmerMin.SelectedValue = arrSTHour[1];

                }

                drpNurse.SelectedValue = lblNurseID.Text;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "SelectProgNotes_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSaveEmerNotes_Click(object sender, EventArgs e)
        {
            try
            {
                if (drpNurse.SelectedIndex == 0)
                {
                    lblStatus.Text = "Select Nurse Name";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                if (txtNurPass.Text.Trim() == "")
                {
                    lblStatus.Text = "Enter Password";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }


                if (CheckPassword(drpNurse.SelectedValue, txtNurPass.Text.Trim()) == false)
                {
                    lblStatus.Text = "Password  Wrong";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }

                IP_Monitoring objPT = new IP_Monitoring();


                objPT.BranchID = Convert.ToString(Session["Branch_ID"]);
                objPT.EMRID = Convert.ToString(Session["EMR_ID"]);
                objPT.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                objPT.IPN_MON_ID = Convert.ToString(ViewState["IPN_MON_ID_1"]);

                objPT.IPN_NOTES = txtEmergencyNotes.Text.Trim();
                objPT.IPN_TYPE = "NU_PROGRESS";
                objPT.IPN_MONITORING_DATE = txtEmerDate.Text.Trim() + " " + drpEmerHour.SelectedValue + ":" + drpEmerMin.SelectedValue + ":00";
                objPT.IPN_STAFF_ID = drpNurse.SelectedValue;
                objPT.IPN_STAFF_NAME = drpNurse.SelectedItem.Text;
                objPT.UserID = Convert.ToString(Session["User_Code"]);
                objPT.PTMonitoringAdd();


                ClearEmerNotes();
                HomeCareNotesBind("NU_PROGRESS");
                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;


            FunEnd: ;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSaveEmerNotes_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteEmerNotes_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblEmerId;
                lblEmerId = (Label)gvScanCard.Cells[0].FindControl("lblEmerId");


                string FieldNameWithValues = " IPN_STATUS ='InActive'  ";
                string Criteria = " IPN_MON_ID = " + lblEmerId.Text;
                CommonBAL objCom = new CommonBAL();
                objCom.fnUpdateTableData(FieldNameWithValues, "IP_PT_MONITORING", Criteria);


                lblStatus.Text = "Notes Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                ClearEmerNotes();
                HomeCareNotesBind("NU_PROGRESS");


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "DeleteEmerNotes_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnClearEmerNotes_Click(object sender, EventArgs e)
        {
            ClearEmerNotes();
        }

        protected void gvEmerNotes_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                lblSerial.Text = ((gvEmerNotes.PageIndex * gvEmerNotes.PageSize) + e.Row.RowIndex + 1).ToString();


                //BELOW CODING FOR STRIKE THROUGH OPTION
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                ImageButton DeleteProgNotes;
                DeleteProgNotes = (ImageButton)e.Row.FindControl("DeleteEmeriNotes");
                if (lblStatus.Text == "InActive")
                {
                    e.Row.Cells[0].Font.Strikeout = true;
                    e.Row.Cells[1].Font.Strikeout = true;
                    e.Row.Cells[2].Font.Strikeout = true;
                    e.Row.Cells[3].Font.Strikeout = true;
                    DeleteProgNotes.Visible = false;
                }
            }
        }




        protected void btnSaveTemplate_Click(object sender, EventArgs e)
        {
            Button btnSave = new Button();
            btnSave = (Button)sender;

            string strPath = "";
            if (btnSave.CommandName == "HC_ProgressNotes")
            {
                Session["Template"] = txtProgressNotes.Text.Trim();
                strPath = "TemplatesPopup.aspx?PageName=PatientMonitoring&CtrlName=HC_ProgressNotes";
            }
            if (btnSave.CommandName == "HC_EmergencyNotes")
            {
                Session["Template"] = txtEmergencyNotes.Text.Trim();
                strPath = "TemplatesPopup.aspx?PageName=PatientMonitoring&CtrlName=HC_EmergencyNotes";
            }




            ScriptManager.RegisterStartupScript(this, this.GetType(), "Templates", "window.open('" + strPath + "','_new','top=200,left=100,height=550,width=500,toolbar=no,scrollbars=yes,menubar=no');", true);


        }

        #endregion
    }
}