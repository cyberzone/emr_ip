﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreAnesthesia.aspx.cs" Inherits="EMR_IP.Patient.PreAnesthesia" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

     

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <title>IP Registration</title>

    <script language="javascript" type="text/javascript">

        
    </script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidthProc
        {
            width: 400px !important;
        }

            #divwidthProc div
            {
                width: 400px !important;
            }
    </style>
    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }


    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;

        }

        function BindCC(e, CC_ID,TargetID) {
            if (e.keyCode == 13) {

                var ChiefComp = document.getElementById(TargetID).value + document.getElementById(CC_ID).value + "\n";
                document.getElementById(TargetID).value = ChiefComp;
                document.getElementById(CC_ID).value = '';
            


                return false;
            }
        }


        function BindValue(vValue, TargetID) {
            
                document.getElementById(TargetID).value = vValue;
                return false;
            
        }

        function BMI(Wt, Ht) {
            if (Wt == 0 || Ht == 0)
                return 0;
            return ((parseFloat(Wt) / parseFloat(Ht) / parseFloat(Ht)) * 10000).toFixed(2)
        }

        function BindBMI(He_ID, Wt_ID, TargetID) {

            var wt = document.getElementById(Wt_ID).value;
            var ht = document.getElementById(He_ID).value;

            var varBmi = BMI(wt, ht)
            document.getElementById(TargetID).value = varBmi;
            Calculation1(varBmi);
        }



    </script>
</head>
<body>
    <form id="form1" runat="server">
         <input type="hidden" id="hidPermission" runat="server" value="9" />

        <div>
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>


            <br />
            <div style="padding-left: 60%; width: 100%;">
                <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
                    Saved Successfully 
                
                </div>
            </div>

            <table width="100%">
                <tr>
                    <td style="text-align: left; width: 50%;">
                        <h3 class="box-title">Pre Anesthesia Record</h3>
                    </td>
                    <td style="text-align: right; width: 50%;">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Saved Record" OnClick="btnSave_Click" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>

            <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel40" runat="server">
                            <ContentTemplate>
                                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px;font-weight:bold;" CssClass="label"></asp:Label>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
            <table style="width: 100%">
                <tr>
                    <td style="width: 50%; vertical-align: top;">
                        <asp:PlaceHolder ID="plhoPhyAssessmentLeft" runat="server"></asp:PlaceHolder>
                    </td>
                    <td style="width: 50%; vertical-align: top;">
                        <asp:PlaceHolder ID="plhoPhyAssessmentRight" runat="server"></asp:PlaceHolder>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
