﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.Patient
{
    public partial class PreOperativeChecklist : System.Web.UI.Page
    {
        static string strSessionDeptId;
        IP_PreOperativeChecklist objPreOp = new IP_PreOperativeChecklist();

        CommonBAL objCom = new CommonBAL();


        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindTime()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {
 



                drpProHour.DataSource = DS;
                drpProHour.DataTextField = "Name";
                drpProHour.DataValueField = "Code";
                drpProHour.DataBind();

                drpSigWardHour.DataSource = DS;
                drpSigWardHour.DataTextField = "Name";
                drpSigWardHour.DataValueField = "Code";
                drpSigWardHour.DataBind();

                drpSigOTHour.DataSource = DS;
                drpSigOTHour.DataTextField = "Name";
                drpSigOTHour.DataValueField = "Code";
                drpSigOTHour.DataBind();
            }

            drpProHour.Items.Insert(0, "00");
            drpProHour.Items[0].Value = "00";

            drpSigWardHour.Items.Insert(0, "00");
            drpSigWardHour.Items[0].Value = "00";

            drpSigOTHour.Items.Insert(0, "00");
            drpSigOTHour.Items[0].Value = "00";


            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {
                
                drpProMin.DataSource = DS;
                drpProMin.DataTextField = "Name";
                drpProMin.DataValueField = "Code";
                drpProMin.DataBind();

                drpSigWardMin.DataSource = DS;
                drpSigWardMin.DataTextField = "Name";
                drpSigWardMin.DataValueField = "Code";
                drpSigWardMin.DataBind();

                drpSigOTMin.DataSource = DS;
                drpSigOTMin.DataTextField = "Name";
                drpSigOTMin.DataValueField = "Code";
                drpSigOTMin.DataBind();
            }

            drpProMin.Items.Insert(0, "00");
            drpProMin.Items[0].Value = "00";

            drpSigWardMin.Items.Insert(0, "00");
            drpSigWardMin.Items[0].Value = "00";

            drpSigOTMin.Items.Insert(0, "00");
            drpSigOTMin.Items[0].Value = "00";


            

        }

        void BindPreOperativeSegmentMaster()
        {
            string Criteria = " 1=1 and IPSM_STATUS=1 ";
            objCom = new CommonBAL();

            DataSet DS = new DataSet();
            DS = objCom.fnGetFieldValue("*", "IP_PREOPERATIVE_SEGMENT_MASTER", Criteria, "IPSM_ORDER");

            gvPreOperative.DataSource = DS;
            gvPreOperative.DataBind();

        }

        void BindSurgon()
        {
            objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string Criteria = " 1=1  AND HSFM_SF_STATUS='Present' AND HSFM_CATEGORY='Doctors' ";
            Criteria += "  AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            DS = objCom.GetStaffMaster(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpSurgeon.DataSource = DS;
                drpSurgeon.DataTextField = "FullName";
                drpSurgeon.DataValueField = "HSFM_STAFF_ID";
                drpSurgeon.DataBind();

            }
            drpSurgeon.Items.Insert(0, "--- Select ---");
            drpSurgeon.Items[0].Value = "";

        }

        void BindNurse()
        {
            objCom = new CommonBAL();
            DataSet DS = new DataSet();

            string Criteria = " 1=1  AND HSFM_SF_STATUS='Present' AND HSFM_CATEGORY='Nurse' ";
            Criteria += "  AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";

            DS = objCom.GetStaffMaster(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpWardNurse.DataSource = DS;
                drpWardNurse.DataTextField = "FullName";
                drpWardNurse.DataValueField = "HSFM_STAFF_ID";
                drpWardNurse.DataBind();

                drpOTNurse.DataSource = DS;
                drpOTNurse.DataTextField = "FullName";
                drpOTNurse.DataValueField = "HSFM_STAFF_ID";
                drpOTNurse.DataBind();

            }
            drpWardNurse.Items.Insert(0, "--- Select ---");
            drpWardNurse.Items[0].Value = "";

            drpOTNurse.Items.Insert(0, "--- Select ---");
            drpOTNurse.Items[0].Value = "";

        }

        Boolean CheckPassword(string UserID, string Password)
        {
            string Criteria = " 1=1 ";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_REMARK ='" + UserID + "' AND   HUM_USER_PASS= '" + Password + "'";

            DataSet ds = new DataSet();
            //ds = dbo.UserMasterGet(drpBranch.SelectedValue, drpUsers.SelectedValue , txtPassword.Text);
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }



        void BindOPDData(string FieldID, out string strValue, out string strValueYes, out string strValueNurseYes)
        {
            strValue = "";
            strValueYes = "";
            strValueNurseYes = "";
            DataSet DS = new DataSet();

            string Criteria = " 1=1 and IPC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPC_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria += " and IPC_FIELD_ID='" + FieldID + "'";

            objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue(" * ", "IP_PREOPERATIVE_CHECKLIST", Criteria, "IPC_ID");
            foreach (DataRow DR in DS.Tables[0].Rows)
            {

                strValueYes = Convert.ToString(DR["IPC_VALUE"]);
                strValue = Convert.ToString(DR["IPC_REMARKS"]);
                strValueNurseYes = Convert.ToString(DR["IPC_VALUE_OTNURSE"]);
            }



        }

        void BIndProc()
        {
            DataSet DS = new DataSet();
            objPreOp = new IP_PreOperativeChecklist();

            string Criteria = " 1=1 and IPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DS = objPreOp.PreOperativeProceduresGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvProc.DataSource = DS;
                gvProc.DataBind();

            }
            else
            {
                gvProc.DataBind();
            }
        }

        void ClearProc()
        {


            Int32 R = 0;
            if (Convert.ToString(ViewState["PhySelectIndex"]) != "" && Convert.ToString(ViewState["PhySelectIndex"]) != null)
            {
                R = Convert.ToInt32(ViewState["PhySelectIndex"]);
                gvProc.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
            }

            ViewState["IPP_PRO_ID"] = "";
            ViewState["PhySelectIndex"] = "";

            txtServCode.Text = "";
            txtServName.Text = "";


            drpProHour.SelectedIndex = 0;
            drpProMin.SelectedIndex = 0;
            objPreOp.IPP_SURG_ID = drpSurgeon.SelectedValue;

        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_PREOPR_CHKLST' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Visible = false;
                btnAddPro.Visible = false;

                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSave.Visible = false;
                btnAddPro.Visible = false;

                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }
        #endregion


        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetServicessList(string prefixText)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string[] Data;

            ds = dbo.HaadServicessListGet("Procedure", prefixText, "", strSessionDeptId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["Code"]).Trim() + " ~ " + Convert.ToString(ds.Tables[0].Rows[i]["Description"]).Trim();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }

                BindTime();
                strSessionDeptId = Convert.ToString(Session["User_DeptID"]);
                this.Page.Title = "PreOperativeChecklist";
                CommonBAL objCom = new CommonBAL();
                string strDate = "", strTime = ""; ;
                strDate = objCom.fnGetDate("dd/MM/yyyy");
                strTime = objCom.fnGetDate("hh:mm:ss");

                txtORCallDate.Text = strDate;


                txtWardNurseDate.Text = strDate;
               


                txtOTNurseDate.Text = strDate;
                


                BindSurgon();
                BindNurse();
                BindPreOperativeSegmentMaster();
                BIndProc();
                ViewState["IPP_PRO_ID"] = "";
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (drpWardNurse.SelectedIndex == 0 && drpOTNurse.SelectedIndex == 0)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Enter Signature Details')", true);
                    goto FunEnd;
                }


                if (drpWardNurse.SelectedIndex != 0)
                {
                    if (CheckPassword(drpWardNurse.SelectedValue, txtWardNursePass.Text.Trim()) == false)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Ward Nurse Password wrong')", true);
                        goto FunEnd;
                    }
                }
                if (drpOTNurse.SelectedIndex != 0)
                {

                    if (CheckPassword(drpOTNurse.SelectedValue, txtOTNurse.Text.Trim()) == false)
                    {
                        ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('OT Nurse Password wrong')", true);
                        goto FunEnd;
                    }
                }


                objCom = new CommonBAL();
                string Criteria = " 1=1 and IPC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPC_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                objCom.fnDeleteTableData("IP_PREOPERATIVE_CHECKLIST", Criteria);


                //if (txtAssProcedure.Text != "")
                //{
                //    objPreOp = new IP_PreOperativeChecklist();
                //    objPreOp.BranchID = Convert.ToString(Session["Branch_ID"]);
                //    objPreOp.EMRID = Convert.ToString(Session["EMR_ID"]);
                //    objPreOp.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                //    objPreOp.IPC_FIELD_ID = "101";
                //    objPreOp.IPC_VALUE = "";
                //    objPreOp.IPC_REMARKS = txtAssProcedure.Text;
                //    objPreOp.IPC_VALUE_OTNURSE = "";
                //    objPreOp.PreOperativeChecklistAdd();

                //}

                //if (txtORCallDate.Text != "")
                //{
                //    objPreOp = new IP_PreOperativeChecklist();
                //    objPreOp.BranchID = Convert.ToString(Session["Branch_ID"]);
                //    objPreOp.EMRID = Convert.ToString(Session["EMR_ID"]);
                //    objPreOp.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                //    objPreOp.IPC_FIELD_ID = "102";
                //    objPreOp.IPC_VALUE = "";
                //    objPreOp.IPC_REMARKS = txtORCallDate.Text.Trim() + " " + drpProHour.SelectedValue + ":" + drpProMin.SelectedValue + ":00";
                //    objPreOp.IPC_VALUE_OTNURSE = "";
                //    objPreOp.PreOperativeChecklistAdd();

                //}

                //if (drpSurgeon.SelectedIndex != 0)
                //{
                //    objPreOp = new IP_PreOperativeChecklist();
                //    objPreOp.BranchID = Convert.ToString(Session["Branch_ID"]);
                //    objPreOp.EMRID = Convert.ToString(Session["EMR_ID"]);
                //    objPreOp.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                //    objPreOp.IPC_FIELD_ID = "103";
                //    objPreOp.IPC_VALUE = "";
                //    objPreOp.IPC_REMARKS = drpSurgeon.SelectedValue;
                //    objPreOp.IPC_VALUE_OTNURSE = "";
                //    objPreOp.PreOperativeChecklistAdd();

                //}




                for (Int32 i = 0; i < gvPreOperative.Rows.Count; i++)
                {

                    Label lblLevelType = (Label)gvPreOperative.Rows[i].FindControl("lblLevelType");
                    Label lblFieldID = (Label)gvPreOperative.Rows[i].FindControl("lblFieldID");
                    RadioButtonList radCheck = (RadioButtonList)gvPreOperative.Rows[i].FindControl("radCheck");
                    TextBox txtDtls = (TextBox)gvPreOperative.Rows[i].FindControl("txtDtls");
                    RadioButtonList radCheckOTNurse = (RadioButtonList)gvPreOperative.Rows[i].FindControl("radCheckOTNurse");

                    if (radCheck.SelectedIndex != -1)
                    {

                        objPreOp = new IP_PreOperativeChecklist();
                        objPreOp.BranchID = Convert.ToString(Session["Branch_ID"]);
                        objPreOp.EMRID = Convert.ToString(Session["EMR_ID"]);
                        objPreOp.PTID = Convert.ToString(Session["EMR_PT_ID"]);

                        objPreOp.IPC_FIELD_ID = lblFieldID.Text;
                        objPreOp.IPC_VALUE = radCheck.SelectedValue;
                        objPreOp.IPC_REMARKS = txtDtls.Text;
                        objPreOp.IPC_VALUE_OTNURSE = radCheckOTNurse.SelectedValue;
                        objPreOp.PreOperativeChecklistAdd();
                    }
                }
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);

            FunEnd: ;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void gvPreOperative_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblLevelType = (Label)e.Row.FindControl("lblLevelType");
                Label lblFieldID = (Label)e.Row.FindControl("lblFieldID");
                RadioButtonList radCheck = (RadioButtonList)e.Row.FindControl("radCheck");
                TextBox txtDtls = (TextBox)e.Row.FindControl("txtDtls");
                RadioButtonList radCheckOTNurse = (RadioButtonList)e.Row.FindControl("radCheckOTNurse");

                if (lblLevelType.Text == "1")
                {
                    radCheck.Visible = false;
                    txtDtls.Visible = false;
                    radCheckOTNurse.Visible = false;
                }
                else
                {
                    string strValue, strValueYes, strValueNurseYes;
                    BindOPDData(lblFieldID.Text, out strValue, out strValueYes, out strValueNurseYes);
                    radCheck.SelectedValue = strValueYes;
                    txtDtls.Text = strValue;
                    radCheckOTNurse.SelectedValue = strValueNurseYes;

                }

            }
        }


        protected void btnAddPror_Click(object sender, EventArgs e)
        {
            try
            {


                objPreOp = new IP_PreOperativeChecklist();
                objPreOp.BranchID = Convert.ToString(Session["Branch_ID"]);
                objPreOp.EMRID = Convert.ToString(Session["EMR_ID"]);
                objPreOp.IPP_PRO_ID = Convert.ToString(ViewState["IPP_PRO_ID"]);

                objPreOp.IPP_PRO_CODE = txtServCode.Text.Trim();
                objPreOp.IPP_PRO_NAME = txtServName.Text.Trim();
                objPreOp.IPP_QTY = "1";
                objPreOp.IPP_DATE = txtORCallDate.Text.Trim() + " " + drpProHour.SelectedValue + ":" + drpProMin.SelectedValue + ":00";
                objPreOp.IPP_SURG_ID = drpSurgeon.SelectedValue;
                objPreOp.IPP_SURG_NAME = drpSurgeon.SelectedItem.Text;
                objPreOp.PreOperativeProceduresAdd();

                ClearProc();
                BIndProc();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnAddPror_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }



        protected void PhySelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;
                ViewState["PhySelectIndex"] = gvScanCard.RowIndex;
                gvProc.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");


                Label lblgvProcID = (Label)gvScanCard.Cells[0].FindControl("lblgvProcID");
                Label lblgvTimeDesc = (Label)gvScanCard.Cells[0].FindControl("lblgvTimeDesc");

                Label lblPhyCode = (Label)gvScanCard.Cells[0].FindControl("lblPhyCode");
                Label lblPhyDesc = (Label)gvScanCard.Cells[0].FindControl("lblPhyDesc");


                Label lblPhyQTY = (Label)gvScanCard.Cells[0].FindControl("lblPhyQTY");
                Label lblgvSurgID = (Label)gvScanCard.Cells[0].FindControl("lblgvSurgID");



                ViewState["IPP_PRO_ID"] = lblgvProcID.Text;


                txtServCode.Text = lblPhyCode.Text;
                txtServName.Text = lblPhyDesc.Text;

                string strSTHour = lblgvTimeDesc.Text;

                string[] arrSTHour = strSTHour.Split(':');
                if (arrSTHour.Length > 1)
                {
                    drpProHour.SelectedValue = arrSTHour[0];
                    drpProMin.SelectedValue = arrSTHour[1];

                }


                drpSurgeon.SelectedValue = lblgvSurgID.Text;





            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void DeleteDiag_Click(object sender, EventArgs e)
        {

            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblgvProcID = (Label)gvScanCard.Cells[0].FindControl("lblgvProcID");



                objCom = new CommonBAL();

                string Criteria = " 1=1 AND IPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "' ";
                Criteria += " AND IPP_PRO_ID=" + lblgvProcID.Text ;

                objCom.fnDeleteTableData("IP_PREOPERATIVE_PROCEDURE", Criteria);
              

                ClearProc();
                BIndProc();




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "DeleteDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

       
    }
}