﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;

namespace EMR_IP.Patient
{
    public partial class RecoveryChart : System.Web.UI.Page
    {
        static string strSessionDeptId;

        CommonBAL objCom = new CommonBAL();
        IP_RecoveryChart objRec = new IP_RecoveryChart();

        DataSet DS = new DataSet();
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {

                drpArrivalHour.DataSource = DS;
                drpArrivalHour.DataTextField = "Name";
                drpArrivalHour.DataValueField = "Code";
                drpArrivalHour.DataBind();

                drpMoniHour.DataSource = DS;
                drpMoniHour.DataTextField = "Name";
                drpMoniHour.DataValueField = "Code";
                drpMoniHour.DataBind();

                drpMedStHour.DataSource = DS;
                drpMedStHour.DataTextField = "Name";
                drpMedStHour.DataValueField = "Code";
                drpMedStHour.DataBind();

                drpMedEndHour.DataSource = DS;
                drpMedEndHour.DataTextField = "Name";
                drpMedEndHour.DataValueField = "Code";
                drpMedEndHour.DataBind();

                drpMedGivenHour.DataSource = DS;
                drpMedGivenHour.DataTextField = "Name";
                drpMedGivenHour.DataValueField = "Code";
                drpMedGivenHour.DataBind();

                drpNurNoteHour.DataSource = DS;
                drpNurNoteHour.DataTextField = "Name";
                drpNurNoteHour.DataValueField = "Code";
                drpNurNoteHour.DataBind();

                drpWardCallHour.DataSource = DS;
                drpWardCallHour.DataTextField = "Name";
                drpWardCallHour.DataValueField = "Code";
                drpWardCallHour.DataBind();

                drpDischargeHour.DataSource = DS;
                drpDischargeHour.DataTextField = "Name";
                drpDischargeHour.DataValueField = "Code";
                drpDischargeHour.DataBind();




            }

            drpArrivalHour.Items.Insert(0, "00");
            drpArrivalHour.Items[0].Value = "00";

            drpMoniHour.Items.Insert(0, "00");
            drpMoniHour.Items[0].Value = "00";

            drpMedStHour.Items.Insert(0, "00");
            drpMedStHour.Items[0].Value = "00";

            drpMedEndHour.Items.Insert(0, "00");
            drpMedEndHour.Items[0].Value = "00";

            drpMedGivenHour.Items.Insert(0, "00");
            drpMedGivenHour.Items[0].Value = "00";

            drpNurNoteHour.Items.Insert(0, "00");
            drpNurNoteHour.Items[0].Value = "00";

            drpWardCallHour.Items.Insert(0, "00");
            drpWardCallHour.Items[0].Value = "00";

            drpDischargeHour.Items.Insert(0, "00");
            drpDischargeHour.Items[0].Value = "00";


            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {


                drpArrivalMin.DataSource = DS;
                drpArrivalMin.DataTextField = "Name";
                drpArrivalMin.DataValueField = "Code";
                drpArrivalMin.DataBind();

                drpMoniMin.DataSource = DS;
                drpMoniMin.DataTextField = "Name";
                drpMoniMin.DataValueField = "Code";
                drpMoniMin.DataBind();

                drpMedStMin.DataSource = DS;
                drpMedStMin.DataTextField = "Name";
                drpMedStMin.DataValueField = "Code";
                drpMedStMin.DataBind();


                drpMedEndMin.DataSource = DS;
                drpMedEndMin.DataTextField = "Name";
                drpMedEndMin.DataValueField = "Code";
                drpMedEndMin.DataBind();


                drpMedGivenMin.DataSource = DS;
                drpMedGivenMin.DataTextField = "Name";
                drpMedGivenMin.DataValueField = "Code";
                drpMedGivenMin.DataBind();


                drpNurNoteMin.DataSource = DS;
                drpNurNoteMin.DataTextField = "Name";
                drpNurNoteMin.DataValueField = "Code";
                drpNurNoteMin.DataBind();

                drpWardCallMin.DataSource = DS;
                drpWardCallMin.DataTextField = "Name";
                drpWardCallMin.DataValueField = "Code";
                drpWardCallMin.DataBind();

                drpDischargeMin.DataSource = DS;
                drpDischargeMin.DataTextField = "Name";
                drpDischargeMin.DataValueField = "Code";
                drpDischargeMin.DataBind();
            }

            drpArrivalMin.Items.Insert(0, "00");
            drpArrivalMin.Items[0].Value = "00";

            drpMoniMin.Items.Insert(0, "00");
            drpMoniMin.Items[0].Value = "00";

            drpMedStMin.Items.Insert(0, "00");
            drpMedStMin.Items[0].Value = "00";


            drpMedEndMin.Items.Insert(0, "00");
            drpMedEndMin.Items[0].Value = "00";


            drpMedGivenMin.Items.Insert(0, "00");
            drpMedGivenMin.Items[0].Value = "00";


            drpNurNoteMin.Items.Insert(0, "00");
            drpNurNoteMin.Items[0].Value = "00";

            drpWardCallMin.Items.Insert(0, "00");
            drpWardCallMin.Items[0].Value = "00";

            drpDischargeMin.Items.Insert(0, "00");
            drpDischargeMin.Items[0].Value = "00";


        }

        #region MasterFunction
        void BindAnesthesiaTyp()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='ANESTHESIA_TYPE' ";

            DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpAnesthesiaType.DataSource = DS;
                drpAnesthesiaType.DataTextField = "EM_NAME";
                drpAnesthesiaType.DataValueField = "EM_CODE";
                drpAnesthesiaType.DataBind();
            }
            drpAnesthesiaType.Items.Insert(0, "--- Select ---");
            drpAnesthesiaType.Items[0].Value = "";

        }

        void BindPTCondition()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='PATIENT_CONDITION' ";

            DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpPTCondition.DataSource = DS;
                drpPTCondition.DataTextField = "EM_NAME";
                drpPTCondition.DataValueField = "EM_CODE";
                drpPTCondition.DataBind();
            }

            drpPTCondition.Items.Insert(0, "--- Select ---");
            drpPTCondition.Items[0].Value = "";
        }

        void BindVentilation()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='VENTILATION_TYPE' ";

            DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpVentilation.DataSource = DS;
                drpVentilation.DataTextField = "EM_NAME";
                drpVentilation.DataValueField = "EM_CODE";
                drpVentilation.DataBind();
            }

            drpVentilation.Items.Insert(0, "--- Select ---");
            drpVentilation.Items[0].Value = "";
        }

        void BindVascularCases()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='VASCULAR_CASES' ";

            DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpVascularCases.DataSource = DS;
                drpVascularCases.DataTextField = "EM_NAME";
                drpVascularCases.DataValueField = "EM_CODE";
                drpVascularCases.DataBind();
            }

            drpVascularCases.Items.Insert(0, "--- Select ---");
            drpVascularCases.Items[0].Value = "";
        }

        void BindDrainage()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='DRAINAGE' ";

            DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpDrainage.DataSource = DS;
                drpDrainage.DataTextField = "EM_NAME";
                drpDrainage.DataValueField = "EM_CODE";
                drpDrainage.DataBind();
            }

            drpDrainage.Items.Insert(0, "--- Select ---");
            drpDrainage.Items[0].Value = "";
        }

        void BindSpecialEvents()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='PATIENT_SPECIAL_EVENTS' ";

            DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpPTSpecialEvents.DataSource = DS;
                drpPTSpecialEvents.DataTextField = "EM_NAME";
                drpPTSpecialEvents.DataValueField = "EM_CODE";
                drpPTSpecialEvents.DataBind();
            }

            drpPTSpecialEvents.Items.Insert(0, "--- Select ---");
            drpPTSpecialEvents.Items[0].Value = "";
        }

        void BindDisIssueType()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='DISCHARGE_ROOM_ISSUE_TYPE' AND (EM_PARENT IS NULL OR  EM_PARENT='') ";

            DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpDisIssueType.DataSource = DS;
                drpDisIssueType.DataTextField = "EM_NAME";
                drpDisIssueType.DataValueField = "EM_CODE";
                drpDisIssueType.DataBind();
            }

            drpDisIssueType.Items.Insert(0, "--- Select ---");
            drpDisIssueType.Items[0].Value = "";
        }


        void BindDisIssueSubType()
        {
            drpDisIssueSubType.Items.Clear();
            objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='DISCHARGE_ROOM_ISSUE_TYPE' AND   EM_PARENT ='" + drpDisIssueType.SelectedValue + "'";

            DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpDisIssueSubType.DataSource = DS;
                drpDisIssueSubType.DataTextField = "EM_NAME";
                drpDisIssueSubType.DataValueField = "EM_CODE";
                drpDisIssueSubType.DataBind();
            }

            drpDisIssueSubType.Items.Insert(0, "--- Select ---");
            drpDisIssueSubType.Items[0].Value = "";
        }



        void BindHMDisIssueType()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='DISCHARGE_HOME_ISSUE_TYPE' AND (EM_PARENT IS NULL OR  EM_PARENT='') ";

            DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpHmDisIssueType.DataSource = DS;
                drpHmDisIssueType.DataTextField = "EM_NAME";
                drpHmDisIssueType.DataValueField = "EM_CODE";
                drpHmDisIssueType.DataBind();
            }

            drpHmDisIssueType.Items.Insert(0, "--- Select ---");
            drpHmDisIssueType.Items[0].Value = "";
        }

        void BindHMDisIssueSubType()
        {
            drpHmDisIssueSubType.Items.Clear();
            objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='DISCHARGE_HOME_ISSUE_TYPE' AND   EM_PARENT ='" + drpHmDisIssueType.SelectedValue + "'";

            DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpHmDisIssueSubType.DataSource = DS;
                drpHmDisIssueSubType.DataTextField = "EM_NAME";
                drpHmDisIssueSubType.DataValueField = "EM_CODE";
                drpHmDisIssueSubType.DataBind();
            }

            drpHmDisIssueSubType.Items.Insert(0, "--- Select ---");
            drpHmDisIssueSubType.Items[0].Value = "";
        }


        void BindDisAssTime()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='DISCHARGE_ROOM_ASSESSMENT_TIME' AND (EM_PARENT IS NULL OR  EM_PARENT='') ";

            DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpDisAssTime.DataSource = DS;
                drpDisAssTime.DataTextField = "EM_NAME";
                drpDisAssTime.DataValueField = "EM_CODE";
                drpDisAssTime.DataBind();
            }

            drpDisAssTime.Items.Insert(0, "--- Select ---");
            drpDisAssTime.Items[0].Value = "";
        }


        void BindHMDisAssTime()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='DISCHARGE_HOME_ASSESSMENT_TIME' AND (EM_PARENT IS NULL OR  EM_PARENT='') ";

            DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                drpHmDisAssTime.DataSource = DS;
                drpHmDisAssTime.DataTextField = "EM_NAME";
                drpHmDisAssTime.DataValueField = "EM_CODE";
                drpHmDisAssTime.DataBind();
            }

            drpHmDisAssTime.Items.Insert(0, "--- Select ---");
            drpHmDisAssTime.Items[0].Value = "";

        }

        void BindRoute()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND EPR_ACTIVE=1 ";
            Criteria += " AND EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.EMRRouteGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpRoute.DataSource = DS;
                drpRoute.DataTextField = "EPR_NAME";
                drpRoute.DataValueField = "EPR_CODE";
                drpRoute.DataBind();
            }
            drpRoute.Items.Insert(0, "--- Select ---");
            drpRoute.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindStaff()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 "; // AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.GetStaffMaster(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpGivenBy.DataSource = DS;
                drpGivenBy.DataTextField = "FullName";
                drpGivenBy.DataValueField = "HSFM_STAFF_ID";
                drpGivenBy.DataBind();
            }
            drpGivenBy.Items.Insert(0, "--- Select ---");
            drpGivenBy.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindNurse()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND HSFM_CATEGORY='Nurse'"; // AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.GetStaffMaster(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpRRNurse.DataSource = DS;
                drpRRNurse.DataTextField = "FullName";
                drpRRNurse.DataValueField = "HSFM_STAFF_ID";
                drpRRNurse.DataBind();

                drpAnesthesiologist.DataSource = DS;
                drpAnesthesiologist.DataTextField = "FullName";
                drpAnesthesiologist.DataValueField = "HSFM_STAFF_ID";
                drpAnesthesiologist.DataBind();

                drpWardNurse.DataSource = DS;
                drpWardNurse.DataTextField = "FullName";
                drpWardNurse.DataValueField = "HSFM_STAFF_ID";
                drpWardNurse.DataBind();

            }
            drpRRNurse.Items.Insert(0, "--- Select ---");
            drpRRNurse.Items[0].Value = "";

            drpAnesthesiologist.Items.Insert(0, "--- Select ---");
            drpAnesthesiologist.Items[0].Value = "";

            drpWardNurse.Items.Insert(0, "--- Select ---");
            drpWardNurse.Items[0].Value = "";



        }
        #endregion


        void BindAnesthesiaDtl()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 and IAD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IAD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = new DataSet();
            DS = objRec.AnesthesiaDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    drpAnesthesiaType.SelectedValue = Convert.ToString(DR["IAD_ANESTHESIA_TYPE"]);
                    drpPTCondition.SelectedValue = Convert.ToString(DR["IAD_PT_CONDITION"]);

                    drpVentilation.SelectedValue = Convert.ToString(DR["IAD_VENTILATION"]);
                    drpProtective.SelectedValue = Convert.ToString(DR["IAD_PROT_AIRWAY_REFLEXES"]);

                    drpAirwayObstSigns.SelectedValue = Convert.ToString(DR["IAD_AIRWAY_OBST_SIGNS"]);
                    drpArtiAirwayDevice.SelectedValue = Convert.ToString(DR["IAD_ARTI_AIRWAY_DEVICE"]);

                    drpVascularCases.SelectedValue = Convert.ToString(DR["IAD_VASCULAR_CASES"]);
                    drpDrainage.SelectedValue = Convert.ToString(DR["IAD_DRAINAGE"]);

                    drpPTSpecialEvents.SelectedValue = Convert.ToString(DR["IAD_PT_SPECIAL_EVENTS"]);
                    drpRRProcedures.SelectedValue = Convert.ToString(DR["IAD_RR_PROCEDURES"]);



                }

            }


        }

        void BindRecoveryMaster()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 and IRRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IRRM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = new DataSet();
            DS = objRec.RecoveryRoomMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    string strPainScore = "";
                    strPainScore = Convert.ToString(DR["IRRM_PAIN_SCORE"]);

                    if (strPainScore != "")
                    {
                        if (strPainScore == "0")
                            PainValue0.Checked = true;
                        if (strPainScore == "1")
                            PainValue1.Checked = true;
                        if (strPainScore == "2")
                            PainValue2.Checked = true;
                        if (strPainScore == "3")
                            PainValue3.Checked = true;
                        if (strPainScore == "4")
                            PainValue4.Checked = true;
                        if (strPainScore == "5")
                            PainValue5.Checked = true;
                        if (strPainScore == "6")
                            PainValue6.Checked = true;
                        if (strPainScore == "7")
                            PainValue7.Checked = true;
                        if (strPainScore == "8")
                            PainValue8.Checked = true;
                        if (strPainScore == "9")
                            PainValue9.Checked = true;
                        if (strPainScore == "10")
                            PainValue10.Checked = true;

                    }

                    txtReceivedFrom.Text = Convert.ToString(DR["IRRM_RECEIVED_FROM"]);
                    txtAccompaniedBy.Text = Convert.ToString(DR["IRRM_ACCOMPANIED_BY"]);

                    txtArrivalDate.Text = Convert.ToString(DR["IRRM_ARRIVAL_DATEDesc"]);



                    string strIA_TIME_IN = Convert.ToString(DR["IRRM_ARRIVAL_DATETimeDesc"]);

                    string[] arrIA_TIME_IN = strIA_TIME_IN.Split(':');
                    if (arrIA_TIME_IN.Length > 1)
                    {
                        drpArrivalHour.SelectedValue = arrIA_TIME_IN[0];
                        drpArrivalMin.SelectedValue = arrIA_TIME_IN[1];

                    }

                    drpMotorBlockIntensity.SelectedValue = Convert.ToString(DR["IRRM_MOTOR_BLOCK_INTENSITY"]);
                    drpSedationAgitationScore.SelectedValue = Convert.ToString(DR["IRRM_SEDATION_AGITATION_SCORE"]);
                    drpPatientTeaching.SelectedValue = Convert.ToString(DR["IRRM_PATIENT_TEACHING"]);
                    drpDischargedWith.SelectedValue = Convert.ToString(DR["IRRM_DISCHARGED_WITH"]);
                    drpDischargedTo.SelectedValue = Convert.ToString(DR["IRRM_DISCHARGED_TO"]);

                    txtPostOpInstComment.Text = Convert.ToString(DR["IRRM_COMMENT"]);

                    drpRRNurse.SelectedValue = Convert.ToString(DR["IRRM_RR_NURSE"]);
                    drpAnesthesiologist.SelectedValue = Convert.ToString(DR["IRRM_ANESTHESIOLOGIST"]);
                    drpWardNurse.SelectedValue = Convert.ToString(DR["IRRM_WARD_NURSE"]);

                    txtWardCallDate.Text = Convert.ToString(DR["IRRM_WARD_CALL_TIME_DATEDesc"]);
                    

                    string strIRRM_WARD_CALL_TIME = Convert.ToString(DR["IRRM_WARD_CALL_TIMEDesc"]);

                    string[] arrIRRM_WARD_CALL_TIME = strIRRM_WARD_CALL_TIME.Split(':');
                    if (arrIRRM_WARD_CALL_TIME.Length > 1)
                    {
                        drpWardCallHour.SelectedValue = arrIRRM_WARD_CALL_TIME[0];
                        drpWardCallMin.SelectedValue = arrIRRM_WARD_CALL_TIME[1];

                    }


                    txtDischargeDate.Text = Convert.ToString(DR["IRRM_DISCHARGE_DATEDesc"]);

                    string strIRRM_DISCHARGE_DATE_TIME = Convert.ToString(DR["IRRM_DISCHARGE_DATE_TIMEDesc"]);

                    string[] arrIRRM_DISCHARGE_DATE_TIME = strIRRM_DISCHARGE_DATE_TIME.Split(':');
                    if (arrIRRM_DISCHARGE_DATE_TIME.Length > 1)
                    {
                        drpDischargeHour.SelectedValue = arrIRRM_DISCHARGE_DATE_TIME[0];
                        drpDischargeMin.SelectedValue = arrIRRM_DISCHARGE_DATE_TIME[1];

                    }


                  

                }

            }




        }


        void BindMonitoringGrid()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 and IRRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IRRM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = new DataSet();
            DS = objRec.RecoveryRoomMonitoringGet(Criteria);

            gvMonitoring.DataBind();
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvMonitoring.DataSource = DS;
                gvMonitoring.DataBind();

            }


        }

        void BindMonitoring()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 AND IRRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IRRM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria += " AND IRRM_MON_ID = " + Convert.ToString(ViewState["IRRM_MON_ID"]);


            DS = new DataSet();
            DS = objRec.RecoveryRoomMonitoringGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    txtMonitoringDate.Text = Convert.ToString(DR["IRRM_TIMEDateDesc"]);

                    string strIA_TIME_IN = Convert.ToString(DR["IRRM_TIMETimeDesc"]);

                    string[] arrIA_TIME_IN = strIA_TIME_IN.Split(':');
                    if (arrIA_TIME_IN.Length > 1)
                    {
                        drpMoniHour.SelectedValue = arrIA_TIME_IN[0];
                        drpMoniMin.SelectedValue = arrIA_TIME_IN[1];

                    }
                    txtBpSystolic.Value = Convert.ToString(DR["IRRM_BP_SYSTOLIC"]);
                    txtBpDiastolic.Value = Convert.ToString(DR["IRRM_BP_DIASTOLIC"]);
                    txtPulse.Value = Convert.ToString(DR["IRRM_PULSE"]);
                    txtSp02.Value = Convert.ToString(DR["IRRM_SPO2"]);
                    txtRespiration.Value = Convert.ToString(DR["IRRM_RESPIRATION"]);
                    txtTemperatureF.Value = Convert.ToString(DR["IRRM_TEMP"]);
                    txtTemperatureC.Value = Convert.ToString(DR["IRRM_TEMP_C"]);
                    txtPainScore.Value = Convert.ToString(DR["IRRM_PAIN_SCORE"]);
                    txtMotorBlock.Value = Convert.ToString(DR["IRRM_MOTOR_BLOCK"]);

                    txtMonitoringComment.Text = Convert.ToString(DR["IRRM_COMMENT"]);




                }

            }


        }

        void MonitoringClear()
        {
            Int32 R = 0;
            if (Convert.ToString(ViewState["gvMonitoringSelectIndex"]) != "" && Convert.ToString(ViewState["gvMonitoringSelectIndex"]) != null)
            {
                R = Convert.ToInt32(ViewState["gvMonitoringSelectIndex"]);
                gvMonitoring.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
            }
            ViewState["IRRM_MON_ID"] = "";
            ViewState["gvMonitoringSelectIndex"] = "";

            txtBpSystolic.Value = "";
            txtBpDiastolic.Value = "";
            txtPulse.Value = "";
            txtSp02.Value = "";
            txtRespiration.Value = "";
            txtTemperatureF.Value = "";
            txtTemperatureC.Value = "";
            txtPainScore.Value = "";
            txtMotorBlock.Value = "";

            txtMonitoringComment.Text = "";

        }

        void BindMedicationGrid()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 and IRRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IRRM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = new DataSet();
            DS = objRec.RecoveryRoomMedicationGet(Criteria);

            gvMonitoring.DataBind();
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvMedication.DataSource = DS;
                gvMedication.DataBind();

            }


        }

        void BindMedication()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 AND IRRM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IRRM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria += " AND IRRM_MED_ID = " + Convert.ToString(ViewState["IRRM_MED_ID"]);


            DS = new DataSet();
            DS = objRec.RecoveryRoomMedicationGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {


                    txtSolution.Text = Convert.ToString(DR["IRRM_SOLUTION"]);
                    txtVolume.Text = Convert.ToString(DR["IRRM_VOLUME"]);
                    txtMedicationStarted.Text = Convert.ToString(DR["IRRM_STARTEDDateDesc"]);


                    string strIA_TIME_IN = Convert.ToString(DR["IRRM_STARTEDTimeDesc"]);

                    string[] arrIA_TIME_IN = strIA_TIME_IN.Split(':');
                    if (arrIA_TIME_IN.Length > 1)
                    {
                        drpMedStHour.SelectedValue = arrIA_TIME_IN[0];
                        drpMedStMin.SelectedValue = arrIA_TIME_IN[1];

                    }


                    txtMedicationSFinished.Text = Convert.ToString(DR["IRRM_FINISHEDDateDesc"]);


                    string strFinish = Convert.ToString(DR["IRRM_FINISHEDTimeDesc"]);

                    string[] arrstrFinish = strFinish.Split(':');
                    if (arrstrFinish.Length > 1)
                    {
                        drpMedEndHour.SelectedValue = arrstrFinish[0];
                        drpMedEndMin.SelectedValue = arrstrFinish[1];

                    }


                    txtServCode.Text = Convert.ToString(DR["IRRM_DRUG_CODE"]);
                    txtServName.Text = Convert.ToString(DR["IRRM_DRUG_NAME"]);
                    txtDossage1.Text = Convert.ToString(DR["IRRM_DOSE"]);
                    drpRoute.SelectedValue = Convert.ToString(DR["IRRM_ROUTE"]);
                    txtMedicationSGivenTimeDate.Text = Convert.ToString(DR["IRRM_TIME_GIVENDateDesc"]);

                    string strGiven = Convert.ToString(DR["IRRM_TIME_GIVENTimeDesc"]);

                    string[] arrstrGiven = strGiven.Split(':');
                    if (arrstrGiven.Length > 1)
                    {
                        drpMedGivenHour.SelectedValue = arrstrGiven[0];
                        drpMedGivenMin.SelectedValue = arrstrGiven[1];

                    }


                    drpGivenBy.SelectedValue = Convert.ToString(DR["IRRM_GIVEN_BY"]);
                }

            }


        }

        void MedicationClear()
        {
            Int32 R = 0;
            if (Convert.ToString(ViewState["gvMedicationSelectIndex"]) != "" && Convert.ToString(ViewState["gvMedicationSelectIndex"]) != null)
            {
                R = Convert.ToInt32(ViewState["gvMedicationSelectIndex"]);
                gvMedication.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");

            }
            ViewState["IRRM_MED_ID"] = "";
            ViewState["gvMedicationSelectIndex"] = "";

            txtSolution.Text = "";
            txtVolume.Text = "";
            // txtMedicationStarted.Text = "";
            //  txtMedicationSFinished.Text = "";
            txtServCode.Text = "";
            txtServName.Text = "";
            txtDossage1.Text = "";
            if (drpRoute.Items.Count > 0)
            {
                drpRoute.SelectedIndex = 0;
            }
            if (drpGivenBy.Items.Count > 0)
            {
                drpGivenBy.SelectedIndex = 0;
            }
            // txtMedicationSGivenTime.Text = "";



        }



        void BindRecoveryNurseNotesGrid()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 and IRNN_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IRNN_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = new DataSet();
            DS = objRec.RecoveryNurseNotesGet(Criteria);

            gvNurseNotes.DataBind();
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvNurseNotes.DataSource = DS;
                gvNurseNotes.DataBind();

            }


        }


        void BindRecoveryNurseNotes()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 AND IRNN_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IRNN_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria += " AND IRNN_NOTES_ID = " + Convert.ToString(ViewState["IRNN_NOTES_ID"]);


            DS = new DataSet();
            DS = objRec.RecoveryNurseNotesGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    txtNurseNotesDate.Text = Convert.ToString(DR["IRNN_TIMEDateDesc"]);


                    string strGiven = Convert.ToString(DR["IRNN_TIMETimeDesc"]);

                    string[] arrstrGiven = strGiven.Split(':');
                    if (arrstrGiven.Length > 1)
                    {
                        drpNurNoteHour.SelectedValue = arrstrGiven[0];
                        drpNurNoteMin.SelectedValue = arrstrGiven[1];

                    }
                    txtNurseNotesRemarks.Text = Convert.ToString(DR["IRNN_REMARKS"]);
                }

            }


        }


        void NurseNotesClear()
        {

            Int32 R = 0;
            if (Convert.ToString(ViewState["gvNurseNotesSelectIndex"]) != "" && Convert.ToString(ViewState["gvNurseNotesSelectIndex"]) != null)
            {
                R = Convert.ToInt32(ViewState["gvNurseNotesSelectIndex"]);
                gvNurseNotes.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");

            }

            ViewState["IRNN_NOTES_ID"] = "";
            ViewState["gvNurseNotesSelectIndex"] = "";

            txtNurseNotesRemarks.Text = "";




        }


        void GetScoreValue(string Type, string strCode, out Int32 ScoreValue)
        {
            ScoreValue = 0;

            objCom = new CommonBAL();
            string Criteria = " 1=1 and EM_ACTIVE=1 AND EM_TYPE='" + Type + "' AND   EM_CODE ='" + strCode + "'";

            DS = new DataSet();
            DS = objCom.EMR_MasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("EM_VALUE") == false)
                {
                    ScoreValue = Convert.ToInt32(DS.Tables[0].Rows[0]["EM_VALUE"]);
                }
            }

        }


        void BindDischargeRoomGrid()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 and IRRS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IRRS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria += "  AND  IRRS_DISCHARGE_TYPE='DISCHARGE_ROOM'  AND EM_TYPE='DISCHARGE_ROOM_ASSESSMENT_TIME'   ";
            DS = new DataSet();
            DS = objRec.RecoveryRoomPTStatusGet(Criteria);

            gvDischargeRoom.DataBind();
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvDischargeRoom.DataSource = DS;
                gvDischargeRoom.DataBind();

            }


        }


        void BindDischargeRoom()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 and IRRS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IRRS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria += " AND  IRRS_DISCHARGE_TYPE='DISCHARGE_ROOM' AND EM_TYPE='DISCHARGE_ROOM_ASSESSMENT_TIME'   AND IRRS_STATUS_ID = " + Convert.ToString(ViewState["IRRS_STATUS_ID"]);


            DS = new DataSet();
            DS = objRec.RecoveryRoomPTStatusGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                string strSubType = "";
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    drpDisIssueType.SelectedValue = Convert.ToString(DR["IRRS_ISSUE_TYPE"]);
                    strSubType = Convert.ToString(DR["IRRS_ISSUE_SUB_TYPE"]);
                    drpDisAssTime.SelectedValue = Convert.ToString(DR["IRRS_ISSUE_TIMING"]);

                    BindDisIssueSubType();
                    drpDisIssueSubType.SelectedValue = strSubType;
                }

            }


        }

        void DischargeRoomClear()
        {
            Int32 R = 0;
            if (Convert.ToString(ViewState["gvDischargeRoomSelectIndex"]) != "" && Convert.ToString(ViewState["gvDischargeRoomSelectIndex"]) != null)
            {
                R = Convert.ToInt32(ViewState["gvDischargeRoomSelectIndex"]);
                gvDischargeRoom.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");

            }
            ViewState["IRRS_STATUS_ID"] = "";
            ViewState["gvDischargeRoomSelectIndex"] = "";

            if (drpDisIssueType.Items.Count > 0)
            {
                drpDisIssueType.SelectedIndex = 0;
            }

            if (drpDisIssueSubType.Items.Count > 0)
            {
                drpDisIssueSubType.SelectedIndex = 0;
            }

            if (drpDisAssTime.Items.Count > 0)
            {
                drpDisAssTime.SelectedIndex = 0;
            }

        }


        void BindDischargeHomeGrid()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 and IRRS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IRRS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria += "  AND  IRRS_DISCHARGE_TYPE='DISCHARGE_HOME' AND EM_TYPE='DISCHARGE_HOME_ASSESSMENT_TIME' ";

            DS = new DataSet();
            DS = objRec.RecoveryRoomPTStatusGet(Criteria);

            gvDischargeHome.DataBind();
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvDischargeHome.DataSource = DS;
                gvDischargeHome.DataBind();

            }


        }

        void BindDischargeHome()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 and IRRS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IRRS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria += "AND  IRRS_DISCHARGE_TYPE='DISCHARGE_HOME' AND EM_TYPE='DISCHARGE_HOME_ASSESSMENT_TIME'   AND IRRS_STATUS_ID = " + Convert.ToString(ViewState["IRRS_STATUS_ID_HM"]);


            DS = new DataSet();
            DS = objRec.RecoveryRoomPTStatusGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                string strSubType = "";
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    drpHmDisIssueType.SelectedValue = Convert.ToString(DR["IRRS_ISSUE_TYPE"]);
                    strSubType = Convert.ToString(DR["IRRS_ISSUE_SUB_TYPE"]);
                    drpHmDisAssTime.SelectedValue = Convert.ToString(DR["IRRS_ISSUE_TIMING"]);

                    BindHMDisIssueSubType();
                    drpHmDisIssueSubType.SelectedValue = strSubType;
                }

            }


        }

        void DischargeHomeClear()
        {
            Int32 R = 0;
            if (Convert.ToString(ViewState["gvDischargeHomeSelectIndex"]) != "" && Convert.ToString(ViewState["gvDischargeHomeSelectIndex"]) != null)
            {
                R = Convert.ToInt32(ViewState["gvDischargeHomeSelectIndex"]);
                gvDischargeRoom.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
            }
            ViewState["IRRS_STATUS_ID_HM"] = "";
            ViewState["gvDischargeHomeSelectIndex"] = "";

            if (drpHmDisIssueType.Items.Count > 0)
            {
                drpHmDisIssueType.SelectedIndex = 0;
            }

            if (drpHmDisIssueSubType.Items.Count > 0)
            {
                drpHmDisIssueSubType.SelectedIndex = 0;
            }

            if (drpHmDisAssTime.Items.Count > 0)
            {
                drpHmDisAssTime.SelectedIndex = 0;
            }

        }

        #region SaveFunction

        void SaveAnesthesiaDtls()
        {


            objRec = new IP_RecoveryChart();
            objRec.BranchID = Convert.ToString(Session["Branch_ID"]);
            objRec.EMRID = Convert.ToString(Session["EMR_ID"]);
            objRec.PTID = Convert.ToString(Session["EMR_PT_ID"]);

            objRec.IAD_ANESTHESIA_TYPE = drpAnesthesiaType.SelectedValue;
            objRec.IAD_PT_CONDITION = drpPTCondition.SelectedValue;

            objRec.IAD_VENTILATION = drpVentilation.SelectedValue;
            objRec.IAD_PROT_AIRWAY_REFLEXES = drpProtective.SelectedValue;

            objRec.IAD_AIRWAY_OBST_SIGNS = drpAirwayObstSigns.SelectedValue;
            objRec.IAD_ARTI_AIRWAY_DEVICE = drpArtiAirwayDevice.SelectedValue;

            objRec.IAD_VASCULAR_CASES = drpVascularCases.SelectedValue;
            objRec.IAD_DRAINAGE = drpDrainage.SelectedValue;

            objRec.IAD_PT_SPECIAL_EVENTS = drpPTSpecialEvents.SelectedValue;
            objRec.IAD_RR_PROCEDURES = drpRRProcedures.SelectedValue;
            objRec.UserID = Convert.ToString(Session["User_ID"]);
            objRec.AnesthesiaDtlsAdd();

        }

        void SaveRecoveryRoomMaster()
        {

            string strPainScore = "";
            if (PainValue0.Checked == true)
                strPainScore = "0";
            if (PainValue1.Checked == true)
                strPainScore = "1";
            if (PainValue2.Checked == true)
                strPainScore = "2";
            if (PainValue3.Checked == true)
                strPainScore = "3";
            if (PainValue4.Checked == true)
                strPainScore = "4";
            if (PainValue5.Checked == true)
                strPainScore = "5";
            if (PainValue6.Checked == true)
                strPainScore = "6";
            if (PainValue7.Checked == true)
                strPainScore = "7";
            if (PainValue8.Checked == true)
                strPainScore = "8";
            if (PainValue9.Checked == true)
                strPainScore = "9";
            if (PainValue10.Checked == true)
                strPainScore = "10";
            objRec = new IP_RecoveryChart();
            objRec.BranchID = Convert.ToString(Session["Branch_ID"]);
            objRec.EMRID = Convert.ToString(Session["EMR_ID"]);
            objRec.PTID = Convert.ToString(Session["EMR_PT_ID"]);

            objRec.IRRM_RECEIVED_FROM = txtReceivedFrom.Text;
            objRec.IRRM_ACCOMPANIED_BY = txtAccompaniedBy.Text;
            objRec.IRRM_ARRIVAL_DATE = txtArrivalDate.Text + " " + drpArrivalHour.SelectedValue + ":" + drpArrivalMin.SelectedValue + ":00";

            objRec.IRRM_PAIN_SCORE = strPainScore;
            objRec.IRRM_MOTOR_BLOCK_INTENSITY = drpMotorBlockIntensity.SelectedValue;
            objRec.IRRM_SEDATION_AGITATION_SCORE = drpSedationAgitationScore.SelectedValue;


            objRec.IRRM_PATIENT_TEACHING = drpPatientTeaching.SelectedValue;
            objRec.IRRM_DISCHARGED_WITH = drpDischargedWith.SelectedValue;
            objRec.IRRM_DISCHARGED_TO = drpDischargedTo.SelectedValue;
            objRec.IRRM_COMMENT = txtPostOpInstComment.Text;

            objRec.IRRM_RR_NURSE = drpRRNurse.SelectedValue;
            objRec.IRRM_ANESTHESIOLOGIST = drpAnesthesiologist.SelectedValue;
            objRec.IRRM_WARD_NURSE = drpWardNurse.SelectedValue;
            objRec.IRRM_WARD_CALL_TIME = txtWardCallDate.Text + " " + drpWardCallHour.SelectedValue + ":" + drpWardCallMin.SelectedValue + ":00";
            objRec.IRRM_DISCHARGE_DATE = txtDischargeDate.Text + " " + drpDischargeHour.SelectedValue + ":" + drpDischargeMin.SelectedValue + ":00";


            objRec.UserID = Convert.ToString(Session["User_ID"]);
            objRec.RecoveryRoomMasterAdd();


        }

        #endregion

        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetServicessList(string prefixText)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string[] Data;


            ds = dbo.HaadServicessListGet("Pharmacy", prefixText, "", strSessionDeptId);
            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Data[i] = ds.Tables[0].Rows[i]["Code"].ToString() + "~" + ds.Tables[0].Rows[i]["Description"].ToString();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }

        #endregion

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_RECOV_CHART' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Visible = false;
                btnMonitorAdd.Visible = false;
                btnNurseNotesAdd.Visible = false;
                btnDischargeAdd.Visible = false;
                btnHMDischargeAdd.Visible = false;
                btnMedicationAdd.Visible = false;


                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSave.Visible = false;
                btnMonitorAdd.Visible = false;
                btnNurseNotesAdd.Visible = false;
                btnDischargeAdd.Visible = false;
                btnHMDischargeAdd.Visible = false;
                btnMedicationAdd.Visible = false;
                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                   // SetPermission();
                }


                try
                {
                    strSessionDeptId = Convert.ToString(Session["User_DeptID"]);

                    objCom = new CommonBAL();
                    string strDate = "", strTime = ""; ;
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("hh:mm:ss");


                    txtArrivalDate.Text = strDate;


                    txtMonitoringDate.Text = strDate;


                    txtNurseNotesDate.Text = strDate;



                    txtMedicationStarted.Text = strDate;

                    txtMedicationSFinished.Text = strDate;


                    txtMedicationSGivenTimeDate.Text = strDate;



                    txtArrivalDate.Text = strDate;


                    txtWardCallDate.Text = strDate;
                   

                    txtDischargeDate.Text = strDate;
                 

                    BindTime();


                    BindAnesthesiaTyp();
                    BindPTCondition();
                    BindVentilation();
                    BindVascularCases();
                    BindDrainage();
                    BindSpecialEvents();
                    BindDisIssueType();
                    BindHMDisIssueType();
                    BindDisAssTime();
                    BindHMDisAssTime();
                    BindRoute();
                    BindStaff();
                    BindNurse();

                    BindAnesthesiaDtl();
                    BindRecoveryMaster();
                    BindMonitoringGrid();
                    BindMedicationGrid();
                    BindRecoveryNurseNotesGrid();
                    BindDischargeRoomGrid();
                    BindDischargeHomeGrid();

                    ViewState["IRRM_MED_ID"] = "";
                    ViewState["IRRM_MON_ID"] = "";
                    ViewState["IRNN_NOTES_ID"] = "";
                    ViewState["IRRS_STATUS_ID"] = "";

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      RecoveryChart.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SaveAnesthesiaDtls();
                SaveRecoveryRoomMaster();

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      RecoveryChart.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void btnMonitorAdd_Click(object sender, EventArgs e)
        {
            try
            {
                objRec = new IP_RecoveryChart();
                objRec.BranchID = Convert.ToString(Session["Branch_ID"]);
                objRec.EMRID = Convert.ToString(Session["EMR_ID"]);
                objRec.PTID = Convert.ToString(Session["EMR_PT_ID"]);
                if (ViewState["IRRM_MON_ID"] == "")
                {
                    objRec.IRRM_MON_ID = "0";
                }
                else
                {
                    objRec.IRRM_MON_ID = Convert.ToString(ViewState["IRRM_MON_ID"]);
                }

                objRec.IRRM_TIME = txtMonitoringDate.Text + " " + drpMoniHour.SelectedValue + ":" + drpMoniMin.SelectedValue + ":00";
                objRec.IRRM_BP_SYSTOLIC = txtBpSystolic.Value;
                objRec.IRRM_BP_DIASTOLIC = txtBpDiastolic.Value;
                objRec.IRRM_PULSE = txtPulse.Value;
                objRec.IRRM_SPO2 = txtSp02.Value;
                objRec.IRRM_RESPIRATION = txtRespiration.Value;
                objRec.IRRM_TEMP = txtTemperatureF.Value;
                objRec.IRRM_PAIN_SCORE = txtPainScore.Value;
                objRec.IRRM_MOTOR_BLOCK = txtMotorBlock.Value;
                objRec.IRRM_COMMENT = txtMonitoringComment.Text;
                objRec.UserID = Convert.ToString(Session["User_ID"]);
                objRec.RecoveryRoomMonitoringAdd();

                MonitoringClear();
                BindMonitoringGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      RecoveryChart.btnMonitorAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void Monitoring_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvMonitoringSelectIndex"] = gvScanCard.RowIndex;
                gvMonitoring.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblMonID = (Label)gvScanCard.Cells[0].FindControl("lblMonID");

                ViewState["IRRM_MON_ID"] = lblMonID.Text;
                BindMonitoring();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      RecoveryChart.Monitoring_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void btnMedicationAdd_Click(object sender, EventArgs e)
        {
            try
            {
                objRec = new IP_RecoveryChart();
                objRec.BranchID = Convert.ToString(Session["Branch_ID"]);
                objRec.EMRID = Convert.ToString(Session["EMR_ID"]);
                objRec.PTID = Convert.ToString(Session["EMR_PT_ID"]);
                if (Convert.ToString(ViewState["IRRM_MED_ID"]) == "" || Convert.ToString(ViewState["IRRM_MED_ID"]) == null)
                {
                    objRec.IRRM_MED_ID = "0";
                }
                else
                {
                    objRec.IRRM_MED_ID = Convert.ToString(ViewState["IRRM_MED_ID"]);
                }


                objRec.IRRM_SOLUTION = txtSolution.Text;
                objRec.IRRM_VOLUME = txtVolume.Text;
                objRec.IRRM_STARTED = txtMedicationStarted.Text + " " + drpMedStHour.SelectedValue + ":" + drpMedStMin.SelectedValue + ":00";
                objRec.IRRM_FINISHED = txtMedicationSFinished.Text + " " + drpMedEndHour.SelectedValue + ":" + drpMedEndMin.SelectedValue + ":00";
                objRec.IRRM_DRUG_CODE = txtServCode.Text;
                objRec.IRRM_DRUG_NAME = txtServName.Text;
                objRec.IRRM_DOSE = txtDossage1.Text;
                objRec.IRRM_ROUTE = drpRoute.SelectedValue;
                objRec.IRRM_GIVEN_BY = drpGivenBy.SelectedValue;
                objRec.IRRM_TIME_GIVEN = txtMedicationSGivenTimeDate.Text + " " + drpMedGivenHour.SelectedValue + ":" + drpMedGivenMin.SelectedValue + ":00";

                objRec.UserID = Convert.ToString(Session["User_ID"]);
                objRec.RecoveryRoomMedicationAdd();

                MedicationClear();
                BindMedicationGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      RecoveryChart.btnMedicationAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void Medication_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvMedicationSelectIndex"] = gvScanCard.RowIndex;
                gvMedication.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblMedID = (Label)gvScanCard.Cells[0].FindControl("lblMedID");

                ViewState["IRRM_MED_ID"] = lblMedID.Text;
                BindMedication();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "     RecoveryChart.Medication_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void btnNurseNotesAdd_Click(object sender, EventArgs e)
        {
            try
            {
                objRec = new IP_RecoveryChart();
                objRec.BranchID = Convert.ToString(Session["Branch_ID"]);
                objRec.EMRID = Convert.ToString(Session["EMR_ID"]);
                objRec.PTID = Convert.ToString(Session["EMR_PT_ID"]);
                if (Convert.ToString(ViewState["IRNN_NOTES_ID"]) == "" || Convert.ToString(ViewState["IRNN_NOTES_ID"]) == null)
                {
                    objRec.IRNN_NOTES_ID = "0";
                }
                else
                {
                    objRec.IRNN_NOTES_ID = Convert.ToString(ViewState["IRNN_NOTES_ID"]);
                }


                objRec.IRNN_TIME = txtNurseNotesDate.Text + " " + drpNurNoteHour.SelectedValue + ":" + drpNurNoteMin.SelectedValue + ":00";

                objRec.IRNN_REMARKS = txtNurseNotesRemarks.Text;


                objRec.UserID = Convert.ToString(Session["User_ID"]);
                objRec.RecoveryNurseNotesAdd();

                NurseNotesClear();
                BindRecoveryNurseNotesGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      RecoveryChart.btnNurseNotesAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void NurseNotes_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvNurseNotesSelectIndex"] = gvScanCard.RowIndex;
                gvNurseNotes.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblNurseID = (Label)gvScanCard.Cells[0].FindControl("lblNurseID");

                ViewState["IRNN_NOTES_ID"] = lblNurseID.Text;
                BindRecoveryNurseNotes();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "     RecoveryChart.NurseNotes_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void drpDisIssueType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDisIssueSubType();
        }

        protected void drpHmDisIssueType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindHMDisIssueSubType();
        }


        protected void btnDischargeAdd_Click(object sender, EventArgs e)
        {
            try
            {

                Int32 ScoreValue;

                GetScoreValue("DISCHARGE_ROOM_ISSUE_TYPE", drpDisIssueSubType.SelectedValue, out  ScoreValue);


                objRec = new IP_RecoveryChart();
                objRec.BranchID = Convert.ToString(Session["Branch_ID"]);
                objRec.EMRID = Convert.ToString(Session["EMR_ID"]);
                objRec.PTID = Convert.ToString(Session["EMR_PT_ID"]);
                if (Convert.ToString(ViewState["IRRS_STATUS_ID"]) == "" || Convert.ToString(ViewState["IRRS_STATUS_ID"]) == null)
                {
                    objRec.IRRS_STATUS_ID = "0";
                }
                else
                {
                    objRec.IRRS_STATUS_ID = Convert.ToString(ViewState["IRRS_STATUS_ID"]);
                }


                objRec.IRRS_ISSUE_TYPE = drpDisIssueType.SelectedValue;

                objRec.IRRS_ISSUE_SUB_TYPE = drpDisIssueSubType.SelectedValue;
                objRec.IRRS_ISSUE_TIMING = drpDisAssTime.SelectedValue;
                objRec.IRRS_SCORE = Convert.ToString(ScoreValue);
                objRec.IRRS_DISCHARGE_TYPE = "DISCHARGE_ROOM";

                objRec.RecoveryRoomPTStatusAdd();

                DischargeRoomClear();
                BindDischargeRoomGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      RecoveryChart.btnNurseNotesAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnDischargeClear_Click(object sender, EventArgs e)
        {
            DischargeRoomClear();
        }

        protected void DischargeRoom_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvDischargeRoomSelectIndex"] = gvScanCard.RowIndex;
                gvDischargeRoom.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblStatusID = (Label)gvScanCard.Cells[0].FindControl("lblStatusID");

                ViewState["IRRS_STATUS_ID"] = lblStatusID.Text;
                BindDischargeRoom();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "     RecoveryChart.Medication_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }



        protected void btnHMDischargeAdd_Click(object sender, EventArgs e)
        {
            try
            {

                Int32 ScoreValue;

                GetScoreValue("DISCHARGE_HOME_ISSUE_TYPE", drpHmDisIssueSubType.SelectedValue, out  ScoreValue);


                objRec = new IP_RecoveryChart();
                objRec.BranchID = Convert.ToString(Session["Branch_ID"]);
                objRec.EMRID = Convert.ToString(Session["EMR_ID"]);
                objRec.PTID = Convert.ToString(Session["EMR_PT_ID"]);
                if (Convert.ToString(ViewState["IRRS_STATUS_ID_HM"]) == "" || Convert.ToString(ViewState["IRRS_STATUS_ID_HM"]) == null)
                {
                    objRec.IRRS_STATUS_ID = "0";
                }
                else
                {
                    objRec.IRRS_STATUS_ID = Convert.ToString(ViewState["IRRS_STATUS_ID_HM"]);
                }


                objRec.IRRS_ISSUE_TYPE = drpHmDisIssueType.SelectedValue;

                objRec.IRRS_ISSUE_SUB_TYPE = drpHmDisIssueSubType.SelectedValue;
                objRec.IRRS_ISSUE_TIMING = drpHmDisAssTime.SelectedValue;
                objRec.IRRS_SCORE = Convert.ToString(ScoreValue);
                objRec.IRRS_DISCHARGE_TYPE = "DISCHARGE_HOME";

                objRec.RecoveryRoomPTStatusAdd();

                DischargeHomeClear();
                BindDischargeHomeGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      RecoveryChart.btnNurseNotesAdd_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnHMDischargeClear_Click(object sender, EventArgs e)
        {
            DischargeHomeClear();
        }

        protected void DischargeHome_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvDischargeHomeSelectIndex"] = gvScanCard.RowIndex;
                gvDischargeHome.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblHmStatusID = (Label)gvScanCard.Cells[0].FindControl("lblHmStatusID");

                ViewState["IRRS_STATUS_ID_HM"] = lblHmStatusID.Text;
                BindDischargeHome();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "     RecoveryChart.DischargeHome_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        #endregion

    }
}