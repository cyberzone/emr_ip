﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="SiteVerification.aspx.cs" Inherits="EMR_IP.Patient.SiteVerification" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }


    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />

    <h3 class="box-title">Site Verification/marking and Time-out Documentation</h3>

    <br />
    <br />
    <br />
    <br />
    <br />
    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully 
                
        </div>
    </div>

    <div style="float: right;">

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Saved Record" OnClick="btnSave_Click" />


            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

     <asp:tabcontainer id="TabContainer1" runat="server" activetabindex="0" cssclass="ajax__tab_yuitabview-theme" width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Site Verification Dtls1" Width="100%">
            <contenttemplate>
      <table width="100%" style="padding-left: 10px;">
          <tr>
             <td class="lblCaption1" colspan="2" style="font-weight: bold;">Part A: Patient Identified by: (√ check all the apply), Minimum two-to be completed by the unit nurse. Check minimum two
            </td>
          </tr>
          <tr>
              <td valign="top">
                 <table width="100%" style="padding-left: 10px;">
      
        <tr>
            <td>
                <asp:UpdatePanel runat="server" ID="updatePanel30">
                    <ContentTemplate>
                        <input type="checkbox" id="chkNameVerByPT" runat="server" value="True" /><label class="lblCaption1" for="chkVerByPT">Name, verbalized by patient/compared with chart</label> 
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel runat="server" ID="updatePanel2">
                    <ContentTemplate>
                        <input type="checkbox" id="chkBirthDateVerByPT" runat="server" value="True" /><label class="lblCaption1" for="chkBirthDateVerByPT">Birth date, verbalized by patient/compared with chart</label> 
                    </ContentTemplate>
                </asp:UpdatePanel>
                 </td>
       </tr>
        <tr>
            <td>
                <asp:UpdatePanel runat="server" ID="updatePanel3">
                    <ContentTemplate>
                        <input type="checkbox" id="chkIDBand" runat="server" value="True" /><label class="lblCaption1" for="chkIDBand">Identification band, compared with  chart</label
                    </ContentTemplate>
                </asp:UpdatePanel>
                 </td>
        </tr>
        </table>

              </td>
              <td valign="top">
                 <table width="100%" style="padding-left: 10px;">
        <tr>
            <td>
                <asp:UpdatePanel runat="server" ID="updatePanel4">
                    <ContentTemplate>
                        <input type="checkbox" id="chkFamilyMember" runat="server" value="Family member" /><label class="lblCaption1" for="chkIDBand">Family member</label>

                        <input type="text" id="txtFamilyMember" runat="server" class="label" style="width: 200px;" /><br />
                    </ContentTemplate>
                </asp:UpdatePanel>
                 </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel runat="server" ID="updatePanel5">
                    <ContentTemplate>
                        <input type="checkbox" id="chkPatientUnit" runat="server" value="Patient unit" /><label class="lblCaption1" for="chkIDBand">Patient unit </label>
                        &nbsp;&nbsp;&nbsp;&nbsp;
               <input type="text" id="txtPatientUnit" runat="server" class="label" style="width: 200px;" /><br />
                    </ContentTemplate>
                </asp:UpdatePanel>
                </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel runat="server" ID="updatePanel6">
                    <ContentTemplate>
                        <input type="checkbox" id="chkVerOther" runat="server" value="Other" /><label class="lblCaption1" for="chkVerOther">Other </label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <input type="text" id="txtVerOther" runat="server" class="label" style="width: 200px;" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
       
       

    </table>
              </td>
           </tr>

  <tr>
            <td class="lblCaption1" colspan="2" style="font-weight: bold;">Part B: Procedure (include Site/Side verification per patient): Do not use abbreviations-to be completed by doctor:
            </td>

        </tr>
     <tr>
            <td colspan="2">
                <asp:UpdatePanel runat="server" ID="updatePanel7">
                    <ContentTemplate>
                        <asp:TextBox ID="txtProcedure" runat="server" CssClass="label" TextMode="MultiLine" Width="100%" Height="100px" Style="resize: none;"></asp:TextBox>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
</table>

    <table width="100%">
        <tr>
            <td class="lblCaption1" style="font-weight: bold; width: 50%; vertical-align: top;">Part C:    To be completed by surgeon/assistant/radiologist in the unit
                Marking of operative site (√ check all that apply

            </td>
            <td class="lblCaption1" style="font-weight: bold; width: 50%; vertical-align: top;">Part D:    To be completed by OR reception area
                Verification of Site/Side:
            </td>

        </tr>
        <tr>
            <td style="vertical-align: top;">
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel8">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkOperSiteNA" runat="server" value="NA because:-" /><label class="lblCaption1" for="chkVerByPT">NA because</label>
                                        <asp:DropDownList ID="drpOperSiteNARea" runat="server" CssClass="label">
                                            <asp:ListItem Text="--- Select ---" Value=""></asp:ListItem>
                                            <asp:ListItem Text="Single Organ case" Value="Single Organ case"></asp:ListItem>
                                            <asp:ListItem Text="Premature infant" Value="Premature infant"></asp:ListItem>
                                            <asp:ListItem Text="Insertion site is not determined" Value="Insertion site is not determined"></asp:ListItem>
                                            <asp:ListItem Text="teeth" Value="teeth"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel9">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkMarkPTRefuse" runat="server" value="Patients refuse because" /><label class="lblCaption1" for="chkPTRefuse">Patients refuse because</label>
                                        <input type="text" id="txtPTRefuse" runat="server" class="label" style="width: 200px;" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel10">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkMarkOther" runat="server" value="Other" /><label class="lblCaption1" for="chkMarkOther">Other</label>
                                        <input type="text" id="txtMarkOther" runat="server" class="label" style="width: 200px;" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel11">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkMarkPTFamily" runat="server" value="Patient/family participated in marking" /><label class="lblCaption1" for="chkMarkPTFamily">Patient/family participated in marking</label>
                                        <input type="text" id="txtMarkPTFamily" runat="server" class="label" style="width: 200px;" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel12">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkLocationMarking" runat="server" value="Location of marking" /><label class="lblCaption1" for="chkLocationMarking">Location of marking</label>
                                        <input type="text" id="txtchkLocationMarking" runat="server" class="label" style="width: 200px;" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel13">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkSiteMarkedBy" runat="server" value="Site marked by" /><label class="lblCaption1" for="chkSiteMarkedBy">Site marked by</label>
                                       
                                        <asp:DropDownList ID="drpSiteMarkedBy" runat="server" CssClass="label" Width="155px" BorderColor="#cccccc"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel14">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkConfirmedBy" runat="server" value="Site marked by" /><label class="lblCaption1" for="chkConfirmedBy">Confirmed by</label>
                                        <asp:DropDownList ID="drpConfirmedBy" runat="server" CssClass="label" Width="155px" BorderColor="#cccccc"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">
                                    <asp:UpdatePanel ID="updatePanel16" runat="server">
                                        <ContentTemplate>
Date & Time
                                            <asp:TextBox ID="txtMarkDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                                Enabled="True" TargetControlID="txtMarkDate" Format="dd/MM/yyyy">
                                            </asp:CalendarExtender>
                                            <asp:DropDownList ID="drpMarkHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                            <asp:DropDownList ID="drpMarkMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                        </ContentTemplate>
                                    </asp:UpdatePanel>

                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td style="vertical-align: top;">
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td class="lblCaption1"  >
                                
                                  <asp:UpdatePanel runat="server" ID="updatePanel15">
                                    <ContentTemplate>One:
                                        <input type="checkbox" id="chkLeft" runat="server" value="Left" /><label class="lblCaption1" for="chkLeft">Left</label>
                                         <input type="checkbox" id="chkCenter" runat="server" value="Center" /><label class="lblCaption1" for="chkCenter">Center</label>
                                         <input type="checkbox" id="chkRight" runat="server" value="Right" /><label class="lblCaption1" for="chkRightf">Right</label>
                                         <input type="checkbox" id="chkUnknown" runat="server" value="Unknown" /><label class="lblCaption1" for="chkUnknown">Unknown</label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" >
                                (√check all that apply)
                            </td>
                        </tr>
                          <tr>
                            <td class="lblCaption1" >
                          
                                     <asp:UpdatePanel runat="server" ID="updatePanel17">
                                    <ContentTemplate>
                                             Patience came from
                                 <input type="text" id="txtPTCameFrom" runat="server" class="label" style="width: 200px;" />    unit
                                           </ContentTemplate>
                                </asp:UpdatePanel>
                               
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel18">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkConsWithConsent" runat="server" value="Consistent with consent" /><label class="lblCaption1" for="chkConsWithConsent">Consistent with consent</label> 
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel19">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkConsWithHistPhy" runat="server" value="Consistent with History & Physical" /><label class="lblCaption1" for="chkConsWithHistPhy">Consistent with History & Physical</label> 
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                 </td>
                       </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel20">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkConsWithXrayTestRes" runat="server" value="Consistent with X-rays, test results, etc." /><label class="lblCaption1" for="chkConsWithXrayTestRes">Consistent with X-rays, test results, etc.</label> 
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel21">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkConsWithOperSiteMark" runat="server" value="Consistent with operative Site Marking" /><label class="lblCaption1" for="chkConsWithOperSiteMark">Consistent with operative Site Marking</label> 
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                 </td>
                       </tr>
                          <tr>
                            <td class="lblCaption1" >
                          
                                     <asp:UpdatePanel runat="server" ID="updatePanel22">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkDiscrepancyIdent" runat="server" value="Discrepancy Identified" /><label class="lblCaption1" for="chkDiscrepancyIdent">Discrepancy Identified</label> 
                                         Explain discrepancy 
                                 <input type="text" id="txtDiscrepancyIdent" runat="server" class="label" style="width: 200px;" />     
                                           </ContentTemplate>
                                </asp:UpdatePanel>
                               
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" >
                          
                                     <asp:UpdatePanel runat="server" ID="updatePanel23">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkResolution" runat="server" value="Resolution" /><label class="lblCaption1" for="chkResolution">Resolution</label> 
                                        <input type="text" id="txtResolution" runat="server" class="label" style="width: 200px;" />     
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                               
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>

                </contenttemplate>
            </asp:TabPanel> 

          <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Site Verification Dtls2" Width="100%">
            <contenttemplate>
                <table width="100%" style="padding-left: 10px;">
                  <tr>
                     <td class="lblCaption1" style="font-weight: bold;width:33%;text-align:left">  BEFORE INDUCTION OF ANESTHESIA <br /> (with at least RN and anesthesiologist)<br /> SIGN IN
                    </td>
                      <td class="lblCaption1" style="font-weight: bold;width:33%;text-align:left">BEFORE SKIN INCISION <br />(Time out with RN, anesthesiologist, surgeon) <br />TIME OUT

                    </td>
                     
                  </tr>
                  <tr>
                      <td valign="top" >
                           <asp:PlaceHolder ID="plhoBeforeInductionAnesthesia" runat="server"></asp:PlaceHolder>

                      </td>
                        <td valign="top" >
                           <asp:PlaceHolder ID="plhoBeforeSkinIncision" runat="server"></asp:PlaceHolder>

                      </td>
                       

                  </tr>
                </table>
                   <table width="100%" style="padding-left: 10px;">
                          <tr>
                               <td class="lblCaption1" style="font-weight: bold;width:33%;text-align:left">
                                   BEFORE PATIENT LEAVES OPERATION ROOM <br /> (with RN, anesthesiologist, surgeon) <br />SIGN OUT
                              </td>
                          </tr>
                          <tr>
                               <td valign="top" >
                                        <asp:PlaceHolder ID="plhoBeforePTLeavesOperRoom" runat="server"></asp:PlaceHolder>
                               </td>
                           </tr>
                    </table>
             </contenttemplate>
            </asp:TabPanel> 

         </asp:tabcontainer> 
    <br />
    <br />
</asp:Content>
