﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;


namespace EMR_IP.Patient
{
    public partial class SiteVerification : System.Web.UI.Page
    {
        IP_SiteVerificationMarking objSiteVer = new IP_SiteVerificationMarking();
        CommonBAL objCom = new CommonBAL();
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("CongenitaHeartDiseaseScreening." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindStaff()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 "; // AND HSFM_CATEGORY='Doctors'";
            Criteria += " AND HSFM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.GetStaffMaster(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpConfirmedBy.DataSource = DS;
                drpConfirmedBy.DataTextField = "FullName";
                drpConfirmedBy.DataValueField = "HSFM_STAFF_ID";
                drpConfirmedBy.DataBind();

                drpSiteMarkedBy.DataSource = DS;
                drpSiteMarkedBy.DataTextField = "FullName";
                drpSiteMarkedBy.DataValueField = "HSFM_STAFF_ID";
                drpSiteMarkedBy.DataBind();


            }
            drpConfirmedBy.Items.Insert(0, "--- Select ---");
            drpConfirmedBy.Items[0].Value = "";


            drpSiteMarkedBy.Items.Insert(0, "--- Select ---");
            drpSiteMarkedBy.Items[0].Value = "";
            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindTime()
        {
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpMarkHour.DataSource = DS;
                drpMarkHour.DataTextField = "Name";
                drpMarkHour.DataValueField = "Code";
                drpMarkHour.DataBind();
            }

            drpMarkHour.Items.Insert(0, "00");
            drpMarkHour.Items[0].Value = "00";


            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpMarkMin.DataSource = DS;
                drpMarkMin.DataTextField = "Name";
                drpMarkMin.DataValueField = "Code";
                drpMarkMin.DataBind();
            }

            drpMarkMin.Items.Insert(0, "00");
            drpMarkMin.Items[0].Value = "00";

             
        }

        void BindData()
        {
            DataSet DS = new DataSet();
            objSiteVer = new IP_SiteVerificationMarking();
            string Criteria = " 1=1 and ISVM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND ISVM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objSiteVer.SiteVerificationMarkingGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    chkNameVerByPT.Checked = Convert.ToBoolean(DR["ISVM_NAME_VERBALIZED_PT_CHART"]);
                    chkBirthDateVerByPT.Checked = Convert.ToBoolean(DR["ISVM_BDATE_VERBALIZED_PT_CHART"]);
                    chkIDBand.Checked = Convert.ToBoolean(DR["ISVM_IDBAND_CHART"]);

                    string strFamilyMember = Convert.ToString(DR["ISVM_PT_IDEN_FAMILY_MEMBER"]);
                    string[] arrFamilyMember = strFamilyMember.Split('|');
                    if (arrFamilyMember.Length > 1)
                    {
                        txtFamilyMember.Value = arrFamilyMember[0];
                        chkFamilyMember.Checked = Convert.ToBoolean(arrFamilyMember[1]);

                    }

                    string strPTUnit = Convert.ToString(DR["ISVM_PT_IDEN_UNIT"]);
                    string[] arrPTUnit = strPTUnit.Split('|');
                    if (arrPTUnit.Length > 1)
                    {
                        txtPatientUnit.Value = arrPTUnit[0];
                        chkPatientUnit.Checked = Convert.ToBoolean(arrPTUnit[1]);

                    }

                    string strIdenOther = Convert.ToString(DR["ISVM_PT_IDEN_OTHER"]);
                    string[] arrIdenOther = strIdenOther.Split('|');
                    if (arrIdenOther.Length > 1)
                    {
                        txtVerOther.Value = arrIdenOther[0];
                        chkVerOther.Checked = Convert.ToBoolean(arrIdenOther[1]);

                    }


                    txtProcedure.Text = Convert.ToString(DR["ISVM_PROCEDURE"]);

                    string strOperSiteNA = Convert.ToString(DR["ISVM_MARKING"]);
                    string[] arrOperSiteNA = strOperSiteNA.Split('|');
                    if (arrOperSiteNA.Length > 1)
                    {
                        drpOperSiteNARea.SelectedValue = arrOperSiteNA[0];
                        chkOperSiteNA.Checked = Convert.ToBoolean(arrOperSiteNA[1]);

                    }



                    string strMarkPTRefuse = Convert.ToString(DR["ISVM_MARK_REFUSE"]);
                    string[] arrMarkPTRefuse = strMarkPTRefuse.Split('|');
                    if (arrMarkPTRefuse.Length > 1)
                    {
                        txtPTRefuse.Value = arrMarkPTRefuse[0];
                        chkMarkPTRefuse.Checked = Convert.ToBoolean(arrMarkPTRefuse[1]);

                    }


                    string strchkMarkOther = Convert.ToString(DR["ISVM_MARK_OTHER"]);
                    string[] arrchkMarkOther = strchkMarkOther.Split('|');
                    if (arrchkMarkOther.Length > 1)
                    {
                        txtMarkOther.Value = arrchkMarkOther[0];
                        chkMarkOther.Checked = Convert.ToBoolean(arrchkMarkOther[1]);

                    }





                    chkMarkPTFamily.Checked = Convert.ToBoolean(DR["ISVM_MARK_FAMILY_PARTICI"]);
                    txtMarkPTFamily.Value = Convert.ToString(DR["ISVM_MARK_FAMILY_PARTICI_DESC"]);

                    chkLocationMarking.Checked = Convert.ToBoolean(DR["ISVM_MARK_LOCATION"]);
                    txtchkLocationMarking.Value = Convert.ToString(DR["ISVM_MARK_LOCATION_DESC"]);

                    drpSiteMarkedBy.SelectedValue = Convert.ToString(DR["ISVM_SITE_MARKED_BY"]);
                    drpConfirmedBy.SelectedValue = Convert.ToString(DR["ISVM_SITE_CONFIRM_BY"]);


                    txtMarkDate.Text = Convert.ToString(DR["ISVM_SITE_MARK_DATEDesc"]);

                    string strSecHour = Convert.ToString(DR["ISVM_SITE_MARK_DATE_TIMEDesc"]);
                    string[] arrSecHour = strSecHour.Split(':');
                    if (arrSecHour.Length > 1)
                    {
                        drpMarkHour.SelectedValue = arrSecHour[0];
                        drpMarkMin.SelectedValue = arrSecHour[1];

                    }



                    string strVerfiSide = Convert.ToString(DR["ISVM_VERIFI_SIDE"]);
                    string[] artVerfiSide = strVerfiSide.Split('|');
                    if (arrMarkPTRefuse.Length > 1)
                    {
                        for (int i = 0; i <= artVerfiSide.Length - 1; i++)
                        {

                            if (chkLeft.Value == artVerfiSide[i])
                            {
                                chkLeft.Checked = true;
                            }

                            if (chkCenter.Value == artVerfiSide[i])
                            {
                                chkCenter.Checked = true;
                            }
                            if (chkRight.Value == artVerfiSide[i])
                            {
                                chkRight.Checked = true;
                            }

                            if (chkUnknown.Value == artVerfiSide[i])
                            {
                                chkUnknown.Checked = true;
                            }

                        }

                    }




                    txtPTCameFrom.Value = Convert.ToString(DR["ISVM_PT_CAME_FROM"]);

                    chkConsWithConsent.Checked = Convert.ToBoolean(DR["ISVM_CONSISTENT_CONSENT"]);
                    chkConsWithHistPhy.Checked = Convert.ToBoolean(DR["ISVM_CONSISTENT_HIS_PHY"]);
                    chkConsWithXrayTestRes.Checked = Convert.ToBoolean(DR["ISVM_CONSISTENT_XRAY_RESULT"]);

                    chkConsWithOperSiteMark.Checked = Convert.ToBoolean(DR["ISVM_CONSISTENT_SITE_MARK"]);

                    chkDiscrepancyIdent.Checked = Convert.ToBoolean(DR["ISVM_DISCRIDENTIFIED"]);
                    txtDiscrepancyIdent.Value = Convert.ToString(DR["ISVM_DISCRIDENTIFIED_EXP"]);
                    chkConsWithOperSiteMark.Checked = Convert.ToBoolean(DR["ISVM_RESOLUTION"]);

                    chkResolution.Checked = Convert.ToBoolean(DR["ISVM_RESOLUTION"]);


                    txtResolution.Value = Convert.ToString(DR["ISVM_RESOLUTION_DESC"]);

                }
            }
        }


        DataSet GetSegmentData(string strType, string Position)
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 AND ISVS_ACTIVE=1 AND ISVS_BRANCH='" + Convert.ToString(Session["Branch_ID"]) + "' AND ISVS_TYPE='" + strType + "'";
            Criteria += " AND  ISVS_POSITION='" + Position + "'";

            DataSet DS = new DataSet();

            DS = objCom.fnGetFieldValue(" * ", "IP_SITE_VERIFICATION_SEGMENT", Criteria, "ISVS_ORDER");



            return DS;

        }

        DataSet GetSegmentMaster(string strType)
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 AND ISVSM_STATUS=1 AND ISVSM_TYPE='" + strType + "'";


            DataSet DS = new DataSet();
            DS = objCom.fnGetFieldValue(" * ", "IP_SITE_VERIFICATION_SEGMENT_MASTER", Criteria, "");


            return DS;
        }

        public void SaveSegmentDtls(string strType, string FieldID, string FieName, string Selected, string Comment)
        {
            objSiteVer = new IP_SiteVerificationMarking();

            objSiteVer.BranchID = Convert.ToString(Session["Branch_ID"]);
            objSiteVer.EMRID = Convert.ToString(Session["EMR_ID"]);
            objSiteVer.PTID = Convert.ToString(Session["EMR_PT_ID"]);
            objSiteVer.ISVSD_TYPE = strType;
            objSiteVer.ISVSD_FIELD_ID = FieldID;
            objSiteVer.ISVSD_FIELD_NAME = FieName;
            objSiteVer.ISVSD_VALUE = Selected;
            objSiteVer.ISVSD_COMMENT = Comment;
            objSiteVer.SiteVerificationSegmentDtlsAdd();
        }


        public void SaveSegment(string strType, string Position)
        {
            DataSet DS = new DataSet();

            DS = GetSegmentData(strType, Position);

            foreach (DataRow DR in DS.Tables[0].Rows)
            {

                objCom = new CommonBAL();

                string CriteriaDel = " 1=1 AND ISVSD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND ISVSD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                CriteriaDel += " AND ISVSD_TYPE='" + Convert.ToString(DR["ISVS_CODE"]).Trim() + "'";
                objCom.fnDeleteTableData("IP_SITE_VERIFICATION_SEGMENT_DTLS", CriteriaDel);


                Int32 i = 1;
                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["ISVS_CODE"]).Trim());



                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {
                    string strValueYes = "", _Comment = "";
                    if (Convert.ToString(DR1["ISVSM_CONTROL_TYPE"]).ToUpper() == "CheckBox".ToUpper())
                    {
                        CheckBox chk1 = new CheckBox();



                        if (strType == "IP_BEFORE_INDUCTION_ANES_SIGNIN")
                        {
                            if (Position == "LEFT")
                            {
                                chk1 = (CheckBox)plhoBeforeInductionAnesthesia.FindControl("chk" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim());

                            }
                            else
                            {

                            }
                        }
                        if (strType == "IP_BEFORE_SKIN_INCISION_TIMEOUT")
                        {
                            if (Position == "RIGHT")
                            {
                                chk1 = (CheckBox)plhoBeforeSkinIncision.FindControl("chk" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim());

                            }
                            else
                            {

                            }
                        }
                        if (strType == "IP_BEFORE_PT_LEAVES_OPER_ROOM")
                        {
                            if (Position == "LEFT")
                            {
                                chk1 = (CheckBox)plhoBeforePTLeavesOperRoom.FindControl("chk" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim());

                            }
                            else
                            {

                            }
                        }




                        if (chk1 != null)
                        {
                            if (chk1.Checked == true)
                            {
                                strValueYes = "Y";
                            }
                        }

                    }
                    if (Convert.ToString(DR1["ISVSM_CONTROL_TYPE"]).ToUpper() == "TextBox".ToUpper() || Convert.ToString(DR1["ISVSM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                    {
                        TextBox txt = new TextBox();
                        if (strType == "IP_BEFORE_INDUCTION_ANES_SIGNIN")
                        {
                            if (Position == "LEFT")
                            {
                                txt = (TextBox)plhoBeforeInductionAnesthesia.FindControl("txt" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim());
                            }
                            else
                            {

                            }
                        }

                        if (strType == "IP_BEFORE_SKIN_INCISION_TIMEOUT")
                        {
                            if (Position == "RIGHT")
                            {
                                txt = (TextBox)plhoBeforeSkinIncision.FindControl("txt" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim());
                            }
                            else
                            {


                            }
                        }

                        if (strType == "IP_BEFORE_PT_LEAVES_OPER_ROOM")
                        {
                            if (Position == "LEFT")
                            {
                                txt = (TextBox)plhoBeforePTLeavesOperRoom.FindControl("txt" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim());
                            }
                            else
                            {


                            }
                        }





                        if (txt.Text != "")
                        {
                            strValueYes = "Y";
                            _Comment = txt.Text;
                        }
                    }



                    if (Convert.ToString(DR1["ISVSM_CONTROL_TYPE"]).ToUpper() == "RadioButtonList".ToUpper())
                    {

                        RadioButtonList RadList = new RadioButtonList();


                        if (strType == "IP_BEFORE_INDUCTION_ANES_SIGNIN")
                        {
                            if (Position == "LEFT")
                            {

                                RadList = (RadioButtonList)plhoBeforeInductionAnesthesia.FindControl("radl" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim());

                            }
                            else
                            {

                            }
                        }


                        if (strType == "IP_BEFORE_SKIN_INCISION_TIMEOUT")
                        {
                            if (Position == "RIGHT")
                            {

                                RadList = (RadioButtonList)plhoBeforeSkinIncision.FindControl("radl" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim());

                            }
                            else
                            {

                            }
                        }

                        if (strType == "IP_BEFORE_PT_LEAVES_OPER_ROOM")
                        {
                            if (Position == "LEFT")
                            {

                                RadList = (RadioButtonList)plhoBeforePTLeavesOperRoom.FindControl("radl" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim());

                            }
                            else
                            {

                            }
                        }


                        if (RadList != null)
                        {
                            if (RadList.SelectedValue != "")
                            {
                                strValueYes = "Y";
                                _Comment = RadList.SelectedValue;
                            }
                        }



                    }



                    if (strValueYes != "")
                    {
                        SaveSegmentDtls(Convert.ToString(DR["ISVS_CODE"]).Trim(), Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim(), Convert.ToString(DR1["ISVSM_FIELD_NAME"]).Trim(), strValueYes, _Comment);

                    }

                }
                i = i + 1;
            }
        }

        public void CreateSegCtrls(string strType, string Position)
        {
            DataSet DS = new DataSet();

            DS = GetSegmentData(strType, Position);

            foreach (DataRow DR in DS.Tables[0].Rows)
            {


                Panel myFieldSet = new Panel();

                myFieldSet.GroupingText = Convert.ToString(DR["ISVS_NAME"]);
                myFieldSet.Style.Add("text-align", "justify");
                myFieldSet.CssClass = "lblCaption1";
                myFieldSet.Width = 500;
                Int32 i = 1;

                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["ISVS_CODE"]));

                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {



                    Boolean _checked = false;
                    string strComment = "";
                    objCom = new CommonBAL();
                    string Criteria2 = "1=1 ";
                    Criteria2 += " AND ISVSD_TYPE='" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "' AND ISVSD_FIELD_ID='" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim() + "' ";
                    Criteria2 += " AND ISVSD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND ISVSD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

                    DataSet DS2 = new DataSet();
                    objSiteVer = new IP_SiteVerificationMarking();

                    DS2 = objSiteVer.SiteVerificationDtlsGet(Criteria2);
                    if (DS2.Tables[0].Rows.Count > 0)
                    {
                        _checked = true;

                        strComment = Convert.ToString(DS2.Tables[0].Rows[0]["ISVSD_COMMENT"]).Trim();
                    }


                    if (Convert.ToString(DR1["ISVSM_CONTROL_TYPE"]).ToUpper() == "CheckBox".ToUpper())
                    {
                        CheckBox chk = new CheckBox();
                        chk.ID = "chk" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim();
                        chk.Text = Convert.ToString(DR1["ISVSM_FIELD_NAME"]).Trim();
                        chk.CssClass = "lblCaption1";

                        chk.Checked = _checked;

                        //tdHe1.Controls.Add(chk);
                        //trHe1.Controls.Add(tdHe1);
                        //myFieldSet.Controls.Add(trHe1);

                        myFieldSet.Controls.Add(chk);
                    }


                    if (Convert.ToString(DR1["ISVSM_CONTROL_TYPE"]).ToUpper() == "TextBox".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["ISVSM_FIELD_NAME"]).Trim() + ":";
                        myFieldSet.Controls.Add(lbl);

                        Literal lit1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                        myFieldSet.Controls.Add(lit1);

                        TextBox txt = new TextBox();
                        txt.ID = "txt" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["ISVSM_FIELD_NAME"]);
                        txt.CssClass = "Label";
                        txt.Style.Add("width", "99%");

                        txt.Text = strComment;

                        myFieldSet.Controls.Add(txt);


                    }

                    if (Convert.ToString(DR1["ISVSM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["ISVSM_FIELD_NAME"]).Trim() + ":";
                        myFieldSet.Controls.Add(lbl);

                        TextBox txt = new TextBox();
                        txt.ID = "txt" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["ISVSM_FIELD_NAME"]);
                        txt.CssClass = "Label";
                        txt.Style.Add("width", "75px");

                        AjaxControlToolkit.CalendarExtender CalendarExtender = new AjaxControlToolkit.CalendarExtender();
                        CalendarExtender.TargetControlID = txt.ID;
                        CalendarExtender.Format = "dd/MM/yyyy";


                        txt.Text = strComment;

                        myFieldSet.Controls.Add(txt);
                        myFieldSet.Controls.Add(CalendarExtender);
                        //tdHe1.Controls.Add(txt);
                        //trHe1.Controls.Add(tdHe1);
                        //myFieldSet.Controls.Add(trHe1);

                    }




                    if (Convert.ToString(DR1["ISVSM_CONTROL_TYPE"]).ToUpper() == "Label".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["ISVSM_FIELD_NAME"]).Trim();
                        myFieldSet.Controls.Add(lbl);

                        //tdHe1.Controls.Add(lbl);
                        //trHe1.Controls.Add(tdHe1);
                        //myFieldSet.Controls.Add(trHe1);

                        //HtmlGenericControl trHe2 = new HtmlGenericControl("tr");
                        //HtmlGenericControl tdHe21 = new HtmlGenericControl("td");
                        //tdHe21.Controls.Add(lbl);
                        //trHe2.Controls.Add(tdHe21);
                        //myFieldSet.Controls.Add(trHe2);

                    }

                    if (Convert.ToString(DR1["ISVSM_CONTROL_TYPE"]) == "RadioButtonList")
                    {


                        RadioButtonList RadList = new RadioButtonList();
                        RadList.ID = "radl" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim();

                        RadList.RepeatDirection = RepeatDirection.Horizontal;
                        RadList.Width = 150;
                        string strRadFieldName = Convert.ToString(DR1["ISVSM_FIELD_NAME"]);
                        string[] arrRadFieldName = strRadFieldName.Split('|');


                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            RadList.Items.Add(new ListItem(arrRadFieldName[intRad], arrRadFieldName[intRad]));

                        }

                        RadList.CssClass = "lblCaption1";

                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            if (RadList.Items[intRad].Value == strComment)
                            {
                                RadList.SelectedValue = strComment;
                            }

                        }

                        myFieldSet.Controls.Add(RadList);

                    }





                    Literal lit = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                    myFieldSet.Controls.Add(lit);

                }


                if (strType == "IP_BEFORE_INDUCTION_ANES_SIGNIN")
                {
                    if (Position == "LEFT")
                    {
                        plhoBeforeInductionAnesthesia.Controls.Add(myFieldSet);
                    }

                }

                if (strType == "IP_BEFORE_SKIN_INCISION_TIMEOUT")
                {
                    if (Position == "RIGHT")
                    {
                        plhoBeforeSkinIncision.Controls.Add(myFieldSet);
                    }

                }

                if (strType == "IP_BEFORE_PT_LEAVES_OPER_ROOM")
                {
                    if (Position == "LEFT")
                    {
                        plhoBeforePTLeavesOperRoom.Controls.Add(myFieldSet);
                    }

                }



                i = i + 1;



            }
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_SITE_VERIFI' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Visible = false;
                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSave.Visible = false;
                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }

                try
                {

                    BindStaff();
                    CommonBAL objCom = new CommonBAL();
                    string strDate = "", strTime = ""; ;
                    strDate = objCom.fnGetDate("dd/MM/yyyy");
                    strTime = objCom.fnGetDate("hh:mm:ss");
                    txtMarkDate.Text = strDate;
                    BindTime();
                    BindData();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }

        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }


        public void generateDynamicControls()
        {

            CreateSegCtrls("IP_BEFORE_INDUCTION_ANES_SIGNIN", "LEFT");
            CreateSegCtrls("IP_BEFORE_SKIN_INCISION_TIMEOUT", "RIGHT");
            CreateSegCtrls("IP_BEFORE_PT_LEAVES_OPER_ROOM", "LEFT");


        }



        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {


                objSiteVer = new IP_SiteVerificationMarking();

                objSiteVer.BranchID = Convert.ToString(Session["Branch_ID"]);
                objSiteVer.EMRID = Convert.ToString(Session["EMR_ID"]);
                objSiteVer.PTID = Convert.ToString(Session["EMR_PT_ID"]);


                objSiteVer.ISVM_NAME_VERBALIZED_PT_CHART = chkNameVerByPT.Checked == true ? "true" : "false";
                objSiteVer.ISVM_BDATE_VERBALIZED_PT_CHART = chkBirthDateVerByPT.Checked == true ? "true" : "false";
                objSiteVer.ISVM_IDBAND_CHART = chkIDBand.Checked == true ? "true" : "false";


                string strFamilyMember = chkFamilyMember.Checked == true ? "true" : "false";
                objSiteVer.ISVM_PT_IDEN_FAMILY_MEMBER = txtFamilyMember.Value + "|" + strFamilyMember;

                string strPTUnit = chkPatientUnit.Checked == true ? "true" : "false";
                objSiteVer.ISVM_PT_IDEN_UNIT = txtPatientUnit.Value + "|" + strPTUnit;


                string strIdenOther = chkVerOther.Checked == true ? "true" : "false";
                objSiteVer.ISVM_PT_IDEN_OTHER = txtVerOther.Value + "|" + strIdenOther;


                objSiteVer.ISVM_PROCEDURE = txtProcedure.Text;

                string strOperSiteNA = chkOperSiteNA.Checked == true ? "true" : "false";
                objSiteVer.ISVM_MARKING = drpOperSiteNARea.SelectedValue + "|" + strOperSiteNA;


                string strMarkPTRefuse = chkMarkPTRefuse.Checked == true ? "true" : "false";
                objSiteVer.ISVM_MARK_REFUSE = txtPTRefuse.Value + "|" + strMarkPTRefuse;


                string strchkMarkOther = chkMarkOther.Checked == true ? "true" : "false";
                objSiteVer.ISVM_MARK_OTHER = txtMarkOther.Value + "|" + strchkMarkOther;



                objSiteVer.ISVM_MARK_FAMILY_PARTICI = chkMarkPTFamily.Checked == true ? "true" : "false";
                objSiteVer.ISVM_MARK_FAMILY_PARTICI_DESC = txtMarkPTFamily.Value;

                objSiteVer.ISVM_MARK_LOCATION = chkLocationMarking.Checked == true ? "true" : "false";
                objSiteVer.ISVM_MARK_LOCATION_DESC = txtchkLocationMarking.Value;

                objSiteVer.ISVM_SITE_MARKED_BY = drpSiteMarkedBy.SelectedValue;
                objSiteVer.ISVM_SITE_CONFIRM_BY = drpConfirmedBy.SelectedValue;

                objSiteVer.ISVM_SITE_MARK_DATE = txtMarkDate.Text.Trim() + " " + drpMarkHour.SelectedValue + ":" + drpMarkMin.SelectedValue + ":00";


                string strVerfiSide = "";

                if (chkLeft.Checked == true)
                {
                    strVerfiSide += chkLeft.Value + "|";
                }

                if (chkCenter.Checked == true)
                {
                    strVerfiSide += chkCenter.Value + "|";
                }
                if (chkRight.Checked == true)
                {
                    strVerfiSide += chkRight.Value + "|";
                }
                if (chkUnknown.Checked == true)
                {
                    strVerfiSide += chkUnknown.Value + "|";
                }


                objSiteVer.ISVM_VERIFI_SIDE = strVerfiSide;


                objSiteVer.ISVM_PT_CAME_FROM = txtPTCameFrom.Value;

                objSiteVer.ISVM_CONSISTENT_CONSENT = chkConsWithConsent.Checked == true ? "true" : "false";
                objSiteVer.ISVM_CONSISTENT_HIS_PHY = chkConsWithHistPhy.Checked == true ? "true" : "false";
                objSiteVer.ISVM_CONSISTENT_XRAY_RESULT = chkConsWithXrayTestRes.Checked == true ? "true" : "false";
                objSiteVer.ISVM_CONSISTENT_SITE_MARK = chkConsWithOperSiteMark.Checked == true ? "true" : "false";



                objSiteVer.ISVM_DISCRIDENTIFIED = chkDiscrepancyIdent.Checked == true ? "true" : "false";
                objSiteVer.ISVM_DISCRIDENTIFIED_EXP = txtDiscrepancyIdent.Value;
                objSiteVer.ISVM_RESOLUTION = chkResolution.Checked == true ? "true" : "false";
                objSiteVer.ISVM_RESOLUTION_DESC = txtResolution.Value;
                objSiteVer.SiteVerificationMarkingAdd();

                SaveSegment("IP_BEFORE_INDUCTION_ANES_SIGNIN", "LEFT");
                SaveSegment("IP_BEFORE_SKIN_INCISION_TIMEOUT", "RIGHT");
                SaveSegment("IP_BEFORE_PT_LEAVES_OPER_ROOM", "LEFT");

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

    }
}