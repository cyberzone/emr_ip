﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplatesControls.aspx.cs" Inherits="EMR_IP.Patient.TemplatesControls" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
      <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../style.css" rel="Stylesheet" type="text/css" />

     <script language="javascript" type="text/javascript">

         function passvalue() {


             var strCtrlData = '';
             var varctrl = document.getElementsByTagName('input');

             for (var i = 0; i <= varctrl.length - 1 ; i++) {

                 if (varctrl[i].type == 'checkbox') {

                     if (varctrl[i].checked == true) {

                         strCtrlData += varctrl[i].value + '\n';
                     }
                 }

             }
             var strCtrlName = document.getElementById('hidCtrlName').value;

             window.opener.BindCtrlData(strCtrlName, strCtrlData);

             opener.focus();
             window.close();

         }
    </script>
     <style type="text/css">

        .Content {
           float:left;padding:5px;width:300px;list-style-type: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
         <asp:hiddenfield id="hidPageName" runat="server" />
         <asp:hiddenfield id="hidCtrlName" runat="server" />

         <table style="padding-left: 5px" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                </td>
          </tr>
    </table>

    <div>
    
    <div style="padding-top: 0px; width: 100%; height: 450px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">
        <table width="100%" cellpadding="5" cellspacing="5">
            <tr>
                <td>
                    <%=strData%>

                </td>
            </tr>

        </table>
     
    </div>
         <table width="100%">
            <tr>
                <td style="float: right; padding-right: 0px;">
                    <asp:button id="btnOK" runat="server" cssclass="button orange small" width="70px"    onclientclick="return passvalue();" text="OK" />
                </td>
            </tr>
        </table>

    </div>
    </form>
</body>
</html>
