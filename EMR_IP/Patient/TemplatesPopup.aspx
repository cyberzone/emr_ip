﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TemplatesPopup.aspx.cs" Inherits="EMR_IP.Patient.TemplatesPopup" %>


<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
      <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../style.css" rel="Stylesheet" type="text/css" />
   
    <script src="../Scripts/jquery-1.7.1.js" type="text/javascript"></script>



    <script language="javascript" type="text/javascript">


        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            var strName = document.getElementById('<%=txtName.ClientID%>').value
            if (/\S+/.test(strName) == false) {

                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Template Name";

                return false;

            }

        }
    </script>
    <script language="javascript" type="text/javascript">

        function passvalue(strType, anchorTag) {
            
            var TemplateData = jQuery(anchorTag).parent().find('.templateData').text();

            window.opener.BindTemplate(strType, TemplateData);

            opener.focus();
            window.close();

        }

        function GetCtrlData() {
            document.getElementById('<%= hidCtrlData.ClientID %>').value = window.opener.GetTemplateData();
        }


    </script>

</head>
<body onload="GetCtrlData()">
    <form id="form1" runat="server">
         <asp:hiddenfield id="hidCtrlName" runat="server" />
         <asp:hiddenfield id="hidCtrlData" runat="server" />
         <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
        <tr>

            <td>
             <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
            </td>
        </tr>
    </table>
        <div id="divSave" runat="server">
         <table width="100%">
                <tr>
                    <td class="lblCaption1" style="height: 30px;">Template Name:
                    </td>
                    <td>
                        <asp:textbox id="txtName" runat="server" BorderWidth="1" bordercolor="#CCCCCC"  width="100%"></asp:textbox>
                     

                    </td>
                </tr>
             <tr>
                  <td class="lblCaption1" style="height: 30px;">
                    </td>
                 <td>
                        <asp:CheckBox ID="chkApply" CssClass="lblCaption1" runat="server" Text="Apply" Visible="false" Checked="true" />
   <asp:button id="btnSave" runat="server" style="padding-left:5px;padding-right:5px;width:50px;height:25px;" cssclass="button orange small"
                            onclick="btnSave_Click" text=" Save " OnClientClick="return SaveVal();" />
                 </td>
             </tr>
            </table>

        </div>
    <div>
       <div style="padding-top: 0px; width: 100%; height: 450px; overflow: auto; border-color: #CCCCCC; border-style: solid; border-width: thin;">

                <asp:gridview id="gvTemplates" runat="server" autogeneratecolumns="False" allowpaging="true" onpageindexchanging="gvTemplates_PageIndexChanging" 
                    enablemodelvalidation="True" width="100%" pagesize="50" >
                <HeaderStyle CssClass="GridHeader" />
                <RowStyle CssClass="GridRow" />
                <AlternatingRowStyle CssClass="GridAlterRow" />
                <Columns>
                     <asp:TemplateField HeaderText="Name">
                        <ItemTemplate>
                            <asp:Label ID="lblBranchID" CssClass="label" runat="server" Text='<%# Bind("IT_BRANCH_ID") %>' Width="100px" Visible="false"></asp:Label>
                            <asp:Label ID="lblType" CssClass="label" runat="server" Text='<%# Bind("IT_TYPE") %>' Width="100px" Visible="false"></asp:Label>
                            <asp:Label ID="lblTemplate" CssClass="label templateData" runat="server" Text='<%# Bind("IT_TEMPLATE") %>' Style="display:none" Width="100px" Visible="true"></asp:Label>

                            <a href="#" onclick="passvalue('<%# Eval("IT_TYPE") %>', this)">
                                <asp:Label ID="lblName" CssClass="label" runat="server" Text='<%# Bind("IT_NAME") %>' Width="100%"></asp:Label>
                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Delete">
                             <ItemTemplate>
                               <asp:ImageButton ID="DeleteTemplates" runat="server" ToolTip="Delete" OnClientClick="return window.confirm('Do you want to Delete?')"  ImageUrl="~/Images/icon_delete.jpg" Width="16px" Height="16px"
                                       OnClick="DeleteTemplates_Click" />&nbsp;&nbsp;
                                                
                            </ItemTemplate>
                             <HeaderStyle Width="50px" />
                           <ItemStyle HorizontalAlign="Center" />
                  </asp:TemplateField>
                   
                </Columns>
                <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />
            </asp:gridview>
            </div>
    </div>
    </form>
</body>
</html>

