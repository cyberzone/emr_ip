﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.Patient
{
    public partial class TemplatesPopup : System.Web.UI.Page
    {
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindGrid()
        {

            IP_Templates obj = new IP_Templates();
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND IT_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND IT_TYPE ='" + hidCtrlName.Value + "'";
            ds = obj.IPTemplatesGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvTemplates.DataSource = ds;
                gvTemplates.DataBind();


            }
            else
            {
                gvTemplates.DataBind();
            }


        }

        void Clear()
        {
            txtName.Text = "";
            Session["Template"] = "";
            ViewState["Template"] = "";
            btnSave.Visible = false;

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            lblStatus.Text = "";

            if (!IsPostBack)
            {
                try
                {
                    this.Page.Title = "TemplatesPopup";
                    // hidPageName.Value = Convert.ToString(Request.QueryString["PageName"]);
                    hidCtrlName.Value = Convert.ToString(Request.QueryString["CtrlName"]);
                    string strMode;
                    strMode = Convert.ToString(Request.QueryString["Mode"]);
                    if (strMode == "Show")
                    {
                        divSave.Visible = false;

                    }

                    ViewState["Template"] = Convert.ToString(Session["Template"]);
                    Session["Template"] = "";


                    BindGrid();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      TemplatesPopup.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }
        }

        protected void gvTemplates_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvTemplates.PageIndex = e.NewPageIndex;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      TemplatesPopup.gvTemplates_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteTemplates_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                Label lblBranchID, lblType, lblName;
                lblBranchID = (Label)gvScanCard.Cells[0].FindControl("lblBranchID");
                lblType = (Label)gvScanCard.Cells[0].FindControl("lblType");
                lblName = (Label)gvScanCard.Cells[0].FindControl("lblName");


                string Criteria = " IT_BRANCH_ID='" + lblBranchID.Text.Trim() + "' and  IT_TYPE='" + lblType.Text.Trim() + "' and  IT_NAME='" + lblName.Text.Trim() + "'";
                CommonBAL objCom = new CommonBAL();
                objCom.fnDeleteTableData("IP_TEMPLATES", Criteria);

                lblStatus.Text = "Template Deleted";
                lblStatus.ForeColor = System.Drawing.Color.Green;

                BindGrid();


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareRegistration.DeleteStaff_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                IP_Templates obj = new IP_Templates();
                obj.IT_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                obj.IT_TYPE = hidCtrlName.Value;
                obj.IT_NAME = txtName.Text.Trim(); ;
                obj.IT_TEMPLATE = Convert.ToString(hidCtrlData.Value);
                obj.IT_DR_CODE = Convert.ToString(Session["User_Code"]);
                obj.IT_DEP_ID = Convert.ToString(Session["User_DeptID"]);

                if (chkApply.Checked == true)
                {
                    obj.IT_APPLY = "1";
                }
                else
                {
                    obj.IT_APPLY = "0";
                }

                obj.IPTemplatesAdd();
                BindGrid();
                Clear();
                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      TemplatesPopup.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }
    }
}