﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="VitalSign.aspx.cs" Inherits="EMR_IP.Patient.VitalSign" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function SaveVitalVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";


            var strVisitDate = document.getElementById('<%=txtVitalDate.ClientID%>').value
            if (/\S+/.test(strVisitDate) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter Vital Date";
                document.getElementById('<%=txtVitalDate.ClientID%>').focus;
                return false;
            }



        }

    </script>

    <script language="javascript" type="text/javascript">

        function BMICalculation() {

            var wt = document.getElementById("<%=txtWeight.ClientID%>").value;
            var ht = document.getElementById("<%=txtHeight.ClientID%>").value;

            var varBmi = BMI(wt, ht)
            document.getElementById("<%=txtBMI.ClientID%>").value = varBmi;

        }

        function BMI(Wt, Ht) {
            if (Wt == 0 || Ht == 0)
                return 0;
            return ((parseFloat(Wt) / parseFloat(Ht) / parseFloat(Ht)) * 10000).toFixed(2)
        }




        function FtoC() {
            var temp = document.getElementById("<%=txtTemperatureF.ClientID%>").value;
            var tempInF = Math.round(((100 / (212 - 32)) * (temp - 32)) * 100) / 100;
            document.getElementById("<%=txtTemperatureC.ClientID%>").value = tempInF;
            if (temp == 0 || temp == "") {
                document.getElementById("<%=txtTemperatureC.ClientID%>").value = '';
            }
        }
        function CtoF() {
            var celc = document.getElementById("<%=txtTemperatureC.ClientID%>").value;
            var tempInC = Math.round((((212 - 32) / 100) * celc + 32) * 100) / 100;
            document.getElementById("<%=txtTemperatureF.ClientID%>").value = tempInC;
            if (celc == 0 || celc == "") {
                document.getElementById("<%=txtTemperatureF.ClientID%>").value = '';
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <input type="hidden" id="hidPTID" runat="server" />
    <table>
        <tr>
            <td class="PageHeader">Vital Signs
               
            </td>
        </tr>
    </table>
    <br />
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
        <tr>

            <td>
                <asp:UpdatePanel ID="UpdatePanel0" runat="server">
                    <ContentTemplate>
                        <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>



    <table width="400px" cellpadding="5px" cellspacing="5px">
        <tr>
            <td class="lblCaption1">Date
            </td>
            <td valign="top">

                <asp:TextBox ID="txtVitalDate" runat="server" Width="70px" Height="20px" CssClass="label" BorderWidth="1px" BorderColor="Red" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server"
                    Enabled="True" TargetControlID="txtVitalDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender3" runat="server" Enabled="true" TargetControlID="txtVitalDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

                <asp:DropDownList ID="drpVitalHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                <asp:DropDownList ID="drpVitalMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1" style="width: 100px; height: 30px;">Weight:
            </td>
            <td class="lblCaption1">
                <asp:TextBox ID="txtWeight" CssClass="label" runat="server" Width="70px" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);" onkeyup="BMICalculation();"></asp:TextBox>
                Kg
            </td>
            <td class="lblCaption1" style="width: 100px; height: 30px;">Respiration
            </td>
            <td class="lblCaption1">
                <asp:TextBox ID="txtRespiration" CssClass="label" runat="server" Width="70px" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                /m
            </td>
        </tr>
        <tr>
            <td class="lblCaption1" style="width: 100px; height: 30px;">Height
            </td>
            <td class="lblCaption1">
                <asp:TextBox ID="txtHeight" CssClass="label" runat="server" Width="70px" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);" onkeyup="BMICalculation();"></asp:TextBox>
                Cms
                                 &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;
                                 BMI
                                <asp:TextBox ID="txtBMI" CssClass="label" runat="server" Width="70px" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>


            </td>
            <td class="lblCaption1">BP:Systolic
            </td>
            <td class="lblCaption1">
                <asp:TextBox ID="txtBPSystolic" CssClass="label" runat="server" Width="70px" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                mm/hg

            </td>
        </tr>
        <tr>
            <td class="lblCaption1" style="width: 100px; height: 30px;">Temperature
            </td>
            <td class="lblCaption1">
                <asp:TextBox ID="txtTemperatureF" CssClass="label" runat="server" Width="70px" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);" onkeyup="FtoC();"></asp:TextBox>
                F
                                &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 <asp:TextBox ID="txtTemperatureC" CssClass="label" runat="server" Width="70px" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);" onblur="CtoF();"></asp:TextBox>
                °C
            </td>
            <td class="lblCaption1">BP:Diastolic
            </td>
            <td class="lblCaption1">
                <asp:TextBox ID="txtBPDiastolic" CssClass="label" runat="server" Width="70px" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                mm/hg
            </td>


        </tr>
        <tr>
            <td class="lblCaption1" style="width: 100px; height: 30px;">Pulse:
            </td>
            <td class="lblCaption1">
                <asp:TextBox ID="txtPulse" CssClass="label" runat="server" Width="70px" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                Beats/Min
            </td>

            <td class="lblCaption1">SPO2
            </td>
            <td>
                <asp:TextBox ID="txtSPO2" CssClass="label" runat="server" Width="70px" BorderWidth="1px" BorderColor="#CCCCCC" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
            </td>

        </tr>
        <tr>
            <td colspan="4">
                <asp:Button ID="btnSaveVital" runat="server" CssClass="button orange small" Width="70px" OnClick="btnSaveVital_Click" OnClientClick="return SaveVitalVal();" Text="Add" />
                <asp:Button ID="btnClear" runat="server" CssClass="button orange small" Width="70px" OnClick="btnClear_Click" Text="Clear" />

            </td>
        </tr>
    </table>



    <div style="padding: 0px; width: 97%; height: 400px; overflow: auto; border: thin; border-color: #cccccc; border-style: Solid;">
        <asp:GridView ID="gvVital" runat="server" AutoGenerateColumns="False"
            EnableModelValidation="True" Width="100%" PageSize="200">
            <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
            <RowStyle CssClass="GridRow" />
            <Columns>
                 <asp:TemplateField HeaderText="Delete">
                    <ItemTemplate>
                        <asp:ImageButton ID="DeleteVitial" runat="server" ToolTip="Delete" OnClientClick="return window.confirm('Do you want to Delete?')" ImageUrl="~/Images/icon_delete.jpg" Width="16px" Height="16px"
                            OnClick="DeleteVitial_Click" />&nbsp;&nbsp;
                    </ItemTemplate>
                    <HeaderStyle Width="50px" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="150px">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkVisitDate" runat="server" OnClick="Select_Click" >
                             <asp:Label ID="lblVitID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_VIT_ID") %>' Visible="false"></asp:Label>
                             <asp:Label ID="lblVitalDate" CssClass="GridRow" Width="70px" runat="server" Text='<%# Bind("IPV_DATEDesc") %>'  ></asp:Label>
                            <asp:Label ID="lblVitalTime" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("VitalTime") %>'  ></asp:Label>
                             
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Weight">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkWeight" runat="server" OnClick="Select_Click" >
                              <asp:Label ID="lblWeight" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_WEIGHT") %>'></asp:Label>
                            Kg
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Height">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkHeight" runat="server" OnClick="Select_Click" >
                               <asp:Label ID="lblHeight" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_HEIGHT") %>' >
                               </asp:Label> Cms  
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="BMI">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkBMI" runat="server" OnClick="Select_Click" >
                                <asp:Label ID="lblBMI" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_BMI") %>' ></asp:Label>
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Temprature">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkTemperature" runat="server" OnClick="Select_Click"  >
                              <asp:Label ID="lblTemperatureF" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_TEMPERATURE_F") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblTemperatureC" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_TEMPERATURE_C") %>' ></asp:Label>
                           °C
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Pulse">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkPulse" runat="server" OnClick="Select_Click" > 
                             <asp:Label ID="lblPulse" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_PULSE") %>'> </asp:Label>Beats/Min
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Respiration">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkRespiration" runat="server" OnClick="Select_Click" > 
                              <asp:Label ID="lblRespiration" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_RESPIRATION") %>'></asp:Label>/m

                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="BP:Systolic">
                    <ItemTemplate>
                        <asp:LinkButton ID="lbllblSystolic" runat="server" OnClick="Select_Click">
                            <asp:Label ID="lblSystolic" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_BP_SYSTOLIC") %>' >mm/hg
                            </asp:Label>mm/hg
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="BP:Systolic">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnklDiastolic" runat="server" OnClick="Select_Click">
                            <asp:Label ID="lblDiastolic" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_BP_DIASTOLIC") %>' ></asp:Label>mm/hg 
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SPO2">
                    <ItemTemplate>
                        <asp:Label ID="lblSPO2" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_SPO2") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
               
            </Columns>
            <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

        </asp:GridView>
    </div>
</asp:Content>
