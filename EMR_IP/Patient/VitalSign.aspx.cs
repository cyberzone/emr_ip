﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.Patient
{
    public partial class VitalSign : System.Web.UI.Page
    {


        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(this.Page.Header.Title + "." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpVitalHour.DataSource = DS;
                drpVitalHour.DataTextField = "Name";
                drpVitalHour.DataValueField = "Code";
                drpVitalHour.DataBind();
            }

            drpVitalHour.Items.Insert(0, "00");
            drpVitalHour.Items[0].Value = "00";


            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpVitalMin.DataSource = DS;
                drpVitalMin.DataTextField = "Name";
                drpVitalMin.DataValueField = "Code";
                drpVitalMin.DataBind();
            }

            drpVitalMin.Items.Insert(0, "00");
            drpVitalMin.Items[0].Value = "00";

           

        }

        void BindVital()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND IPV_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";

            IP_PTVitalSign objPTVital = new IP_PTVitalSign();
            ds = objPTVital.PTVitalSignGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvVital.DataSource = ds;
                gvVital.DataBind();

                string strPermission = hidPermission.Value;
                if (strPermission == "1" || strPermission == "5")
                {
                    gvVital.Columns[0].Visible = false;
                }

            }
            else
            {
                gvVital.DataBind();
            }
        }


        void ClearVital()
        {

            Int32 R = 0;
            if (Convert.ToString(ViewState["EncSelectIndex"]) != "" && Convert.ToString(ViewState["EncSelectIndex"]) != null)
            {
                R = Convert.ToInt32(ViewState["EncSelectIndex"]);
                gvVital.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
            }

            ViewState["IPV_VIT_ID"] = "";
            ViewState["EncSelectIndex"] = "";


            txtVitalDate.Text = "";
            drpVitalHour.SelectedIndex = 0;
            drpVitalMin.SelectedIndex = 0;
            txtWeight.Text = "";
            txtHeight.Text = "";
            txtBMI.Text = "";
            txtTemperatureF.Text = "";
            txtTemperatureC.Text = "";
            txtPulse.Text = "";
            txtRespiration.Text = "";
            txtBPSystolic.Text = "";
            txtBPDiastolic.Text = "";
            txtSPO2.Text = "";

          
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_VITAL' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

          CommonBAL  objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSaveVital.Visible = false;
                //btnDelete.Visible = false;
                btnClear.Visible = false;

            }

            if (strPermission == "5")
            {
                //btnDelete.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSaveVital.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            lblStatus.Text = "";

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }
                try
                {
                    ViewState["IPV_VIT_ID"] = "";



                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtVitalDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    BindTime();
                    BindVital();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      VitalSign.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }

        }


        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                ClearVital();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      VitalSign.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void btnSaveVital_Click(object sender, EventArgs e)
        {
            try
            {

                IP_PTVitalSign objPTVital = new IP_PTVitalSign();

                objPTVital.BranchID = Convert.ToString(Session["Branch_ID"]);
                objPTVital.EMRID = Convert.ToString(Session["EMR_ID"]);
                objPTVital.PTID = Convert.ToString(Session["EMR_PT_ID"]);
 


                objPTVital.IPV_VIT_ID = Convert.ToString(ViewState["IPV_VIT_ID"]);


                if (txtVitalDate.Text.Trim() != "")
                {
                    objPTVital.IPV_DATE = txtVitalDate.Text.Trim() + " " + drpVitalHour.SelectedValue + ":" + drpVitalMin.SelectedValue + ":00";
                }
                else
                {
                    objPTVital.IPV_DATE = "";
                }

                objPTVital.IPV_WEIGHT = txtWeight.Text.Trim();
                objPTVital.IPV_HEIGHT = txtHeight.Text.Trim();
                objPTVital.IPV_BMI = txtBMI.Text.Trim();
                objPTVital.IPV_TEMPERATURE_F = txtTemperatureF.Text.Trim();
                objPTVital.IPV_TEMPERATURE_C = txtTemperatureC.Text.Trim();
                objPTVital.IPV_PULSE = txtPulse.Text.Trim();
                objPTVital.IPV_RESPIRATION = txtRespiration.Text.Trim();
                objPTVital.IPV_BP_SYSTOLIC = txtBPSystolic.Text.Trim();
                objPTVital.IPV_BP_DIASTOLIC = txtBPDiastolic.Text.Trim();
                objPTVital.IPV_SPO2 = txtSPO2.Text.Trim();
                objPTVital.UserId = Convert.ToString(Session["User_Code"]);

                objPTVital.PTVitalSignAdd();

                BindVital();
                ClearVital();
                lblStatus.Text = "Data Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Green;


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      VitalSign.btnSave_Click");
                TextFileWriting(ex.Message.ToString());
                lblStatus.Text = "Data Not Saved.";
                lblStatus.ForeColor = System.Drawing.Color.Red;
            }

        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {
                
             
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["EncSelectIndex"] = gvScanCard.RowIndex;
                gvVital.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblVitID, lblVitalDate, lblVitalTime, lblWeight, lblHeight, lblBMI, lblTemperatureF, lblTemperatureC, lblPulse, lblRespiration, lblSystolic, lblDiastolic, lblSPO2;

                lblVitID = (Label)gvScanCard.Cells[0].FindControl("lblVitID");
                lblVitalDate = (Label)gvScanCard.Cells[0].FindControl("lblVitalDate");
                lblVitalTime = (Label)gvScanCard.Cells[0].FindControl("lblVitalTime");
                lblWeight = (Label)gvScanCard.Cells[0].FindControl("lblWeight");
                lblHeight = (Label)gvScanCard.Cells[0].FindControl("lblHeight");
                lblBMI = (Label)gvScanCard.Cells[0].FindControl("lblBMI");
                lblTemperatureF = (Label)gvScanCard.Cells[0].FindControl("lblTemperatureF");
                lblTemperatureC = (Label)gvScanCard.Cells[0].FindControl("lblTemperatureC");
                lblPulse = (Label)gvScanCard.Cells[0].FindControl("lblPulse");
                lblRespiration = (Label)gvScanCard.Cells[0].FindControl("lblRespiration");
                lblSystolic = (Label)gvScanCard.Cells[0].FindControl("lblSystolic");
                lblDiastolic = (Label)gvScanCard.Cells[0].FindControl("lblDiastolic");
                lblSPO2 = (Label)gvScanCard.Cells[0].FindControl("lblSPO2");


                ViewState["IPV_VIT_ID"] = lblVitID.Text;
                txtVitalDate.Text = lblVitalDate.Text;

                string strSTHour = lblVitalTime.Text;

                string[] arrSTHour = strSTHour.Split(':');
                if (arrSTHour.Length > 1)
                {
                    drpVitalHour.SelectedValue = arrSTHour[0];
                    drpVitalMin.SelectedValue = arrSTHour[1];

                }



                txtWeight.Text = lblWeight.Text;
                txtHeight.Text = lblHeight.Text;
                txtBMI.Text = lblBMI.Text;
                txtTemperatureF.Text = lblTemperatureF.Text;
                txtTemperatureC.Text = lblTemperatureC.Text;
                txtPulse.Text = lblPulse.Text;
                txtRespiration.Text = lblRespiration.Text;
                txtBPSystolic.Text = lblSystolic.Text;
                txtBPDiastolic.Text = lblDiastolic.Text;
                txtSPO2.Text = lblSPO2.Text;




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      VitalSign.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void DeleteVitial_Click(object sender, EventArgs e)
        {
            try
            {
                string strPermission = hidPermission.Value;
                if (strPermission == "7" || strPermission == "9")
                {
                    ImageButton btnEdit = new ImageButton();
                    btnEdit = (ImageButton)sender;

                    GridViewRow gvScanCard;
                    gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                    Label lblVitID;
                    lblVitID = (Label)gvScanCard.Cells[0].FindControl("lblVitID");


                    IP_PTVitalSign objPTVital = new IP_PTVitalSign();
                    objPTVital.IPV_VIT_ID = Convert.ToString(lblVitID.Text);
                    objPTVital.UserId = Convert.ToString(Session["User_Code"]);
                    objPTVital.PTVitalSignDelete();

                    lblStatus.Text = "VitalSign Deleted";
                    lblStatus.ForeColor = System.Drawing.Color.Green;

                    ClearVital(); ;
                    BindVital();
                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      VitalSign.DeleteSchServ_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvVital_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                lblSerial.Text = ((gvVital.PageIndex * gvVital.PageSize) + e.Row.RowIndex + 1).ToString();
            }
        }
    }
}