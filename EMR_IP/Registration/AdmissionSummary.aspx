﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="AdmissionSummary.aspx.cs" Inherits="EMR_IP.Registration.AdmissionSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>


    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="wrapper">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Admission Request </h3>
            </div>
        </div>

            <fieldset>

                <table id="admissionSummary" class="table spacy">
                    <tr>


                        <td>
                            <label for="AdmissionElective" class="lblCaption1">Admission</label>
                        </td>
                        <td colspan="2">

                            <asp:RadioButtonList ID="radAdmissionType" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Enabled="false">
                                <asp:ListItem Value="Elective" Text="Elective" Selected="True"></asp:ListItem>
                                <asp:ListItem Value="Emergency" Text="Emergency"></asp:ListItem>

                            </asp:RadioButtonList>
                        </td>
                    </tr>


                    <tr>
                        <td>
                            <label for="AttendingPhysician" class="lblCaption1">Attending Physician</label></td>
                        <td>
                            <asp:TextBox ID="AttendingPhysician" runat="server" CssClass="lblCaption1" Enabled="false" Width="200px" ></asp:TextBox>
                        </td>

                    </tr>
                </table>
            </fieldset>

            <fieldset>
                <legend class="lblCaption1">Investigations Requested</legend>
                <table class="table spacy">

                    <tr>
                        <td>
                            <label for="ProvisionalDiagonosis" class="lblCaption1">Provisional Diagnosis</label></td>
                        <td>
                            <textarea id="ProvisionalDiagnosis" runat="server" name="ProvisionalDiagnosis" rows="3" cols="112" class="lblCaption1" disabled="disabled"></textarea></td>
                    </tr>
                    <tr>
                        <td style="width: 227px;">
                            <label for="Laboratory" class="lblCaption1">Laboratory</label></td>
                        <td>
                            <textarea id="Laboratory" runat="server" name="Laboratory" rows="3" cols="112" class="lblCaption1" disabled="disabled"></textarea></td>
                    </tr>
                    <tr>
                        <td style="width: 227px;">
                            <label for="Radiological" class="lblCaption1">Radiological</label></td>
                        <td>
                            <textarea id="Radiological" runat="server" name="Radiological" rows="3" cols="112" class="lblCaption1" disabled="disabled"></textarea></td>
                    </tr>
                    <tr>
                        <td style="width: 227px;">
                            <label for="OtherAdmissionSummary" class="lblCaption1">Others</label>
                        </td>
                        <td>
                            <textarea id="OtherAdmissionSummary" runat="server" name="OtherAdmissionSummary" rows="3" cols="112" class="lblCaption1" disabled="disabled"></textarea>
                        </td>
                    </tr>
                </table>
            </fieldset>
            <fieldset>

                <table class="table spacy">
                    <tr>
                        <td style="width: 227px">
                            <label for="ProcedurePlanned" class="lblCaption1">Procedure Planned</label></td>
                        <td>
                            <textarea id="ProcedurePlanned" runat="server" name="ProcedurePlanned" rows="3" cols="112" class="lblCaption1" disabled="disabled"></textarea></td>
                    </tr>
                    <tr>
                        <td style="width: 227px">
                            <label for="Anesthesia" class="lblCaption1">Anesthesia</label></td>
                        <td>
                            <input type="text" id="Anesthesia" runat="server" name="Anesthesia" style="width: 683px;" class="lblCaption1" disabled="disabled" /></td>
                    </tr>
                    <tr>
                        <td style="width: 227px">
                            <label for="Treatment" class="lblCaption1">Treatment</label></td>
                        <td>
                            <textarea id="Treatment" runat="server" name="Treatment" rows="3" cols="112" class="lblCaption1" disabled="disabled"></textarea></td>
                    </tr>

                    <tr>
                        <td style="width: 227px">
                            <label for="ReasonforAdmission" class="lblCaption1">Reason for Admission</label></td>
                        <td>
                            <textarea id="ReasonforAdmission" runat="server" name="ReasonforAdmission" rows="3" cols="112" class="lblCaption1" disabled="disabled"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 227px">
                            <label for="Remarks" class="lblCaption1">Remarks</label></td>
                        <td>
                            <textarea id="Remarks" runat="server" name="Remarks" rows="3" cols="112" class="lblCaption1" disabled="disabled"></textarea>
                        </td>
                    </tr>

                </table>
            </fieldset>
       
    </div>
</asp:Content>
