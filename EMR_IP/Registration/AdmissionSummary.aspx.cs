﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;
using Newtonsoft.Json;

namespace EMR_IP.Registration
{
    public partial class AdmissionSummary : System.Web.UI.Page
    {
        CommonBAL objCom = new CommonBAL();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("AdmissionSummary." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }
               

        Boolean BindAdmissionSummary()
        {
            DataSet DS = new DataSet();
            IP_AdmissionSummary obj = new IP_AdmissionSummary();
            string Criteria = " 1=1 ";
            Criteria += " and IAS_ADMISSION_NO='" + Convert.ToString(Session["IAS_ADMISSION_NO"]) + "'";

            DS = obj.GetIPAdmissionSummary(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                // txtInpatientNo.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_NO"]);

                //  AdmissionDateTime.Value = Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateTime"]);
                ProvisionalDiagnosis.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PROVISIONAL_DIAGNOSIS"]);
                AttendingPhysician.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ATTENDING_PHYSICIANName"]);
                Laboratory.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_LABORATORY"]);
                Radiological.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_RADIOLOGICAL"]);
                OtherAdmissionSummary.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_OTHERS"]);
                ProcedurePlanned.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PROCEDURE_PLANNED"]);
                Anesthesia.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ANESTHESIA"]);
                Treatment.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_TREATMENT"]);
                //ReferralPhysician.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REFERRAL_PHYSICIAN"]);

                radAdmissionType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMN_MODE"]);
                ReasonforAdmission.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REASON_FOR_ADMISSION"]);
                Remarks.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REMARKS"]);


                return true;
            }
            else
            {

                return false;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {

                if (!IsPostBack)
                {

                    
                    BindAdmissionSummary();




                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }


    }
}