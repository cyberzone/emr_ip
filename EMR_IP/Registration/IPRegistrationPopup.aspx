﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IPRegistrationPopup.aspx.cs" Inherits="EMR_IP.Registration.IPRegistrationPopup" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <title>IP Registration</title>

    <script language="javascript" type="text/javascript">

        
    </script>

    
    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }


    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
         <input type="hidden" id="hidPermission" runat="server" value="9" />

        <div>
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            
            
       <br />
        <div style="padding-left: 60%; width: 100%;">
            <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
                Saved Successfully 
                
            </div>
        </div>

             <table width="100%"  >
            <tr>
                <td style="text-align:left;width:50%;" class="PageHeader" >
                    IP Registration 
                </td>
                <td style="text-align:right;;width:50%;">
                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Saved Record" OnClick="btnSave_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>
             <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
            <tr>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel40" runat="server">
                                            <ContentTemplate>
                    <asp:Label ID="lblStatus" runat="server" ForeColor="red" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>
                                                 </ContentTemplate>
                                     </asp:UpdatePanel>
                </td>
            </tr>
                 </table>
             <asp:Button ID="btnNew" runat="server" CssClass="button orange small" Text="New" OnClick="btnNew_Click" />
            <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
                <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Patient Details 1" Width="100%">
                    <ContentTemplate>
                         <fieldset>
                        <table width="100%" border="0">
                            <tr>
                                <td class="lblCaption1" style="height: 25px;">File No
                                </td>
                                <td>
                                     <asp:UpdatePanel ID="UpdatePanel17" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtFileNo" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true" OnTextChanged="txtFileNo_TextChanged"></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>
                          
                                <td class="lblCaption1" style="height: 25px;">Admission No
                                </td>
                                <td>
                                     <asp:UpdatePanel ID="UpdatePanel18" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtAdmissionNo" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" AutoPostBack="true"  OnTextChanged="txtAdmissionNo_TextChanged"></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:UpdatePanel>

                                </td>
                                    <td class="lblCaption1" style="height: 25px;">Admission  Type
                                </td>
                                <td>
                                      <asp:UpdatePanel ID="UpdatePanel19" runat="server">
                                            <ContentTemplate>
                                     <asp:dropdownlist id="AdmissionType" runat="server"  style="width: 144px" class="lblCaption1" AutoPostBack="true" OnSelectedIndexChanged="AdmissionType_SelectedIndexChanged" >
                                         <asp:ListItem Value="DAYCARE">Day Car</asp:ListItem>
                                      <asp:ListItem Value="INPATIENT" Selected="True">InPatient</asp:ListItem>
                                     <asp:ListItem Value="OBSERVATION">Observation</asp:ListItem>
                                   </asp:dropdownlist>
                                        </ContentTemplate>
                                     </asp:UpdatePanel>
                               </td>

                            </tr>
                            <tr>
                                <td class="lblCaption1" style="height: 25px;"> Name
                                </td>
                                <td colspan="5">
                                      <asp:UpdatePanel ID="UpdatePanel20" runat="server">
                                            <ContentTemplate>
                                         <asp:TextBox ID="txtFName" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc"></asp:TextBox>
                                             </ContentTemplate>
                                     </asp:UpdatePanel>
                               </td>
                                </td>
                                 
                            </tr>
                            <tr>

                                <td class="lblCaption1" style="height: 25px;">DOB
                                </td>
                                <td>
                                       <asp:UpdatePanel ID="UpdatePanel23" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtDOB" runat="server" Width="75px" CssClass="label" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>
                                    <asp:Label ID="lblAge" runat="server" CssClass="label" Text="Age"></asp:Label>
                                             </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>

                                <td class="lblCaption1" style="height: 25px;">Sex
                                </td>
                                <td>
                                     <asp:UpdatePanel ID="UpdatePanel24" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtSex" runat="server" Width="150px" CssClass="label" MaxLength="10" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" style="height: 25px;">
                                    
                                </td>
                                <td>
                                   

                                </td>

                            </tr>
                            <tr>

                                <td class="lblCaption1" style="height: 25px;">PO Box
                                </td>
                                <td>
                                     <asp:UpdatePanel ID="UpdatePanel27" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtPoBox" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>
                                                  </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>

                                <td class="lblCaption1" style="height: 25px;">Address
                                </td>
                                <td colspan="3">
                                     <asp:UpdatePanel ID="UpdatePanel28" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtAddress" runat="server" Width="87%" CssClass="label" BorderWidth="1px" Height="30px" TextMode="MultiLine" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>

                            </tr>
                            <tr>

                                <td class="lblCaption1" style="height: 25px;">City
                                </td>
                                <td>
                                     <asp:UpdatePanel ID="UpdatePanel29" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtCity" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>

                                <td class="lblCaption1" style="height: 25px;">Area
                                </td>
                                <td>
                                     <asp:UpdatePanel ID="UpdatePanel30" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtArea" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" style="height: 25px;">Nationality
                                </td>
                                <td>
                                     <asp:UpdatePanel ID="UpdatePanel31" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtNationality" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>
                                            </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>

                            </tr>
                            <tr>

                                <td class="lblCaption1" style="height: 25px;">Home PH.No.
                                </td>
                                <td>
                                     <asp:UpdatePanel ID="UpdatePanel32" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtPhone1" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>
                                                   </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>

                                <td class="lblCaption1" style="height: 25px;">Mob. No 1
                                </td>
                                <td>
                                     <asp:UpdatePanel ID="UpdatePanel33" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtMobile1" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>
                                                   </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" style="height: 25px;">Mob. No 2
                                </td>
                                <td>
                                     <asp:UpdatePanel ID="UpdatePanel34" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtMobile2" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>
                                                   </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>

                            </tr>
                           
                            <tr>
                                <td class="lblCaption1" style="height: 25px;" valign="top">Doctor Name
                                </td>
                                <td valign="top">
                                     <asp:UpdatePanel ID="UpdatePanel35" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtDrName" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>
                                                   </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" style="height: 25px;" valign="top">Ordering Clinician  
                                </td>
                                <td valign="top">
                                     <asp:UpdatePanel ID="UpdatePanel36" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtOrderingClinician" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>
                                                   </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>

                            </tr>




                        </table>

                         </fieldset>
                    </ContentTemplate>
                    
                </asp:TabPanel>


                <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Patient Details 2" Width="100%">
                    <ContentTemplate>
                         <fieldset>
                        <table>
                           <tr>
                                <td class="lblCaption1" style="height: 25px;">Ins. Co
                                </td>
                                <td>
                                      <asp:UpdatePanel ID="UpdatePanel37" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtProviderName" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>
                                       </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" style="height: 25px;">Policy Type
                                </td>
                                <td>
                                      <asp:UpdatePanel ID="UpdatePanel38" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtPolicyType" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>
                                       </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>
                                <td class="lblCaption1" style="height: 25px;">Policy No.
                                </td>
                                <td>
                                      <asp:UpdatePanel ID="UpdatePanel39" runat="server">
                                            <ContentTemplate>
                                    <asp:TextBox ID="txtPolicyNo" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" ReadOnly="true"></asp:TextBox>
                                       </ContentTemplate>
                                     </asp:UpdatePanel>
                                </td>

                            </tr>
                            <tr>

                                <td colspan="2">
                                    <asp:UpdatePanel ID="UpdatePanel16" runat="server">
                                        <ContentTemplate>
                                            <asp:Image ID="imgFront" runat="server" Height="160px" Width="285px" />
                                        </ContentTemplate>

                                    </asp:UpdatePanel>
                                </td>
                                <td colspan="2">

                                    <asp:UpdatePanel ID="UpdatePanel15" runat="server">
                                        <ContentTemplate>
                                            <asp:Image ID="imgBack" runat="server" Height="160px" Width="285px" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                                <td colspan="2" valign="top"></td>
                            </tr>
                        </table>

                        </fieldset>
                    </ContentTemplate>
                </asp:TabPanel>

                <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="Patient Details 3" Width="100%">
                    <ContentTemplate>
                        <fieldset>

                            <table width="100%" cellpadding="3" cellspacing="3" >
                                <tr>
                                    <td class="lblCaption1">Admission Date
                                    </td>
                                    <td colspan="5">
                                        <asp:UpdatePanel ID="UpdatePanel41" runat="server">
                                            <ContentTemplate>
                                                 <asp:TextBox ID="txtAdmiDate" runat="server" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender2" runat="server"
                                                        Enabled="True" TargetControlID="txtAdmiDate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                 <asp:DropDownList ID="drpAdminHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                <asp:DropDownList ID="drpAdminMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                   </tr>
                                <tr>
                                    <td class="lblCaption1">Ward No
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpWardNo" runat="server" CssClass="label" Width="205px" AutoPostBack="true" OnSelectedIndexChanged="drpWardNo_SelectedIndexChanged"></asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Room No
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpRoomNo" runat="server" CssClass="label" Width="205px" AutoPostBack="true" OnSelectedIndexChanged="drpRoomNo_SelectedIndexChanged"></asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Bed No
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpBed" runat="server" CssClass="label" Width="205px"></asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1">Emergent Contact Person 
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtEmgContName" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Relationship to Patient
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtEmgContRelation" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Contact Number
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtEmgContNo" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1">DRG Code
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtDrgCode" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">DRG Amount
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtDRGAmount" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Prior Authorization ID
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtPriorAuthoID" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                 <tr>
                                    <td class="lblCaption1">Authorization Start
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                            <ContentTemplate>
                                                 <asp:TextBox ID="txtFromDate" runat="server" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                                        Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Authorization End
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                            <ContentTemplate>
                                                 <asp:TextBox ID="txtToDate" runat="server" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                                        Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Lenth of Stay
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtLenthOfStay" runat="server" CssClass="label" Width="200px" onkeypress="return OnlyNumeric(event);" ReadOnly="true" ></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                     <td class="lblCaption1">Additional Bed
                                    </td>
                                    <td>
                                        <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtAdditionalBed" runat="server" CssClass="label" Width="50px" Text="0" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1">
                                       Cancel Admission
                                    </td>
                                    <td >
                                       <asp:UpdatePanel runat="server" ID="updatePanel42">
                                            <ContentTemplate>
                                                <input type="checkbox" name="chkStop" id="chkAdmiCancel" class="lblCaption1"  runat="server" value="true"   onclick="ShowReasion()"  /><span class="lblCaption1">Admission Cancel</span>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    
                                    <td class="lblCaption1"> 
                                            <asp:Label ID="lblCancelReasion" runat="server" Text="Cancel Reasion" CssClass="lblCaption1"  style="display:none"  ></asp:Label>
                                    </td>
                                     <td colspan="3" >
                                        <asp:UpdatePanel runat="server" ID="updatePanel43">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtAdmiCancelReasion" runat="server" CssClass="label" TextMode="MultiLine" Width="90%" Height="30px" Style="resize: none;display:none;" ></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </ContentTemplate>
                </asp:TabPanel>
            </asp:TabContainer>
            <table width="100%">
                <tr>
                       <td align="right">
                             <asp:updatepanel id="UpdatePanel22" runat="server">
                                            <ContentTemplate>
                               <asp:Button ID="btnSummaryReport" runat="server" CssClass="button orange small" Text="Summary Report" OnClick="btnSummaryReport_Click" />

                                 <asp:Button ID="btnSummayrHistory" runat="server" CssClass="button orange small" Text="History" OnClick="btnSummayrHistory_Click" />
                                
                                                 </ContentTemplate>
                                     </asp:updatepanel>

                        </td>
                </tr>
            </table>
            
             <div id="divSummaryHistory" style="display: none;overflow:hidden; border: groove; height: 450px; width: 800px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 100px; top: 200px;">
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td style="vertical-align: top;"></td>
                        <td align="right" style="vertical-align: top;">

                            <input type="button" id="Button2" class="ButtonStyle" style="color: black; border: none;" value=" X " onclick="HideHistoryPopup()" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <asp:updatepanel id="UpdatePanel25" runat="server">
                                            <ContentTemplate>
                              <asp:TextBox ID="txtSrcDtls" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="650PX" MaxLength="10" CssClass="label"></asp:TextBox>
                              
                               <asp:Button ID="btnSearch" runat="server" CssClass="button orange small" Width="70px" Text="Search" OnClick="btnSearch_Click" />

                                                </ContentTemplate>
                                 </asp:updatepanel>
                        </td>
                    </tr>
                </table>
                <div style="padding-top: 0px; width: 100%; height: 330px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                    <table cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td colspan="2">

                                  <asp:updatepanel id="UpdatePanel21" runat="server">
                                            <ContentTemplate>
                                <asp:GridView ID="gvSummaryHistory" runat="server" Width="100%" ShowFooter="true"
                    AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvGridView_Sorting"  
                    EnableModelValidation="True"  GridLines="None" >
                    <HeaderStyle CssClass="GridHeader_Blue" />
                 
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAlterRow" />
                    <Columns>
                        <asp:TemplateField HeaderText="File No." SortExpression="IAS_PT_Id" FooterText="File No"  HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                               
                                      <asp:Label ID="lblEMR_ID" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("IAS_EMR_ID") %>'></asp:Label>
                                      <asp:Label ID="lblIPID" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("IAS_IP_ID") %>'></asp:Label>
                                    <asp:Label ID="lblDeptName" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("IAS_DEPARTMENT") %>'></asp:Label>
                                    <asp:Label ID="lblRefDrCode" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("IAS_REFERRAL_PHYSICIAN") %>'></asp:Label>

                                     <asp:Label ID="lblCompID" CssClass="lblCaption1"  runat="server" Text='<%# Bind("IAS_INS_COMP_ID") %>'  Visible="false" ></asp:Label>
                                    <asp:Label ID="lblCompName" CssClass="lblCaption1"  runat="server" Text='<%# Bind("IAS_COMP_NAME") %>' Visible="false"></asp:Label>
                                     <asp:Label ID="lblVisitDate" CssClass="lblCaption1" runat="server" Text='<%# Bind("IAS_DateDesc") %>'   Visible="false"  ></asp:Label>

                                  <asp:LinkButton ID="lnkPatientId" CssClass="lblCaption1"  runat="server"  OnClick="SummaryHistoryEdit_Click"    >

                                    <asp:Label ID="lblPatientId" CssClass="lblCaption1"  runat="server" Text='<%# Bind("IAS_PT_ID") %>'  Visible="true" ></asp:Label>
                                    </asp:LinkButton>
                            </ItemTemplate>
                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient Name" SortExpression="IAS_PT_NAME" FooterText="Patient Name" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPTName" CssClass="lblCaption1"  runat="server"  OnClick="SummaryHistoryEdit_Click"    >
                                    <asp:Label ID="lblPTName" CssClass="lblCaption1"  runat="server" Text='<%# Bind("IAS_PT_NAME") %>'></asp:Label>
                              </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Sex"   FooterText="Sex"  SortExpression="IAS_SEX" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                
                                    <asp:Label ID="lblSex" CssClass="lblCaption1"  runat="server" Text='<%# Bind("IAS_SEX") %>'></asp:Label>
                               
                            </ItemTemplate>

                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Nationality" FooterText="Nationality" SortExpression="IAS_NATIONALITY"   HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                               
                                    <asp:Label ID="Label2" CssClass="lblCaption1" runat="server" Text='<%# Bind("IAS_NATIONALITY") %>'></asp:Label>
                                   
                               
                            </ItemTemplate>

                        </asp:TemplateField>


                          <asp:TemplateField HeaderText="Mobile" FooterText="Mobile" SortExpression="IAS_MOBILE"   HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                               
                                    <asp:Label ID="lblMobile" CssClass="lblCaption1" runat="server" Text='<%# Bind("IAS_MOBILE") %>'></asp:Label>
                                   
                               
                            </ItemTemplate>

                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Admission ID" FooterText="Admission ID" SortExpression="IAS_ADMISSION_NO"   HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                               
                                    <asp:Label ID="lblAdmissionNo" CssClass="lblCaption1" runat="server" Text='<%# Bind("IAS_ADMISSION_NO") %>'></asp:Label>
                                   
                               
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Admission Date & Time" FooterText="Admission Date & Time" SortExpression="AdmissionDateDesc"   HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                               
                                    <asp:Label ID="lblAdmissionDate" CssClass="lblCaption1" runat="server" Text='<%# Bind("AdmissionDateDesc") %>'></asp:Label>
                                    <asp:Label ID="lblAdmissionTime" CssClass="lblCaption1" runat="server" Text='<%# Bind("AdmissionDateTimeDesc") %>'></asp:Label>
                               
                            </ItemTemplate>

                        </asp:TemplateField>


                          <asp:TemplateField HeaderText="Status" FooterText="Status" SortExpression="IAS_STATUSDesc"   HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                               
                                    <asp:Label ID="lblIASStatus" CssClass="lblCaption1" runat="server" Text='<%# Bind("IAS_STATUSDesc") %>'></asp:Label>
                                   
                               
                            </ItemTemplate>

                        </asp:TemplateField>
                          
 
                         
                    </Columns>
                    
                  
                </asp:GridView>

                                            </ContentTemplate>
                                       
                                     </asp:updatepanel>

                            </td>

                        </tr>

                    </table>
                </div>

           </div>
        </div>
    </form>
     <script language="javascript" type="text/javascript">


         function ShowReasion() {

             var chk = document.getElementById("<%=chkAdmiCancel.ClientID%>");

            if (chk.checked == true) {
                document.getElementById("<%=lblCancelReasion.ClientID%>").style.display = 'block'
                document.getElementById("<%=txtAdmiCancelReasion.ClientID%>").style.display = 'block'
            }
            else {
                document.getElementById("<%=lblCancelReasion.ClientID%>").style.display = 'none'
                document.getElementById("<%=txtAdmiCancelReasion.ClientID%>").style.display = 'none'
            }
         }

         function ShowHistoryPopup() {

             document.getElementById("divSummaryHistory").style.display = 'block';


         }

         function HideHistoryPopup() {

             document.getElementById("divSummaryHistory").style.display = 'none';

         }
    </script>
</body>

    
   
</html>
