﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;

namespace EMR_IP.Registration
{
    public partial class IPRegistrationPopup : System.Web.UI.Page
    {

        CommonBAL objCom = new CommonBAL();

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("IPRegistrationPopup." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindTime()
        {
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpAdminHour.DataSource = DS;
                drpAdminHour.DataTextField = "Name";
                drpAdminHour.DataValueField = "Code";
                drpAdminHour.DataBind();
            }

            drpAdminHour.Items.Insert(0, "00");
            drpAdminHour.Items[0].Value = "00";


            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {
                drpAdminMin.DataSource = DS;
                drpAdminMin.DataTextField = "Name";
                drpAdminMin.DataValueField = "Code";
                drpAdminMin.DataBind();
            }

            drpAdminMin.Items.Insert(0, "00");
            drpAdminMin.Items[0].Value = "00";


            ////int AppointmentInterval = 5;
            ////int AppointmentStart = 0;
            ////int AppointmentEnd = 23;
            ////int index = 0;

            ////for (int i = AppointmentStart; i <= AppointmentEnd; i++)
            ////{
            ////    string strHour = Convert.ToString(index);
            ////    if (index <= 9)
            ////    {
            ////        strHour = "0" + Convert.ToString(index);
            ////    }



            ////    drpAdminHour.Items.Insert(index, Convert.ToString(strHour));
            ////    drpAdminHour.Items[index].Value = Convert.ToString(strHour);



            ////    index = index + 1;

            ////}
            ////index = 1;

            ////Int32 intMin = AppointmentInterval;



            ////drpAdminMin.Items.Insert(0, Convert.ToString("00"));
            ////drpAdminMin.Items[0].Value = Convert.ToString("00");


            ////for (int j = AppointmentInterval; j < 60 ; j += AppointmentInterval)
            ////{
            ////    string strTime = j.ToString();
            ////    if (strTime.Length == 1)
            ////    {
            ////        strTime = "0" + strTime;
            ////    }

            ////    drpAdminMin.Items.Insert(index, Convert.ToString(strTime));
            ////    drpAdminMin.Items[index].Value = Convert.ToString(strTime);

            ////    index = index + 1;


            ////}

        }

        void BindWardNo()
        {

            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " HWM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND HWM_STATUS='A' ";

            DS = objCom.fnGetFieldValue("HWM_ID,HWM_NAME", "HMS_WARD_MASTER", Criteria, "HWM_ID");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpWardNo.DataSource = DS;
                drpWardNo.DataTextField = "HWM_NAME";
                drpWardNo.DataValueField = "HWM_ID";
                drpWardNo.DataBind();


            }
            drpWardNo.Items.Insert(0, "--- Select ---");
            drpWardNo.Items[0].Value = "";

        }

        void BindRoom()
        {
            drpRoomNo.Items.Clear();
            drpBed.Items.Clear();

            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " HRM_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND HRM_STATUS='A' ";
            Criteria += " AND HRM_WARD_ID='" + drpWardNo.SelectedValue + "'";


            DS = objCom.fnGetFieldValue("HRM_ID,HRM_NAME", "HMS_ROOM_MASTER", Criteria, "HRM_ID");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpRoomNo.DataSource = DS;
                drpRoomNo.DataTextField = "HRM_NAME";
                drpRoomNo.DataValueField = "HRM_ID";
                drpRoomNo.DataBind();


            }
            drpRoomNo.Items.Insert(0, "--- Select ---");
            drpRoomNo.Items[0].Value = "";



        }


        void BindBed()
        {
            drpBed.Items.Clear();
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " HBD_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "' AND HBD_STATUS='A' ";
            Criteria += " AND HBD_WARD_ID='" + drpWardNo.SelectedValue + "' and  HBD_ROOM_ID='" + drpRoomNo.SelectedValue + "'";


            DS = objCom.fnGetFieldValue("HBD_ID", "HMS_BED_MASTER", Criteria, "HBD_ID");

            if (DS.Tables[0].Rows.Count > 0)
            {

                drpBed.DataSource = DS;
                drpBed.DataTextField = "HBD_ID";
                drpBed.DataValueField = "HBD_ID";
                drpBed.DataBind();


            }
            drpBed.Items.Insert(0, "--- Select ---");
            drpBed.Items[0].Value = "";



        }


        void PatientDataBind()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND HPM_PT_ID = '" + txtFileNo.Text + "'";
            Criteria += " AND HPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            objCom = new CommonBAL();

            ds = objCom.PatientMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtFileNo.Text = ds.Tables[0].Rows[0]["HPM_PT_ID"].ToString();
                txtFName.Text = ds.Tables[0].Rows[0]["FullName"].ToString();


                txtDOB.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DOBDesc"]);
                lblAge.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["HPM_Age1"]);

                //   hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

                txtSex.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_SEX"]);

                txtAddress.Text = ds.Tables[0].Rows[0]["HPM_ADDR"].ToString();
                txtPoBox.Text = ds.Tables[0].Rows[0]["HPM_POBOX"].ToString();

                txtArea.Text = ds.Tables[0].Rows[0]["HPM_AREA"].ToString();
                txtCity.Text = ds.Tables[0].Rows[0]["HPM_CITY"].ToString();
                txtNationality.Text = ds.Tables[0].Rows[0]["HPM_NATIONALITY"].ToString();

                txtPhone1.Text = ds.Tables[0].Rows[0]["HPM_PHONE1"].ToString();
                txtMobile1.Text = ds.Tables[0].Rows[0]["HPM_MOBILE"].ToString();
                txtMobile2.Text = ds.Tables[0].Rows[0]["HPM_PHONE2"].ToString();

                // if (ds.Tables[0].Rows[0].IsNull("HPM_ID_CAPTION") == false && Convert.ToString(ds.Tables[0].Rows[0]["HPM_ID_CAPTION"]) != "")
                //   lblIDCaption.Text = ds.Tables[0].Rows[0]["HPM_ID_CAPTION"].ToString();
                // txtEmiratesID.Text = ds.Tables[0].Rows[0]["HPM_IQAMA_NO"].ToString();

                //  hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);
                //  hidInsCode.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_ID"]);
                //  hidInsName.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                ViewState["User_DeptID"] = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DEP_NAME"]);


                txtProviderName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
                txtPolicyNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_NO"]);
                txtPolicyType.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_POLICY_TYPE"]);
                txtDrName.Text = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_NAME"]);
                ViewState["DR_ID"] = Convert.ToString(ds.Tables[0].Rows[0]["HPM_DR_ID"]);


                // GetPatientVisit();



            }

        }

        void BindPatientPhoto()
        {
            imgFront.ImageUrl = "~/DisplayCard1.aspx?PT_ID=" + txtFileNo.Text;
            imgBack.ImageUrl = "../DisplayCard2.aspx?PT_ID=" + txtFileNo.Text;

        }

        Boolean BindAdmissionSummary()
        {
            DataSet DS = new DataSet();
            IP_AdmissionSummary obj = new IP_AdmissionSummary();
            string Criteria = " 1=1 ";
            Criteria += " and IAS_ADMISSION_NO='" + txtAdmissionNo.Text + "'";

            DS = obj.GetIPAdmissionSummary(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                ViewState["Mode"] = "Edit";

                AdmissionType.Enabled = false;

                txtFName.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_NAME"]);
                txtDOB.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DOBDesc"]);
                lblAge.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_AGE"]);

                txtSex.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_Sex"]);

                txtAddress.Text = DS.Tables[0].Rows[0]["IAS_ADDR"].ToString();
                txtPoBox.Text = DS.Tables[0].Rows[0]["IAS_POBOX"].ToString();

                txtArea.Text = DS.Tables[0].Rows[0]["IAS_AREA"].ToString();
                txtCity.Text = DS.Tables[0].Rows[0]["IAS_CITY"].ToString();
                txtNationality.Text = DS.Tables[0].Rows[0]["IAS_NATIONALITY"].ToString();

                txtPhone1.Text = DS.Tables[0].Rows[0]["IAS_PHONE1"].ToString();
                txtMobile1.Text = DS.Tables[0].Rows[0]["IAS_MOBILE"].ToString();
                txtMobile2.Text = DS.Tables[0].Rows[0]["IAS_PHONE2"].ToString();


                ViewState["User_DeptID"] = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DEPARTMENT"]);


                txtProviderName.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_INS_COMP_NAME"]);
                txtPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_NO"]);
                txtPolicyType.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_TYPE"]);
                txtDrName.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_NAME"]);
                ViewState["DR_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_ID"]);

                txtAdmiDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DATEDesc"]);

                string strSTHour = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DATETimeDesc"]);

                string[] arrSTHour = strSTHour.Split(':');
                if (arrSTHour.Length > 1)
                {
                    drpAdminHour.SelectedValue = arrSTHour[0];
                    drpAdminMin.SelectedValue = arrSTHour[1];

                }

                txtFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_ID"]);
                txtEmgContName.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_EMG_CONT_PERSON"]);
                txtEmgContRelation.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_EMG_CONT_RELATION"]);
                txtEmgContNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_EMG_CONT_NUMBER"]);

                AdmissionType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_TYPE"]);
                drpWardNo.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["IAS_WARD_NO"]);

                if (DS.Tables[0].Rows[0].IsNull("IAS_ROOM_NO") == false)
                {
                    BindRoom();
                    drpRoomNo.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ROOM_NO"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("IAS_BED_NO") == false)
                {
                    BindBed();
                    drpBed.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["IAS_BED_NO"]);
                }


                txtEmgContName.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_EMG_CONT_PERSON"]);
                txtEmgContRelation.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_EMG_CONT_RELATION"]);
                txtEmgContNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_EMG_CONT_NUMBER"]);

                txtDrgCode.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DRG_CODE"]);
                txtDRGAmount.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DRG_AMOUNT"]);
                txtPriorAuthoID.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_AUTHORIZATION_ID"]);

                txtFromDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_AUTHORIZATION_STARTDesc"]);
                txtToDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_AUTHORIZATION_ENDDesc"]);
                txtLenthOfStay.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_LENTH_OF_STAY"]);

                txtAdditionalBed.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADDITIONAL_BED"]);


                if (DS.Tables[0].Rows[0].IsNull("IAS_CANCEL") == false)
                {
                    chkAdmiCancel.Checked = Convert.ToBoolean(DS.Tables[0].Rows[0]["IAS_CANCEL"]);
                }

                if (DS.Tables[0].Rows[0].IsNull("IAS_CANCEL_REASION") == false)
                {
                    txtAdmiCancelReasion.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_CANCEL_REASION"]);
                }
                ViewState["EMR_ID"] = Convert.ToString(DS.Tables[0].Rows[0]["IAS_IP_ID"]);

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowReasion()", true);
                return true;
            }
            else
            {
                return false;
            }
        }

        void BindAdmissionSummaryGrid()
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1  ";



            if (txtSrcDtls.Text.Trim() != "")
            {
                Criteria += " AND ( IAS_PT_ID like '%" + txtSrcDtls.Text.Trim() + "%' OR  IAS_PT_NAME LIKE '%" + txtSrcDtls.Text.Trim() + "%'";
                Criteria += " OR IAS_MOBILE like '%" + txtSrcDtls.Text.Trim() + "%' OR  IAS_ADMISSION_NO LIKE '%" + txtSrcDtls.Text.Trim() + "%'";

                Criteria += " )";
            }


            //string strStartDate = txtFromDate.Text;
            //string[] arrDate = strStartDate.Split('/');
            //string strForStartDate = "";

            //if (arrDate.Length > 1)
            //{
            //    strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            //}

            //if (txtFromDate.Text != "")
            //{
            //    Criteria += " AND   CONVERT(datetime,convert(varchar(10),IAS_DATE,101),101) >= '" + strForStartDate + "'";
            //}



            //string strTotDate = txtToDate.Text;
            //string[] arrToDate = strTotDate.Split('/');
            //string strForToDate = "";

            //if (arrToDate.Length > 1)
            //{
            //    strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            //}


            //if (txtToDate.Text != "")
            //{
            //    Criteria += " AND   CONVERT(datetime,convert(varchar(10),IAS_DATE,101),101) <= '" + strForToDate + "'";
            //}

            //if (txtSrcFileNo.Text.Trim() != "")
            //{
            //    Criteria += " AND IAS_PT_ID like '%" + txtSrcFileNo.Text.Trim() + "%'";

            //}

            //if (txtSrcName.Text.Trim() != "")
            //{
            //    Criteria += " AND IAS_PT_NAME LIKE '%" + txtSrcName.Text.Trim() + "%'";
            //}

            //if (txtSrcMobile1.Text.Trim() != "")
            //{
            //    Criteria += " AND IAS_MOBILE= '" + txtSrcMobile1.Text.Trim() + "'";
            //}


            //if (drpStatus.SelectedValue != "")
            //{
            //    if (drpStatus.SelectedValue == "NA")
            //    {
            //        Criteria += " AND ( IAS_STATUS  IS NULL OR  IAS_STATUS='' OR  IAS_STATUS='NA'  ) ";
            //    }
            //    if (drpStatus.SelectedValue == "A")
            //    {
            //        Criteria += " AND (IAS_STATUS='A' ) ";
            //    }
            //    if (drpStatus.SelectedValue == "D")
            //    {
            //        Criteria += " AND (IAS_STATUS='D' ) ";
            //    }
            //    if (drpStatus.SelectedValue == "AP")
            //    {
            //        Criteria += " AND (IAS_STATUS='AP' ) ";
            //    }
            //}
            //else
            //{
            //    Criteria += " AND (IAS_STATUS !='D' ) ";
            //}
            //if (drpPatientType.SelectedIndex != 0)
            //{
            //    if (drpPatientType.SelectedIndex == 1)
            //    {
            //        Criteria += " AND ( IAS_PT_TYPE ='CA' OR  IAS_PT_TYPE ='CASH') ";
            //    }
            //    else if (drpPatientType.SelectedIndex == 2)
            //    {
            //        Criteria += " AND ( IAS_PT_TYPE ='CR' OR  IAS_PT_TYPE ='CREDIT') ";
            //    }

            //}

            //if (drpSrcCompany.SelectedIndex != 0)
            //{

            //    Criteria += " AND IAS_INS_COMP_ID = '" + drpSrcCompany.SelectedValue + "'";

            //}

            //if (Convert.ToString(Session["User_Category"]).ToLower() != "nurse" && Convert.ToString(Session["User_Category"]).ToLower() != "others")
            //{
            //    Criteria += " AND IAS_DR_ID='" + Convert.ToString(Session["User_Code"]) + "'";
            //}

            IP_AdmissionSummary objAdmSum = new IP_AdmissionSummary();
            DS = objAdmSum.GetIPAdmissionSummary(Criteria);
            //  }
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END



            gvSummaryHistory.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvSummaryHistory.Visible = true;
                gvSummaryHistory.DataSource = DV;
                gvSummaryHistory.DataBind();

            }

            //if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE" || Convert.ToString(Session["User_Category"]).ToUpper() == "OTHERS")
            //{
            //    gvSummaryHistory.Columns[8].Visible = true;
            //}


        }

        void Clear()
        {
            AdmissionType.Enabled = true;
            AdmissionType.SelectedIndex = 0;
            ViewState["Mode"] = "New";
            lblStatus.Text = "";
            drpAdminHour.SelectedIndex = 0;
            drpAdminMin.SelectedIndex = 0;
        }

        void PTDtlsClear()
        {

            txtFName.Text = "";


            txtDOB.Text = "";
            lblAge.Text = "";

            //   hidAgeType.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_AGE_TYPE"]);

            txtSex.Text = "";

            txtAddress.Text = "";
            txtPoBox.Text = "";

            txtArea.Text = "";
            txtCity.Text = "";
            txtNationality.Text = "";

            txtPhone1.Text = "";
            txtMobile1.Text = "";
            txtMobile2.Text = "";




            //  hidInsName.Value = Convert.ToString(ds.Tables[0].Rows[0]["HPM_INS_COMP_NAME"]);
            ViewState["User_DeptID"] = "";


            txtProviderName.Text = "";
            txtPolicyNo.Text = "";
            txtPolicyType.Text = "";
            txtDrName.Text = "";
            ViewState["DR_ID"] = "";

            ViewState["EMR_ID"] = "";
            imgFront.ImageUrl = "";
            imgBack.ImageUrl = "";

        }

        void AdmissionSummaryClear()
        {
            txtFileNo.Text = "";
            txtEmgContName.Text = "";
            txtEmgContRelation.Text = "";
            txtEmgContNo.Text = "";

            if (drpWardNo.Items.Count > 0)
                drpWardNo.SelectedIndex = 0;


            drpRoomNo.Items.Clear();
            drpBed.Items.Clear();

            txtEmgContName.Text = "";
            txtEmgContRelation.Text = "";
            txtEmgContNo.Text = "";

            txtDrgCode.Text = "";
            txtDRGAmount.Text = "";
            txtPriorAuthoID.Text = "";

            txtFromDate.Text = "";
            txtToDate.Text = "";
            txtLenthOfStay.Text = "";

            txtAdditionalBed.Text = "";
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_REGISTRATION' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnSave.Visible = false;
                btnNew.Visible = false;
                // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnSave.Visible = false;
                btnNew.Visible = false;
                //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }


        Boolean CheckAdmissionSummary()
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";

            Criteria += " AND  IAS_PT_ID='" + txtFileNo.Text.Trim() + "'";



            string strStartDate = txtAdmiDate.Text.Trim();
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtAdmiDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),IAS_DATE,101),101) = '" + strForStartDate + "'";
            }

            IP_AdmissionSummary objAdmSum = new IP_AdmissionSummary();
            DS = objAdmSum.GetIPAdmissionSummary(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                return true;
            }

            return false;


        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }


                ViewState["Mode"] = "New";
                DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");
                txtAdmiDate.Text = strFromDate.ToString("dd/MM/yyyy");
                BindTime();
                BindWardNo();

                txtAdmissionNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "IPADMISS");

            }
        }

        protected void txtFileNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                PTDtlsClear();
                PatientDataBind();
                BindPatientPhoto();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "txtFileNo_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpWardNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindRoom();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "drpWardNo_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void drpRoomNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindBed();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "drpRoomNo_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void AdmissionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (AdmissionType.SelectedValue == "DAYCARE")
            {
                txtAdmissionNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "IPDAYCARE");
            }
            else if (AdmissionType.SelectedValue == "INPATIENT")
            {
                txtAdmissionNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "IPADMISS");
            }
            else if (AdmissionType.SelectedValue == "OBSERVATION")
            {
                txtAdmissionNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "IPOBSERV");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToString(ViewState["Mode"]) == "New")
                {
                    if (CheckAdmissionSummary() == false)
                    {
                        IP_PTMaster objPT = new IP_PTMaster();
                        objPT.IPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                        objPT.IPM_PT_ID = txtFileNo.Text;
                        objPT.IAS_DATE = txtAdmiDate.Text + " " + drpAdminHour.SelectedValue + ":" + drpAdminMin.SelectedValue +":00";
                        objPT.AdmissionType = AdmissionType.SelectedValue;

                        objPT.IPM_DR_CODE = Convert.ToString(ViewState["DR_ID"]);
                        objPT.IPM_DR_NAME = txtDrName.Text;
                        objPT.IPM_DEP_ID = Convert.ToString(ViewState["User_DeptID"]);
                        objPT.IAS_EMG_CONT_PERSON = txtEmgContName.Text;
                        objPT.IAS_EMG_CONT_RELATION = txtEmgContRelation.Text;
                        objPT.IAS_EMG_CONT_NUMBER = txtEmgContNo.Text;
                        objPT.IAS_WARD_NO = drpWardNo.SelectedValue;
                        objPT.IAS_ROOM_NO = drpRoomNo.SelectedValue;
                        objPT.IAS_BED_NO = drpBed.SelectedValue;

                        objPT.IAS_DRG_CODE = txtDrgCode.Text;
                        objPT.IAS_DRG_AMOUNT = txtDRGAmount.Text;
                        objPT.IAS_AUTHORIZATION_ID = txtPriorAuthoID.Text;
                        objPT.IAS_AUTHORIZATION_START = txtFromDate.Text;
                        objPT.IAS_AUTHORIZATION_END = txtToDate.Text;
                        objPT.IAS_LENTH_OF_STAY = txtLenthOfStay.Text;
                        objPT.IAS_ADDITIONAL_BED = txtAdditionalBed.Text;

                        objPT.IPAddToWaiting();
                    }
                    else
                    {

                        lblStatus.Text = " Already Registred this patient for Same Date. ";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }
                }
                else
                {
                    string strDRG_AMOUNT = "0", strLenthOfStay = "0", strAdditionalBed = "0";


                    if (txtDRGAmount.Text.Trim() != "")
                    {
                        strDRG_AMOUNT = txtDRGAmount.Text;
                    }
                    if (txtLenthOfStay.Text.Trim() != "")
                    {
                        strLenthOfStay = txtLenthOfStay.Text;
                    }


                    if (txtAdditionalBed.Text.Trim() != "")
                    {
                        strAdditionalBed = txtAdditionalBed.Text.Trim();
                    }
                    objCom = new CommonBAL();

                    string strFieldNameWithValues = "";

                    strFieldNameWithValues += " IAS_DATE=CONVERT(DATETIME,'" + txtAdmiDate.Text + " " + drpAdminHour.SelectedValue + ":" + drpAdminMin.SelectedValue + ":00',103)";
                    strFieldNameWithValues += ",IAS_WARD_NO='" + drpWardNo.SelectedValue + "'";
                    strFieldNameWithValues += ",IAS_ROOM_NO='" + drpRoomNo.SelectedValue + "'";
                    strFieldNameWithValues += ",IAS_BED_NO='" + drpBed.SelectedValue + "'";

                    strFieldNameWithValues += ",IAS_EMG_CONT_PERSON='" + txtEmgContName.Text + "'";
                    strFieldNameWithValues += ",IAS_EMG_CONT_RELATION='" + txtEmgContRelation.Text + "'";
                    strFieldNameWithValues += ",IAS_EMG_CONT_NUMBER='" + txtEmgContNo.Text + "'";

                    strFieldNameWithValues += ",IAS_DRG_CODE='" + txtDrgCode.Text + "'";
                    strFieldNameWithValues += ",IAS_DRG_AMOUNT=" + strDRG_AMOUNT;
                    strFieldNameWithValues += ",IAS_AUTHORIZATION_ID='" + txtPriorAuthoID.Text + "'";

                    strFieldNameWithValues += ",IAS_AUTHORIZATION_START=CONVERT(DATETIME,'" + txtFromDate.Text + "',103)";
                    strFieldNameWithValues += ",IAS_AUTHORIZATION_END=CONVERT(DATETIME,'" + txtToDate.Text + "',103)";
                    //strFieldNameWithValues += ",IAS_LENTH_OF_STAY=" + strLenthOfStay;

                    strFieldNameWithValues += ",IAS_ADDITIONAL_BED=" + strAdditionalBed;

                    strFieldNameWithValues += ",IAS_CANCEL='" + Convert.ToString(chkAdmiCancel.Checked) + "'";
                    strFieldNameWithValues += ",IAS_CANCEL_REASION='" + txtAdmiCancelReasion.Text + "'";

                    string Criteria = " 1=1 ";
                    Criteria += " and IAS_ADMISSION_NO='" + txtAdmissionNo.Text + "'";

                    objCom.fnUpdateTableData(strFieldNameWithValues, "IP_ADMISSION_SUMMARY", Criteria);
                }



                if (Convert.ToString(Session["EMR_ENABLE_MALAFFI"]) == "1" && AdmissionType.SelectedValue == "INPATIENT")  
                {

                    string Criteria = " HMHM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HMHM_PT_ID='" + txtFileNo.Text + "' AND HMHM_IP_ADMISSION_NO='" + txtAdmissionNo.Text + "'";
                    Criteria += " AND HMHM_MESSAGE_TYPE='ADT' AND HMHM_MESSAGE_CODE='ADT-A01'";

                    DataSet DS = new DataSet();
                    CommonBAL objCom = new CommonBAL();

                    DS = objCom.fnGetFieldValue("TOP 1 * ", "HMS_MALAFFI_HL7_MESSAGE_MASTER", Criteria, " HMHM_CREATED_DATE DESC");

                    if (DS.Tables[0].Rows.Count <= 0)
                    {
                        HL7MessageGenerator objHL7Mess = new HL7MessageGenerator();
                        objHL7Mess.PT_ID = txtFileNo.Text;
                        objHL7Mess.VISIT_ID = "";
                        objHL7Mess.EMR_ID = "";
                        objHL7Mess.IP_ADMISSION_NO = txtAdmissionNo.Text;

                        objHL7Mess.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                        objHL7Mess.BRANCH_NAME = Convert.ToString(Session["Branch_Name"]);
                        objHL7Mess.PROVIDER_ID = Convert.ToString(Session["Branch_ProviderID"]);
                        objHL7Mess.FileDescription = GlobalValues.FileDescription;
                        objHL7Mess.UserID = Convert.ToString(Session["User_ID"]);

                        objHL7Mess.MESSAGE_TYPE = "ADT";
                        objHL7Mess.UPLOAD_STATUS = "PENDING";
                        objHL7Mess.MessageCode = "ADT-A01";
                        objHL7Mess.MessageDesc = "Admit/visit Notification";
                        objHL7Mess.MalaffiHL7MessageMasterAdd();
                    }

                }


                lblStatus.Text = " Saved Successfully ";
                lblStatus.ForeColor = System.Drawing.Color.Green;
            //ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowMessage()", true);
            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSave_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void txtAdmissionNo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                PTDtlsClear();
                AdmissionSummaryClear();
                BindAdmissionSummary();
                BindPatientPhoto();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "drpRoomNo_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["Mode"] = "New";
                Clear();
                AdmissionSummaryClear();
                PTDtlsClear();

                txtAdmissionNo.Text = objCom.InitialNoGet(Convert.ToString(Session["Branch_ID"]), "IPADMISS");


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "drpRoomNo_SelectedIndexChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindAdmissionSummaryGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.gvGridView_Sorting");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void btnSummayrHistory_Click(object sender, EventArgs e)
        {
            try
            {
                BindAdmissionSummaryGrid();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowHistoryPopup()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSummayrHistory_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnSummaryReport_Click(object sender, EventArgs e)
        {
            try
            {

                string strRptPath = "";
                strRptPath = "../WebReports/AdmissionRequest.aspx";
                string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(ViewState["EMR_ID"]) + "&EMR_PT_ID=" + txtFileNo.Text.Trim() + "&DR_ID=" + Convert.ToString(ViewState["DR_ID"]) + "&ADMISSION_NO=" + txtAdmissionNo.Text.Trim();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "AdmissionRequest", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSummaryReport_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindAdmissionSummaryGrid();
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowHistoryPopup()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "btnSummayrHistory_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void SummaryHistoryEdit_Click(object sender, EventArgs e)
        {
            PTDtlsClear();
            AdmissionSummaryClear();

            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
            Label lblAdmissionNo = (Label)gvScanCard.Cells[0].FindControl("lblAdmissionNo");



            txtAdmissionNo.Text = lblAdmissionNo.Text;
            BindAdmissionSummary();
            BindPatientPhoto();
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HideHistoryPopup()", true);
        }



    }
}