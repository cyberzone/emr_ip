﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;



namespace EMR_IP.Registration
{
    public partial class PatientPopup : System.Web.UI.Page
    {
        DataSet ds = new DataSet();

        public string PTRegChangePolicyNoCaption = "";
        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BindGrid()
        {
            string Criteria = " 1=1 ";
            if (txtSearch.Text.Trim() != "")
            {

                Criteria += " AND ( HPM_PT_FNAME like '%" + txtSearch.Text.Trim() + "%' OR  HPM_PT_MNAME like '%" + txtSearch.Text.Trim() + "%' " +
                "  OR   HPM_PT_LNAME like '%" + txtSearch.Text.Trim() + "%' OR   HPM_PT_ID like '%" + txtSearch.Text.Trim() + "%' " +
                "  OR   HPM_IQAMA_NO like '%" + txtSearch.Text.Trim() + "%' OR   HPM_MOBILE like '%" + txtSearch.Text.Trim() + "%' " +
                "  OR   HPM_PHONE1 like '%" + txtSearch.Text.Trim() + "%' OR   HPM_PHONE2 like '%" + txtSearch.Text.Trim() + "%' " +
                "  OR   HPM_PHONE3 like '%" + txtSearch.Text.Trim() + "%' OR   HPM_POBOX like '%" + txtSearch.Text.Trim() + "%' " +
                "  OR   HPM_NATIONALITY like '%" + txtSearch.Text.Trim() + "%' OR  HPM_POBOX like '%" + txtSearch.Text.Trim() + "%' " +
                "  OR   HPM_POLICY_NO like '%" + txtSearch.Text.Trim() + "%' OR   HPM_POBOX like '%" + txtSearch.Text.Trim() + "%' " +
                "  OR   HPM_BILL_CODE like '%" + txtSearch.Text.Trim() + "%' OR   HPM_INS_COMP_ID like '%" + txtSearch.Text.Trim() + "%' )";

            }


            //if (drpPlanType.Items.Count > 0)
            //{
            //    if (drpPlanType.SelectedIndex != 0)
            //    {
            //        Criteria += " AND HPM_POLICY_TYPE = '" + drpPlanType.SelectedValue + "'";
            //    }

            //}






            //if (drpSrcDoctor.SelectedIndex != 0)
            //{
            //    Criteria += " AND HPM_DR_ID='" + drpSrcDoctor.SelectedValue + "'";
            //}




            CommonBAL com = new CommonBAL();
            ds = com.PatientMasterGet(Criteria);
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = ds.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END

            lblTotal.Text = "0";

            gvPTList.Visible = false;
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvPTList.Visible = true;
                gvPTList.DataSource = DV;
                gvPTList.DataBind();

                lblTotal.Text = ds.Tables[0].Rows.Count.ToString();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "alert('No Data !');", true);

            }


            ds.Clear();
            ds.Dispose();
        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {

                if (!IsPostBack)
                {

                    hidPageName.Value = Request.QueryString["PageName"].ToString();

                    BindGrid();

                }

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientPopup.Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnFind_Click(object sender, EventArgs e)
        {
            BindGrid();
        }


        protected void gvPTList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {

            gvPTList.PageIndex = e.NewPageIndex;
            BindGrid();

        }

        protected void gvPTList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindGrid();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientPopup.gvPTList_Sorting");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                BindGrid();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientPopup.btnSearch_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void Select_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["ServSelectIndex"] = gvScanCard.RowIndex;
                gvPTList.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblPatientId;

                lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");

                ViewState["PatientId"] = lblPatientId.Text;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientPopup.Select_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void btnAddWaiting_Click(object sender, EventArgs e)
        {
            try
            {
               
                IP_PTMaster objPT = new IP_PTMaster();
                objPT.IPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objPT.IPM_PT_ID = Convert.ToString(ViewState["PatientId"]);
                objPT.IPM_DR_CODE = Convert.ToString(Session["User_Code"]);
                objPT.IPM_DR_NAME = Convert.ToString(Session["FullName"]);
                objPT.IPM_DEP_ID = Convert.ToString(Session["User_DeptID"]);

                objPT.IPAddToWaiting();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientPopup.btnAddWaiting_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }
    }
}