﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;


namespace EMR_IP.Registration
{
    public partial class PatientWaitingList : System.Web.UI.Page
    {
        # region Variable Declaration

        CommonBAL objCom = new CommonBAL();
        // EMR_PTMasterBAL objEMR_PTMast = new EMR_PTMasterBAL();

        IP_PTMaster objPTMaster = new IP_PTMaster();
        IP_AdmissionSummary objAdmSum = new IP_AdmissionSummary();



        #endregion

        #region Methods

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void SaveEmrPTMaster()
        {
            string EMR_ID;
            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            Int32 intCurRow = Convert.ToInt32(ViewState["SelectIndex"]);

            Label lblPatientId = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblPatientId");
            Label lblPTName = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblPTName");
            Label lblAge = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblAge");


            Label lblCompID = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblCompID");
            Label lblCompName = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblCompName");
            Label lblDrID = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblDrID");
            Label lblDrName = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblDrName");
            Label lblDeptName = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblDeptName");
            Label lblVisitType = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblVisitType");
            Label lblRefDrCode = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblRefDrCode");
            Label lblVisitDate = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblVisitDate");


            string strAge = lblAge.Text;


            objPTMaster = new IP_PTMaster();
            objPTMaster.IPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objPTMaster.IPM_PT_ID = lblPatientId.Text;
            objPTMaster.IPM_PT_NAME = lblPTName.Text;
            objPTMaster.IPM_AGE = strAge;// lblAge.Text;
            objPTMaster.IPM_DR_CODE = lblDrID.Text;
            objPTMaster.IPM_DR_NAME = lblDrName.Text;
            objPTMaster.IPM_INS_CODE = lblCompID.Text;
            objPTMaster.IPM_INS_NAME = lblCompName.Text;

            objPTMaster.IPM_START_DATE = strFromDate.ToString("dd/MM/yyyy"); ;
            objPTMaster.IPM_END_DATE = strFromDate.ToString("dd/MM/yyyy");
            objPTMaster.IPM_DEP_ID = lblDeptName.Text;
            objPTMaster.IPM_DEP_NAME = lblDeptName.Text;
            objPTMaster.IPM_PT_TYPE = "IP";
            objPTMaster.IPM_TYPE = "IP";
            objPTMaster.IPM_REF_DR_CODE = lblRefDrCode.Text;
            objPTMaster.VisitDate = lblVisitDate.Text;
            objPTMaster.UserId = Convert.ToString(Session["User_ID"]);

            objPTMaster.PTMasterAdd(out EMR_ID);


        }

        void SaveSalesOrder(string EMRID)
        {
            IP_PTMaster objPTMaster = new IP_PTMaster();
            objPTMaster.IPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objPTMaster.IPM_ID = EMRID;
            objPTMaster.IPM_DR_CODE = Convert.ToString(Session["IAS_DR_ID"]);
            objPTMaster.IPM_DATE = Convert.ToString(Session["IAS_DATE"]);
            objPTMaster.AdmissionNo = Convert.ToString(Session["IAS_ADMISSION_NO"]);
            objPTMaster.IPSalesOrderAdd();
        }

        void EMRPTStartProcess(string EMR_ID)
        {

            DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
            Int32 intCurRow = Convert.ToInt32(ViewState["SelectIndex"]);

            Label lblPatientId = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblPatientId");

            Label lblDrID = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblDrID");

            Label lblVisitDate = (Label)gvGridView.Rows[intCurRow].Cells[0].FindControl("lblVisitDate");


            objPTMaster = new IP_PTMaster();
            objPTMaster.IPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
            objPTMaster.IPM_ID = EMR_ID;
            objPTMaster.IPM_PT_ID = lblPatientId.Text;
            objPTMaster.IPM_DR_CODE = lblDrID.Text;
            objPTMaster.VisitDate = lblVisitDate.Text;
            objPTMaster.UserId = Convert.ToString(Session["User_ID"]);
            objPTMaster.IP_StartProcess();


        }


        string BindDepID(string DeptName)
        {
            string DeptID = "";
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND HDM_DEP_NAME='" + DeptName + "'";

            DS = objCom.DepMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DeptID = Convert.ToString(DS.Tables[0].Rows[0]["HDM_DEP_ID"]);
            }
            return DeptID;
        }

        void BindCompany()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            objCom = new CommonBAL();
            ds = objCom.GetCompanyMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpSrcCompany.DataSource = ds;
                drpSrcCompany.DataValueField = "HCM_COMP_ID";
                drpSrcCompany.DataTextField = "HCM_NAME";
                drpSrcCompany.DataBind();
                drpSrcCompany.Items.Insert(0, "--- All ---");
                drpSrcCompany.Items[0].Value = "0";

            }

            ds.Clear();
            ds.Dispose();

        }

        Boolean CheckEMRAvailable()
        {

            string Criteria = " 1=1 ";
            Criteria += " AND IPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND  IPM_DR_CODE='" + Convert.ToString(Session["IAS_DR_ID"]) + "' AND CONVERT(VARCHAR,IPM_DATE,103)='" + Convert.ToString(Session["IAS_DATE"]) + "'";

            DataSet ds = new DataSet();
            objPTMaster = new IP_PTMaster();
            ds = objPTMaster.PTMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;

        }

        void GetEMRID(out string EMR_ID)
        {
            EMR_ID = "0";
            string Criteria = " 1=1 ";
            Criteria += " AND IPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND  IPM_DR_CODE='" + Convert.ToString(Session["IAS_DR_ID"]) + "' AND CONVERT(VARCHAR,IPM_DATE,103)='" + Convert.ToString(Session["IAS_DATE"]) + "'";

            DataSet ds = new DataSet();
            objPTMaster = new IP_PTMaster();
            ds = objPTMaster.PTMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                EMR_ID = Convert.ToString(ds.Tables[0].Rows[0]["IPM_ID"]);
            }


        }

        void BindAdmissionSummary()
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 ";






            string strStartDate = txtFromDate.Text;
            string[] arrDate = strStartDate.Split('/');
            string strForStartDate = "";

            if (arrDate.Length > 1)
            {
                strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
            }

            if (txtFromDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),IAS_DATE,101),101) >= '" + strForStartDate + "'";
            }



            string strTotDate = txtToDate.Text;
            string[] arrToDate = strTotDate.Split('/');
            string strForToDate = "";

            if (arrToDate.Length > 1)
            {
                strForToDate = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
            }


            if (txtToDate.Text != "")
            {
                Criteria += " AND   CONVERT(datetime,convert(varchar(10),IAS_DATE,101),101) <= '" + strForToDate + "'";
            }

            if (txtSrcFileNo.Text.Trim() != "")
            {
                Criteria += " AND IAS_PT_ID like '%" + txtSrcFileNo.Text.Trim() + "%'";

            }

            if (txtSrcName.Text.Trim() != "")
            {
                Criteria += " AND IAS_PT_NAME LIKE '%" + txtSrcName.Text.Trim() + "%'";
            }

            if (txtSrcMobile1.Text.Trim() != "")
            {
                Criteria += " AND IAS_MOBILE= '" + txtSrcMobile1.Text.Trim() + "'";
            }


            if (drpStatus.SelectedValue != "")
            {
                if (drpStatus.SelectedValue == "NA")
                {
                    Criteria += " AND ( IAS_STATUS  IS NULL OR  IAS_STATUS='' OR  IAS_STATUS='NA'  ) ";
                }
                if (drpStatus.SelectedValue == "A")
                {
                    Criteria += " AND (IAS_STATUS='A' ) ";
                }
                if (drpStatus.SelectedValue == "D")
                {
                    Criteria += " AND (IAS_STATUS='D' ) ";
                }
                if (drpStatus.SelectedValue == "AP")
                {
                    Criteria += " AND (IAS_STATUS='AP' ) ";
                }
            }
            else
            {
                Criteria += " AND (IAS_STATUS !='D' ) ";
            }
            if (drpPatientType.SelectedIndex != 0)
            {
                if (drpPatientType.SelectedIndex == 1)
                {
                    Criteria += " AND ( IAS_PT_TYPE ='CA' OR  IAS_PT_TYPE ='CASH') ";
                }
                else if (drpPatientType.SelectedIndex == 2)
                {
                    Criteria += " AND ( IAS_PT_TYPE ='CR' OR  IAS_PT_TYPE ='CREDIT') ";
                }

            }

            if (drpSrcCompany.SelectedIndex != 0)
            {

                Criteria += " AND IAS_INS_COMP_ID = '" + drpSrcCompany.SelectedValue + "'";

            }

            // if (Convert.ToString(Session["User_Category"]).ToLower() != "nurse" && Convert.ToString(Session["User_Category"]).ToLower() != "others")
            if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST" && Convert.ToString(Session["User_DeptID"]) != "DENTAL")
            {
                Criteria += " AND IAS_DR_ID='" + Convert.ToString(Session["User_Code"]) + "'";
            }


            DS = objAdmSum.GetIPAdmissionSummary(Criteria);
            //  }
            //SORTING CODING - START
            DataView DV = new DataView();
            DV.Table = DS.Tables[0];
            DV.Sort = Convert.ToString(ViewState["SortOrder"]);
            //SORTING CODING - END



            gvGridView.Visible = false;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvGridView.Visible = true;
                gvGridView.DataSource = DV;
                gvGridView.DataBind();

            }

           // if (Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE" || Convert.ToString(Session["User_Category"]).ToUpper() == "OTHERS")
            if (Convert.ToString(Session["User_Category"]).ToUpper() == "ADMIN STAFF" || Convert.ToString(Session["User_ID"]) == "SUPER_ADMIN" || Convert.ToString(Session["User_Category"]).ToUpper() == "NURSE" || Convert.ToString(Session["User_Category"]).ToUpper() == "OTHERS" )
            {
                gvGridView.Columns[8].Visible = true;
            }


        }

        void GetSO_ID(string EMR_ID, out string SO_ID)
        {
            SO_ID = "0";
            string Criteria = " 1=1  and IDSM_FROM_MODULE='IP' ";
            Criteria += " AND IDSM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND  IDSM_DOCTOR_TRANS_ID='" + EMR_ID + "'";

            DataSet ds = new DataSet();
            CommonBAL objCom = new CommonBAL();

            ds = objCom.fnGetFieldValue(" * ", "IP_DR_SALESORDER_MASTER", Criteria, "IDSM_SALESORDER_ID");

            if (ds.Tables[0].Rows.Count > 0)
            {
                SO_ID = Convert.ToString(ds.Tables[0].Rows[0]["IDSM_SALESORDER_ID"]);
            }


        }

        void BindScreenCustomization()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_IP_WL' ";

            DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);
            ViewState["MASTER_BTN"] = "0";
            ViewState["IP_REGIS_BTN"] = "0";
            ViewState["PT_WAITING_ADD_BTN"] = "0";


            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["SEGMENT"]) == "MASTER_BTN")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["MASTER_BTN"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "IP_REGIS_BTN")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["IP_REGIS_BTN"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }

                    if (Convert.ToString(DR["SEGMENT"]) == "PT_WAITING_ADD_BTN")
                    {
                        if (DR.IsNull("CUST_VALUE") == false)
                        {
                            ViewState["PT_WAITING_ADD_BTN"] = Convert.ToString(DR["CUST_VALUE"]);
                        }

                    }


                }
            }



        }
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            if (!IsPostBack)
            {
                TextFileWriting("Rolld ID: " + Convert.ToString(Session["Roll_Id"]));


                if (Convert.ToString(Session["User_ID"]).ToLower() != "admin")
                {
                    //SetPermission();
                }
                try
                {
                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    // txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    //  txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    BindScreenCustomization();

                    // BindGrid();





                    if (Convert.ToString(ViewState["IP_REGIS_BTN"]) == "1")
                    {
                        btnMasterAllo.Visible = true;
                    }
                    if (Convert.ToString(ViewState["MASTER_BTN"]) == "1")
                    {
                        btnIPRegistration.Visible = true;
                    }

                    if (Convert.ToString(ViewState["PT_WAITING_ADD_BTN"]) == "1")
                    {
                        btnPatientList.Visible = true;
                    }



                    if (Convert.ToString(Session["WaitingListFromDate"]) != null && Convert.ToString(Session["WaitingListFromDate"]) != "")
                    {

                        txtFromDate.Text = Convert.ToString(Session["WaitingLIstFromDate"]);
                    }


                    if (Convert.ToString(Session["WaitingListToDate"]) != null && Convert.ToString(Session["WaitingListToDate"]) != "")
                    {

                        txtToDate.Text = Convert.ToString(Session["WaitingLIstToDate"]);
                    }


                    if (Convert.ToString(Session["WaitingListStatus"]) != null && Convert.ToString(Session["WaitingListStatus"]) != "")
                    {

                        drpStatus.SelectedIndex = Convert.ToInt32(Session["WaitingListStatus"]);
                    }



                    Session["WaitingListFromDate"] = txtFromDate.Text;
                    Session["WaitingListToDate"] = txtToDate.Text;
                    Session["WaitingListStatus"] = drpStatus.SelectedIndex;

                    BindCompany();
                    BindAdmissionSummary();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }

            }


        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                Session["WaitingLIstFromDate"] = txtFromDate.Text;
                Session["WaitingLIstToDate"] = txtToDate.Text;
                Session["WaitingLIstStatus"] = drpStatus.SelectedIndex;
                BindAdmissionSummary();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.btnRefresh_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvGridView.PageIndex = e.NewPageIndex;
                BindAdmissionSummary();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.gvGridView_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void gvGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                String strSort = Convert.ToString(ViewState["SortOrder"]);

                if (strSort == e.SortExpression + " Asc")
                {
                    strSort = e.SortExpression + " Desc";
                }
                else
                {
                    strSort = e.SortExpression + " Asc";
                }

                ViewState["SortOrder"] = strSort;
                BindAdmissionSummary();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.gvGridView_Sorting");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void Edit_Click(object sender, EventArgs e)
        {


            LinkButton btnEdit = new LinkButton();
            btnEdit = (LinkButton)sender;

            GridViewRow gvScanCard;
            gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
            ViewState["SelectIndex"] = gvScanCard.RowIndex;


            Label lblPatientId = (Label)gvScanCard.Cells[0].FindControl("lblPatientId");
            Label lblAdmissionNo = (Label)gvScanCard.Cells[0].FindControl("lblAdmissionNo");
            Label lblEMR_ID = (Label)gvScanCard.Cells[0].FindControl("lblEMR_ID");
            Label lblDrID = (Label)gvScanCard.Cells[0].FindControl("lblDrID");
            Label lblDrName = (Label)gvScanCard.Cells[0].FindControl("lblDrName");
            Label lblDeptName = (Label)gvScanCard.Cells[0].FindControl("lblDeptName");
            Label lblVisitType = (Label)gvScanCard.Cells[0].FindControl("lblVisitType");
            Label lblCompID = (Label)gvScanCard.Cells[0].FindControl("lblCompID");
            Label lblVisitDate = (Label)gvScanCard.Cells[0].FindControl("lblVisitDate");
            Label lblIPID = (Label)gvScanCard.Cells[0].FindControl("lblIPID");

            Session["IAS_ADMISSION_NO"] = lblAdmissionNo.Text; //EMR_ID;

            Session["EMR_PT_ID"] = lblPatientId.Text;
            Session["IAS_DR_ID"] = lblDrID.Text;
            Session["IAS_DR_NAME"] = lblDrName.Text;
            Session["IAS_DEP_NAME"] = lblDeptName.Text;
            Session["IAS_DEP_ID"] = BindDepID(lblDeptName.Text);
            //  Session["HPV_VISIT_TYPE"] = lblVisitType.Text;
            Session["IAS_COMP_ID"] = lblCompID.Text;
            Session["IAS_DATE"] = lblVisitDate.Text;

            string EMR_ID = lblIPID.Text;


            if (EMR_ID != "")
            {
                string Criteria1 = " 1=1  AND IPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria1 += " AND IPM_ID = '" + EMR_ID + "' AND IPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";

                DataSet ds1 = new DataSet();
                objPTMaster = new IP_PTMaster();
                ds1 = objPTMaster.PTMasterGet(Criteria1);
                if (ds1.Tables[0].Rows.Count <= 0)
                {
                    TextFileWriting("EMR ID not Matched");
                    GetEMRID(out EMR_ID);
                    if (EMR_ID == "0")
                    {
                        EMR_ID = "";
                        hidSrtProc.Value = "true";
                    }

                }

            }




            try
            {

                if (EMR_ID == "")
                {
                    //SaveEmrPTMaster(out EMR_ID);

                    if (CheckEMRAvailable() == false)
                    {
                        SaveEmrPTMaster();
                        TextFileWriting("EMR Created");
                        GetEMRID(out EMR_ID);
                        TextFileWriting("GetEMRID() return this EMR ID  : " + EMR_ID + " PT ID is " + Convert.ToString(Session["EMR_PT_ID"]));

                        //objEmrPTMast = new EMR_PTMasterBAL();
                        //UpdateEMRIDInVisit(EMR_ID);
                        SaveSalesOrder(EMR_ID);

                        /*
                        string FieldNamem = " HPV_EMR_ID='" + EMR_ID + "', HPV_EMR_STATUS='P' ";
                        string Criteria = "  HPV_TYPE='IP' AND  HPV_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "' AND  HPV_DR_ID='" + Convert.ToString(Session["IAS_DR_ID"]) + "' AND CONVERT(VARCHAR,HPV_DATE,103)='" + Convert.ToString(Session["IAS_DATE"]) + "'";
                        objCom.fnUpdateTableData(FieldNamem, "HMS_PATIENT_VISIT", Criteria);
                        TextFileWriting("Updated EMR ID in visit table EMR id is " + EMR_ID + " PT ID is " + Convert.ToString(Session["EMR_PT_ID"]));
                       */

                        string FieldNamem1 = " IAS_IP_ID='" + EMR_ID + "'  , IAS_STATUS='A' ";
                        string Criteria1 = " IAS_ADMISSION_NO='" + lblAdmissionNo.Text + "'";

                        objCom.fnUpdateTableData(FieldNamem1, "IP_ADMISSION_SUMMARY", Criteria1);

                        //EMRPTStartProcess(EMR_ID);
                        //TextFileWriting("Process Started EMR ID is " + EMR_ID + " PT ID is " + Convert.ToString(Session["EMR_PT_ID"]));
                    }
                }
                //else
                //{
                //    if (hidSrtProc.Value == "true")
                //    {
                //        EMRPTStartProcess(EMR_ID);
                //        TextFileWriting("Process Started for existing, EMR ID is " + EMR_ID + " PT ID is " + Convert.ToString(Session["EMR_PT_ID"]));
                //    }
                //}


                Session["EMR_ID"] = EMR_ID;

                string SO_ID = "";
                GetSO_ID(EMR_ID, out SO_ID);
                Session["SO_ID"] = SO_ID;

                GlobalValues.EMR_ID = EMR_ID;
                GlobalValues.EMR_PT_ID = lblPatientId.Text;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      PatientWaitingList.Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }

            if (EMR_ID != "")
            {
                TextFileWriting("EMR ID checking in PT Master  : " + EMR_ID + " PT ID is " + Convert.ToString(Session["EMR_PT_ID"]));

                string Criteria = " 1=1  AND iPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
                Criteria += " AND iPM_ID = '" + EMR_ID + "' AND iPM_PT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";

                DataSet ds = new DataSet();
                objPTMaster = new IP_PTMaster();
                ds = objPTMaster.PTMasterGet(Criteria);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    TextFileWriting("PT Master having data");
                    Response.Redirect("AdmissionSummary.aspx?PatientID=" + lblPatientId.Text.Trim() + "&EMR_ID=" + EMR_ID);

                }
            }





        }

        protected void lnkEMR_Click(object sender, EventArgs e)
        {
            string rptcall = "BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&User_ID=" + Convert.ToString(Session["User_ID"]) + "&User_Code=" + Convert.ToString(Session["User_Code"]) + "&User_Name=" + Convert.ToString(Session["User_Name"]) + "&Roll_Id=" + Convert.ToString(Session["Roll_Id"]) + "&User_DeptID=" + Convert.ToString(Session["User_DeptID"]) + "&PageName=Home&LoginFrom=CommonPage";

            Response.Redirect(GlobalValues.EMR_PATH + "/CommonPageLoader.aspx?" + rptcall);
        }

        #endregion
    }
}