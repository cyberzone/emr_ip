﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;


namespace EMR_IP
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // if (Session["User_ID"] == null) { Response.Redirect("Default.aspx?NoSession=1"); }


            lblLoginUser.Text = "Welcome " + Convert.ToString(Session["User_Name"]) + ", " + Convert.ToString(Session["User_DeptID"]);

            if (!IsPostBack)
            {
                if (GlobalValues.FileDescription.ToUpper() == "SMCH")
                {
                    if (Convert.ToString(Session["User_Code"]) != "")
                    {
                        DataSet DS = new DataSet();
                        CommonBAL dbo = new CommonBAL();
                        string Criteria = " 1=1  ";
                        Criteria += " AND HSP_STAFF_ID='" + Convert.ToString(Session["User_Code"]) + "'";

                        DS = dbo.StaffPhotoGet(Criteria);

                        if (DS.Tables[0].Rows.Count > 0)
                        {
                            if (DS.Tables[0].Rows[0].IsNull("HSP_PHOTO") == false)
                            {
                                imgStaffPhoto.ImageUrl = "DisplayUserProfileImage.aspx";
                            }

                        }
                    }
                }


            }

        }
    }
}