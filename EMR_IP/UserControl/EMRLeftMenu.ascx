﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EMRLeftMenu.ascx.cs" Inherits="EMR_IP.UserControl.EMRLeftMenu" %>

    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
  <script src="../Scripts/jquery.caret.1.02.min.js" type="text/javascript"></script>

<link href="../Styles/style.css" rel="stylesheet" type="text/css" />

<script>
    function ConfirmProcessComplete1() {

        var vEPM_STATUS = '<%= Convert.ToString(Session["EPM_STATUS"]) %>'
           // alert(vEPM_STATUS);

           if (vEPM_STATUS != 'Y') {
               var isSrtProc = window.confirm('Do you want to complete the process ? ');
               
              }
           window.location.assign('../Billing/Invoice.aspx?CompleteProc=' + isSrtProc)
              return true;

          }

    function readyFn(jQuery) {

        var DeptID= '<%=Convert.ToString(Session["User_DeptID"])%>'

        if (DeptID == 'PEDIATRIC') {
             document.getElementById('liPediatric').style.display = 'block';

         }


        if (DeptID == 'PHYSIOTHERAPY') {
            document.getElementById('liTherapy').style.display = 'block';

        }

        if (DeptID == 'ENT') {
             document.getElementById('liENT').style.display = 'block';

        }

        

        if (DeptID == 'DENTAL') {
            document.getElementById('liDental').style.display = 'block';

        }

        if (DeptID == 'ORTHOPEDIC')
        {
            document.getElementById('liOrthodontic').style.display = 'block';

        }
        if (DeptID == 'OPTHALMOLOGY') {
            document.getElementById('liOphthalmology').style.display = 'block';

        }



    }


    $(document).ready(readyFn);

</script>
<div style="padding-left:0px;width:100%">
<ul id="patient-nav-list" style="list-style:none;padding-left:0px;">
   
     <li class="patient-nav" style="font-size:12px;font-family:'Segoe UI';">
        <a href='<%= ResolveUrl("~/Registration/AdmissionSummary.aspx")%>' class="patient-nav-link"  > 
             <img src='<%= ResolveUrl("~/Images/Icons/image_blue.png")%>'      /><span class="patient-nav-text" >Admission Request </span></a>
    </li>
     
     <li id="liAdmissionNursingAssessment" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';" >
        <a href='<%= ResolveUrl("~/Patient/AdmissionNursingAssessment.aspx")%>' class="patient-nav-link" > 
            <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'   /><span class="patient-nav-text" >Admission Nursing Ass.</span></a>
    </li>
     
      <li class="patient-nav" style="font-size:12px;font-family:'Segoe UI';">
        <a href='<%= ResolveUrl("~/Patient/VitalSign.aspx")%>' class="patient-nav-link" > 
            <img src='<%= ResolveUrl("~/Images/Icons/address-book_blue.png")%>' /><span class="patient-nav-text" >Vital Sign</span></a>
    </li>

    <li id="liPainAssessment" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';" >
        <a href='<%= ResolveUrl("~/Patient/PainAssessment.aspx")%>' class="patient-nav-link" > 
             <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'     /><span class="patient-nav-text" >Pain Assessment</span></a>
    </li>

    <li id="liNursingCarePlan" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';" >
        <a href='<%= ResolveUrl("~/Patient/NursingCarePlan.aspx")%>' class="patient-nav-link" > 
            <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'     /><span class="patient-nav-text" >Nursing Care Plan</span></a>
    </li>

    <li id="liInitialAssessmentForm" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';" >
        <a href='<%= ResolveUrl("~/Patient/InitialAssessmentForm.aspx")%>' class="patient-nav-link" > 
            <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'     /><span class="patient-nav-text" >Initial Assessment Form</span></a>
    </li>

     <li class="patient-nav" style="font-size:12px;font-family:'Segoe UI';">
        <a href='<%= ResolveUrl("~/VisitDetails/Index.aspx")%>' class="patient-nav-link" > 
             <img src='<%= ResolveUrl("~/Images/Icons/address-book_blue.png")%>' /><span class="patient-nav-text" >Visit Details</span></a>
    </li>
       <li id="liPatientMonitoring" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';" >
        <a href='<%= ResolveUrl("~/Patient/PatientMonitoring.aspx")%>' class="patient-nav-link" > 
            <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'    /><span class="patient-nav-text" >Patient Monitoring</span></a>
    </li>
    <li id="liFluidBalancechart" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';" >
        <a href='<%= ResolveUrl("~/Patient/FluidBalancechart.aspx")%>' class="patient-nav-link" > 
             <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'     /><span class="patient-nav-text" >Fluid Balance Chart</span></a>
    </li>
      <li id="li2" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';display:block;" >
        <a href='<%= ResolveUrl("../Common/IPContainer.aspx?PageName=PreAnesthesia")%>' class="patient-nav-link" > 
            <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'      /><span class="patient-nav-text" >PreAnesthesia  </span></a>
    </li>
        <li id="liPreOperativeChecklist" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';" >
        <a href='<%= ResolveUrl("~/Patient/PreOperativeChecklist.aspx")%>' class="patient-nav-link" > 
             <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'     /><span class="patient-nav-text" >PreOperativeChecklist</span></a>
    </li>

     <li id="liSiteVerification" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';" >
        <a href='<%= ResolveUrl("~/Patient/SiteVerification.aspx")%>' class="patient-nav-link" > 
           <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'      /><span class="patient-nav-text" >Site Verification</span></a>
    </li>
      <li id="liAnesthesia" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';display:block;" >
        <a href='<%= ResolveUrl("~/Patient/Anesthesia.aspx")%>' class="patient-nav-link" > 
             <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'     /><span class="patient-nav-text" >Anesthesia Record </span></a>
    </li>
    <li id="liIntraOperativeNursingRecord" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';" >
        <a href='<%= ResolveUrl("~/Patient/IntraOperativeNursingRecord.aspx")%>' class="patient-nav-link" > 
            <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'      /><span class="patient-nav-text" >Intra Oper. Nursing Rec.</span></a>
    </li>

      <li id="li1" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';" >
        <a href='<%= ResolveUrl("~/Patient/OperationNotes.aspx")%>' class="patient-nav-link" > 
            <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'     /><span class="patient-nav-text" >Operation Notes</span></a>
    </li>

     <li id="liRecoveryChart" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';" >
        <a href='<%= ResolveUrl("~/Patient/RecoveryChart.aspx")%>' class="patient-nav-link" > 
            <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'      /><span class="patient-nav-text" >Recovery Chart </span></a>
    </li>
    

      <%   if (Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE")   { %>
  
     <li class="patient-nav" style="font-size:12px;font-family:'Segoe UI';">
        <a href='<%= ResolveUrl("~/EducationalForm/Index.aspx")%>' class="patient-nav-link" > 
             <img src='<%= ResolveUrl("~/Images/Icons/safe_blue.png")%>'     /><span class="patient-nav-text" >Educational Form</span></a>
    </li>
     
    <%  } %>
      

        <li id="liDischargeSummary" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';" >
        <a href='<%= ResolveUrl("~/Patient/DischargeSummary.aspx")%>' class="patient-nav-link" > 
             <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'     /><span class="patient-nav-text" >Discharge Summary</span></a>
    </li>
  
  
    <li id="liOT" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';" >
        <a href='<%= ResolveUrl("~/Patient/OTConsumables.aspx")%>' class="patient-nav-link" > 
           <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'     /><span class="patient-nav-text" >OT Consumables</span></a>
    </li>

    <li id="liCCHD" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';" >
        <a href='<%= ResolveUrl("~/Patient/CongenitaHeartDiseaseScreening.aspx")%>' class="patient-nav-link" > 
            <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'      /><span class="patient-nav-text" >CCHD</span></a>
    </li>

    <li class="patient-nav" style="font-size:12px;font-family:'Segoe UI';">
        <a href='<%= ResolveUrl("~/Patient/BedTransfer.aspx")%>' class="patient-nav-link" > 
            <img src='<%= ResolveUrl("~/Images/Icons/address-book_blue.png")%>' /><span class="patient-nav-text" >Bed Transfer</span></a>
    </li>

 <li id="liInvoice" class="patient-nav" style="font-size:12px;font-family:'Segoe UI';" >
        <a href='<%= ResolveUrl("~/Billing/Invoice.aspx")%>' class="patient-nav-link" > 
            <img src='<%= ResolveUrl("~/Images/Icons/wallet_blue.png")%>'    /><span class="patient-nav-text" >Invoice</span></a>
    </li>
  
    
</ul>
</div>
