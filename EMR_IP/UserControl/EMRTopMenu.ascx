﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EMRTopMenu.ascx.cs" Inherits="EMR_IP.UserControl.EMRTopMenu" %>

     <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />

<link href="../Styles/MenuStyle.css" rel="stylesheet" type="text/css" />
<script language="javascript" type="text/javascript">

    function LoadPage(strPage) {
        alert('test');
        window.top.frames[2].location.href = strPage;

        if (strPage == '../Default.aspx') {
            parent.location.href = strPage;
        }
    }

        function LabResultReport(TransNo,RptType) {
            var Report = "LabReport.rpt";


            if (RptType.toUpperCase() == 'U') {
                Report = "LabReportUsr.rpt";
            }
            var Criteria = " 1=1 ";

            if (TransNo != "") {

                Criteria += ' AND {LAB_TEST_REPORT_MASTER.LTRM_TEST_REPORT_ID}=\'' + TransNo + '\'';

                var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

                win.focus();

            }

        }

        function RadResultReport(TransNo) {

            var Report = "RadiologyReport.rpt";
            var Criteria = " 1=1 ";

            if (TransNo != "") {

                Criteria += ' AND {RAD_TEST_REPORT.RTR_TRANS_NO}=\'' + TransNo + '\'';

                var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
                win.focus();


            }

        }

        function ShowClaimPrintPDF(Report, FileNo, BranchId) {

            var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }

        function ShowConsentFormPrintPDF(FileNo, BranchId,EMR_ID) {
            var Report = "DamanConsentForm.rpt";
            var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            win.focus();

        }

        function ShowReferral(ReferalType, FileNo, BranchId, EMRID) {
            var Report;
            if (ReferalType == "IntRef") {
                Report = "IntReferral.rpt";

            }
            else {
                Report = "ExtReferral.rpt";
            }

            var Criteria = " 1=1 ";
             Criteria += ' AND {EMR_PT_MASTER.EPM_PT_ID}=\'' + FileNo + '\'';
             Criteria += ' AND {EMR_PT_MASTER.EPM_BRANCH_ID}=\'' + BranchId + '\'';
             Criteria += ' AND {EMR_PT_MASTER.EPM_ID}=' + EMRID;

            //var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }

        function ShowClinicalSummaryCrptPDF(FileNo, BranchId, EMRID) {
            Report = "IP_ClinicalSummary.rpt";

            var Criteria = " 1=1 ";
            Criteria += ' AND {IP_PT_MASTER.IPM_PT_ID}=\'' + FileNo + '\'';
            Criteria += ' AND {IP_PT_MASTER.IPM_BRANCH_ID}=\'' + BranchId + '\'';
            Criteria += ' AND {IP_PT_MASTER.IPM_ID}=' + EMRID;

            //var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula={HMS_PATIENT_MASTER.HPM_PT_ID}=\'' + FileNo + '\' and  {HMS_PATIENT_MASTER.HPM_BRANCH_ID}=\'' + BranchId + '\'', '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')
            var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

            win.focus();

        }

        function ConfirmProcessComplete() {

            var vEPM_STATUS = '<%= Convert.ToString(Session["EPM_STATUS"]) %>'
           // alert(vEPM_STATUS);

            if (vEPM_STATUS != 'Y') {
                var isSrtProc = window.confirm('Do you want to complete the process? ');
                document.getElementById('<%=hidCompleteProc.ClientID%>').value = isSrtProc;
            }

            return true;

        }
   
</script>
<input type="hidden" id="hidCompleteProc" runat="server" />

            <div   style="background-image:url(../Images/menubar-bg.jpg);height:30px;"  >
                 
                <div style=" text-align: left;vertical-align:middle;padding-left:30px;">
                     
                    <asp:LinkButton ID="lnkHome" runat="server" Text="Home"  style="color:#ffffff;text-decoration:none;" class="label" OnClick="lnkHome_Click"   ></asp:LinkButton>
                     <asp:ImageButton ID="imgHome" runat="server" ImageUrl="~/Images/home_menu.PNG" OnClick="imgHome_Click" Width="20px" Height="20px"   />
                                              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <select id="selReports" runat="server"  class="lblCaption1"  ></select>
                        <asp:Button ID="btnPrint" runat="server"  style="width:50px;border-radius:5px 5px 5px 5px" CssClass="orange" Text="Print" OnClick="btnPrint_Click" />
                
                    <div style="float:right;padding-right:200px;">
                    <asp:LinkButton ID="lnkLogout" runat="server"  style="padding-left:5px;padding-right:5px;" PostBackUrl="~/Default.aspx" Height="22px" Width="50px" cssClass="button blue small" Text="Logout" OnClientClick="return  window.confirm('Are you sure, you want to logout?');" ></asp:LinkButton>
                    </div>
                 </div>
               
            </div>
