﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.UserControl
{
    public partial class EMRTopMenu : System.Web.UI.UserControl
    {
        CommonBAL objCom = new CommonBAL();

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        public void BindMenus()
        {
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
            objCom.Type = "IP_PRINT_MENU";
            objCom.DEP_ID = Convert.ToString(Session["HPV_DEP_NAME"]);

            DS = objCom.GetMenus();
            selReports.DataSource = DS;
            selReports.DataTextField = "MenuName";
            selReports.DataValueField = "MenuAction";
            selReports.DataBind();

        }

        void BindLaboratoryResult()
        {
            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.LaboratoryResultGet(Convert.ToString(Session["Branch_ID"]), Convert.ToString(Session["EMR_PT_ID"]));
            if (DS.Tables[0].Rows.Count > 0)
            {
                string TransNo = "", RptType = "";

                TransNo = Convert.ToString(DS.Tables[0].Rows[0]["TransNo"]);
                RptType = Convert.ToString(DS.Tables[0].Rows[0]["ReportType"]);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "LabResult", "LabResultReport('" + TransNo + "','" + RptType + "');", true);

            }
        }

        void BindRadiologyResult()
        {

            string Criteria = " 1=1 ";

            Criteria += " AND RTR_PATIENT_ID='" + Convert.ToString(Session["EMR_PT_ID"]) + "'";
            Criteria += " AND RTR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            DS = objCom.RadiologyResultGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                string TransNo = "";
                TransNo = Convert.ToString(DS.Tables[0].Rows[0]["TransNo"]);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Radio Report", "RadResultReport('" + TransNo + "');", true);

            }

        }

        string fnGetClaimFormName()
        {
            string mCompId = "", Dept = "";
            mCompId = Convert.ToString(Session["HPV_COMP_ID"]);
            Dept = Convert.ToString(Session["HPV_DEP_ID"]);
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND  hcm_comp_Id='" + mCompId + "'";
            DS = objCom.GetCompanyMaster(Criteria);

            string RptName;
            RptName = "HMSInsCardPrint.rpt";

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD2") == true || Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) == "")
                {
                    RptName = "HMSInsCardPrint.rpt";
                }
                else
                {
                    RptName = (Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) + ".rpt");
                }

                //if (Dept.ToUpper() == "DENTAL" && chkCompletionRpt.Checked == true)
                //{
                //    if (File.Exists(strReportPath + "Completion" + Dept + RptName) == true)
                //    {
                //        return "Completion" + Dept + RptName;
                //    }
                //    else
                //    {
                //        return RptName;
                //    }
                //}
                if (Dept.ToUpper() == "DENTAL")
                {
                    if (File.Exists(GlobalValues.REPORT_PATH + Dept + RptName) == true)
                    {
                        return Dept + RptName;
                    }
                    else
                    {
                        return RptName;
                    }
                }

                //if (chkCompletionRpt.Checked == true)
                //{
                //    if (File.Exists(strReportPath + "Completion" + RptName) == true)
                //    {
                //        return "Completion" + RptName;
                //    }
                //    else
                //    {
                //        return RptName;
                //    }
                //}

                return RptName;
            }
            else
            {
                return RptName;
            }



        }

        string fnGetEMRClaimFormName()
        {
            string mCompId = "", Dept = "";
            mCompId = Convert.ToString(Session["IAS_COMP_ID"]);
            Dept = Convert.ToString(Session["IAS_DEP_ID"]);
            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND  hcm_comp_Id='" + mCompId + "'";
            DS = objCom.GetCompanyMaster(Criteria);

            string RptName = "";


            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("HCM_FIELD2") == true || Convert.ToString(DS.Tables[0].Rows[0]["HCM_FIELD2"]) == "")
                {
                    RptName = "IP_DAMAN.rpt";

                }
                else
                {
                    RptName = ("IP_" + DS.Tables[0].Rows[0]["HCM_FIELD2"].ToString() + ".rpt");
                }

                if (Dept.ToUpper() == "DENTAL")
                {
                    RptName = (DS.Tables[0].Rows[0]["HCM_FIELD2"].ToString() + ".rpt");
                    if (File.Exists(Server.MapPath("IP_" + Dept + RptName)))
                    {
                        return "IP_" + Dept + RptName;
                    }
                    else
                    {
                        return "IP_" + RptName;
                    }
                }
                return RptName;
            }
            else
            {
                return RptName;
            }



        }

        void CheckReferralType()
        {
            ViewState["ReferalType"] = "IntRef";
            EMR_PTReferral objRef = new EMR_PTReferral();
            DataSet DS = new DataSet();
            DS = objRef.WEMR_spS_GetReferral(Convert.ToString(Session["Branch_ID"]), Convert.ToString(Session["EMR_ID"]));
            if (DS.Tables[0].Rows.Count > 0)
            {

                if (Convert.ToString(DS.Tables[0].Rows[0]["Type"]) != null && Convert.ToString(DS.Tables[0].Rows[0]["Type"]).Equals("2"))
                {
                    ViewState["ReferalType"] = "ExtRef";

                }
                else
                {
                    ViewState["ReferalType"] = "IntRef";

                }
            }


        }

        void ChekcProcessCompleted()
        {

            string Criteria = " 1=1  AND EPM_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND EPM_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";    //BAL.GlobalValues.HomeCare_ID  + "' ";
            //Criteria += " AND EPM_STATUS = 'Y' ";

            DataSet ds = new DataSet();
            EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();
            ds = objEmrPTMast.GetEMR_PTMaster(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ViewState["EPM_STATUS"] = Convert.ToString(ds.Tables[0].Rows[0]["EPM_STATUS"]);
                ViewState["EPM_VIP"] = Convert.ToString(ds.Tables[0].Rows[0]["EPM_VIP"]);
                ViewState["EPM_ROYAL"] = Convert.ToString(ds.Tables[0].Rows[0]["EPM_ROYAL"]);
                ViewState["EPM_START_DATE"] = Convert.ToString(ds.Tables[0].Rows[0]["EPM_START_DATEDesc"]);

            }


        }

        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                try
                {
                    BindMenus();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareTopmenu.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {

                string strControl = selReports.Value;// hidPrintClicked.Value;// gvdrpReports.SelectedValue;

                string strRptPath = "";


                if (strControl.ToUpper() == "DISCHARGESTATEMENT")
                {
                    strRptPath = "../WebReports/DischargeStatement.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["IAS_DR_ID"]) + "&ADMISSION_NO=" + Convert.ToString(Session["IAS_ADMISSION_NO"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "DischargeStatement", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);
                }

                if (strControl.ToUpper() == "ClinicalSummary".ToUpper())
                {
                    strRptPath = "../WebReports/IPClinicalSummary.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]) + "&ADMISSION_NO=" + Convert.ToString(Session["IAS_ADMISSION_NO"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "IPClinicalSummary", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);
                }

                if (strControl.ToUpper() == "AdmissionRequest".ToUpper())
                {
                    strRptPath = "../WebReports/AdmissionRequest.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]) + "&ADMISSION_NO=" + Convert.ToString(Session["IAS_ADMISSION_NO"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "AdmissionRequest", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1');", true);
                }


                else if (strControl.ToUpper() == "PRESCRIPTION")
                {
                    strRptPath = "../WebReports/Prescription.aspx";
                    string rptcall = @strRptPath + "?EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "&EMR_PT_ID=" + Convert.ToString(Session["EMR_PT_ID"]) + "&DR_ID=" + Convert.ToString(Session["HPV_DR_ID"]);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Presc", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);

                }

                else if (strControl.ToUpper() == "CLAIMFORM" || strControl.ToUpper() == "WEBCLAIMFORM")
                {

                    string strRptName = "";
                    strRptName = fnGetEMRClaimFormName();

                    if (File.Exists(GlobalValues.REPORT_PATH + strRptName) == false)
                    {
                        // lblStatus.Text = "File Not Found";
                        //lblStatus.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ClaimForm", "ShowClaimPrintPDF('" + strRptName + "','" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);

                    //  string strRptName = "";
                    //  strRptName = fnGetClaimFormName();

                    //if (File.Exists(GlobalValues.REPORT_PATH  + strRptName) == false)
                    //{
                    //   // lblStatus.Text = "File Not Found";
                    //    //lblStatus.ForeColor = System.Drawing.Color.Red;
                    //    goto FunEnd;
                    //}

                    // ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowClaimPrintPDF('" + strRptName + "','" +  Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "');", true);
                    // ScriptManager.RegisterStartupScript(this, this.GetType(), "ClaimForm", "ShowClaimPrintPDF('" + strRptName + "','" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "');", true);
                    //if(Convert.ToString(Session["HPV_COMP_ID"]) != "" && Convert.ToString(Session["HPV_COMP_ID"]) != null)
                    //{
                    //strRptPath = GlobalValues.HMS_Path + "/WebReport/WebReportLoader.aspx";
                    //string rptcall = @strRptPath + "?PatientId=" + Convert.ToString(Session["EMR_PT_ID"]) + "&BranchId=" + Convert.ToString(Session["Branch_ID"]) + "&EPMId=" + Convert.ToString(Session["EMR_ID"]) + "&DRId=" + Convert.ToString(Session["HPV_DR_ID"]) + "&DrDepName=" + Convert.ToString(Session["HPV_DEP_NAME"]) + "&Date=" + Convert.ToString(Session["EPM_DATE"]) + "&CompanyId=" + Convert.ToString(Session["HPV_COMP_ID"]);

                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Lab", "window.open('" + rptcall + "','_new','menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1');", true);
                    //}




                }
                else if (strControl.ToUpper() == "CLINICALSUMMARYCRPT")
                {

                    string strRptName = "";
                    strRptName = fnGetEMRClaimFormName();

     
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ClaimForm", "ShowClinicalSummaryCrptPDF('" +  Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);

                   


                }

                else if (strControl.ToUpper() == "ConsentForm".ToUpper())
                {


                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ConsentForm", "ShowConsentFormPrintPDF('" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);

                    




                }
                else if (strControl.ToUpper() == "REFERRAL")
                {
                    CheckReferralType();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Referral", "ShowReferral('" + Convert.ToString(ViewState["ReferalType"]) + "','" + Convert.ToString(Session["EMR_PT_ID"]) + "','" + Convert.ToString(Session["Branch_ID"]) + "','" + Convert.ToString(Session["EMR_ID"]) + "');", true);


                }


            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      EMRTopMenu.btnPrint_Click");
                TextFileWriting(ex.Message.ToString());
            }

        FunEnd: ;

        }

        protected void imgHome_Click(object sender, ImageClickEventArgs e)
        {/*
            try
            {
                string strDate = "", strTime = ""; ;
                strDate = objCom.fnGetDateddMMyyyy();
                strTime = objCom.fnGetDate("hh:mm:ss");

                ChekcProcessCompleted();

                if (Convert.ToString(ViewState["EPM_STATUS"]).ToUpper() != "Y" && hidCompleteProc.Value == "true")
                {
                    EMR_PTMasterBAL objPTMaster = new EMR_PTMasterBAL();
                    objPTMaster.EPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPTMaster.EPM_ID = Convert.ToString(Session["EMR_ID"]);

                    objPTMaster.EPM_CRITICAL_NOTES = "";
                    objPTMaster.ProcessCompleted = "Completed";
                    objPTMaster.EPM_VIP = Convert.ToString(ViewState["EPM_VIP"]);// chkVIP.Checked == true ? "1" : "0";
                    objPTMaster.EPM_ROYAL = Convert.ToString(ViewState["EPM_ROYAL"]);//chkRoyal.Checked == true ? "1" : "0";
                    objPTMaster.EPM_START_DATE = Convert.ToString(ViewState["EPM_START_DATE"]);// lblBeginDt.Text;

                    //DateTime strEndmDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    //string Time = DateTime.Now.ToString("HH:mm:ss tt");
                    // objPTMaster.EPM_END_DATE = strEndmDate.ToString("dd/MM/yyyy") + " " + Time;
                    objPTMaster.EPM_END_DATE = strDate + " " + strTime;


                    objPTMaster.EPM_DR_CODE = Convert.ToString(Session["HPV_DR_ID"]);
                    objPTMaster.EPM_DATE = Convert.ToString(Session["HPV_DATE"]);//  lblEMRDate.Text ;

                    objPTMaster.AddPatientProcess();
                }
        
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareTopmenu.imgHome_Click");
                TextFileWriting(ex.Message.ToString());
            }
    */
            Response.Redirect("~/Registration/PatientWaitingList.aspx");
        }

        protected void lnkHome_Click(object sender, EventArgs e)
        {
            try
            {
                string strDate = "", strTime = ""; ;
                strDate = objCom.fnGetDateddMMyyyy();
                strTime = objCom.fnGetDate("hh:mm:ss");

                ChekcProcessCompleted();

                if (Convert.ToString(ViewState["EPM_STATUS"]).ToUpper() != "Y" && hidCompleteProc.Value == "true")
                {
                    EMR_PTMasterBAL objPTMaster = new EMR_PTMasterBAL();
                    objPTMaster.EPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPTMaster.EPM_ID = Convert.ToString(Session["EMR_ID"]);

                    objPTMaster.EPM_CRITICAL_NOTES = "";
                    objPTMaster.ProcessCompleted = "Completed";
                    objPTMaster.EPM_VIP = Convert.ToString(ViewState["EPM_VIP"]);// chkVIP.Checked == true ? "1" : "0";
                    objPTMaster.EPM_ROYAL = Convert.ToString(ViewState["EPM_ROYAL"]);//chkRoyal.Checked == true ? "1" : "0";
                    objPTMaster.EPM_START_DATE = Convert.ToString(ViewState["EPM_START_DATE"]);// lblBeginDt.Text;

                    //DateTime strEndmDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    //string Time = DateTime.Now.ToString("HH:mm:ss tt");
                    // objPTMaster.EPM_END_DATE = strEndmDate.ToString("dd/MM/yyyy") + " " + Time;
                    objPTMaster.EPM_END_DATE = strDate + " " + strTime;
                    // TextFileWriting(Convert.ToString(ViewState["EPM_START_DATE"]));
                    // TextFileWriting(strDate + " " + strTime);

                    objPTMaster.EPM_DR_CODE = Convert.ToString(Session["HPV_DR_ID"]);
                    objPTMaster.EPM_DATE = Convert.ToString(Session["HPV_DATE"]);//  lblEMRDate.Text ;

                    objPTMaster.AddPatientProcess();


                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      HomeCareTopmenu.lnkHome_Click");
                TextFileWriting(ex.Message.ToString());
            }

            Response.Redirect("~/Registration/PatientWaitingList.aspx");
        }
    }
}