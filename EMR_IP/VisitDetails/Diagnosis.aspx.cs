﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;


namespace EMR_IP.VisitDetails
{
    public partial class Diagnosis1 : System.Web.UI.Page
    {
        static Int32 DiagCount = 0;
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMRLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_DG' AND SEGMENT='DG_SEARCH_LIST'";

            DataSet DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("CUST_VALUE") == false)
                {
                    ViewState["CUST_VALUE"] = DS.Tables[0].Rows[0]["CUST_VALUE"].ToString();
                }
                else
                {
                    ViewState["CUST_VALUE"] = "0";
                }
            }


        }

        void BindData()
        {

            string SearchFilter = txtSearch.Text.Trim();
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();
            if (drpSrcType.SelectedIndex == 0)
            {
                ds = dbo.HaadServicessListGet("Diagnosis", SearchFilter, "", Convert.ToString(Session["User_DeptID"]));
            }
            else
            {
                string Criteria = " 1=1 ";

                if (txtSearch.Text.Trim() != "")
                {
                    Criteria += " AND( EDF_CODE like'%" + txtSearch.Text.Trim() + "%' OR  EDF_NAME  like'%" + txtSearch.Text.Trim() + "%')";
                }
                Criteria += " AND EDF_TYPE='ICD' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "')";

                ds = dbo.FavoritesGet(Criteria);
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvServicess.DataSource = ds;
                gvServicess.DataBind();

                string strPermission = hidPermission.Value;
                if (strPermission == "1" || strPermission == "7")
                {
                    gvServicess.Columns[0].Visible = false;
                }

            }
            else
            {
                gvServicess.DataBind();
            }
        }

        Boolean CheckFavorite(string Code)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string Criteria = " 1=1 ";

            Criteria += " AND EDF_CODE='" + Code + "'";

            Criteria += " AND EDF_TYPE='ICD' AND (EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "')";

            ds = dbo.FavoritesGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void BindDiagnosis()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND IPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            IP_PTDiagnosis objDiag = new IP_PTDiagnosis();
            DS = objDiag.IPDiagnosisGet(Criteria);
            DiagCount = DS.Tables[0].Rows.Count;
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvDiagnosis.DataSource = DS;
                gvDiagnosis.DataBind();

                string strPermission = hidPermission.Value;
                if (strPermission == "1" || strPermission == "5")
                {
                    gvDiagnosis.Columns[0].Visible = false;
                }

            }
            else
            {
                gvDiagnosis.DataBind();
            }

            txtRemarks.Text = "";
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                if (Convert.ToString(DR["IPD_REMARKS"]) != "")
                    txtRemarks.Text = Convert.ToString(DR["IPD_REMARKS"]);
            }

        FunEnd: ;
        }

        Boolean CheckDiagnosis(string Code)
        {
            string Criteria = " 1=1 ";

            Criteria += " AND IPD_DIAG_CODE='" + Code + "'";
            Criteria += " AND IPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DataSet DS = new DataSet();
            IP_PTDiagnosis objDiag = new IP_PTDiagnosis();
            DS = objDiag.IPDiagnosisGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        Boolean CheckPrincipalDiagnosis()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND IPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND IPD_TYPE='Principal'";
            Criteria += " AND IPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DataSet DS = new DataSet();
            IP_PTDiagnosis objDiag = new IP_PTDiagnosis();
            DS = objDiag.IPDiagnosisGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        void ClearFavorite()
        {

            ViewState["Code"] = "";
            ViewState["Description"] = "";
            ViewState["Price"] = "";
            ViewState["gvServSelectIndex"] = "";
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_DIAG' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnAddFav.Visible = false;
                btnDeleteFav.Visible = false;
                // btnClear.Visible = false;

            }

            if (strPermission == "5")
            {
                btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnAddFav.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }


        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {

            lblMessage.Text = "";
            string parameter = Request["__EVENTARGUMENT"];
            if (parameter == "TextData")
            {
                BindData();
                txtSearch.Focus();

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "LabResult", "ShowSearch();", true);



            }

            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }
                try
                {
                    if (CheckPrincipalDiagnosis() == true)
                    {
                        drpDiagType.SelectedIndex = 1;
                    }
                    BindScreenCustomization();

                    if (hidPermission.Value == "9")
                    {
                        if (Convert.ToString(ViewState["CUST_VALUE"]) == "1")
                        {
                            drpSrcType.SelectedIndex = 1;
                            btnDeleteFav.Visible = true;
                            btnAddFav.Visible = false;
                        }
                        else
                        {
                            drpSrcType.SelectedIndex = 0;
                            btnAddFav.Visible = true;
                            btnDeleteFav.Visible = false;
                        }
                    }

                    BindData();
                    BindDiagnosis();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.txtSearch_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvServicess_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvServicess.PageIndex = e.NewPageIndex;
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.gvServicess_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        }




        protected void Edit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
                gvServicess.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.Cells[0].FindControl("lblDesc");
                Label lblPrice = (Label)gvScanCard.Cells[0].FindControl("lblPrice");

                ViewState["Code"] = lblCode.Text;
                ViewState["Description"] = lblDesc.Text;
                ViewState["Price"] = lblPrice.Text;
                lblCode.BackColor = System.Drawing.Color.FromName("#c5e26d");
                lblDesc.BackColor = System.Drawing.Color.FromName("#c5e26d");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void Add_Click(object sender, EventArgs e)
        {
            try
            {
                if (gvDiagnosis.Rows.Count > 0 && drpDiagType.SelectedValue == "Principal")
                {
                    if (CheckPrincipalDiagnosis() == true)
                    {
                        lblMessage.Text = "Principal diagnosis already added";
                        lblMessage.ForeColor = System.Drawing.Color.Red;
                        goto FunEnd;
                    }
                }


                Int32 intPos = 1;
                if (gvDiagnosis.Rows.Count >= 1)
                {
                    intPos = Convert.ToInt32(gvDiagnosis.Rows.Count) + 1;

                }

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;

                // GridViewRow currentRow = (GridViewRow)((ImageButton)sender).Parent.Parent.Parent.Parent;


                Label lblCode = (Label)gvScanCard.FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.FindControl("lblDesc");
                Label lblPrice = (Label)gvScanCard.FindControl("lblPrice");

                /*
                if (CheckDiagnosis(lblCode.Text) == true)
                {
                    lblMessage.Text = "This code already added";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                */
                IP_PTDiagnosis objDiag = new IP_PTDiagnosis();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.IPD_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.IPD_DIAG_ID = "";
                objDiag.IPD_DIAG_CODE = lblCode.Text;
                objDiag.IPD_DIAG_NAME = lblDesc.Text;
                objDiag.IPD_REMARKS = txtRemarks.Text.Trim().Replace("'", "''");
                objDiag.IPD_TEMPLATE_CODE = "";
                objDiag.IPD_PROBLEM_TYPE = "";
                objDiag.IPD_TYPE = drpDiagType.SelectedValue;
                objDiag.IPD_POSITION = Convert.ToString(intPos);
                objDiag.IPDiagnosisAdd();


                BindDiagnosis();

                drpDiagType.SelectedValue = "Secondary";
            /*
            CommonBAL objCom = new CommonBAL();
            string FieldNameWithValues = "IPD_REMARKS='" + txtRemarks.Text + "'";
            string Criteria = "IPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_DIAGNOSIS", Criteria);

            */

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.Add_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void drpSrcType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (hidPermission.Value == "9")
            {
                if (drpSrcType.SelectedIndex == 0)
                {
                    btnAddFav.Visible = true;
                    btnDeleteFav.Visible = false;
                }
                else
                {
                    btnAddFav.Visible = false;
                    btnDeleteFav.Visible = true;
                }
            }
            BindData();

        }

        protected void btnAddFav_Click(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }

                if (CheckFavorite(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.Description = Convert.ToString(ViewState["Description"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = "ICD";
                objCom.Price = Convert.ToString(ViewState["Price"]);
                objCom.FavoritesAdd();

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["gvServSelectIndex"])) == false)
                {
                    Int32 R = 0;
                    R = Convert.ToInt32(ViewState["gvServSelectIndex"]);
                    Label lblCode = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblCode");
                    Label lblDesc = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblDesc");
                    Label lblPrice = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblPrice");

                    if (R % 2 == 0)
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#ffffff");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#ffffff");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
                    }
                    else
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#f6f6f6");
                    }

                }

                ClearFavorite();
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.btnAddFav_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnDeleteFav_Click(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }
                if (drpSrcType.SelectedIndex == 0)
                {
                    goto FunEnd;
                }

                //if (CheckFavorite(Convert.ToString(ViewState["Code"])) == false)
                //{
                //    goto FunEnd;
                //}


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = "ICD";
                objCom.FavoritesDelete();

                ClearFavorite();

                BindData(); ;

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.btnDeleteFav_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteeDiag_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblDiagID = (Label)gvScanCard.Cells[0].FindControl("lblDiagID");


                IP_PTDiagnosis objDiag = new IP_PTDiagnosis();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.IPD_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.IPD_DIAG_ID = lblDiagID.Text;

                objDiag.IPDiagnosisDelete();


                BindDiagnosis();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        #endregion

        protected void gvDiagnosis_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Label lblDiagType = (Label)e.Row.FindControl("lblDiagType");
                    Button btnColor = (Button)e.Row.FindControl("btnColor");

                    if (lblDiagType.Text == "Principal")
                    {
                        btnColor.BackColor = System.Drawing.ColorTranslator.FromHtml("#02a0e0");

                    }
                    else if (lblDiagType.Text == "Secondary")
                    {
                        btnColor.BackColor = System.Drawing.ColorTranslator.FromHtml("#9ACD32");
                    }
                    else if (lblDiagType.Text == "Admitting")
                    {
                        btnColor.BackColor = System.Drawing.ColorTranslator.FromHtml("#FFFF00");
                    }
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Diagnosis.gvDiagnosis_RowDataBound");
                TextFileWriting(ex.Message.ToString());
            }
        }


        private static void EnableNextPrevNavigationForNumericPagedGrid(GridView gv)
        {
            if (gv.BottomPagerRow == null)
                return;
            Table pagerTable = (Table)gv.BottomPagerRow.Controls[0].Controls[0];

            bool prevAdded = false;
            if (gv.PageIndex != 0)
            {
                TableCell prevCell = new TableCell();
                LinkButton prevLink = new LinkButton
                {
                    Text = "<<",
                    CommandName = "Page",
                    CommandArgument = ((LinkButton)pagerTable.Rows[0].Cells[gv.PageIndex - 1].Controls[0]).CommandArgument
                };
                prevCell.Controls.Add(prevLink);
                pagerTable.Rows[0].Cells.AddAt(0, prevCell);
                prevAdded = true;
            }

            if (gv.PageIndex != gv.PageCount - 1)
            {
                TableCell nextCell = new TableCell();
                LinkButton nextLink = new LinkButton
                {
                    Text = ">>",
                    CommandName = "Page",
                    CommandArgument = ((LinkButton)pagerTable.Rows[0].Cells[gv.PageIndex +
                      (prevAdded ? 2 : 1)].Controls[0]).CommandArgument
                };
                nextCell.Controls.Add(nextLink);
                pagerTable.Rows[0].Cells.Add(nextCell);
            }
        }

        protected void gvServicess_DataBound(object sender, EventArgs e)
        {
            // EnableNextPrevNavigationForNumericPagedGrid(gvServicess);
        }


    }
}