﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IVFluid.aspx.cs" Inherits="EMR_IP.VisitDetails.IVFluid" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />



    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>

    <script language="javascript" type="text/javascript">


        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }



    </script>

    <script language="javascript" type="text/javascript">
        function refreshPanelPha1(strValue) {
            // alert(strValue)
            __doPostBack(strValue, "Pharmacy1");

        }

        function setCursorPha1(el, st, end) {
            if (el.setSelectionRange) {
                el.focus();
                el.setSelectionRange(st, end);
            } else {
                if (el.createTextRange) {
                    range = el.createTextRange();
                    range.collapse(true);
                    range.moveEnd('character', end);
                    range.moveStart('character', st);
                    range.select();
                }
            }
        }

        function ShowSearchPha1(strName) {
            var elem = strName;

            var elemLen = elem.value.length;
            //alert(elemLen);
            setCursorPha1(elem, elemLen, elemLen)
        }

        function LoadFunction() {
            HidegvPhyShedule()
        }

    </script>

<script language="javascript" type="text/javascript">

    function ShowErrorMessage(vMessage) {

        document.getElementById("divMessage").style.display = 'block';
        document.getElementById("lblMsg").innerHTML = vMessage;
        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';
            document.getElementById("lblMsg").innerHTML = '';
        }

  </script>
</head>
<body onload="LoadFunction()" >
    <form id="form1" runat="server">
        <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
        <div>
             <input type="hidden" id="hidPermission" runat="server" value="9" />
    <div style="padding-left: 60%; width: 100%;">
        <div id="divMessage" style="display: none; border: groove; height: 50px; width: 300px; background-color: brown; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right" valign="top">

                        <input type="button" id="Button1" class="ButtonStyle" style="background-color: White; color: black; border: none;" value=" X " onclick="HideErrorMessage()" />
                    </td>
                </tr>
                <tr>
                    <td>
                       <label id="lblMsg" class="label" ></label>
                    </td>
                </tr>
            </table>

        </div>
    </div>
            <table>
                <tr>


                    <td style="width: 50%; vertical-align: top;">
                        <div>
                            <table style="width: 100%">
                                <tr>
                                    <td colspan="4">
                                        <asp:UpdatePanel runat="server" ID="updatePanel3">
                                            <ContentTemplate>
                                                <asp:Label ID="lblMessage" runat="server" ForeColor="Red" CssClass="label"></asp:Label>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1">Description
                                    </td>
                                    <td>
                                        <asp:UpdatePanel runat="server" ID="updatePanel19">
                                            <ContentTemplate>

                                                <asp:TextBox ID="txtServCode" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Enabled="false"></asp:TextBox>
                                                <asp:TextBox ID="txtServName" runat="server" Width="99%" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()"></asp:TextBox>
                                                <div id="divwidth" style="visibility: hidden;"></div>
                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtServCode" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                                </asp:AutoCompleteExtender>
                                                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtServName" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                                    CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                                </asp:AutoCompleteExtender>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Duration
                                    </td>
                                    <td>
                                        <asp:UpdatePanel runat="server" ID="updatePanel6">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtDuration" runat="server" CssClass="label" Width="55px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                <span class="lblCaption1">Type</span>
                                                <asp:DropDownList ID="drpDurationType" CssClass="label" runat="server" Width="60px" BorderColor="#cccccc">
                                                    <asp:ListItem Text="" Value="8" Selected="True"></asp:ListItem>
                                                    <asp:ListItem Text="Day(s)" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="Week(s)" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="Month(s)" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="Year(s)" Value="2"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1">Quantity
                                    </td>
                                    <td>
                                        <asp:UpdatePanel runat="server" ID="updatePanel7">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtQty" runat="server" CssClass="label" Width="150px" Text="1" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </td>
                                    <td class="lblCaption1">Dosage
                                    </td>
                                    <td>
                                        <asp:UpdatePanel runat="server" ID="updatePanel8">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtDossage1" runat="server" CssClass="label" Width="150px" TextMode="MultiLine" Height="30px" Style="resize: none;"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1">Time
                                    </td>
                                    <td>
                                        <asp:UpdatePanel runat="server" ID="updatePanel9">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpDossage" CssClass="label" runat="server" Width="155px" BorderColor="#cccccc">
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Taken
                                    </td>
                                    <td>
                                        <asp:UpdatePanel runat="server" ID="updatePanel10">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpTaken" CssClass="label" runat="server" Width="155px" BorderColor="#cccccc">
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1">Route
                                    </td>
                                    <td>
                                        <asp:UpdatePanel runat="server" ID="updatePanel11">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpRoute" CssClass="label" runat="server" Width="155px" BorderColor="#cccccc">
                                                </asp:DropDownList>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">Refil
                                    </td>
                                    <td>
                                        <asp:UpdatePanel runat="server" ID="updatePanel12">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtRefil" runat="server" CssClass="label" Width="150px"></asp:TextBox>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1">Start Date
                                    </td>
                                    <td>
                                        <asp:UpdatePanel runat="server" ID="updatePanel15">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtFromDate" runat="server" Width="70px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                                                    Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                                                </asp:CalendarExtender>
                                                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                                <asp:DropDownList ID="drpFrmHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                <asp:DropDownList ID="drpFrmlMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td class="lblCaption1">End Date

                                    </td>
                                    <td>
                                        <asp:UpdatePanel runat="server" ID="updatePanel16">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtToDate" runat="server" Width="70px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                                                <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                                                    Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                                                </asp:CalendarExtender>
                                                <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>
                                                <asp:DropDownList ID="drpEndHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                <asp:DropDownList ID="drpEndMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1">Interval 
                             
                                    </td>
                                    <td colspan="3">
                                        <asp:UpdatePanel runat="server" ID="updatePanel1">
                                            <ContentTemplate>
                                                <asp:DropDownList ID="drpinterHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                <asp:DropDownList ID="drpinterMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1">Remarks 
                             
                                    </td>
                                    <td colspan="3">
                                        <asp:UpdatePanel runat="server" ID="updatePanel13">
                                            <ContentTemplate>
                                                <asp:TextBox ID="txtRemarks" runat="server" CssClass="label" TextMode="MultiLine" Width="90%" Height="30px" Style="resize: none;"></asp:TextBox>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="lblCaption1"></td>
                                    <td colspan="3">
                                        <asp:UpdatePanel runat="server" ID="updatePanel14">
                                            <ContentTemplate>
                                                <input type="checkbox" name="chkStatus" id="chkStatus" runat="server" value="0" checked="checked" /><span class="label">Active</span>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <asp:UpdatePanel runat="server" ID="updatePanel4">
                                            <ContentTemplate>
                                                <asp:Button ID="btnAddPhar" runat="server" CssClass="orange" Width="50px" Text="Add" OnClick="btnAddPhar_Click" Style="width: 50px; border-radius: 5px 5px 5px 5px" />
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>


                            </table>
                            <div style="padding-left: 40%; width: 100%;">
                                <div id="divPhyScedule" style="display: none; border: groove; height: 400px; width: 500px; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;left:400px;top:200px;">

                                    <table cellpadding="0" cellspacing="0" width="100%" >
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <asp:UpdatePanel ID="updatePanel22" runat="server">
                                                    <ContentTemplate>
                                                        <asp:Label ID="lblSheduleMsg" runat="server" CssClass="lblCaption"  Visible="false"></asp:Label>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </td>
                                            <td align="right" style="vertical-align: top;">

                                                <input type="button" id="btnMsgClose" class="Button"  style="color: black; border: none;" value=" X " onclick="HidePhyScedule()" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                   <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
                                                        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Schedule" Width="100%">
                                                            <ContentTemplate>
                                              
                                                    <table width="100%">

                                                        <tr>
                                                            <td>
                                                                <fieldset>
                                                                    <legend class="lblCaption1" ">Pharmacy Schedule</legend>
                                                                    <table style="width: 100%;">
                                                                        <tr>
                                                                              <td class="lblCaption1" style="width: 100px;vertical-align:top;" >Description 
                                                                            </td>
                                                                            <td>
                                                                                 <asp:UpdatePanel ID="updatePanel24" runat="server">
                                                                                    <ContentTemplate>
                                                                                <asp:Label ID="lblSheMedicinCode" runat="server"  CssClass="label" ForeColor="Black"  ></asp:Label><br />
                                                                                <asp:Label ID="lblSheMedicinDesc" runat="server"  CssClass="label" ForeColor="Black" ></asp:Label>
                                                                                        </ContentTemplate>
                                                                                     </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>

                                                                            <td class="lblCaption1" style="width: 100px; ">Name  
                                                                            </td>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="updatePanel20" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:DropDownList ID="drpWardNurse" runat="server" CssClass="label" Width="205px"></asp:DropDownList>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="lblCaption1" style="width: 150px; ">Password
                                                                            </td>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="updatePanel21" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtWardNursePass" runat="server" CssClass="label" TextMode="Password" Width="200px"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="lblCaption1" ">Start Time
                                                                            </td>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="updatePanel2" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtSheduleDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                                                        <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                                                                            Enabled="True" TargetControlID="txtSheduleDate" Format="dd/MM/yyyy">
                                                                                        </asp:CalendarExtender>
                                                                                        <asp:DropDownList ID="drpSheduleHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                                                        <asp:DropDownList ID="drpSheduleMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                             <td class="lblCaption1" ">End Time
                                                                            </td>
                                                                            <td>
                                                                                 <asp:UpdatePanel ID="updatePanel27" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtSheduleEndDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                                                        <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                                                                            Enabled="True" TargetControlID="txtSheduleEndDate" Format="dd/MM/yyyy">
                                                                                        </asp:CalendarExtender>
                                                                                        <asp:DropDownList ID="drpSheduleEndHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                                                        <asp:DropDownList ID="drpSheduleEndMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                                                            </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="lblCaption1" >Remarks
                                                                            </td>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="updatePanel17" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:TextBox ID="txtSheduleComment" runat="server" Width="300px" Height="50px" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>

                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td></td>
                                                                            <td>
                                                                                <asp:UpdatePanel ID="updatePanel18" runat="server">
                                                                                    <ContentTemplate>
                                                                                        <asp:Button ID="btnPhySheduleAdd" runat="server" CssClass="orange" Text="Add" Width="50px" Style="width: 50px; border-radius: 5px 5px 5px 5px" OnClick="btnPhySheduleAdd_Click" />
                                                                                    </ContentTemplate>
                                                                                </asp:UpdatePanel>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </fieldset>
                                                            </td>
                                                        </tr>
                                                    </table>

                                                
                                                              </ContentTemplate>
                                                    </asp:TabPanel>
                                                    <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="History" Width="100%">
                                                        <ContentTemplate>
                         
                                                    <asp:UpdatePanel runat="server" ID="updatePanel23">
                                                        <ContentTemplate>
                                                            <asp:GridView ID="gvPhyShedule" runat="server" AllowSorting="True" AutoGenerateColumns="False" 
                                                                EnableModelValidation="True" GridLines="None" Width="100%">
                                                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                                                <RowStyle CssClass="GridRow" />

                                                                <Columns>

                                                                    <asp:TemplateField HeaderText="Start Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px" >
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPhySheDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPIS_DATEDesc") %>'></asp:Label>
                                                                            <asp:Label ID="lblPhySheTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPIS_DATETime") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="End Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="100px" >
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblPhySheEndDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPIS_ENDTIMEDesc") %>'></asp:Label>
                                                                            <asp:Label ID="lblPhySheEndTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPIS_ENDTIMETime") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="Given By" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="lblPhySheGivenBy" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPIS_GIVENBY") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Remarks" HeaderStyle-HorizontalAlign="Left">
                                                                        <ItemTemplate>

                                                                            <asp:Label ID="lblPhySheRemarks" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPIS_REMARKS") %>'></asp:Label>

                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>

                                                          </ContentTemplate>
                                                </asp:TabPanel>
                                              </asp:TabContainer>
                                            </td>
                                        </tr>
                                        
                                    </table>

                                </div>
                            </div>
                            <table style="width: 100%">
                                <tr>
                                    <td colspan="4">
                                        <div style="padding-top: 0px; width: 100%; height: 330px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                            <asp:UpdatePanel runat="server" ID="updatePanel5">
                                                <ContentTemplate>
                                                    <asp:GridView ID="gvPharmacy" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                                        EnableModelValidation="True" GridLines="None" Width="100%" OnRowDataBound="gvPharmacy_RowDataBound">
                                                        <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                                        <RowStyle CssClass="GridRow" />


                                                        <Columns>
                                                            <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="18" Width="18"
                                                                        OnClick="DeleteeDiag_Click" />&nbsp;&nbsp;
                                                
                                                                </ItemTemplate>

                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkPhyDesc" runat="server" OnClick="PhySelect_Click">

                                                                        <asp:Label ID="lblPhyStDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_START_DATEDesc") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lblPhyStTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_START_TIMEDesc") %>' Visible="false"></asp:Label>


                                                                        <asp:Label ID="lblPhyEndDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_END_DATEDesc") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lblPhyEndTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_END_TIMEDesc") %>' Visible="false"></asp:Label>

                                                                        <asp:Label ID="lblInterval" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_INTERVAL") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lblPhyRemarks" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_REMARKS") %>' Visible="false"></asp:Label>
                                                                        <asp:Label ID="lblPhyTaken" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_TAKEN") %>' Visible="false"></asp:Label>

                                                                        <asp:Label ID="lblStatus" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_STATUS") %>' Visible="false"></asp:Label>

                                                                        <asp:Label ID="lblPhyID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_IV_ID") %>' Visible="false"></asp:Label>

                                                                        <asp:Label ID="lblPhyCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_IV_CODE") %>'></asp:Label>
                                                                        <br />
                                                                        <asp:Label ID="lblPhyDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_IV_NAME") %>'></asp:Label>
                                                                    </asp:LinkButton>

                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Qty" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkPhyQTY" runat="server" OnClick="PhySelect_Click">
                                                                        <asp:Label ID="lblPhyQTY" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_QTY") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="Dosage" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkPhyDosage" runat="server" OnClick="PhySelect_Click">
                                                                        <asp:Label ID="lblPhyDossage1" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_DOSAGE1") %>'></asp:Label>
                                                                        <asp:Label ID="lblPhyDosage" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_DOSAGE") %>' Visible="false"></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Route" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkPhyRoute" runat="server" OnClick="PhySelect_Click">
                                                                        <asp:Label ID="lblPhyRoute" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_ROUTE") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Refil" HeaderStyle-HorizontalAlign="Left" Visible="false">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkPhyRefil" runat="server" OnClick="PhySelect_Click">
                                                                        <asp:Label ID="lblPhyRefil" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_REFILL") %>'></asp:Label>
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Duration" HeaderStyle-HorizontalAlign="Left">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkPhyDuration" runat="server" OnClick="PhySelect_Click">
                                                                        <asp:Label ID="lblPhyDuration" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_DURATION") %>'></asp:Label>&nbsp;
                                                    <asp:Label ID="lblPhyDurationTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_DURATION_TYPEDesc") %>'></asp:Label>
                                                                        <asp:Label ID="lblPhyDurationType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPI_DURATION_TYPE") %>' Visible="false"></asp:Label>

                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkTask" runat="server" OnClick="PhyTask_Click" Text="Schedule">
                                                                                                                                               
                                                   
                                                                    </asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>

                                                        </Columns>
                                                    </asp:GridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </td>

                                </tr>
                            </table>
                        </div>

                    </td>
                </tr>
            </table>


        </div>
    </form>
</body>
<style type="text/css">
    .style1
    {
        width: 20px;
        height: 32px;
    }

    .modalBackground
    {
        background-color: black;
        filter: alpha(opacity=40);
        opacity: 0.5;
    }

    .modalPopup
    {
        background-color: #ffffff;
        border-width: 3px;
        border-style: solid;
        border-color: Gray;
        padding: 3px;
        width: 250px;
        top: -1000px;
        position: absolute;
        padding: 0px10px10px10px;
    }
</style>

<script language="javascript" type="text/javascript">



    function ServNameSelected() {
        if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
            var Data = document.getElementById('<%=txtServName.ClientID%>').value;
            var Data1 = Data.split('~');

            if (Data1.length > 1) {
                document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
            }
        }

        return true;
    }

    function ShowPhyScedule() {

        document.getElementById("divPhyScedule").style.display = 'block';
        HidegvPhyShedule();

    }

    function HidePhyScedule() {

        document.getElementById("divPhyScedule").style.display = 'none';

    }

    function ShowgvPhyShedule() {

        document.getElementById("divgvPhyShedule").style.display = 'block';
        document.getElementById("divSrcPhyShedule").style.display = 'none';

        document.getElementById("btnShowShedule").style.display = 'none';
        document.getElementById("btnHideShedule").style.display = 'block';
    }

    function HidegvPhyShedule() {

        document.getElementById("divgvPhyShedule").style.display = 'none';
        document.getElementById("divSrcPhyShedule").style.display = 'block';

        document.getElementById("btnShowShedule").style.display = 'block';
        document.getElementById("btnHideShedule").style.display = 'none';

    }


</script>

</html>
