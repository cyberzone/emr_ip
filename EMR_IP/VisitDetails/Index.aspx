﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="EMR_IP.VisitDetails.Index" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

 

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <link href="../Styles/MenuStyle.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Visit Details </h3>
        </div>
    </div>

    <br />
    <asp:UpdatePanel runat="server" ID="updatePanel5">
        <ContentTemplate>
            <asp:Button ID="btnDiagnosis" runat="server" Text="Diagnosis" CssClass="TabButtonBlue" OnClick="btnDiagnosis_Click" />

            <asp:Button ID="btnProcedure" runat="server" Text="Procedure" CssClass="TabButtonBlue" OnClick="btnProcedure_Click" />
            <asp:Button ID="btnRadiology" runat="server" Text="Radiology" CssClass="TabButtonBlue" OnClick="btnRadiology_Click" />


            <asp:Button ID="btnLaboratory" runat="server" Text="Laboratory" CssClass="TabButtonBlue" OnClick="btnLaboratory_Click" />
            <asp:Button ID="btnPharmacy" runat="server" Text="Pharmacy" CssClass="TabButtonBlue" OnClick="btnPharmacy_Click" />
 <asp:Button ID="btnIVFluid" runat="server" Text="IV Fluid " CssClass="TabButtonBlue" OnClick="btnIVFluid_Click" />

            
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />

    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <iframe id="frmVisit" runat="server" style="width: 100%; height: 700px"></iframe>

        </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>
