﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;

namespace EMR_IP.VisitDetails
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            if (!IsPostBack)
            {
                btnDiagnosis.CssClass = "TabButtonWhite";
                frmVisit.Src = "~/VisitDetails/Diagnosis.aspx";
            }

        }

        protected void btnDiagnosis_Click(object sender, EventArgs e)
        {
            btnDiagnosis.CssClass = "TabButtonBlue";
            btnProcedure.CssClass = "TabButtonBlue";
            btnRadiology.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonBlue";
            btnPharmacy.CssClass = "TabButtonBlue";
            btnIVFluid.CssClass = "TabButtonBlue";

            btnDiagnosis.CssClass = "TabButtonWhite";
            frmVisit.Src = "~/VisitDetails/Diagnosis.aspx";
        }

        protected void btnProcedure_Click(object sender, EventArgs e)
        {
            btnDiagnosis.CssClass = "TabButtonBlue";
            btnProcedure.CssClass = "TabButtonBlue";
            btnRadiology.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonBlue";
            btnPharmacy.CssClass = "TabButtonBlue";
            btnIVFluid.CssClass = "TabButtonBlue";

            btnProcedure.CssClass = "TabButtonWhite";
            frmVisit.Src = "~/VisitDetails/Procedures.aspx";
        }

        protected void btnRadiology_Click(object sender, EventArgs e)
        {
            btnDiagnosis.CssClass = "TabButtonBlue";
            btnProcedure.CssClass = "TabButtonBlue";
            btnRadiology.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonBlue";
            btnPharmacy.CssClass = "TabButtonBlue";
            btnIVFluid.CssClass = "TabButtonBlue";

            btnRadiology.CssClass = "TabButtonWhite";
            frmVisit.Src = "~/VisitDetails/Radiology.aspx";

        }

        protected void btnLaboratory_Click(object sender, EventArgs e)
        {
            btnDiagnosis.CssClass = "TabButtonBlue";
            btnProcedure.CssClass = "TabButtonBlue";
            btnRadiology.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonBlue";
            btnPharmacy.CssClass = "TabButtonBlue";
            btnIVFluid.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonWhite";
            frmVisit.Src = "~/VisitDetails/Laboratory.aspx";
        }

        protected void btnPharmacy_Click(object sender, EventArgs e)
        {
            btnDiagnosis.CssClass = "TabButtonBlue";
            btnProcedure.CssClass = "TabButtonBlue";
            btnRadiology.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonBlue";
            btnPharmacy.CssClass = "TabButtonBlue";
            btnIVFluid.CssClass = "TabButtonBlue";


            btnPharmacy.CssClass = "TabButtonWhite";
            // frmVisit.Src = "~/VisitDetails/PharmacyStandard.aspx";
            if (GlobalValues.FileDescription.ToUpper() == "ADVCARE" || GlobalValues.FileDescription.ToUpper() == "ALNOOR")
            {
                frmVisit.Src = "~/VisitDetails/PharmacyForDHA.aspx";
            }
            else
            {
                frmVisit.Src = "~/VisitDetails/PharmacyStandard.aspx";
            }


        }


        protected void btnIVFluid_Click(object sender, EventArgs e)
        {
            btnDiagnosis.CssClass = "TabButtonBlue";
            btnProcedure.CssClass = "TabButtonBlue";
            btnRadiology.CssClass = "TabButtonBlue";

            btnLaboratory.CssClass = "TabButtonBlue";
            btnPharmacy.CssClass = "TabButtonBlue";
            btnPharmacy.CssClass = "TabButtonBlue";

            btnIVFluid.CssClass = "TabButtonWhite";


            frmVisit.Src = "~/VisitDetails/IVFluid.aspx";
        }

    }
}