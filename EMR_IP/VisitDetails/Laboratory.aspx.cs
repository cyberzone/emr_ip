﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;

namespace EMR_IP.VisitDetails
{
    public partial class Laboratory1 : System.Web.UI.Page
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_LAB' AND SEGMENT='LAB_SEARCH_LIST'";

            DataSet DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("CUST_VALUE") == false)
                {
                    ViewState["CUST_VALUE"] = DS.Tables[0].Rows[0]["CUST_VALUE"].ToString();
                }
                else
                {
                    ViewState["CUST_VALUE"] = "0";
                }
            }


        }

        void BindData()
        {

            string SearchFilter = txtSearch.Text.Trim();
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();
            if (drpSrcType.SelectedIndex == 0)
            {
                ds = dbo.HaadServicessListGet("Laboratory", SearchFilter, "", Convert.ToString(Session["User_DeptID"]));
            }
            else
            {
                string Criteria = " 1=1 ";

                if (txtSearch.Text.Trim() != "")
                {
                    Criteria += " AND( EDF_CODE like'%" + txtSearch.Text.Trim() + "%' OR  EDF_NAME  like'%" + txtSearch.Text.Trim() + "%')";
                }
                Criteria += " AND EDF_TYPE='LAB' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "' )";

                ds = dbo.FavoritesGet(Criteria);
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvServicess.DataSource = ds;
                gvServicess.DataBind();

                string strPermission = hidPermission.Value;
                if (strPermission == "1" || strPermission == "5")
                {
                    gvServicess.Columns[0].Visible = false;
                }
            }
            else
            {
                gvServicess.DataBind();
            }
        }

        Boolean CheckFavorite(string Code)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string Criteria = " 1=1 ";

            Criteria += " AND EDF_CODE='" + Code + "'";

            Criteria += " AND EDF_TYPE='LAB' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "')";

            ds = dbo.FavoritesGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void BindLaboratory()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND IPL_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPL_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            IP_PTLaboratory objPro = new IP_PTLaboratory();
            DS = objPro.IPLaboratoryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvLaboratory.DataSource = DS;
                gvLaboratory.DataBind();

                string strPermission = hidPermission.Value;
                if (strPermission == "1" || strPermission == "5")
                {
                    gvLaboratory.Columns[0].Visible = false;
                }
            }
            else
            {
                gvLaboratory.DataBind();
            }

            txtRemarks.Text = "";
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                if (Convert.ToString(DR["IPL_REMARKS"]) != "")
                    txtRemarks.Text = Convert.ToString(DR["IPL_REMARKS"]);
            }
        FunEnd: ;
        }

        Boolean CheckLaboratory(string Code)
        {
            string Criteria = " 1=1 ";

            Criteria += " AND IPL_LAB_CODE='" + Code + "'";
            Criteria += " AND IPL_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPL_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";




            DataSet DS = new DataSet();
            IP_PTLaboratory objPro = new IP_PTLaboratory();
            DS = objPro.IPLaboratoryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        void ClearFavorite()
        {

            ViewState["Code"] = "";
            ViewState["Description"] = "";
            ViewState["Price"] = "";
            ViewState["gvServSelectIndex"] = "";
        }


        void ClearProc()
        {
            hidProcCode.Value = "";
            hidProcCode.Value = "";
            txtRemarks.Text = "";
            txtQty.Text = "";

        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_LAB' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnAddFav.Visible = false;
                btnDeleteFav.Visible = false;
                // btnClear.Visible = false;



            }

            if (strPermission == "5")
            {
                btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnAddFav.Visible = false;


            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = "";
            string parameter = Request["__EVENTARGUMENT"];
            if (parameter == "Laboratory")
            {
                BindData();
                txtSearch.Focus();

            }

            if (!IsPostBack)
            {

                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }


                try
                {
                    BindScreenCustomization();
                    if (hidPermission.Value == "9")
                    {
                        if (Convert.ToString(ViewState["CUST_VALUE"]) == "1")
                        {
                            drpSrcType.SelectedIndex = 1;
                            btnDeleteFav.Visible = true;
                            btnAddFav.Visible = false;
                        }
                        else
                        {
                            drpSrcType.SelectedIndex = 0;
                            btnAddFav.Visible = true;
                            btnDeleteFav.Visible = false;
                        }
                    }
                    BindData();
                    BindLaboratory();

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Laboratory.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Laboratory.txtSearch_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvServicess_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvServicess.PageIndex = e.NewPageIndex;
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Laboratory.gvServicess_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void Edit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
                gvServicess.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.Cells[0].FindControl("lblDesc");
                Label lblPrice = (Label)gvScanCard.Cells[0].FindControl("lblPrice");

                ViewState["Code"] = lblCode.Text;
                ViewState["Description"] = lblDesc.Text;
                ViewState["Price"] = lblPrice.Text;
                lblCode.BackColor = System.Drawing.Color.FromName("#c5e26d");
                lblDesc.BackColor = System.Drawing.Color.FromName("#c5e26d");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Laboratory.Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }

        protected void drpSrcType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (hidPermission.Value == "9")
            {
                if (drpSrcType.SelectedIndex == 0)
                {
                    btnAddFav.Visible = true;
                    btnDeleteFav.Visible = false;
                }
                else
                {
                    btnAddFav.Visible = false;
                    btnDeleteFav.Visible = true;
                }
            }
            BindData();

        }

        protected void btnAddFav_Click(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }

                if (CheckFavorite(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.Description = Convert.ToString(ViewState["Description"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = "LAB";
                objCom.Price = Convert.ToString(ViewState["Price"]);
                objCom.FavoritesAdd();

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["gvServSelectIndex"])) == false)
                {
                    Int32 R = 0;
                    R = Convert.ToInt32(ViewState["gvServSelectIndex"]);
                    Label lblCode = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblCode");
                    Label lblDesc = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblDesc");
                    Label lblPrice = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblPrice");

                    if (R % 2 == 0)
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#ffffff");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#ffffff");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
                    }
                    else
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#f6f6f6");
                    }

                }

                ClearFavorite();
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Laboratory.btnAddFav_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnDeleteFav_Click(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }
                if (drpSrcType.SelectedIndex == 0)
                {
                    goto FunEnd;
                }

                //if (CheckFavorite(Convert.ToString(ViewState["Code"])) == false)
                //{
                //    goto FunEnd;
                //}


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = "LAB";
                objCom.FavoritesDelete();

                ClearFavorite();

                BindData(); ;

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Laboratory.btnDeleteFav_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void Add_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 intPos = 1;
                if (gvLaboratory.Rows.Count >= 1)
                {
                    intPos = Convert.ToInt32(gvLaboratory.Rows.Count) + 1;

                }

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.Cells[0].FindControl("lblDesc");
                Label lblPrice = (Label)gvScanCard.Cells[0].FindControl("lblPrice");

                IP_PTMaster objPTM = new IP_PTMaster();
                string SO_TRANS_ID = "";

                try
                {

                    objPTM.IPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPTM.IPM_ID = Convert.ToString(Session["EMR_ID"]);
                    objPTM.IPM_DR_CODE = Convert.ToString(Session["IAS_DR_ID"]);
                    objPTM.DR_Name = Convert.ToString(Session["IAS_DR_NAME"]);
                    objPTM.SO_ID = Convert.ToString(Session["SO_ID"]);

                    objPTM.ServCode = lblCode.Text;
                    objPTM.ServDesc = lblDesc.Text;
                    objPTM.ServQty = txtQty.Text.Trim();
                    objPTM.ServType = "LAB";
                    objPTM.IPSalesOrderTransAdd(out SO_TRANS_ID);


                }
                catch (Exception ex)
                {
                    goto FunEnd;
                }


                IP_PTLaboratory objDiag = new IP_PTLaboratory();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.IPL_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.IPL_LAB_ID = "";
                objDiag.IPL_LAB_CODE = lblCode.Text;
                objDiag.IPL_LAB_NAME = lblDesc.Text;
                objDiag.IPL_COST = lblPrice.Text;
                objDiag.IPL_REMARKS = txtRemarks.Text.Trim().Replace("'", "''");
                objDiag.IPL_QTY = txtQty.Text.Trim();
                objDiag.IPL_TEMPLATE_CODE = "";
                objDiag.IPL_SO_TRANS_ID = SO_TRANS_ID;
                objDiag.IPLaboratoryAdd();


              
                /*
                CommonBAL objCom = new CommonBAL();
                string FieldNameWithValues = "IPL_REMARKS='" + txtRemarks.Text + "'";
                string Criteria = "IPL_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPL_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_LABORATORY", Criteria);
                */
                BindLaboratory();

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Laboratory.Add_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }

        protected void DeleteeDiag_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblDiagCode = (Label)gvScanCard.Cells[0].FindControl("lblDiagCode");
                Label lblLabID = (Label)gvScanCard.Cells[0].FindControl("lblLabID");
                Label lblSOTransID = (Label)gvScanCard.Cells[0].FindControl("lblSOTransID");

                IP_PTLaboratory objDiag = new IP_PTLaboratory();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.IPL_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.IPL_LAB_ID = lblLabID.Text;
                objDiag.IPL_SO_TRANS_ID = lblSOTransID.Text;

                objDiag.IPLaboratoryDelete();


                BindLaboratory();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Laboratory.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        #endregion
 
    }
}