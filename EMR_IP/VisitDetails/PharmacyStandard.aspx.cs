﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;

namespace EMR_IP.VisitDetails
{
    public partial class PharmacyStandard : System.Web.UI.Page
    {
        static string strSessionDeptId, strUserCode;

        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_PHY' AND SEGMENT='PHY_SEARCH_LIST'";

            DataSet DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("CUST_VALUE") == false)
                {
                    ViewState["CUST_VALUE"] = DS.Tables[0].Rows[0]["CUST_VALUE"].ToString();
                }
                else
                {
                    ViewState["CUST_VALUE"] = "0";
                }
            }


        }

        void BindTime()
        {

            CommonBAL objCom = new CommonBAL();
            DataSet DS = new DataSet();

            DS = objCom.HoursGet();
            if (DS.Tables[0].Rows.Count > 0)
            {
 



                drpFrmHour.DataSource = DS;
                drpFrmHour.DataTextField = "Name";
                drpFrmHour.DataValueField = "Code";
                drpFrmHour.DataBind();

                drpEndHour.DataSource = DS;
                drpEndHour.DataTextField = "Name";
                drpEndHour.DataValueField = "Code";
                drpEndHour.DataBind();

                drpinterHour.DataSource = DS;
                drpinterHour.DataTextField = "Name";
                drpinterHour.DataValueField = "Code";
                drpinterHour.DataBind();

                drpSheduleHour.DataSource = DS;
                drpSheduleHour.DataTextField = "Name";
                drpSheduleHour.DataValueField = "Code";
                drpSheduleHour.DataBind();
            }

            drpFrmHour.Items.Insert(0, "00");
            drpFrmHour.Items[0].Value = "00";

            drpEndHour.Items.Insert(0, "00");
            drpEndHour.Items[0].Value = "00";

            drpinterHour.Items.Insert(0, "00");
            drpinterHour.Items[0].Value = "00";

            drpSheduleHour.Items.Insert(0, "00");
            drpSheduleHour.Items[0].Value = "00";


            DS = new DataSet();

            DS = objCom.MinutesGet(Convert.ToString(Session["HMS_TIME_INTERVAL"]));
            if (DS.Tables[0].Rows.Count > 0)
            {

                drpFrmlMin.DataSource = DS;
                drpFrmlMin.DataTextField = "Name";
                drpFrmlMin.DataValueField = "Code";
                drpFrmlMin.DataBind();

                drpEndMin.DataSource = DS;
                drpEndMin.DataTextField = "Name";
                drpEndMin.DataValueField = "Code";
                drpEndMin.DataBind();

                drpinterMin.DataSource = DS;
                drpinterMin.DataTextField = "Name";
                drpinterMin.DataValueField = "Code";
                drpinterMin.DataBind();

                drpSheduleMin.DataSource = DS;
                drpSheduleMin.DataTextField = "Name";
                drpSheduleMin.DataValueField = "Code";
                drpSheduleMin.DataBind();
            }

            drpFrmlMin.Items.Insert(0, "00");
            drpFrmlMin.Items[0].Value = "00";

            drpEndMin.Items.Insert(0, "00");
            drpEndMin.Items[0].Value = "00";

            drpinterMin.Items.Insert(0, "00");
            drpinterMin.Items[0].Value = "00";

            drpSheduleMin.Items.Insert(0, "00");
            drpSheduleMin.Items[0].Value = "00";
             

        }

        void BindPharmacy()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND IPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            IP_PTPharmacy objPro = new IP_PTPharmacy();

            // if (drpTemplate.SelectedIndex == 0)
            // {
            //    DS = objPro.IPPharmacyGet(Criteria);
            //}
            //else
            //{
            //    Criteria = " 1=1 ";
            //    Criteria += " AND IPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            //    Criteria += " AND ET_CODE='" + drpTemplate.SelectedValue + "'";

            //    DS = objPro.PharmacyTemplatGe(Criteria);
            //}

            DS = objPro.IPPharmacyGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPharmacy.DataSource = DS;
                gvPharmacy.DataBind();

                string strPermission = hidPermission.Value;
                if (strPermission == "1" || strPermission == "7")
                {
                    gvPharmacy.Columns[0].Visible = false;
                }
            }
            else
            {
                gvPharmacy.DataBind();
            }
        FunEnd: ;
        }



        void BindDossage()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND EPT_ACTIVE=1 ";
            Criteria += " AND EPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.EMRTimeGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpDossage.DataSource = DS;
                drpDossage.DataTextField = "EPT_NAME";
                drpDossage.DataValueField = "EPT_NAME";
                drpDossage.DataBind();
            }
            drpDossage.Items.Insert(0, "Select When");
            drpDossage.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindTaken()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND EPT_ACTIVE=1 ";
            Criteria += " AND EPT_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.EMRTakenGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpTaken.DataSource = DS;
                drpTaken.DataTextField = "EPT_NAME";
                drpTaken.DataValueField = "EPT_CODE";
                drpTaken.DataBind();
            }
            drpTaken.Items.Insert(0, "Select How");
            drpTaken.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }

        void BindRoute()
        {
            DataSet DS = new DataSet();
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 AND EPR_ACTIVE=1 ";
            Criteria += " AND EPR_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' ";
            DS = objCom.EMRRouteGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                drpRoute.DataSource = DS;
                drpRoute.DataTextField = "EPR_NAME";
                drpRoute.DataValueField = "EPR_CODE";
                drpRoute.DataBind();
            }
            drpRoute.Items.Insert(0, "Select Route");
            drpRoute.Items[0].Value = "";

            // objCom.BindMasterDropdown(Criteria, out drpDossage);

        }


        void BindUsers()
        {
            string Criteria = " 1=1 AND HUM_STATUS='A' ";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND ( HUM_REMARK IN (SELECT HSFM_STAFF_ID FROM HMS_STAFF_MASTER WHERE HSFM_CATEGORY IN ('Doctors','Nurse') OR HUM_USER_ID ='ADMIN')) ";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                drpWardNurse.DataSource = ds;
                drpWardNurse.DataValueField = "HUM_USER_ID";
                drpWardNurse.DataTextField = "HUM_USER_NAME";
                drpWardNurse.DataBind();

            }
            drpWardNurse.Items.Insert(0, "--- Select ---");
            drpWardNurse.Items[0].Value = "";


        }







        void BindPharmacyShedul()
        {

            string Criteria = " 1=1 ";
            Criteria += " AND IPPS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND IPPS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria += " AND IPPS_PHY_ID=" + Convert.ToString(ViewState["TaskPhyID"]);

            DataSet DS = new DataSet();
            IP_PTPharmacy objPro = new IP_PTPharmacy();
            DS = objPro.IPPharmacySheduleGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPhyShedule.DataSource = DS;
                gvPhyShedule.DataBind();

            }
            else
            {
                gvPhyShedule.DataBind();
            }

        }

        Boolean CheckPassword(string UserID, string Password)
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            //Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_REMARK ='" + UserID + "' AND   HUM_USER_PASS= '" + Password + "'";
            Criteria += " AND HUM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND HUM_USER_ID='" + UserID + "' AND   HUM_USER_PASS= '" + Password + "'";
            DataSet ds = new DataSet();
            //ds = dbo.UserMasterGet(drpBranch.SelectedValue, drpUsers.SelectedValue , txtPassword.Text);
            ds = objCom.UserMasterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void ClearPhar()
        {


            Int32 R = 0;
            if (Convert.ToString(ViewState["PhySelectIndex"]) != "" && Convert.ToString(ViewState["PhySelectIndex"]) != null)
            {
                R = Convert.ToInt32(ViewState["PhySelectIndex"]);
                gvPharmacy.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
            }

            ViewState["IPP_PHY_ID"] = "";
            ViewState["PhySelectIndex"] = "";

            txtServCode.Text = "";
            txtServName.Text = "";
            txtDuration.Text = "";

            if (drpDurationType.Items.Count > 0)
                drpDurationType.SelectedIndex = 0;
            // txtQty.Text = "";
            if (drpDossage.Items.Count > 0)
                drpDossage.SelectedIndex = 0;

            if (drpTaken.Items.Count > 0)
                drpTaken.SelectedIndex = 0;

            if (drpRoute.Items.Count > 0)
                drpRoute.SelectedIndex = 0;

            txtQty.Text = "";
            txtDossage1.Text = "";

            txtRefil.Text = "";
            //  txtFromDate.Text = "";
            // txtToDate.Text = "";

            drpFrmHour.SelectedIndex = 0;
            drpFrmlMin.SelectedIndex = 0;

            drpEndHour.SelectedIndex = 0;
            drpEndMin.SelectedIndex = 0;

            drpinterHour.SelectedIndex = 0;
            drpinterMin.SelectedIndex = 0;

            chkStop.Checked = false;
            txtStopReasion.Text = "";

            chkStatus.Checked = true;
            txtRemarks.Text = "";



        }

        void ClearPhyShedule()
        {
            //  ViewState["TaskPhyID"] = "";
            // txtSheduleDate.Text =
            drpSheduleHour.SelectedIndex = 0;
            drpSheduleMin.SelectedIndex = 0;
            drpWardNurse.SelectedIndex = 0;
            txtSheduleComment.Text = "";
            lblSheduleMsg.Text = "";
            lblSheduleMsg.Visible = false;
        }

        Boolean CheckSchedule(string strTaskID)
        {

            string Criteria = " 1=1 ";
            Criteria += " AND IPPS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += " AND IPPS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            Criteria += " AND IPPS_PHY_ID=" + strTaskID;

            DataSet DS = new DataSet();
            IP_PTPharmacy objPro = new IP_PTPharmacy();
            DS = objPro.IPPharmacySheduleGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_PHY' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnAddPhar.Visible = false;
               // btnDeleteFav.Visible = false;
                // btnClear.Visible = false;

              

            }

            if (strPermission == "5")
            {
                //btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnAddPhar.Visible = false;
              //  btnAddFav.Visible = false;
                //btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }
        #endregion


        #region AutoExt
        [System.Web.Services.WebMethod]
        public static string[] GetServicessList(string prefixText)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string[] Data;

            ds = dbo.HaadServicessListGet("Pharmacy", prefixText, "", strSessionDeptId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                Data = new string[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    //  Data[i] = ds.Tables[0].Rows[i]["Code"].ToString() + "~" + ds.Tables[0].Rows[i]["ProductName"].ToString() + '~' + ds.Tables[0].Rows[i]["Description"].ToString();
                    Data[i] = Convert.ToString(ds.Tables[0].Rows[i]["Code"]).Trim() + "~" + Convert.ToString(ds.Tables[0].Rows[i]["Description"]).Trim();


                }

                return Data;
            }
            string[] Data1 = { "" };

            return Data1;
        }
        #endregion


        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {

                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }

                try
                {
                    BindScreenCustomization();
                    BindTime();
                    strSessionDeptId = Convert.ToString(Session["User_DeptID"]);
                    strUserCode = Convert.ToString(Session["User_Code"]);

                    DateTime strFromDate = Convert.ToDateTime(System.DateTime.Now.ToShortDateString());
                    txtFromDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    txtToDate.Text = strFromDate.ToString("dd/MM/yyyy");
                    // txtToDate.Text = strFromDate.AddDays(3).ToString("dd/MM/yyyy");

                    txtSheduleDate.Text = strFromDate.ToString("dd/MM/yyyy");

                    BindDossage();
                    BindTaken();
                    BindRoute();
                    BindUsers();

                    BindPharmacy();
                    ViewState["IPP_PHY_ID"] = "";
                    ViewState["TaskPhyID"] = "";

                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }


        protected void btnAddPhar_Click(object sender, EventArgs e)
        {
            try
            {
                IP_PTPharmacy objDiag = new IP_PTPharmacy();

                if (txtServCode.Text == "" || txtServCode.Text == null)
                {
                    lblMessage.Text = "Please select the Code";
                    lblMessage.ForeColor = System.Drawing.Color.Red;
                    goto FunEnd;
                }
                Int32 intPos = 1;
                if (gvPharmacy.Rows.Count >= 1)
                {
                    intPos = Convert.ToInt32(gvPharmacy.Rows.Count) + 1;

                }



                objDiag.BranchID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EMRID = Convert.ToString(Session["EMR_ID"]);
                objDiag.PTID = Convert.ToString(Session["EMR_PT_ID"]);



                objDiag.IPP_PHY_ID = Convert.ToString(ViewState["IPP_PHY_ID"]);

                objDiag.IPP_PHY_CODE = txtServCode.Text;
                objDiag.IPP_PHY_NAME = txtServName.Text.Replace("'", "''");

                objDiag.IPP_DOSAGE = drpDossage.SelectedValue;
                objDiag.IPP_ROUTE = drpRoute.SelectedValue;
                objDiag.IPP_REFILL = txtRefil.Text.Trim();

                objDiag.IPP_DURATION = txtDuration.Text;
                objDiag.IPP_DURATION_TYPE = drpDurationType.SelectedValue;
                objDiag.IPP_START_DATE = txtFromDate.Text + " " + drpFrmHour.SelectedValue + ":" + drpFrmlMin.SelectedValue + ":00";
                objDiag.IPP_END_DATE = txtToDate.Text + " " + drpEndHour.SelectedValue + ":" + drpEndMin.SelectedValue + ":00";
                objDiag.IPP_INTERVAL = drpinterHour.SelectedValue + ":" + drpinterMin.SelectedValue;

                objDiag.IPP_REMARKS = txtRemarks.Text.Replace("'", "''");

                objDiag.IPP_QTY = txtQty.Text.Trim();

                objDiag.IPP_DOSAGE1 = txtDossage1.Text.Trim();


                objDiag.IPP_TAKEN = drpTaken.SelectedValue;


                if (chkStatus.Checked == true)
                {
                    objDiag.IPP_STATUS = "Active";
                }
                else
                {
                    objDiag.IPP_STATUS = "InActive";
                }
                objDiag.IPP_STOP = Convert.ToString(chkStop.Checked);
                objDiag.IPP_STOP_REASON = txtStopReasion.Text;

                objDiag.IPPharmacyAdd();

                BindPharmacy();
                ClearPhar();

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.btnAddPhar_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteeDiag_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblPhyID = (Label)gvScanCard.Cells[0].FindControl("lblPhyID");
                
                if (CheckSchedule(lblPhyID.Text) == true)
                {
                    ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowErrorMessage('Once Schedule Done you can not Delet the Medicine.')", true);
                    goto FunEnd;
                }

                IP_PTPharmacy objDiag = new IP_PTPharmacy();
                objDiag.BranchID = Convert.ToString(Session["Branch_ID"]);
                objDiag.EMRID = Convert.ToString(Session["EMR_ID"]);

                objDiag.IPP_PHY_ID = lblPhyID.Text;

                objDiag.IPPharmacyDelete();


                BindPharmacy();
                ClearPhar();
            FunEnd: ;

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.DeletePhar_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void PhySelect_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;
                ViewState["PhySelectIndex"] = gvScanCard.RowIndex;
                gvPharmacy.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");



                Label lblPhyID = (Label)gvScanCard.Cells[0].FindControl("lblPhyID");

                Label lblPhyCode = (Label)gvScanCard.Cells[0].FindControl("lblPhyCode");
                Label lblPhyDesc = (Label)gvScanCard.Cells[0].FindControl("lblPhyDesc");

                Label lblPhyDuration = (Label)gvScanCard.Cells[0].FindControl("lblPhyDuration");
                Label lblPhyDurationType = (Label)gvScanCard.Cells[0].FindControl("lblPhyDurationType");

                Label lblPhyQTY = (Label)gvScanCard.Cells[0].FindControl("lblPhyQTY");
                Label lblPhyDosage = (Label)gvScanCard.Cells[0].FindControl("lblPhyDosage");

                Label lblPhyTaken = (Label)gvScanCard.Cells[0].FindControl("lblPhyTaken");
                Label lblPhyRoute = (Label)gvScanCard.Cells[0].FindControl("lblPhyRoute");



                Label lblPhyRefil = (Label)gvScanCard.Cells[0].FindControl("lblPhyRefil");
                Label lblPhyStDate = (Label)gvScanCard.Cells[0].FindControl("lblPhyStDate");
                Label lblPhyStTime = (Label)gvScanCard.Cells[0].FindControl("lblPhyStTime");
                Label lblPhyEndDate = (Label)gvScanCard.Cells[0].FindControl("lblPhyEndDate");
                Label lblPhyEndTime = (Label)gvScanCard.Cells[0].FindControl("lblPhyEndTime");

                Label lblInterval = (Label)gvScanCard.Cells[0].FindControl("lblInterval");

                Label lblPhyLifeLong = (Label)gvScanCard.Cells[0].FindControl("lblPhyLifeLong");
                Label lblPhyRemarks = (Label)gvScanCard.Cells[0].FindControl("lblPhyRemarks");
                Label lblPhyDossage1 = (Label)gvScanCard.Cells[0].FindControl("lblPhyDossage1");

                Label lblStatus = (Label)gvScanCard.Cells[0].FindControl("lblStatus");

                Label lblgvStop = (Label)gvScanCard.Cells[0].FindControl("lblgvStop");
                Label lblgvStopReasion = (Label)gvScanCard.Cells[0].FindControl("lblgvStopReasion");

                ViewState["IPP_PHY_ID"] = lblPhyID.Text;

                txtServCode.Text = lblPhyCode.Text;
                txtServName.Text = lblPhyDesc.Text;
                txtDuration.Text = lblPhyDuration.Text;

                string strSTHour = lblPhyStTime.Text;

                string[] arrSTHour = strSTHour.Split(':');
                if (arrSTHour.Length > 1)
                {
                    drpFrmHour.SelectedValue = arrSTHour[0];
                    drpFrmlMin.SelectedValue = arrSTHour[1];

                }

                string strEndHour = lblPhyEndTime.Text;

                string[] arrEndHour = strEndHour.Split(':');
                if (arrEndHour.Length > 1)
                {
                    drpEndHour.SelectedValue = arrEndHour[0];
                    drpEndMin.SelectedValue = arrEndHour[1];

                }

                string strInterHour = lblInterval.Text;

                string[] arrInterHour = strInterHour.Split(':');
                if (arrInterHour.Length > 1)
                {
                    drpinterHour.SelectedValue = arrInterHour[0];
                    drpinterMin.SelectedValue = arrInterHour[1];

                }



                drpDurationType.SelectedValue = lblPhyDurationType.Text;
                txtQty.Text = lblPhyQTY.Text;

                txtDossage1.Text = lblPhyDossage1.Text;

                drpDossage.SelectedValue = lblPhyDosage.Text;

                drpTaken.SelectedValue = lblPhyTaken.Text;
                drpRoute.SelectedValue = lblPhyRoute.Text;

                txtRefil.Text = lblPhyRefil.Text;
                txtFromDate.Text = lblPhyStDate.Text;
                txtToDate.Text = lblPhyEndDate.Text;
                if (lblStatus.Text == "Active")
                {
                    chkStatus.Checked = true;

                }
                else
                {
                    chkStatus.Checked = false;

                }



                txtRemarks.Text = lblPhyRemarks.Text;

                chkStop.Checked = Convert.ToBoolean(lblgvStop.Text);
                txtStopReasion.Text = lblgvStopReasion.Text;

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowReasion()", true);

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        //protected void gvPharmacy_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {


        //        //BELOW CODING FOR STRIKE THROUGH OPTION
        //        Label lblStatus = (Label)e.Row.FindControl("lblStatus");
        //        Label lblgvStop = (Label)e.Row.FindControl("lblgvStop");

        //        if (lblStatus.Text == "InActive" || lblgvStop.Text == "True")
        //        {
        //            e.Row.Font.Strikeout = true;

        //        }


        //    }
        //}

        protected void PhyTask_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblPhyID = (Label)gvScanCard.Cells[0].FindControl("lblPhyID");
                Label lblPhyCode = (Label)gvScanCard.Cells[0].FindControl("lblPhyCode");
                Label lblPhyDesc = (Label)gvScanCard.Cells[0].FindControl("lblPhyDesc");

                string strMed = lblPhyDesc.Text;


                ViewState["TaskPhyID"] = lblPhyID.Text;

                lblSheMedicinCode.Text = lblPhyCode.Text;

                if (strMed.Length >= 100)
                {
                    lblSheMedicinDesc.Text = strMed.Substring(0, 100);
                }
                else
                {
                    lblSheMedicinDesc.Text = strMed;
                }
                BindPharmacyShedul();

                ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "ShowPhyScedule()", true);




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Pharmacy.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }
        #endregion

        protected void btnPhySheduleAdd_Click(object sender, EventArgs e)
        {
            lblSheduleMsg.Text = "";
            if (CheckPassword(drpWardNurse.SelectedValue, txtWardNursePass.Text.Trim()) == false)
            {
                lblSheduleMsg.Text = "Wrong Password";
                lblSheduleMsg.Visible = true;
                goto FunEnd;
            }

            IP_PTPharmacy objDiag = new IP_PTPharmacy();
            objDiag.BranchID = Convert.ToString(Session["Branch_ID"]);
            objDiag.EMRID = Convert.ToString(Session["EMR_ID"]);
            objDiag.IPP_PHY_ID = Convert.ToString(ViewState["TaskPhyID"]);

            objDiag.IPPS_DATE = txtSheduleDate.Text + " " + drpSheduleHour.SelectedValue + ":" + drpSheduleMin.SelectedValue + ":00";
            objDiag.IPPS_GIVENBY = drpWardNurse.SelectedItem.Text;
            objDiag.IPPS_REMARKS = txtSheduleComment.Text;
            objDiag.IPPharmacySheduleAdd();

            ClearPhyShedule();
            BindPharmacyShedul();
            ScriptManager.RegisterClientScriptBlock(Page, Page.GetType(), Guid.NewGuid().ToString(), "HidePhyScedule()", true);

        FunEnd: ;
        }
    }
}