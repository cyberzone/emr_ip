﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;

namespace EMR_IP.VisitDetails
{
    public partial class Procedures1 : System.Web.UI.Page
    {
        #region Methods
        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine(strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindScreenCustomization()
        {
            CommonBAL objCom = new CommonBAL();
            string Criteria = " 1=1 ";

            Criteria += " AND SCREENNAME='EMR_PROC' AND SEGMENT='PROC_SEARCH_LIST'";

            DataSet DS = new DataSet();
            DS = objCom.ScreenCustomizationGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                if (DS.Tables[0].Rows[0].IsNull("CUST_VALUE") == false)
                {
                    ViewState["CUST_VALUE"] = DS.Tables[0].Rows[0]["CUST_VALUE"].ToString();
                }
                else
                {
                    ViewState["CUST_VALUE"] = "0";
                }
            }


        }

        void BindData()
        {

            string SearchFilter = txtSearch.Text.Trim();
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();
            if (drpSrcType.SelectedIndex == 0)
            {
                ds = dbo.HaadServicessListGet(drpServiceType.SelectedItem.Text, SearchFilter, "", Convert.ToString(Session["User_DeptID"]));
            }
            else
            {
                string Criteria = " 1=1 ";

                if (txtSearch.Text.Trim() != "")
                {
                    Criteria += " AND( EDF_CODE like'%" + txtSearch.Text.Trim() + "%' OR  EDF_NAME  like'%" + txtSearch.Text.Trim() + "%')";
                }
                Criteria += " AND EDF_TYPE='" + drpServiceType.SelectedValue + "' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "' )";

                ds = dbo.FavoritesGet(Criteria);
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvServicess.DataSource = ds;
                gvServicess.DataBind();

                string strPermission = hidPermission.Value;
                if (strPermission == "1" || strPermission == "7")
                {
                    gvServicess.Columns[0].Visible = false;
                }
            }
            else
            {
                gvServicess.DataBind();
            }
        }

        Boolean CheckFavorite(string Code)
        {
            DataSet ds = new DataSet();
            CommonBAL dbo = new CommonBAL();

            string Criteria = " 1=1 ";

            Criteria += " AND EDF_CODE='" + Code + "'";

            Criteria += " AND EDF_TYPE='" + drpServiceType.SelectedValue + "' AND ( EDF_DR_ID='" + Convert.ToString(Session["User_Code"]) + "' OR EDF_DEP_ID='" + Convert.ToString(Session["User_DeptID"]) + "')";

            ds = dbo.FavoritesGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        void BindProcedure()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND IPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            IP_PTProcedure objPro = new IP_PTProcedure();
            DS = objPro.IPProceduresGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvProcedure.DataSource = DS;
                gvProcedure.DataBind();

                string strPermission = hidPermission.Value;
                if (strPermission == "1" || strPermission == "5")
                {
                    gvProcedure.Columns[0].Visible = false;
                }

            }
            else
            {
                gvProcedure.DataBind();
            }

            txtRemarks.Text = "";
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                if (Convert.ToString(DR["IPP_REMARKS"]) != "")
                    txtRemarks.Text = Convert.ToString(DR["IPP_REMARKS"]);
            }
        FunEnd: ;
        }

        Boolean CheckProcedures(string Code)
        {
            string Criteria = " 1=1 ";

            Criteria += " AND IPP_PRO_CODE='" + Code + "'";
            Criteria += " AND IPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";




            DataSet DS = new DataSet();
            IP_PTProcedure objPro = new IP_PTProcedure();
            DS = objPro.IPProceduresGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                return true;
            }

            return false;
        }

        void ClearFavorite()
        {

            ViewState["Code"] = "";
            ViewState["Description"] = "";
            ViewState["Price"] = "";
            ViewState["gvServSelectIndex"] = "";
        }


        void ClearProc()
        {


            ViewState["PhySelectIndex"] = "";

            hidProcCode.Value = "";
            txtRemarks.Text = "";
            txtQty.Text = "1";
            txtPrice.Text = "1";
            // txtProcNotes.Text = "";
            ViewState["IPP_PRO_ID"] = "";
            ViewState["IPP_SO_TRANS_ID"] = "";
        }

        void BindProcedureNotes()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND IPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            IP_PTMaster objPTMast = new IP_PTMaster();
            DS = objPTMast.PTMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                txtProcNotes.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IPM_PROCEDURE_REMARKS"]));
            }

        }

        void UpdateProcedureNotes()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND IPM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPM_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            string ProceNotes = txtProcNotes.Text.Replace("'", "''");
            string FieldNameWithValues = " IPM_PROCEDURE_REMARKS='" + ProceNotes + "'";

            CommonBAL objCom = new CommonBAL();
            objCom.fnUpdateTableData(FieldNameWithValues, "IP_PT_MASTER", Criteria);

        }

        void SetPermission()
        {
            //HCREG,HCPTENOC
            string Criteria = " 1=1 AND HRT_SCREEN_ID='EMR_IP_PROC' ";
            Criteria += " AND  HRT_ROLL_ID='" + Convert.ToString(Session["Roll_Id"]) + "'";

            CommonBAL objCom = new CommonBAL();
            DataSet ds = new DataSet();
            ds = objCom.RollTransGet(Criteria);

            string strPermission = "0";
            if (ds.Tables[0].Rows.Count > 0)
            {
                strPermission = Convert.ToString(ds.Tables[0].Rows[0]["HRT_PERMISSION"]);

            }

            if (strPermission == "1")
            {

                btnAddFav.Visible = false;
                btnDeleteFav.Visible = false;
                // btnClear.Visible = false;

                btnProcUpdate.Visible = false;

            }

            if (strPermission == "5")
            {
                btnDeleteFav.Visible = false;

            }

            if (strPermission == "7")
            {
                btnAddFav.Visible = false;
                btnProcUpdate.Visible = false;

            }

            hidPermission.Value = strPermission;
            if (strPermission == "0")
            {
                Response.Redirect("../Common/PermissionDenied.aspx");
            }
        }

        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = "";
            string parameter = Request["__EVENTARGUMENT"];
            if (parameter == "Procedure")
            {
                BindData();
                txtSearch.Focus();

                //ScriptManager.RegisterStartupScript(this, this.GetType(), "LabResult", "ShowSearch();", true);



            }
            if (!IsPostBack)
            {
                if (Convert.ToString(Session["User_Category"]).ToUpper() != "ADMIN STAFF" && Convert.ToString(Session["User_ID"]) != "SUPER_ADMIN" && Convert.ToString(Session["User_Category"]).ToUpper() != "NURSE" && Convert.ToString(Session["User_Category"]).ToUpper() != "OTHERS" && Convert.ToString(Session["User_Category"]).ToUpper() != "RADIOLOGIST" && Convert.ToString(Session["User_Category"]).ToUpper() != "PATHOLOGIST")
                {
                    SetPermission();
                }

                try
                {
                    BindScreenCustomization();
                    if (hidPermission.Value == "9")
                    {
                        if (Convert.ToString(ViewState["CUST_VALUE"]) == "1")
                        {
                            drpSrcType.SelectedIndex = 1;
                            btnDeleteFav.Visible = true;
                            btnAddFav.Visible = false;
                        }
                        else
                        {
                            drpSrcType.SelectedIndex = 0;
                            btnAddFav.Visible = true;
                            btnDeleteFav.Visible = false;
                        }
                    }
                    BindData();
                    BindProcedure();
                    BindProcedureNotes();
                }
                catch (Exception ex)
                {
                    TextFileWriting("-----------------------------------------------");
                    TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.Page_Load");
                    TextFileWriting(ex.Message.ToString());
                }
            }
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.txtSearch_TextChanged");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvServicess_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvServicess.PageIndex = e.NewPageIndex;
                BindData();
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.gvServicess_PageIndexChanging");
                TextFileWriting(ex.Message.ToString());
            }
        }


        protected void Edit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnEdit = new LinkButton();
                btnEdit = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;
                ViewState["gvServSelectIndex"] = gvScanCard.RowIndex;
                gvServicess.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.Cells[0].FindControl("lblDesc");
                Label lblPrice = (Label)gvScanCard.Cells[0].FindControl("lblPrice");

                ViewState["Code"] = lblCode.Text;
                ViewState["Description"] = lblDesc.Text;
                ViewState["Price"] = lblPrice.Text;
                lblCode.BackColor = System.Drawing.Color.FromName("#c5e26d");
                lblDesc.BackColor = System.Drawing.Color.FromName("#c5e26d");
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.Edit_Click");
                TextFileWriting(ex.Message.ToString());
            }


        }


        protected void Add_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateProcedureNotes();

                Int32 intPos = 1;
                if (gvProcedure.Rows.Count >= 1)
                {
                    intPos = Convert.ToInt32(gvProcedure.Rows.Count) + 1;

                }

                ImageButton btnEdit = new ImageButton();
                btnEdit = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnEdit.Parent.Parent;


                Label lblCode = (Label)gvScanCard.Cells[0].FindControl("lblCode");
                Label lblDesc = (Label)gvScanCard.Cells[0].FindControl("lblDesc");
                Label lblPrice = (Label)gvScanCard.Cells[0].FindControl("lblPrice");

                //if (CheckProcedures(lblCode.Text) == true)
                //{
                //    lblMessage.Text = "This code already added";
                //    lblMessage.ForeColor = System.Drawing.Color.Red;
                //    goto FunEnd;
                //}

                IP_PTMaster objPTM = new IP_PTMaster();
                string SO_TRANS_ID = "";

                try
                {

                    objPTM.IPM_BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                    objPTM.IPM_ID = Convert.ToString(Session["EMR_ID"]);
                    objPTM.IPM_DR_CODE = Convert.ToString(Session["IAS_DR_ID"]);
                    objPTM.DR_Name = Convert.ToString(Session["IAS_DR_NAME"]);
                    objPTM.SO_ID = Convert.ToString(Session["SO_ID"]);

                    objPTM.ServCode = lblCode.Text;
                    objPTM.ServDesc = lblDesc.Text;
                    objPTM.ServQty = txtQty.Text.Trim();
                    objPTM.ServType = "PROC";
                    objPTM.IPSalesOrderTransAdd(out SO_TRANS_ID);


                }
                catch (Exception ex)
                {
                    goto FunEnd;
                }

                IP_PTProcedure objDiag = new IP_PTProcedure();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.IPP_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.IPP_PRO_ID = "";
                objDiag.IPP_PRO_CODE = lblCode.Text;
                objDiag.IPP_PRO_NAME = lblDesc.Text;
                objDiag.IPP_COST = lblPrice.Text.Trim();
                objDiag.IPP_REMARKS = txtRemarks.Text.Trim().Replace("'", "''"); ;
                objDiag.IPP_QTY = txtQty.Text.Trim();
                objDiag.IPP_PRICE = txtPrice.Text.Trim();
                objDiag.IPP_ORDERTYPE = drpOrderType.SelectedValue;
                objDiag.IPP_USEPRICE = txtPrice.Text.Trim();
                objDiag.IPP_NOTES = "";
                objDiag.IPP_TEMPLATE_CODE = "";
                objDiag.IPP_SO_TRANS_ID = SO_TRANS_ID;
                objDiag.IPProceduresAdd();


                /*

                  CommonBAL objCom = new CommonBAL();
                  string FieldNameWithValues = "IPP_REMARKS='" + txtRemarks.Text + "'";
                  string Criteria = "IPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPP_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
                  objCom.fnUpdateTableData(FieldNameWithValues, "EMR_PT_PROCEDURES", Criteria);
                  */
                BindProcedure();
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.Add_Click");
                TextFileWriting(ex.Message.ToString());
            }

        }


        protected void drpSrcType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (hidPermission.Value == "9")
            {
                if (drpSrcType.SelectedIndex == 0)
                {
                    btnAddFav.Visible = true;
                    btnDeleteFav.Visible = false;
                }
                else
                {
                    btnAddFav.Visible = false;
                    btnDeleteFav.Visible = true;
                }
            }
            BindData();

        }

        protected void btnAddFav_Click(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }

                if (CheckFavorite(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.Description = Convert.ToString(ViewState["Description"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = drpServiceType.SelectedValue;
                objCom.Price = Convert.ToString(ViewState["Price"]);
                objCom.FavoritesAdd();

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["gvServSelectIndex"])) == false)
                {
                    Int32 R = 0;
                    R = Convert.ToInt32(ViewState["gvServSelectIndex"]);
                    Label lblCode = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblCode");
                    Label lblDesc = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblDesc");
                    Label lblPrice = (Label)gvServicess.Rows[R].Cells[0].FindControl("lblPrice");

                    if (R % 2 == 0)
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#ffffff");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#ffffff");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#ffffff");
                    }
                    else
                    {
                        lblCode.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        lblDesc.BackColor = System.Drawing.Color.FromName("#f6f6f6");
                        gvServicess.Rows[R].BackColor = System.Drawing.Color.FromName("#f6f6f6");
                    }

                }

                ClearFavorite();
            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.btnAddFav_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void btnDeleteFav_Click(object sender, EventArgs e)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Code"])) == true)
                {
                    goto FunEnd;
                }
                if (string.IsNullOrEmpty(Convert.ToString(ViewState["Description"])) == true)
                {
                    goto FunEnd;
                }
                if (drpSrcType.SelectedIndex == 0)
                {
                    goto FunEnd;
                }

                //if (CheckFavorite(Convert.ToString(ViewState["Code"])) == false)
                //{
                //    goto FunEnd;
                //}


                CommonBAL objCom = new CommonBAL();
                objCom.BranchID = Convert.ToString(Session["Branch_ID"]);
                objCom.Code = Convert.ToString(ViewState["Code"]);
                objCom.DR_ID = Convert.ToString(Session["User_Code"]);
                objCom.DEP_ID = Convert.ToString(Session["User_DeptID"]);
                objCom.Type = drpServiceType.SelectedValue;
                objCom.FavoritesDelete();

                ClearFavorite();

                BindData(); ;

            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.btnDeleteFav_Click");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void DeleteeDiag_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnDel = new ImageButton();
                btnDel = (ImageButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;

                Label lblDiagCode = (Label)gvScanCard.Cells[0].FindControl("lblDiagCode");
                Label lblProID = (Label)gvScanCard.Cells[0].FindControl("lblProID");
                Label lblSOTransID = (Label)gvScanCard.Cells[0].FindControl("lblSOTransID");

                IP_PTProcedure objDiag = new IP_PTProcedure();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.IPP_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.IPP_PRO_ID = lblProID.Text;
                objDiag.IPP_SO_TRANS_ID = lblSOTransID.Text;

                objDiag.IPProceduresDelete();


                BindProcedure();

            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }


        protected void ProcEdit_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton btnDel = new LinkButton();
                btnDel = (LinkButton)sender;

                GridViewRow gvScanCard;
                gvScanCard = (GridViewRow)btnDel.Parent.Parent;
                ViewState["PhySelectIndex"] = gvScanCard.RowIndex;
                // gvProcedure.Rows[gvScanCard.RowIndex].BackColor = System.Drawing.Color.FromName("#c5e26d");

                Label lblProID = (Label)gvScanCard.Cells[0].FindControl("lblProID");
                Label lblDiagCode = (Label)gvScanCard.Cells[0].FindControl("lblDiagCode");

                Label lblDiagRemaark = (Label)gvScanCard.Cells[0].FindControl("lblDiagRemaark");
                Label lblDiagQTY = (Label)gvScanCard.Cells[0].FindControl("lblDiagQTY");
                Label lblDiagUserPrice = (Label)gvScanCard.Cells[0].FindControl("lblDiagUserPrice");
                Label lblOrderType = (Label)gvScanCard.Cells[0].FindControl("lblOrderType");
                Label lblNotes = (Label)gvScanCard.Cells[0].FindControl("lblNotes");
                Label lblSOTransID = (Label)gvScanCard.Cells[0].FindControl("lblSOTransID");


                hidProcCode.Value = lblDiagCode.Text;
                txtRemarks.Text = lblDiagRemaark.Text;
                txtQty.Text = lblDiagQTY.Text;
                txtPrice.Text = lblDiagUserPrice.Text;
                //txtProcNotes.Text = lblNotes.Text;

                ViewState["IPP_PRO_ID"] = lblProID.Text;
                ViewState["IPP_SO_TRANS_ID"] = lblSOTransID.Text;

                if (lblOrderType.Text != "")
                {

                    for (int intCount = 0; intCount < drpOrderType.Items.Count; intCount++)
                    {
                        if (drpOrderType.Items[intCount].Value == lblOrderType.Text)
                        {
                            drpOrderType.SelectedValue = lblOrderType.Text;

                        }
                    }
                }


                //IP_PTProcedure objDiag = new IP_PTProcedure();
                //objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                //objDiag.IPP_ID = Convert.ToString(Session["EMR_ID"]);
                //objDiag.IPP_DIAG_CODE = lblDiagCode.Text;
                //objDiag.IPP_REMARKS = lblDiagRemaark.Text.Trim();
                //objDiag.IPP_QTY = lblDiagQTY.Text.Trim();
                //objDiag.IPP_ORDERTYPE = lblOrderType.Text;
                //objDiag.IPP_USEPRICE = lblDiagUserPrice.Text.Trim();
                //objDiag.IPP_NOTES = lblNotes.Text;




            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }

        protected void btnProcUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                UpdateProcedureNotes();

                IP_PTProcedure objDiag = new IP_PTProcedure();
                objDiag.BRANCH_ID = Convert.ToString(Session["Branch_ID"]);
                objDiag.IPP_ID = Convert.ToString(Session["EMR_ID"]);
                objDiag.IPP_PRO_ID = Convert.ToString(ViewState["IPP_PRO_ID"]);
                objDiag.IPP_PRO_CODE = hidProcCode.Value;
                objDiag.IPP_REMARKS = txtRemarks.Text.Trim();
                objDiag.IPP_QTY = txtQty.Text.Trim();
                objDiag.IPP_ORDERTYPE = drpOrderType.SelectedValue;
                objDiag.IPP_USEPRICE = txtPrice.Text.Trim();
                objDiag.IPP_NOTES = "";
                objDiag.IPP_SO_TRANS_ID = Convert.ToString(ViewState["IPP_SO_TRANS_ID"]);
                objDiag.IPProceduresAdd();


                ClearProc();
                BindProcedure();


            FunEnd: ;
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "      Procedures.DeleteeDiag_Click");
                TextFileWriting(ex.Message.ToString());
            }



        }



        #endregion

    }
}