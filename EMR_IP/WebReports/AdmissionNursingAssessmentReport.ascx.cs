﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.WebReports
{
    public partial class AdmissionNursingAssessmentReport : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

        CommonBAL objCom = new CommonBAL();
        IP_AdmissionNursingAssessment objAdmNurAss = new IP_AdmissionNursingAssessment();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("AdmissionNursingAssessmentReport." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindData()
        {
            objAdmNurAss = new IP_AdmissionNursingAssessment();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 and IANA_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IANA_ID='" + EMR_ID + "'";
            DS = objAdmNurAss.AdmiNursingAssessmentGet(Criteria);

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                txtAdmissRoute.Text = Convert.ToString(DR["IANA_ADMISSION_ROUTE"]);
                drpWardNo.Text = Convert.ToString(DR["IANA_ADMISSION_WARD"]);
                drpRoomNo.Text = Convert.ToString(DR["IANA_ROOM_NO"]);
                drpBed.Text = Convert.ToString(DR["IANA_BED_NO"]);


                txtArrivalDate.Text = Convert.ToString(DR["IANA_WARD_ARRIVAL_DateDesc"]);
                string strArrivalDate = Convert.ToString(DR["IANA_WARD_ARRIVAL_TIMEDesc"]);
                string[] arrArrivalDater = strArrivalDate.Split(':');


                txtAdmiDate.Text = Convert.ToString(DR["IANA_ADMISSION_DATE_DateDesc"]);


                drpAdmissionMode.Text = Convert.ToString(DR["IANA_ADMISSION_MODE"]);
                drpInfObtFrom.Text = Convert.ToString(DR["IANA_INFO_OBTAINED_FROM"]);
                txtDiag.Text = Convert.ToString(DR["IANA_DIAGNOSIS"]);
                txtWeight.Text = Convert.ToString(DR["IANA_WEIGHT"]);
                txtHeight.Text = Convert.ToString(DR["IANA_HEIGHT"]);
                txtPTPainScore.Text = Convert.ToString(DR["IANA_PT_PAIN_SCORE"]);


                string strOrientationUnit = Convert.ToString(DR["IANA_ORIENTATION_UNIT"]);

                string[] arrOrientationUnit = strOrientationUnit.Split('|');

                if (arrOrientationUnit.Length > 0)
                {
                    for (Int32 i = 0; i < chkOrientationUnit.Items.Count - 1; i++)
                    {
                        for (Int32 j = 0; j < arrOrientationUnit.Length; j++)
                        {

                            if (chkOrientationUnit.Items[i].Value == arrOrientationUnit[j])
                            {
                                chkOrientationUnit.Items[i].Selected = true;
                                goto OrientForEnd;
                            }

                        }
                    OrientForEnd: ;
                    }

                }

                radValuables.SelectedValue = Convert.ToString(DR["IANA_BROUGHT_VALUABLE"]);
                txtPTSenthWith.Text = Convert.ToString(DR["IANA_SENT_HOME_WITH"]);
                txtProGivenBy.Text = Convert.ToString(DR["IANA_PRO_GIVEN_BY"]);
                drpLivingWith.Text = Convert.ToString(DR["IANA_LIVING_SITUATION"]);
                drpProfession.Text = Convert.ToString(DR["IANA_PROFESSION"]);
                textLivingYears.Text = Convert.ToString(DR["IANA_LIVING_UAE"]);
                radMarried.SelectedValue = Convert.ToString(DR["IANA_MARRIED"]);

                if (Convert.ToString(DR["IANA_SMOKE"]) == radSmokeYes.Value)
                {
                    radSmokeYes.Checked = true;
                }
                else if (Convert.ToString(DR["IANA_SMOKE"]) == radSmokeNo.Value)
                {
                    radSmokeNo.Checked = true;
                }


                CigsCount.Text = Convert.ToString(DR["IANA_SMOKE_COUNT"]);

                string strSmokeQuit = Convert.ToString(DR["IANA_QUIT_SMOKE"]);
                string[] arrSmokeQuit = strSmokeQuit.Split('|');
                if (arrSmokeQuit.Length > 0)
                {
                    txtSmokeQuitYears.Text = arrSmokeQuit[0];
                    if (arrSmokeQuit.Length > 1)
                    {
                        txtSmokeQuitMonth.Text = arrSmokeQuit[1];
                    }

                }


                if (Convert.ToString(DR["IANA_ALCOHOL"]) == radAlcoholYes.Value)
                {
                    radAlcoholYes.Checked = true;
                }
                else if (Convert.ToString(DR["IANA_ALCOHOL"]) == radAlcoholNo.Value)
                {
                    radAlcoholNo.Checked = true;
                }

                string strAlcoholQuit = Convert.ToString(DR["IANA_QUIT_ALCOHOL"]);
                string[] arrAlcoholQuit = strAlcoholQuit.Split('|');
                if (arrAlcoholQuit.Length > 0)
                {
                    txtDrinkQuitYears.Text = arrAlcoholQuit[0];
                    if (arrSmokeQuit.Length > 1)
                    {
                        txtDrinkQuitMonth.Text = arrAlcoholQuit[1];
                    }

                }

                for (Int32 i = 0; i < chkPresPastHealthProb.Items.Count - 1; i++)
                {

                    if (chkPresPastHealthProb.Items[i].Selected == true)
                    {
                        objAdmNurAss.IANA_PRES_HEALTH_PROB += chkPresPastHealthProb.Items[i].Value + "|";
                    }
                }

                string strPresPastHealthProb = Convert.ToString(DR["IANA_PRES_HEALTH_PROB"]);

                string[] arrPresPastHealthProb = strPresPastHealthProb.Split('|');

                if (arrPresPastHealthProb.Length > 0)
                {
                    for (Int32 i = 0; i < chkPresPastHealthProb.Items.Count - 1; i++)
                    {
                        for (Int32 j = 0; j < arrPresPastHealthProb.Length; j++)
                        {

                            if (chkPresPastHealthProb.Items[i].Value == arrPresPastHealthProb[j])
                            {
                                chkPresPastHealthProb.Items[i].Selected = true;
                                goto PresPastProbtForEnd;
                            }

                        }
                    PresPastProbtForEnd: ;
                    }

                }


                txtProbIdentified.Text = Convert.ToString(DR["IANA_PROB_IDENTIFIED"]);
                txtPrevHostSurgeries.Text = Convert.ToString(DR["IANA_PREV_HOSP_SURG"]);



                if (Convert.ToString(DR["IANA_ALLERGY"]) == radAlergiesYes.Value)
                {
                    radAlergiesYes.Checked = true;
                }
                else if (Convert.ToString(DR["IANA_ALLERGY"]) == radAlergiesNo.Value)
                {
                    radAlergiesNo.Checked = true;
                }



                txtAlergies.Text = Convert.ToString(DR["IANA_ALLERGY_DESC"]);
                txtReactionType.Text = Convert.ToString(DR["IANA_REACTION_TYPE"]);

                if (Convert.ToString(DR["IANA_MEDICINE_BROUGHT"]) == "True")
                {
                    chkMedicineBrought.Checked = true;
                }
                else
                {
                    chkMedicineBrought.Checked = false;
                }

                if (Convert.ToString(DR["IANA_PHYSICIAN_VARIFIED"]) == "True")
                {
                    chkVarifiedPhysician.Checked = true;
                }
                else
                {
                    chkVarifiedPhysician.Checked = false;
                }

                txtSentHomeWith.Text = Convert.ToString(DR["IANA_MED_SENT_HOME_WITH"]);
                txtPainScore.Text = Convert.ToString(DR["IANA_PAIN_SCORE"]);
                PainScoreType.SelectedValue = Convert.ToString(DR["IANA_PAIN_SCORE_TYPE"]);
                txtPainLocation.Text = Convert.ToString(DR["IANA_PAIN_LOCATION"]);
                txtPainOnset.Text = Convert.ToString(DR["IANA_PAIN_ONSET"]);
                txtPainVariations.Text = Convert.ToString(DR["IANA_PAIN_VARIATIONS"]);
                drpPainAggravates.Text = Convert.ToString(DR["IANA_PAIN_AGGRAVATES"]);
                drpPainRelieves.Text = Convert.ToString(DR["IANA_PAIN_RELIEVES"]);
                txtMedications.Text = Convert.ToString(DR["IANA_PAIN_MEDICATIONS"]);
                drpPainEffects.Text = Convert.ToString(DR["IANA_PAIN_EFFECTS"]);


            }

        }

        void BindPharmacyGrid()
        {

            DataSet ds = new DataSet();
            objAdmNurAss = new IP_AdmissionNursingAssessment();

            string Criteria = " 1=1 ";
            Criteria += " AND IANM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IANM_ID='" + EMR_ID + "'";
            ds = objAdmNurAss.AdmiNurseMedicHistoryGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvPharmacy.DataSource = ds;
                gvPharmacy.DataBind();

            }
            else
            {
                gvPharmacy.DataBind();
            }
        }


        DataSet GetSegmentData(string strType, string Position)
        {
            objAdmNurAss = new IP_AdmissionNursingAssessment();
            string Criteria = " 1=1 AND INAS_ACTIVE=1 AND INAS_BRANCH='" + Convert.ToString(Session["Branch_ID"]) + "' AND INAS_TYPE='" + strType + "'";
            Criteria += " AND  INAS_POSITION='" + Position + "'";

            DataSet DS = new DataSet();
            DS = objAdmNurAss.AdminNurseAssSegmentGet(Criteria);


            return DS;

        }

        DataSet GetSegmentMaster(string strType)
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 AND INASM_STATUS=1 AND INASM_TYPE='" + strType + "'";


            DataSet DS = new DataSet();
            DS = objCom.fnGetFieldValue(" * ", "IP_ADMIN_NURSE_ASS_SEGMENT_MASTER", Criteria, "");


            return DS;
        }

        public void CreateSegCtrls(string strType, string Position)
        {
            DataSet DS = new DataSet();

            DS = GetSegmentData(strType, Position);

          
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                Boolean boolFieldSet = false;

                Panel myFieldSet = new Panel();

                myFieldSet.GroupingText = Convert.ToString(DR["INAS_NAME"]);
                myFieldSet.Style.Add("text-align", "justify");
                myFieldSet.CssClass = "lblCaption1";
                myFieldSet.Width = 500;
                Int32 i = 1;

                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["INAS_CODE"]));

                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {



                    Boolean _checked = false;
                    string strComment = "";
                    objAdmNurAss = new IP_AdmissionNursingAssessment();
                    string Criteria2 = "1=1 ";
                    Criteria2 += " AND INASD_TYPE='" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "' AND INASD_FIELD_ID='" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim() + "' ";
                    Criteria2 += " AND INASD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND INASD_ID='" + EMR_ID  + "'";


                    DataSet DS2 = new DataSet();
                    DS2 = objAdmNurAss.AdminNurseAssSegmentDtlsGet(Criteria2);

                    if (DS2.Tables[0].Rows.Count > 0)
                    {
                        _checked = true;

                        strComment = Convert.ToString(DS2.Tables[0].Rows[0]["INASD_COMMENT"]).Trim();
                    }


                    if (Convert.ToString(DR1["INASM_CONTROL_TYPE"]).ToUpper() == "CheckBox".ToUpper())
                    {
                        CheckBox chk = new CheckBox();
                        chk.ID = "chk" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim();
                        chk.Text = Convert.ToString(DR1["INASM_FIELD_NAME"]).Trim();
                        chk.CssClass = "lblCaption1";
                        chk.Enabled = false;

                        chk.Checked = _checked;

                        if (_checked == true)
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(chk);
                        }
                    }


                    if (Convert.ToString(DR1["INASM_CONTROL_TYPE"]).ToUpper() == "TextBox".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["INASM_FIELD_NAME"]).Trim() + ":";
                        

                        Literal lit1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };


                        Label txt = new Label();
                        txt.ID = "txt" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["INASM_FIELD_NAME"]);
                        txt.CssClass = "Label";
                        txt.Style.Add("width", "99%");

                        txt.Text = strComment;

                        


                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(lbl);
                          //  myFieldSet.Controls.Add(lit1);
                            myFieldSet.Controls.Add(txt);
                        }

                    }

                    if (Convert.ToString(DR1["INASM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Width = 150;
                        lbl.Text = Convert.ToString(DR1["INASM_FIELD_NAME"]).Trim() + ":";
                       

                        Label txt = new Label();
                        txt.ID = "txt" + Convert.ToString(DR1["INASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["INASM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["INASM_FIELD_NAME"]);
                        txt.CssClass = "Label";
                        txt.Style.Add("width", "75px");
 


                        txt.Text = strComment;

                        myFieldSet.Controls.Add(txt);
                       

                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(lbl);
                            myFieldSet.Controls.Add(txt);
                        }
                       
                    }




                    if (Convert.ToString(DR1["INASM_CONTROL_TYPE"]).ToUpper() == "Label".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["INASM_FIELD_NAME"]).Trim();
                        myFieldSet.Controls.Add(lbl);
 

                    }


                    if (strComment.Trim() != "")
                    {
                        boolFieldSet = true;
                        Literal litGap = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                        myFieldSet.Controls.Add(litGap);
                    }

                }

                //if (Convert.ToString(DR["IASM_CONTROL_TYPE"]) == "RadioButtonList")
                //{

                //    intIndex = intIndex + 1;
                //    RadList.Items.Add(new ListItem(Convert.ToString(DR["IASM_FIELD_NAME"]), Convert.ToString(DR["IASM_FIELD_ID"])));
                //    RadList.CssClass = "lblCaption1";
                //    if (_checked == true)
                //    {
                //        RadList.SelectedValue = Convert.ToString(DR["IASM_FIELD_ID"]);
                //    }
                //    myFieldSet.Controls.Add(RadList);

                //}
                if (strType == "IP_NURSE_ASSESSMENT_PHY")
                {
                    if (boolFieldSet == true)
                    {
                        if (Position == "LEFT")
                        {
                            plhoPhyAssessmentLeft.Controls.Add(myFieldSet);
                        }
                        else
                        {
                            plhoPhyAssessmentRight.Controls.Add(myFieldSet);
                        }
                    }
                }

                if (strType == "IP_NURSE_ASSESSMENT_RSA")
                {
                    if (boolFieldSet == true)
                    {
                        if (Position == "LEFT")
                        {
                            plhoRiskSafetyAssessmentLeft.Controls.Add(myFieldSet);
                        }
                        else
                        {

                        }
                    }
                }

                if (strType == "IP_NURSE_ASSESSMENT_EDU")
                {
                    if (boolFieldSet == true)
                    {
                        if (Position == "LEFT")
                        {
                            plhoEducationalAssessmentLeft.Controls.Add(myFieldSet);
                        }
                        else
                        {

                        }
                    }
                }


                if (strType == "IP_NURSE_ASSESSMENT_FUN")
                {
                    if (boolFieldSet == true)
                    {
                        if (Position == "LEFT")
                        {
                            plhoFunctionalAssessmentLeft.Controls.Add(myFieldSet);
                        }
                        else
                        {

                        }
                    }
                }


                if (strType == "IP_NURSE_ASSESSMENT_BRSKIN")
                {
                    if (boolFieldSet == true)
                    {
                        if (Position == "RIGHT")
                        {
                            plhoBraSkinRiskAssessmentRight.Controls.Add(myFieldSet);
                        }
                        else
                        {

                        }
                    }
                }

                if (strType == "IP_NURSE_ASSESSMENT_DISCH")
                {
                    if (boolFieldSet == true)
                    {
                        if (Position == "LEFT")
                        {
                            plhoPatientDischAssessmentLeft.Controls.Add(myFieldSet);
                        }
                        else
                        {
                            plhoPatientDischAssessmentRight.Controls.Add(myFieldSet);
                        }
                    }
                }

                if (strType == "IP_NURSE_ASSESSMENT_DISCH_SUM")
                {
                    if (boolFieldSet == true)
                    {
                        if (Position == "LEFT")
                        {
                            plhoPatientDischSummaryLeft.Controls.Add(myFieldSet);
                        }
                        else
                        {

                        }
                    }
                }

                i = i + 1;



            }
        }

        public void BindAdmissionNursingAssessment()
        {
            BindData();
            BindPharmacyGrid();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                    

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }


        public void generateDynamicControls()
        {

            CreateSegCtrls("IP_NURSE_ASSESSMENT_PHY", "LEFT");
            CreateSegCtrls("IP_NURSE_ASSESSMENT_PHY", "RIGHT");

            CreateSegCtrls("IP_NURSE_ASSESSMENT_RSA", "LEFT");
            CreateSegCtrls("IP_NURSE_ASSESSMENT_EDU", "LEFT");
            CreateSegCtrls("IP_NURSE_ASSESSMENT_FUN", "LEFT");
            CreateSegCtrls("IP_NURSE_ASSESSMENT_BRSKIN", "RIGHT");

            CreateSegCtrls("IP_NURSE_ASSESSMENT_DISCH", "LEFT");
            CreateSegCtrls("IP_NURSE_ASSESSMENT_DISCH_SUM", "LEFT");
            CreateSegCtrls("IP_NURSE_ASSESSMENT_DISCH", "RIGHT");

        }
    }
}