﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdmissionRequestReport.ascx.cs" Inherits="EMR_IP.WebReports.AdmissionRequestReport" %>

<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1 BoldStyle"> <u> Admission Request</u></span>
<br />



<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>


        <td class="lblCaption1 BoldStyle BorderStyle"  style="width:150px;">
            <label for="AdmissionElective" class="lblCaption1">Admission</label>
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:RadioButtonList ID="radAdmissionType" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Enabled="false">
                <asp:ListItem Value="Elective" Text="Elective" Selected="True"></asp:ListItem>
                <asp:ListItem Value="Emergency" Text="Emergency"></asp:ListItem>

            </asp:RadioButtonList>
        </td>
    </tr>


    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <label for="AttendingPhysician" class="lblCaption1">Attending Physician</label></td>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:label ID="AttendingPhysician" runat="server" CssClass="lblCaption1" Enabled="false" Width="200px"></asp:label>
        </td>

    </tr>
</table>

<span class="lblCaption1"  >Investigations Requested</span>
<table cellpadding="0" cellspacing="0" style="width: 100%">

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" style="width:150px;">
            <label for="ProvisionalDiagonosis" class="lblCaption1">Provisional Diagnosis</label></td>
       <td class="lblCaption1 BoldStyle BorderStyle">
             <asp:Label id="ProvisionalDiagnosis" runat="server"  class="lblCaption1" ></asp:Label></td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <label for="Laboratory" class="lblCaption1">Laboratory</label></td>
        <td class="lblCaption1 BoldStyle BorderStyle">
              <asp:Label id="Laboratory" runat="server" class="lblCaption1"  ></asp:Label></td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <label for="Radiological" class="lblCaption1">Radiological</label></td>
        <td>
              <asp:Label id="Radiological" runat="server"   class="lblCaption1"></asp:Label></td>
    </tr>
      <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <label for="OtherAdmissionSummary" class="lblCaption1">Others</label>
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="OtherAdmissionSummary" runat="server" CssClass="label" Width="100px"></asp:Label>
        </td>
    </tr>
</table>
<br />

<table cellpadding="0" cellspacing="0" style="width: 100%">
   
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle"  style="width:150px;">
            <label for="ProcedurePlanned" class="lblCaption1">Procedure Planned</label></td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="ProcedurePlanned" runat="server" CssClass="label" ></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <label for="Anesthesia" class="lblCaption1">Anesthesia</label></td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="Anesthesia" runat="server" CssClass="label"></asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <label for="Treatment" class="lblCaption1">Treatment</label></td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="Treatment" runat="server" CssClass="label" ></asp:Label>
        </td>
    </tr>

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <label for="ReasonforAdmission" class="lblCaption1">Reason for Admission</label></td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="ReasonforAdmission" runat="server" CssClass="label"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <label for="Remarks" class="lblCaption1">Remarks</label></td>
        <td class="lblCaption1 BoldStyle BorderStyle">


            <asp:Label ID="Remarks" runat="server" CssClass="label" ></asp:Label>
        </td>
    </tr>

</table>
