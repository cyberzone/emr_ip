﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnesthesiaReport.ascx.cs" Inherits="EMR_IP.WebReports.AnesthesiaReport" %>

  <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1">Anesthesia Record</span>

<table cellpadding="0" cellspacing="0" style="width: 100%">

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Date :
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblAnesDate" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Time In :
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblTimeIn" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Patient out of the OR :
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblTimeOut" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Anesthesia Start :
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblAnesST" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Anesthesia stopes :
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblAnesFin" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle"></td>
        <td class="lblCaption1 BorderStyle"></td>

        <tr>
            <td class="lblCaption1 BoldStyle BorderStyle">Surgery Start :
            </td>
            <td class="lblCaption1 BorderStyle">
                <asp:Label ID="lblSurgST" runat="server" CssClass="lblCaption1"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle BorderStyle">Surgery Ends :
            </td>
            <td class="lblCaption1 BorderStyle">
                <asp:Label ID="lblSurgFin" runat="server" CssClass="lblCaption1"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle BorderStyle"></td>
            <td class="lblCaption1 BorderStyle"></td>


        </tr>
</table>

<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 ">Anesthetic procedure 

        </td>
    </tr>
    <tr>

        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblAnesProcedure" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>

    </tr>
    <tr>
        <td class="lblCaption1 ">Surgical procedure 

        </td>
    </tr>
    <tr>

        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblSurProcedure" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>

    </tr>

    <tr>
        <td class="lblCaption1">Name of Surgeon

        </td>
    </tr>
    <tr>

        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblSurgeon" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>

    </tr>

    <tr>
        <td class="lblCaption1">Assistants

        </td>
    </tr>
    <tr>

        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblAssistantsDtls" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>

    </tr>
</table>

<span class="lblCaption1">The immediate post anesthesia status ot the patient at the time of the transfer from OR  and score</span>
<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle"></td>

        <td class="lblCaption1 BorderStyle">Score
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle"></td>
        <td class="lblCaption1 BorderStyle">Score
        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">B/P :
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblBPValue" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Motor function :
        </td>

        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblMotorFnValue" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">SPo2 :
        </td>

        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblSPo2Value" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Pain Score :
        </td>

        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblPainScoreValue" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Conciousness :
        </td>

        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblConciousnessValue" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Respiration :
        </td>

        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblRespirationValue" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
    </tr>


</table>

<table style="width: 100%">
    <tr>
        <td style="width: 50%; vertical-align: top;">
            <asp:PlaceHolder ID="plhoPhyAssessmentLeft" runat="server"></asp:PlaceHolder>
        </td>
        <td style="width: 50%; vertical-align: top;">
            <asp:PlaceHolder ID="plhoPhyAssessmentRight" runat="server"></asp:PlaceHolder>
        </td>
    </tr>
</table>
<span  class="lblCaption1">Anesthesiologist Signature</span>
<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>

        <td class="lblCaption1 BoldStyle BorderStyle">Name  
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblSigAnesthesiologist" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">Date & Time  
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblSigAnestheDt" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
    </tr>



</table>

<br />


