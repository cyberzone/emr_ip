﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;
namespace EMR_IP.WebReports
{
    public partial class AnesthesiaReport : System.Web.UI.UserControl
    {

        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

        DataSet DS = new DataSet();
        CommonBAL objCom = new CommonBAL();
        IP_Anesthesia objAnes = new IP_Anesthesia();


       public void BindAnesthesia()
        {
            objAnes = new IP_Anesthesia();
            DS = new DataSet();
            string Criteria = "   IA_ID ='" + EMR_ID + "'";
            DS = objAnes.AnesthesiaGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    lblAnesDate.Text = Convert.ToString(DR["IA_DATEDesc"]);

                    lblAnesProcedure.Text = Convert.ToString(DR["IA_ANESTHETIC_PROCEDURES"]);
                    lblSurProcedure.Text = Convert.ToString(DR["IA_SURGICAL_PROCEDURES"]);
                    lblSurgeon.Text = Convert.ToString(DR["IA_SURGEONDesc"]);
                    lblAssistantsDtls.Text = Convert.ToString(DR["IA_ASSISTANT"]);


                    lblTimeIn.Text = Convert.ToString(DR["IA_TIME_INDesc"]);



                    lblAnesST.Text = Convert.ToString(DR["IA_ANESTHESIA_STARTDesc"]);




                    lblSurgFin.Text = Convert.ToString(DR["IA_SURGERY_ENDDesc"]);



                    lblAnesFin.Text = Convert.ToString(DR["IA_ANESTHESIA_STOPESDesc"]);


                    lblTimeOut.Text = Convert.ToString(DR["IA_PT_OUT_OF_ORDesc"]);



                    //lblBP.Text = Convert.ToString(DR["IA_BP_SCORE"]);
                    //lblSPo2.Text = Convert.ToString(DR["IA_SPO2_SCORE"]);
                    //lblConciousness.Text = Convert.ToString(DR["IA_CONCIOUSNESS_SCORE"]);
                    //lblMotorFn.Text = Convert.ToString(DR["IA_MOTOR_FUN_SCORE"]);
                    //lblPainScore.Text = Convert.ToString(DR["IA_PAIN_SCORE"]);
                    //lblRespiration.Text = Convert.ToString(DR["IA_RESPIRATION_SCORE"]);


                    lblBPValue.Text = Convert.ToString(DR["IA_BP_SCORE"]);
                    lblSPo2Value.Text = Convert.ToString(DR["IA_SPO2_SCORE"]);
                    lblConciousnessValue.Text = Convert.ToString(DR["IA_CONCIOUSNESS_SCORE"]);
                    lblMotorFnValue.Text = Convert.ToString(DR["IA_MOTOR_FUN_SCORE"]);
                    lblPainScoreValue.Text = Convert.ToString(DR["IA_PAIN_SCORE"]);
                    lblRespirationValue.Text = Convert.ToString(DR["IA_RESPIRATION_SCORE"]);

                    lblSigAnesthesiologist.Text = Convert.ToString(DR["IA_ANESTHESIOLOGIST_SIGN"]);
                    lblSigAnestheDt.Text = Convert.ToString(DR["IA_ANESTHESIOLOGIST_SIGN_DATE"]);


                }

            }
        }
        

        DataSet GetSegmentData()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 AND IAS_ACTIVE=1 AND IAS_BRANCH='" + Convert.ToString(Session["Branch_ID"]) + "'";// AND IAS_TYPE='" + strType + "'";
            // Criteria += " AND  IAS_POSITION='" + Position + "'";

            DataSet DS = new DataSet();

            DS = objCom.fnGetFieldValue(" * ", "IP_ANESTHESIA_SEGMENT", Criteria, "IAS_ORDER");



            return DS;

        }

        DataSet GetSegmentMaster(string SegmentType)
        {
            DataSet DS = new DataSet();

            string Criteria = " 1=1 AND IASM_STATUS=1 ";
            Criteria += " and IASM_TYPE='" + SegmentType + "'";


            objAnes = new IP_Anesthesia();
            DS = objAnes.AnesthesiaSegmentMasterGet(Criteria);


            return DS;

        }

        public void CreateSegCtrls()
        {
            string Position = "";
            DataSet DS = new DataSet();

            DS = GetSegmentData();

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                Boolean boolFieldSet = false;
                Position = Convert.ToString(DR["IAS_POSITION"]);

                Panel myFieldSet = new Panel();

                myFieldSet.GroupingText = Convert.ToString(DR["IAS_NAME"]);
                myFieldSet.Style.Add("text-align", "justify");
                myFieldSet.CssClass = "lblCaption1";
                myFieldSet.Width = 500;
                Int32 i = 1;

                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["IAS_CODE"]));

                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {


                    Boolean _checked = false;
                    string strComment = "";
                    objAnes = new IP_Anesthesia();
                    string Criteria2 = "1=1 ";
                    Criteria2 += " AND IASD_TYPE='" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "' AND IASD_FIELD_ID='" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim() + "' ";
                    Criteria2 += " AND IASD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IASD_ID='" + EMR_ID + "'";

                    DataSet DS2 = new DataSet();
                    DS2 = objAnes.AnesthesiaSegmentDtlsGet(Criteria2);

                    if (DS2.Tables[0].Rows.Count > 0)
                    {
                        _checked = true;

                        strComment = Convert.ToString(DS2.Tables[0].Rows[0]["IASD_COMMENT"]).Trim();
                    }


                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "CheckBox".ToUpper())
                    {
                        CheckBox chk = new CheckBox();
                        chk.ID = "chk" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();
                        chk.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim();
                        chk.CssClass = "lblCaption1";
                        chk.Enabled = false;

                        chk.Checked = _checked;

                        if (_checked == true)
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(chk);
                        }
                    }


                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "TextBox".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim() + ":";
                      

                        Literal lit1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                      

                        Label txt = new Label();
                        txt.ID = "txt" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]);
                        txt.CssClass = "Label";
                      

                        txt.Text = strComment;

                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(lbl);
                            myFieldSet.Controls.Add(lit1);
                            myFieldSet.Controls.Add(txt);
                        }


                    }


                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "AutoTextBox".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim() + ":";
                      

                        Literal lit1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                      

                        Label txt = new Label();
                        txt.ID = "txt" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();
                        
                        txt.CssClass = "Label";


                        txt.Text = strComment;

                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(lbl);
                            myFieldSet.Controls.Add(lit1);
                            myFieldSet.Controls.Add(txt);

                        }



                    }

                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim() + ":";
                        lbl.Width = 150;
                     

                        Label txt = new Label();
                        txt.ID = "txt" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]);
                        txt.CssClass = "Label";
                      


                        txt.Text = strComment;
                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(lbl);
                            myFieldSet.Controls.Add(txt);
                        }
                       

                    }




                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]).ToUpper() == "Label".ToUpper())
                    {
                        Label lblCaption = new Label();
                        lblCaption.CssClass = "lblCaption1";
                        lblCaption.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim();

                       
                       // myFieldSet.Controls.Add(lblCaption);



                    }

                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]) == "DropDownList")
                    {

                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim() + ":    ";
                        lbl.Width = 150;
                      

                        DropDownList DrpList = new DropDownList();
                        DrpList.ID = "drp" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();
                        DrpList.Width = 200;
                        DrpList.Enabled = false;
                        string strRadFieldName = Convert.ToString(DR1["IASM_DATA"]);
                        string[] arrRadFieldName = strRadFieldName.Split('|');


                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            DrpList.Items.Add(new ListItem(arrRadFieldName[intRad], arrRadFieldName[intRad]));

                        }

                        DrpList.CssClass = "lblCaption1";

                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            if (DrpList.Items[intRad].Value == strComment)
                            {
                                DrpList.SelectedValue = strComment;
                            }

                        }
                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(lbl);
                            myFieldSet.Controls.Add(DrpList);
                        }

                    }




                    if (Convert.ToString(DR1["IASM_CONTROL_TYPE"]) == "RadioButtonList")
                    {

                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IASM_FIELD_NAME"]).Trim() + ":    ";
                        lbl.Width = 150;
                      

                        RadioButtonList RadList = new RadioButtonList();
                        RadList.ID = "radl" + Convert.ToString(DR1["IASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IASM_FIELD_ID"]).Trim();
                        RadList.Enabled = false;
                        RadList.RepeatDirection = RepeatDirection.Horizontal;
                        RadList.Width = Convert.ToInt32(DR1["IASM_WIDTH"]);
                        string strRadFieldName = Convert.ToString(DR1["IASM_DATA"]);
                        string[] arrRadFieldName = strRadFieldName.Split('|');


                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            RadList.Items.Add(new ListItem(arrRadFieldName[intRad], arrRadFieldName[intRad]));

                        }

                        RadList.CssClass = "lblCaption1";

                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            if (RadList.Items[intRad].Value == strComment)
                            {
                                RadList.SelectedValue = strComment;
                            }

                        }
                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(lbl);
                            myFieldSet.Controls.Add(RadList);
                        }

                    }
                    if (strComment.Trim() != "")
                    {
                           boolFieldSet = true;
                        Literal lit = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                        myFieldSet.Controls.Add(lit);
                    }

                }



                if (Position == "LEFT")
                {
                    if (boolFieldSet == true)
                    {
                        plhoPhyAssessmentLeft.Controls.Add(myFieldSet);
                    }
                }
                else
                {
                    if (boolFieldSet == true)
                    {
                        plhoPhyAssessmentRight.Controls.Add(myFieldSet);
                    }
                }






                i = i + 1;



            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }



        public void generateDynamicControls()
        {

            CreateSegCtrls();



        }
    }
}