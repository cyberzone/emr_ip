﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CCHDReport.ascx.cs" Inherits="EMR_IP.WebReports.CCHDReport" %>

<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1">Admission Nursing Assessment</span>
<br />

<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" style="width: 150px">Age at Initial Screening
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">


            <asp:Label ID="txtAge" runat="server" CssClass="label"></asp:Label>
            Hours
                  

        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1" colspan="5" style="font-weight: bold;">Initial Screening:
        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Time
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Pulse Ox Saturation of Right Hand
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Pulse Ox Saturation of Foot 
        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">Difference (right hand – foot)
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle"></td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:Label ID="txtIniDate" runat="server" Width="70px" CssClass="label"> </asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtINI_PULSE_OX_SATU_RHAND" runat="server" CssClass="lblCaption1"></asp:Label>
            %
                   

        </td>


        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtINI_PULSE_OX_SATU_FOOT" runat="server" CssClass="lblCaption1"></asp:Label>
            %
                     

        </td>


        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtINI_DIFF_RHAND_FOOT" runat="server" CssClass="lblCaption1"></asp:Label>
            %
        </td>


        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:RadioButtonList ID="radINI_DIFF_RESULT" runat="server" CssClass="lblCaption1" Width="100px" RepeatDirection="Horizontal" Enabled="false">
                <asp:ListItem Value="Pass" Text="Pass"></asp:ListItem>
                <asp:ListItem Value="Fail" Text="Fail"></asp:ListItem>

            </asp:RadioButtonList>


        </td>
    </tr>

    <tr>
        <td class="lblCaption1" colspan="5" style="font-weight: bold;">Second Screening (1 hour following initial screen if fail initial screen)
        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Time
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Pulse Ox Saturation of Right Hand
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Pulse Ox Saturation of Foot 
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Difference (right hand – foot)
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle"></td>
    </tr>

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtSecDate" runat="server" Width="70px" CssClass="label"  ></asp:Label>


        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtSEC_PULSE_OX_SATU_RHAND" runat="server" CssClass="label" ></asp:Label>
            %
                   

        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtSEC_PULSE_OX_SATU_FOOT" runat="server" CssClass="label" ></asp:Label>
            %
                   

        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtSEC_DIFF_RHAND_FOOT" runat="server" CssClass="lblCaption1"></asp:Label>
            %
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:RadioButtonList ID="radSEC_DIFF_RESULT" runat="server" CssClass="lblCaption1" Width="100px" RepeatDirection="Horizontal" Enabled="false">
                <asp:ListItem Value="Pass" Text="Pass"></asp:ListItem>
                <asp:ListItem Value="Fail" Text="Fail"></asp:ListItem>

            </asp:RadioButtonList>


        </td>
    </tr>

    <tr>
        <td class="lblCaption1" colspan="5" style="font-weight: bold;">Third Screening (1 hour following second screening if fail second screen)
        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Time
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Pulse Ox Saturation of Right Hand
        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">Pulse Ox Saturation of Foot 
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Difference (right hand – foot)
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle"></td>
    </tr>
    <tr>

        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtThiDate" runat="server" CssClass="lblCaption1"></asp:Label>


        </td>


        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtTHI_PULSE_OX_SATU_RHAND" runat="server" CssClass="lblCaption1"></asp:Label>
            %
                   

        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtTHI_PULSE_OX_SATU_FOOT" runat="server" CssClass="lblCaption1"></asp:Label>
            %
                  

        </td>


        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtTHI_DIFF_RHAND_FOOT" runat="server" CssClass="lblCaption1"></asp:Label>
            %

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:RadioButtonList ID="radTHI_DIFF_RESULT" runat="server" CssClass="lblCaption1" Width="100px" RepeatDirection="Horizontal" Enabled="false">
                <asp:ListItem Value="Pass" Text="Pass"></asp:ListItem>
                <asp:ListItem Value="Fail" Text="Fail"></asp:ListItem>

            </asp:RadioButtonList>


        </td>
    </tr>

</table>
