﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;
namespace EMR_IP.WebReports
{
    public partial class CCHDReport : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

        CommonBAL objCom = new CommonBAL();
        IP_CongenitaHeartDiseaseScreening objCong = new IP_CongenitaHeartDiseaseScreening();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("CCHDReport." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindCCHD()
        {
            objCong = new IP_CongenitaHeartDiseaseScreening();
            DataSet DS = new DataSet();

            string Criteria = " 1=1 and ICHDS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND ICHDS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = objCong.CongHearDiseaseScreeGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    txtAge.Text = Convert.ToString(DR["ICHDS_AGE"]);
                    txtIniDate.Text = Convert.ToString(DR["ICHDS_INI_TIME_DateDesc"]);



                    txtINI_PULSE_OX_SATU_RHAND.Text = Convert.ToString(DR["ICHDS_INI_PULSE_OX_SATU_RHAND"]);
                    txtINI_PULSE_OX_SATU_FOOT.Text = Convert.ToString(DR["ICHDS_INI_PULSE_OX_SATU_FOOT"]);
                    txtINI_DIFF_RHAND_FOOT.Text = Convert.ToString(DR["ICHDS_INI_DIFF_RHAND_FOOT"]);
                    radINI_DIFF_RESULT.SelectedValue = Convert.ToString(DR["ICHDS_INI_DIFF_RESULT"]);



                    txtSecDate.Text = Convert.ToString(DR["ICHDS_SEC_TIME_DateDesc"]);


                    txtSEC_PULSE_OX_SATU_RHAND.Text = Convert.ToString(DR["ICHDS_SEC_PULSE_OX_SATU_RHAND"]);
                    txtSEC_PULSE_OX_SATU_FOOT.Text = Convert.ToString(DR["ICHDS_SEC_PULSE_OX_SATU_FOOT"]);
                    txtSEC_DIFF_RHAND_FOOT.Text = Convert.ToString(DR["ICHDS_SEC_DIFF_RHAND_FOOT"]);
                    radSEC_DIFF_RESULT.SelectedValue = Convert.ToString(DR["ICHDS_SEC_DIFF_RESULT"]);



                    txtThiDate.Text = Convert.ToString(DR["ICHDS_THI_TIME_DateDesc"]);




                    txtTHI_PULSE_OX_SATU_RHAND.Text = Convert.ToString(DR["ICHDS_THI_PULSE_OX_SATU_RHAND"]);
                    txtTHI_PULSE_OX_SATU_FOOT.Text = Convert.ToString(DR["ICHDS_THI_PULSE_OX_SATU_FOOT"]);
                    txtTHI_DIFF_RHAND_FOOT.Text = Convert.ToString(DR["ICHDS_THI_DIFF_RHAND_FOOT"]);
                    radTHI_DIFF_RESULT.SelectedValue = Convert.ToString(DR["ICHDS_THI_DIFF_RESULT"]);

                }
            }


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {


                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}