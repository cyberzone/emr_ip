﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;
using System.Text;
using System.IO;

namespace EMR_IP.WebReports
{
    public partial class DischargeStatement : System.Web.UI.Page
    {
        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }


        DataSet DS = new DataSet();
        CommonBAL objCom = new CommonBAL();
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

        string BindDepID(string DeptName)
        {
            string DeptID = "";
            DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND HDM_DEP_NAME='" + DeptName + "'";

            DS = objCom.DepMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DeptID = Convert.ToString(DS.Tables[0].Rows[0]["HDM_DEP_ID"]);
            }
            return DeptID;
        }

        public StringBuilder strData = new StringBuilder();

        Boolean BindAdmissionSummary()
        {
            DataSet DS = new DataSet();
            IP_AdmissionSummary obj = new IP_AdmissionSummary();
            string Criteria = " 1=1 ";
            Criteria += " and IAS_ADMISSION_NO='" + Convert.ToString(Session["IAS_ADMISSION_NO"]) + "'";

            DS = obj.GetIPAdmissionSummary(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("IAS_DEPARTMENT") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_DEPARTMENT"]) != "")
                    lblDept.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_DEPARTMENT"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_DATE"]) != "")
                {
                    lblAdmissionDate.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_DATEDesc"]));
                    ViewState["AdmissionDate"] = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_DATE"]));

                }

                if (DS.Tables[0].Rows[0].IsNull("IAS_PT_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_NAME"]) != "")
                    lblPTFullName.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_NAME"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_PT_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_ID"]) != "")
                    lblFileNo.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_ID"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_NATIONALITY") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_NATIONALITY"]) != "")
                    lblNationality.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_NATIONALITY"]));



                if (DS.Tables[0].Rows[0].IsNull("IAS_AGE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_AGE"]) != "")
                    lblAge.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_AGE"]));
                //lblAgeType.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE_TYPE"]));
                //lblAge1.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE1"]));
                //lblAgeType1.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE_TYPE1"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_MOBILE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_MOBILE"]) != "")
                    lblMobile.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_MOBILE"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_IP_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_IP_ID"]) != "")
                    lblEPMID.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_IP_ID"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_SEX") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_SEX"]) != "")
                    lblSex.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_SEX"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_INS_COMP_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_INS_COMP_NAME"]) != "")
                    lblInsCo.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_INS_COMP_NAME"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_POLICY_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_TYPE"]) != "")
                    lblPolicyType.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_TYPE"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_POLICY_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_NO"]) != "")
                    lblPolicyNo.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_NO"]));


                if (DS.Tables[0].Rows[0].IsNull("IAS_DR_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_NAME"]) != "")
                    lblDrName.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_NAME"]));

                lblDrCode.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_ID"]));

                //if (DS.Tables[0].Rows[0].IsNull("HPM_IQAMA_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_IQAMA_NO"]) != "")
                //    lblEmiratesID.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_IQAMA_NO"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_PT_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_TYPE"]) != "")
                    lblVisitType.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_TYPE"]));




                lblAdmissionNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_NO"]);
                lblAdmissionType.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_TYPE"]);
                lblAddmissionMode.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMN_MODE"]);




                //lblNationality.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["IAS_NATIONALITY"]);

                //lblFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_ID"]);
                //lblDoctor.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_ID"]) + "-" + Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_NAME"]);
                //lblPolicyType.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_TYPE"]);


                //lblAge.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_AGE"]);
                //lblSex.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_SEX"]);
                //lblAdmissionDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateDesc"]) + " " + Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateTimeDesc"]);
                //lblPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_NO"]);

                //lblMobile.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_MOBILE"]);
                //lblDOB.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DOBDesc"]);
                //lblProviderName.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_INSURANCE_COMPANY"]);


                ////lblRoom.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_MOBILE"]);
                ////lblBead.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DOB"]);
                ////lblWard.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_INSURANCE_COMPANY"]);

                ////OtherAdmissionSummary.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_OTHERS"]);
                ////ProcedurePlanned.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PROCEDURE_PLANNED"]);
                ////Anesthesia.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ANESTHESIA"]);
                ////Treatment.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_TREATMENT"]);


                ////ReasonforAdmission.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REASON_FOR_ADMISSION"]);
                ////Remarks.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REMARKS"]);
                return true;



                return true;
            }
            else
            {

                return false;
            }
        }

        void BindDischargeSummary()
        {
            IP_DischargeSummary objDis = new IP_DischargeSummary();
            string Criteria = " 1=1 and IDS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IDS_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";
            DataSet DS = new DataSet();

            DS = objDis.DischargeSummaryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    lblDischDate.Text = Convert.ToString(DR["IDS_DATEEDesc"]);
                    lblDischTime.Text = Convert.ToString(DR["IDS_DATE_TIMEDesc"]);


                }

            }

        }

        void BindDays()
        {
            objCom = new CommonBAL();
           // DateTime strFromDate = Convert.ToDateTime(objCom.fnGetDate("MM/dd/yyyy"));

            DateTime dtAdmissionDate = Convert.ToDateTime(Convert.ToString(ViewState["AdmissionDate"]));
            DateTime Today;
           // Today = strFromDate;
            if (lblDischDate.Text != "")
            {
                Today = Convert.ToDateTime(lblDischDate.Text);
                TimeSpan Diff = Today.Date - dtAdmissionDate.Date;

                lblNoOfDays.Text = Convert.ToString(Diff.Days);
            }

           
        }

        void GetAmount(string Criteria, out decimal decAmount)
        {
            decAmount = 0;

            string stQry = "SELECT CAST(SUM(HIT_AMOUNT) AS DECIMAL(18,2) ) as TotalAmount    FROM HMS_INVOICE_MASTER HIM INNER JOIN   " +
                " HMS_INVOICE_TRANSACTION HIT ON HIM_INVOICE_ID = HIT_INVOICE_ID  INNER JOIN " +
                " HMS_SERVICE_MASTER ON HIT_SERV_CODE = HSM_SERV_ID   WHERE  " + Criteria + " GROUP by  HSM_GROUP  ";

            // HIM_EMR_ID=4 and HIT_TRANS_DATE='2016-01-05 00:00:00.000' and  HSM_GROUP='ANESTH' 
            DataSet DS = new DataSet();
            DS = objCom.fnGetFieldValue(stQry);
            if (DS.Tables[0].Rows.Count > 0)
            {

                decAmount = Convert.ToDecimal(DS.Tables[0].Rows[0]["TotalAmount"]);

            }

        }

        void BindTable()
        {


            string stQry = "SELECT  CONVERT(varchar, HIT_TRANS_DATE,103) as TransDateDesc,CONVERT(DATETIME,HIT_TRANS_DATE,101) as TransDate    FROM HMS_INVOICE_MASTER HIM INNER JOIN " +
                            " HMS_INVOICE_TRANSACTION HIT ON HIM_INVOICE_ID = HIT_INVOICE_ID  INNER JOIN " +
                            " HMS_SERVICE_MASTER ON HIT_SERV_CODE = HSM_SERV_ID WHERE HIM_EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + " GROUP by  HIT_TRANS_DATE  ";

            objCom = new CommonBAL();

            DataSet DSDate = new DataSet();
            DSDate = objCom.fnGetFieldValue(stQry);

            strData.Append("<Table border='1' style='border-color:black;width:100%' ><tr>");
            strData.Append("<td width='250px'     class='Header' >");
            strData.Append("Day");
            strData.Append("</td>");
            foreach (DataRow DRDate in DSDate.Tables[0].Rows)
            {
                strData.Append("<td width='250px'     class='Header' >");
                strData.Append(Convert.ToString(DRDate["TransDateDesc"]));
                strData.Append("</td>");

            }

            strData.Append("<td width='250px'     class='Header' >");
            strData.Append("TOTAL");
            strData.Append("</td>");
            strData.Append("</tr>");

            DataSet DS = new DataSet();
            objCom = new CommonBAL();
            string CriteraiMast = " 1=1";
            CriteraiMast += " AND EM_ACTIVE=1 AND EM_TYPE='DISC_STATMENT_TYPE' ";
            DS = objCom.EMR_MasterGet(CriteraiMast);

            Decimal decGrandTotal = 0, decGrandAdvance = 0, decGrandBalance = 0;
            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                strData.Append("<tr>");
                strData.Append("<td width='250px'     class='RowStyle'  >");
                strData.Append(Convert.ToString(DR["EM_NAME"]));
                strData.Append("</td>");

                Decimal decTotal = 0;
                foreach (DataRow DRDate1 in DSDate.Tables[0].Rows)
                {
                    string strStartDate = Convert.ToString(DRDate1["TransDateDesc"]);
                    string[] arrDate = strStartDate.Split('/');
                    string strForStartDate = "";

                    if (arrDate.Length > 1)
                    {
                        strForStartDate = arrDate[2] + "-" + arrDate[1] + "-" + arrDate[0];
                    }

                    Decimal decAmount = 0;


                    string Criteria = " 1=1 ";
                    Criteria += " AND HIM_EMR_ID=" + Convert.ToString(Session["EMR_ID"]) + "  AND CONVERT(datetime,convert(varchar(10), HIT_TRANS_DATE,101),101)='" + strForStartDate + "' AND  HSM_GROUP='" + Convert.ToString(DR["EM_CODE"]) + "'";
                    GetAmount(Criteria, out decAmount);

                    strData.Append("<td width='250px'     class='RowStyleRight' >");
                    strData.Append("<span class='Label'  >" + decAmount + "</span>");
                    strData.Append("</td>");
                    decTotal = decTotal + decAmount;
                }
                decGrandTotal = decGrandTotal + decTotal;

                strData.Append("<td width='250px'     class='RowStyleRight' style='font-weight:bold;' >");
                strData.Append("<span class=Label >" + decTotal + "</span>");
                strData.Append("</td>");

                strData.Append("</tr>");
            }
            //---------------------------------
            strData.Append("<tr>");
            strData.Append("<td width='250px'     class='RowStyle' >");
            strData.Append("TOTAL AMOUNT:");
            strData.Append("</td>");


            foreach (DataRow DRDate1 in DSDate.Tables[0].Rows)
            {


                strData.Append("<td width='250px'     class='Header' >");
                strData.Append("<span class=Label ></span>");
                strData.Append("</td>");

            }

            strData.Append("<td width='250px'     class='RowStyleRight' style='font-weight:bold;' >");
            strData.Append("<span class=Label>" + decGrandTotal + "</span>");
            strData.Append("</td>");

            strData.Append("</tr>");
            //---------------------------------//

            //---------------------------------
            strData.Append("<tr>");
            strData.Append("<td width='250px'     class='RowStyle' >");
            strData.Append("LESS:  Advance");
            strData.Append("</td>");


            foreach (DataRow DRDate1 in DSDate.Tables[0].Rows)
            {


                strData.Append("<td width='250px'     class='Header' >");
                strData.Append("<span class=Label ></span>");
                strData.Append("</td>");

            }

            strData.Append("<td width='250px'     class='RowStyleRight' style='font-weight:bold;' >");
            strData.Append("<span class=Label>" + decGrandAdvance + "</span>");
            strData.Append("</td>");

            strData.Append("</tr>");
            //---------------------------------//


            //---------------------------------

            decGrandBalance = decGrandTotal - decGrandAdvance;

            strData.Append("<tr>");
            strData.Append("<td width='250px'     class='RowStyle' >");
            strData.Append("BALANCE AMOUNT:");
            strData.Append("</td>");


            foreach (DataRow DRDate1 in DSDate.Tables[0].Rows)
            {


                strData.Append("<td width='250px'     class='Header' >");
                strData.Append("<span class=Label ></span>");
                strData.Append("</td>");

            }

            strData.Append("<td width='250px'     class='RowStyleRight' style='font-weight:bold;' >");
            strData.Append("<span class=Label>" + decGrandBalance + "</span>");
            strData.Append("</td>");

            strData.Append("</tr>");
            //---------------------------------//


            strData.Append("</Table>");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                EMR_ID = Convert.ToString(Request.QueryString["EMR_ID"]);
                EMR_PT_ID = Convert.ToString(Request.QueryString["EMR_PT_ID"]);

                DR_ID = Convert.ToString(Request.QueryString["DR_ID"]);
                IAS_ADMISSION_NO = Convert.ToString(Request.QueryString["ADMISSION_NO"]);

                lblAdmissionNo.Text = IAS_ADMISSION_NO;
                // BindEMRPTMaster();
                BindAdmissionSummary();
                BindDischargeSummary();
                BindDays();
                BindTable();

            }

        }
    }
}