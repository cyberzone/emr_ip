﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DischargeSummaryReport.ascx.cs" Inherits="EMR_IP.WebReports.DischargeSummaryReport" %>



<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1">DischargeS ummary</span>


<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" style="width:200px;">
            Principal Diagnosis</td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="txtPrincDiag" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
          Additional Diagnosis</td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="txtAdditDiag" runat="server" CssClass="label"></asp:Label>

        </td>
    </tr>

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
          Reason for Admission</td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="ReasonforAdmission" runat="server" CssClass="lblCaption1"></asp:Label>


        </td>
    </tr>

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
           Significant Findings</td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="txtSignificantFindings" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
    </tr>

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
            Operations and Other Procedures Performed</td>

        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="txtProcedute" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
    </tr>

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
           In – hospital Treatment Including Medications</td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="txtTreatmentMedic" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
    </tr>
</table>


<span class="lblCaption1">Discharge Details </span>


<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" style="width:200px;">Discharge Date
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="txtDisDate" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Condition at Discharge or Transfer from Hospital
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="txtCondition" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Discharge Instructions
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="txtInstructions" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>


    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Physical Activity
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="txtPhyActivity" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Diet
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="txtDiet" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>

    </tr>

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Follow Up Care
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="txtFollowUpCare" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Discharge Medications
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="txtMedications" runat="server" CssClass="label"></asp:Label>

        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Instructions on safe and effective use of medication given
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:RadioButtonList ID="radInstrMedication" runat="server" CssClass="lblCaption1" Width="100px" RepeatDirection="Horizontal" Enabled="false">
                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                <asp:ListItem Text="No" Value="False"></asp:ListItem>
            </asp:RadioButtonList>
        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Information on potential instructions between medication and food given
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:RadioButtonList ID="radInfoMedicFood" runat="server" CssClass="lblCaption1" Width="100px" RepeatDirection="Horizontal" Enabled="false">
                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                <asp:ListItem Text="No" Value="False"></asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Type of Transportation
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="txtTransType" runat="server" CssClass="label" Width="200px"></asp:Label>

        </td>

    </tr>

</table>
