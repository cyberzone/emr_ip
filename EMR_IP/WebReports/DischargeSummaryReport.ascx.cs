﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.WebReports
{
    public partial class DischargeSummaryReport : System.Web.UI.UserControl
    {

        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

        IP_DischargeSummary objDis = new IP_DischargeSummary();


        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("DischargeSummaryReport." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindData()
        {
            objDis = new IP_DischargeSummary();
            string Criteria = " 1=1 and IDS_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IDS_ID='" + EMR_ID + "'";
            DataSet DS = new DataSet();

            DS = objDis.DischargeSummaryGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    txtDisDate.Text = Convert.ToString(DR["IDS_DATEEDesc"]);

                    txtCondition.Text = Convert.ToString(DR["IDS_CONDITION"]);
                    txtInstructions.Text = Convert.ToString(DR["IDS_INSTRUCTIONS"]);
                    txtPhyActivity.Text = Convert.ToString(DR["IDS_PHY_ACTIVITY"]);
                    txtDiet.Text = Convert.ToString(DR["IDS_DIET"]);
                    txtFollowUpCare.Text = Convert.ToString(DR["IDS_FOLLOWUPCARE"]);
                    txtMedications.Text = Convert.ToString(DR["IDS_MEDICATIONS"]);

                    radInstrMedication.SelectedValue = Convert.ToString(DR["IDS_INSTR_MEDICATION"]);
                    radInfoMedicFood.SelectedValue = Convert.ToString(DR["IDS_INFO_MEDIC_FOOD"]);

                    txtTransType.Text = Convert.ToString(DR["IDS_TRANSPOR_TYPE"]);

                }

            }

        }

        Boolean BindAdmissionSummary()
        {
            DataSet DS = new DataSet();
            IP_AdmissionSummary obj = new IP_AdmissionSummary();
            string Criteria = " 1=1 ";
            Criteria += " and IAS_ADMISSION_NO='" + IAS_ADMISSION_NO + "'";

            DS = obj.GetIPAdmissionSummary(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                // txtInpatientNo.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_NO"]);

                //  AdmissionDateTime.Value = Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateTime"]);
                //  ProvisionalDiagnosis.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PROVISIONAL_DIAGNOSIS"]);
                // AttendingPhysician.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ATTENDING_PHYSICIAN"]);
                //Laboratory.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_LABORATORY"]);
                // Radiological.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_RADIOLOGICAL"]);
                // OtherAdmissionSummary.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_OTHERS"]);
                // ProcedurePlanned.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PROCEDURE_PLANNED"]);
                // Anesthesia.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ANESTHESIA"]);
                //  Treatment.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_TREATMENT"]);
                //ReferralPhysician.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REFERRAL_PHYSICIAN"]);

                // radAdmissionType.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_TYPE"]);
                ReasonforAdmission.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REASON_FOR_ADMISSION"]);
                // Remarks.Value = Convert.ToString(DS.Tables[0].Rows[0]["IAS_REMARKS"]);
                return true;
            }
            else
            {

                return false;
            }
        }

        void BindDiagnosis()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND EPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND EPD_ID='" + EMR_ID + "'";


            DataSet DS = new DataSet();
            EMR_PTDiagnosis objDiag = new EMR_PTDiagnosis();
            DS = objDiag.DiagnosisGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    if (Convert.ToString(DR["EPD_TYPE"]).ToUpper() == "Principal".ToUpper())
                    {
                        txtPrincDiag.Text = Convert.ToString(DR["EPD_DIAG_CODE"]).Trim() + " ~ " + Convert.ToString(DR["EPD_DIAG_NAME"]).Trim();
                    }
                    else
                    {
                        if (txtAdditDiag.Text == "")
                        {
                            txtAdditDiag.Text = Convert.ToString(DR["EPD_DIAG_CODE"]).Trim() + " ~ " + Convert.ToString(DR["EPD_DIAG_NAME"]).Trim();
                        }
                        else
                        {
                            txtAdditDiag.Text += "\n" + Convert.ToString(DR["EPD_DIAG_CODE"]).Trim() + "~" + Convert.ToString(DR["EPD_DIAG_NAME"]).Trim();
                        }

                    }

                }


            }

        }

        void BindProcedure()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND IPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPP_ID='" + EMR_ID + "'";



            DataSet DS = new DataSet();
            IP_PTProcedure objPro = new IP_PTProcedure();
            DS = objPro.IPProceduresGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {


                    if (txtAdditDiag.Text == "")
                    {
                        txtProcedute.Text = Convert.ToString(DR["IPP_PRO_CODE"]).Trim() + " ~ " + Convert.ToString(DR["IPP_PRO_NAME"]).Trim();
                    }
                    else
                    {
                        txtProcedute.Text += "</br>" + Convert.ToString(DR["IPP_PRO_CODE"]).Trim() + " ~ " + Convert.ToString(DR["IPP_PRO_NAME"]).Trim();
                    }


                }
            }


        }

        public void BindDischargeSummary()
        {


                    BindAdmissionSummary();
                    BindDiagnosis();
                    BindProcedure();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                   



                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}