﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EducationalFormReport.ascx.cs" Inherits="EMR_IP.WebReports.EducationalFormReport" %>

 <input type="hidden" id="EDU_PART1_SAVE_PT_ID" runat="server" value="FALSE" />
<table style="width:100%">
     <tr>
          <td class="lblCaption1"   >
              Educational Form
          </td>
       </tr>
    <tr>
        <td>
            <table style="width:100%">
                <tr>
                    <tr>
                        <td style=" border: 1px solid #dcdcdc;height:25px;"><label for="Language" class="lblCaption1" >Patient's language</label>
                            <span>: </span>
                            <label id="lbllanguage" class="lblCaption1" runat="server" ></label>
                        </td>
                        <td class="lblCaption1"  style=" border: 1px solid #dcdcdc;height:25px;"><label class="lblCaption1" >Patient's literacy</label>
                            <input type="checkbox" name="Read" id="chkRead" value="1" checked="" disabled="disabled" class="lblCaption1" runat="server" > Read
                            <input type="checkbox" name="Write" id="chkWrite" value="1" checked="" disabled="disabled" class="lblCaption1" runat="server" > Write
                            <input type="checkbox" name="Speak" id="chkSpeak" value="1" checked="" disabled="disabled" class="lblCaption1" runat="server" > Speak
                        </td>
                        <td style=" border: 1px solid #dcdcdc;height:25px;"><label for="ReligionBelief" class="lblCaption1" >Religion/Belief</label>
                            <span>: </span>
                             <label id="lblReligion" class="lblCaption1" runat="server" ></label>
                        </td>
                    </tr>
                </tr>
            </table>
        </td>
    </tr>
   
      <tr>
           <td style="height:20px;">
                <asp:label id="lblEduFormDiet" cssclass="lblCaption1"  runat="server"></asp:label>
               
           </td>
              </tr>
</table>