﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;
namespace EMR_IP.WebReports
{
    public partial class FluidBalancechartReport : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

        IP_FluidBalancechart objFluid = new IP_FluidBalancechart();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("FluidBalancechartReport." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

       public  void BindFluidBalancechart()
        {
            DataSet DS = new DataSet();
            objFluid = new IP_FluidBalancechart();



            string Criteria = " 1=1 and IFBC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IFBC_ID='" + EMR_ID + "'";

            DS = objFluid.FluidBalanceChartGet(Criteria);


            gvFluidBalance.DataBind();
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvFluidBalance.DataSource = DS;
                gvFluidBalance.DataBind();

            }

        }

        void CalculateTotalScore()
        {
            Decimal intTotalInput = 0, intTotalOutPut = 0;

            string strResult = "Positive ";
            for (int intCurRow = 0; intCurRow < gvFluidBalance.Rows.Count; intCurRow++)
            {
                Label lblgvOral = (Label)gvFluidBalance.Rows[intCurRow].FindControl("lblgvOral");
                Label lblgvIV = (Label)gvFluidBalance.Rows[intCurRow].FindControl("lblgvIV");
                Label lblgvRouteOther = (Label)gvFluidBalance.Rows[intCurRow].FindControl("lblgvRouteOther");

                Label lblgvUrine = (Label)gvFluidBalance.Rows[intCurRow].FindControl("lblgvUrine");
                Label lblgvVomit = (Label)gvFluidBalance.Rows[intCurRow].FindControl("lblgvVomit");
                Label lblgvNGAspiration = (Label)gvFluidBalance.Rows[intCurRow].FindControl("lblgvNGAspiration");
                Label lblgvOutputOther = (Label)gvFluidBalance.Rows[intCurRow].FindControl("lblgvOutputOther");

                if (lblgvOral.Text.Trim() != "")
                {
                    intTotalInput += Convert.ToDecimal(lblgvOral.Text);

                }
                if (lblgvIV.Text.Trim() != "")
                {
                    intTotalInput += Convert.ToDecimal(lblgvIV.Text);
                }

                if (lblgvRouteOther.Text.Trim() != "")
                {
                    intTotalInput += Convert.ToDecimal(lblgvRouteOther.Text);
                }



                if (lblgvUrine.Text.Trim() != "")
                {
                    intTotalOutPut += Convert.ToDecimal(lblgvUrine.Text);
                }

                if (lblgvVomit.Text.Trim() != "")
                {
                    intTotalOutPut += Convert.ToDecimal(lblgvVomit.Text);
                }
                if (lblgvNGAspiration.Text.Trim() != "")
                {
                    intTotalOutPut += Convert.ToDecimal(lblgvNGAspiration.Text);
                }

                if (lblgvOutputOther.Text.Trim() != "")
                {
                    intTotalOutPut += Convert.ToDecimal(lblgvOutputOther.Text);
                }
            }
            TextBox txtgvTotalInput = (TextBox)gvFluidBalance.FooterRow.FindControl("txtgvTotalInput");
            TextBox txtgvTotalOutput = (TextBox)gvFluidBalance.FooterRow.FindControl("txtgvTotalOutput");
            TextBox txtgvResult = (TextBox)gvFluidBalance.FooterRow.FindControl("txtgvResult");


            txtgvTotalInput.Text = Convert.ToString(intTotalInput);
            txtgvTotalOutput.Text = Convert.ToString(intTotalOutPut);
            txtgvResult.Text = strResult;





        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            try
            {
                if (!IsPostBack)
                {

                    
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}