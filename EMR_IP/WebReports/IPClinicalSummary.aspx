﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="IPClinicalSummary.aspx.cs" Inherits="EMR_IP.WebReports.IPClinicalSummary" %>


<%@ Register Src="~/WebReports/AdmissionRequestReport.ascx" TagPrefix="UC1" TagName="AdmissionRequest" %>

<%@ Register Src="~/WebReports/IPDiagnosisReport.ascx" TagPrefix="UC1" TagName="IPDiagnosis" %>
<%@ Register Src="~/WebReports/IPRadiologyReport.ascx" TagPrefix="UC1" TagName="IPRadiology" %>
<%@ Register Src="~/WebReports/IPLaboratoryReport.ascx" TagPrefix="UC1" TagName="IPLaboratory" %>
<%@ Register Src="~/WebReports/IPProcedureReport.ascx" TagPrefix="UC1" TagName="IPProcedure" %>



<%@ Register Src="~/WebReports/IPVitalSignReport.ascx" TagPrefix="UC1" TagName="IPVitalSign" %>
<%@ Register Src="~/WebReports/IPPrescriptionsReport.ascx" TagPrefix="UC1" TagName="IPPrescriptions" %>
<%@ Register Src="~/WebReports/IVFluidReport.ascx" TagPrefix="UC1" TagName="IVFluid" %>

<%@ Register Src="~/WebReports/RecoveryChartReport.ascx" TagPrefix="UC1" TagName="RecoveryChart" %>

<%@ Register Src="~/WebReports/AnesthesiaReport.ascx" TagPrefix="UC1" TagName="Anesthesia" %>

<%@ Register Src="~/WebReports/PreAnesthesiaReport.ascx" TagPrefix="UC1" TagName="PreAnesthesia" %>

<%@ Register Src="~/WebReports/OperationNotesReport.ascx" TagPrefix="UC1" TagName="OperationNotes" %>

<%@ Register Src="~/WebReports/SiteVerificationReport.ascx" TagPrefix="UC1" TagName="SiteVerification" %>

<%@ Register Src="~/WebReports/EducationalFormReport.ascx" TagPrefix="UC1" TagName="EducationalForm" %>

<%@ Register Src="~/WebReports/PainAssessmentReport.ascx" TagPrefix="UC1" TagName="PainAssessment" %>

<%@ Register Src="~/WebReports/PreOperativeChecklistReport.ascx" TagPrefix="UC1" TagName="PreOperativeChecklist" %>

<%@ Register Src="~/WebReports/NursingCarePlanReport.ascx" TagPrefix="UC1" TagName="NursingCarePlan" %>

<%@ Register Src="~/WebReports/FluidBalancechartReport.ascx" TagPrefix="UC1" TagName="FluidBalancechart" %>

<%@ Register Src="~/WebReports/DischargeSummaryReport.ascx" TagPrefix="UC1" TagName="DischargeSummary" %>

<%@ Register Src="~/WebReports/PatientMonitoringReport.ascx" TagPrefix="UC1" TagName="PatientMonitoring" %>

<%@ Register Src="~/WebReports/IntraOperativeNursingReport.ascx" TagPrefix="UC1" TagName="IntraOperativeNursing" %>

<%@ Register Src="~/WebReports/OTConsumablesReport.ascx" TagPrefix="UC1" TagName="OTConsumables" %>

<%@ Register Src="~/WebReports/AdmissionNursingAssessmentReport.ascx" TagPrefix="UC1" TagName="AdmissionNursingAssessment" %>

<%@ Register Src="~/WebReports/CCHDReport.ascx" TagPrefix="UC1" TagName="CCHD" %>

<%@ Register Src="~/WebReports/InitialAssessmentReport.ascx" TagPrefix="UC1" TagName="InitialAssessment" %>

<%@ Register Src="~/WebReports/IPInvoiceReport.ascx" TagPrefix="UC1" TagName="IPInvoice" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
     

    <script language="javascript" type="text/javascript">

        function ShowPhySchedule() {

            document.getElementById("divPhyScedule").style.display = 'block';
            HidegvPhyShedule();

        }

        function HidePhyScedule() {

            document.getElementById("divPhyScedule").style.display = 'none';

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">

    <input type="hidden" id="hidEMRDeptID" runat="server" />
    <input type="hidden" id="hidEMRDeptName" runat="server" />

    <table style="width: 100%; text-align: center; vertical-align: top;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img style="padding: 1px; height: 70px; border: none;" src="images/Report_Logo.PNG" />
            </td>
        </tr>
    </table>


    <div style="padding-bottom: 2px; border-bottom: 4px solid #92c500; float: left; font-size: 1.2em; color: #2078c0;">
        <span style="font-family: Segoe UI, Arial, Helvetica, sans-serif;">Clinical Summary</span>
    </div>
    <br />
    <br />
    <div    style="padding-left: 40%; width: 100%;">
        <div id="divPhyScedule" style="display: block; border: groove; height: 200px; width: 80%; background-color: white; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #eee9e9; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; left: 100px; top: 0px;">
            <table style="width: 100%">
                <tr>
                    <td  style="text-align: right">
                         <input type="button" id="btnMsgClose" class="ButtonStyle"  color: black; border: none;" value=" X " onclick="HidePhyScedule()" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:CheckBoxList ID="chkScreens1" runat="server" RepeatDirection="Horizontal" CssClass="lblCaption1" Width="750px">
                            <asp:ListItem Text="AdmissionRequest" Value="AdmissionRequest"></asp:ListItem>
                            <asp:ListItem Text="IPVitalSign" Value="IPVitalSign"></asp:ListItem>
                            <asp:ListItem Text="IPLaboratory" Value="IPLaboratory"></asp:ListItem>
                            <asp:ListItem Text="IPRadiology" Value="IPRadiology"></asp:ListItem>
                            <asp:ListItem Text="IPDiagnosis" Value="IPDiagnosis"></asp:ListItem>
                            <asp:ListItem Text="IPProcedure" Value="IPProcedure"></asp:ListItem>
                        </asp:CheckBoxList>
                        <asp:CheckBoxList ID="chkScreens2" runat="server" RepeatDirection="Horizontal" CssClass="lblCaption1" Width="600px">
                            <asp:ListItem Text="IPPrescriptions" Value="IPPrescriptions"></asp:ListItem>
                            <asp:ListItem Text="IVFluid" Value="IVFluid"></asp:ListItem>
                            <asp:ListItem Text="RecoveryChart" Value="RecoveryChart"></asp:ListItem>
                            <asp:ListItem Text="Anesthesia" Value="Anesthesia"></asp:ListItem>
                            <asp:ListItem Text="PreAnesthesia" Value="PreAnesthesia"></asp:ListItem>
                        </asp:CheckBoxList>
                        <asp:CheckBoxList ID="chkScreens3" runat="server" RepeatDirection="Horizontal" CssClass="lblCaption1" Width="700px">
                            <asp:ListItem Text="OperationNotes" Value="OperationNotes"></asp:ListItem>
                            <asp:ListItem Text="SiteVerification" Value="SiteVerification"></asp:ListItem>
                            <asp:ListItem Text="EducationalForm" Value="EducationalForm"></asp:ListItem>
                            <asp:ListItem Text="PainAssessment" Value="PainAssessment"></asp:ListItem>
                            <asp:ListItem Text="PreOperativeChecklist" Value="PreOperativeChecklist"></asp:ListItem>
                        </asp:CheckBoxList>
                        <asp:CheckBoxList ID="chkScreens4" runat="server" RepeatDirection="Horizontal" CssClass="lblCaption1" Width="700px">
                            <asp:ListItem Text="NursingCarePlan" Value="NursingCarePlan"></asp:ListItem>
                            <asp:ListItem Text="FluidBalancechart" Value="FluidBalancechart"></asp:ListItem>
                            <asp:ListItem Text="DischargeSummary" Value="DischargeSummary"></asp:ListItem>
                            <asp:ListItem Text="PatientMonitoring" Value="PatientMonitoring"></asp:ListItem>
                            <asp:ListItem Text="IntraOperativeNursing" Value="IntraOperativeNursing"></asp:ListItem>
                        </asp:CheckBoxList>
                        <asp:CheckBoxList ID="chkScreens5" runat="server" RepeatDirection="Horizontal" CssClass="lblCaption1" Width="700px">
                            <asp:ListItem Text="OTConsumables" Value="OTConsumables"></asp:ListItem>
                            <asp:ListItem Text="AdmissionNursingAssessment" Value="AdmissionNursingAssessment"></asp:ListItem>
                            <asp:ListItem Text="CCHD" Value="CCHD"></asp:ListItem>
                            <asp:ListItem Text="InitialAssessment" Value="InitialAssessment"></asp:ListItem>
                            <asp:ListItem Text="IPInvoice" Value="IPInvoice"></asp:ListItem>

                        </asp:CheckBoxList>

                    </td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        <asp:Button ID="btnShowReport" runat="server" Text="Show Report" CssClass="button orange small" OnClick="btnShowReport_Click" OnClientClick="HidePhyScedule()" />
                    </td>
                </tr>
            </table>

        </div>
    </div>



    <table style="width: 100%; border: 1px solid #dcdcdc" class="gridspacy">
        <tr>
            <td class="lblCaption1 BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">Department:
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblDept" CssClass="lblCaption1" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">Date:
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblEmrDate" CssClass="lblCaption1" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">Admission# 
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblAdmissionNo" CssClass="lblCaption1" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">Admission Type
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblAdmissionType" CssClass="lblCaption1" runat="server"></asp:Label>
            </td>

        </tr>
    </table>

    <table width="100%; border: 1px solid #dcdcdc" class="gridspacy">
        <tr>
            <td class="lblCaption1 BoldStyle" colspan="4" style="border: 1px solid #dcdcdc; height: 25px;">Patient Full Name:<asp:Label ID="lblPTFullName" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Admission Mode:<asp:Label ID="lblAddmissionMode" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>&nbsp;  
                
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">File No:<asp:Label ID="lblFileNo" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Nationality:<asp:Label ID="lblNationality" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Age:<asp:Label ID="lblAge" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>&nbsp;  
                
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Phone No:<asp:Label ID="lblMobile" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Visit No:<asp:Label ID="lblEPMID" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Sex:<asp:Label ID="lblSex" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>&nbsp;  
                 
            </td>
        </tr>

        <tr>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Ins. Co.:<asp:Label ID="lblInsCo" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Policy Type:<asp:Label ID="lblPolicyType" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Policy No:<asp:Label ID="lblPolicyNo" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>&nbsp;  
                 
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Doctor Name:<asp:Label ID="lblDrName" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
                <asp:Label ID="lblDrCode" CssClass="lblCaption1 BoldStyle" runat="server" Visible="false"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;"></td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Visit Type:<asp:Label ID="lblVisitType" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>&nbsp;  
                 
            </td>
        </tr>
    </table>
    <br />

    <UC1:AdmissionRequest ID="ucAdmissionRequestReport" runat="server" Visible="false"></UC1:AdmissionRequest>
    <UC1:IPVitalSign ID="ucIPVitalSignReport" runat="server" Visible="false"></UC1:IPVitalSign>
    <UC1:IPLaboratory ID="ucIPLaboratoryReport" runat="server" Visible="false"></UC1:IPLaboratory>
    <UC1:IPRadiology ID="ucIPRadiologyReport" runat="server" Visible="false"></UC1:IPRadiology>
    <UC1:IPDiagnosis ID="ucIPDiagnosisReport" runat="server" Visible="false"></UC1:IPDiagnosis>
    <UC1:IPProcedure ID="ucIPProcedureReport" runat="server" Visible="false"></UC1:IPProcedure>


    <UC1:IPPrescriptions ID="ucIPPrescriptionsReport" runat="server" Visible="false"></UC1:IPPrescriptions>
    <UC1:IVFluid ID="ucIVFluidReport" runat="server" Visible="false"></UC1:IVFluid>
    <UC1:RecoveryChart ID="ucRecoveryChartReport" runat="server" Visible="false"></UC1:RecoveryChart>
    <UC1:Anesthesia ID="ucAnesthesiaReport" runat="server" Visible="false"></UC1:Anesthesia>
    <UC1:PreAnesthesia ID="ucPreAnesthesiaReport" runat="server" Visible="false"></UC1:PreAnesthesia>

    <UC1:OperationNotes ID="ucOperationNotesReport" runat="server" Visible="false"></UC1:OperationNotes>
    <UC1:SiteVerification ID="ucSiteVerificationReport" runat="server" Visible="false"></UC1:SiteVerification>
    <UC1:EducationalForm ID="ucEducationalFormReport" runat="server" Visible="false"></UC1:EducationalForm>
    <UC1:PainAssessment ID="ucPainAssessmentReport" runat="server" Visible="false"></UC1:PainAssessment>
    <UC1:PreOperativeChecklist ID="ucPreOperativeChecklistReport" runat="server" Visible="false"></UC1:PreOperativeChecklist>

    <UC1:NursingCarePlan ID="ucNursingCarePlanReport" runat="server" Visible="false"></UC1:NursingCarePlan>
    <UC1:FluidBalancechart ID="ucFluidBalancechartReport" runat="server" Visible="false"></UC1:FluidBalancechart>
    <UC1:DischargeSummary ID="ucDischargeSummaryReport" runat="server" Visible="false"></UC1:DischargeSummary>
    <UC1:PatientMonitoring ID="ucPatientMonitoringReport" runat="server" Visible="false"></UC1:PatientMonitoring>
    <UC1:IntraOperativeNursing ID="ucIntraOperativeNursingReport" runat="server" Visible="false"></UC1:IntraOperativeNursing>

    <UC1:OTConsumables ID="ucOTConsumablesReport" runat="server" Visible="false"></UC1:OTConsumables>
    <UC1:AdmissionNursingAssessment ID="ucAdmissionNursingAssessmentReport" runat="server" Visible="false"></UC1:AdmissionNursingAssessment>
    <UC1:CCHD ID="ucCCHDReport" runat="server" Visible="false"></UC1:CCHD>
    <UC1:InitialAssessment ID="ucInitialAssessmentReport" runat="server" Visible="false"></UC1:InitialAssessment>
    <UC1:IPInvoice ID="ucIPInvoiceReport" runat="server" Visible="false"></UC1:IPInvoice>


   


</asp:Content>
