﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.WebReports
{
    public partial class IPClinicalSummary : System.Web.UI.Page
    {
        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }


        DataSet DS = new DataSet();
        CommonBAL objCom = new CommonBAL();
        EMR_PTMasterBAL objEmrPTMast = new EMR_PTMasterBAL();

        string BindDepID(string DeptName)
        {
            string DeptID = "";
            DS = new DataSet();
            objCom = new CommonBAL();
            string Criteria = " 1=1 ";
            Criteria += " AND HDM_DEP_NAME='" + DeptName + "'";

            DS = objCom.DepMasterGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                DeptID = Convert.ToString(DS.Tables[0].Rows[0]["HDM_DEP_ID"]);
            }
            return DeptID;
        }


        Boolean BindAdmissionSummary()
        {
            DataSet DS = new DataSet();
            IP_AdmissionSummary obj = new IP_AdmissionSummary();
            string Criteria = " 1=1 ";
            Criteria += " and IAS_ADMISSION_NO='" + Convert.ToString(ViewState["IAS_ADMISSION_NO"]) + "'";

            DS = obj.GetIPAdmissionSummary(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                if (DS.Tables[0].Rows[0].IsNull("IAS_DEPARTMENT") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_DEPARTMENT"]) != "")
                    lblDept.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_DEPARTMENT"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_DATE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_DATE"]) != "")
                    lblEmrDate.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_DATEDesc"]));



                if (DS.Tables[0].Rows[0].IsNull("IAS_PT_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_NAME"]) != "")
                    lblPTFullName.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_NAME"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_PT_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_ID"]) != "")
                    lblFileNo.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_ID"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_NATIONALITY") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_NATIONALITY"]) != "")
                    lblNationality.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_NATIONALITY"]));



                if (DS.Tables[0].Rows[0].IsNull("IAS_AGE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_AGE"]) != "")
                    lblAge.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_AGE"]));
                //lblAgeType.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE_TYPE"]));
                //lblAge1.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE1"]));
                //lblAgeType1.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_AGE_TYPE1"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_MOBILE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_MOBILE"]) != "")
                    lblMobile.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_MOBILE"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_IP_ID") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_IP_ID"]) != "")
                    lblEPMID.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_IP_ID"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_SEX") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_SEX"]) != "")
                    lblSex.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_SEX"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_INS_COMP_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_INS_COMP_NAME"]) != "")
                    lblInsCo.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_INS_COMP_NAME"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_POLICY_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_TYPE"]) != "")
                    lblPolicyType.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_TYPE"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_POLICY_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_NO"]) != "")
                    lblPolicyNo.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_NO"]));


                if (DS.Tables[0].Rows[0].IsNull("IAS_DR_NAME") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_NAME"]) != "")
                    lblDrName.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_NAME"]));

                lblDrCode.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_ID"]));

                //if (DS.Tables[0].Rows[0].IsNull("HPM_IQAMA_NO") == false && Convert.ToString(DS.Tables[0].Rows[0]["HPM_IQAMA_NO"]) != "")
                //    lblEmiratesID.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["HPM_IQAMA_NO"]));

                if (DS.Tables[0].Rows[0].IsNull("IAS_PT_TYPE") == false && Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_TYPE"]) != "")
                    lblVisitType.Text = CommonBAL.ConvertToTxt(Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_TYPE"]));




                lblAdmissionNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_NO"]);
                lblAdmissionType.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMISSION_TYPE"]);
                lblAddmissionMode.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_ADMN_MODE"]);




                //lblNationality.ToolTip = Convert.ToString(DS.Tables[0].Rows[0]["IAS_NATIONALITY"]);

                //lblFileNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_PT_ID"]);
                //lblDoctor.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_ID"]) + "-" + Convert.ToString(DS.Tables[0].Rows[0]["IAS_DR_NAME"]);
                //lblPolicyType.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_TYPE"]);


                //lblAge.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_AGE"]);
                //lblSex.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_SEX"]);
                //lblAdmissionDate.Text = Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateDesc"]) + " " + Convert.ToString(DS.Tables[0].Rows[0]["AdmissionDateTimeDesc"]);
                //lblPolicyNo.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_POLICY_NO"]);

                //lblMobile.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_MOBILE"]);
                //lblDOB.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DOBDesc"]);
                //lblProviderName.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_INSURANCE_COMPANY"]);


                ////lblRoom.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_MOBILE"]);
                ////lblBead.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_DOB"]);
                ////lblWard.Text = Convert.ToString(DS.Tables[0].Rows[0]["IAS_INSURANCE_COMPANY"]);

                return true;
            }
            else
            {

                return false;
            }
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                ViewState["EMR_ID"] = Convert.ToString(Request.QueryString["EMR_ID"]);
                ViewState["EMR_PT_ID"] = Convert.ToString(Request.QueryString["EMR_PT_ID"]);

                ViewState["DR_ID"] = Convert.ToString(Request.QueryString["DR_ID"]);
                ViewState["IAS_ADMISSION_NO"] = Convert.ToString(Request.QueryString["ADMISSION_NO"]);

                lblAdmissionNo.Text = Convert.ToString(ViewState["IAS_ADMISSION_NO"]);// IAS_ADMISSION_NO;
                // BindEMRPTMaster();
                BindAdmissionSummary();


            }

        }

        protected void btnShowReport_Click(object sender, EventArgs e)
        {
            ucAdmissionRequestReport.Visible = false;
            ucIPVitalSignReport.Visible = false;
            ucIPLaboratoryReport.Visible = false;
            ucIPRadiologyReport.Visible = false;
            ucIPDiagnosisReport.Visible = false;
            ucIPProcedureReport.Visible = false;

            ucIPPrescriptionsReport.Visible = false;
            ucIVFluidReport.Visible = false;
            ucRecoveryChartReport.Visible = false;
            ucAnesthesiaReport.Visible = false;
            ucPreAnesthesiaReport.Visible = false;

            ucOperationNotesReport.Visible = false;
            ucSiteVerificationReport.Visible = false;
            ucEducationalFormReport.Visible = false;
            ucPainAssessmentReport.Visible = false;
            ucPreOperativeChecklistReport.Visible = false;

            ucNursingCarePlanReport.Visible = false;
            ucFluidBalancechartReport.Visible = false;
            ucDischargeSummaryReport.Visible = false;
            ucPatientMonitoringReport.Visible = false;
            ucIntraOperativeNursingReport.Visible = false;

            ucOTConsumablesReport.Visible = false;
            ucAdmissionNursingAssessmentReport.Visible = false;
            ucCCHDReport.Visible = false;
            ucInitialAssessmentReport.Visible = false;
            ucIPInvoiceReport.Visible = false;


            for (Int32 i = 0; i < chkScreens1.Items.Count; i++)
            {

                if (chkScreens1.Items[i].Selected == true)
                {

                    if (chkScreens1.Items[i].Text == "AdmissionRequest")
                    {
                        ucAdmissionRequestReport.Visible = true;
                        ucAdmissionRequestReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucAdmissionRequestReport.IAS_ADMISSION_NO = Convert.ToString(ViewState["IAS_ADMISSION_NO"]);
                        ucAdmissionRequestReport.BindAdmissionSummary();

                    }


                    if (chkScreens1.Items[i].Text == "IPVitalSign")
                    {
                        ucIPVitalSignReport.Visible = true;
                        ucIPVitalSignReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucIPVitalSignReport.BindVital();

                    }



                    if (chkScreens1.Items[i].Text == "IPLaboratory")
                    {
                        ucIPLaboratoryReport.Visible = true;
                        ucIPLaboratoryReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucIPLaboratoryReport.BindLaboratoryRequest();

                    }

                    if (chkScreens1.Items[i].Text == "IPRadiology")
                    {
                        ucIPRadiologyReport.Visible = true;
                        ucIPRadiologyReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucIPRadiologyReport.BindRadiologyRequest();

                    }

                    if (chkScreens1.Items[i].Text == "IPDiagnosis")
                    {
                        ucIPDiagnosisReport.Visible = true;
                        ucIPDiagnosisReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucIPDiagnosisReport.BindDiagnosis();

                    }

                    if (chkScreens1.Items[i].Text == "IPProcedure")
                    {
                        ucIPProcedureReport.Visible = true;
                        ucIPProcedureReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucIPProcedureReport.BindProcedure();

                    }
                }
            }

            for (Int32 i = 0; i < chkScreens2.Items.Count; i++)
            {

                if (chkScreens2.Items[i].Selected == true)
                {
                    if (chkScreens2.Items[i].Text == "IPPrescriptions")
                    {
                        ucIPPrescriptionsReport.Visible = true;
                        ucIPPrescriptionsReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucIPPrescriptionsReport.BindPharmacy();

                    }

                    if (chkScreens2.Items[i].Text == "IVFluid")
                    {
                        ucIVFluidReport.Visible = true;
                        ucIVFluidReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucIVFluidReport.BindIVFluid();

                    }

                    if (chkScreens2.Items[i].Text == "RecoveryChart")
                    {
                        ucRecoveryChartReport.Visible = true;
                        ucRecoveryChartReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucRecoveryChartReport.BindRecoveryChart();

                    }



                    if (chkScreens2.Items[i].Text == "Anesthesia")
                    {
                        ucAnesthesiaReport.Visible = true;
                        ucAnesthesiaReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucAnesthesiaReport.BindAnesthesia();

                    }


                    if (chkScreens2.Items[i].Text == "PreAnesthesia")
                    {
                        ucPreAnesthesiaReport.Visible = true;
                        ucPreAnesthesiaReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        // ucPreAnesthesiaReport.BindAnesthesia();

                    }

                         }
            }

            for (Int32 i = 0; i < chkScreens3.Items.Count; i++)
            {

                 if (chkScreens3.Items[i].Selected == true)
                {
                    if (chkScreens3.Items[i].Text == "OperationNotes")
                    {
                        ucOperationNotesReport.Visible = true;
                        ucOperationNotesReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucOperationNotesReport.BindOperationNotes();

                    }

                    if (chkScreens3.Items[i].Text == "SiteVerification")
                    {
                        ucSiteVerificationReport.Visible = true;
                        ucSiteVerificationReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucSiteVerificationReport.BindSiteVerification();

                    }


                    if (chkScreens3.Items[i].Text == "EducationalForm")
                    {
                        ucEducationalFormReport.Visible = true;
                        ucEducationalFormReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucEducationalFormReport.BindEMR_EDUCATIONALFORM();
                        ucEducationalFormReport.SegmentLoad();

                    }

                    if (chkScreens3.Items[i].Text == "PainAssessment")
                    {
                        ucPainAssessmentReport.Visible = true;
                        ucPainAssessmentReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucPainAssessmentReport.BindPainAssessmentData();

                    }

                    if (chkScreens3.Items[i].Text == "PreOperativeChecklist")
                    {
                        ucPreOperativeChecklistReport.Visible = true;
                        ucPreOperativeChecklistReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucPreOperativeChecklistReport.BindPreOperativeChecklistData();

                    }



                    }
            }



            for (Int32 i = 0; i < chkScreens4.Items.Count; i++)
            {
                if (chkScreens4.Items[i].Selected == true)
                {

                    if (chkScreens4.Items[i].Text == "NursingCarePlan")
                    {
                        ucNursingCarePlanReport.Visible = true;
                        ucNursingCarePlanReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucNursingCarePlanReport.BindNursingCarePlan();


                    }

                    if (chkScreens4.Items[i].Text == "FluidBalancechart")
                    {
                        ucFluidBalancechartReport.Visible = true;
                        ucFluidBalancechartReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucFluidBalancechartReport.BindFluidBalancechart();

                    }

                    if (chkScreens4.Items[i].Text == "DischargeSummary")
                    {
                        ucDischargeSummaryReport.Visible = true;
                        ucDischargeSummaryReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucDischargeSummaryReport.IAS_ADMISSION_NO = Convert.ToString(ViewState["IAS_ADMISSION_NO"]);

                        ucDischargeSummaryReport.BindDischargeSummary();

                    }

                    if (chkScreens4.Items[i].Text == "PatientMonitoring")
                    {
                        ucPatientMonitoringReport.Visible = true;
                        ucPatientMonitoringReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucPatientMonitoringReport.BindPatientMonitoring();

                    }



                    if (chkScreens4.Items[i].Text == "IntraOperativeNursing")
                    {
                        ucIntraOperativeNursingReport.Visible = true;
                        ucIntraOperativeNursingReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucIntraOperativeNursingReport.IAS_ADMISSION_NO = Convert.ToString(ViewState["IAS_ADMISSION_NO"]);

                        ucIntraOperativeNursingReport.BindIntraOperativeNursing();

                    }


                }
            }



            for (Int32 i = 0; i < chkScreens5.Items.Count; i++)
            {
                if (chkScreens5.Items[i].Selected == true)
                {

                    if (chkScreens5.Items[i].Text == "OTConsumables")
                    {
                        ucOTConsumablesReport.Visible = true;
                        ucOTConsumablesReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucOTConsumablesReport.BindOTConsumables();

                    }



                    if (chkScreens5.Items[i].Text == "AdmissionNursingAssessment")
                    {
                        ucAdmissionNursingAssessmentReport.Visible = true;
                        ucAdmissionNursingAssessmentReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucAdmissionNursingAssessmentReport.BindAdmissionNursingAssessment();

                    }



                    if (chkScreens5.Items[i].Text == "CCHD")
                    {
                        ucCCHDReport.Visible = true;
                        ucCCHDReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucCCHDReport.BindCCHD();

                    }


                    if (chkScreens5.Items[i].Text == "InitialAssessment")
                    {
                        ucInitialAssessmentReport.Visible = true;
                        ucInitialAssessmentReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucInitialAssessmentReport.BindInitialAssessment();

                    }


                    if (chkScreens5.Items[i].Text == "IPInvoice")
                    {
                        ucIPInvoiceReport.Visible = true;
                        ucIPInvoiceReport.EMR_ID = Convert.ToString(ViewState["EMR_ID"]);
                        ucIPInvoiceReport.BindInvoiceDetails();

                    }

                }
            }

        }
    }
}