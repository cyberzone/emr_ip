﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;
namespace EMR_IP.WebReports
{
    public partial class IPDiagnosisReport : System.Web.UI.UserControl
    {

        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

        IP_PTDiagnosis objDiag = new IP_PTDiagnosis();

      public  void BindDiagnosis()
        {
            string Criteria = " 1=1 ";
            //  Criteria += " AND IPD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPD_ID='" + EMR_ID + "'";


            DataSet DS = new DataSet();

            DS = objDiag.IPDiagnosisGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();

                DataColumn Type = new DataColumn();
                Type.ColumnName = "Type";

                DS.Tables[0].Columns.Add(Type);

                Int32 i = 0;
                foreach (DataRow DR in DS.Tables[0].Rows)
                {


                    if (i == 0)
                    {
                        DR["Type"] = "Principal Diagnosis:";
                    }
                    else
                    {
                        DR["Type"] = "Secondary Diagnosis:";

                    }

                    i = i + 1;
                }

                DS.Tables[0].AcceptChanges();
                gvDiagnosis.DataSource = DS;
                gvDiagnosis.DataBind();



            }
            else
            {
                gvDiagnosis.DataBind();
            }
        FunEnd: ;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {

               // BindDiagnosis();
            }
        }
    }
}