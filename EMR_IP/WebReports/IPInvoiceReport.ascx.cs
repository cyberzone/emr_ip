﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;


namespace EMR_IP.WebReports
{
    public partial class IPInvoiceReport : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

        public string strClaimDetails;

        CommonBAL objCom = new CommonBAL();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("IPInvoiceReport." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        void BuildeInvoiceClaims()
        {

            DataSet ds = new DataSet();

            DataTable dt = new DataTable();

            DataColumn HIC_INVOICE_ID = new DataColumn();
            HIC_INVOICE_ID.ColumnName = "HIC_INVOICE_ID";

            DataColumn HIC_ICD_CODE = new DataColumn();
            HIC_ICD_CODE.ColumnName = "HIC_ICD_CODE";

            DataColumn HIC_ICD_DESC = new DataColumn();
            HIC_ICD_DESC.ColumnName = "HIC_ICD_DESC";


            DataColumn HIC_ICD_TYPE = new DataColumn();
            HIC_ICD_TYPE.ColumnName = "HIC_ICD_TYPE";


            dt.Columns.Add(HIC_INVOICE_ID);
            dt.Columns.Add(HIC_ICD_CODE);
            dt.Columns.Add(HIC_ICD_DESC);
            dt.Columns.Add(HIC_ICD_TYPE);

            ViewState["InvoiceClaims"] = dt;
        }

        void BindInvoiceClaimDtls(string InvoiceID)
        {
            string Criteria = " 1=1 ";

            // Criteria += "   AND  HIC_INVOICE_ID in (select HIM_INVOICE_ID  FROM HMS_INVOICE_MASTER WHERE    HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'   AND  HIM_EMR_ID='" + EMR_ID + "')";//"       AND  HIC_INVOICE_ID='" + txtInvoiceNo.Text.Trim() + "'";

            Criteria += " AND  HIC_INVOICE_ID='" + InvoiceID + "'";

            IP_InvoiceClaims objInvCla = new IP_InvoiceClaims();
            DataSet DS = new DataSet();
            DS = objInvCla.InvoiceClaimDtlsGet(Criteria);


            if (DS.Tables[0].Rows.Count > 0)
            {
                DataTable DT = new DataTable();
                for (int i = 0; i < DS.Tables[0].Rows.Count; i++)
                {
                    DT = (DataTable)ViewState["InvoiceClaims"];

                    DataRow objrow;

                  

                    if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_P") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_P"]) != "")
                    {
                        objrow = DT.NewRow();

                        objrow["HIC_INVOICE_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_INVOICE_ID"]);
                        //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                        //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                        //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                        //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                        //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);

                        objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_P"]);
                        objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_PDesc"]);

                        objrow["HIC_ICD_TYPE"] = "Principal";


                        DT.Rows.Add(objrow);
                    }

                    if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_A") == false && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_A"]) != "")
                    {
                        objrow = DT.NewRow();
                        objrow["HIC_INVOICE_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_INVOICE_ID"]);
                        //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                        //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                        //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                        //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                        //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);


                        objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_A"]);
                        objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_ADesc"]);
                        objrow["HIC_ICD_TYPE"] = "Admitting";
                        DT.Rows.Add(objrow);

                    }

                    for (int j = 1; j <= 30; j++)
                    {
                        if (DS.Tables[0].Columns.Contains("HIC_ICD_CODE_S" + j) == true && Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j]) != "")
                        {

                            if (DS.Tables[0].Rows[i].IsNull("HIC_ICD_CODE_S" + j) == false)
                            {
                                objrow = DT.NewRow();
                                objrow["HIC_INVOICE_ID"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_INVOICE_ID"]);
                                //objrow["HIC_TYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_TYPE"]);
                                //objrow["HIC_STARTDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTDATE"]);
                                //objrow["HIC_ENDDATE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDDATE"]);
                                //objrow["HIC_STARTTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_STARTTYPE"]);
                                //objrow["HIC_ENDTYPE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ENDTYPE"]);

                                objrow["HIC_ICD_CODE"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j]);
                                objrow["HIC_ICD_DESC"] = Convert.ToString(DS.Tables[0].Rows[i]["HIC_ICD_CODE_S" + j + "Desc"]);
                                objrow["HIC_ICD_TYPE"] = "Secondary";
                                DT.Rows.Add(objrow);

                            }
                        }
                    }




                }

                ViewState["InvoiceClaims"] = DT;
                BIndClaims();
            }
        }



        void BIndClaims()
        {
            string strHeader = "";

            DataTable DT = new DataTable();

            DT = (DataTable)ViewState["InvoiceClaims"];

          
            if (DT.Rows.Count > 0)
            {
                strHeader += "<table cellpadding='0' cellspacing='0' style='width:100%;' >";
                strHeader += "<tr><td style='height:10px' class='lblCaption1' >Claim Details</td></tr>";
                strHeader += "<tr>";
                strHeader += "<td   style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1 BoldStyle BorderStyle' colspan='2' >Invoice ID</td>";
                strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1 BoldStyle BorderStyle' colspan='2' >Code</td>";
                strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1 BoldStyle BorderStyle' colspan='2' >Description</td>";
                strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1 BoldStyle BorderStyle' colspan='2' >Type</td>";
                strHeader += "</tr>";

                foreach (DataRow DR in DT.Rows)
                {



                    strHeader += "<tr>";
                    strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1  BorderStyle' colspan='2' >" + Convert.ToString(DR["HIC_INVOICE_ID"]) + "</td>";
                    strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1  BorderStyle' colspan='2' >" + Convert.ToString(DR["HIC_ICD_CODE"]) + "</td>";
                    strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1  BorderStyle' colspan='2' >" + Convert.ToString(DR["HIC_ICD_DESC"]) + "</td>";
                    strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1  BorderStyle' colspan='2' >" + Convert.ToString(DR["HIC_ICD_TYPE"]) + "</td>";
                    strHeader += "</tr>";


                }
                strHeader += "</table";
                strHeader += "</br>";
                strClaimDetails += strHeader;
            }

        }


        void BIndTransaction(string InvoiceID)
        {
            string strHeader = "";
            string Criteria = "1=1";
            Criteria += " AND HIT_INVOICE_ID='"+   InvoiceID +"'";

            IP_Invoice objInv = new IP_Invoice();
            DataSet DS = new DataSet();

            DS = objInv.IPInvoiceTransGet(Criteria);

          
            if (DS.Tables[0].Rows.Count > 0)
            {
                strHeader += "<table cellpadding='0' cellspacing='0' style='width:100%;' >";
                strHeader += "<tr><td style='height:10px' class='lblCaption1' >Transaction Details</td></tr>";
                strHeader += "<tr>";
                strHeader += "<td   style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1 BoldStyle BorderStyle' colspan='2' >Invoice ID</td>";
                strHeader += "<td   style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1 BoldStyle BorderStyle' colspan='2' >Trans. Date</td>";
                strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1 BoldStyle BorderStyle' colspan='2' >Code</td>";
                strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1 BoldStyle BorderStyle' colspan='2' >Description</td>";
                strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1 BoldStyle BorderStyle' colspan='2' >Qty</td>";
                strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1 BoldStyle BorderStyle' colspan='2' >Amount</td>";

                strHeader += "</tr>";

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    strHeader += "<tr>";
                    strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1  BorderStyle' colspan='2' >" + Convert.ToString(DR["HIT_INVOICE_ID"]) + "</td>";
                    strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1  BorderStyle' colspan='2' >" + Convert.ToString(DR["HIT_TRANS_DATEDesc"]) + "</td>";
                    strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1  BorderStyle' colspan='2' >" + Convert.ToString(DR["HIT_SERV_CODE"]) + "</td>";
                    strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1  BorderStyle' colspan='2' >" + Convert.ToString(DR["HIT_Description"]) + "</td>";
                    strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1  BorderStyle' colspan='2' >" + Convert.ToString(DR["HIT_QTY"]) + "</td>";
                    strHeader += "<td style=' border: 1px solid #dcdcdc;height:25px;width:200px;' class='lblCaption1  BorderStyle' colspan='2' >" + Convert.ToString(DR["HIT_AMOUNT"]) + "</td>";

                    strHeader += "</tr>";


                }
                strHeader += "<tr><td style='height:10px;' ></td></tr>";
                strHeader += "</table";
                strClaimDetails += strHeader;
            }

        }

      public  void BindInvoiceDetails()
        {
            string Criteria = " 1=1 ";

            Criteria += " AND  HIM_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'   AND  HIM_EMR_ID='" + EMR_ID + "'";
            DataSet DS = new DataSet();
            IP_Invoice objInv = new IP_Invoice();

            DS = objInv.InvoiceMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                string strInvoiceID = "";
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    strInvoiceID = Convert.ToString(DR["HIM_INVOICE_ID"]);
                    BuildeInvoiceClaims();
                    BindInvoiceClaimDtls(strInvoiceID);
                    BIndTransaction(strInvoiceID);
                }

            }


        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                    

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}