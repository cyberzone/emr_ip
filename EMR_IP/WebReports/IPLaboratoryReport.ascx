﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IPLaboratoryReport.ascx.cs" Inherits="EMR_IP.WebReports.IPLaboratoryReport" %>

 <table   style="width:100%;" >
                                   <tr>
                                      <td class="lblCaption1" >
                                       Laboratory  
                                      </td>
                                  </tr>
                                   <tr>
                                       <td class="lblCaption1  BoldStyle">
                                         <asp:GridView ID="gvLabRequest" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                            EnableModelValidation="True" Width="100%">
                                            <HeaderStyle CssClass="lblCaption1"  Font-Bold="true"/>
                                            <RowStyle CssClass="lblCaption1" />
                            
                                            <Columns>
                                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                            <asp:Label ID="Label27" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPL_LAB_CODE") %>'  ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description"  HeaderStyle-Width="90%">
                                                    <ItemTemplate>
                                                            <asp:Label ID="Label28" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPL_LAB_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Price" HeaderStyle-Width="10%" Visible="false">
                                                    <ItemTemplate>
                                                            <asp:Label ID="Label29" CssClass="lblCaption1" runat="server" Text="0.00"  ></asp:Label>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                 
                                    </Columns>
                                </asp:GridView>
                                   </td>
                               </tr>
                               </table>