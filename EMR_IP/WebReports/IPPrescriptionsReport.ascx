﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IPPrescriptionsReport.ascx.cs" Inherits="EMR_IP.WebReports.IPPrescriptionsReport" %>


<table width="100%">

    <tr>
        <td class="lblCaption1  BoldStyle">
           <asp:GridView ID="gvPharmacy" runat="server" AllowSorting="True" AutoGenerateColumns="False"  
                EnableModelValidation="True" Width="100%" >
                <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
                <RowStyle CssClass="lblCaption1"  HorizontalAlign="Center" />

                <Columns>
                    <asp:TemplateField HeaderText="Drug Code"  HeaderStyle-Width="100px"  >
                        <ItemTemplate>
                          
                        <%# Eval("IPP_PHY_CODE")  %>

                        </ItemTemplate>

                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Product Name"  HeaderStyle-Width="100px"  >
                        <ItemTemplate>
                          
                        <%# Eval("ProductName")  %>

                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description"  >
                        <ItemTemplate>
                      
                        <%# Convert.ToString(Eval("IPP_PHY_NAME")).Length > 100 ? Convert.ToString(Eval("IPP_PHY_NAME")).Substring(0,100) : Eval("IPP_PHY_NAME") %>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Dosage/Time/Route"  HeaderStyle-Width="100px"  >
                        <ItemTemplate>
                          
                      <%# Eval("IPP_DOSAGE1") %> &nbsp;
                      <%# Eval("IPP_DOSAGE") %> &nbsp; 
                      <%# Eval("IPP_ROUTEDesc") %> &nbsp;

                        </ItemTemplate>

                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Duration" HeaderStyle-Width="100px" >
                        <ItemTemplate>

                               <%# Eval("IPP_DURATION") %> &nbsp; <%# Eval("IPP_DURATION_TYPEDesc") %> 

                        </ItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Qty" HeaderStyle-Width="50px" >
                        <ItemTemplate>

                             <%# Eval("IPP_QTY") %> 

                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Refil" Visible="false">
                        <ItemTemplate>

                             <%# Eval("IPP_REFILL") %> 

                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Remarks" HeaderStyle-Width="200px" >
                        <ItemTemplate>

                             <%# Eval("IPP_REMARKS") %> 

                        </ItemTemplate>

                    </asp:TemplateField>
                    
                     <asp:TemplateField HeaderText="Taken"  >
                        <ItemTemplate>

                             <%# Eval("IPP_TAKENDESC") %> 

                        </ItemTemplate>

                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>