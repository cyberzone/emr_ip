﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;
namespace EMR_IP.WebReports
{
    public partial class IPPrescriptionsReport : System.Web.UI.UserControl
    {

        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }


        public void BindPharmacy()
        {
            string Criteria = " 1=1 ";
            //Criteria += " AND IPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPP_ID='" + EMR_ID + "'";
 


            DataSet DS = new DataSet();
            IP_PTPharmacy objPro = new IP_PTPharmacy();


            DS = objPro.IPPharmacyGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvPharmacy.DataSource = DS;
                gvPharmacy.DataBind();

            }
            else
            {
                gvPharmacy.DataBind();
            }
        FunEnd: ;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
             if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

             if (!IsPostBack)
             {

                 
             }
        }
    }
}