﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IPProcedureReport.ascx.cs" Inherits="EMR_IP.WebReports.IPProcedureReport" %>


<table style="width: 100%" class="gridspacy" >
                    <tr>
                                      <td class="lblCaption1"  >
                                    Procedure
                                      </td>
                                  </tr>
                      <tr>
                       
                        <td valign="top" class="lblCaption1  BoldStyle">
                          
                        <asp:GridView ID="gvProcedure" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                            EnableModelValidation="True"  Width="100%">
                            <HeaderStyle CssClass="lblCaption1"   Font-Bold="true"/>
                            <RowStyle CssClass="lblCaption1" />
                            
                            <Columns>
                                <asp:TemplateField HeaderText="Code" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                           
                                            <asp:Label ID="lblDiagCode" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPP_PRO_CODE") %>'  ></asp:Label>
                                          
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description"  HeaderStyle-Width="60%">
                                    <ItemTemplate>
                                         
                                            <asp:Label ID="lblDiagName" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPP_PRO_NAME") %>'></asp:Label>
                                             
                                    </ItemTemplate>

                                </asp:TemplateField>
                               

                                 <asp:TemplateField HeaderText="Price" HeaderStyle-Width="10%" Visible="false">
                                    <ItemTemplate>
                                       
                                            <asp:Label ID="lblPrice" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPP_PRICE") %>'></asp:Label>
                                           
                                    </ItemTemplate>

                                </asp:TemplateField>
                                 
                                 <asp:TemplateField HeaderText="Procedure Remarks" HeaderStyle-Width="40%">
                                    <ItemTemplate>
                                       
                                            <asp:Label ID="Label3" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPP_REMARKS") %>'></asp:Label>
                                           
                                    </ItemTemplate>

                                </asp:TemplateField>
                                
                    </Columns>

                </asp:GridView>
                        </td>
                    </tr>

                </table>