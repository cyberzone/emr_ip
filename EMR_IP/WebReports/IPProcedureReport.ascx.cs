﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;
namespace EMR_IP.WebReports
{
    public partial class IPProcedureReport : System.Web.UI.UserControl
    {

        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

        IP_PTProcedure objPTProc = new IP_PTProcedure();


      public  void BindProcedure()
        {
            string Criteria = " 1=1 ";
            // Criteria += " AND EPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPP_ID='" + EMR_ID + "'";

            DataSet DS = new DataSet();
            CommonBAL objWebrpt = new CommonBAL();
            DS = objPTProc.IPProceduresGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvProcedure.DataSource = DS;
                gvProcedure.DataBind();
            }
            else
            {
                gvProcedure.DataBind();
            }




        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {

               // BindProcedure();
            }
        }
    }
}