﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.WebReports
{
    public partial class IPRadiologyReport : System.Web.UI.UserControl
    {

        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }


        IP_PTRadiology objRad = new IP_PTRadiology();

       public  void BindRadiologyRequest()
        {
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            //Criteria += " AND EPR_BRANCH_ID ='" + Convert.ToString(Session["Branch_ID"]) + "'";
            Criteria += "AND IPR_ID in (" + EMR_ID + ")";


            DS = objRad.IPRadiologyGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvRadRequest.DataSource = DS;
                gvRadRequest.DataBind();
            }
            else
            {
                gvRadRequest.DataBind();
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
             if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

             if (!IsPostBack)
             {

                // BindRadiologyRequest();

             }
        }
    }
}