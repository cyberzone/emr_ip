﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IPVitalSignReport.ascx.cs" Inherits="EMR_IP.WebReports.IPVitalSignReport" %>

 <span class="lblCaption1" > Vital Sign</span>

   <asp:GridView ID="gvVital" runat="server" AutoGenerateColumns="False"
            EnableModelValidation="True" Width="100%" >
            <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
            <RowStyle CssClass="GridRow" />
            <AlternatingRowStyle CssClass="GridAlterRow"  />
            <Columns>
                
                <asp:TemplateField HeaderText="Date" HeaderStyle-Width="150px">
                    <ItemTemplate>
                       
                             <asp:Label ID="lblVitalDate" CssClass="GridRow" Width="70px" runat="server" Text='<%# Bind("IPV_DATEDesc") %>'  ></asp:Label>
                            <asp:Label ID="lblVitalTime" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("VitalTime") %>'  ></asp:Label>
                             
                        
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Weight">
                    <ItemTemplate>
                       
                              <asp:Label ID="lblWeight" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_WEIGHT") %>'></asp:Label>
                            Kg
                       
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Height">
                    <ItemTemplate>
                        
                               <asp:Label ID="lblHeight" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_HEIGHT") %>' >
                               </asp:Label> Cms  
                       
                    </ItemTemplate>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="BMI">
                    <ItemTemplate>
                        
                                <asp:Label ID="lblBMI" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_BMI") %>' ></asp:Label>
                       
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Temprature">
                    <ItemTemplate>
                        
                              <asp:Label ID="lblTemperatureF" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_TEMPERATURE_F") %>' Visible="false"></asp:Label>
                            <asp:Label ID="lblTemperatureC" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_TEMPERATURE_C") %>' ></asp:Label>
                           °C
                         
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Pulse">
                    <ItemTemplate>
                        
                             <asp:Label ID="lblPulse" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_PULSE") %>'> </asp:Label>Beats/Min
                        
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Respiration">
                    <ItemTemplate>
                        
                              <asp:Label ID="lblRespiration" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_RESPIRATION") %>'></asp:Label>/m

                       
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="BP:Systolic">
                    <ItemTemplate>
                        
                            <asp:Label ID="lblSystolic" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_BP_SYSTOLIC") %>' >mm/hg
                            </asp:Label>mm/hg
                       
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="BP:Systolic">
                    <ItemTemplate>
                        
                            <asp:Label ID="lblDiastolic" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_BP_DIASTOLIC") %>' ></asp:Label>mm/hg 
                         
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SPO2">
                    <ItemTemplate>
                        <asp:Label ID="lblSPO2" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPV_SPO2") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 
            </Columns>
           

        </asp:GridView>