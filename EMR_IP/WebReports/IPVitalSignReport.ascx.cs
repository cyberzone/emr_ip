﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.WebReports
{
    public partial class IPVitalSignReport : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

       public  void BindVital()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND IPV_ID = '" + EMR_ID + "'";

            IP_PTVitalSign objPTVital = new IP_PTVitalSign();
            ds = objPTVital.PTVitalSignGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvVital.DataSource = ds;
                gvVital.DataBind();

            }
            else
            {
                gvVital.DataBind();
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }
            if (!IsPostBack)
            {
              //  BindVital();

                
            }
        }
    }
}