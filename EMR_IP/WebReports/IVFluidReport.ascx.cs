﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;
namespace EMR_IP.WebReports
{
    public partial class IVFluidReport : System.Web.UI.UserControl
    {

        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }


       public  void BindIVFluid()
        {
            string Criteria = " 1=1 ";
            Criteria += " AND IPI_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPI_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            if (Convert.ToString(Session["EMR_ID"]) == "")
            {
                goto FunEnd;
            }


            DataSet DS = new DataSet();
            IP_PTIVFluid objPro = new IP_PTIVFluid();

            // if (drpTemplate.SelectedIndex == 0)
            // {
            //    DS = objPro.IPPTIVFluidGetGet(Criteria);
            //}
            //else
            //{
            //    Criteria = " 1=1 ";
            //    Criteria += " AND IPI_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";
            //    Criteria += " AND ET_CODE='" + drpTemplate.SelectedValue + "'";

            //    DS = objPro.PharmacyTemplatGe(Criteria);
            //}

            DS = objPro.IPPTIVFluidGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvIVFluid.DataSource = DS;
                gvIVFluid.DataBind();

            }
            else
            {
                gvIVFluid.DataBind();
            }
        FunEnd: ;
        }



        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {

               
            }
        }
    }
}