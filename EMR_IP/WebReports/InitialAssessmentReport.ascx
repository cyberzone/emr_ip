﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="InitialAssessmentReport.ascx.cs" Inherits="EMR_IP.WebReports.InitialAssessmentReport" %>

<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1">Initial Assessment Form</span>
<br />

<span class="lblCaption1">Visit Details</span>
<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" style="width: 50%">Area of Encounter/Assessment: 
                   <asp:Label ID="txtArea" CssClass="lblCaption1" runat="server"></asp:Label>
        </td>

        <td class="lblCaption1 BoldStyle BorderStyle" style="width: 50%">Arrival time to Area:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                  <asp:Label ID="txtArrivalDate" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
    </tr>
    <tr>
        <td style="height: 10px" colspan="2"></td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Date of visit:
                    <asp:Label ID="txtVisitDate" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle" style="height: 30px;">Assessment done by:
                    <asp:Label ID="txtAssessmentDoneBy" CssClass="lblCaption1" runat="server"></asp:Label>

        </td>

    </tr>
    <tr>
        <td style="height: 10px" colspan="2"></td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                <span class="lblCaption">Provisional  Diagnosis: </span>
                <br />
                <asp:Label ID="txtProvisionalDiagnosis" runat="server" CssClass="lblCaption1"></asp:Label>
            </div>
        </td>
       <td class="lblCaption1 BoldStyle BorderStyle">

            <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                <span class="lblCaption" style="width: 150px;">Condition upon visit:</span>

                <asp:RadioButtonList ID="radConditionUponVisit" runat="server" CssClass="lblCaption1" Width="400px" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Text="Ambulatory" Value="Ambulatory"></asp:ListItem>
                    <asp:ListItem Text="Wheel Chair" Value="WheelChair"></asp:ListItem>
                    <asp:ListItem Text="Stretcher" Value="Stretcher"></asp:ListItem>
                    <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                </asp:RadioButtonList>
                <br />
                <asp:Label ID="txtConditionOther" CssClass="lblCaption1" runat="server"></asp:Label>

            </div>

        </td>

    </tr>
    <tr>
        <td style="height: 10px" colspan="2"></td>
    </tr>
    <tr>


        <td class="lblCaption1 BoldStyle BorderStyle">

            <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                <span class="lblCaption">Information obtained from:</span>
                <asp:RadioButtonList ID="radInfoObtainedFrom" runat="server" CssClass="lblCaption1" Width="50%" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Text="Patient" Value="Patient"></asp:ListItem>
                    <asp:ListItem Text="Family/Friend" Value="Family"></asp:ListItem>
                    <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                </asp:RadioButtonList>
                <br />
                <asp:Label ID="txtInfoObtainedOther" CssClass="lblCaption1" runat="server"></asp:Label>

            </div>

        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">
            <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                <span class="lblCaption">Patient / Explanation given:</span>
                <asp:RadioButtonList ID="radExplanationGiven" runat="server" CssClass="lblCaption1" Width="50%" RepeatDirection="Horizontal" Enabled="false">
                    <asp:ListItem Text="Patient" Value="Patient"></asp:ListItem>
                    <asp:ListItem Text="Relative" Value="Relative"></asp:ListItem>
                    <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                </asp:RadioButtonList>
                <br />
                <asp:Label ID="txtExplanationGivenOther" CssClass="lblCaption1" runat="server"></asp:Label>

            </div>

        </td>

    </tr>

</table>

<span class="lblCaption1">Health History</span>

<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td valign="top" style="width: 49%">

            <span class="lblCaption1" style="font-weight: bold;">Present And / Or past Health Problems</span>

           <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <asp:Label ID="txtHC_ASS_Health_PresPastHist" CssClass="label" runat="server" Width="99%" Height="50px" TextMode="MultiLine" BorderWidth="1px" BorderColor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Health_PresPastHist',this)"></asp:Label>

                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <div style="padding: 5px; width: 97%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">

                            <span class="lblCaption">If any problem identified (Please specify)</span>

                            <br />
                            <asp:Label ID="txtPresPastProblem" runat="server" Width="100%" CssClass="label"></asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <div style="padding: 5px; width: 97%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">

                            <span class="lblCaption">Previous Hospitalizations / If any Surgeries write</span>


                            <br />
                            <asp:Label ID="txtPresPastSurgeries" runat="server" Width="100%" CssClass="label"></asp:Label>
                        </div>


                    </td>
                </tr>
                <tr>
                    <td style="height: 10px;"></td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <div style="padding: 5px; width: 97%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                            <span class="lblCaption1" style="font-weight: bold;">Alergies:</span>
                            <br />
                            Medication :
                                        <br />
                            <asp:Label ID="txtAlergiesMedication" CssClass="label" runat="server"></asp:Label><br />
                            Food  :
                                      <br />
                            <asp:Label ID="txtAlergiesFood" CssClass="label" runat="server"></asp:Label><br />
                            Other  :
                                      <br />
                            <asp:Label ID="txtAlergiesOther" CssClass="label" runat="server"></asp:Label><br />


                        </div>

                    </td>
                </tr>
            </table>

        </td>

        <td valign="top" style="width: 49%">
            <span class="lblCaption1" style="font-weight: bold;">Medication History</span>

          <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Medicine Name / Dosage / Frequency </span>


                        <br />
                        <asp:Label ID="txtHC_ASS_Health_MedicHist_Medicine" CssClass="lblCaption1" runat="server"></asp:Label>

                    </td>
                </tr>


                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Sleep / Rest </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Health_MedicHist_Sleep" CssClass="lblCaption1" runat="server"></asp:Label>

                    </td>
                </tr>

                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Psychological Assessment </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Health_MedicHist_PschoyAss" CssClass="lblCaption1" runat="server"></asp:Label>
                    </td>
                </tr>

                <tr>

                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1" style="font-weight: bold;">Pain Assessment </span>
                        <br />
                        Patient expresses presence of pain   
                                  <asp:RadioButtonList ID="radPainAssYes" runat="server" CssClass="label" Width="100px" RepeatDirection="Horizontal" Enabled="false">
                                      <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                      <asp:ListItem Text="No" Value="False"></asp:ListItem>
                                  </asp:RadioButtonList>

                    </td>
                </tr>


            </table>

        </td>
    </tr>
</table>

<span class="lblCaption1">Encounter</span>

<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td style="width: 50%">
            <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td style="width: 50%" valign="top">
                        <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">

                            <span class="lblCaption">Present Complaint</span>
                            <br />
                            <asp:Label ID="txtPresentComplaient" runat="server" Width="100%" CssClass="lblCaption"></asp:Label>
                        </div>
                        <br />
                        <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                            <span class="lblCaption">Past Medical History </span>
                            <asp:Label ID="txtPastMedicalHistory" runat="server" Width="100%" CssClass="lblCaption"></asp:Label>

                        </div>
                        <br />
                        <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                            <span class="lblCaption">Past surgical History</span>
                            <asp:Label ID="txtSurgicallHistory" runat="server" Width="100%" CssClass="lblCaption"></asp:Label>
                        </div>
                        <br />
                        <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                            <span class="lblCaption">Psycho socio- economic History	</span>
                            <asp:Label ID="txtPsychoSocEcoHistory" runat="server" Width="100%" CssClass="lblCaption"></asp:Label>

                        </div>
                        <br />
                        <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                            <span class="lblCaption">Family History</span>
                            <asp:Label ID="txtFamilyHistory" runat="server" Width="100%" CssClass="lblCaption"></asp:Label>

                        </div>
                        <br />
                        <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                            <span class="lblCaption">Physical Assessment</span>

                            <asp:Label ID="txtPhysicalAssessment" runat="server" Width="100%" CssClass="lblCaption"></asp:Label>


                        </div>
                        <br />
                        <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                            <span class="lblCaption">Review of Systems </span>
                            <asp:Label ID="txtReviewOfSystem" runat="server" Width="100%" CssClass="lblCaption"></asp:Label>


                        </div>
                    </td>
                </tr>

            </table>
        </td>
        <td style="width: 50%" valign="top">
            <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                <span class="lblCaption">Investigations</span>
                <br />
                <asp:Label ID="txtInvestigations" runat="server" Width="100%" CssClass="label"></asp:Label>
            </div>
            <br />
            <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                <span class="lblCaption">Diagnosis</span>
                <br />
                <span class="lblCaption1">Principal Diagnosis     : </span>
                <br />
                <asp:Label ID="txtPrincipalDiagnosis" runat="server" Width="100%" CssClass="lblCaption"></asp:Label>
                <br />
                <br />
                <span class="lblCaption1">Secondary  Diagnosis     : </span>
                <br />
                <asp:Label ID="txtSecondaryDiagnosis" runat="server" Width="100%" CssClass="lblCaption"></asp:Label>


            </div>
            <br />
            <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                <span class="lblCaption">Treatment Plan and Patient Education</span>
                <asp:Label ID="txtTreatmentPlan" runat="server" Width="100%" CssClass="lblCaption"></asp:Label>
            </div>

            <br />
            <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                <span class="lblCaption">Procedure</span>
                <br />
                <asp:Label ID="txtProcedure" runat="server" Width="100%" CssClass="lblCaption"></asp:Label>
            </div>

            <br />
            <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                <span class="lblCaption">Treatment</span>
                <asp:Label ID="txtTreatment" runat="server" Width="100%" CssClass="lblCaption"></asp:Label>
            </div>

            <br />
            <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                <span class="lblCaption">Follow Up notes</span>
                <asp:Label ID="txtFollowUpNotes" runat="server" Width="100%" CssClass="lblCaption"></asp:Label>
            </div>



        </td>
    </tr>
</table>

<span class="lblCaption1">Physical Assessment</span>

 <table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td valign="top" style="width: 49%">
            
                <span class="lblCaption1" style="font-weight: bold;">Gastrointestinal</span>
                <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">Bowel Sounds </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Gast_BowelSounds" CssClass="label" runat="server"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">Elimination </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Gast_Elimination" CssClass="label" runat="server" Width="99%"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">General </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Gast_General" CssClass="label" runat="server" Width="99%"></asp:Label>
                        </td>
                    </tr>
                </table>
            

            
                <span class="lblCaption1" style="font-weight: bold;">Skin/Integumentary    </span>
                 <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">Color </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Skin_Color" CssClass="label" runat="server" Width="99%"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">Temperature </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Skin_Temperature" CssClass="label" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">Lesions </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Skin_Lesions" CssClass="label" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
            
                <span class="lblCaption1" style="font-weight: bold;">Neurological        </span>
                <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">General </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Neuro_General" CssClass="label" runat="server"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">Level of Consciousness </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Neuro_Conscio" CssClass="label" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">Oriented to </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Neuro_Oriented" CssClass="label" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">Time  Responsivenes </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Neuro_TimeResp" CssClass="label" runat="server"></asp:Label>
                        </td>
                    </tr>


                </table>
          
                <span class="lblCaption1" style="font-weight: bold;">Cardiovascular        </span>
                <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">General </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Cardio_General" CssClass="label" runat="server"></asp:Label>
                        </td>
                    </tr>

                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">Pulse </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Cardio_Pulse" CssClass="label" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">Pedal Pulses </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Cardio_PedalPulse" CssClass="label" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">Edema </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Cardio_Edema" CssClass="label" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">Nail beds </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Cardio_NailBeds" CssClass="label" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">Capillary refill </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Phy_Cardio_Capillary" CssClass="label" runat="server"></asp:Label>
                        </td>
                    </tr>
                </table>
             

        </td>
        <td style="width: 1%;"></td>
        <td valign="top" style="width: 49%">


            <span class="lblCaption1" style="font-weight: bold;">Reproductive</span>
             <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Male </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Repro_Male" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Female </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Repro_Female" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Breasts </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Repro_Breasts" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>


            <span class="lblCaption1" style="font-weight: bold;">Genitourinary</span>
            <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <asp:Label ID="txtHC_ASS_Phy_Genit_General" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>

            </table>



            <span class="lblCaption1" style="font-weight: bold;">EENT & Mouth                       </span>
            <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Eyes </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Ent_Eyes" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Ears </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Ent_Ears" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Nose </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Ent_Nose" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Throat / Neck </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Ent_Throat" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>

            </table>



            <span class="lblCaption1" style="font-weight: bold;">Respiratory                     </span>
             <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Chest Appearance </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Resp_Chest" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Breath Pattern </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Resp_BreathPatt" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Breath Sound </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Resp_BreathSound" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Breath Cough </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Resp_BreathCough" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>


            </table>

            <span class="lblCaption1" style="font-weight: bold;">Diet / Nutrition Assessment  </span>
             <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Diet </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Nutri_Diet" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Appetite </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Nutri_Appetite" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Nutritional Support </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Nutri_NutriSupport" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Feeding difficulties: </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Nutri_FeedingDif" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Weight status </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Nutri_WeightStatus" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">By Diagnosis </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Phy_Nutri_Diag" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>

            </table>

        </td>
    </tr>
</table>

<span class="lblCaption1">Risk/Safety Assessment</span>

 <table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td valign="top" style="width: 49%">


            <span class="lblCaption1" style="font-weight: bold;">Braden Skin Risk Assessment </span>
            <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Sensory perception </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Risk_SkinRisk_Sensory" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>

                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Moisture </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Risk_SkinRisk_Moisture" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Activity</span><br />
                        <asp:Label ID="txtHC_ASS_Risk_SkinRisk_Activity" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Mobility </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Risk_SkinRisk_Mobility" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Nutrition </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Risk_SkinRisk_Nutrition" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Friction / Shear </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Risk_SkinRisk_Friction" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>

            </table>



            <span class="lblCaption1" style="font-weight: bold;">Educational Assessment </span>
            <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Patient and / or family Need Eduction On </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Risk_Edu_General" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>


            </table>

        </td>
        <td style="width: 1%;"></td>
        <td valign="top" style="width: 49%">

            
                <span class="lblCaption1" style="font-weight: bold;">Socioeconomic </span>
               <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td class="lblCaption1 BoldStyle BorderStyle">
                            <span class="lblCaption1">Living Situation </span>
                            <br />
                            <asp:Label ID="txtHC_ASS_Risk_Socio_Living" CssClass="label" runat="server"></asp:Label>
                        </td>
                    </tr>

                </table>
             

            <span class="lblCaption1" style="font-weight: bold;">Fall/Safety Risk </span>
           <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1"></span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Risk_Safety_General" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>

            </table>


            <span class="lblCaption1" style="font-weight: bold;">Functional Assessment </span>
            <table cellpadding="0" cellspacing="0" style="width: 100%">
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Self Caring </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Risk_Fun_SelfCaring" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Musculoskeletal   </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Risk_Fun_Musculos" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <span class="lblCaption1">Use of Assisting Equipment   </span>
                        <br />
                        <asp:Label ID="txtHC_ASS_Risk_Fun_Equipment" CssClass="label" runat="server"></asp:Label>
                    </td>
                </tr>

            </table>


        </td>
    </tr>
</table>
