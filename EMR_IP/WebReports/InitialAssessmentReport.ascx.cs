﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;


namespace EMR_IP.WebReports
{
    public partial class InitialAssessmentReport : System.Web.UI.UserControl
    {

        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

        CommonBAL objCom = new CommonBAL();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("InitialAssessmentReport." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void BindEncounter()
        {
            DataSet ds = new DataSet();
            string Criteria = " 1=1 and IPE_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPE_ID='" + EMR_ID + "'";

            IP_InitialAssessmentForm objPTEnc = new IP_InitialAssessmentForm();
            ds = objPTEnc.PTEncounterGet(Criteria);
            if (ds.Tables[0].Rows.Count > 0)
            {

                txtArea.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_AREA"]);
                txtVisitDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_VISIT_DATEDesc"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["VisitTime"]);
                txtArrivalDate.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_ARRIVAL_TIMEDesc"]);
                txtProvisionalDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_DIAGNOSIS"]);

                radConditionUponVisit.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["IPE_CONDITION"]);
                txtConditionOther.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_CONDITION_OTH"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["ArrivalTime"]);



                radInfoObtainedFrom.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["IPE_INFOBTAINDFROM"]);
                txtInfoObtainedOther.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_INFOBTAINDFROM_OTH"]);

                radExplanationGiven.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["IPE_EXPLI_GIVEN"]);
                txtExplanationGivenOther.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_EXPLI_GIVEN_OTH"]);

                if (ds.Tables[0].Rows[0].IsNull("IPE_ASSESSMENT_DONE_CODE") == false)
                {
                    if (Convert.ToString(ds.Tables[0].Rows[0]["IPE_ASSESSMENT_DONE_CODE"]) != "")
                    {
                        txtAssessmentDoneBy.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_ASSESSMENT_DONE_CODE"]) + " ~ " + Convert.ToString(ds.Tables[0].Rows[0]["IPE_ASSESSMENT_DONE_NAME"]);

                    }
                }

                txtPresentComplaient.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_PRESENT_COMPLAINT"]);
                txtPastMedicalHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_PAST_MEDICAL_HISTORY"]);
                txtSurgicallHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_PAST_SURGICAL_HISTORY"]);
                txtPsychoSocEcoHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_PSY_SOC_ECONOMIC_HISTORY"]);
                txtFamilyHistory.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_FAMILY_HISTORY"]);
                txtPhysicalAssessment.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_PHYSICAL_ASSESSMENT"]);
                txtReviewOfSystem.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_REVIEW_OF_SYSTEMS"]);
                txtInvestigations.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_INVESTIGATIONS"]);
                txtPrincipalDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_PRINCIPAL_DIAGNOSIS"]);
                txtSecondaryDiagnosis.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_SECONDARY_DIAGNOSIS"]);
                txtTreatmentPlan.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_TREATMENT_PLAN"]);
                txtProcedure.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_PROCEDURE"]);
                txtTreatment.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_TREATMENT"]);
                txtFollowUpNotes.Text = Convert.ToString(ds.Tables[0].Rows[0]["IPE_FOLLOWUPNOTES"]);

            }

        }

        void BindAssessment()
        {
            IP_InitialAssessmentForm obj = new IP_InitialAssessmentForm();

            DataSet DS = new DataSet();
            string Criteria = " 1=1 and IPA_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPA_ID='" + EMR_ID + "'";
            DS = obj.PTAssessmentGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                txtHC_ASS_Health_PresPastHist.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_PRESPASTHIST"]);
                txtPresPastProblem.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_PRESPAS_PROB"]);
                txtPresPastSurgeries.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_PRESPAS_PREVPROB"]);
                txtAlergiesMedication.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_ALLERGIES_MEDIC"]);
                txtAlergiesFood.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_ALLERGIES_FOOD"]);
                txtAlergiesOther.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_ALLERGIES_OTHER"]);
                txtHC_ASS_Health_MedicHist_Medicine.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_MEDICHIST_MEDICINE"]);
                txtHC_ASS_Health_MedicHist_Sleep.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_MEDICHIST_SLEEP"]);
                txtHC_ASS_Health_MedicHist_PschoyAss.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_MEDICHIST_PSCHOYASS"]);
                radPainAssYes.SelectedValue = Convert.ToString(DS.Tables[0].Rows[0]["IPA_HEALTH_PAIN_EXPRESSES"]);
                txtHC_ASS_Phy_Gast_BowelSounds.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_GAST_BOWELSOUNDS"]);
                txtHC_ASS_Phy_Gast_Elimination.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_GAST_ELIMINATION"]);
                txtHC_ASS_Phy_Gast_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_GAST_GENERAL"]);
                txtHC_ASS_Phy_Repro_Male.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_REPRO_MALE"]);
                txtHC_ASS_Phy_Repro_Female.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_REPRO_FEMALE"]);
                txtHC_ASS_Phy_Repro_Breasts.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_REPRO_BREASTS"]);
                txtHC_ASS_Phy_Genit_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_GENIT_GENERAL"]);
                txtHC_ASS_Phy_Skin_Color.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_SKIN_COLOR"]);
                txtHC_ASS_Phy_Skin_Temperature.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_SKIN_TEMPERATURE"]);
                txtHC_ASS_Phy_Skin_Lesions.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_SKIN_LESIONS"]);
                txtHC_ASS_Phy_Neuro_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NEURO_GENERAL"]);
                txtHC_ASS_Phy_Neuro_Conscio.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NEURO_CONSCIO"]);
                txtHC_ASS_Phy_Neuro_Oriented.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NEURO_ORIENTED"]);
                txtHC_ASS_Phy_Neuro_TimeResp.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NEURO_TIMERESP"]);
                txtHC_ASS_Phy_Cardio_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_CARDIO_GENERAL"]);
                txtHC_ASS_Phy_Cardio_Pulse.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_CARDIO_PULSE"]);
                txtHC_ASS_Phy_Cardio_PedalPulse.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_CARDIO_PEDALPULSE"]);
                txtHC_ASS_Phy_Cardio_Edema.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_CARDIO_EDEMA"]);
                txtHC_ASS_Phy_Cardio_NailBeds.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_CARDIO_NAILBEDS"]);
                txtHC_ASS_Phy_Cardio_Capillary.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_CARDIO_CAPILLARY"]);
                txtHC_ASS_Phy_Ent_Eyes.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_ENT_EYES"]);
                txtHC_ASS_Phy_Ent_Ears.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_ENT_EARS"]);
                txtHC_ASS_Phy_Ent_Nose.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_ENT_NOSE"]);
                txtHC_ASS_Phy_Ent_Throat.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_ENT_THROAT"]);
                txtHC_ASS_Phy_Resp_Chest.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_RESP_CHEST"]);
                txtHC_ASS_Phy_Resp_BreathPatt.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_RESP_BREATHPATT"]);
                txtHC_ASS_Phy_Resp_BreathSound.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_RESP_BREATHSOUND"]);
                txtHC_ASS_Phy_Resp_BreathCough.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RESP_BREATHCOUGH"]);
                txtHC_ASS_Phy_Nutri_Diet.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NUTRI_DIET"]);
                txtHC_ASS_Phy_Nutri_Appetite.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NUTRI_APPETITE"]);
                txtHC_ASS_Phy_Nutri_NutriSupport.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NUTRI_NUTRISUPPORT"]);
                txtHC_ASS_Phy_Nutri_FeedingDif.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NUTRI_FEEDINGDIF"]);
                txtHC_ASS_Phy_Nutri_WeightStatus.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NUTRI_WEIGHTSTATUS"]);
                txtHC_ASS_Phy_Nutri_Diag.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_PHY_NUTRI_DIAG"]);
                txtHC_ASS_Risk_SkinRisk_Sensory.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SKINRISK_SENSORY"]);
                txtHC_ASS_Risk_SkinRisk_Moisture.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SKINRISK_MOISTURE"]);
                txtHC_ASS_Risk_SkinRisk_Activity.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SKINRISK_ACTIVITY"]);
                txtHC_ASS_Risk_SkinRisk_Mobility.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SKINRISK_MOBILITY"]);
                txtHC_ASS_Risk_SkinRisk_Nutrition.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SKINRISK_NUTRITION"]);
                txtHC_ASS_Risk_SkinRisk_Friction.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SKINRISK_FRICTION"]);
                txtHC_ASS_Risk_Socio_Living.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SOCIO_LIVING"]);
                txtHC_ASS_Risk_Safety_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_SAFETY_GENERAL"]);
                txtHC_ASS_Risk_Fun_SelfCaring.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_FUN_SELFCARING"]);
                txtHC_ASS_Risk_Fun_Musculos.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_FUN_MUSCULOS"]);
                txtHC_ASS_Risk_Fun_Equipment.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_FUN_EQUIPMENT"]);
                txtHC_ASS_Risk_Edu_General.Text = Convert.ToString(DS.Tables[0].Rows[0]["IPA_RISK_EDU_GENERAL"]);

            }


        }

        public void BindInitialAssessment()
        {
            BindEncounter();
            BindAssessment();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                  
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}