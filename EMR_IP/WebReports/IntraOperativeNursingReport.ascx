﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IntraOperativeNursingReport.ascx.cs" Inherits="EMR_IP.WebReports.IntraOperativeNursingReport" %>


<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1">Intra Operative Nursing Record</span>
<br />

<table cellpadding="0" cellspacing="0" style="width: 100%">

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Date & Time
        </td>
        <td class="lblCaption1 BorderStyle" colspan="3">
            <asp:Label ID="txtInterOperDate" runat="server" Width="70px" CssClass="lblCaption1"></asp:Label>

        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Pre-operative Diagnosis
        </td>
        <td class="lblCaption1 BorderStyle" colspan="3">

            <asp:Label ID="txtPreOperDiag" runat="server" CssClass="lblCaption1"></asp:Label>


        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Post Operative Diagnosis
        </td>
        <td class="lblCaption1 BorderStyle" colspan="3">

            <asp:Label ID="txtPostOperDiag" runat="server" CssClass="lblCaption1"></asp:Label>


        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Operative procedure Performed
        </td>
        <td class="lblCaption1 BorderStyle" colspan="3">

            <asp:Label ID="txtOperProcPerf" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Name of Surgeon
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="drpSurgeon" runat="server" CssClass="lblCaption1" Width="200px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Name of Anaesthetist
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="drpAnaesthetist" runat="server" CssClass="lblCaption1" Width="200px"></asp:Label>


        </td>


    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Anesthesia Type 
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="drpAnesType" runat="server" CssClass="lblCaption1" Width="200px"></asp:Label>


        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">ASA Classification
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="drpAnesClassifi" runat="server" CssClass="lblCaption1" Width="200px">
                                     
            </asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1" valign="top">Assessment
        </td>
        <td colspan="3">


            <table width="100%">

                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">Patient Identification 
                    </td>
                    <td class="lblCaption1 BorderStyle">

                        <asp:RadioButtonList ID="radInfoMedicFood" runat="server" CssClass="lblCaption1" Width="250px" RepeatDirection="Horizontal" Enabled="false">
                            <asp:ListItem Text="ID Band" Value="ID Band"></asp:ListItem>
                            <asp:ListItem Text="Verbal" Value="Verbal"></asp:ListItem>
                        </asp:RadioButtonList>

                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">Allergies
                    </td>
                    <td class="lblCaption1 BorderStyle">

                        <input type="radio" id="radAllergiesYes" name="Allergies" runat="server" class="lblCaption1" value="Yes" disabled="disabled" /><span class="lblCaption1">Yes</span>
                        <input type="radio" id="radAllergiesNo" name="Allergies" runat="server" class="lblCaption1" value="No" disabled="disabled" /><span class="lblCaption1">No</span>
                        <input type="text" id="txtAlergies" runat="server" class="lblCaption1" style="width: 400px;" />

                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">NPO
                    </td>
                    <td class="lblCaption1 BorderStyle">

                        <input type="radio" id="radNPOYes" name="radNPO" runat="server" class="lblCaption1" value="Yes" disabled="disabled" /><span class="lblCaption1">Yes</span>
                        <input type="radio" id="radNPONo" name="radNPO" runat="server" class="lblCaption1" value="No" disabled="disabled" /><span class="lblCaption1">No</span>
                        <input type="text" id="txtNPO" runat="server" class="lblCaption1" style="width: 400px;" />

                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">Operative Consent
                    </td>
                    <td class="lblCaption1 BorderStyle">

                        <input type="radio" id="radOperConYes" name="radOperCon" runat="server" class="lblCaption1" value="Yes" disabled="disabled" /><span class="lblCaption1">Yes</span>
                        <input type="radio" id="radOperConNo" name="radOperCon" runat="server" class="lblCaption1" value="No" disabled="disabled" /><span class="lblCaption1">No</span>
                        <input type="text" id="txtOperCon" runat="server" class="lblCaption1" style="width: 400px;" />

                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">Time out
                    </td>
                    <td class="lblCaption1 BorderStyle">

                        <input type="radio" id="radTimeOutYes" name="radTimeOut" runat="server" class="lblCaption1" value="Yes" disabled="disabled" /><span class="lblCaption1">Yes</span>
                        <input type="radio" id="radTimeOutNo" name="radTimeOut" runat="server" class="lblCaption1" value="No" disabled="disabled" /><span class="lblCaption1">No</span>
                        <input type="text" id="txtTimeOut" runat="server" class="lblCaption1" style="width: 400px;" />

                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">Varification operative checklist
                    </td>
                    <td class="lblCaption1 BorderStyle">

                        <input type="radio" id="radVarifOperCheckList" name="radVarifOperCheckList" runat="server" class="lblCaption1" value="Yes" disabled="disabled" /><span class="lblCaption1">Yes</span>



                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">Comment
                    </td>
                    <td class="lblCaption1 BorderStyle">
                        <asp:Label ID="txtAssComment" CssClass="lblCaption1" runat="server"></asp:Label>


                    </td>

                </tr>
            </table>

        </td>
        <td></td>
    </tr>


    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Case Classification
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="drpCaseClasification" Width="200px" runat="server" CssClass="lblCaption1">
            </asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Type
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="drpCaseType" Width="200px" runat="server" CssClass="lblCaption1">
            </asp:Label>

        </td>
    </tr>

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Intubation time
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="txtIntubationTime" runat="server" CssClass="lblCaption1" Width="200px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Incision time
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="txtIncisionTime" runat="server" CssClass="lblCaption1" Width="200px"></asp:Label>

        </td>
    </tr>

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Hair Removal 
        </td>
        <td class="lblCaption1 BorderStyle">

            <input type="radio" id="radHairRemovalYes" name="radHairRemoval" runat="server" class="lblCaption1" value="Yes" disabled="disabled" /><span class="lblCaption1">Yes</span>
            <input type="radio" id="radHairRemovalNo" name="radHairRemoval" runat="server" class="lblCaption1" value="No" disabled="disabled" /><span class="lblCaption1">No</span>
            <input type="radio" id="radClipped" name="radHairRemoval" runat="server" class="lblCaption1" value="Clipped" disabled="disabled" /><span class="lblCaption1">Clipped</span>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Location
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="txtDrainsLocation" runat="server" CssClass="lblCaption1" Width="200px"></asp:Label>

        </td>
    </tr>

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Skin condition
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="drpSkinCondition" runat="server" CssClass="lblCaption1">
            </asp:Label>


        </td>
        <td class="lblCaption1 BoldStyle BorderStyle"></td>
        <td class="lblCaption1 BoldStyle BorderStyle"></td>
    </tr>
</table>



<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Position
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="drpPosition" Width="200px" runat="server" CssClass="label">
            </asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Positional Aide
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="drpPositionalAide" Width="200px" runat="server" CssClass="label">
            </asp:Label>

        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Safety strap on
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <input type="radio" id="radSafetyStrapYes" name="radSafetyStrapYes" runat="server" class="lblCaption1" value="True" disabled="disabled" /><label class="lblCaption1" for="radSafetyStrapYes">Yes</label>
            <input type="radio" id="radSafetyStrapNo" name="radSafetyStrapYes" runat="server" class="lblCaption1" value="False" disabled="disabled" /><label class="lblCaption1" for="radSafetyStrapNo"> No </label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Positioned By
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:Label ID="drpSafetyStrapPositionedBy" runat="server" CssClass="label" Width="200px"></asp:Label>

        </td>
    </tr>
    <tr>
        <td></td>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <input type="checkbox" id="chkLeftArmStraps" name="chkLeftArmStraps" runat="server" class="lblCaption1" value="Left" disabled="disabled" /><label class="lblCaption1" for="chkLeftArmStraps">Left arm straps-Board less than 90 degrees</label>
            <input type="checkbox" id="chkRightArmStraps" name="chkRightArmStraps" runat="server" class="lblCaption1" value="Right" disabled="disabled" /><label class="lblCaption1" for="chkRightArmStraps"> Right arm straps-Board less than 90 Degrees </label>
        </td>

    </tr>
</table>



<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Electrosurgical Unit
        </td>
        <td>

            <input type="radio" id="radElectroUnitYes" name="radElectroUnitYes" runat="server" class="lblCaption1" value="True" disabled="disabled" /><span class="lblCaption1">Yes </span>
            <input type="radio" id="radElectroUnitNo" name="radElectroUnitYes" runat="server" class="lblCaption1" value="False" disabled="disabled" /><span class="lblCaption1"> No </span>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Return electrode location
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtRetElectroLocation" runat="server" CssClass="label" Width="200px"></asp:Label>

        </td>
    </tr>

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Settings
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Cut:&nbsp;
                                     <asp:Label ID="txtSettingsCut" runat="server" CssClass="label" Width="100px"></asp:Label>
            Coag.:&nbsp;
                                     <asp:Label ID="txtSettingsCoag" runat="server" CssClass="label" Width="100px"></asp:Label>
            Bipolar:&nbsp;
                                     <asp:Label ID="txtBipolar" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Applied By 
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="drpElectroUnitAppliedBy" runat="server" CssClass="label" Width="200px"></asp:Label>

        </td>
    </tr>
</table>


<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" style="width: 150px">Post op skin condition under pad
        </td>
        <td class="lblCaption1 BorderStyle" colspan="3">

            <asp:Label ID="txtSkinCondition" runat="server" CssClass="label" Width="90%"></asp:Label>

        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Tourniquets
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Time up: &nbsp;
                                                <asp:Label ID="txtTourniTimeUpDate" runat="server" Width="70px" CssClass="label"></asp:Label>


        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Time Down:
        </td>
        <td class="lblCaption1 BorderStyle">

            <asp:Label ID="txtTourniTimeDownDate" runat="server" Width="70px" CssClass="label"></asp:Label>



        </td>
    </tr>
    <tr>

        <td class="lblCaption1 BoldStyle BorderStyle">Skin status
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Before placement:&nbsp;
                                    <asp:Label ID="txtSkinStatusBeforePlacement" runat="server" CssClass="label" Width="200px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">After placement:&nbsp;
        </td>
        <td>

            <asp:Label ID="txtSkinStatusAfterPlacement" runat="server" CssClass="label" Width="200px"></asp:Label>

        </td>
    </tr>
    <tr>

        <td class="lblCaption1 BoldStyle BorderStyle">Site/Location
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">


            <asp:Label ID="txtSiteLocation" runat="server" CssClass="label" Width="200px"></asp:Label>


        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Applied By
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">


            <asp:Label ID="drpSkinAppliedBy" runat="server" CssClass="label" Width="200px"></asp:Label>

        </td>
    </tr>
</table>


<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" style="width: 150px">Counts
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">1ST
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">2ND
        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">SPONGE
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtSponge1" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtSponge2" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">GAUZE
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtGauze1" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtGauze2" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">PAENUT
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtPaenut1" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtPaenut2" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">NEEDLE
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtNeedle1" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtNeedle2" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">INSTRUMENTS
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtInstruments1" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtInstruments2" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">COTTON BALLS
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtCottonBalls1" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtCottonBalls2" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
    </tr>
</table>

<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td style="width: 300px;">


            <input type="checkbox" id="chkCountCorYes" name="radCountCorYes" runat="server" class="lblCaption1" value="True" disabled="disabled" /><span class="lblCaption1"> Count Correct </span>
            <input type="checkbox" id="chkCountCorSurgeonInformed" name="radCountCorSurgeonInformed" runat="server" class="lblCaption1" value="True" disabled="disabled" /><span class="lblCaption1"> Surgeon Informed </span>

        </td>

        <td  class="lblCaption1 BoldStyle BorderStyle" colspan="3">

            <input type="checkbox" id="chkCountincorYes" name="radCountincorrectYes" runat="server" class="lblCaption1" value="True" disabled="disabled" /><span class="lblCaption1"> Count InCorrect </span>
            <input type="checkbox" id="chkCountincorSurgeonInformed" name="radCountincorrectYes" runat="server" class="lblCaption1" value="True" disabled="disabled" /><span class="lblCaption1"> surgeon informed </span>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Corrective Action Taken
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <input type="checkbox" id="radCorrectiveActionTaken" name="radCountincorrectYes" runat="server" class="lblCaption1" value="True" disabled="disabled" /><span class="lblCaption1"> Yes </span>


        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Comment 
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtCorrectiveComment" runat="server" CssClass="label" Height="30px" Width="300px" TextMode="MultiLine"></asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Extubation Time
        </td>
         <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtExtubationTime" runat="server" CssClass="label" Width="200px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Drains/packing
        </td>
         <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtDrainspacking" runat="server" CssClass="label" Width="200px"></asp:Label>

        </td>


    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Clipper
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <input type="radio" id="radClipperYes" name="radClipperYes" runat="server" class="lblCaption1" value="Yes" disabled="disabled" /><span class="lblCaption1">Yes</span>
            <input type="radio" id="radClipperNo" name="radClipperYes" runat="server" class="lblCaption1" value="No" disabled="disabled" /><span class="lblCaption1">No</span>

        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">Dressing
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtDressing" runat="server" CssClass="label" Width="200px"></asp:Label>

        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Specimen
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <input type="radio" id="radpecimenYes" name="chkSpecimenYes" runat="server" class="lblCaption1" value="Yes" disabled="disabled" /><span class="lblCaption1"> Yes </span>
            <input type="radio" id="radSpecimenNo" name="chkSpecimenYes" runat="server" class="lblCaption1" value="No" disabled="disabled" /><span class="lblCaption1"> No </span>


        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Specimen Type
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="drpSpecimenType" Width="200px" runat="server" CssClass="label">
            </asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Patient Transferred 
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="drpPatientTransTo" Width="200px" runat="server" CssClass="label">
            </asp:Label>

        </td>
    </tr>
</table>



<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" style="width: 150px;">Urinary  Catheter:
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Foly cath No &nbsp;
                                     <asp:Label ID="txtUriCatheterFolyCathno" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Inserted By
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="drpUriCatheterInsertedBy" runat="server" CssClass="label" Width="200px"></asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Urine Returned</td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="drpUrineReturned" Width="200px" runat="server" CssClass="label">
            </asp:Label>

        </td>
    </tr>
</table>


<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" style="width: 150px;">Per Anesthesiologist</td>
        <td  class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="drpPreAnesthesiologist" runat="server" CssClass="label" Width="200px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Total Blood Products  &nbsp;
                                  <asp:Label ID="txtTotalBloodProd" runat="server" CssClass="label" Width="100px"></asp:Label>
            Total IV Fluids  &nbsp;
                                  <asp:Label ID="txtTotalIVFluids" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
    </tr>
</table>

<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" style="width: 150px;">Urinary Drainage </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtUrinaryDrainage" runat="server" CssClass="label" Width="195px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Estimated Blood Loss  &nbsp;
                                  <asp:Label ID="txtEstBloodLoss" runat="server" CssClass="label" Width="100px"></asp:Label>
            Others   &nbsp;
                                  <asp:Label ID="txtOutPutOthers" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
    </tr>
</table>



<span class="lblCaption1">PROSTHESIS/IMPLANTS/GRAFT</span>

<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>


        <td class="lblCaption1 BoldStyle BorderStyle" style="width: 150px;">Location/Type</td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtProsthesisLocation" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Size</td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtProsthesisSize" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Lot/Serial No</td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtProsthesisSlNo" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
    </tr>

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Manufacturer
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle" colspan="5">

            <asp:Label ID="txtCarmManufacturer" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
    </tr>
</table>



<span class="lblCaption1 BoldStyle BorderStyle">X-Ray/LASER</span>

<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>


        <td class="lblCaption1 BoldStyle BorderStyle" style="width: 150px;">C-arm</td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <input type="radio" id="radCarmYes" name="radCarmYes" runat="server" class="lblCaption1" value="Yes" disabled="disabled" /><span class="lblCaption1">Yes</span>
            <input type="radio" id="radCarmNo" name="radCarmYes" runat="server" class="lblCaption1" value="No" disabled="disabled" /><span class="lblCaption1">No</span>
            <input type="radio" id="radCarmNA" name="radCarmYes" runat="server" class="lblCaption1" value="NA" disabled="disabled" /><span class="lblCaption1">Not Applicable</span>

        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Laser</td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtLaser" runat="server" CssClass="label" Width="100%"></asp:Label>

        </td>
    </tr>

</table>


<span class="lblCaption1 BoldStyle BorderStyle">Others</span>

<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>


        <td class="lblCaption1 BoldStyle BorderStyle" style="width: 150px;">Microscope</td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtOthersMicroscope" runat="server" CssClass="label" Width="200px"></asp:Label>

        </td>
        <td  class="lblCaption1 BoldStyle BorderStyle">Others</td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtOthersOthers" runat="server" CssClass="label" Width="200px"></asp:Label>

        </td>
    </tr>
</table>



<table cellpadding="0" cellspacing="0" style="width: 100%">



    <tr>
        <td  class="lblCaption1 BoldStyle BorderStyle" style="width: 150px">Comment
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtComment" runat="server" CssClass="label" Height="50px" Width="100%" TextMode="MultiLine" Style="resize: none;"></asp:Label>

        </td>

    </tr>
</table>

<span class="lblCaption1 BoldStyle BorderStyle" >Signature</span>
<table cellpadding="0" cellspacing="0" style="width: 100%">

    <tr>
        <td>

            <span class="lblCaption1">Scrub Nurse </span>
            <table width="100%">
                <tr>

                    <td class="lblCaption1 BoldStyle BorderStyle" style="width: 150px">Name  
                    </td>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <asp:Label ID="drpScrubNurse" runat="server" CssClass="label" Width="205px"></asp:Label>
                    </td>
                </tr>
              
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">Date & Time
                    </td>
                     <td class="lblCaption1 BoldStyle BorderStyle">

                        <asp:Label ID="txtScrubNurseDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:Label>

                    </td>
                </tr>

            </table>



        </td>
        <td>

            <span class="lblCaption1">Circulatory Nurse</span>
            <table width="100%">
                <tr>

                    <td class="lblCaption1 BoldStyle BorderStyle" style="width: 150px">Name  
                    </td>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <asp:Label ID="drpCirculNurse" runat="server" CssClass="label" Width="205px"></asp:Label>
                    </td>
                </tr>
               
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">Date & Time
                    </td>
                    <td class="lblCaption1 BoldStyle BorderStyle">

                        <asp:Label ID="txtCirculNurseDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:Label>

                    </td>
                </tr>

            </table>

        </td>

    </tr>

    <tr>
        <td>

            <span class="lblCaption1">Releiver Scrub Nurse </span>
            <table width="100%">
                <tr>

                    <td class="lblCaption1 BoldStyle BorderStyle" style="width: 150px">Name  
                    </td>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <asp:Label ID="drpReScrubNurse" runat="server" CssClass="label" Width="205px"></asp:Label>
                    </td>
                </tr>
               
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">Date & Time
                    </td>
                    <td class="lblCaption1 BoldStyle BorderStyle">

                        <asp:Label ID="txtReScrubNurseDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:Label>

                    </td>
                </tr>

            </table>



        </td>
        <td>

            <span class="lblCaption1">Releiver Circulatory Nurse</span>
            <table width="100%">
                <tr>

                    <td class="lblCaption1 BoldStyle BorderStyle" style="width: 150px">Name  
                    </td>
                    <td class="lblCaption1 BoldStyle BorderStyle">
                        <asp:Label ID="drpReCirculNurse" runat="server" CssClass="label" Width="205px"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1 BoldStyle BorderStyle">Date & Time
                    </td>
                    <td class="lblCaption1 BoldStyle BorderStyle">

                        <asp:Label ID="txtReCirculNurseDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:Label>


                    </td>
                </tr>

            </table>
           
        </td>

    </tr>

</table>
