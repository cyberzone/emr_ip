﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;


namespace EMR_IP.WebReports
{
    public partial class IntraOperativeNursingReport : System.Web.UI.UserControl
    {

        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("IntraOperativeNursingReport." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindIntraOperativeNursing()
        {
            IP_IntraOperativeNursingRecord objIONS = new IP_IntraOperativeNursingRecord();
            DataSet DS = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " and  IONR_BRANCH_ID = '" + Convert.ToString(Session["Branch_ID"]) + "' AND IONR_ID=" + Convert.ToString(Session["EMR_ID"]);
            DS = objIONS.InterOperNursRecordGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {
                    txtInterOperDate.Text = Convert.ToString(DR["IONR_DATEDesc"]);


                    txtPreOperDiag.Text = Convert.ToString(DR["IONR_PRE_OPER_DIAG"]);
                    txtPostOperDiag.Text = Convert.ToString(DR["IONR_POST_OPER_DIAG"]);
                    txtOperProcPerf.Text = Convert.ToString(DR["IONR_OPER_PROCEDURE"]);


                    drpSurgeon.Text = Convert.ToString(DR["IONR_SURGEN"]);
                    drpAnaesthetist.Text = Convert.ToString(DR["IONR_ANAESTHETIST"]);

                    drpAnesType.Text = Convert.ToString(DR["IONR_ANESTHESIA_TYPE"]);
                    drpAnesClassifi.Text = Convert.ToString(DR["IONR_ANESTHESIA_CLASS"]);


                    radInfoMedicFood.SelectedValue = Convert.ToString(DR["IONR_PT_IDENTIFI"]);

                    string strAllergies = Convert.ToString(DR["IONR_ALLERGIES"]);

                    string[] arrAllergies = strAllergies.Split('|');

                    if (arrAllergies.Length > 0)
                    {

                        if (arrAllergies[0] == radAllergiesYes.Value)
                        {
                            radAllergiesYes.Checked = true;
                        }
                        else if (arrAllergies[0] == radAllergiesNo.Value)
                        {
                            radAllergiesNo.Checked = true;
                        }
                        if (arrAllergies.Length > 1)
                        {
                            txtAlergies.Value = arrAllergies[1];
                        }
                    }


                    string strIONR_NPO = Convert.ToString(DR["IONR_NPO"]);

                    string[] arrIONR_NPO = strIONR_NPO.Split('|');

                    if (arrIONR_NPO.Length > 0)
                    {

                        if (arrIONR_NPO[0] == radNPOYes.Value)
                        {
                            radNPOYes.Checked = true;
                        }
                        else if (arrIONR_NPO[0] == radNPONo.Value)
                        {
                            radNPONo.Checked = true;
                        }
                        if (arrIONR_NPO.Length > 1)
                        {
                            txtNPO.Value = arrIONR_NPO[1];
                        }
                    }




                    string strIONR_OPER_CONSENT = Convert.ToString(DR["IONR_OPER_CONSENT"]);

                    string[] arrIONR_OPER_CONSENT = strIONR_OPER_CONSENT.Split('|');

                    if (arrIONR_OPER_CONSENT.Length > 0)
                    {

                        if (arrIONR_OPER_CONSENT[0] == radOperConYes.Value)
                        {
                            radOperConYes.Checked = true;
                        }
                        else if (arrIONR_OPER_CONSENT[0] == radOperConNo.Value)
                        {
                            radOperConNo.Checked = true;
                        }
                        if (arrIONR_OPER_CONSENT.Length > 1)
                        {
                            txtOperCon.Value = arrIONR_OPER_CONSENT[1];
                        }
                    }





                    string strIONR_TIME_OUT = Convert.ToString(DR["IONR_TIME_OUT"]);

                    string[] arrIONR_TIME_OUT = strIONR_TIME_OUT.Split('|');

                    if (arrIONR_TIME_OUT.Length > 0)
                    {

                        if (arrIONR_TIME_OUT[0] == radTimeOutYes.Value)
                        {
                            radTimeOutYes.Checked = true;
                        }
                        else if (arrIONR_TIME_OUT[0] == radTimeOutNo.Value)
                        {
                            radTimeOutNo.Checked = true;
                        }
                        if (arrIONR_TIME_OUT.Length > 1)
                        {
                            txtTimeOut.Value = arrIONR_TIME_OUT[1];
                        }
                    }

                    string strIONR_VARIFI_OPER_CHECKLIST = Convert.ToString(DR["IONR_VARIFI_OPER_CHECKLIST"]);
                    if (strIONR_VARIFI_OPER_CHECKLIST == radVarifOperCheckList.Value)
                    {
                        radVarifOperCheckList.Checked = true;
                    }

                    txtAssComment.Text = Convert.ToString(DR["IONR_ASSESS_COMMENT"]);

                    drpCaseClasification.Text = Convert.ToString(DR["IONR_CASE_CLASSIFI"]);

                    drpCaseType.Text = Convert.ToString(DR["IONR_CASE_TYPE"]);

                    txtIntubationTime.Text = Convert.ToString(DR["IONR_INTUBATION_TIME"]);

                    txtIncisionTime.Text = Convert.ToString(DR["IONR_NCISION_TIME"]);


                    txtExtubationTime.Text = Convert.ToString(DR["IONR_EXTUBATION_TIME"]);


                    string strIONR_HAIR_REMOVAL = Convert.ToString(DR["IONR_HAIR_REMOVAL"]);

                    if (strIONR_HAIR_REMOVAL == radHairRemovalYes.Value)
                    {
                        radHairRemovalYes.Checked = true;
                    }
                    else if (strIONR_HAIR_REMOVAL == radHairRemovalNo.Value)
                    {
                        radHairRemovalNo.Checked = true;
                    }

                    else if (strIONR_HAIR_REMOVAL == radClipped.Value)
                    {
                        radClipped.Checked = true;
                    }

                    txtDrainspacking.Text = Convert.ToString(DR["IONR_DRAINS_PACKING"]);

                    txtDrainsLocation.Text = Convert.ToString(DR["IONR_DRAINS_LOCATION"]);



                    string strIONR_CLIPPER = Convert.ToString(DR["IONR_CLIPPER"]);

                    if (strIONR_CLIPPER == radClipperYes.Value)
                    {
                        radClipperYes.Checked = true;
                    }
                    else if (strIONR_CLIPPER == radClipperNo.Value)
                    {
                        radClipperNo.Checked = true;
                    }


                    drpSkinCondition.Text = Convert.ToString(DR["IONR_SKIN_CONDITION"]).Replace("?", "");

                    txtDressing.Text = Convert.ToString(DR["IONR_DRESSING"]);



                    string strIONR_SPONGE_COUNT = Convert.ToString(DR["IONR_SPONGE_COUNT"]);

                    string[] arrIONR_SPONGE_COUNT = strIONR_SPONGE_COUNT.Split('|');

                    if (arrIONR_SPONGE_COUNT.Length > 0)
                    {

                        txtSponge1.Text = arrIONR_SPONGE_COUNT[0];
                        if (arrIONR_SPONGE_COUNT.Length > 1)
                        {
                            txtSponge2.Text = arrIONR_SPONGE_COUNT[1];
                        }

                    }

                    string strIONR_GAUZE_COUNT = Convert.ToString(DR["IONR_GAUZE_COUNT"]);

                    string[] arrIONR_GAUZE_COUNT = strIONR_GAUZE_COUNT.Split('|');

                    if (arrIONR_GAUZE_COUNT.Length > 0)
                    {

                        txtGauze1.Text = arrIONR_GAUZE_COUNT[0];
                        if (arrIONR_GAUZE_COUNT.Length > 1)
                        {
                            txtGauze2.Text = arrIONR_GAUZE_COUNT[1];
                        }

                    }

                    string strIONR_PAENUT_COUNT = Convert.ToString(DR["IONR_PAENUT_COUNT"]);

                    string[] arrIONR_PAENUT_COUNT = strIONR_PAENUT_COUNT.Split('|');

                    if (arrIONR_PAENUT_COUNT.Length > 0)
                    {

                        txtPaenut1.Text = arrIONR_PAENUT_COUNT[0];
                        if (arrIONR_PAENUT_COUNT.Length > 1)
                        {
                            txtPaenut2.Text = arrIONR_PAENUT_COUNT[1];
                        }

                    }


                    string strIONR_NEEDLE_COUNT = Convert.ToString(DR["IONR_NEEDLE_COUNT"]);

                    string[] arrIONR_NEEDLE_COUNT = strIONR_NEEDLE_COUNT.Split('|');

                    if (arrIONR_NEEDLE_COUNT.Length > 0)
                    {

                        txtNeedle1.Text = arrIONR_NEEDLE_COUNT[0];
                        if (arrIONR_NEEDLE_COUNT.Length > 1)
                        {
                            txtNeedle2.Text = arrIONR_NEEDLE_COUNT[1];
                        }

                    }

                    string strIONR_INSTRUMENTS_COUNT = Convert.ToString(DR["IONR_INSTRUMENTS_COUNT"]);

                    string[] arrIONR_INSTRUMENTS_COUNT = strIONR_INSTRUMENTS_COUNT.Split('|');

                    if (arrIONR_INSTRUMENTS_COUNT.Length > 0)
                    {

                        txtInstruments1.Text = arrIONR_INSTRUMENTS_COUNT[0];
                        if (arrIONR_INSTRUMENTS_COUNT.Length > 1)
                        {
                            txtInstruments2.Text = arrIONR_INSTRUMENTS_COUNT[1];
                        }

                    }

                    string strIONR_COTTON_BALLS_COUNT = Convert.ToString(DR["IONR_COTTON_BALLS_COUNT"]);

                    string[] arrIONR_COTTON_BALLS_COUNT = strIONR_COTTON_BALLS_COUNT.Split('|');

                    if (arrIONR_COTTON_BALLS_COUNT.Length > 0)
                    {

                        txtCottonBalls1.Text = arrIONR_COTTON_BALLS_COUNT[0];
                        if (arrIONR_COTTON_BALLS_COUNT.Length > 1)
                        {
                            txtCottonBalls2.Text = arrIONR_COTTON_BALLS_COUNT[1];
                        }
                    }



                    string strIONR_COUNT_CORRECT = Convert.ToString(DR["IONR_COUNT_CORRECT"]);
                    if (strIONR_COUNT_CORRECT == chkCountCorYes.Value)
                    {
                        chkCountCorYes.Checked = true;
                    }
                    string strIONR_COUNT_CORRECT_SURG_INF = Convert.ToString(DR["IONR_COUNT_CORRECT_SURG_INF"]);
                    if (strIONR_COUNT_CORRECT_SURG_INF == chkCountCorSurgeonInformed.Value)
                    {
                        chkCountCorSurgeonInformed.Checked = true;
                    }


                    string strIONR_COUNT_INCORRECT = Convert.ToString(DR["IONR_COUNT_INCORRECT"]);
                    if (strIONR_COUNT_INCORRECT == chkCountincorYes.Value)
                    {
                        chkCountincorYes.Checked = true;
                    }
                    string strIONR_COUNT_INCORRECT_SURG_INF = Convert.ToString(DR["IONR_COUNT_CORRECT_SURG_INF"]);
                    if (strIONR_COUNT_INCORRECT_SURG_INF == chkCountincorSurgeonInformed.Value)
                    {
                        chkCountincorSurgeonInformed.Checked = true;
                    }


                    string strIONR_CORRECTIVE_ACTION = Convert.ToString(DR["IONR_CORRECTIVE_ACTION"]);
                    if (strIONR_CORRECTIVE_ACTION == radCorrectiveActionTaken.Value)
                    {
                        radCorrectiveActionTaken.Checked = true;
                    }

                    txtCorrectiveComment.Text = Convert.ToString(DR["IONR_CORRECTIVE_COMMENT"]);


                    string strIONR_SPECIMEN = Convert.ToString(DR["IONR_SPECIMEN"]);

                    if (strIONR_SPECIMEN == radpecimenYes.Value)
                    {
                        radpecimenYes.Checked = true;
                    }
                    else if (strIONR_SPECIMEN == radSpecimenNo.Value)
                    {
                        radSpecimenNo.Checked = true;
                    }


                    drpSpecimenType.Text = Convert.ToString(DR["IONR_SPECIMEN_TYPE"]);
                    drpPatientTransTo.Text = Convert.ToString(DR["IONR_PATIENT_TRANS_TO"]);


                    drpPosition.Text = Convert.ToString(DR["IONR_POSITION"]);
                    drpPositionalAide.Text = Convert.ToString(DR["IONR_POSITIONAL_AIDE"]);


                    string strIONR_SAFETY_STRAP_ON = Convert.ToString(DR["IONR_SAFETY_STRAP_ON"]);

                    if (strIONR_SAFETY_STRAP_ON == radSafetyStrapYes.Value)
                    {
                        radSafetyStrapYes.Checked = true;
                    }
                    else if (strIONR_SAFETY_STRAP_ON == radSafetyStrapNo.Value)
                    {
                        radSafetyStrapNo.Checked = true;
                    }

                    string strIONR_SAFETY_STRAP_DESC = Convert.ToString(DR["IONR_SAFETY_STRAP_DESC"]);

                    string[] arrIONR_SAFETY_STRAP_DESC = strIONR_SAFETY_STRAP_DESC.Split('|');

                    if (arrIONR_SAFETY_STRAP_DESC.Length > 0)
                    {
                        if (arrIONR_SAFETY_STRAP_DESC[0] == chkLeftArmStraps.Value)
                        {
                            chkLeftArmStraps.Checked = true;
                        }
                        if (arrIONR_SAFETY_STRAP_DESC[0] == chkRightArmStraps.Value)
                        {
                            chkRightArmStraps.Checked = true;
                        }
                        if (arrIONR_SAFETY_STRAP_DESC.Length > 1)
                        {
                            if (arrIONR_SAFETY_STRAP_DESC[1] == chkRightArmStraps.Value)
                            {
                                chkRightArmStraps.Checked = true;
                            }
                        }
                    }



                    drpSafetyStrapPositionedBy.Text = Convert.ToString(DR["IONR_POSITIONED_BY"]);

                    string strIONR_ELECTROSURGICAL_UNIT = Convert.ToString(DR["IONR_ELECTROSURGICAL_UNIT"]);

                    if (strIONR_ELECTROSURGICAL_UNIT == radElectroUnitYes.Value)
                    {
                        radElectroUnitYes.Checked = true;
                    }
                    else if (strIONR_ELECTROSURGICAL_UNIT == radElectroUnitNo.Value)
                    {
                        radElectroUnitNo.Checked = true;
                    }




                    txtRetElectroLocation.Text = Convert.ToString(DR["IONR_RET_ELECTRODE_LOCATION"]);
                    txtSettingsCut.Text = Convert.ToString(DR["IONR_ELECT_SURG_CUT"]);
                    txtSettingsCoag.Text = Convert.ToString(DR["IONR_ELECT_SURG_COAG"]);
                    txtBipolar.Text = Convert.ToString(DR["IONR_ELECT_BIPOLAR"]);
                    drpElectroUnitAppliedBy.Text = Convert.ToString(DR["IONR_ELECT_APPLIEDBY"]);


                    txtSkinCondition.Text = Convert.ToString(DR["IONR_SKIN_COND_UNDER_PAD"]);


                    txtTourniTimeUpDate.Text = Convert.ToString(DR["IONR_TOURNIQUETS_TIME_UP_Desc"]);



                    txtTourniTimeDownDate.Text = Convert.ToString(DR["IONR_TOURNIQUETS_TIME_DOWNDesc"]);


                    txtSkinStatusBeforePlacement.Text = Convert.ToString(DR["IONR_SKIN_STATUS_BE_PLACEMENT"]);
                    txtSkinStatusAfterPlacement.Text = Convert.ToString(DR["IONR_SKIN_STATUS_AF_PLACEMENT"]);
                    txtSiteLocation.Text = Convert.ToString(DR["IONR_SKIN_SITE_LOCATION"]);
                    drpSkinAppliedBy.Text = Convert.ToString(DR["IONR_SKIN_APPLIED_BY"]);




                    txtUriCatheterFolyCathno.Text = Convert.ToString(DR["IONR_FOLY_CATH_NO"]);
                    drpUriCatheterInsertedBy.Text = Convert.ToString(DR["IONR_URIN_CAT_INSERTEDBY"]);
                    drpUrineReturned.Text = Convert.ToString(DR["IONR_URINE_RETURNED"]);
                    drpPreAnesthesiologist.Text = Convert.ToString(DR["IONR_PER_ANESTHESIOLOGIST"]);
                    txtTotalBloodProd.Text = Convert.ToString(DR["IONR_TOTAL_BLOOD_PROD"]);
                    txtTotalIVFluids.Text = Convert.ToString(DR["IONR_TOTAL_IV_FLUIDS"]);
                    txtUrinaryDrainage.Text = Convert.ToString(DR["IONR_URINARY_DRAINAGE"]);
                    txtEstBloodLoss.Text = Convert.ToString(DR["IONR_ESTI_BLOOD_LOSS"]);
                    txtOutPutOthers.Text = Convert.ToString(DR["IONR_OUTPUT_OTHERS"]);


                    txtProsthesisLocation.Text = Convert.ToString(DR["IONR_PROSTHESIS_LOCA"]);
                    txtProsthesisSize.Text = Convert.ToString(DR["IONR_PROSTHESIS_SIZE"]);
                    txtProsthesisSlNo.Text = Convert.ToString(DR["IONR_PROSTHESIS_SL_NO"]);
                    txtCarmManufacturer.Text = Convert.ToString(DR["IONR_PROSTHESIS_MANUFA"]);

                    string strIONR_XRAY_CARAM = Convert.ToString(DR["IONR_XRAY_CARAM"]);


                    if (strIONR_XRAY_CARAM == radCarmYes.Value)
                    {
                        radCarmYes.Checked = true;
                    }
                    else if (strIONR_XRAY_CARAM == radCarmNo.Value)
                    {
                        radCarmNo.Checked = true;
                    }
                    else if (strIONR_XRAY_CARAM == radCarmNA.Value)
                    {
                        radCarmNA.Checked = true;
                    }


                    txtLaser.Text = Convert.ToString(DR["IONR_XRAY_LASER"]);
                    txtOthersMicroscope.Text = Convert.ToString(DR["IONR_MICROSCOPE"]);
                    txtOthersOthers.Text = Convert.ToString(DR["IONR_OTHER"]);
                    txtComment.Text = Convert.ToString(DR["IONR_COMMENT"]);

                    drpScrubNurse.Text = Convert.ToString(DR["IONR_SCRUB_NURSE"]);


                    txtScrubNurseDate.Text = Convert.ToString(DR["IONR_SCRUB_NURSE_DATEDesc"]);






                    drpCirculNurse.Text = Convert.ToString(DR["IONR_CIRCUL_NURSE"]);

                    txtCirculNurseDate.Text = Convert.ToString(DR["IONR_CIRCUL_NURSE_DATEDesc"]);




                    drpReScrubNurse.Text = Convert.ToString(DR["IONR_REL_SCRUB_NURSE"]);

                    txtReScrubNurseDate.Text = Convert.ToString(DR["IONR_REL_SCRUB_NURSE_DATEDesc"]);


                    drpReCirculNurse.Text = Convert.ToString(DR["IONR_REL_CIRCUL_NURSE"]);

                    txtReCirculNurseDate.Text = Convert.ToString(DR["IONR_REL_CIRCUL_NURSE_DATEDesc"]);


                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                   

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}