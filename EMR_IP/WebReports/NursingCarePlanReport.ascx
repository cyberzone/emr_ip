﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NursingCarePlanReport.ascx.cs" Inherits="EMR_IP.WebReports.NursingCarePlanReport" %>

<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1">Nursing Care Plan </span>



<asp:GridView ID="gvNursing" runat="server" AutoGenerateColumns="False"
    EnableModelValidation="True" Width="100%">
    <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
    <RowStyle CssClass="lblCaption1" HorizontalAlign="Center" />
    <Columns>
        <asp:TemplateField HeaderText="Date & TIme">
            <ItemTemplate>
               
                    <asp:Label ID="lblNursingID" CssClass="GridRow" runat="server" Text='<%# Bind("INCP_NURC_ID") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lblgvAssDate" CssClass="GridRow" runat="server" Text='<%# Bind("INCP_DATEDesc") %>'></asp:Label>
                    <asp:Label ID="lblgvAssTime" CssClass="GridRow" runat="server" Text='<%# Bind("INCP_DATE_TIMEDesc") %>'></asp:Label>
                
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Assessment">
            <ItemTemplate>
                 
                    <asp:Label ID="lblgvAssessment" CssClass="GridRow" runat="server" Text='<%# Bind("INCP_ASSESSMENT") %>'></asp:Label>

                
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Nursing Diagnosis">
            <ItemTemplate>
                
                    <asp:Label ID="lblgvDiagnosis" CssClass="GridRow" runat="server" Text='<%# Bind("INCP_DIAGNOSIS") %>'></asp:Label>

                
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Implementation">
            <ItemTemplate>
                
                    <asp:Label ID="lblgvImplementation" CssClass="GridRow" runat="server" Text='<%# Bind("INCP_IMPLEMENTATION") %>'></asp:Label>
                 
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Evaluation">
            <ItemTemplate>
                
                    <asp:Label ID="lblgvEvaluation" CssClass="GridRow" runat="server" Text='<%# Bind("INCP_EVALUATION") %>'></asp:Label>
              
            </ItemTemplate>
        </asp:TemplateField>

    </Columns>


</asp:GridView>
