﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;
namespace EMR_IP.WebReports
{
    public partial class NursingCarePlanReport : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

        IP_NursingCarePlan objNurs = new IP_NursingCarePlan();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("NursingCarePlanReport." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

      public  void BindNursingCarePlan()
        {
            DataSet DS = new DataSet();
            objNurs = new IP_NursingCarePlan();



            string Criteria = " 1=1 and INCP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND INCP_ID='" + EMR_ID + "'";

            DS = objNurs.NurseCarePlanGet(Criteria);


            gvNursing.DataBind();
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvNursing.DataSource = DS;
                gvNursing.DataBind();

            }

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            try
            {
                if (!IsPostBack)
                {

                     
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}