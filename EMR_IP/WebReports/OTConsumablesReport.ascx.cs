﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;


namespace EMR_IP.WebReports
{
    public partial class OTConsumablesReport : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }
        IP_OTConsumables objOT = new IP_OTConsumables();
        DataSet DS = new DataSet();


        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("OTConsumablesReport." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        public void BindOTConsumables()
        {

            objOT = new IP_OTConsumables();

            string Criteria = " 1=1 ";
            Criteria += " AND IPOTC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "'";

            Criteria += " AND IPOTC_ID='" + EMR_ID + "'";

          

            DS = new DataSet();


            DS = objOT.OTConsumablesGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                gvOTConsum.DataSource = DS;
                gvOTConsum.DataBind();

            }
            else
            {
                gvOTConsum.DataBind();
            }

            CalculateTotalAmount();
      

        }

        void CalculateTotalAmount()
        {
            Decimal decTotalAmount = 0;
            for (int intCurRow = 0; intCurRow < gvOTConsum.Rows.Count; intCurRow++)
            {
                Label lblgvAmount = (Label)gvOTConsum.Rows[intCurRow].FindControl("lblgvAmount");

                if (lblgvAmount.Text.Trim() != "")
                {
                    decTotalAmount += Convert.ToDecimal(lblgvAmount.Text);
                }
            }


            txtTotalAmount.Text = Convert.ToString(decTotalAmount);


        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {
                    

                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}