﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OperationNotesReport.ascx.cs" Inherits="EMR_IP.WebReports.OperationNotesReport" %>

<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1">Operation Notes</span>



<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Date :
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblORDate" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle"></td>
        <td class="lblCaption1 BorderStyle"></td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Name of Surgeon
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblSurgeon" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Assistants
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblAssistants" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>


    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">PreOperative Diagnosis
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblPreOperDiag" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">PostOperative Diagnosis
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblPostOperDiag" runat="server" CssClass="lblCaption1"></asp:Label>


        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Operative procedure Performed
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblOperProcPerf" runat="server" CssClass="lblCaption1"></asp:Label>


        </td>
        <td class="lblCaption1 BorderStyle"></td>
        <td class="lblCaption1 BorderStyle"></td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Name of Anaesthetist
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblAnaesthetist" runat="server" CssClass="lblCaption1"></asp:Label>


        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Anesthesia Type 
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblAnesType" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">OR No.
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblORRooms" runat="server" CssClass="lblCaption1"></asp:Label>



        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">SWAB & Instrument Check
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:CheckBox ID="chkSWABInstrumentCheck" runat="server" CssClass="lblCaption1" Width="200px" Enabled="false" Text="SWAB & Instrument Check" />
        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Theatre Sisters 
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblNurseDtls" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle"></td>
        <td class="lblCaption1 BorderStyle"></td>


    </tr>

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Specimens to Pathology 
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblSpecimensPathology" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>

        <td class="lblCaption1" style="width: 150px;">Time Started &nbsp;
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:Label ID="lblStarted" runat="server" CssClass="lblCaption1"></asp:Label>
            &nbsp; &nbsp;
                            Finished Time  
                      <asp:Label ID="lblFinished" runat="server" CssClass="lblCaption1"></asp:Label>
            &nbsp; &nbsp;
                       
        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">I. V. Transfusion/lnfusion
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:Label ID="lblIVTransFusion" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">Drains
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblDrains" runat="server" CssClass="lblCaption1"></asp:Label>


        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Sedation/Antibiolics/Drugs
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblSedation" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">Catheters
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblCatheters" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Procedure and Findings
        </td>

        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblProcedureFindings" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle"></td>
        <td class="lblCaption1 BorderStyle"></td>
    </tr>
</table>
<span class="lblCaption1">Surgeon Signature</span>
<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>

        <td class="lblCaption1 BoldStyle BorderStyle">Name  
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblSigSurgeon" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">Date & Time  
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblSigSurgeonDt" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
    </tr>
</table>

<span class="lblCaption1">Assistant Signature</span>
<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>

        <td class="lblCaption1 BoldStyle BorderStyle">Name  
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblSigAssistant" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">Date & Time  
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="lblSigAssistantDate" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
    </tr>
</table>
