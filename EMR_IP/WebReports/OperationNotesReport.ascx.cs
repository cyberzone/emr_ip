﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;

namespace EMR_IP.WebReports
{
    public partial class OperationNotesReport : System.Web.UI.UserControl
    {

        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }


        DataSet DS = new DataSet();

        CommonBAL objCom = new CommonBAL();
        IP_OperationNotes objOper = new IP_OperationNotes();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("OperationNotesReport." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        public void BindOperationNotes()
        {
            DS = new DataSet();
            objOper = new IP_OperationNotes();
            string Criteria =  " ION_ID ='" + EMR_ID + "'";

            DS = objOper.OperationNotesGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    lblORDate.Text = Convert.ToString(DR["ION_DATEDesc"]);
                    lblSurgeon.Text = Convert.ToString(DR["ION_SURGEON_ID"]);
                    lblAssistants.Text = Convert.ToString(DR["ION_ASSISTANTS"]);


                    lblPreOperDiag.Text = Convert.ToString(DR["ION_PREOPERATIVE_DIAGNOSIS"]);
                    lblPostOperDiag.Text = Convert.ToString(DR["ION_POSTOPERATIVE_DIAGNOSIS"]);
                    lblOperProcPerf.Text = Convert.ToString(DR["ION_OPERATION_PERFORMED"]);
                    lblAnaesthetist.Text = Convert.ToString(DR["ION_ANAESTHETIST_ID"]);
                    lblAnesType.Text = Convert.ToString(DR["ION_ANAESTHETIST_TYPE"]);
                    lblORRooms.Text = Convert.ToString(DR["ION_OR_NO"]);


                    lblNurseDtls.Text = Convert.ToString(DR["ION_THEATRE_SISTERS"]);
                    lblProcedureFindings.Text = Convert.ToString(DR["ION_PROCEDURE_FINDINGS"]);
                    lblSpecimensPathology.Text = Convert.ToString(DR["ION_SPECIMENTS_PATHOLOGY"]);
                    chkSWABInstrumentCheck.Checked = Convert.ToBoolean(DR["ION_SWAB_INSTRUMENT_CHECK"]);

                   lblStarted.Text  = Convert.ToString(DR["ION_STARTED_TIMEDesc"]);
                     

                    lblFinished.Text  = Convert.ToString(DR["ION_FINISHED_TIMEDesc"]);
                     


                    lblIVTransFusion.Text = Convert.ToString(DR["ION_IV_TRANSFUSION_INFUSION"]);
                    lblDrains.Text = Convert.ToString(DR["ION_DRAINS"]);
                    lblSedation.Text = Convert.ToString(DR["ION_SEDATION_ANTIBIOTICS_DRUGS"]);
                    lblCatheters.Text = Convert.ToString(DR["ION_CATHETERS"]);

                    lblSigSurgeon.Text = Convert.ToString(DR["ION_SURGEON_SIGN"]);
                    lblSigSurgeonDt.Text = Convert.ToString(DR["ION_SURGEON_SIGN_DATE"]);


                    lblSigAssistant.Text = Convert.ToString(DR["ION_ASSISTANTS_SIGN"]);
                    lblSigAssistantDate.Text = Convert.ToString(DR["ION_ASSISTANTS_SIGN_DATE"]);

                }
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            try
            {
                if (!IsPostBack)
                {
             
                    
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }
    }
}