﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;
namespace EMR_IP.WebReports
{
    public partial class PainAssessmentReport : System.Web.UI.UserControl
    {

        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

 IP_PainAssessment    objPainAss = new IP_PainAssessment();


        void BindAssessmentGrid()
        {
            DataSet DS = new DataSet();
             objPainAss = new IP_PainAssessment();


             string Criteria = " 1=1 AND IPAD_ID='" + EMR_ID + "'";

            DS = objPainAss.PainAssessmentDtlsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                gvAssessment.DataBind();
                if (DS.Tables[0].Rows.Count > 0)
                {
                    gvAssessment.DataSource = DS;
                    gvAssessment.DataBind();

                }
            }
        }

        void BindPainAssessment()
        {
            DataSet DS = new DataSet();
            objPainAss = new IP_PainAssessment();


            string Criteria = " 1=1 and IPA_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPA_ID='" + EMR_ID + "'";

            DS = objPainAss.PainAssessmentGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    string strPainRadiationNo = Convert.ToString(DR["IPA_PAIN_RADIATION"]);
                    string[] arrPainRadiationNo = strPainRadiationNo.Split('|');

                    if (arrPainRadiationNo.Length >= 0)
                    {

                        if (arrPainRadiationNo[0] == "No")
                        {
                            radPainRadiationNo.Checked = true;
                        }
                        else if (arrPainRadiationNo[0] == "Yes")
                        {
                            radPainRadiationYes.Checked = true;
                        }
                        if (arrPainRadiationNo.Length > 0)
                        {
                            txtCurrentMedications.Text = arrPainRadiationNo[1];
                        }

                    }
                    radPainPattern.SelectedValue = Convert.ToString(DR["IPA_PAIN_PATTERN"]);
                    radPainOnset.SelectedValue = Convert.ToString(DR["IPA_PAIN_ONSET"]);

                    txtPainCauses.Text = Convert.ToString(DR["IPA_PAIN_CAUSES"]);
                    txtPainRelieves.Text = Convert.ToString(DR["IPA_PAIN_RELIEVES"]);
                    txtEffectsOnActivities.Text = Convert.ToString(DR["IPA_EFFECTS_ONACTIVITIES"]);
                    txtMedication.Text = Convert.ToString(DR["IPA_MEDICATION"]);




                    string strNonMedication = Convert.ToString(DR["IPA_NONMEDICATION"]);
                    string[] arrNonMedication = strNonMedication.Split('|');



                    for (Int32 i = 0; i <= chkNonMedication.Items.Count - 1; i++)
                    {
                        for (Int32 j = 0; j <= arrNonMedication.Length - 1; j++)
                        {
                            if (arrNonMedication[j] == chkNonMedication.Items[i].Value)
                            {
                                chkNonMedication.Items[i].Selected = true;
                                goto EndArrLoop;
                            }

                        }

                    EndArrLoop: ;

                    }

                    txtOtherInterventions.Text = Convert.ToString(DR["IPA_OTHER_INTERVENTIONS"]);
                    txtInterventionDate.Text = Convert.ToString(DR["IPA_INTERVENTION_DateDesc"]) + " " + Convert.ToString(DR["IPA_INTERVENTION_TIMEDesc"]);

 

                    string strPAIN_ASSESS_SCALE = Convert.ToString(DR["IPA_PAIN_ASSESS_SCALE"]);
                    string[] arrPAIN_ASSESS_SCALE = strPAIN_ASSESS_SCALE.Split('|');


                    if (arrPAIN_ASSESS_SCALE.Length >= 0)
                    {
                        for (Int32 j = 0; j <= arrPAIN_ASSESS_SCALE.Length - 1; j++)
                        {
                            if (arrPAIN_ASSESS_SCALE[j] == "For verbal patients explain the Faces Pain Scale and ask thepatient to rate his/her pain")
                            {
                                chkVerbal.Checked = true;
                            }

                            if (arrPAIN_ASSESS_SCALE[j] == "For non-verbal / pre-verbal rate the patients` pain using the Faces Pain Scale")
                            {
                                chkNonVerbal.Checked = true;
                            }

                            if (arrPAIN_ASSESS_SCALE[j] == "For Neonates use NIPS pain scoring scale")
                            {
                                chkNeonates.Checked = true;
                            }

                        }

                    }


                }

            }

        }

        void BindReAssessmentGrid()
        {
            DataSet DS = new DataSet();
            objPainAss = new IP_PainAssessment();


            string Criteria = " 1=1 AND IPNAD_ID='" + EMR_ID + "'";

            DS = objPainAss.PainReAssessmentDtlsGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {

                gvReAssessment.DataBind();
                if (DS.Tables[0].Rows.Count > 0)
                {
                    gvReAssessment.DataSource = DS;
                    gvReAssessment.DataBind();

                }
            }
        }


        public void BindPainAssessmentData()
        {
            BindAssessmentGrid();
            BindPainAssessment();
            BindReAssessmentGrid();
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {

               
            }
        }
    }
}