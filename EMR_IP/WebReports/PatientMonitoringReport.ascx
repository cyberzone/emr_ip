﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PatientMonitoringReport.ascx.cs" Inherits="EMR_IP.WebReports.PatientMonitoringReport" %>


<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1">Patient Monitoring </span>
<br />
<span class="lblCaption1">Doctor Progress Notes </span>
<asp:GridView ID="gvProgNotes" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvProgNotes_RowDataBound"
    EnableModelValidation="True" Width="100%" PageSize="200">
    <HeaderStyle CssClass="GridHeader" Font-Bold="True" />
    <RowStyle CssClass="GridRow" />
    <Columns>
        <asp:TemplateField HeaderText="SL.No">
            <ItemTemplate>

                <asp:Label ID="lblProgId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPN_MON_ID") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblStatus" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPN_STATUS") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblProgDate" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPN_MONITORING_DATEDesc") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblProgTime" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("MonitoringTime") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPN_MON_ID") %>' ></asp:Label>
                <asp:Label ID="lblDoctorID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPN_STAFF_ID") %>' Visible="false"></asp:Label>

            </ItemTemplate>

            <HeaderStyle Width="40px" />

        </asp:TemplateField>

        <asp:TemplateField HeaderText="Progress Notes">
            <ItemTemplate>

                <%# Eval("IPN_NOTES") %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="User Name">
            <ItemTemplate>
                <%# Eval("CreatedStaffName") %>
            </ItemTemplate>
            <HeaderStyle Width="200px" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Date & Time">
            <ItemTemplate>
                <%# Eval("HHN_MONITORING_DATE_TimeDesc") %>
            </ItemTemplate>
            <HeaderStyle Width="130px" />
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Doctor" HeaderStyle-Width="130px">
            <ItemTemplate>
                <%# Eval("IPN_STAFF_NAME") %>
            </ItemTemplate>
        </asp:TemplateField>

    </Columns>
    <PagerStyle CssClass="GridHeader" Font-Bold="True" HorizontalAlign="Center" />

</asp:GridView>

<span class="lblCaption1">Nurse Progress Notes </span>



<asp:GridView ID="gvEmerNotes" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvEmerNotes_RowDataBound"
    EnableModelValidation="True" Width="100%" PageSize="200">
    <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
    <RowStyle CssClass="GridRow" />
    <Columns>
        <asp:TemplateField HeaderText="SL.No" HeaderStyle-Width="40px">
            <ItemTemplate>

                <asp:Label ID="lblEmerId" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPN_MON_ID") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblStatus" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPN_STATUS") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblEmerDate" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPN_MONITORING_DATEDesc") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblEmerTime" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("MonitoringTime") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblNurseID" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPN_STAFF_ID") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblSerial" CssClass="GridRow" Width="40px" runat="server" Text='<%# Bind("IPN_MON_ID") %>'></asp:Label>

            </ItemTemplate>

        </asp:TemplateField>

        <asp:TemplateField HeaderText="Progress Notes">
            <ItemTemplate>

                <%# Eval("IPN_NOTES") %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="User Name" HeaderStyle-Width="200px">
            <ItemTemplate>
                <%# Eval("CreatedStaffName") %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Date & Time" HeaderStyle-Width="130px">
            <ItemTemplate>
                <%# Eval("HHN_MONITORING_DATE_TimeDesc") %>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Nurse" HeaderStyle-Width="130px">
            <ItemTemplate>
                <%# Eval("IPN_STAFF_NAME") %>
            </ItemTemplate>
        </asp:TemplateField>



    </Columns>
    <PagerStyle CssClass="GridHeader" Font-Bold="true" HorizontalAlign="Center" />

</asp:GridView>
