﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.WebReports
{
    public partial class PatientMonitoringReport : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

        IP_DischargeSummary objDis = new IP_DischargeSummary();


        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("PatientMonitoringReport." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }

        void HomeCareNotesBind(string strType)
        {

            DataSet ds = new DataSet();
            string Criteria = " 1=1 ";
            Criteria += " AND IPN_TYPE = '" + strType + "'";
            Criteria += " AND IPN_ID = '" + Convert.ToString(Session["EMR_ID"]) + "'";


            IP_Monitoring objPT = new IP_Monitoring();
            ds = objPT.PTMonitoringGet(Criteria);

            if (strType == "DR_PROGRESS")
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvProgNotes.DataSource = ds;
                    gvProgNotes.DataBind();
                }
                else
                {
                    gvProgNotes.DataBind();
                }

            }

            if (strType == "NU_PROGRESS")
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvEmerNotes.DataSource = ds;
                    gvEmerNotes.DataBind();
                }
                else
                {
                    gvEmerNotes.DataBind();
                }

            }




        }

        public void BindPatientMonitoring()
        {
            HomeCareNotesBind("DR_PROGRESS");
            HomeCareNotesBind("NU_PROGRESS");
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            try
            {
                if (!IsPostBack)
                {


                  


                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvProgNotes_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                lblSerial.Text = ((gvProgNotes.PageIndex * gvProgNotes.PageSize) + e.Row.RowIndex + 1).ToString();

                //BELOW CODING FOR STRIKE THROUGH OPTION
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");
             
                if (lblStatus.Text == "InActive")
                {
                    e.Row.Font.Strikeout = true;
                   
                }


            }
        }

        protected void gvEmerNotes_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSerial = (Label)e.Row.FindControl("lblSerial");
                lblSerial.Text = ((gvEmerNotes.PageIndex * gvEmerNotes.PageSize) + e.Row.RowIndex + 1).ToString();


                //BELOW CODING FOR STRIKE THROUGH OPTION
                Label lblStatus = (Label)e.Row.FindControl("lblStatus");
                
                if (lblStatus.Text == "InActive")
                {
                    e.Row.Font.Strikeout = true;
                    
                }
            }
        }

    }
}