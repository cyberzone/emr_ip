﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PreAnesthesiaReport.ascx.cs" Inherits="EMR_IP.WebReports.PreAnesthesiaReport" %>

  <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1">Pre Anesthesia Record</span>
 <table style="width: 100%">
                <tr>
                    <td style="width: 50%; vertical-align: top;">
                        <asp:PlaceHolder ID="plhoPhyAssessmentLeft" runat="server"></asp:PlaceHolder>
                    </td>
                    <td style="width: 50%; vertical-align: top;">
                        <asp:PlaceHolder ID="plhoPhyAssessmentRight" runat="server"></asp:PlaceHolder>
                    </td>
                </tr>
            </table>
