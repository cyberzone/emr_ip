﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.WebReports
{
    public partial class PreAnesthesiaReport : System.Web.UI.UserControl
    {

        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

        DataSet DS = new DataSet();
        CommonBAL objCom = new CommonBAL();
        IP_PreAnesthesia objAnes = new IP_PreAnesthesia();

        DataSet GetSegmentData()
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 AND IPAS_ACTIVE=1 AND IPAS_BRANCH='" + Convert.ToString(Session["Branch_ID"]) + "'";// AND IPAS_TYPE='" + strType + "'";
            // Criteria += " AND  IPAS_POSITION='" + Position + "'";

            DataSet DS = new DataSet();

            DS = objCom.fnGetFieldValue(" * ", "IP_PRE_ANESTHESIA_SEGMENT", Criteria, "IPAS_ORDER");



            return DS;

        }

         DataSet GetSegmentMaster(string SegmentType)
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 AND IPASM_STATUS=1 ";
            Criteria += " and IPASM_TYPE='" + SegmentType + "'";



            DataSet DS = new DataSet();

            DS = objCom.fnGetFieldValue(" * ", "IP_PRE_ANESTHESIA_SEGMENT_MASTER", Criteria, "IPASM_ORDER");



            return DS;





            return DS;

        }

        public void CreateSegCtrls()
        {
            string Position = "";
            DataSet DS = new DataSet();

            DS = GetSegmentData();

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                Boolean boolFieldSet = false;
                Position = Convert.ToString(DR["IPAS_POSITION"]);

                Panel myFieldSet = new Panel();

                myFieldSet.GroupingText = Convert.ToString(DR["IPAS_NAME"]);
                myFieldSet.Style.Add("text-align", "justify");
                myFieldSet.CssClass = "lblCaption1";
                myFieldSet.Width = 500;
                Int32 i = 1;

                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["IPAS_CODE"]));

                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {


                    Boolean _checked = false;
                    string strComment = "";
                    objAnes = new IP_PreAnesthesia();
                    string Criteria2 = "1=1 AND IPASD_ENTRY_FROM='IP' ";
                    Criteria2 += " AND IPASD_TYPE='" + Convert.ToString(DR1["IPASM_TYPE"]).Trim() + "' AND IPASD_FIELD_ID='" + Convert.ToString(DR1["IPASM_FIELD_ID"]).Trim() + "' ";
                    Criteria2 += " AND IPASD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPASD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

                    DataSet DS2 = new DataSet();
                    DS2 = objAnes.PreAnesthesiaSegmentDtlsGet(Criteria2);

                    if (DS2.Tables[0].Rows.Count > 0)
                    {
                        _checked = true;

                        strComment = Convert.ToString(DS2.Tables[0].Rows[0]["IPASD_COMMENT"]).Trim();
                    }


                    if (Convert.ToString(DR1["IPASM_CONTROL_TYPE"]).ToUpper() == "CheckBox".ToUpper())
                    {
                        CheckBox chk = new CheckBox();
                        chk.ID = "chk" + Convert.ToString(DR1["IPASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IPASM_FIELD_ID"]).Trim();
                        chk.Text = Convert.ToString(DR1["IPASM_FIELD_NAME"]).Trim();
                        chk.CssClass = "lblCaption1";
                        chk.Enabled = false;

                        chk.Checked = _checked;

                        if (_checked == true)
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(chk);
                        }
                    }


                    if (Convert.ToString(DR1["IPASM_CONTROL_TYPE"]).ToUpper() == "TextBox".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IPASM_FIELD_NAME"]).Trim() + ":";

                        Literal lit1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };

                        Label txt = new Label();
                        txt.ID = "txt" + Convert.ToString(DR1["IPASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IPASM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["IPASM_FIELD_NAME"]);
                        txt.CssClass = "Label";

                        txt.Text = strComment;

                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(lbl);
                            myFieldSet.Controls.Add(lit1);
                            myFieldSet.Controls.Add(txt);
                        }



                    }



                    if (Convert.ToString(DR1["IPASM_CONTROL_TYPE"]).ToUpper() == "AutoTextBox".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IPASM_FIELD_NAME"]).Trim() + ":";

                        Literal lit1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };

                        Label txt = new Label();
                        txt.ID = "txt" + Convert.ToString(DR1["IPASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IPASM_FIELD_ID"]).Trim();
                       
                        txt.CssClass = "Label";

                        txt.Text = strComment;

                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(lbl);
                            myFieldSet.Controls.Add(lit1);
                            myFieldSet.Controls.Add(txt);

                        }



                    }



                    if (Convert.ToString(DR1["IPASM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IPASM_FIELD_NAME"]).Trim() + ":";
                        lbl.Width = 150;

                        Label txt = new Label();
                        txt.ID = "txt" + Convert.ToString(DR1["IPASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IPASM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["IPASM_FIELD_NAME"]);
                        txt.CssClass = "Label";

                        txt.Text = strComment;
                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(lbl);
                            myFieldSet.Controls.Add(txt);
                        }
                       


                    }




                    if (Convert.ToString(DR1["IPASM_CONTROL_TYPE"]).ToUpper() == "Label".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IPASM_FIELD_NAME"]).Trim();
                        myFieldSet.Controls.Add(lbl);



                    }

                    if (Convert.ToString(DR1["IPASM_CONTROL_TYPE"]) == "DropDownList")
                    {

                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IPASM_FIELD_NAME"]).Trim() + ":    ";
                        lbl.Width = 150;
                         

                        DropDownList DrpList = new DropDownList();
                        DrpList.ID = "drp" + Convert.ToString(DR1["IPASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IPASM_FIELD_ID"]).Trim();
                        DrpList.Width = 200;
                        DrpList.Enabled = false;
                        string strRadFieldName = Convert.ToString(DR1["IPASM_DATA"]);
                        string[] arrRadFieldName = strRadFieldName.Split('|');


                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            DrpList.Items.Add(new ListItem(arrRadFieldName[intRad], arrRadFieldName[intRad]));

                        }

                        DrpList.CssClass = "lblCaption1";

                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            if (DrpList.Items[intRad].Value == strComment)
                            {
                                DrpList.SelectedValue = strComment;
                            }

                        }

                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(lbl);
                            myFieldSet.Controls.Add(DrpList);
                        }

                    }




                    if (Convert.ToString(DR1["IPASM_CONTROL_TYPE"]) == "RadioButtonList")
                    {

                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IPASM_FIELD_NAME"]).Trim() + ":    ";
                        lbl.Width = 150;
                       

                        RadioButtonList RadList = new RadioButtonList();
                        RadList.ID = "radl" + Convert.ToString(DR1["IPASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IPASM_FIELD_ID"]).Trim();
                        RadList.Enabled = false;
                        RadList.RepeatDirection = RepeatDirection.Horizontal;
                        if (DR1.IsNull("IPASM_WIDTH") == false && Convert.ToString(DR1["IPASM_WIDTH"]) != "")
                        {
                            RadList.Width = Convert.ToInt32(DR1["IPASM_WIDTH"]);
                        }
                        else
                        {
                            RadList.Width = 200;
                        }
                        string strRadFieldName = Convert.ToString(DR1["IPASM_DATA"]);
                        string[] arrRadFieldName = strRadFieldName.Split('|');


                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            RadList.Items.Add(new ListItem(arrRadFieldName[intRad], arrRadFieldName[intRad]));

                        }

                        RadList.CssClass = "lblCaption1";

                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            if (RadList.Items[intRad].Value == strComment)
                            {
                                RadList.SelectedValue = strComment;
                            }

                        }

                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(lbl);
                            myFieldSet.Controls.Add(RadList);
                        }

                    }


                    if (Convert.ToString(DR1["IPASM_CONTROL_TYPE"]) == "Image")
                    {

                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["IPASM_FIELD_NAME"]).Trim() + ":    ";
                        lbl.Width = 150;
                       

                        Image img = new Image();
                        img.ID = "img" + Convert.ToString(DR1["IPASM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["IPASM_FIELD_ID"]).Trim();

                        img.ImageUrl = "../Images/" + Convert.ToString(DR1["IPASM_DATA"]).Trim();

                        if (DR1.IsNull("IPASM_WIDTH") == false && Convert.ToString(DR1["IPASM_WIDTH"]) != "")
                        {
                            img.Width = Convert.ToInt32(DR1["IPASM_WIDTH"]);
                        }
                        else
                        {
                            img.Width = 50;
                        }



                      

                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            Literal lit = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                            myFieldSet.Controls.Add(lit);
                        }

                    }



                    if (strComment.Trim() != "")
                    {
                        boolFieldSet = true;
                        Literal litGap = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                        myFieldSet.Controls.Add(litGap);
                    }




                }



                if (Position == "LEFT")
                {
                    if (boolFieldSet == true)
                    {
                        plhoPhyAssessmentLeft.Controls.Add(myFieldSet);
                    }
                }
                else
                {
                    if (boolFieldSet == true)
                    {
                        plhoPhyAssessmentRight.Controls.Add(myFieldSet);
                    }
                }





                i = i + 1;



            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {
                
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }



        public void generateDynamicControls()
        {

            CreateSegCtrls();



        }
    }
}