﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.WebReports
{
    public partial class PreOperativeChecklistReport : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

          CommonBAL  objCom = new CommonBAL();
          IP_PreOperativeChecklist objPreOp = new IP_PreOperativeChecklist();

          void TextFileWriting(string strContent)
          {
              try
              {
                  string strFileName = Server.MapPath("../EMR_IPLog.txt");

                  StreamWriter oWrite;

                  if (File.Exists(strFileName) == true)
                  {
                      oWrite = File.AppendText(strFileName);
                  }
                  else
                  {
                      oWrite = File.CreateText(strFileName);
                      //oWrite.WriteLine(strContent);
                      oWrite.WriteLine();
                  }

                  oWrite.WriteLine("PreOperativeChecklistReport." + strContent);
                  //  oWrite.WriteLine();
                  oWrite.Close();
              }
              catch (Exception ex)
              {

              }

          }

        void BindPreOperativeSegmentMaster()
        {
            string Criteria = " 1=1 and IPSM_STATUS=1 ";
             objCom = new CommonBAL();

            DataSet DS = new DataSet();
            DS = objCom.fnGetFieldValue("*", "IP_PREOPERATIVE_SEGMENT_MASTER", Criteria, "IPSM_ORDER");

            gvPreOperative.DataSource = DS;
            gvPreOperative.DataBind();

        }

        void BindOPDData(string FieldID, out string strValue, out string strValueYes, out string strValueNurseYes)
        {
            strValue = "";
            strValueYes = "";
            strValueNurseYes = "";
            DataSet DS = new DataSet();

            string Criteria = " 1=1 and IPC_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPC_ID='" + EMR_ID + "'";
            Criteria += " and IPC_FIELD_ID='" + FieldID + "'";

            objCom = new CommonBAL();
            DS = objCom.fnGetFieldValue(" * ", "IP_PREOPERATIVE_CHECKLIST", Criteria, "IPC_ID");
            foreach (DataRow DR in DS.Tables[0].Rows)
            {

                strValueYes = Convert.ToString(DR["IPC_VALUE"]);
                strValue = Convert.ToString(DR["IPC_REMARKS"]);
                strValueNurseYes = Convert.ToString(DR["IPC_VALUE_OTNURSE"]);
            }



        }

        void BIndProc()
        {
            DataSet DS = new DataSet();
            objPreOp = new IP_PreOperativeChecklist();

            string Criteria = " 1=1 and IPP_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IPP_ID='" + EMR_ID + "'";
            DS = objPreOp.PreOperativeProceduresGet(Criteria);
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvProc.DataSource = DS;
                gvProc.DataBind();

            }
            else
            {
                gvProc.DataBind();
            }
        }

        public void BindPreOperativeChecklistData()
        {

            BindPreOperativeSegmentMaster();
            BIndProc();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            try
            {
                if (!IsPostBack)
                {

                  
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected void gvPreOperative_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblLevelType = (Label)e.Row.FindControl("lblLevelType");
                Label lblFieldID = (Label)e.Row.FindControl("lblFieldID");
                RadioButtonList radCheck = (RadioButtonList)e.Row.FindControl("radCheck");
                Label txtDtls = (Label)e.Row.FindControl("txtDtls");
                RadioButtonList radCheckOTNurse = (RadioButtonList)e.Row.FindControl("radCheckOTNurse");

                if (lblLevelType.Text == "1")
                {
                    radCheck.Visible = false;
                    txtDtls.Visible = false;
                    radCheckOTNurse.Visible = false;

                }
                else
                {
                    string strValue, strValueYes, strValueNurseYes;
                    BindOPDData(lblFieldID.Text, out strValue, out strValueYes, out strValueNurseYes);
                    radCheck.SelectedValue = strValueYes;
                    txtDtls.Text = strValue;
                    radCheckOTNurse.SelectedValue = strValueNurseYes;
                    e.Row.Visible = false;
                    if (strValue != "" || strValueYes != "" || strValueNurseYes != "")
                    {
                        e.Row.Visible = true;
                    }

                }

            }
        }
    }
}