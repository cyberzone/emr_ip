﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecoveryChartReport.ascx.cs" Inherits="EMR_IP.WebReports.RecoveryChartReport1" %>
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight:bold;
    }
    .BorderStyle
    {
        border: 1px solid #dcdcdc; height: 20px;
    }
</style>

<span class="lblCaption1">Recovery Chart </span>

 <span class="lblCaption1" > Post Anesthesia Record</span>
    <table cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            
            <td class="lblCaption1 BoldStyle BorderStyle">Received from :
            </td>
            <td class="lblCaption1 BorderStyle" >
                <asp:Label ID="lblReceivedFrom" runat="server" CssClass="lblCaption1"></asp:Label>

            </td>
            <td class="lblCaption1 BoldStyle  BorderStyle"  >Accompanied by:
            </td>
          <td class="lblCaption1 BorderStyle" >

                <asp:Label ID="lblAccompaniedBy" runat="server" CssClass="lblCaption1"></asp:Label>

            </td>

            <td class="lblCaption1 BoldStyle  BorderStyle"  ">Date :</td>
           <td class="lblCaption1 BorderStyle" >
                 <asp:Label ID="lblArrivalDate" runat="server" CssClass="lblCaption1"></asp:Label>&nbsp;
                 <asp:Label ID="lblArrivalTime" runat="server" CssClass="lblCaption1"></asp:Label>
            </td>
        </tr>

    </table>
    
        <span class="lblCaption1" >Procedure Performed</span>
         <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                   <td class="lblCaption1 BoldStyle BorderStyle">Anesthesia Type :
                </td>
             <td class="lblCaption1 BorderStyle" >
                    <asp:Label ID="lblAnesthesiaType" runat="server" CssClass="Label"></asp:Label>


                </td>
                  <td class="lblCaption1 BoldStyle BorderStyle">General Condition :
                </td>
                 <td class="lblCaption1 BorderStyle" >
                    <asp:Label ID="lblPTCondition" runat="server" CssClass="Label"></asp:Label>

                </td>


            </tr>
            <tr>
                   <td class="lblCaption1 BoldStyle BorderStyle">Ventilation/ O2 Therapy:

                </td>
                 <td class="lblCaption1 BorderStyle" >
                    <asp:Label ID="lblVentilation" runat="server" CssClass="Label"></asp:Label>

                </td>
                   <td class="lblCaption1 BoldStyle BorderStyle">Protective Airway Reflexes (Swallow, Gag, Cough) :

                </td>
                 <td class="lblCaption1 BorderStyle" >
                    <asp:Label ID="lblProtective" runat="server" CssClass="Label"></asp:Label>


                </td>

            </tr>
            <tr>
                   <td class="lblCaption1 BoldStyle BorderStyle">Airway Obstruction Signs :

                </td>
                 <td class="lblCaption1 BorderStyle" >
                    <asp:Label ID="lblAirwayObstSigns" runat="server" CssClass="Label"></asp:Label>

                </td>
                   <td class="lblCaption1 BoldStyle BorderStyle">Artificial Airway Device :

                </td>
                 <td class="lblCaption1 BorderStyle" >
                    <asp:Label ID="lblArtiAirwayDevice" runat="server" CssClass="Label"></asp:Label>

                </td>

            </tr>
            <tr>
                   <td class="lblCaption1 BoldStyle BorderStyle">Vascular Cases :

                </td>
                 <td class="lblCaption1 BorderStyle" >

                    <asp:Label ID="lblVascularCases" runat="server" CssClass="Label"></asp:Label>
                </td>
                   <td class="lblCaption1 BoldStyle BorderStyle">Drainages :

                </td>
                 <td class="lblCaption1 BorderStyle" >
                    <asp:Label ID="lblDrainage" runat="server" CssClass="Label"></asp:Label>

                </td>


            </tr>
            <tr>
                  <td class="lblCaption1 BoldStyle BorderStyle">Special Events :

                </td>
                 <td class="lblCaption1 BorderStyle" >
                    <asp:Label ID="lblPTSpecialEvents" runat="server" CssClass="Label"></asp:Label>
                </td>
                  <td class="lblCaption1 BoldStyle BorderStyle">RR Procedures :

                </td>
                 <td class="lblCaption1 BorderStyle" >
                    <asp:Label ID="lblRRProcedures" runat="server" CssClass="Label"></asp:Label>

                </td>
            </tr>
        </table>
    
 



<span class="lblCaption1">Monitoring in Recovery Room </span>


<asp:GridView ID="gvMonitoring" runat="server" AutoGenerateColumns="False"
    EnableModelValidation="True" Width="100%">
    <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
    <RowStyle CssClass="lblCaption1" HorizontalAlign="Center" />
    <Columns>
        <asp:TemplateField HeaderText="Date & Tome">
            <ItemTemplate>


                <asp:Label ID="lblDate" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_TIMEDateDesc") %>'></asp:Label>
                <asp:Label ID="lblTime" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_TIMETimeDesc") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="BP:Systolic">
            <ItemTemplate>

                <asp:Label ID="Label1" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_BP_SYSTOLIC") %>'></asp:Label>
                &nbsp;  mm/Hg
                                          
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="BP:DiaStolic">
            <ItemTemplate>

                <asp:Label ID="Label2" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_BP_DIASTOLIC") %>'></asp:Label>
                &nbsp;  mm/Hg 
                                             
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Pulse">
            <ItemTemplate>

                <asp:Label ID="lblgvPulse" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_PULSE") %>'></asp:Label>
                &nbsp;Beats/Min
                                            
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="SPO2">
            <ItemTemplate>

                <asp:Label ID="lblgvSpo2" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_SPO2") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Respiration">
            <ItemTemplate>

                <asp:Label ID="lblgvRespiration" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_RESPIRATION") %>'></asp:Label>
                &nbsp; /m
                                           
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Temprature">
            <ItemTemplate>


                <asp:Label ID="lblgvTemperatureF" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_TEMP") %>'></asp:Label>
                &nbsp; F
                                            
            </ItemTemplate>
        </asp:TemplateField>




        <asp:TemplateField HeaderText="Pain Score">
            <ItemTemplate>

                <asp:Label ID="lblgvPainScore" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_PAIN_SCORE") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Motor Block">
            <ItemTemplate>

                <asp:Label ID="lblgvMotorBlock" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_MOTOR_BLOCK") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Comment">
            <ItemTemplate>

                <asp:Label ID="lblgvComment" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_COMMENT") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>
    </Columns>


</asp:GridView>

<span class="lblCaption1">Medication </span>


<asp:GridView ID="gvMedication" runat="server" AutoGenerateColumns="False"
    EnableModelValidation="True" Width="100%">
    <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
    <RowStyle CssClass="lblCaption1" HorizontalAlign="Center" />
    <Columns>
        <asp:TemplateField HeaderText="Solution">
            <ItemTemplate>

                <asp:Label ID="lblMedID" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_MED_ID") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblSolu" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_SOLUTION") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Volume">
            <ItemTemplate>

                <asp:Label ID="Label1" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_VOLUME") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Started">
            <ItemTemplate>

                <asp:Label ID="lblgvStarted" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_STARTEDDateDesc") %>'></asp:Label>
                <asp:Label ID="lblgvStartedTime" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_STARTEDTimeDesc") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Finished">
            <ItemTemplate>

                <asp:Label ID="lblgvFinished" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_FINISHEDDateDesc") %>'></asp:Label>
                <asp:Label ID="lblgvFinishedTime" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_FINISHEDTimeDesc") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Drug">
            <ItemTemplate>

                <asp:Label ID="lblgvDrugCode" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_DRUG_CODE") %>'></asp:Label>
                <asp:Label ID="lblgvDrugName" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_DRUG_NAME") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Dose">
            <ItemTemplate>

                <asp:Label ID="lblgvDose" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_DOSE") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Route">
            <ItemTemplate>

                <asp:Label ID="lblgvRoute" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_ROUTE") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>




        <asp:TemplateField HeaderText="Given By">
            <ItemTemplate>

                <asp:Label ID="lblgvGivenBy" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_GIVEN_BY") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Time Given">
            <ItemTemplate>

                <asp:Label ID="lblgvGiven" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_TIME_GIVENDateDesc") %>'></asp:Label>
                <asp:Label ID="lblgvGivenTime" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRM_TIME_GIVENTimeDesc") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>

    </Columns>


</asp:GridView>

<span class="lblCaption1">Recovery Nurse Notes </span>


<asp:GridView ID="gvNurseNotes" runat="server" AutoGenerateColumns="False"
    EnableModelValidation="True" Width="100%">
    <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
    <RowStyle CssClass="lblCaption1" HorizontalAlign="left" />
    <Columns>
        <asp:TemplateField HeaderText=" Date & Time" HeaderStyle-Width="150px">
            <ItemTemplate>


                <asp:Label ID="lblgvNurseNotesDate" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRNN_TIMEDateDesc") %>'></asp:Label>
                <asp:Label ID="lblgvNurseNotesTime" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRNN_TIMETimeDesc") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Remarks">
            <ItemTemplate>

                <asp:Label ID="gvNurseNotesRemarks" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRNN_REMARKS") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>




    </Columns>


</asp:GridView>

<span class="lblCaption1">Criteria for Discharge from Recovery Room (Modified Aldrete Score) </span>

<asp:GridView ID="gvDischargeRoom" runat="server" AutoGenerateColumns="False"
    EnableModelValidation="True" Width="100%">
    <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
    <RowStyle CssClass="lblCaption1" HorizontalAlign="left" />
    <Columns>
        <asp:TemplateField HeaderText=" Issue Type" HeaderStyle-Width="100px">
            <ItemTemplate>

                <asp:Label ID="lblgvIssueType" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRS_ISSUE_TYPE") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Issue Sub Type">
            <ItemTemplate>

                <asp:Label ID="lblgvIssueSubType" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRS_ISSUE_SUB_TYPE") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Assessment Time">
            <ItemTemplate>

                <asp:Label ID="lblgvAssessmentTime" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRS_ISSUE_TIMING") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Score">
            <ItemTemplate>

                <asp:Label ID="lblgvScore" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRS_SCORE") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>


    </Columns>


</asp:GridView>

<span class="lblCaption1">Additional Criteria for Home Discharge </span>


<asp:GridView ID="gvDischargeHome" runat="server" AutoGenerateColumns="False"
    EnableModelValidation="True" Width="100%">
    <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
    <RowStyle CssClass="lblCaption1" HorizontalAlign="left" />
    <Columns>
        <asp:TemplateField HeaderText=" Issue Type" HeaderStyle-Width="100px">
            <ItemTemplate>


                <asp:Label ID="lblgvHmIssueType" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRS_ISSUE_TYPE") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Issue Sub Type">
            <ItemTemplate>

                <asp:Label ID="lblgvHmIssueSubType" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRS_ISSUE_SUB_TYPE") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Assessment Time">
            <ItemTemplate>

                <asp:Label ID="lblgvHmAssessmentTime" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRS_ISSUE_TIMING") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Score">
            <ItemTemplate>

                <asp:Label ID="lblgvHmScore" CssClass="lblCaption1" runat="server" Text='<%# Bind("IRRS_SCORE") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>


    </Columns>


</asp:GridView>
<table class="table spacy" style="width: 100%;">
    <tr>
        <td class="lblCaption1" style="font-weight: bold;">Pain Score
        </td>
    </tr>
    <tr id="trPain1" runat="server" visible="false">
         <td class="lblCaption1 BorderStyle" >
            <label class="lblCaption1">No Pain</label>&nbsp;&nbsp;<img src="../Images/smilie-1.png" style="width: 32px; height: 32px;" alt="Pain Score" class="pain-smilie" /></td>
    </tr>
    <tr id="trPain2" runat="server" visible="false">
        <td>
            <label for="PainValue2" class="lblCaption1">Mild Pain</label>&nbsp;&nbsp;<img src="../Images/smilie-2.png" style="width: 32px; height: 32px;" alt="Pain Score" class="pain-smilie" /></td>
    </tr>
    <tr id="trPain3" runat="server" visible="false">
        <td>
            <label for="PainValue4" class="lblCaption1">Moderate</label>&nbsp;&nbsp;<img src="../Images/smilie-3.png" style="width: 32px; height: 32px;" alt="Pain Score" class="pain-smilie" /></td>
    </tr>
    <tr id="trPain4" runat="server" visible="false">
        <td>
            <label for="PainValue6" class="lblCaption1">Quite a Lot</label>&nbsp;&nbsp;<img src="../Images/smilie-4.png" style="width: 32px; height: 32px;" alt="Pain Score" class="pain-smilie" /></td>
    </tr>
    <tr id="trPain5" runat="server" visible="false">
        <td>
            <label for="PainValue8" class="lblCaption1">Very Bad</label>&nbsp;&nbsp;<img src="../Images/smilie-5.png" style="width: 32px; height: 32px;" alt="Pain Score" class="pain-smilie" /></td>
    </tr>
    <tr id="trPain6" runat="server" visible="false">
        <td>
            <label for="PainValue10" class="lblCaption1">Worst Pain</label>&nbsp;&nbsp;<img src="../Images/smilie-6.png" style="width: 32px; height: 32px;" alt="Pain Score" class="pain-smilie" /></td>
    </tr>

</table>
<br />
<table cellpadding="=0" cellspacing="0" style="width: 100%">
    <tr>
         <td class="lblCaption1 BoldStyle BorderStyle">Motor Block Intensity:
        </td>
         <td class="lblCaption1 BorderStyle" >
            <asp:Label ID="lblMotorBlockIntensity" runat="server" CssClass="lblCaption1"></asp:Label>


        </td>
    </tr>
    <tr>
         <td class="lblCaption1 BoldStyle BorderStyle">Sedation and Agitation Score:
        </td>
         <td class="lblCaption1 BorderStyle" >

            <asp:Label ID="lblSedationAgitationScore" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
    </tr>
    <tr>
         <td class="lblCaption1 BoldStyle BorderStyle">Patient Teaching
        </td>
         <td class="lblCaption1 BorderStyle" >
            <asp:Label ID="lblPatientTeaching" runat="server" CssClass="lblCaption1"></asp:Label>


        </td>
    </tr>
    <tr>
         <td class="lblCaption1 BoldStyle BorderStyle">Discharged With
        </td>
         <td class="lblCaption1 BorderStyle" >
            <asp:Label ID="lblDischargedWith" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
    </tr>

    <tr>
         <td class="lblCaption1 BoldStyle BorderStyle">Discharged to
        </td>
         <td class="lblCaption1 BorderStyle" >
            <asp:Label ID="lblDischargedTo" runat="server" CssClass="lblCaption1"></asp:Label>


        </td>

    </tr>

</table>
<span class="lblCaption1">Comments and Post-op Instructions</span>
<table cellpadding="=0" cellspacing="0" style="width: 100%">

    <tr>
        <td class="lblCaption1" style="border: 1px solid #dcdcdc; height: 25px;">
            <asp:Label ID="lblPostOpInstComment" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
    </tr>
</table>

<table cellpadding="=0" cellspacing="0" style="width: 100%">

    <tr>
         <td class="lblCaption1 BoldStyle BorderStyle">RR Nurse
        </td>
         <td class="lblCaption1 BorderStyle" >
            <asp:Label ID="lblRRNurse" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
         <td class="lblCaption1 BoldStyle BorderStyle">Anesthesiologist
        </td>
         <td class="lblCaption1 BorderStyle" >
            <asp:Label ID="lblAnesthesiologist" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
         <td class="lblCaption1 BoldStyle BorderStyle">Ward Nurse
        </td>
         <td class="lblCaption1 BorderStyle" >
            <asp:Label ID="lblWardNurse" runat="server" CssClass="lblCaption1"></asp:Label>

        </td>
    </tr>
    <tr>
         <td class="lblCaption1 BoldStyle BorderStyle">Ward Call Time
        </td>
         <td class="lblCaption1 BorderStyle" >
            <asp:Label ID="lblWardCallDate" runat="server" CssClass="lblCaption1"></asp:Label>
            <asp:Label ID="lblWardCallTime" runat="server" CssClass="lblCaption1"></asp:Label>


        </td>
         <td class="lblCaption1 BoldStyle BorderStyle">Discharge Date & Time
        </td>
         <td class="lblCaption1 BorderStyle" >
            <asp:Label ID="lblDischargeDate" runat="server" CssClass="lblCaption1"></asp:Label>
            <asp:Label ID="lblDischargeTime" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
         <td class="lblCaption1 BorderStyle" ></td>
         <td class="lblCaption1 BorderStyle" ></td>
    </tr>
</table>
<br />
<br />
