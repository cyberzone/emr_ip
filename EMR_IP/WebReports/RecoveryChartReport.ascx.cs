﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using EMR_IP_BAL;

namespace EMR_IP.WebReports
{
    public partial class RecoveryChartReport1 : System.Web.UI.UserControl
    {
        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }

        DataSet DS = new DataSet();

        IP_RecoveryChart objRec = new IP_RecoveryChart();

        void BindRecoveryMaster()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 AND IRRM_ID='" + EMR_ID + "'";

            DS = new DataSet();
            DS = objRec.RecoveryRoomMasterGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    trPain1.Visible = false;
                    trPain2.Visible = false;
                    trPain3.Visible = false;
                    trPain4.Visible = false;
                    trPain5.Visible = false;
                    trPain6.Visible = false;
                    if (DS.Tables[0].Rows.Count > 0)
                    {
                        string strPainValue = "";
                        strPainValue = Convert.ToString(DR["IRRM_PAIN_SCORE"]);
                        if (strPainValue == "0" || strPainValue == "1")
                            trPain1.Visible = true;
                        if (strPainValue == "2" || strPainValue == "3")
                            trPain2.Visible = true;
                        if (strPainValue == "4" || strPainValue == "5")
                            trPain3.Visible = true;
                        if (strPainValue == "6" || strPainValue == "7")
                            trPain4.Visible = true;
                        if (strPainValue == "8" || strPainValue == "9")
                            trPain5.Visible = true;
                        if (strPainValue == "10")
                            trPain6.Visible = true;


                    }



                    lblReceivedFrom.Text = Convert.ToString(DR["IRRM_RECEIVED_FROM"]);
                    lblAccompaniedBy.Text = Convert.ToString(DR["IRRM_ACCOMPANIED_BY"]);

                    lblArrivalDate.Text = Convert.ToString(DR["IRRM_ARRIVAL_DATEDesc"]);
                    lblArrivalTime.Text = Convert.ToString(DR["IRRM_ARRIVAL_DATETimeDesc"]);



                    lblMotorBlockIntensity.Text = Convert.ToString(DR["IRRM_MOTOR_BLOCK_INTENSITY"]);
                    lblSedationAgitationScore.Text = Convert.ToString(DR["IRRM_SEDATION_AGITATION_SCORE"]);
                    lblPatientTeaching.Text = Convert.ToString(DR["IRRM_PATIENT_TEACHING"]);
                    lblDischargedWith.Text = Convert.ToString(DR["IRRM_DISCHARGED_WITH"]);
                    lblDischargedTo.Text = Convert.ToString(DR["IRRM_DISCHARGED_TO"]);

                    lblPostOpInstComment.Text = Convert.ToString(DR["IRRM_COMMENT"]);

                    lblRRNurse.Text = Convert.ToString(DR["IRRM_RR_NURSE"]);
                    lblAnesthesiologist.Text = Convert.ToString(DR["IRRM_ANESTHESIOLOGIST"]);
                    lblWardNurse.Text = Convert.ToString(DR["IRRM_WARD_NURSE"]);

                    lblWardCallDate.Text = Convert.ToString(DR["IRRM_WARD_CALL_TIME_DATEDesc"]);
                    lblWardCallTime.Text = Convert.ToString(DR["IRRM_WARD_CALL_TIMEDesc"]);

                    lblDischargeDate.Text = Convert.ToString(DR["IRRM_DISCHARGE_DATEDesc"]);
                    lblDischargeTime.Text = Convert.ToString(DR["IRRM_DISCHARGE_DATE_TIMEDesc"]);

                }

            }




        }

        void BindAnesthesiaDtl()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 and IAD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND IAD_ID='" + Convert.ToString(Session["EMR_ID"]) + "'";

            DS = new DataSet();
            DS = objRec.AnesthesiaDtlsGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    lblAnesthesiaType.Text = Convert.ToString(DR["IAD_ANESTHESIA_TYPE"]);
                    lblPTCondition.Text = Convert.ToString(DR["IAD_PT_CONDITION"]);

                    lblVentilation.Text = Convert.ToString(DR["IAD_VENTILATION"]);
                    lblProtective.Text = Convert.ToString(DR["IAD_PROT_AIRWAY_REFLEXES"]);

                    lblAirwayObstSigns.Text = Convert.ToString(DR["IAD_AIRWAY_OBST_SIGNS"]);
                    lblArtiAirwayDevice.Text = Convert.ToString(DR["IAD_ARTI_AIRWAY_DEVICE"]);

                    lblVascularCases.Text = Convert.ToString(DR["IAD_VASCULAR_CASES"]);
                    lblDrainage.Text = Convert.ToString(DR["IAD_DRAINAGE"]);

                    lblPTSpecialEvents.Text = Convert.ToString(DR["IAD_PT_SPECIAL_EVENTS"]);
                    lblRRProcedures.Text = Convert.ToString(DR["IAD_RR_PROCEDURES"]);



                }

            }


        }

        void BindMonitoringGrid()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 AND IRRM_ID='" + EMR_ID + "'";

            DS = new DataSet();
            DS = objRec.RecoveryRoomMonitoringGet(Criteria);

            gvMonitoring.DataBind();
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvMonitoring.DataSource = DS;
                gvMonitoring.DataBind();

            }


        }

        void BindMedicationGrid()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1 AND  IRRM_ID='" + EMR_ID + "'";

            DS = new DataSet();
            DS = objRec.RecoveryRoomMedicationGet(Criteria);

            gvMonitoring.DataBind();
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvMedication.DataSource = DS;
                gvMedication.DataBind();

            }


        }

        void BindRecoveryNurseNotesGrid()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1   AND IRNN_ID='" + EMR_ID + "'";

            DS = new DataSet();
            DS = objRec.RecoveryNurseNotesGet(Criteria);

            gvNurseNotes.DataBind();
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvNurseNotes.DataSource = DS;
                gvNurseNotes.DataBind();

            }


        }

        void BindDischargeRoomGrid()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1   AND IRRS_ID='" + EMR_ID + "'";
            Criteria += "  AND  IRRS_DISCHARGE_TYPE='DISCHARGE_ROOM' ";
            DS = new DataSet();
            DS = objRec.RecoveryRoomPTStatusGet(Criteria);

            gvDischargeRoom.DataBind();
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvDischargeRoom.DataSource = DS;
                gvDischargeRoom.DataBind();

            }


        }

        void BindDischargeHomeGrid()
        {
            objRec = new IP_RecoveryChart();
            string Criteria = " 1=1   AND IRRS_ID='" + EMR_ID + "'";
            Criteria += "  AND  IRRS_DISCHARGE_TYPE='DISCHARGE_HOME' ";

            DS = new DataSet();
            DS = objRec.RecoveryRoomPTStatusGet(Criteria);

            gvDischargeHome.DataBind();
            if (DS.Tables[0].Rows.Count > 0)
            {
                gvDischargeHome.DataSource = DS;
                gvDischargeHome.DataBind();

            }


        }

        public void BindRecoveryChart()
        {

            BindRecoveryMaster();
            BindAnesthesiaDtl();
            BindMonitoringGrid();
            BindMedicationGrid();
            BindRecoveryNurseNotesGrid();
            BindDischargeRoomGrid();
            BindDischargeHomeGrid();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }

            if (!IsPostBack)
            {


            }
        }
    }
}