﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SiteVerificationReport.ascx.cs" Inherits="EMR_IP.WebReports.SiteVerificationReport" %>

<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1">Site Verification/marking and Time-out Documentation
</span>

 <table width="100%" style="padding-left: 10px;">
          <tr>
             <td class="lblCaption1" colspan="2" style="font-weight: bold;">Part A: Patient Identified by: (√ check all the apply), Minimum two-to be completed by the unit nurse. Check minimum two
            </td>
          </tr>
          <tr>
              <td valign="top">
               <table cellpadding="0" cellspacing="0" style="width: 100%">
      
                    <tr>
                        <td>
                             <input type="checkbox" id="chkNameVerByPT" runat="server" value="True" /><label class="lblCaption1" for="chkVerByPT">Name, verbalized by patient/compared with chart</label> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                                    <input type="checkbox" id="chkBirthDateVerByPT" runat="server" value="True" /><label class="lblCaption1" for="chkBirthDateVerByPT">Birth date, verbalized by patient/compared with chart</label> 
                             </td>
                   </tr>
                    <tr>
                        <td>
                                    <input type="checkbox" id="chkIDBand" runat="server" value="True" /><label class="lblCaption1" for="chkIDBand">Identification band, compared with  chart</label
                             </td>
                    </tr>
        </table>

              </td>
              <td valign="top">
                 <table cellpadding="0" cellspacing="0" style="width: 100%">
                    <tr>
                        <td>
                
                                    <input type="checkbox" id="chkFamilyMember" runat="server" value="Family member" /><label class="lblCaption1" for="chkIDBand">Family member</label>

                                    <input type="text" id="txtFamilyMember" runat="server" class="label" style="width: 200px;" /><br />
                     
                             </td>
                    </tr>
                    <tr>
                        <td>
                 
                                    <input type="checkbox" id="chkPatientUnit" runat="server" value="Patient unit" /><label class="lblCaption1" for="chkIDBand">Patient unit </label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                           <input type="text" id="txtPatientUnit" runat="server" class="label" style="width: 200px;" /><br />
                    
                            </td>
                    </tr>
                    <tr>
                        <td>
               
                                    <input type="checkbox" id="chkVerOther" runat="server" value="Other" /><label class="lblCaption1" for="chkVerOther">Other </label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="text" id="txtVerOther" runat="server" class="label" style="width: 200px;" />
                    
                        </td>
                    </tr>
                </table>
              </td>
           </tr>

  <tr>
            <td class="lblCaption1" colspan="2" style="font-weight: bold;">Part B: Procedure (include Site/Side verification per patient): Do not use abbreviations-to be completed by doctor:
            </td>

        </tr>
     <tr>
            <td colspan="2">
                 <asp:Label ID="lblProcedure" runat="server" CssClass="lblCaption1"></asp:Label>
                    
            </td>
        </tr>
</table>

 <table width="100%">
        <tr>
            <td class="lblCaption1" style="font-weight: bold; width: 50%; vertical-align: top;">Part C:    To be completed by surgeon/assistant/radiologist in the unit
                Marking of operative site (√ check all that apply

            </td>
            <td class="lblCaption1" style="font-weight: bold; width: 50%; vertical-align: top;">Part D:    To be completed by OR reception area
                Verification of Site/Side:
            </td>

        </tr>
        <tr>
            <td style="vertical-align: top;">
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td>
                               
                                        <input type="checkbox" id="chkOperSiteNA" runat="server" value="NA because:-"  disabled="disabled"  /><label class="lblCaption1" for="chkVerByPT">NA because</label>
                                         <asp:Label ID="lblOperSiteNARea" runat="server" CssClass="lblCaption1"></asp:Label>
                                         
                            </td>
                        </tr>
                        <tr>
                            <td>
                              
                                        <input type="checkbox" id="chkMarkPTRefuse" runat="server" value="Patients refuse because"  disabled="disabled"  /><label class="lblCaption1" for="chkPTRefuse">Patients refuse because</label>
                                        <input type="text" id="txtPTRefuse" runat="server" class="label" style="width: 200px;"  readonly="true" />
                                  
                            </td>
                        </tr>
                        <tr>
                            <td>
                                
                                        <input type="checkbox" id="chkMarkOther" runat="server" value="Other"  disabled="disabled"  /><label class="lblCaption1" for="chkMarkOther">Other</label>
                                        <input type="text" id="txtMarkOther" runat="server" class="label" style="width: 200px;" readonly="true"  />
                                   
                            </td>
                        </tr>
                        <tr>
                            <td>
                               
                                        <input type="checkbox" id="chkMarkPTFamily" runat="server" value="Patient/family participated in marking"  disabled="disabled"  /><label class="lblCaption1" for="chkMarkPTFamily">Patient/family participated in marking</label>
                                        <input type="text" id="txtMarkPTFamily" runat="server" class="label" style="width: 200px;"  readonly="true" />
                                  
                            </td>
                        </tr>
                        <tr>
                            <td>
                                
                                        <input type="checkbox" id="chkLocationMarking" runat="server" value="Location of marking"  disabled="disabled"  /><label class="lblCaption1" for="chkLocationMarking">Location of marking</label>
                                        <input type="text" id="txtchkLocationMarking" runat="server" class="label" style="width: 200px;" readonly="true" />
                                     
                            </td>
                        </tr>
                        <tr>
                            <td>
                                
                                        <input type="checkbox" id="chkSiteMarkedBy" runat="server" value="Site marked by"  disabled="disabled"  /><label class="lblCaption1" for="chkSiteMarkedBy">Site marked by</label>
                                       <asp:Label ID="lblSiteMarkedBy" runat="server" CssClass="lblCaption1"></asp:Label>
                                        
                            </td>
                        </tr>
                        <tr>
                            <td>
                                
                                   <input type="checkbox" id="chkConfirmedBy" runat="server" value="Site marked by"  disabled="disabled"  /><label class="lblCaption1" for="chkConfirmedBy">Confirmed by</label>
                                    <asp:Label ID="lblConfirmedBy" runat="server" CssClass="lblCaption1"></asp:Label>
                                     
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">
                                    
Date & Time : &nbsp;  <asp:Label ID="lblMarkDate" runat="server" CssClass="lblCaption1"></asp:Label>
                                            
                                     

                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td style="vertical-align: top;">
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td class="lblCaption1"  >
                                
                                  <asp:UpdatePanel runat="server" ID="updatePanel15">
                                    <ContentTemplate>One:
                                        <input type="checkbox" id="chkLeft" runat="server" value="Left"  disabled="disabled"  /><label class="lblCaption1" for="chkLeft">Left</label>
                                         <input type="checkbox" id="chkCenter" runat="server" value="Center"  disabled="disabled"  /><label class="lblCaption1" for="chkCenter">Center</label>
                                         <input type="checkbox" id="chkRight" runat="server" value="Right"   disabled="disabled" /><label class="lblCaption1" for="chkRightf">Right</label>
                                         <input type="checkbox" id="chkUnknown" runat="server" value="Unknown"  disabled="disabled"  /><label class="lblCaption1" for="chkUnknown">Unknown</label>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" >
                                (√check all that apply)
                            </td>
                        </tr>
                          <tr>
                            <td class="lblCaption1" >
                          
                                     <asp:UpdatePanel runat="server" ID="updatePanel17">
                                    <ContentTemplate>
                                             Patience came from
                                 <input type="text" id="txtPTCameFrom" runat="server" class="label" style="width: 200px;" readonly="true" />    unit
                                           </ContentTemplate>
                                </asp:UpdatePanel>
                               
                            </td>
                        </tr>
                         <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel18">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkConsWithConsent" runat="server" value="Consistent with consent" disabled="disabled" /><label class="lblCaption1" for="chkConsWithConsent">Consistent with consent</label> 
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel19">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkConsWithHistPhy" runat="server" value="Consistent with History & Physical"  disabled="disabled"  /><label class="lblCaption1" for="chkConsWithHistPhy">Consistent with History & Physical</label> 
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                 </td>
                       </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel20">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkConsWithXrayTestRes" runat="server" value="Consistent with X-rays, test results, etc."  disabled="disabled"  /><label class="lblCaption1" for="chkConsWithXrayTestRes">Consistent with X-rays, test results, etc.</label> 
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel21">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkConsWithOperSiteMark" runat="server" value="Consistent with operative Site Marking"   disabled="disabled" /><label class="lblCaption1" for="chkConsWithOperSiteMark">Consistent with operative Site Marking</label> 
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                 </td>
                       </tr>
                          <tr>
                            <td class="lblCaption1" >
                          
                                     <asp:UpdatePanel runat="server" ID="updatePanel22">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkDiscrepancyIdent" runat="server" value="Discrepancy Identified"  disabled="disabled"  /><label class="lblCaption1" for="chkDiscrepancyIdent">Discrepancy Identified</label> 
                                         Explain discrepancy 
                                 <input type="text" id="txtDiscrepancyIdent" runat="server" class="label" style="width: 200px;"  readonly="true" />     
                                           </ContentTemplate>
                                </asp:UpdatePanel>
                               
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" >
                          
                                     <asp:UpdatePanel runat="server" ID="updatePanel23">
                                    <ContentTemplate>
                                        <input type="checkbox" id="chkResolution" runat="server" value="Resolution"  disabled="disabled"  /><label class="lblCaption1" for="chkResolution">Resolution</label> 
                                        <input type="text" id="txtResolution" runat="server" class="label" style="width: 200px;"  readonly="true" />     
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                               
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>

                <table width="100%" style="padding-left: 10px;">
                  <tr>
                     <td class="lblCaption1" style="font-weight: bold;width:33%;text-align:left">  BEFORE INDUCTION OF ANESTHESIA <br /> (with at least RN and anesthesiologist)<br /> SIGN IN
                    </td>
                      <td class="lblCaption1" style="font-weight: bold;width:33%;text-align:left">BEFORE SKIN INCISION <br />(Time out with RN, anesthesiologist, surgeon) <br />TIME OUT

                    </td>
                     
                  </tr>
                  <tr>
                      <td valign="top" >
                           <asp:PlaceHolder ID="plhoBeforeInductionAnesthesia" runat="server"></asp:PlaceHolder>

                      </td>
                        <td valign="top" >
                           <asp:PlaceHolder ID="plhoBeforeSkinIncision" runat="server"></asp:PlaceHolder>

                      </td>
                       

                  </tr>
                </table>
                   <table width="100%" style="padding-left: 10px;">
                          <tr>
                               <td class="lblCaption1" style="font-weight: bold;width:33%;text-align:left">
                                   BEFORE PATIENT LEAVES OPERATION ROOM <br /> (with RN, anesthesiologist, surgeon) <br />SIGN OUT
                              </td>
                          </tr>
                          <tr>
                               <td valign="top" >
                                        <asp:PlaceHolder ID="plhoBeforePTLeavesOperRoom" runat="server"></asp:PlaceHolder>
                               </td>
                           </tr>
                    </table>


