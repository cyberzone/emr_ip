﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using EMR_IP_BAL;


namespace EMR_IP.WebReports
{
    public partial class SiteVerificationReport : System.Web.UI.UserControl
    {

        public string EMR_ID { set; get; }
        public string EMR_PT_ID { set; get; }

        public string DR_ID { set; get; }
        public string IAS_ADMISSION_NO { set; get; }


        DataSet DS = new DataSet();

        CommonBAL objCom = new CommonBAL();
        IP_SiteVerificationMarking objSiteVer = new IP_SiteVerificationMarking();

        void TextFileWriting(string strContent)
        {
            try
            {
                string strFileName = Server.MapPath("../EMR_IPLog.txt");

                StreamWriter oWrite;

                if (File.Exists(strFileName) == true)
                {
                    oWrite = File.AppendText(strFileName);
                }
                else
                {
                    oWrite = File.CreateText(strFileName);
                    //oWrite.WriteLine(strContent);
                    oWrite.WriteLine();
                }

                oWrite.WriteLine("SiteVerificationReport." + strContent);
                //  oWrite.WriteLine();
                oWrite.Close();
            }
            catch (Exception ex)
            {

            }

        }


        public void BindSiteVerification()
        {
            DataSet DS = new DataSet();
            objSiteVer = new IP_SiteVerificationMarking();
            string Criteria = " 1=1   AND ISVM_ID='" + EMR_ID + "'";

            DS = objSiteVer.SiteVerificationMarkingGet(Criteria);

            if (DS.Tables[0].Rows.Count > 0)
            {

                foreach (DataRow DR in DS.Tables[0].Rows)
                {

                    chkNameVerByPT.Checked = Convert.ToBoolean(DR["ISVM_NAME_VERBALIZED_PT_CHART"]);
                    chkBirthDateVerByPT.Checked = Convert.ToBoolean(DR["ISVM_BDATE_VERBALIZED_PT_CHART"]);
                    chkIDBand.Checked = Convert.ToBoolean(DR["ISVM_IDBAND_CHART"]);

                    string strFamilyMember = Convert.ToString(DR["ISVM_PT_IDEN_FAMILY_MEMBER"]);
                    string[] arrFamilyMember = strFamilyMember.Split('|');
                    if (arrFamilyMember.Length > 1)
                    {
                        txtFamilyMember.Value = arrFamilyMember[0];
                        chkFamilyMember.Checked = Convert.ToBoolean(arrFamilyMember[1]);

                    }

                    string strPTUnit = Convert.ToString(DR["ISVM_PT_IDEN_UNIT"]);
                    string[] arrPTUnit = strPTUnit.Split('|');
                    if (arrPTUnit.Length > 1)
                    {
                        txtPatientUnit.Value = arrPTUnit[0];
                        chkPatientUnit.Checked = Convert.ToBoolean(arrPTUnit[1]);

                    }

                    string strIdenOther = Convert.ToString(DR["ISVM_PT_IDEN_OTHER"]);
                    string[] arrIdenOther = strIdenOther.Split('|');
                    if (arrIdenOther.Length > 1)
                    {
                        txtVerOther.Value = arrIdenOther[0];
                        chkVerOther.Checked = Convert.ToBoolean(arrIdenOther[1]);

                    }


                    lblProcedure.Text = Convert.ToString(DR["ISVM_PROCEDURE"]);

                    string strOperSiteNA = Convert.ToString(DR["ISVM_MARKING"]);
                    string[] arrOperSiteNA = strOperSiteNA.Split('|');
                    if (arrOperSiteNA.Length > 1)
                    {
                        lblOperSiteNARea.Text  = arrOperSiteNA[0];
                        chkOperSiteNA.Checked = Convert.ToBoolean(arrOperSiteNA[1]);

                    }



                    string strMarkPTRefuse = Convert.ToString(DR["ISVM_MARK_REFUSE"]);
                    string[] arrMarkPTRefuse = strMarkPTRefuse.Split('|');
                    if (arrMarkPTRefuse.Length > 1)
                    {
                        txtPTRefuse.Value = arrMarkPTRefuse[0];
                        chkMarkPTRefuse.Checked = Convert.ToBoolean(arrMarkPTRefuse[1]);

                    }


                    string strchkMarkOther = Convert.ToString(DR["ISVM_MARK_OTHER"]);
                    string[] arrchkMarkOther = strchkMarkOther.Split('|');
                    if (arrchkMarkOther.Length > 1)
                    {
                        txtMarkOther.Value = arrchkMarkOther[0];
                        chkMarkOther.Checked = Convert.ToBoolean(arrchkMarkOther[1]);

                    }





                    chkMarkPTFamily.Checked = Convert.ToBoolean(DR["ISVM_MARK_FAMILY_PARTICI"]);
                    txtMarkPTFamily.Value = Convert.ToString(DR["ISVM_MARK_FAMILY_PARTICI_DESC"]);

                    chkLocationMarking.Checked = Convert.ToBoolean(DR["ISVM_MARK_LOCATION"]);
                    txtchkLocationMarking.Value = Convert.ToString(DR["ISVM_MARK_LOCATION_DESC"]);

                    lblSiteMarkedBy.Text  = Convert.ToString(DR["ISVM_SITE_MARKED_BY"]);
                    lblConfirmedBy.Text  = Convert.ToString(DR["ISVM_SITE_CONFIRM_BY"]);


                    lblMarkDate.Text = Convert.ToString(DR["ISVM_SITE_MARK_DATEDesc"]) + " " + Convert.ToString(DR["ISVM_SITE_MARK_DATE_TIMEDesc"]);

                    string strVerfiSide = Convert.ToString(DR["ISVM_VERIFI_SIDE"]);
                    string[] artVerfiSide = strVerfiSide.Split('|');
                    if (arrMarkPTRefuse.Length > 1)
                    {
                        for (int i = 0; i <= artVerfiSide.Length - 1; i++)
                        {

                            if (chkLeft.Value == artVerfiSide[i])
                            {
                                chkLeft.Checked = true;
                            }

                            if (chkCenter.Value == artVerfiSide[i])
                            {
                                chkCenter.Checked = true;
                            }
                            if (chkRight.Value == artVerfiSide[i])
                            {
                                chkRight.Checked = true;
                            }

                            if (chkUnknown.Value == artVerfiSide[i])
                            {
                                chkUnknown.Checked = true;
                            }

                        }

                    }




                    txtPTCameFrom.Value = Convert.ToString(DR["ISVM_PT_CAME_FROM"]);

                    chkConsWithConsent.Checked = Convert.ToBoolean(DR["ISVM_CONSISTENT_CONSENT"]);
                    chkConsWithHistPhy.Checked = Convert.ToBoolean(DR["ISVM_CONSISTENT_HIS_PHY"]);
                    chkConsWithXrayTestRes.Checked = Convert.ToBoolean(DR["ISVM_CONSISTENT_XRAY_RESULT"]);

                    chkConsWithOperSiteMark.Checked = Convert.ToBoolean(DR["ISVM_CONSISTENT_SITE_MARK"]);

                    chkDiscrepancyIdent.Checked = Convert.ToBoolean(DR["ISVM_DISCRIDENTIFIED"]);
                    txtDiscrepancyIdent.Value = Convert.ToString(DR["ISVM_DISCRIDENTIFIED_EXP"]);
                    chkConsWithOperSiteMark.Checked = Convert.ToBoolean(DR["ISVM_RESOLUTION"]);

                    chkResolution.Checked = Convert.ToBoolean(DR["ISVM_RESOLUTION"]);


                    txtResolution.Value = Convert.ToString(DR["ISVM_RESOLUTION_DESC"]);

                }
            }
        }

        DataSet GetSegmentData(string strType, string Position)
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 AND ISVS_ACTIVE=1 AND ISVS_BRANCH='" + Convert.ToString(Session["Branch_ID"]) + "' AND ISVS_TYPE='" + strType + "'";
            Criteria += " AND  ISVS_POSITION='" + Position + "'";

            DataSet DS = new DataSet();

            DS = objCom.fnGetFieldValue(" * ", "IP_SITE_VERIFICATION_SEGMENT", Criteria, "ISVS_ORDER");



            return DS;

        }

        DataSet GetSegmentMaster(string strType)
        {
            objCom = new CommonBAL();
            string Criteria = " 1=1 AND ISVSM_STATUS=1 AND ISVSM_TYPE='" + strType + "'";


            DataSet DS = new DataSet();
            DS = objCom.fnGetFieldValue(" * ", "IP_SITE_VERIFICATION_SEGMENT_MASTER", Criteria, "");


            return DS;
        }

        public void CreateSegCtrls(string strType, string Position)
        {
            DataSet DS = new DataSet();

            DS = GetSegmentData(strType, Position);

            foreach (DataRow DR in DS.Tables[0].Rows)
            {
                Boolean boolFieldSet = false;

                Panel myFieldSet = new Panel();

                myFieldSet.GroupingText = Convert.ToString(DR["ISVS_NAME"]);
                myFieldSet.Style.Add("text-align", "justify");
                myFieldSet.CssClass = "lblCaption1";
                myFieldSet.Width = 500;
                Int32 i = 1;

                DataSet DS1 = new DataSet();

                DS1 = GetSegmentMaster(Convert.ToString(DR["ISVS_CODE"]));

                foreach (DataRow DR1 in DS1.Tables[0].Rows)
                {



                    Boolean _checked = false;
                    string strComment = "";
                    objCom = new CommonBAL();
                    string Criteria2 = "1=1 ";
                    Criteria2 += " AND ISVSD_TYPE='" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "' AND ISVSD_FIELD_ID='" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim() + "' ";
                    Criteria2 += " AND ISVSD_BRANCH_ID='" + Convert.ToString(Session["Branch_ID"]) + "' AND ISVSD_ID='" + EMR_ID + "'";

                    DataSet DS2 = new DataSet();
                    objSiteVer = new IP_SiteVerificationMarking();

                    DS2 = objSiteVer.SiteVerificationDtlsGet(Criteria2);
                    if (DS2.Tables[0].Rows.Count > 0)
                    {
                        _checked = true;

                        strComment = Convert.ToString(DS2.Tables[0].Rows[0]["ISVSD_COMMENT"]).Trim();
                    }


                    if (Convert.ToString(DR1["ISVSM_CONTROL_TYPE"]).ToUpper() == "CheckBox".ToUpper())
                    {
                        CheckBox chk = new CheckBox();
                        chk.ID = "chk" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim();
                        chk.Text = Convert.ToString(DR1["ISVSM_FIELD_NAME"]).Trim();
                        chk.CssClass = "lblCaption1";
                        chk.Enabled = false;
                        chk.Checked = _checked;

                        //tdHe1.Controls.Add(chk);
                        //trHe1.Controls.Add(tdHe1);
                        //myFieldSet.Controls.Add(trHe1);
                        if (_checked == true)
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(chk);
                        }
                       
                    }


                    if (Convert.ToString(DR1["ISVSM_CONTROL_TYPE"]).ToUpper() == "TextBox".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["ISVSM_FIELD_NAME"]).Trim() + ":";
                        myFieldSet.Controls.Add(lbl);

                        Literal lit1 = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                        myFieldSet.Controls.Add(lit1);

                        Label txt = new Label();
                        txt.ID = "txt" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["ISVSM_FIELD_NAME"]);
                        txt.CssClass = "Label";
                        txt.Style.Add("width", "99%");

                        txt.Text = strComment;

                       

                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(lbl);
                            myFieldSet.Controls.Add(lit1);
                            myFieldSet.Controls.Add(txt);
                        }

                    }

                    if (Convert.ToString(DR1["ISVSM_CONTROL_TYPE"]).ToUpper() == "DateTime".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["ISVSM_FIELD_NAME"]).Trim() + ":";
                        myFieldSet.Controls.Add(lbl);

                        Label txt = new Label();
                        txt.ID = "txt" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim();
                        // txt.Text = Convert.ToString(DR1["ISVSM_FIELD_NAME"]);
                        txt.CssClass = "Label";
                        txt.Style.Add("width", "75px");



                        txt.Text = strComment;
                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(lbl);
                            myFieldSet.Controls.Add(txt);
                        }
                       
                        

                    }




                    if (Convert.ToString(DR1["ISVSM_CONTROL_TYPE"]).ToUpper() == "Label".ToUpper())
                    {
                        Label lbl = new Label();
                        lbl.CssClass = "lblCaption1";
                        lbl.Text = Convert.ToString(DR1["ISVSM_FIELD_NAME"]).Trim();
                        myFieldSet.Controls.Add(lbl);

                    
                    }

                    if (Convert.ToString(DR1["ISVSM_CONTROL_TYPE"]) == "RadioButtonList")
                    {


                        RadioButtonList RadList = new RadioButtonList();
                        RadList.ID = "radl" + Convert.ToString(DR1["ISVSM_TYPE"]).Trim() + "_" + Convert.ToString(DR1["ISVSM_FIELD_ID"]).Trim();
                        RadList.Enabled = false;
                        RadList.RepeatDirection = RepeatDirection.Horizontal;
                        RadList.Width = 150;
                        string strRadFieldName = Convert.ToString(DR1["ISVSM_FIELD_NAME"]);
                        string[] arrRadFieldName = strRadFieldName.Split('|');


                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            RadList.Items.Add(new ListItem(arrRadFieldName[intRad], arrRadFieldName[intRad]));

                        }

                        RadList.CssClass = "lblCaption1";

                        for (int intRad = 0; intRad <= arrRadFieldName.Length - 1; intRad++)
                        {

                            if (RadList.Items[intRad].Value == strComment)
                            {
                                RadList.SelectedValue = strComment;
                            }

                        }

                        if (strComment.Trim() != "")
                        {
                            boolFieldSet = true;
                            myFieldSet.Controls.Add(RadList);
                        }

                    }





                    Literal lit = new Literal() { Mode = LiteralMode.PassThrough, Text = "<br/>" };
                    myFieldSet.Controls.Add(lit);

                }


                if (strType == "IP_BEFORE_INDUCTION_ANES_SIGNIN")
                {
                    if (Position == "LEFT")
                    {
                        plhoBeforeInductionAnesthesia.Controls.Add(myFieldSet);
                    }

                }

                if (strType == "IP_BEFORE_SKIN_INCISION_TIMEOUT")
                {
                    if (Position == "RIGHT")
                    {
                        if (boolFieldSet == true)
                        {
                            plhoBeforeSkinIncision.Controls.Add(myFieldSet);
                        }
                    }

                }

                if (strType == "IP_BEFORE_PT_LEAVES_OPER_ROOM")
                {
                    if (Position == "LEFT")
                    {
                        if (boolFieldSet == true)
                        {
                            plhoBeforePTLeavesOperRoom.Controls.Add(myFieldSet);
                        }
                    }

                }



                i = i + 1;



            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User_ID"] == null) { Response.Redirect("../Default.aspx?NoSession=1"); }


            try
            {
                if (!IsPostBack)
                {

                    
                }
            }
            catch (Exception ex)
            {
                TextFileWriting("-----------------------------------------------");
                TextFileWriting(System.DateTime.Now.ToString() + "Page_Load");
                TextFileWriting(ex.Message.ToString());
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            generateDynamicControls();
        }


        public void generateDynamicControls()
        {

            CreateSegCtrls("IP_BEFORE_INDUCTION_ANES_SIGNIN", "LEFT");
            CreateSegCtrls("IP_BEFORE_SKIN_INCISION_TIMEOUT", "RIGHT");
            CreateSegCtrls("IP_BEFORE_PT_LEAVES_OPER_ROOM", "LEFT");


        }
    }
}