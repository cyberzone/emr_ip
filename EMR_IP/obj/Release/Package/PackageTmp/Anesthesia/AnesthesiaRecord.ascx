﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AnesthesiaRecord.ascx.cs" Inherits="EMR_IP.Anesthesia.AnesthesiaRecord" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
<link href="../Styles/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript">

</script>
<table style="width: 100%">
    <tr>
        <td class="lblCaption1">Compiled by
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel1">
                <ContentTemplate>
                    <asp:DropDownList ID="drpCompiledby" runat="server" CssClass="label" Width="150px"></asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>

        <td class="lblCaption1">Approved by:
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel2">
                <ContentTemplate>
                    <asp:DropDownList ID="drpApprovedBy" runat="server" CssClass="label" Width="150px"></asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1">Date of issue
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel3">
                <ContentTemplate>
                    <asp:TextBox ID="txtDateOfIssue" runat="server" Width="70px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtInvDate_TextChanged"></asp:TextBox>
                    <asp:CalendarExtender ID="Calendarextender1" runat="server"
                        Enabled="True" TargetControlID="txtDateOfIssue" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
        <td class="lblCaption1">Review date
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel4">
                <ContentTemplate>
                    <asp:TextBox ID="txtReviewDate" runat="server" Width="70px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtInvDate_TextChanged"></asp:TextBox>
                    <asp:CalendarExtender ID="Calendarextender3" runat="server"
                        Enabled="True" TargetControlID="txtReviewDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>

    </tr>
    <tr>
        <td class="lblCaption1">Name of the Anesthetic procedure 
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanelDrug">
                <ContentTemplate>
                    <asp:TextBox ID="txtAnesProceCode" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Enabled="false"></asp:TextBox>
                    <asp:TextBox ID="txtAnesProceName" runat="server" Width="400px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()"></asp:TextBox>

                    <div id="divwidth" style="visibility: hidden;"></div>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtAnesProceCode" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                    </asp:AutoCompleteExtender>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtAnesProceName" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                    </asp:AutoCompleteExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
        <td class="lblCaption1">Name of the surgeon
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel5">
                <ContentTemplate>
                    <asp:DropDownList ID="drpSurgeon" runat="server" CssClass="label" Width="150px"></asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
     <tr>
        <td class="lblCaption1">Surgical procedure 
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel6">
                <ContentTemplate>
                    <asp:TextBox ID="txtSurgProceCode" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Enabled="false"></asp:TextBox>
                    <asp:TextBox ID="txtSurgProceName" runat="server" Width="400px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()"></asp:TextBox>

                    <div id="div1" style="visibility: hidden;"></div>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtSurgProceCode" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                    </asp:AutoCompleteExtender>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtSurgProceName" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                    </asp:AutoCompleteExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
        <td class="lblCaption1">Name of the assistant
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel7">
                <ContentTemplate>
                    <asp:DropDownList ID="drpAssistant" runat="server" CssClass="label" Width="150px"> </asp:DropDownList>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>

     <tr>
        <td class="lblCaption1">Time in   
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel8">
                <ContentTemplate>
                    <asp:TextBox ID="txtTimeInDate" runat="server" Width="70px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtInvDate_TextChanged"></asp:TextBox>
                    <asp:CalendarExtender ID="Calendarextender2" runat="server"
                        Enabled="True" TargetControlID="txtTimeInDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
        <td class="lblCaption1">Anesthesia start
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel9">
                <ContentTemplate>
                    <asp:TextBox ID="txtAnesStart" runat="server" Width="70px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtInvDate_TextChanged"></asp:TextBox>
                    <asp:CalendarExtender ID="Calendarextender4" runat="server"
                        Enabled="True" TargetControlID="txtAnesStart" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>

    </tr>
      <tr>
        <td class="lblCaption1">Surgery start
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel10">
                <ContentTemplate>
                    <asp:TextBox ID="txtSurStart" runat="server" Width="70px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtInvDate_TextChanged"></asp:TextBox>
                    <asp:CalendarExtender ID="Calendarextender5" runat="server"
                        Enabled="True" TargetControlID="txtSurStart" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
        <td class="lblCaption1">Surgery Ends
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel11">
                <ContentTemplate>
                    <asp:TextBox ID="txtSurEnd" runat="server" Width="70px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtInvDate_TextChanged"></asp:TextBox>
                    <asp:CalendarExtender ID="Calendarextender6" runat="server"
                        Enabled="True" TargetControlID="txtSurEnd" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>

    </tr>
     <tr>
        <td class="lblCaption1">Anesthesia Stopes
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel12">
                <ContentTemplate>
                    <asp:TextBox ID="txtAnesStope" runat="server" Width="70px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtInvDate_TextChanged"></asp:TextBox>
                    <asp:CalendarExtender ID="Calendarextender7" runat="server"
                        Enabled="True" TargetControlID="txtAnesStope" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
        <td class="lblCaption1">patient out of the OR
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel13">
                <ContentTemplate>
                    <asp:TextBox ID="txtPTOutOFOR" runat="server" Width="70px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);" AutoPostBack="true" OnTextChanged="txtInvDate_TextChanged"></asp:TextBox>
                    <asp:CalendarExtender ID="Calendarextender8" runat="server"
                        Enabled="True" TargetControlID="txtPTOutOFOR" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>

    </tr>
</table>

<table style="width:100%">
    <tr>
        <td class="lblCaption1">
            Immediate pre anesth chek list <br />
            <asp:CheckBoxList ID="chkImePreAnes" runat="server" CssClass="label" >
                <asp:ListItem  Text="Pt.Identified" Value="Pt.Identified"></asp:ListItem>
                <asp:ListItem  Text="Questioned" Value="Questioned"></asp:ListItem>
                 <asp:ListItem  Text="Chart reviewed" Value="Chart reviewed"></asp:ListItem>
                 <asp:ListItem  Text="Concent signed" Value="Concent signed"></asp:ListItem>
                 <asp:ListItem  Text="NPO since 12 midnight" Value="NPO since 12 midnight"></asp:ListItem>
                 <asp:ListItem  Text="Patient reassessed prior to anesthesia" Value="Patient reassessed prior to anesthesia"></asp:ListItem>
                 <asp:ListItem  Text="surgical site verified- Ready to proceed" Value="surgical site verified- Ready to proceed"></asp:ListItem>
                 <asp:ListItem  Text="Peri-operative pain management discussedwith patient/guardian, plan of care completed" Value="Peri-operative pain management discussedwith patient/guardian, plan of care completed"></asp:ListItem>
            </asp:CheckBoxList>
        </td>
         <td class="lblCaption1">
           MONITORING <br />
            <asp:CheckBoxList ID="CheckBoxList1" runat="server" CssClass="label" >
                <asp:ListItem  Text="Non Invasive B/P" Value="Non Invasive B/P"></asp:ListItem>
                <asp:ListItem  Text="V Lead ECG" Value="V Lead ECG"></asp:ListItem>
                <asp:ListItem  Text="Pulse \oximeter" Value="Pulse \oximeter"></asp:ListItem>
                <asp:ListItem  Text="End tidal Co2" Value="End tidal Co2"></asp:ListItem>
                <asp:ListItem  Text="ET Tube Analyzer" Value="ET Tube Analyzer"></asp:ListItem>
                <asp:ListItem  Text="IV Line" Value="IV Line"></asp:ListItem>
                <asp:ListItem  Text="Temperature" Value="Temperature"></asp:ListItem>
            </asp:CheckBoxList><br />
                O2 / Fio2 Analyser 
              <asp:DropDownList ID="drpO2_Fio2Analyser" runat="server"  CssClass="label" >
                  <asp:ListItem Text="Fluid/Blood warmer" Value="Fluid/Blood warmer"></asp:ListItem>
                  <asp:ListItem Text="Cerebral state monitor" Value ="Cerebral state monitor" ></asp:ListItem>
                  <asp:ListItem Text="Invasive B/P monitor" Value ="Invasive B/P monitor" ></asp:ListItem>
                  <asp:ListItem Text="Body warmer" Value ="Body warmer" ></asp:ListItem>
                  <asp:ListItem Text="CVP Monitor" Value ="CVP Monitor" ></asp:ListItem>
              </asp:DropDownList> 
           


        </td>
    </tr>
    <tr>
        <td class="lblCaption1">
           <fieldset>
                    <legend class="lblCaption1"> Anesthetic procedure</legend>
            <table style="width:100%">
                <tr>
                    <td class="lblCaption1" style="font-weight:bold;">
                          General Anesthetic Induction& Maintanance 
                    </td>
                </tr>
                <tr>
                     <td class="lblCaption1">
                            Induction :     
                          <asp:DropDownList ID="drpInduction" runat="server"  CssClass="label" >
                              <asp:ListItem Text="Intramuscular" Value="Intramuscular"></asp:ListItem>
                              <asp:ListItem Text="Intravenous" Value ="Intravenous" ></asp:ListItem>
                              <asp:ListItem Text="Inhalation" Value ="Inhalation" ></asp:ListItem>
                          </asp:DropDownList> 
                         </td>
                    <td class="lblCaption1">
                        RSI:
                        <asp:DropDownList ID="drpRSI" runat="server"  CssClass="label" >
                              <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                              <asp:ListItem Text="No" Value ="No" ></asp:ListItem>
                          </asp:DropDownList> 
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">
                        Cricoid pressure :
                         <asp:DropDownList ID="drpCricoidPressure" runat="server"  CssClass="label" >
                              <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                              <asp:ListItem Text="No" Value ="No" ></asp:ListItem>
                          </asp:DropDownList> 
                             </td>
                    <td class="lblCaption1">
                        Pre oxygenation
                        <asp:TextBox ID="txtPreOxygenation" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="lblCaption1">
                       GA maintanance :<br />
                          <asp:TextBox ID="txtGAMaintanance" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                         </td>
                </tr>
                     <tr>
                    <td class="lblCaption1">
                       Mixture of Oxygen +Air-----30-40% :<br />
                          <asp:TextBox ID="txtMixtureofOxygen" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                         </td>
                </tr>

            </table>
         
                 <table style="width:100%">
                <tr>
                    <td class="lblCaption1" style="font-weight:bold;">
                         REGIONAL ANESTHESIA
                    </td>
                </tr>
                     <tr>
                    <td class="lblCaption1">
                       
                         <asp:DropDownList ID="drpEpidural" runat="server"  CssClass="label" >
                              <asp:ListItem Text="Epidural" Value="Epidural"></asp:ListItem>
                              <asp:ListItem Text="Spinal" Value ="Spinal" ></asp:ListItem>
                          </asp:DropDownList> 

                        Preparation Betadine + spirit
                         <asp:TextBox ID="txtPreparationBetadine" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                        Sitting position
                         <asp:TextBox ID="txtSittingPosition" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                       </td>

                         </tr>
                     <tr>
                        <td class="lblCaption1">
                        <asp:DropDownList ID="drpRopivacaine" runat="server"  CssClass="label" >
                              <asp:ListItem Text="Ropivacaine" Value="Ropivacaine"></asp:ListItem>
                              <asp:ListItem Text="Bupivacaine" Value ="Bupivacaine" ></asp:ListItem>
                                <asp:ListItem Text="Levobupivacaine" Value ="Levobupivacaine" ></asp:ListItem>
                                <asp:ListItem Text="Levo bupivacaine+Fentanyl 20mcg+Morphine 0.2mg" Value ="Levo bupivacaine+Fentanyl 20mcg+Morphine 0.2mg" ></asp:ListItem>
                                <asp:ListItem Text="Levo bupivacaine + Fntanyl 20 mcg" Value ="Levo bupivacaine + Fntanyl 20 mcg" ></asp:ListItem>

                           </asp:DropDownList> 
                         </td>
                     </tr>
                     <tr>
                        <td class="lblCaption1">
                             Needle  # pencil point 27
                              <asp:TextBox ID="txtNeedlePencilpoint" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                         </td>
                     </tr>
                     </table>
               </fieldset>
        </td>
        <td>

        </td>
    </tr>
</table>