﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="EMR_IP.Anesthesia.Index" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp1" %>

<%@ Register Src="~/Anesthesia/AnesthesiaRecord.ascx" TagPrefix ="UC1" TagName="AnesthesiaRecord" %>




<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp1:TabContainer ID="tabAnesthesiaTab" runat="server" >
        <asp1:TabPanel >
           <ContentTemplate>
             <asp:PlaceHolder ID="PlaceHolderAnesthesiaRecord" runat="server"></asp:PlaceHolder>
                <UC1:AnesthesiaRecord ID="pnlAnesthesiaRecord" runat="server" ></UC1:AnesthesiaRecord>
            </ContentTemplate>
        </asp1:TabPanel>


    </asp1:TabContainer>
</asp:Content>
