﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IPContainer.aspx.cs" Inherits="EMR_IP.Common.IPContainer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="UC1" TagName="TopMenu" Src="~/UserControl/EMRTopMenu.ascx" %>
<%@ Register TagPrefix="UC2" TagName="LeftMenu" Src="~/UserControl/EMRLeftMenu.ascx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <script src="Scripts/Validation.js" type="text/javascript"></script>

    <link href="Styles/style.css" rel="stylesheet" type="text/css" />

    <link href="Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Datagrid.css" rel="stylesheet" type="text/css" />

    <link href="Styles/MenuStyle.css" rel="stylesheet" type="text/css" />
    <title></title>

     
    <style type="text/css">
        #form1
        {
            height: 933px;
        }

        table
        {
            width: 100%;
        }
    </style>

    <style type="text/css" media="print">
        #nottoprint
        {
            display: none;
        }
        .HeaderLableStyle
        {

        }
    </style>

    <script language="javascript" type="text/javascript" >

        function ShowInsuranceCardZoom() {
            var win = window.open("../InsuranceCardZoom.aspx", "newwin1", "top=200,left=300,height=550,width=600,toolbar=no,scrollbars=1,menubar=no");
            win.focus();
        }

    </script>
</head>

<body style="margin-top: 0px; width: 100%; background-color: #ffffff; margin-left: 0; margin-bottom: 0px; overflow-x: hidden;">


    <div style="background-color: white; margin: 0 auto; width: 100%;">
        <form id="form1" runat="server">
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            <div style="height: 80px; background-image: url('<%= ResolveUrl("~/Styles/Images/header_bg.PNG")%>')">
                <div style="width: 80%">
                    <table cellpadding="0" cellspacing="0" width="100%" style="margin-top: 0px; padding-top: 0px;" border="0">
                        <tr>
                            <td style="padding-left:5px;height: 20px;padding-top: 0px; color: #fff; width: 230px; font-weight: bold; font-size: 11px;" class="label">Patient Name &nbsp;&nbsp;&nbsp;:
                   
                         <asp:Label ID="lblPTName" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>
                            </td>
                            <td style=" padding-top: 0px; color: #fff;width: 200px; font-weight: bold; font-size: 11px;" class="label">Admission# &nbsp;&nbsp;:
                    
                       <asp:Label ID="lblAdmissionNo" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>

                            </td>
                            <td style="padding-top: 0px; color: #fff;width: 300px;  font-weight: bold; font-size: 11px;" class="label">Admission Type&nbsp;&nbsp;:
                   
                                 <asp:Label ID="lblAdmissionType" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>
                     

                            </td>
                            <td style="padding-top: 0px; color: #fff;width: 200px;  font-weight: bold; font-size: 11px;" class="label">Admission Mode&nbsp;:
                             <asp:Label ID="lblAddmissionMode" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>

                     

                            </td>
                            <td style="padding-left:5px;padding-top: 0px; color: #fff;width: 235px;  font-weight: bold; font-size: 11px;" class="label">
                              

                                       
                                     
                            </td>
                        </tr>


                          <tr>
                            <td style="padding-left:5px;height: 20px;  padding-top: 0px; color: #fff;  font-weight: bold; font-size: 11px;" class="label">Nationality :
                    
                           <asp:Label ID="lblNationality" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>
                            </td>
                            <td style=" padding-top: 0px; color: #fff;  font-weight: bold; font-size: 11px;" class="label">Patient#&nbsp;&nbsp;&nbsp;&nbsp;:
                   
                       <asp:Label ID="lblPTID" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>

                            </td>
                            <td style="padding-top: 0px; color: #fff; font-weight: bold; font-size: 11px;" class="label">Doctor&nbsp;&nbsp;&nbsp:
                    
                             <asp:Label ID="lblDoctor" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>

                            </td>
                            <td style=" padding-top: 0px; color: #fff;  font-weight: bold; font-size: 11px;" class="label">policy Type:
                   
                             <asp:Label ID="lblPolicyType" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>
                            </td>
                            <td style="padding-left:5px;padding-top: 0px; color: #fff;  font-weight: bold; font-size: 11px;" class="label">
                               

                            </td>
                        </tr>


                        <tr>
                            <td style="padding-left:5px;height: 20px;  padding-top: 0px; color: #fff;  font-weight: bold; font-size: 11px;" class="label">Age :
                    
                       <asp:Label ID="lblAge" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label> 
                       
                            </td>
                            <td style=" padding-top: 0px; color: #fff;  font-weight: bold; font-size: 11px;" class="label">Sex&nbsp;&nbsp;&nbsp;&nbsp;:
                   
                       <asp:Label ID="lblSex" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>

                            </td>
                            <td style="padding-top: 0px; color: #fff; font-weight: bold; font-size: 11px;" class="label">Date & Time&nbsp;&nbsp;&nbsp:
                       <asp:Label ID="lblAdmissionDate" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>
                     

                            </td>
                            <td style=" padding-top: 0px; color: #fff;  font-weight: bold; font-size: 11px;" class="label"> PolicyNo:
                      <asp:Label ID="lblPolicyNo" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>
                       
                            </td>
                            <td style="padding-left:5px;padding-top: 0px; color: #fff;  font-weight: bold; font-size: 11px;" class="label">
                              
                                 

                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:5px;height: 20px;  padding-top: 0px; color: #fff;  font-weight: bold; font-size: 11px;" class="label">Mobile&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                 
                           <asp:Label ID="lblMobile" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>
                            </td>
                            <td style=" padding-top: 0px; color: #fff;  font-weight: bold; font-size: 11px;" class="label">DOB&nbsp;&nbsp;&nbsp;&nbsp;:
                   
                                 <asp:Label ID="lblDOB" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>
                            </td>
                            <td style=" padding-top: 0px; color: #fff;  font-weight: bold; font-size: 11px;" class="label">Provider Name&nbsp;:
                  
                       <asp:Label ID="lblProviderName" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>

                            </td>
                            <td style="padding-top: 0px; color: #fff;  font-weight: bold; font-size: 11px;" class="label">Room&nbsp;:
                   
                       <asp:Label ID="lblRoom" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>,Bead&nbsp;
                                <asp:Label ID="lblBead" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>,ward&nbsp;
                                <asp:Label ID="lblWard" runat="server" ForeColor="#FFFFFF" Font-Bold="true" Style="font-size: 11px;" CssClass="label"></asp:Label>

                            </td>
                            <td style="padding-left:5px;padding-top: 0px; color: #fff;  font-weight: bold; font-size: 11px;" class="label">
                              
                      


                            </td>
                        </tr>
                    </table>
                </div>
                <div class="imageRow" style="position: absolute; right: 20px; z-index: 1000; top: 0px;">
                    <div id="site-info" class="set">
                        <div class="single first">

                            <a href="Javascript:ShowInsuranceCardZoom();">
                                <asp:Image ID="imgFront" runat="server" Height="105px" Width="150px" />

                            </a>

                        </div>
                    </div>
                </div>
            </div>
            <div style="background-color: white; margin: 0 auto; width: 100%; padding-left: 0px;">


                <table cellpadding="0" cellspacing="0" width="100%">


                    <tr>

                        <td colspan="2" style=" padding-left: 0px; padding-right: 0px;">
                            <div id="nottoprint">
                                <UC1:TopMenu ID="TopMenu1" runat="server" />
                            </div>
                        </td>

                    </tr>
                    <tr>
                        <td style="padding-left: 20px; padding-right: 0px; padding-top: 0px; width: 170px; vertical-align: top;">
                            <UC2:LeftMenu ID="LeftMenu" runat="server" />
                        </td>
                        <td style="padding-left: 0px; padding-right: 10px; padding-top: 0px; vertical-align: top;">
                            <div style="width: 99%; float: left; height: auto; background-color: white;">
                                 <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>
                                        <iframe id="IfrmPage" runat="server" style="width: 100%; height: 750px"></iframe>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                        </td>

                    </tr>

                </table>

            </div>


        </form>
    </div>
</body>

