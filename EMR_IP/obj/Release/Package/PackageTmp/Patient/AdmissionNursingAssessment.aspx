﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="AdmissionNursingAssessment.aspx.cs" Inherits="EMR_IP.Patient.AdmissionNursingAssessment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }


        #divDiagExt
        {
            width: 400px !important;
        }

            #divDiagExt div
            {
                width: 400px !important;
            }

        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }


        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }

        #divOrdClin
        {
            width: 400px !important;
        }

            #divOrdClin div
            {
                width: 400px !important;
            }
    </style>


    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

        function ShowErrorMessage(vMessage) {

            document.getElementById("divMessage").style.display = 'block';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = vMessage;
        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = '';
        }

    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function ServNameSelected() {
            if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />

    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully 
                
        </div>
    </div>
    <div style="padding-left: 60%; width: 100%;">
        <div id="divMessage" style="display: none; border: groove; height: 100px; width: 300px; background-color: brown; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute; top: 300px; left: 500px;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right" valign="top">

                        <input type="button" id="btnMsgClose" class="ButtonStyle" style="background-color: White; color: black; border: none;" value=" X " onclick="HideErrorMessage()" />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblMessage" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                    </td>
                </tr>
            </table>


            <br />

        </div>
    </div>
    <table width="100%">
        <tr>
            <td style="text-align: left; width: 50%;">
                <h3 class="box-title">Admission Nursing Assessment</h3>
            </td>
            <td style="text-align: right; width: 50%;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Saved Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>

    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="NursingAssessment 1" Width="100%">
            <ContentTemplate>
                <table style="width: 100%" cellpadding="5" cellspacing="5">
                    <tr>
                        <td class="lblCaption1">Route of Admission
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtAdmissRoute" runat="server" CssClass="label" Width="200px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Ward No
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpWardNo" runat="server" CssClass="label" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="drpWardNo_SelectedIndexChanged"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Room No
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpRoomNo" runat="server" CssClass="label" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="drpRoomNo_SelectedIndexChanged"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Bed No
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpBed" runat="server" CssClass="label" Width="100px"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Arrival time to ward
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel6" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtArrivalDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                        Enabled="True" TargetControlID="txtArrivalDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:DropDownList ID="drpArrivalHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                    <asp:DropDownList ID="drpArrivalMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Admission Mode
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel7">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpAdmissionMode" runat="server" CssClass="label" Width="100px">
                                        <asp:ListItem Text="--- Select ---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Ambulatory" Value="Ambulatory"></asp:ListItem>
                                        <asp:ListItem Text="Wheel Chair" Value="Wheel Chair"></asp:ListItem>
                                        <asp:ListItem Text="Stretcher" Value="Stretcher"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                        <td class="lblCaption1">Info. obtained from
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel9">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpInfObtFrom" runat="server" CssClass="label" Width="100px">
                                        <asp:ListItem Text="--- Select ---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Patient " Value="Patient "></asp:ListItem>
                                        <asp:ListItem Text="Family/Friend " Value="Family/Friend "></asp:ListItem>
                                        <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>


                    </tr>
                    <tr>
                        <td class="lblCaption1">Date of Admission
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel46" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtAdmiDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                        Enabled="True" TargetControlID="txtAdmiDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:DropDownList ID="drpAdmiHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                    <asp:DropDownList ID="drpAdmiMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>

                    <tr>
                        <td class="lblCaption1">Diagnosis
                        </td>
                        <td colspan="7">
                            <asp:UpdatePanel ID="updatePanel8" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtDiag" runat="server" CssClass="label" TextMode="MultiLine" Width="100%" Height="50px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Weight
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel10" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtWeight" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                        <td class="lblCaption1">Height
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel11" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtHeight" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                        <td class="lblCaption1">Patient Pain Score
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel12" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtPTPainScore" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>

                    </tr>
                    <tr>

                        <td colspan="8">
                            <fieldset>
                                <legend class="lblCaption1">Orientation to the Unit</legend>

                                <asp:UpdatePanel ID="updatePanel13" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBoxList ID="chkOrientationUnit" runat="server" CssClass="lblCaption1" Width="100%" RepeatDirection="Horizontal"></asp:CheckBoxList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </fieldset>
                        </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1" colspan="3">Valuables brought to hospital by patient at the time of admission   
                        </td>
                        <td colspan="5">
                            <asp:UpdatePanel ID="updatePanel14" runat="server">
                                <ContentTemplate>
                                    <asp:RadioButtonList ID="radValuables" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="300px">
                                        <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                                        <asp:ListItem Text="No" Value="No"></asp:ListItem>
                                        <asp:ListItem Text="Kept with patient" Value="Kept with patient"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1">Sent home with  
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel15" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtPTSenthWith" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                        <td class="lblCaption1">Given to PRO  by
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel16" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtProGivenBy" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>

                        <td class="lblCaption1">Living situation
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel ID="updatePanel17" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpLivingWith" runat="server" CssClass="lblCaption1" Width="200px">
                                        <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Independent" Value="Independent"></asp:ListItem>
                                        <asp:ListItem Text="With family" Value="With family"></asp:ListItem>
                                        <asp:ListItem Text="Room mates" Value="Room mates"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1">Profession
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel18" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpProfession" runat="server" CssClass="lblCaption1" Width="200px">
                                        <asp:ListItem Text="---Select---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Employed" Value="Employed"></asp:ListItem>
                                        <asp:ListItem Text="Occupation" Value="Occupation"></asp:ListItem>
                                        <asp:ListItem Text="unemployed" Value="unemployed"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                        <td class="lblCaption1">Living in UAE  
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel20" runat="server">
                                <ContentTemplate>

                                    <asp:TextBox ID="textLivingYears" runat="server" CssClass="label" Width="50px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    Yrs
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Married</td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel21" runat="server">
                                <ContentTemplate>
                                    <asp:RadioButtonList ID="radMarried" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                        <asp:ListItem Text="No" Value="False"></asp:ListItem>

                                    </asp:RadioButtonList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Smoker
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel19" runat="server">
                                <ContentTemplate>

                                    <input type="radio" id="radSmokeYes" name="radSmokeYes" runat="server" class="lblCaption1" value="True" /><label for="radSmokeYes">Yes</label>
                                    <input type="radio" id="radSmokeNo" name="radSmokeYes" runat="server" class="lblCaption1" value="False" /><label for="radSmokeNo">No</label>
                                    <asp:TextBox ID="CigsCount" runat="server" Width="50px" CssClass="label" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    Cigs/Day
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                        <td class="lblCaption1">Quit Smoking
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel22" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtSmokeQuitYears" runat="server" Width="30px" CssClass="label" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    Yrs
                                     <asp:TextBox ID="txtSmokeQuitMonth" runat="server" Width="30px" CssClass="label" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    Month
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1">Alcohol
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel23" runat="server">
                                <ContentTemplate>

                                    <input type="radio" id="radAlcoholYes" name="radAlcoholYes" runat="server" class="lblCaption1" value="True" /><label for="radAlcoholYes">Yes</label>
                                    <input type="radio" id="radAlcoholNo" name="radAlcoholYes" runat="server" class="lblCaption1" value="False" /><label for="radAlcoholNo">No</label>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                        <td class="lblCaption1">Quit Drinking
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel24" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtDrinkQuitYears" runat="server" Width="30px" CssClass="label" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    Yrs
                                      <asp:TextBox ID="txtDrinkQuitMonth" runat="server" Width="30px" CssClass="label" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    Month
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>
                </table>


            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Present/past Health problems" Width="100%">
            <ContentTemplate>
                <table width="100%" cellpadding="5" cellspacing="5">
                    <tr>
                        <td>
                            <fieldset>

                                <asp:UpdatePanel ID="updatePanel25" runat="server">
                                    <ContentTemplate>
                                        <asp:CheckBoxList ID="chkPresPastHealthProb" runat="server" CssClass="lblCaption1" RepeatLayout="Flow" Width="900px" RepeatDirection="Horizontal"></asp:CheckBoxList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </fieldset>
                        </td>
                    </tr>
                </table>
                <table width="100%" cellpadding="5" cellspacing="5">
                    <tr>
                        <td class="lblCaption1" style="width: 200px;">If any problem identified (Please specify)
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel26" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtProbIdentified" runat="server" Width="100%" Height="30px" CssClass="label" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Previous Hospitalizations / If any Surgeries write
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel27" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtPrevHostSurgeries" runat="server" Width="100%" Height="30px" CssClass="label" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" valign="top">Alergies
                        </td>
                        <td class="lblCaption1" valign="top">
                            <asp:UpdatePanel ID="updatePanel28" runat="server">
                                <ContentTemplate>
                                    <input type="radio" id="radAlergiesYes" name="radAlergiesYes" runat="server" class="lblCaption1" value="True" /><label for="radAlergiesYes">Yes</label>
                                    <input type="radio" id="radAlergiesNo" name="radAlergiesYes" runat="server" class="lblCaption1" value="False" /><label for="radAlergiesNo">No</label>
                                    &nbsp; &nbsp; (If Yes Specify) 
                                     <asp:TextBox ID="txtAlergies" runat="server" Width="100%" Height="30px" CssClass="label"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" valign="top">Type of Reaction
                        </td>
                        <td valign="top">
                            <asp:UpdatePanel ID="updatePanel29" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtReactionType" runat="server" Width="100%" Height="30px" CssClass="label" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="Medication History" Width="100%">
            <ContentTemplate>

                <fieldset>
                    <table cellpadding="5" cellspacing="5" width="50%">
                        <tr>
                            <td class="lblCaption1">Medication
                            </td>
                            <td colspan="3">
                                <asp:UpdatePanel runat="server" ID="updatePanel30">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtServCode" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Enabled="false"></asp:TextBox>
                                        <asp:TextBox ID="txtServName" runat="server" Width="400px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()"></asp:TextBox>

                                        <div id="divwidth" style="visibility: hidden;"></div>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtServCode" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                        </asp:AutoCompleteExtender>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtServName" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                        </asp:AutoCompleteExtender>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Dosage
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel31">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtDossage1" runat="server" CssClass="label" Width="150px" TextMode="MultiLine" Height="30px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                            <td class="lblCaption1">Route
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel32">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpRoute" CssClass="label" runat="server" Width="155px" BorderColor="#cccccc">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Freqyency
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel33">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtFreqyency" runat="server" Width="48px" Height="19px" Text="" CssClass="label" MaxLength="4" BorderWidth="1px" BorderColor="#cccccc" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:DropDownList ID="drpFreqType" CssClass="label" runat="server" Width="100px" BorderColor="#cccccc">
                                            <asp:ListItem Text="Per Day" Value="04" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Per Hour" Value="05"></asp:ListItem>
                                            <asp:ListItem Text="Per Week" Value="06"></asp:ListItem>
                                            <asp:ListItem Text="" Value="8"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Last dose
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel36" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtLastDoseDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                            Enabled="True" TargetControlID="txtLastDoseDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:DropDownList ID="drpLastDoseHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                        <asp:DropDownList ID="drpLastDosemin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:UpdatePanel runat="server" ID="updatePanel34">
                                    <ContentTemplate>
                                        <asp:Button ID="btnAddPhar" runat="server" CssClass="orange" Width="50px" Text="Add" OnClick="btnAddPhar_Click" Style="width: 50px; border-radius: 5px 5px 5px 5px" />

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td></td>
                        </tr>
                    </table>

                    <asp:UpdatePanel runat="server" ID="updatePanel35">
                        <ContentTemplate>
                            <asp:GridView ID="gvPharmacy" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                EnableModelValidation="True" GridLines="None">
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />

                                <Columns>
                                    <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="18" Width="18"
                                                OnClick="DeleteeDiag_Click" />&nbsp;&nbsp;
                                                
                                        </ItemTemplate>

                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="10%">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPhyCode" runat="server" OnClick="PhySelect_Click">
                                                <asp:Label ID="lblPhyCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_PHY_CODE") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" HeaderStyle-Width="50%" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPhyDesc" runat="server" OnClick="PhySelect_Click">
                                                <asp:Label ID="lblPhyDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_PHY_NAME") %>'></asp:Label>

                                            </asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Dosage" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDosage" runat="server" OnClick="PhySelect_Click">
                                                <asp:Label ID="lblPhyDosage" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_DOSAGE") %>'></asp:Label>


                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Route" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPhyRoute" runat="server" OnClick="PhySelect_Click">
                                                <asp:Label ID="lblPhyRoute" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_ROUTE") %>'></asp:Label>


                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Frequency" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkFrequency" runat="server" OnClick="PhySelect_Click">
                                                <asp:Label ID="lblPhyFrequency" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_FREQUENCY") %>' Visible="false"></asp:Label>&nbsp;
                                                            <asp:Label ID="lblPhyFrequencyTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_FREQUENCYTYPEDesc") %>'></asp:Label>
                                                <asp:Label ID="lblPhyFrequencyType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_FREQUENCYTYPE") %>' Visible="false"></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Last dose" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkLastDose" runat="server" OnClick="PhySelect_Click">
                                                <asp:Label ID="lblLastDoseDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_LAST_DOSAGE_DateDesc") %>'></asp:Label>&nbsp;
                                            <asp:Label ID="lblLastDoseTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_LAST_DOSAGE_TimeDesc") %>'></asp:Label>


                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </fieldset>
                <br />


                <fieldset>
                    <legend class="lblCaption1">Disposition of Medicine </legend>
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:CheckBox ID="chkMedicineBrought" runat="server" CssClass="label" Text="No medicine brought to the hospital" />
                            </td>
                            <td>
                                <asp:CheckBox ID="chkVarifiedPhysician" runat="server" CssClass="label" Text="To be varified by Physician for use in the nursing unit" />
                            </td>
                        </tr>
                        <tr>
                            <td>Sent home with
                            </td>
                            <td>
                                <asp:TextBox ID="txtSentHomeWith" runat="server" Width="200px" CssClass="label"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </fieldset>

            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel3" HeaderText="Pain Assessment" Width="100%">
            <ContentTemplate>
                <table cellpadding="5" cellspacing="5" width="50%">
                    <tr>
                        <td class="lblCaption1">Pain Score
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel37" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtPainScore" runat="server" Width="50px" CssClass="label" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel38" runat="server">
                                <ContentTemplate>
                                    <asp:RadioButtonList ID="PainScoreType" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Numbers" Value="Numbers"></asp:ListItem>
                                        <asp:ListItem Text="Faces" Value="Faces"></asp:ListItem>
                                        <asp:ListItem Text="Flacc" Value="Flacc"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Location
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel39" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtPainLocation" runat="server" Width="200px" CssClass="label"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                        <td class="lblCaption1">Onset
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel40" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtPainOnset" runat="server" Width="200px" CssClass="label"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>


                    </tr>
                    <tr>
                        <td class="lblCaption1">Variations
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel41" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtPainVariations" runat="server" Width="200px" CssClass="label"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Medications
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel42" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtMedications" runat="server" Width="200px" CssClass="label"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>


                    </tr>
                    <tr>
                        <td class="lblCaption1">Aggravates
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel43" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpPainAggravates" runat="server" CssClass="label" Width="200px">
                                        <asp:ListItem Text="--- Select ---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Light" Value="Light"></asp:ListItem>
                                        <asp:ListItem Text="Dark" Value="Dark"></asp:ListItem>
                                        <asp:ListItem Text="Movement" Value="Movement"></asp:ListItem>
                                        <asp:ListItem Text="Lying down" Value="Lying down"></asp:ListItem>
                                        <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                        <td class="lblCaption1">Relieves
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel44" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpPainRelieves" runat="server" CssClass="label" Width="200px">
                                        <asp:ListItem Text="--- Select ---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Eating" Value="Eating"></asp:ListItem>
                                        <asp:ListItem Text="Quiet" Value="Quiet"></asp:ListItem>
                                        <asp:ListItem Text="Cold" Value="Cold"></asp:ListItem>
                                        <asp:ListItem Text="Heat" Value="Heat"></asp:ListItem>
                                        <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Effects of pain
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel ID="updatePanel45" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpPainEffects" runat="server" CssClass="label" Width="200px">
                                        <asp:ListItem Text="--- Select ---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Nausea/Vomiting" Value="Nausea/Vomiting"></asp:ListItem>
                                        <asp:ListItem Text="Sleep" Value="Sleep"></asp:ListItem>
                                        <asp:ListItem Text="Appetite" Value="Appetite"></asp:ListItem>
                                        <asp:ListItem Text="Activity" Value="Activity"></asp:ListItem>
                                        <asp:ListItem Text="Emotions" Value="Emotions"></asp:ListItem>
                                        <asp:ListItem Text="Relationship" Value="Relationship"></asp:ListItem>
                                        <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>


                </table>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel4" HeaderText="Physical Assessment" Width="100%">
            <ContentTemplate>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 50%; vertical-align: top;">
                            <asp:PlaceHolder ID="plhoPhyAssessmentLeft" runat="server"></asp:PlaceHolder>
                        </td>
                        <td style="width: 50%; vertical-align: top;">
                            <asp:PlaceHolder ID="plhoPhyAssessmentRight" runat="server"></asp:PlaceHolder>
                        </td>
                    </tr>
                </table>

            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel5" HeaderText="Other Assessment" Width="100%">
            <ContentTemplate>
                <table style="width: 100%">
                    <tr>
                        <td style="width: 50%; vertical-align: top;">
                            <fieldset>
                                <legend class="lblCaption1">Risk/Safety Assessment</legend>
                                <asp:PlaceHolder ID="plhoRiskSafetyAssessmentLeft" runat="server"></asp:PlaceHolder>

                            </fieldset>
                            <fieldset>
                                <legend class="lblCaption1">Educational Assessment</legend>
                                <asp:PlaceHolder ID="plhoEducationalAssessmentLeft" runat="server"></asp:PlaceHolder>

                            </fieldset>
                            <fieldset>
                                <legend class="lblCaption1">Functional Assessment  </legend>
                                <asp:PlaceHolder ID="plhoFunctionalAssessmentLeft" runat="server"></asp:PlaceHolder>

                            </fieldset>

                        </td>
                        <td style="width: 50%; vertical-align: top;">
                            <fieldset>
                                <legend class="lblCaption1">Braden Skin Risk  Assessment Scale Circle one item from each category</legend>
                                <asp:PlaceHolder ID="plhoBraSkinRiskAssessmentRight" runat="server"></asp:PlaceHolder>
                            </fieldset>
                        </td>
                    </tr>
                </table>

            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel6" HeaderText="Patient Discharge" Width="100%">
            <ContentTemplate>

                <table style="width: 100%">
                    <tr>
                        <td style="width: 50%; vertical-align: top;">
                            <asp:PlaceHolder ID="plhoPatientDischAssessmentLeft" runat="server"></asp:PlaceHolder>
                            <asp:PlaceHolder ID="plhoPatientDischSummaryLeft" runat="server"></asp:PlaceHolder>
                        </td>

                        <td style="width: 50%; vertical-align: top;" class="lblCaption1">
                            <asp:PlaceHolder ID="plhoPatientDischAssessmentRight" runat="server"></asp:PlaceHolder>
                            <fieldset style="width:90%">
                                Select LAMA File&nbsp;:&nbsp;
                             <asp:FileUpload ID="fileLogo" runat="server" Width="300px" CssClass="ButtonStyle" /><br />
                                <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="button orange small" OnClick="btnUpload_Click" />
                                <br />
                                File Name&nbsp;&nbsp;:&nbsp;
                            <asp:Label ID="lblLAMAFileName" runat="server" CssClass="lblCaption1"></asp:Label>
                            </fieldset>
                        </td>
                    </tr>

                </table>

            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>


</asp:Content>
