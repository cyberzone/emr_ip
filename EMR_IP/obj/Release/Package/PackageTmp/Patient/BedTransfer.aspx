﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="BedTransfer.aspx.cs" Inherits="EMR_IP.Patient.BedTransfer" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }


        #divDiagExt
        {
            width: 400px !important;
        }

            #divDiagExt div
            {
                width: 400px !important;
            }

        #divComp
        {
            width: 400px !important;
        }

            #divComp div
            {
                width: 400px !important;
            }


        #divDr
        {
            width: 400px !important;
        }

            #divDr div
            {
                width: 400px !important;
            }
    </style>


    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function DRIdSelected() {
            if (document.getElementById('<%=txtDoctorID.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtDoctorID.ClientID%>').value;
                var Data1 = Data.split('~');
                if (Data1.length > 1) {
                    document.getElementById('<%=txtDoctorID.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtDoctorName.ClientID%>').value = Data1[1];
                }
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />

    <div class="wrapper">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Bed Transfer</h3>
            </div>
        </div>


        <br />
        <div style="padding-left: 60%; width: 100%;">
            <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
                Saved Successfully 
                
            </div>
        </div>

        <div style="float: right;">

            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Saved Record" OnClick="btnSave_Click" />


                </ContentTemplate>
            </asp:UpdatePanel>

        </div>
        <table cellpadding="5" cellspacing="5">
            <tr>
                <td class="lblCaption1">Ward No
                </td>
                <td>
                      <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                         <ContentTemplate>
                             <asp:DropDownList ID="drpWardNo" runat="server" CssClass="label" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="drpWardNo_SelectedIndexChanged"></asp:DropDownList>
                        </ContentTemplate>
                     </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Room No
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                         <ContentTemplate>
                    <asp:DropDownList ID="drpRoomNo" runat="server" CssClass="label" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="drpRoomNo_SelectedIndexChanged"></asp:DropDownList>
                         </ContentTemplate>
                     </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Bed No
                </td>
                <td>
                      <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                         <ContentTemplate>
                    <asp:DropDownList ID="drpBed" runat="server" CssClass="label" Width="100px"></asp:DropDownList>
                               </ContentTemplate>
                     </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Admitted for
                </td>
                <td>
                     <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                         <ContentTemplate>
                    <asp:TextBox ID="txtAdmittedfor" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                             </ContentTemplate>
                         </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Fee Charged per day
                </td>
                <td>
                     <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                         <ContentTemplate>
                    <asp:TextBox ID="txtFee" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                             </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
                <td class="lblCaption1">Status
                </td>
                <td>
                     <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                         <ContentTemplate>
                    <asp:DropDownList ID="drpStatus" runat="server" CssClass="label" Width="100px">
                        <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
                        <asp:ListItem Text="InActive" Value="InActive"></asp:ListItem>
                    </asp:DropDownList>
                                 </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td class="lblCaption1">Recommended Doctor
                </td>
                <td colspan="3">
                    <div id="divDr" style="visibility: hidden;"></div>
                      <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                         <ContentTemplate>
                    <asp:TextBox ID="txtDoctorID" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" Width="20%" onblur="return DRIdSelected()"></asp:TextBox>
                    <asp:TextBox ID="txtDoctorName" runat="server" CssClass="label" BorderWidth="1px" BorderColor="#CCCCCC" Width="75%" MaxLength="50" onblur="return DRIdSelected()"></asp:TextBox>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="Server" TargetControlID="txtDoctorID" MinimumPrefixLength="1" ServiceMethod="GetDoctorDtls"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDr">
                    </asp:AutoCompleteExtender>
                    <asp:AutoCompleteExtender ID="AutoCompleteExtender4" runat="Server" TargetControlID="txtDoctorName" MinimumPrefixLength="1" ServiceMethod="GetDoctorDtls"
                        CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divDr">
                    </asp:AutoCompleteExtender>
                                </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
                 
                <td class="lblCaption1">Date & Time
                </td>
                <td>
                     <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                         <ContentTemplate>
                    <asp:TextBox ID="txtDate" runat="server" Width="100px" Height="22px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                      <asp:DropDownList ID="drpTransHour" Style="font-size: 11px;" CssClass="label" Height="25px" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                    <asp:DropDownList ID="drpTransMin" Style="font-size: 11px;" CssClass="label" Height="25px"  runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                    <asp:CalendarExtender ID="Calendarextender3" runat="server"
                        Enabled="True" TargetControlID="txtDate" Format="dd/MM/yyyy">
                    </asp:CalendarExtender>
                            </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
               
            </tr>
            <tr>
                <td class="lblCaption1">Remarks & Reference
                </td>
                <td colspan="5">
                      <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                         <ContentTemplate>
                    <asp:TextBox ID="txtRemarks" runat="server" CssClass="lable" TextMode="MultiLine" Height="50px" Width="95%"></asp:TextBox>
                             </ContentTemplate>
                     </asp:UpdatePanel>
                </td>
            </tr>
        </table>
        <div>
              <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                         <ContentTemplate>
            <asp:GridView ID="gvBedTrans" runat="server" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="100%">
                <HeaderStyle CssClass="GridHeader" Font-Bold="true" />
                <RowStyle CssClass="GridRow" />
                <Columns>
                    <asp:TemplateField HeaderText="WardNo">
                        <ItemTemplate>
                            <asp:Label ID="lblRdId" CssClass="GridRow" runat="server" Text='<%# Bind("RD_ID") %>' Visible="false"></asp:Label>


                            <asp:LinkButton ID="lnkWardNo" runat="server" OnClick="Edit_Click">


                                <asp:Label ID="lblWardNo" CssClass="GridRow" runat="server" Text='<%# Bind("WardNo") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="RoomNo">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkRoomNo" runat="server" OnClick="Edit_Click">
                                <asp:Label ID="lblRoomNo" CssClass="GridRow" runat="server" Text='<%# Bind("RoomNo") %>'></asp:Label>
                            </asp:LinkButton>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="BedNo">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkBedNo" runat="server" OnClick="Edit_Click">
                                <asp:Label ID="lblBedNo" CssClass="GridRow" runat="server" Text='<%# Bind("BedNo") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Fee">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkFee" runat="server" OnClick="Edit_Click">
                                <asp:Label ID="lblFee" CssClass="GridRow" runat="server" Text='<%# Bind("Fee") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="RecomDrID">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkRecomDrID" runat="server" OnClick="Edit_Click">
                                <asp:Label ID="lblRecomDrID" CssClass="GridRow" runat="server" Text='<%# Bind("RecomDrID") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="RecomDrName">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkRecomDrName" runat="server" OnClick="Edit_Click">
                                <asp:Label ID="lblRecomDrName" CssClass="GridRow" runat="server" Text='<%# Bind("RecomDrName") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkStatus" runat="server" OnClick="Edit_Click">
                                <asp:Label ID="lblStatus" CssClass="GridRow" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkDate" runat="server" OnClick="Edit_Click">
                                <asp:Label ID="lblDate" CssClass="GridRow" runat="server" Text='<%# Bind("Date") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Time">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkTime" runat="server" OnClick="Edit_Click">
                                <asp:Label ID="lblTime" CssClass="GridRow" runat="server" Text='<%# Bind("Time") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Admittedfor">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkAdmittedfor" runat="server" OnClick="Edit_Click">
                                <asp:Label ID="lblAdmittedfor" CssClass="GridRow" runat="server" Text='<%# Bind("Admittedfor") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Remarks">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkRemarks" runat="server" OnClick="Edit_Click">
                                <asp:Label ID="lblRemarks" CssClass="GridRow" runat="server" Text='<%# Bind("Remarks") %>'></asp:Label>
                            </asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Delete">
                        <ItemTemplate>
                            <asp:ImageButton ID="DeleteDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Width="16px" Height="16px"
                                OnClick="DeletegvBedTrans_Click" />&nbsp;&nbsp;
                                                
                        </ItemTemplate>
                        <HeaderStyle Width="50px" />
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>


            </asp:GridView>

                        </ContentTemplate>
             </asp:UpdatePanel>
        </div>
</asp:Content>
