﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="CongenitaHeartDiseaseScreening.aspx.cs" Inherits="EMR_IP.Patient.CongenitaHeartDiseaseScreening" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }


    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

        function InitialScreeningResult() {

            var RHand = document.getElementById("<%=txtINI_PULSE_OX_SATU_RHAND.ClientID%>").value;
            var Foot = document.getElementById("<%=txtINI_PULSE_OX_SATU_FOOT.ClientID%>").value;

            if (RHand != "" && Foot != "") {
               // var Diff = (Foot > RHand) ? RHand - Foot : Foot - RHand
                var Diff = Math.abs(RHand - Foot);
                document.getElementById("<%=txtINI_DIFF_RHAND_FOOT.ClientID%>").value = Diff;

                var vResult
                vResult = ResultCalculation(RHand, Foot);
                if (vResult == "Fail") {
                    document.getElementById("<%=radIniFail.ClientID%>").checked = true;
                 }
                 else {
                     document.getElementById("<%=radIniPass.ClientID%>").checked = true;
                   
                 }
            }

        }

        function SecondScreeningResult() {

            var RHand = document.getElementById("<%=txtSEC_PULSE_OX_SATU_RHAND.ClientID%>").value;
             var Foot = document.getElementById("<%=txtSEC_PULSE_OX_SATU_FOOT.ClientID%>").value;

             if (RHand != "" && Foot != "") {
                 // var Diff = (Foot > RHand) ? RHand - Foot : Foot - RHand
                 var Diff = Math.abs(RHand - Foot);
                 document.getElementById("<%=txtSEC_DIFF_RHAND_FOOT.ClientID%>").value = Diff;

                 var vResult
                 vResult = ResultCalculation(RHand, Foot);
                 if (vResult == "Fail") {
                     document.getElementById("<%=radSecFail.ClientID%>").checked = true;
                }
                else {
                    document.getElementById("<%=radSecPass.ClientID%>").checked = true;

                }
            }

        }

        function ThirdScreeningResult() {

            var RHand = document.getElementById("<%=txtTHI_PULSE_OX_SATU_RHAND.ClientID%>").value;
            var Foot = document.getElementById("<%=txtTHI_PULSE_OX_SATU_FOOT.ClientID%>").value;

            if (RHand != "" && Foot != "") {
                // var Diff = (Foot > RHand) ? RHand - Foot : Foot - RHand
                var Diff = Math.abs(RHand - Foot);
                document.getElementById("<%=txtTHI_DIFF_RHAND_FOOT.ClientID%>").value = Diff;

                 var vResult
                 vResult = ResultCalculation(RHand, Foot);
                 if (vResult == "Fail") {
                     document.getElementById("<%=radThiFail.ClientID%>").checked = true;
                 }
                 else {
                     document.getElementById("<%=radThiPass.ClientID%>").checked = true;

                 }
             }

         }

        function ResultCalculation(RHand, Foot) {
            var vResult
            var Diff = Math.abs(RHand - Foot);
          
          //  alert(RHand > 95);
           // alert(Foot > 95);
          //  alert(Diff < 3);

            if (RHand < 90 || Foot < 90) {
                vResult = "Fail";
            }
            else if ((RHand < 95 && Foot < 95)  && Diff > 3 ) {
                vResult = "Fail";
            }
            else if (RHand > 95 && Foot > 95 && Diff < 3) {
                vResult = "Pass";
            }

            return vResult;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />
    <h3 class="box-title">Congenital Heart Disease Screening Form</h3>

    <br />
    <br />
    <br />
    <br />
    <br />
    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully 
                
        </div>
    </div>

    <div style="float: right;">

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Saved Record" OnClick="btnSave_Click" />


            </ContentTemplate>
        </asp:UpdatePanel>

    </div>

    <table width="100%" style="padding-left: 10px;">
        <tr>
            <td class="lblCaption1">Age at Initial Screening
            </td>
            <td class="lblCaption1">

                <asp:UpdatePanel runat="server" ID="updatePanel7">
                    <ContentTemplate>
                        <asp:TextBox ID="txtAge" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        Hours
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1" colspan="2" style="font-weight: bold;">Initial Screening:
            </td>

        </tr>
        <tr>
            <td class="lblCaption1">Time
            </td>
            <td class="lblCaption1">
                <asp:UpdatePanel ID="updatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtIniDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender1" runat="server"
                            Enabled="True" TargetControlID="txtIniDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:DropDownList ID="drpIniHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                        <asp:DropDownList ID="drpIniMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Pulse Ox Saturation of Right Hand
            </td>
            <td class="lblCaption1">
                <asp:UpdatePanel runat="server" ID="updatePanel3">
                    <ContentTemplate>
                        <asp:TextBox ID="txtINI_PULSE_OX_SATU_RHAND" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);" onkeyup="InitialScreeningResult()"></asp:TextBox>
                        %
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Pulse Ox Saturation of Foot 
            </td>
            <td class="lblCaption1">
                <asp:UpdatePanel runat="server" ID="updatePanel4">
                    <ContentTemplate>
                        <asp:TextBox ID="txtINI_PULSE_OX_SATU_FOOT" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);" onkeyup="InitialScreeningResult()"></asp:TextBox>
                        %
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Difference (right hand – foot)
            </td>
            <td class="lblCaption1">
                <asp:UpdatePanel runat="server" ID="updatePanel5">
                    <ContentTemplate>
                        <asp:TextBox ID="txtINI_DIFF_RHAND_FOOT" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);" Enabled="false"></asp:TextBox>
                        %
                        <input type="radio" id="radIniPass" runat="server" name="radInidResult" class="lblCaption1" value="Pass" /><label class="lblCaption1" for="radIniPass">Pass</label>
                        <input type="radio" id="radIniFail" runat="server" name="radInidResult" class="lblCaption1" value="Fail" /><label class="lblCaption1" for="radIniFail">Fail</label>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>

        <tr>
            <td class="lblCaption1" colspan="2" style="font-weight: bold;">Second Screening (1 hour following initial screen if fail initial screen)
            </td>

        </tr>
        <tr>
            <td class="lblCaption1">Time
            </td>
            <td class="lblCaption1">
                <asp:UpdatePanel ID="updatePanel16" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtSecDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender2" runat="server"
                            Enabled="True" TargetControlID="txtSecDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:DropDownList ID="drpSecHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                        <asp:DropDownList ID="drpSecMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Pulse Ox Saturation of Right Hand
            </td>
            <td class="lblCaption1">
                <asp:UpdatePanel runat="server" ID="updatePanel17">
                    <ContentTemplate>
                        <asp:TextBox ID="txtSEC_PULSE_OX_SATU_RHAND" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);"   onkeyup="SecondScreeningResult()"></asp:TextBox>
                        %
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Pulse Ox Saturation of Foot 
            </td>
            <td class="lblCaption1">
                <asp:UpdatePanel runat="server" ID="updatePanel18">
                    <ContentTemplate>
                        <asp:TextBox ID="txtSEC_PULSE_OX_SATU_FOOT" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);"  onkeyup="SecondScreeningResult()"></asp:TextBox>
                        %
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Difference (right hand – foot)
            </td>
            <td class="lblCaption1">
                <asp:UpdatePanel runat="server" ID="updatePanel19">
                    <ContentTemplate>
                        <asp:TextBox ID="txtSEC_DIFF_RHAND_FOOT" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);" Enabled="false"></asp:TextBox>
                        %

                        <input type="radio" id="radSecPass" runat="server" name="radSecdResult" class="lblCaption1" value="Pass" /><label class="lblCaption1" for="radSecPass">Pass</label>
                        <input type="radio" id="radSecFail" runat="server" name="radSecdResult" class="lblCaption1" value="Fail" /><label class="lblCaption1" for="radSecFail">Fail</label>

                       
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>

        <tr>
            <td class="lblCaption1" colspan="2" style="font-weight: bold;">Third Screening (1 hour following second screening if fail second screen)
            </td>

        </tr>
        <tr>
            <td class="lblCaption1">Time
            </td>
            <td class="lblCaption1">
                <asp:UpdatePanel ID="updatePanel20" runat="server">
                    <ContentTemplate>
                        <asp:TextBox ID="txtThiDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                        <asp:CalendarExtender ID="Calendarextender3" runat="server"
                            Enabled="True" TargetControlID="txtThiDate" Format="dd/MM/yyyy">
                        </asp:CalendarExtender>
                        <asp:DropDownList ID="drpThiHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                        <asp:DropDownList ID="drpThiMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Pulse Ox Saturation of Right Hand
            </td>
            <td class="lblCaption1">
                <asp:UpdatePanel runat="server" ID="updatePanel21">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTHI_PULSE_OX_SATU_RHAND" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);" onkeyup="ThirdScreeningResult()"></asp:TextBox>
                        %
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Pulse Ox Saturation of Foot 
            </td>
            <td class="lblCaption1">
                <asp:UpdatePanel runat="server" ID="updatePanel22">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTHI_PULSE_OX_SATU_FOOT" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);" onkeyup="ThirdScreeningResult()"></asp:TextBox>
                        %
                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">Difference (right hand – foot)
            </td>
            <td class="lblCaption1">
                <asp:UpdatePanel runat="server" ID="updatePanel23">
                    <ContentTemplate>
                        <asp:TextBox ID="txtTHI_DIFF_RHAND_FOOT" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);" Enabled="false"></asp:TextBox>
                        %
                        <input type="radio" id="radThiPass" runat="server" name="radThidResult" class="lblCaption1" value="Pass" /><label class="lblCaption1" for="radThiPass">Pass</label>
                        <input type="radio" id="radThiFail" runat="server" name="radThidResult" class="lblCaption1" value="Fail" /><label class="lblCaption1" for="radThiFail">Fail</label>


                    </ContentTemplate>
                </asp:UpdatePanel>

            </td>
        </tr>
        <tr>
            <td class="lblCaption1" colspan="2" style="height: 20px;"></td>
        </tr>
    </table>
    <table width="70%" style="padding-left: 10px;">
        <tr>
            <td class="lblCaption1" colspan="2" style="text-align: justify;">* If pulse ox saturation is <90% in either the hand or foot the infant’s MD or NP must be notified immediately. “Fail must be checked”.<br />
                * If pulse ox saturations are <95% in both the hand and foot or there is a >3% difference between the two on three measures each separated by one hour the MD or NP must be notified. “Fail must be checked.”<br />
                * If pulse ox saturations are >95% in either extremity, with a <3% difference between the two the reading is expected for an infant. “Pass” should be checked”.

            </td>
        </tr>
        
    </table>
    <br />
    <br />
</asp:Content>
