﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="DischargeSummary.aspx.cs" Inherits="EMR_IP.Patient.DischargeSummary" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidthPDiag
        {
            width: 400px !important;
        }

            #divwidthPDiag div
            {
                width: 400px !important;
            }
    </style>
    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

        function ShowErrorMessage(vMessage) {

            document.getElementById("divMessage").style.display = 'block';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = vMessage;
        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = '';
        }
    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <input type="hidden" id="hidPermission" runat="server" value="9" />

    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully 
                
        </div>
    </div>
    <div style="padding-left: 60%; width: 100%;">
        <div id="divMessage" style="display: none; border: groove; height: 50px; width: 300px; background-color: brown; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right" valign="top">

                        <input type="button" id="btnMsgClose" class="ButtonStyle" style="background-color: White; color: black; border: none;" value=" X " onclick="HideErrorMessage()" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMessage" runat="server" CssClass="label"></asp:Label>
                    </td>
                </tr>
            </table>

        </div>
    </div>
   <table width="100%">
        <tr>
            <td style="text-align: left; width: 50%;">
                <h3 class="box-title">Discharge Summary</h3>
            </td>
            <td style="text-align: right; width: 50%;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Saved Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <fieldset>
       
        <table class="table spacy" style="width: 100%">
            <tr>
                <td>
                     <label for="ProvisionalDiagonosis" class="lblCaption1"> Diagnosis</label>
                </td>
                <td>
                    <asp:UpdatePanel ID="updatePanel20" runat="server">
                        <ContentTemplate>
                            <div id="div1" style="visibility: hidden;"></div>
                            <asp:TextBox ID="txtDiagnosis" runat="server" CssClass="label" Width="500"></asp:TextBox>
                            <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtDiagnosis" MinimumPrefixLength="1" ServiceMethod="GetDiagnosis"
                                CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidthPDiag">
                            </asp:AutoCompleteExtender>
                            <asp:dropdownlist id="drpDiagType" runat="server" cssclass="lblCaption1" width="100px" borderwidth="1px" bordercolor="#cccccc" >
                                            <asp:ListItem Value="Principal" selected="True">Principal</asp:ListItem>
                                            <asp:ListItem Value="Secondary">Secondary</asp:ListItem>
                                            <asp:ListItem Value="Admitting">Admitting</asp:ListItem>
                                           
                       </asp:dropdownlist>
                             <asp:Button ID="btnAddDiag" runat="server" CssClass="button orange small" Width="70px" OnClick="btnAddDiag_Click" Text="Add" />
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </td>
            </tr>
            <tr>
                <td>

                </td>
                        <td>
                             <div style="padding-top: 0px; width: 100%;  height:200px;  overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                            <asp:UpdatePanel runat="server" ID="updatePanel21">
                <ContentTemplate>
                                  <asp:GridView ID="gvDiagnosis" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                EnableModelValidation="True"   OnRowDataBound="gvDiagnosis_RowDataBound"    GridLines="None"   >
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />
                                 <AlternatingRowStyle CssClass="GridAlterRow" />

                                <Columns>
                                     <asp:TemplateField HeaderText=""  HeaderStyle-Width="50px" >
                                        <ItemTemplate>
                                            <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="18" Width="18"
                                                OnClick="DeleteeDiag_Click" />&nbsp;&nbsp;
                                        </ItemTemplate>
                                        
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                              <asp:Label ID="lblDiagID" CssClass="label" Font-Size="11px"  runat="server" Text='<%# Bind("IPD_DIAG_ID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblDiagType" CssClass="label" Font-Size="11px"  Visible="false"  runat="server" Text='<%# Bind("IPD_TYPE") %>'></asp:Label>
                                            <asp:Button ID="btnColor" runat="server" Width="10px" Enabled="false" BorderStyle="None" />&nbsp
                                            <asp:Label ID="lblDiagCode" CssClass="label" Font-Size="11px"   Width="100px" runat="server" Text='<%# Bind("IPD_DIAG_CODE") %>'> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" HeaderStyle-Width="800px" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiagDesc" CssClass="label" Font-Size="11px" Width="100%"  runat="server" Text='<%# Bind("IPD_DIAG_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                </Columns>
                            </asp:GridView>
                  </ContentTemplate>
                                </asp:UpdatePanel>
                    
                             </div>
                        </td>
                    </tr>
            <tr>
                <td style="width: 227px">
                    <label for="ReasonforAdmission" class="lblCaption1">Reason for Admission</label></td>
                <td>
                    <asp:UpdatePanel ID="updatePanel13" runat="server">
                        <ContentTemplate>
                            <textarea id="ReasonforAdmission" runat="server" name="ReasonforAdmission" rows="3" cols="112" class="label" style="width: 100%; resize: none;"></textarea>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

            <tr>
                <td style="width: 227px">
                    <label for="ReasonforAdmission" class="lblCaption1">Significant Findings</label></td>
                <td>
                    <asp:UpdatePanel ID="updatePanel19" runat="server">
                        <ContentTemplate>
                            <textarea id="txtSignificantFindings" runat="server" name="txtSignificantFindings" rows="3" cols="112" class="label" style="width: 100%; resize: none;"></textarea>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

            <tr>
                <td style="width: 227px;">
                    <label for="Laboratory" class="lblCaption1">Operations and Other Procedures Performed</label></td>

                <td>
                    <asp:UpdatePanel ID="updatePanel14" runat="server">
                        <ContentTemplate>
                            <textarea id="txtProcedute" runat="server" name="Laboratory" rows="3" cols="112" class="label" style="width: 100%; resize: none;"></textarea>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>

            <tr>
                <td style="width: 227px">
                    <label for="Treatment" class="lblCaption1">In – hospital Treatment Including Medications</label></td>
                <td>
                    <asp:UpdatePanel ID="updatePanel17" runat="server">
                        <ContentTemplate>
                            <textarea id="txtTreatmentMedic" runat="server" name="txtTreatmentMedic" rows="3" cols="112" class="label" style="width: 100%; resize: none;"></textarea>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>

    </fieldset>
    <fieldset>
        <legend class="lblCaption1">Discharge</legend>

        <table style="width: 100%">
            <tr>
                <td class="lblCaption1" style="width: 300px;">Discharge Date
                </td>
                <td>
                    <asp:UpdatePanel ID="updatePanel2" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtDisDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                            <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                Enabled="True" TargetControlID="txtDisDate" Format="dd/MM/yyyy">
                            </asp:CalendarExtender>
                               <asp:DropDownList ID="drpFrmHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                               <asp:DropDownList ID="drpFrmlMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1">Condition at Discharge or Transfer from Hospital
                </td>
                <td>
                    <asp:UpdatePanel ID="updatePanel3" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtCondition" runat="server" CssClass="label" Width="100%" Height="50px" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1">Discharge Instructions
                </td>
                <td>
                    <asp:UpdatePanel ID="updatePanel4" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtInstructions" runat="server" CssClass="label" Width="100%" Height="50px" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>


            </tr>
            <tr>
                <td class="lblCaption1">Physical Activity
                </td>
                <td>
                    <asp:UpdatePanel ID="updatePanel5" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtPhyActivity" runat="server" CssClass="label" Width="100%" Height="50px" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1">Diet
                </td>
                <td>
                    <asp:UpdatePanel ID="updatePanel6" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtDiet" runat="server" CssClass="label" Width="100%" Height="50px" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>

            <tr>
                <td class="lblCaption1">Follow Up Care
                </td>
                <td>
                    <asp:UpdatePanel ID="updatePanel7" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtFollowUpCare" runat="server" CssClass="label" Width="100%" Height="50px" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1">Discharge Medications
                </td>
                <td>
                    <asp:UpdatePanel ID="updatePanel8" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtMedications" runat="server" CssClass="label" Width="100%" Height="50px" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1">Instructions on safe and effective use of medication given
                </td>
                <td>
                    <asp:UpdatePanel ID="updatePanel9" runat="server">
                        <ContentTemplate>
                            <asp:RadioButtonList ID="radInstrMedication" runat="server" CssClass="lblCaption1" Width="100px" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="radInstrMedication_SelectedIndexChanged">
                                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                <asp:ListItem Text="No" Value="False"></asp:ListItem>
                            </asp:RadioButtonList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
             <tr>
                <td class="lblCaption1"> 
                </td>
                <td>
                    <asp:UpdatePanel ID="updatePanel16" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtInstrMedication" runat="server" Visible="false"  CssClass="label" Width="100%" Height="50px" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1">Information on potential instructions between medication and food given
                </td>
                <td>
                    <asp:UpdatePanel ID="updatePanel10" runat="server">
                        <ContentTemplate>
                            <asp:RadioButtonList ID="radInfoMedicFood" runat="server" CssClass="lblCaption1" Width="100px" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="radInfoMedicFood_SelectedIndexChanged">
                                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                                <asp:ListItem Text="No" Value="False"></asp:ListItem>
                            </asp:RadioButtonList>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            
            <tr>
                <td class="lblCaption1"> 
                </td>
                <td>
                    <asp:UpdatePanel ID="updatePanel15" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtInfoMedicFood" runat="server" Visible="false" CssClass="label" Width="100%" Height="50px" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>
            <tr>
                <td class="lblCaption1">Type of Transportation
                </td>
                <td>
                    <asp:UpdatePanel ID="updatePanel11" runat="server">
                        <ContentTemplate>
                            <asp:TextBox ID="txtTransType" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>

            </tr>

        </table>
    </fieldset>
</asp:Content>
