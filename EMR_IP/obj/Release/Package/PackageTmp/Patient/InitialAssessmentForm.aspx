﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="InitialAssessmentForm.aspx.cs" Inherits="EMR_IP.Patient.InitialAssessmentForm" %>

<%@ register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
    <script src="../Scripts/jquery-1.7.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidthProc
        {
            width: 400px !important;
        }

            #divwidthProc div
            {
                width: 400px !important;
            }
    </style>
    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }



        function SaveVal() {
            var label;
            label = document.getElementById('<%=lblStatus.ClientID%>');
            label.style.color = 'red';
            document.getElementById('<%=lblStatus.ClientID%>').innerHTML = " ";

            var strArrivalDate = document.getElementById('<%=txtArrivalDate.ClientID%>').value
            if (/\S+/.test(strArrivalDate) == true) {
                if (isDate(document.getElementById('<%=txtArrivalDate.ClientID%>').value) == false) {
                    document.getElementById('<%=txtArrivalDate.ClientID%>').focus()
                    return false;
                }
            }

            var strVisitDate = document.getElementById('<%=txtVisitDate.ClientID%>').value
            if (/\S+/.test(strVisitDate) == false) {
                document.getElementById('<%=lblStatus.ClientID%>').innerHTML = "Please enter visit Date";
                document.getElementById('<%=txtVisitDate.ClientID%>').focus;
                return false;
            }

            if (isDate(document.getElementById('<%=txtVisitDate.ClientID%>').value) == false) {
                document.getElementById('<%=txtVisitDate.ClientID%>').focus()
                return false;
            }


        }
        function DivShow(pnlID, AncID) {
            if (document.getElementById("ContentPlaceHolder1_TabContainer1_TabPanel3_" + AncID).innerHTML == '+') {
                document.getElementById("ContentPlaceHolder1_TabContainer1_TabPanel3_" + AncID).innerHTML = '-';
                document.getElementById("ContentPlaceHolder1_TabContainer1_TabPanel3_" + pnlID).style.display = 'block';
            }
            else {
                document.getElementById("ContentPlaceHolder1_TabContainer1_TabPanel3_" + AncID).innerHTML = '+';
                document.getElementById("ContentPlaceHolder1_TabContainer1_TabPanel3_" + pnlID).style.display = 'none';
            }


        }

        function DivROSShow() {
            

            if (document.getElementById('<%=lblROSMax.ClientID%>').innerHTML == '+') {
              
                document.getElementById('<%=lblROSMax.ClientID%>').innerHTML = '-';
                document.getElementById('divROSDtls').style.display = 'block';
            }
            else {
               
                document.getElementById('<%=lblROSMax.ClientID%>').innerHTML = '+';
                document.getElementById('divROSDtls').style.display = 'none';
            }


        }

        function DivPEShow() {


            if (document.getElementById('<%=lblPEMax.ClientID%>').innerHTML == '+') {

                 document.getElementById('<%=lblPEMax.ClientID%>').innerHTML = '-';
                document.getElementById('divPEDtls').style.display = 'block';
            }
            else {

                document.getElementById('<%=lblPEMax.ClientID%>').innerHTML = '+';
                document.getElementById('divPEDtls').style.display = 'none';
            }


        }

    </script>

    <script language="javascript" type="text/javascript">


        function TemplatesPopup(CtrlName, obj) {
            document.getElementById('hidCurElement').value = obj.getAttribute("id");
            var win = window.open("TemplatesPopup.aspx?CtrlName=" + CtrlName + "&Mode=Show", "newwin", "top=200,left=100,height=550,width=500,toolbar=no,scrollbars=yes,menubar=no");
            win.focus();

            return true;

        }

        function TemplateSave(CtrlName, obj) {
            document.getElementById('hidCurElement').value = jQuery(obj).parent().find('textarea').attr('id');
            var win = window.open("TemplatesPopup.aspx?CtrlName=" + CtrlName, "newwin", "top=200,left=100,height=550,width=500,toolbar=no,scrollbars=yes,menubar=no");
            win.focus();
            return true;
        }

        function GetTemplateData() {
            return (document.getElementById(document.getElementById("hidCurElement").value).value);
        }

        function BindTemplate(ctrlName, strTemplate) {
            if (ctrlName != '') {
                var ctrlData = document.getElementById(document.getElementById('hidCurElement').value).value;
                document.getElementById(document.getElementById('hidCurElement').value).value = ctrlData + strTemplate;
            }

        }



    </script>

    <script language="javascript" type="text/javascript">

        function ServicessShow(CtrlName, CategoryType) {
            var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
            if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                var win = window.open("ServicessLookup.aspx?CtrlName=" + CtrlName + "&CategoryType=" + CategoryType, "newwin1", "top=200,left=270,height=500,width=750,toolbar=no,scrollbars=yes,menubar=no");
                win.focus();
                return false;
            }

        }


        function BindServicess(Code, Desc, CtrlName) {


            if (CtrlName == 'ProvDiagnosis') {
                var data = document.getElementById("<%=txtProvisionalDiagnosis.ClientID%>").value

                if (data != "") {
                    document.getElementById("<%=txtProvisionalDiagnosis.ClientID%>").value = data + "\n" + Code + ' - ' + Desc
                }
                else {
                    document.getElementById("<%=txtProvisionalDiagnosis.ClientID%>").value = Code + ' - ' + Desc
                }

            }



            if (CtrlName == 'PrincDiagnosis') {
                var data = document.getElementById("<%=txtPrincipalDiagnosis.ClientID%>").value

                 document.getElementById("<%=txtPrincipalDiagnosis.ClientID%>").value = Code + ' - ' + Desc

             }



             if (CtrlName == 'SecDiagnosis') {
                 var data = document.getElementById("<%=txtSecondaryDiagnosis.ClientID%>").value

                if (data != "") {
                    document.getElementById("<%=txtSecondaryDiagnosis.ClientID%>").value = data + "\n" + Code + ' - ' + Desc
                 }
                 else {
                     document.getElementById("<%=txtSecondaryDiagnosis.ClientID%>").value = Code + ' - ' + Desc
                 }

             }


             if (CtrlName == 'Investigations') {
                 var data = document.getElementById("<%=txtInvestigations.ClientID%>").value

                 if (data != "") {
                     document.getElementById("<%=txtInvestigations.ClientID%>").value = data + "\n" + Code + ' - ' + Desc
                }
                else {
                    document.getElementById("<%=txtInvestigations.ClientID%>").value = Code + ' - ' + Desc
                }

            }

            if (CtrlName == 'Procedure') {
                var data = document.getElementById("<%=txtProcedure.ClientID%>").value

                 if (data != "") {
                     document.getElementById("<%=txtProcedure.ClientID%>").value = data + "\n" + Code + ' - ' + Desc
                   }
                   else {
                       document.getElementById("<%=txtProcedure.ClientID%>").value = Code + ' - ' + Desc
                   }

               }

           }

    </script>



     <script language="javascript" type="text/javascript">


         function TemplatesCtrlPopup(CtrlName, obj) {
             var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
             if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                 document.getElementById('hidCurElement').value = obj.getAttribute('id');
                 var win = window.open("TemplatesControls.aspx?PageName=PatientAssessment&CtrlName=" + CtrlName, "newwin", "top=200,left=100,height=550,width=500,toolbar=no,scrollbars=yes,menubar=no");
                 win.focus();
             }
             return true;

         }

         function BindCtrlData(ctrlName, strCtrlData) {

             if (ctrlName != '') {
                 var ctrlData = document.getElementById(document.getElementById('hidCurElement').value).value;

                 document.getElementById(document.getElementById('hidCurElement').value).value = ctrlData + strCtrlData
             }


         }

    </script>
       

      <script language="javascript" type="text/javascript">


          function TemplatesPopup(CtrlName, obj) {
              var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
              if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                  document.getElementById('hidCurElement').value = obj.getAttribute("id");
                  var win = window.open("TemplatesPopup.aspx?CtrlName=" + CtrlName + "&Mode=Show", "newwin", "top=200,left=100,height=550,width=500,toolbar=no,scrollbars=yes,menubar=no");
                  win.focus();
              }
              return true;

          }

          function TemplateSave(CtrlName, obj) {
              var strPermission = document.getElementById("<%=hidPermission.ClientID%>").value;
              if (strPermission == "3" || strPermission == "5" || strPermission == "9") {
                  document.getElementById('hidCurElement').value = jQuery(obj).parent().find('textarea').attr('id');
                  var win = window.open("TemplatesPopup.aspx?CtrlName=" + CtrlName, "newwin", "top=200,left=100,height=550,width=500,toolbar=no,scrollbars=yes,menubar=no");
                  win.focus();
              }
              return true;
          }

          function GetTemplateData() {
              return (document.getElementById(document.getElementById("hidCurElement").value).value);
          }

          function BindTemplate(ctrlName, strTemplate) {
              if (ctrlName != '') {
                  var ctrlData = document.getElementById(document.getElementById('hidCurElement').value).value;
                  document.getElementById(document.getElementById('hidCurElement').value).value = ctrlData + strTemplate;
              }

          }



    </script>

</asp:Content>
<asp:content id="Content2" contentplaceholderid="ContentPlaceHolder1" runat="server">

    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <input type="hidden" id="hidCurElement" name="hidCurElement" value="" />
    <input type="hidden" id="hidPTID" runat="server" />
    <table>
        <tr>
            <td class="PageHeader">Initial Assessment Form
               
            </td>
        </tr>
    </table>
    <table style="padding-left: 10px" cellspacing="0" cellpadding="0" width="100%">
        <tr>

            <td>
                <asp:updatepanel id="UpdatePanel0" runat="server">
                    <contenttemplate>
                <asp:Label ID="lblStatus" runat="server" ForeColor="red" Font-Bold="true" Style="letter-spacing: 1px;" CssClass="label"></asp:Label>

                    </contenttemplate>
                </asp:updatepanel>
            </td>
        </tr>
    </table>
   
    <div style="padding: 5px; width: 100%; overflow: auto; border: thin; border-color: #cccccc; border-style: groove;">

        <table width="95%">
            <tr>
                <td style="float: right; padding-right: 20px;">
                    <asp:button id="btnSave" runat="server" cssclass="button orange small" width="70px" onclick="btnSave_Click" onclientclick="return SaveVal();" text="Save" />
                </td>
            </tr>
        </table>
        <asp:tabcontainer id="TabContainer1" runat="server" activetabindex="0" cssclass="ajax__tab_yuitabview-theme" width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Visit Details" Width="100%">
            <contenttemplate>
                 <table width="100%" cellpadding="5px" cellspacing="5px">
                      <tr  style="display:none;" >
                                <td class="lblCaption1" style="width:50%">Area of Encounter/Assessment: 
                              
                                    <asp:textbox id="txtArea" cssclass="label" runat="server" width="175px" height="20px" borderwidth="1px" bordercolor="#CCCCCC"></asp:textbox>
                                </td>

                                <td class="lblCaption1" style="width:50%">Arrival time to Area:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                               
                               
                                    <asp:textbox id="txtArrivalDate" runat="server" width="70px" height="20px" cssclass="label" borderwidth="1px" bordercolor="#CCCCCC" maxlength="10" onkeypress="return OnlyNumeric(event);"></asp:textbox>
                                    <asp:calendarextender id="TextBox1_CalendarExtender" runat="server"
                                        enabled="True" targetcontrolid="txtArrivalDate" format="dd/MM/yyyy">
                                     </asp:calendarextender>
                                    <asp:maskededitextender id="MaskedEditExtender1" runat="server" enabled="true" targetcontrolid="txtArrivalDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>

                                    <asp:dropdownlist id="drpArrHour" style="font-size:11px;" cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"> </asp:dropdownlist>
                                    <asp:dropdownlist id="drpArrMin" style="font-size:11px;"  cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"></asp:dropdownlist>

                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px" colspan="2"></td>
                            </tr>
                            <tr>
                                <td class="lblCaption1" >Date of Admission:
                                
                                    <asp:textbox id="txtVisitDate" runat="server" width="70px" height="20px" cssclass="label" borderwidth="1px" bordercolor="Red" maxlength="10" onkeypress="return OnlyNumeric(event);"></asp:textbox>
                                    <asp:calendarextender id="CalendarExtender1" runat="server"
                                        enabled="True" targetcontrolid="txtVisitDate" format="dd/MM/yyyy">
                                     </asp:calendarextender>
                                    <asp:maskededitextender id="MaskedEditExtender2" runat="server" enabled="true" targetcontrolid="txtVisitDate" mask="99/99/9999" masktype="Date"></asp:maskededitextender>

                                    <asp:dropdownlist id="drpVisitHour" style="font-size:11px;"  cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"> </asp:dropdownlist>
                                    <asp:dropdownlist id="drpVisiMin" style="font-size:11px;"  cssclass="label" runat="server" borderwidth="1px" bordercolor="#cccccc" width="48px"></asp:dropdownlist>

                                </td>
                                 <td class="lblCaption1" style="height: 30px;" >Assessment done by:
                                       
                                            <asp:textbox id="txtAssessmentDoneBy" cssclass="label" runat="server" width="77%" borderwidth="1px" bordercolor="#CCCCCC"></asp:textbox>
                                            <asp:autocompleteextender id="AutoCompleteExtender4" runat="Server" targetcontrolid="txtAssessmentDoneBy" minimumprefixlength="1" servicemethod="GetStaffName"></asp:autocompleteextender>

                                        </td>

                            </tr>
                            <tr>
                                <td style="height: 10px" colspan="2"></td>
                            </tr>
                             <tr>
                                 <td class="lblCaption" >
                                     <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                        <span class="lblCaption">Provisional  Diagnosis: </span><br />
                                        <asp:textbox id="txtProvisionalDiagnosis" runat="server" width="100%" cssclass="label" borderwidth="1px" height="80px" textmode="MultiLine" bordercolor="#cccccc" ondblclick="return ServicessShow('ProvDiagnosis','Diagnosis');"></asp:textbox>
                                     </div>
                                 </td>
                                 <td class="lblCaption1">

                                        <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                            <span class="lblCaption" style="width:150px;">Condition upon visit:</span>
                                          
                                            <asp:radiobuttonlist id="radConditionUponVisit" runat="server" cssclass="label" width="50%" repeatdirection="Horizontal">
                                                    <asp:ListItem Text="Ambulatory" Value="Ambulatory"></asp:ListItem>
                                                        <asp:ListItem Text="Wheel Chair" Value="WheelChair"></asp:ListItem>
                                                        <asp:ListItem Text="Stretcher" Value="Stretcher"></asp:ListItem>
                                                        <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                               </asp:radiobuttonlist>
                                            <br />
                                            <asp:textbox id="txtConditionOther" cssclass="label" runat="server" width="100%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC"></asp:textbox>

                                        </div>

                                  </td>

                             </tr>
                             <tr>
                                <td style="height: 10px" colspan="2"></td>
                            </tr>
                            <tr>
                               

                                <td class="lblCaption1"  >

                                    <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                        <span class="lblCaption">Information obtained from:</span>
                                        <asp:radiobuttonlist id="radInfoObtainedFrom" runat="server" cssclass="label" width="50%" repeatdirection="Horizontal">
                                                    <asp:ListItem Text="Patient" Value="Patient"></asp:ListItem>
                                                    <asp:ListItem Text="Family/Friend" Value="Family"></asp:ListItem>
                                                    <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                         </asp:radiobuttonlist>
                                        <br />
                                        <asp:textbox id="txtInfoObtainedOther" cssclass="label" runat="server" width="100%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC"></asp:textbox>

                                    </div>

                                </td>

                               <td class="lblCaption1"    >
                                           <div style="padding: 5px; width: 95%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                                <span class="lblCaption">Patient / Explanation given:</span>
                                                <asp:radiobuttonlist id="radExplanationGiven" runat="server" cssclass="label" width="50%" repeatdirection="Horizontal">
                                                        <asp:ListItem Text="Patient" Value="Patient"></asp:ListItem>
                                                            <asp:ListItem Text="Relative" Value="Relative"></asp:ListItem>
                                                            <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
                                                   </asp:radiobuttonlist>
                                                <br />
                                                <asp:textbox id="txtExplanationGivenOther" cssclass="label" runat="server" width="100%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC"></asp:textbox>

                                        </div>
                                             
                                        </td>

                            </tr>
                              
                 </table>
                
            </contenttemplate>

        </asp:TabPanel>
         <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Health History" Width="100%">
            <contenttemplate>
                     <table>
                    <tr>
                         <td valign="top" style="width:49%">
                 <fieldset>
                    <legend class="lblCaption1" style="font-weight:bold;">Present And / Or past Health Problems</legend>
                   
                    <table >
                          <tr>
                            <td class="lblCaption1">
                                 <asp:textbox id="txtHC_ASS_Health_PresPastHist" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Health_PresPastHist',this)"></asp:textbox>
                                                   
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">
                                 <div style="padding:5px; width: 97%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             
                                   <span class="lblCaption" >    If any problem identified (Please specify)</span> 
                                    <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HEALTH_PRESPAS_PROB', this)"  value="Template" />
                               
                                   <br />
                                 <asp:TextBox ID="txtPresPastProblem" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"   BorderColor="#cccccc"  ondblclick="return TemplatesPopup('HC_HEALTH_PRESPAS_PROB',this);"  ></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                         <tr>
                            <td class="lblCaption1">
                                 <div style="padding:5px; width: 97%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             
                                   <span class="lblCaption" >Previous Hospitalizations / If any Surgeries write</span> 
                                    
                                    <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HEALTH_PRESPAS_PREVPROB', this)"  value="Template" />

                               
                                   <br />
                                 <asp:TextBox ID="txtPresPastSurgeries" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"   BorderColor="#cccccc"  ondblclick="return TemplatesPopup('HC_HEALTH_PRESPAS_PREVPROB',this);"  ></asp:TextBox>
                                </div>


                            </td>
                        </tr>
                        <tr>
                            <td style="height:10px;"></td>
                        </tr>
                        <tr>
                            <td class="lblCaption1" >
                                  <div style="padding: 5px; width: 97%; border: thin; border-color: #cccccc; border-style: solid; border-radius: 4px;">
                                        <span class="lblCaption1" style="font-weight:bold;">Alergies:</span> <br />
                                         Medication :
                                         <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HEALTH_ALLERGIES_MEDIC', this)"  value="Template" />
                                        <br />
                                         <asp:textbox id="txtAlergiesMedication" cssclass="label" runat="server" width="100%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesPopup('HC_HEALTH_ALLERGIES_MEDIC',this);" ></asp:textbox><br />
                                        Food  :
                                          
                                         <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HEALTH_ALLERGIES_FOOD', this)"  value="Template" />
                                      <br />
                                           <asp:textbox id="txtAlergiesFood" cssclass="label" runat="server" width="100%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesPopup('HC_HEALTH_ALLERGIES_FOOD',this);" ></asp:textbox><br />
                                       Other  :
                                        
                                         <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HEALTH_ALLERGIES_OTHER', this)"  value="Template" />

                                      <br />
                                           <asp:textbox id="txtAlergiesOther" cssclass="label" runat="server" width="100%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesPopup('HC_HEALTH_ALLERGIES_OTHER',this);" ></asp:textbox><br />

 
                                    </div>

                            </td>
                        </tr>
                    </table>
               
                </fieldset>
                         <td style="width:1%;"></td>
                         <td valign="top" style="width:49%">

                 <fieldset>
                <legend class="lblCaption1" style="font-weight:bold;">Medication History</legend>
             
                     <table>
                    <tr>
                             <td>
                     <span class="lblCaption1"  >  Medicine Name / Dosage / Frequency </span>
                         
                         <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HEALTH_MEDICHIST_MEDICINE', this)"  value="Template" />

                         <br />
                         <asp:textbox id="txtHC_ASS_Health_MedicHist_Medicine" cssclass="label" runat="server" width="99%" height="80px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesPopup('HC_HEALTH_MEDICHIST_MEDICINE',this);" ></asp:textbox>
                                         
                             </td>
                         </tr>


                         <tr>
                             <td>
                     <span class="lblCaption1"  >  Sleep / Rest </span><br />
                         <asp:textbox id="txtHC_ASS_Health_MedicHist_Sleep" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Health_MedicHist_Sleep',this)"></asp:textbox>
                                         
                             </td>
                         </tr>
                   
                         <tr>
                             <td>
                             <span class="lblCaption1"  >  Psychological Assessment </span><br />
                           <asp:textbox id="txtHC_ASS_Health_MedicHist_PschoyAss" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Health_MedicHist_PschoyAss',this)"></asp:textbox>
                             </td>
                         </tr>
                         <tr><td style="height:10px;"></td></tr>
                       <tr>

                           <td class="lblCaption1"  >
                             <span class="lblCaption1" style="font-weight:bold;" >  Pain Assessment </span><br />
                                 Patient expresses presence of pain   
                                  <asp:RadioButtonList ID="radPainAssYes" runat="server" CssClass="label"  Width="100px"  RepeatDirection="Horizontal">
                                        <asp:ListItem Text="Yes" Value="True"  ></asp:ListItem>
                                        <asp:ListItem Text="No" Value="False"  ></asp:ListItem>
                                  </asp:RadioButtonList>
                                 
                           </td>
                       </tr>


                     </table>
                
 
            </fieldset>

                        </td>
                    </tr>
                </table>
            </contenttemplate>

        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel2" HeaderText=" Encounter " Width="100%">
            <contenttemplate>
                 <table width="100%" border="0" cellpadding="5" cellspacing="5">
        <tr>
            <td style="width:50%">
                 <table width="100%" border="0">
                    <tr>
                        <td style="width:50%" valign="top">
                            <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             
                                   <span class="lblCaption" >   Present Complaint</span> 
                                     <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HPE_PresentComplaint', this)"  value="Template" />

                                   <br />
                                <asp:TextBox ID="txtPresentComplaient" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"   BorderColor="#cccccc"  ondblclick="return TemplatesPopup('HC_HPE_PresentComplaint',this);"  ></asp:TextBox>
                            </div>
                             <br />
                             <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                            <span class="lblCaption">  Past Medical History </span>  
                                <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HPE_PastMedicalHistory', this)"  value="Template" />                                <br />
                                 <asp:TextBox ID="txtPastMedicalHistory" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"    BorderColor="#cccccc"  ondblclick="return TemplatesPopup('HC_HPE_PastMedicalHistory',this);"></asp:TextBox>

                            </div>
                              <br />
                             <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                           <span class="lblCaption">  Past surgical History</span> 
                            <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HPE_PastSurgicalHistory', this)"  value="Template" />                                <br />
                                <asp:TextBox ID="txtSurgicallHistory" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"    BorderColor="#cccccc" ondblclick="return TemplatesPopup('HC_HPE_PastSurgicalHistory',this);"></asp:TextBox>
                            </div>
                              <br />
                             <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                           <span class="lblCaption"> Psycho socio- economic History	</span> 
                            <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HPE_PsychoSocEcoHistory', this)"  value="Template" />                                <br />
                            <asp:TextBox ID="txtPsychoSocEcoHistory" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"    BorderColor="#cccccc" ondblclick="return TemplatesPopup('HC_HPE_PsychoSocEcoHistory',this);"></asp:TextBox>

                            </div>
                              <br />
                             <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                          <span class="lblCaption">   Family History</span> 
                            <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HPE_FamilylHistory', this)"  value="Template" />                                <br />
                             <asp:TextBox ID="txtFamilyHistory" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"    BorderColor="#cccccc" ondblclick="return TemplatesPopup('HC_HPE_FamilylHistory',this);"></asp:TextBox>

                             </div>
                              <br />
                             <div style="display:none;padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                               <span class="lblCaption"> Physical Assessment</span>
                            <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HPE_PhysicalAssessment', this)"  value="Template" />                                <br />

                            <asp:TextBox ID="txtPhysicalAssessment" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"    BorderColor="#cccccc" ondblclick="return TemplatesPopup('HC_HPE_PhysicalAssessment',this);"></asp:TextBox>


                             </div>
                              <br />
                             <div style="display:none;padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             <span class="lblCaption">  Review of Systems </span>  
                            <input type="button" style="padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HPE_ReviewSystems', this)"  value="Template" />                                <br />
                            <asp:TextBox ID="txtReviewOfSystem" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"    BorderColor="#cccccc" ondblclick="return TemplatesPopup('HC_HPE_ReviewSystems',this);"></asp:TextBox>


                             </div>
                        </td>
                   </tr>
                   
                </table>
            </td>
             <td style="width:50%" valign="top">
                        <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             <span class="lblCaption">  Investigations</span> <br />
                                <asp:TextBox ID="txtInvestigations" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"    BorderColor="#cccccc" ondblclick="return ServicessShow('Investigations','Investigations');" ></asp:TextBox>
                        </div>
                        <br />
                        <div style="display:none;padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             <span class="lblCaption">  Diagnosis</span> <br />
                               <span class="lblCaption1"> Principal Diagnosis     : </span> <br />
                                <asp:TextBox ID="txtPrincipalDiagnosis" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"  BorderColor="#cccccc" ondblclick="return ServicessShow('PrincDiagnosis','Diagnosis');" ></asp:TextBox>
                                <br /> <br />
                                <span class="lblCaption1"> Secondary  Diagnosis     : </span> <br />
                                <asp:TextBox ID="txtSecondaryDiagnosis" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"    BorderColor="#cccccc" ondblclick="return ServicessShow('SecDiagnosis','Diagnosis');" ></asp:TextBox>

                                
                        </div>
                       



           </td>
        </tr>
    </table>
             </contenttemplate>

        </asp:TabPanel>
 <asp:TabPanel runat="server" ID="TabPanel3" HeaderText=" Physical Assessment " Width="100%" >
            <contenttemplate>
                 <table style="width: 100%" >
                    <tr>
                        <td style="width: 50%; vertical-align: top;">
                            <asp:PlaceHolder ID="plhoPhyAssessmentLeft" runat="server"></asp:PlaceHolder>
                        </td>
                        <td style="width: 50%; vertical-align: top;">
                            <asp:PlaceHolder ID="plhoPhyAssessmentRight" runat="server"></asp:PlaceHolder>

                                <asp:Panel ID="pnlROS" runat="server"   style="width:99.5%;border:thin;border-color:#F7F2F2;border-style:groove;border-radius:5px;height:25px;">

                                <div onclick="DivROSShow()"  style="padding: 2px;">
                                  <table cellpadding="0" cellspacing="0" >
                                      <tr>
                                          <td class="lblCaption" style="font-size:13px;width:97%"  >
                                             <asp:Label id="lblROS" runat="server" CssClass="lblCaption1" Text=" Review of Systems" style="font-family:Segoe UI,arial;font-size:13px;" ></asp:Label>  
                                          </td>
                                          <td class="lblCaption1"  >
                                            
                                                <asp:Label id="lblROSMax" runat="server" CssClass="lblCaption1" style="font-family:Arial Black;font-weight:bold;font-size:18px;" Text="+"  ></asp:Label>  
                                          </td>
                                      </tr>
                                  </table>
                                    
                               </div>
                                </asp:Panel>
                                <div id="divROSDtls"  style="display:none;padding: 5px; width: 98%;  overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                     <asp:PlaceHolder ID="plhoPhyROSRight" runat="server"></asp:PlaceHolder>
                                </div>
                            <br />
                             <asp:Panel ID="pnlPE" runat="server"   style="width:99.5%;border:thin;border-color:#F7F2F2;border-style:groove;border-radius:5px;height:25px;">

                                <div onclick="DivPEShow()" style="padding: 2px;">
                                  <table cellpadding="0" cellspacing="0" >
                                      <tr>
                                          <td class="lblCaption" style="font-size:13px;width:97%"  >
                                             <asp:Label id="lblPE" runat="server" CssClass="lblCaption1" Text=" Physical Exam" style="font-family:Segoe UI,arial;font-size:13px;" ></asp:Label>  
                                          </td>
                                          <td class="lblCaption1"  >
                                            
                                                <asp:Label id="lblPEMax" runat="server" CssClass="lblCaption1" style="font-family:Arial Black;font-weight:bold;font-size:18px;" Text="+"  ></asp:Label>  
                                          </td>
                                      </tr>
                                  </table>
                                    
                               </div>
                                </asp:Panel>
                                <div id="divPEDtls"  style="display:none;padding: 5px; width: 98%;  overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                     <asp:PlaceHolder ID="plhoPhyPESRight" runat="server"></asp:PlaceHolder>
                                </div>


                        </td>
                    </tr>
                     <tr>
                          <td style="width: 50%; vertical-align: top;">
    
                         </td>
                          <td style="width: 50%; vertical-align: top;">
                           
                              
                          </td>
                     </tr>
                </table>
            </contenttemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server" ID="TabPanel4" HeaderText=" Plan" Width="100%">
            <contenttemplate>
                <table>
                    <tr>
                     <td valign="top" style="width:49%">
                            
                     <fieldset style="display:none;">
                 <legend class="lblCaption1" style="font-weight:bold;">Braden Skin Risk Assessment </legend>
                     <table>
                         <tr>
                             <td>
                                 <span class="lblCaption1"  > Sensory perception </span><br />
                                 <asp:textbox id="txtHC_ASS_Risk_SkinRisk_Sensory" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_SkinRisk_Sensory',this)"></asp:textbox>
                             </td>
                         </tr>  

                         <tr>
                             <td>
                                 <span class="lblCaption1" >  Moisture </span><br />
                                 <asp:textbox id="txtHC_ASS_Risk_SkinRisk_Moisture" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_SkinRisk_Moisture',this)"></asp:textbox>
                             </td>
                         </tr>  
                         <tr>
                             <td>
                                 <span class="lblCaption1"  > Activity</span><br />
                                 <asp:textbox id="txtHC_ASS_Risk_SkinRisk_Activity" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_SkinRisk_Activity',this)"></asp:textbox>
                             </td>
                         </tr>  
                         <tr>
                             <td>
                                 <span class="lblCaption1"  > Mobility </span><br />
                                 <asp:textbox id="txtHC_ASS_Risk_SkinRisk_Mobility" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_SkinRisk_Mobility',this)"></asp:textbox>
                             </td>
                         </tr>  
                         <tr>
                             <td>
                                 <span class="lblCaption1" > Nutrition </span><br />
                                 <asp:textbox id="txtHC_ASS_Risk_SkinRisk_Nutrition" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_SkinRisk_Nutrition',this)"></asp:textbox>
                             </td>
                         </tr>  
                         <tr>
                             <td>
                                 <span class="lblCaption1" > Friction / Shear </span><br />
                                 <asp:textbox id="txtHC_ASS_Risk_SkinRisk_Friction" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_SkinRisk_Friction',this)"></asp:textbox>
                             </td>
                         </tr>  
                        
                     </table>
                 </fieldset>

                <fieldset style="display:none;">
                 <legend class="lblCaption1" style="font-weight:bold;">Educational Assessment </legend>
                     <table>
                         <tr>
                             <td>
                                 <span class="lblCaption1"  > Patient and / or family Need Eduction On </span><br />
                                 <asp:textbox id="txtHC_ASS_Risk_Edu_General" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_Edu_General',this)"></asp:textbox>
                             </td>
                         </tr>  
 
                        
                     </table>
                 </fieldset>
 <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             <span class="lblCaption">  Treatment Plan and Patient Education</span> 
                            <input type="button" style="display:none;padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HPE_TreatmentPlan', this)"  value="Template" />                            <br />
                                <asp:TextBox ID="txtTreatmentPlan" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"    BorderColor="#cccccc" ondblclick="return TemplatesPopup('HC_HPE_TreatmentPlan',this);"></asp:TextBox>
                        </div>
 <br />
                        <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             <span class="lblCaption"> Follow Up notes</span> 
                            <input type="button" style="display:none;padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HPE_FollowuUpNotes', this)"  value="Template" />                                <br />
                          <asp:TextBox ID="txtFollowUpNotes" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"    BorderColor="#cccccc" ondblclick="return TemplatesPopup('HC_HPE_FollowuUpNotes',this);" ></asp:TextBox>
                        </div>

                        </td>
                   <td style="width:1%;"></td>
                       <td valign="top" style="width:49%">
                                       
                         <fieldset style="display:none;">
                         <legend class="lblCaption1" style="font-weight:bold;">Socioeconomic </legend>
                             <table>
                                 <tr>
                                     <td>
                                         <span class="lblCaption1" > Living Situation </span><br />
                                         <asp:textbox id="txtHC_ASS_Risk_Socio_Living" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_Socio_Living',this)"></asp:textbox>
                                     </td>
                                 </tr>  
                        
                             </table>
                         </fieldset>
                         <fieldset style="display:none;">
                         <legend class="lblCaption1" style="font-weight:bold;">Fall/Safety Risk </legend>
                             <table>
                                 <tr>
                                     <td>
                                         <span class="lblCaption1" > </span><br />
                                         <asp:textbox id="txtHC_ASS_Risk_Safety_General" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_Safety_General',this)"></asp:textbox>
                                     </td>
                                 </tr>  
                        
                             </table>
                         </fieldset>
                         <fieldset style="display:none;">
                         <legend class="lblCaption1" style="font-weight:bold;">Functional Assessment </legend>
                             <table>
                                 <tr>
                                     <td>
                                         <span class="lblCaption1" >Self Caring </span><br />
                                         <asp:textbox id="txtHC_ASS_Risk_Fun_SelfCaring" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_Fun_SelfCaring',this)"></asp:textbox>
                                     </td>
                                 </tr>  
                                 <tr>
                                     <td>
                                         <span class="lblCaption1" >Musculoskeletal   </span><br />
                                         <asp:textbox id="txtHC_ASS_Risk_Fun_Musculos" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_Fun_Musculos',this)"></asp:textbox>
                                     </td>
                                 </tr>  
                                 <tr>
                                     <td>
                                         <span class="lblCaption1" >Use of Assisting Equipment   </span><br />
                                         <asp:textbox id="txtHC_ASS_Risk_Fun_Equipment" cssclass="label" runat="server" width="99%" height="50px" textmode="MultiLine" borderwidth="1px" bordercolor="#CCCCCC" ondblclick="return TemplatesCtrlPopup('HC_ASS_Risk_Fun_Equipment',this)"></asp:textbox>
                                     </td>
                                 </tr>  

                             </table>
                         </fieldset>
                           
                        <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             <span class="lblCaption"> Procedure</span> <br />
                                <asp:TextBox ID="txtProcedure" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"    BorderColor="#cccccc" ondblclick="return ServicessShow('Procedure','Procedure');"  ></asp:TextBox>
                        </div>

                        <br />
                        <div style="padding:5px; width: 95%;border: thin; border-color: #cccccc; border-style: solid;border-radius:4px;">
                             <span class="lblCaption"> Treatment</span> 
                            <input type="button" style="display:none;padding-left:5px;padding-right:5px;width:70px;height:25px;float:right;" class="button orange small"  onclick="TemplateSave('HC_HPE_Treatment', this)"  value="Template" />                            <br />
                            <asp:TextBox ID="txtTreatment" runat="server" Width="100%" CssClass="label"   BorderWidth="1px"   Height="50px"  TextMode="MultiLine"    BorderColor="#cccccc" ondblclick="return TemplatesPopup('HC_HPE_Treatment',this);" ></asp:TextBox>
                        </div>

                       
                        </td>
                    </tr>

                </table>
            </contenttemplate>
        </asp:TabPanel>
          
    </asp:tabcontainer>



    </div>



    <br />
</asp:content>
