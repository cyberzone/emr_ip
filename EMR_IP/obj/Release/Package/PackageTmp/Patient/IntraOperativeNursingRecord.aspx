﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="IntraOperativeNursingRecord.aspx.cs" Inherits="EMR_IP.Patient.IntraOperativeNursingRecord" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

        function ShowErrorMessage(vMessage) {

            document.getElementById("divMessage").style.display = 'block';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = vMessage;
        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = '';
        }

    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />
    
    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully 
                
        </div>
    </div>
    <div style="padding-left: 60%; width: 100%;">
        <div id="divMessage" style="display: none; border: groove; height: 50px; width: 300px; background-color: brown; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right" valign="top">

                        <input type="button" id="btnMsgClose" class="ButtonStyle" style="background-color: White; color: black; border: none;" value=" X " onclick="HideErrorMessage()" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMessage" runat="server" CssClass="label"></asp:Label>
                    </td>
                </tr>
            </table>


            <br />

        </div>
    </div>
      <table width="100%"  >
            <tr>
                <td style="text-align:left;width:50%;" >
                    <h3 class="box-title">Intra Operative Nursing Record</h3>
                </td>
                <td style="text-align:right;;width:50%;">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Saved Record" OnClick="btnSave_Click" />
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
        </table>

    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Intra Operative1" Width="100%">
            <ContentTemplate>
                <table style="width: 100%">
                    <tr>
                        <td class="lblCaption1">Date & Time
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel ID="updatePanel3" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtInterOperDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                        Enabled="True" TargetControlID="txtInterOperDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:DropDownList ID="drpInterOperHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                    <asp:DropDownList ID="drpInterOperMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1">Pre-operative Diagnosis
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel ID="updatePanel12" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtPreOperDiag" runat="server" CssClass="label" TextMode="MultiLine" Width="100%" Height="50px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Post Operative Diagnosis
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel ID="updatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtPostOperDiag" runat="server" CssClass="label" TextMode="MultiLine" Width="100%" Height="50px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Operative procedure Performed
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel ID="updatePanel4" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtOperProcPerf" runat="server" CssClass="label" TextMode="MultiLine" Width="100%" Height="50px" Style="resize: none;"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Name of Surgeon
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel5">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpSurgeon" runat="server" CssClass="label" Width="200px"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Name of Anaesthetist
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel6">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpAnaesthetist" runat="server" CssClass="label" Width="200px"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>


                    </tr>
                    <tr>
                        <td class="lblCaption1">Anesthesia Type 
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel69" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpAnesType" runat="server" CssClass="label" Width="200px">
                                        <asp:ListItem Text="--- Select ---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="GA" Value="GA"></asp:ListItem>
                                        <asp:ListItem Text="SA" Value="SA"></asp:ListItem>
                                        <asp:ListItem Text="LA" Value="LA"></asp:ListItem>
                                        <asp:ListItem Text="Epidural" Value="Epidural"></asp:ListItem>
                                        <asp:ListItem Text="Others" Value="Others"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">ASA Classification
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel70" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpAnesClassifi" runat="server" CssClass="label" Width="200px">
                                        <asp:ListItem Text="--- Select ---" Value=""></asp:ListItem>
                                        <asp:ListItem Text="I" Value="I"></asp:ListItem>
                                        <asp:ListItem Text="II" Value="II"></asp:ListItem>
                                        <asp:ListItem Text="III" Value="III"></asp:ListItem>
                                        <asp:ListItem Text="IV" Value="IV"></asp:ListItem>
                                        <asp:ListItem Text="V" Value="V"></asp:ListItem>
                                        <asp:ListItem Text="VI" Value="VI"></asp:ListItem>
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1" valign="top">Assessment
                        </td>
                        <td colspan="3">

                            <fieldset>
                                <legend class="lblCaption1">Assessment</legend>
                                <table width="100%">

                                    <tr>
                                        <td class="lblCaption1">Patient Identification 
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel10" runat="server">
                                                <ContentTemplate>
                                                    <asp:RadioButtonList ID="radInfoMedicFood" runat="server" CssClass="lblCaption1" Width="250px" RepeatDirection="Horizontal">
                                                        <asp:ListItem Text="ID Band" Value="ID Band"></asp:ListItem>
                                                        <asp:ListItem Text="Verbal" Value="Verbal"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Allergies
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel7" runat="server">
                                                <ContentTemplate>
                                                    <input type="radio" id="radAllergiesYes" name="Allergies" runat="server" class="lblCaption1" value="Yes" /><span class="lblCaption1">Yes</span>
                                                    <input type="radio" id="radAllergiesNo" name="Allergies" runat="server" class="lblCaption1" value="No" /><span class="lblCaption1">No</span>
                                                    <input type="text" id="txtAlergies" runat="server" class="label" style="width: 400px;" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">NPO
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel8" runat="server">
                                                <ContentTemplate>
                                                    <input type="radio" id="radNPOYes" name="radNPO" runat="server" class="lblCaption1" value="Yes" /><span class="lblCaption1">Yes</span>
                                                    <input type="radio" id="radNPONo" name="radNPO" runat="server" class="lblCaption1" value="No" /><span class="lblCaption1">No</span>
                                                    <input type="text" id="txtNPO" runat="server" class="label" style="width: 400px;" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Operative Consent
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel9" runat="server">
                                                <ContentTemplate>
                                                    <input type="radio" id="radOperConYes" name="radOperCon" runat="server" class="lblCaption1" value="Yes" /><span class="lblCaption1">Yes</span>
                                                    <input type="radio" id="radOperConNo" name="radOperCon" runat="server" class="lblCaption1" value="No" /><span class="lblCaption1">No</span>
                                                    <input type="text" id="txtOperCon" runat="server" class="label" style="width: 400px;" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Time out
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel11" runat="server">
                                                <ContentTemplate>
                                                    <input type="radio" id="radTimeOutYes" name="radTimeOut" runat="server" class="lblCaption1" value="Yes" /><span class="lblCaption1">Yes</span>
                                                    <input type="radio" id="radTimeOutNo" name="radTimeOut" runat="server" class="lblCaption1" value="No" /><span class="lblCaption1">No</span>
                                                    <input type="text" id="txtTimeOut" runat="server" class="label" style="width: 400px;" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Varification operative checklist
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel13" runat="server">
                                                <ContentTemplate>
                                                    <input type="radio" id="radVarifOperCheckList" name="radVarifOperCheckList" runat="server" class="lblCaption1" value="Yes" /><span class="lblCaption1">Yes</span>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Comment
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel14" runat="server">
                                                <ContentTemplate>

                                                    <asp:TextBox ID="txtAssComment" CssClass="label" runat="server" Width="100%" Height="50px" TextMode="MultiLine" BorderWidth="1px" BorderColor="#CCCCCC" ondblclick="return TemplatesPopup('HC_HEALTH_ALLERGIES_OTHER',this);"></asp:TextBox>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>

                                        </td>

                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                        <td></td>
                    </tr>


                    <tr>
                        <td class="lblCaption1">Case Classification
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel62" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpCaseClasification" Width="200px" runat="server" CssClass="label">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Type
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel63" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpCaseType" Width="200px" runat="server" CssClass="label">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>

                    <tr>
                        <td class="lblCaption1">Intubation time
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel64" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtIntubationTime" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Incision time
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel65" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtIncisionTime" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>

                    <tr>
                        <td class="lblCaption1">Hair Removal 
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel15" runat="server">
                                <ContentTemplate>
                                    <input type="radio" id="radHairRemovalYes" name="radHairRemoval" runat="server" class="lblCaption1" value="Yes" /><span class="lblCaption1">Yes</span>
                                    <input type="radio" id="radHairRemovalNo" name="radHairRemoval" runat="server" class="lblCaption1" value="No" /><span class="lblCaption1">No</span>
                                    <input type="radio" id="radClipped" name="radHairRemoval" runat="server" class="lblCaption1" value="Clipped" /><span class="lblCaption1">Clipped</span>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Location
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel59" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtDrainsLocation" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>

                    <tr>
                        <td class="lblCaption1">Skin condition
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel60" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpSkinCondition" Width="200px" runat="server" CssClass="label">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                        <td class="lblCaption1"></td>
                        <td></td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="Potential For Injury" Width="100%">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td class="lblCaption1">Position
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel21" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpPosition" Width="200px" runat="server" CssClass="label">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Positional Aide
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel22" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpPositionalAide" Width="200px" runat="server" CssClass="label">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1">Safety strap on
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel23" runat="server">
                                <ContentTemplate>
                                    <input type="radio" id="radSafetyStrapYes" name="radSafetyStrapYes" runat="server" class="lblCaption1" value="True" /><label class="lblCaption1" for="radSafetyStrapYes">Yes</label>
                                    <input type="radio" id="radSafetyStrapNo" name="radSafetyStrapYes" runat="server" class="lblCaption1" value="False" /><label class="lblCaption1" for="radSafetyStrapNo"> No </label>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Positioned By
                        </td>
                        <td class="lblCaption1">

                            <asp:UpdatePanel ID="updatePanel24" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpSafetyStrapPositionedBy" runat="server" CssClass="label" Width="200px"></asp:DropDownList>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel25">
                                <ContentTemplate>

                                    <input type="checkbox" id="chkLeftArmStraps" name="chkLeftArmStraps" runat="server" class="lblCaption1" value="Left" /><label class="lblCaption1" for="chkLeftArmStraps">Left arm straps-Board less than 90 degrees</label>
                                    <input type="checkbox" id="chkRightArmStraps" name="chkRightArmStraps" runat="server" class="lblCaption1" value="Right" /><label class="lblCaption1" for="chkRightArmStraps"> Right arm straps-Board less than 90 Degrees </label>


                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>

                    </tr>
                </table>
                <fieldset>


                    <table width="100%">
                        <tr>
                            <td class="lblCaption1">Electrosurgical Unit
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel26" runat="server">
                                    <ContentTemplate>
                                        <input type="radio" id="radElectroUnitYes" name="radElectroUnitYes" runat="server" class="lblCaption1" value="True" /><span class="lblCaption1">Yes </span>
                                        <input type="radio" id="radElectroUnitNo" name="radElectroUnitYes" runat="server" class="lblCaption1" value="False" /><span class="lblCaption1"> No </span>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Return electrode location
                            </td>
                            <td class="lblCaption1">

                                <asp:UpdatePanel ID="updatePanel27" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtRetElectroLocation" runat="server" CssClass="label" Width="200px"></asp:TextBox>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>

                        <tr>
                            <td class="lblCaption1">Settings
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="updatePanel28" runat="server">
                                    <ContentTemplate>
                                        Cut:&nbsp;
                                     <asp:TextBox ID="txtSettingsCut" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                        Coag.:&nbsp;
                                     <asp:TextBox ID="txtSettingsCoag" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                        Bipolar:&nbsp;
                                     <asp:TextBox ID="txtBipolar" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Applied By 
                            </td>
                            <td class="lblCaption1">

                                <asp:UpdatePanel ID="updatePanel29" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpElectroUnitAppliedBy" runat="server" CssClass="label" Width="200px"></asp:DropDownList>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>

                </fieldset>
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td class="lblCaption1">Post op skin condition under pad
                            </td>
                            <td class="lblCaption1" colspan="3">

                                <asp:UpdatePanel ID="updatePanel30" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSkinCondition" runat="server" CssClass="label" Width="90%"></asp:TextBox>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                        <tr>
                            <td class="lblCaption1">Tourniquets
                            </td>
                            <td class="lblCaption1">

                                <asp:UpdatePanel ID="updatePanel42" runat="server">
                                    <ContentTemplate>
                                        Time up: &nbsp;
                                                <asp:TextBox ID="txtTourniTimeUpDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                            Enabled="True" TargetControlID="txtTourniTimeUpDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:DropDownList ID="drpourniTimeUpHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                        <asp:DropDownList ID="drpourniTimeUpMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Time Down:
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel43" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtTourniTimeDownDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                            Enabled="True" TargetControlID="txtTourniTimeDownDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:DropDownList ID="drpourniTimeDownHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                        <asp:DropDownList ID="drpourniTimeDownMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                        </tr>
                        <tr>

                            <td class="lblCaption1">Skin status
                            </td>
                            <td class="lblCaption1">

                                <asp:UpdatePanel ID="updatePanel32" runat="server">
                                    <ContentTemplate>
                                        Before placement:&nbsp;
                                    <asp:TextBox ID="txtSkinStatusBeforePlacement" runat="server" CssClass="label" Width="200px"></asp:TextBox>


                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">After placement:&nbsp;
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel44" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSkinStatusAfterPlacement" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>

                            <td class="lblCaption1">Site/Location
                            </td>
                            <td class="lblCaption1">

                                <asp:UpdatePanel ID="updatePanel33" runat="server">
                                    <ContentTemplate>

                                        <asp:TextBox ID="txtSiteLocation" runat="server" CssClass="label" Width="200px"></asp:TextBox>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Applied By
                            </td>
                            <td class="lblCaption1" c>

                                <asp:UpdatePanel ID="updatePanel34" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpSkinAppliedBy" runat="server" CssClass="label" Width="200px"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Intra Operative2" Width="100%">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td class="lblCaption1">Counts
                        </td>
                        <td class="lblCaption1">1ST
                        </td>
                        <td class="lblCaption1">2ND
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">SPONGE
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel71" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtSponge1" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel72" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtSponge2" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">GAUZE
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel73" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtGauze1" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel74" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtGauze2" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">PAENUT
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel75" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtPaenut1" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel76" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtPaenut2" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">NEEDLE
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel77" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtNeedle1" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel78" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtNeedle2" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">INSTRUMENTS
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel79" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtInstruments1" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel80" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtInstruments2" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">COTTON BALLS
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel81" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCottonBalls1" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel82" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCottonBalls2" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>

                <table width="100%">
                    <tr>
                        <td style="width: 300px;">
                            <asp:UpdatePanel ID="updatePanel16" runat="server">
                                <ContentTemplate>

                                    <input type="checkbox" id="chkCountCorYes" name="radCountCorYes" runat="server" class="lblCaption1" value="True" /><span class="lblCaption1"> Count Correct </span>
                                    <input type="checkbox" id="chkCountCorSurgeonInformed" name="radCountCorSurgeonInformed" runat="server" class="lblCaption1" value="True" /><span class="lblCaption1"> Surgeon Informed </span>


                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                        <td colspan="3">
                            <asp:UpdatePanel ID="updatePanel17" runat="server">
                                <ContentTemplate>
                                    <input type="checkbox" id="chkCountincorYes" name="radCountincorrectYes" runat="server" class="lblCaption1" value="True" /><span class="lblCaption1"> Count InCorrect </span>
                                    <input type="checkbox" id="chkCountincorSurgeonInformed" name="radCountincorrectYes" runat="server" class="lblCaption1" value="True" /><span class="lblCaption1"> surgeon informed </span>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Corrective Action Taken
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel18" runat="server">
                                <ContentTemplate>
                                    <input type="checkbox" id="radCorrectiveActionTaken" name="radCountincorrectYes" runat="server" class="lblCaption1" value="True" /><span class="lblCaption1"> Yes </span>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Comment 
                        </td>
                        <td class="lblCaption1">
                            <asp:UpdatePanel ID="updatePanel83" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtCorrectiveComment" runat="server" CssClass="label" Height="30px" Width="300px" TextMode="MultiLine"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Extubation Time
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel66" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtExtubationTime" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Drains/packing
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel58" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtDrainspacking" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>


                    </tr>
                    <tr>
                        <td class="lblCaption1">Clipper
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel57" runat="server">
                                <ContentTemplate>
                                    <input type="radio" id="radClipperYes" name="radClipperYes" runat="server" class="lblCaption1" value="Yes" /><span class="lblCaption1">Yes</span>
                                    <input type="radio" id="radClipperNo" name="radClipperYes" runat="server" class="lblCaption1" value="No" /><span class="lblCaption1">No</span>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                        <td class="lblCaption1">Dressing
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel61" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtDressing" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                    </tr>
                    <tr>
                        <td class="lblCaption1">Specimen
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel67" runat="server">
                                <ContentTemplate>
                                    <input type="radio" id="radpecimenYes" name="chkSpecimenYes" runat="server" class="lblCaption1" value="Yes" /><span class="lblCaption1"> Yes </span>
                                    <input type="radio" id="radSpecimenNo" name="chkSpecimenYes" runat="server" class="lblCaption1" value="No" /><span class="lblCaption1"> No </span>

                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Specimen Type
                        </td>
                        <td>
                            <asp:UpdatePanel ID="updatePanel68" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpSpecimenType" Width="200px" runat="server" CssClass="label">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Patient Transferred 
                        </td>
                        <td colspan="3">
                            <asp:UpdatePanel ID="updatePanel20" runat="server">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpPatientTransTo" Width="200px" runat="server" CssClass="label">
                                    </asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel3" HeaderText="Potential for fluid Imbalance" Width="100%">
            <ContentTemplate>
                <fieldset>
                    <table width="100%">
                        <tr>
                            <td class="lblCaption1" style="width: 150px;">Urinary  Catheter:
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="updatePanel35" runat="server">
                                    <ContentTemplate>
                                        Foly cath No &nbsp;
                                     <asp:TextBox ID="txtUriCatheterFolyCathno" runat="server" CssClass="label" Width="100px"></asp:TextBox>


                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Inserted By
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel41" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpUriCatheterInsertedBy" runat="server" CssClass="label" Width="200px"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Urine Returned</td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel36" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpUrineReturned" Width="200px" runat="server" CssClass="label">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset>
                    <legend class="lblCaption1">Intake</legend>
                    <table>
                        <tr>
                            <td class="lblCaption1" style="width: 150px;">Per Anesthesiologist</td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel37" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpPreAnesthesiologist" runat="server" CssClass="label" Width="200px"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">

                                <asp:UpdatePanel ID="updatePanel38" runat="server">
                                    <ContentTemplate>
                                        Total Blood Products  &nbsp;
                                  <asp:TextBox ID="txtTotalBloodProd" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                        Total IV Fluids  &nbsp;
                                  <asp:TextBox ID="txtTotalIVFluids" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </fieldset>
                <fieldset>
                    <legend class="lblCaption1">Output</legend>
                    <table>
                        <tr>
                            <td class="lblCaption1" style="width: 150px;">Urinary Drainage </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel39" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtUrinaryDrainage" runat="server" CssClass="label" Width="195px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">

                                <asp:UpdatePanel ID="updatePanel40" runat="server">
                                    <ContentTemplate>
                                        Estimated Blood Loss  &nbsp;
                                  <asp:TextBox ID="txtEstBloodLoss" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                        Others   &nbsp;
                                  <asp:TextBox ID="txtOutPutOthers" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </ContentTemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server" ID="TabPanel4" HeaderText="Intra Operative3" Width="100%">
            <ContentTemplate>
                <fieldset>
                    <legend class="lblCaption1">PROSTHESIS/IMPLANTS/GRAFT</legend>
                    <table>
                        <tr>


                            <td class="lblCaption1" style="width: 150px;">Location/Type</td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel31" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtProsthesisLocation" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Size</td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel45" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtProsthesisSize" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Lot/Serial No</td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel46" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtProsthesisSlNo" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>

                        <tr>
                            <td class="lblCaption1">Manufacturer
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel50" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtCarmManufacturer" runat="server" CssClass="label" Width="100px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </fieldset>

                <fieldset>
                    <legend class="lblCaption1">X-Ray/LASER</legend>
                    <table>
                        <tr>


                            <td class="lblCaption1" style="width: 150px;">C-arm</td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel47" runat="server">
                                    <ContentTemplate>
                                        <input type="radio" id="radCarmYes" name="radCarmYes" runat="server" class="lblCaption1" value="Yes" /><span class="lblCaption1">Yes</span>
                                        <input type="radio" id="radCarmNo" name="radCarmYes" runat="server" class="lblCaption1" value="No" /><span class="lblCaption1">No</span>
                                        <input type="radio" id="radCarmNA" name="radCarmYes" runat="server" class="lblCaption1" value="NA" /><span class="lblCaption1">Not Applicable</span>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                        <tr>
                            <td class="lblCaption1">Laser</td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel48" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtLaser" runat="server" CssClass="label" Width="100%"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>

                    </table>
                </fieldset>

                <fieldset>
                    <legend class="lblCaption1">Others</legend>
                    <table>
                        <tr>


                            <td class="lblCaption1" style="width: 150px;">Microscope</td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel51" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtOthersMicroscope" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Others</td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel52" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtOthersOthers" runat="server" CssClass="label" Width="200px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </fieldset>

                <fieldset>
                    <legend class="lblCaption1">Comment</legend>
                    <table>
                        <tr>

                            <td>
                                <asp:UpdatePanel ID="updatePanel53" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtComment" runat="server" CssClass="label" Height="50px" Width="100%" TextMode="MultiLine" Style="resize: none;"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                    </table>
                </fieldset>

            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel5" HeaderText="Signature" Width="100%">
            <ContentTemplate>
                <table width="100%">

                    <tr>
                        <td>
                            <fieldset>
                                <legend class="lblCaption1">Scrub Nurse </legend>
                                <table width="100%">
                                    <tr>

                                        <td class="lblCaption1" style="width: 150px">Name  
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpScrubNurse" runat="server" CssClass="label" Width="205px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="width: 150px">Password
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtScrubNursePass" runat="server" CssClass="label" TextMode="Password" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Date & Time
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel54" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtScrubNurseDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="Calendarextender5" runat="server"
                                                        Enabled="True" TargetControlID="txtScrubNurseDate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:DropDownList ID="drpScrubNurseHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                    <asp:DropDownList ID="drpScrubNurseMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>


                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>

                                </table>


                            </fieldset>
                        </td>
                        <td>
                            <fieldset>
                                <legend class="lblCaption1">Circulatory Nurse</legend>
                                <table width="100%">
                                    <tr>

                                        <td class="lblCaption1" style="width: 150px">Name  
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpCirculNurse" runat="server" CssClass="label" Width="205px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="width: 150px">Password
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCirculNursePass" runat="server" CssClass="label" TextMode="Password" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Date & Time
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel49" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtCirculNurseDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="Calendarextender4" runat="server"
                                                        Enabled="True" TargetControlID="txtCirculNurseDate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>

                                                    <asp:DropDownList ID="drpCirculNurseHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                    <asp:DropDownList ID="drpCirculNurseMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>

                                </table>
                            </fieldset>
                        </td>

                    </tr>

                    <tr>
                        <td>
                            <fieldset>
                                <legend class="lblCaption1">Releiver Scrub Nurse </legend>
                                <table width="100%">
                                    <tr>

                                        <td class="lblCaption1" style="width: 150px">Name  
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpReScrubNurse" runat="server" CssClass="label" Width="205px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="width: 150px">Password
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtReScrubNursePass" runat="server" CssClass="label" TextMode="Password" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Date & Time
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel55" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtReScrubNurseDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="Calendarextender6" runat="server"
                                                        Enabled="True" TargetControlID="txtReScrubNurseDate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:DropDownList ID="drpReScrubNurseHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                    <asp:DropDownList ID="drpReScrubNurseMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>


                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>

                                </table>


                            </fieldset>
                        </td>
                        <td>
                            <fieldset>
                                <legend class="lblCaption1">Releiver Circulatory Nurse</legend>
                                <table width="100%">
                                    <tr>

                                        <td class="lblCaption1" style="width: 150px">Name  
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpReCirculNurse" runat="server" CssClass="label" Width="205px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="width: 150px">Password
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtReCirculNursePass" runat="server" CssClass="label" TextMode="Password" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Date & Time
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel56" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtReCirculNurseDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="Calendarextender7" runat="server"
                                                        Enabled="True" TargetControlID="txtReCirculNurseDate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>

                                                    <asp:DropDownList ID="drpReCirculNurseHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                    <asp:DropDownList ID="drpReCirculNurseMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>

                                </table>
                            </fieldset>
                        </td>

                    </tr>

                </table>
            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>
    <br />
    <br />
</asp:Content>
