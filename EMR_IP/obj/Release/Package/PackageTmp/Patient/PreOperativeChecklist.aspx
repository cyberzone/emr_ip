﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="PreOperativeChecklist.aspx.cs" Inherits="EMR_IP.Patient.PreOperativeChecklist" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>

    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidthProc
        {
            width: 400px !important;
        }

            #divwidthProc div
            {
                width: 400px !important;
            }
    </style>

    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

        function ShowErrorMessage(vMessage) {

            document.getElementById("divMessage").style.display = 'block';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = vMessage;
        }

        function HideErrorMessage() {

            document.getElementById("divMessage").style.display = 'none';
            document.getElementById("<%=lblMessage.ClientID%>").innerHTML = '';
        }

    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }



        function ServNameSelected() {
            if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
            }
        }

        return true;
    }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />
    <h3 class="box-title">Pre-OperativeChecklistt</h3>

    <br />
    <br />
    <br />
    <br />
    <br />
    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully 
                
        </div>
    </div>
    <div style="padding-left: 60%; width: 100%;">
        <div id="divMessage" style="display: none; border: groove; height: 50px; width: 300px; background-color: brown; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 10px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right" valign="t;">

                        <input type="button" id="btnMsgClose" class="ButtonStyle" style="background-color: White; color: black; border: none;" value=" X " onclick="HideErrorMessage()" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblMessage" runat="server" CssClass="label"></asp:Label>
                    </td>
                </tr>
            </table>


            <br />

        </div>
    </div>
    <div style="float: right;">

        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Saved Record" OnClick="btnSave_Click" />


            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <br />
    <br />
    <br />
    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Procedure" Width="100%">
            <ContentTemplate>
                <fieldset>



                    <table style="width: 100%">
                        <tr>
                            <td class="lblCaption1" valign="top">Procedure
                            </td>
                            <td colspan="3" valign="top">
                                <asp:UpdatePanel runat="server" ID="updatePanel19">
                                    <ContentTemplate>

                                        <asp:TextBox ID="txtServCode" runat="server" Width="150px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Enabled="false"></asp:TextBox>
                                        <asp:TextBox ID="txtServName" runat="server" Width="99%" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()"></asp:TextBox>
                                        <div id="divwidth" style="visibility: hidden;"></div>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtServCode" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                        </asp:AutoCompleteExtender>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtServName" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                        </asp:AutoCompleteExtender>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>


                        </tr>
                        <tr>
                            <td class="lblCaption1">Date & Time
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel3" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtORCallDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                            Enabled="True" TargetControlID="txtORCallDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                        <asp:DropDownList ID="drpProHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                        <asp:DropDownList ID="drpProMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                            <td class="lblCaption1">Surgeon
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel7">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpSurgeon" runat="server" CssClass="label" Width="250px"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:UpdatePanel runat="server" ID="updatePanel6">
                                    <ContentTemplate>
                                        <asp:Button ID="btnAddPro" runat="server" CssClass="orange" Width="50px" Text="Add" OnClick="btnAddPror_Click" Style="width: 50px; border-radius: 5px 5px 5px 5px" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>

                    </table>

                    <table style="width: 100%">
                        <tr>
                            <td colspan="4">
                                <div style="padding-top: 0px; width: 100%; height: 330px; overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                    <asp:UpdatePanel runat="server" ID="updatePanel4">
                                        <ContentTemplate>
                                            <asp:GridView ID="gvProc" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                                EnableModelValidation="True" GridLines="None" Width="100%">
                                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                                <RowStyle CssClass="GridRow" />


                                                <Columns>
                                                    <asp:TemplateField HeaderText="" HeaderStyle-Width="5%">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" OnClientClick="return window.confirm('Do you want to Delete?')"  ImageUrl="~/Images/icon_delete.jpg" Height="18" Width="18"
                                                                OnClick="DeleteDiag_Click" />&nbsp;&nbsp;
                                                
                                                        </ItemTemplate>

                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkPhyDesc" runat="server" OnClick="PhySelect_Click">
                                                                <asp:Label ID="lblgvProcID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_PRO_ID") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblPhyQTY" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_QTY") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblPhyCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_PRO_CODE") %>'></asp:Label>
                                                                <br />
                                                                <asp:Label ID="lblPhyDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_PRO_NAME") %>'></asp:Label>
                                                            </asp:LinkButton>

                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="Date & Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkgvDate" runat="server" OnClick="PhySelect_Click">
                                                                <asp:Label ID="lblgvDateDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_DATEDesc") %>'></asp:Label>&nbsp;
                                                                        <asp:Label ID="lblgvTimeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_DATETimeDesc") %>'></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Surgeon" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="250px">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkSurg" runat="server" OnClick="PhySelect_Click">
                                                                <asp:Label ID="lblgvSurgName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_SURG_NAME") %>'></asp:Label>
                                                                <asp:Label ID="lblgvSurgID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_SURG_ID") %>' Visible="false"></asp:Label>
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>


                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </td>

                        </tr>
                    </table>
                </fieldset>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Check List" Width="100%">
            <ContentTemplate>
                <div style="padding-top: 0px; width: 100%; height: 500px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">

                    <asp:GridView ID="gvPreOperative" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                        EnableModelValidation="True" Width="99%" GridLines="None" OnRowDataBound="gvPreOperative_RowDataBound">
                        <HeaderStyle CssClass="GridHeader_Blue" />
                        <RowStyle CssClass="GridRow" />
                        <AlternatingRowStyle CssClass="GridAlterRow" />
                        <Columns>
                            <asp:TemplateField HeaderText="Description" HeaderStyle-Width="50%">
                                <ItemTemplate>
                                    <asp:Label ID="lblLevelType" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPSM_LEVEL") %>' Visible="false"></asp:Label>
                                    <asp:Label ID="lblFieldID" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPSM_FIELD_ID") %>' Visible="false"></asp:Label>

                                    <asp:Label ID="Label1" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPSM_FIELD_NAME") %>' Width="100%"></asp:Label>

                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ward Nurse" HeaderStyle-Width="25%">
                                <ItemTemplate>
                                    <asp:RadioButtonList ID="radCheck" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="200px">
                                        <asp:ListItem Text="Yes" Value="Yes"> </asp:ListItem>
                                        <asp:ListItem Text="No" Value="No"> </asp:ListItem>
                                        <asp:ListItem Text="N/A" Value="N/A"> </asp:ListItem>
                                    </asp:RadioButtonList>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Details" HeaderStyle-Width="25%">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtDtls" runat="server" CssClass="lblCaption1" TextMode="MultiLine" Height="30px" Width="100%" Style="resize: none;"></asp:TextBox>
                                </ItemTemplate>

                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Countercheck by OT Nurse" HeaderStyle-Width="25%">
                                <ItemTemplate>
                                    <asp:RadioButtonList ID="radCheckOTNurse" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="200px">
                                        <asp:ListItem Text="Yes" Value="Yes"> </asp:ListItem>
                                        <asp:ListItem Text="No" Value="No"> </asp:ListItem>

                                    </asp:RadioButtonList>
                                </ItemTemplate>

                            </asp:TemplateField>

                        </Columns>

                    </asp:GridView>


                </div>
            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="Signature" Width="100%">
            <ContentTemplate>
                <table width="100%">

                    <tr>
                        <td>
                            <fieldset>
                                <legend class="lblCaption1">Ward Nurse</legend>
                                <table width="100%">
                                    <tr>

                                        <td class="lblCaption1" style="width: 150px">Name  
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpWardNurse" runat="server" CssClass="label" Width="205px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="width: 150px">Password
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtWardNursePass" runat="server" CssClass="label" TextMode="Password" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Date & Time
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel2" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtWardNurseDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                                        Enabled="True" TargetControlID="txtWardNurseDate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                    <asp:DropDownList ID="drpSigWardHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                    <asp:DropDownList ID="drpSigWardMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>

                                </table>
                            </fieldset>
                        </td>
                        <td>
                            <fieldset>
                                <legend class="lblCaption1">OT Nurse</legend>
                                <table width="100%">
                                    <tr>

                                        <td class="lblCaption1" style="width: 150px">Name  
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpOTNurse" runat="server" CssClass="label" Width="205px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1" style="width: 150px">Password
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtOTNurse" runat="server" CssClass="label" TextMode="Password" Width="200px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Date & Time
                                        </td>
                                        <td>
                                            <asp:UpdatePanel ID="updatePanel5" runat="server">
                                                <ContentTemplate>
                                                    <asp:TextBox ID="txtOTNurseDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                                    <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                                        Enabled="True" TargetControlID="txtOTNurseDate" Format="dd/MM/yyyy">
                                                    </asp:CalendarExtender>
                                                     <asp:DropDownList ID="drpSigOTHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                                    <asp:DropDownList ID="drpSigOTMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>


                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>

                                </table>


                            </fieldset>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>

    <br />
    <br />
</asp:Content>
