﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="RecoveryChart.aspx.cs" Inherits="EMR_IP.Patient.RecoveryChart" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <script src="../Validation.js" type="text/javascript"></script>

    <style type="text/css">
        .box-header
        {
            border-bottom: 4px solid #f4fbfd;
            height: 70px;
        }

        .box-title
        {
            padding-bottom: 5px;
            border-bottom: 4px solid #92c500;
            float: left;
            font-size: 1.2em;
            color: #2078c0;
        }

        h1, h2, h3, h4, h5, h6
        {
            font-size: 100%;
            font-weight: normal;
            font-family: "Segoe UI", Arial, Helvetica, sans-serif;
        }
    </style>
    <style>
        .AutoExtender
        {
            font-family: Verdana, Helvetica, sans-serif;
            font-size: .8em;
            font-weight: normal;
            border: solid 1px #006699;
            line-height: 20px;
            padding: 10px;
            background-color: White;
            margin-left: 10px;
        }

        .AutoExtenderList
        {
            border-bottom: dotted 1px #006699;
            cursor: pointer;
            color: Maroon;
        }

        .AutoExtenderHighlight
        {
            color: White;
            background-color: #006699;
            cursor: pointer;
        }

        #divwidth
        {
            width: 400px !important;
        }

            #divwidth div
            {
                width: 400px !important;
            }
    </style>


    <script type="text/javascript">
        function ShowMessage() {
            $("#myMessage").show();
            setTimeout(function () {
                var selectedEffect = 'blind';
                var options = {};
                $("#myMessage").hide();
            }, 2000);
            return true;
        }

        function ServNameSelected() {
            if (document.getElementById('<%=txtServName.ClientID%>').value != "") {
                var Data = document.getElementById('<%=txtServName.ClientID%>').value;
                var Data1 = Data.split('~');

                if (Data1.length > 1) {
                    document.getElementById('<%=txtServCode.ClientID%>').value = Data1[0];
                    document.getElementById('<%=txtServName.ClientID%>').value = Data1[1];
                }
            }

            return true;
        }
    </script>

    <script language="javascript" type="text/javascript">

        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }


        function FtoC() {
            var temp = document.getElementById("<%=txtTemperatureF.ClientID%>").value;
            var tempInF = Math.round(((100 / (212 - 32)) * (temp - 32)) * 100) / 100;
            document.getElementById("<%=txtTemperatureC.ClientID%>").value = tempInF;
            if (temp == 0 || temp == "") {
                document.getElementById("<%=txtTemperatureC.ClientID%>").value = '';
            }
        }
        function CtoF() {
            var celc = document.getElementById("<%=txtTemperatureC.ClientID%>").value;
            var tempInC = Math.round((((212 - 32) / 100) * celc + 32) * 100) / 100;
            document.getElementById("<%=txtTemperatureF.ClientID%>").value = tempInC;
            if (celc == 0 || celc == "") {
                document.getElementById("<%=txtTemperatureF.ClientID%>").value = '';
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <input type="hidden" id="hidPermission" runat="server" value="9" />

    <div style="padding-left: 60%; width: 100%;">
        <div id="myMessage" style="display: none; border: groove; height: 30px; width: 200px; background-color: gray; color: #ffffff; font-family: arial,helvetica,clean,sans-serif; font-size: small; border: 1px solid #fff; padding: 20px; border-radius: 10px; box-shadow: 1px 11px 11px 1px rgba(55,55,55,.5); position: absolute;">
            Saved Successfully 
                
        </div>
    </div>

     <table width="100%">
        <tr>
            <td style="text-align: left; width: 50%;">
                <h3 class="box-title">Recovery Chart</h3>
            </td>
            <td style="text-align: right; width: 50%;">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" CssClass="button orange small" Text="Saved Record" OnClick="btnSave_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>

    <asp:TabContainer ID="TabContainer1" runat="server" ActiveTabIndex="0" CssClass="ajax__tab_yuitabview-theme" Width="100%">
        <asp:TabPanel runat="server" ID="TabPanelGeneral" HeaderText="Post Anesthesia Record" Width="100%">
            <ContentTemplate>
                <table width="100%" style="padding-left:10px;">
                    <tr>
                        <td class="lblCaption1">Received from
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel13" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtReceivedFrom" runat="server" CssClass="lblCaption1" Width="150px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Accompanied by
                        </td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel14" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtAccompaniedBy" runat="server" CssClass="lblCaption1" Width="150px"></asp:TextBox>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>

                        <td class="lblCaption1">Date</td>
                        <td>
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtArrivalDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:CalendarExtender ID="Calendarextender2" runat="server"
                                        Enabled="True" TargetControlID="txtArrivalDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                    <asp:DropDownList ID="drpArrivalHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                    <asp:DropDownList ID="drpArrivalMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>


                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </td>
                    </tr>

                </table>
                <fieldset style="width: 95%;">
                    <legend class="lblCaption1">Procedure Performed</legend>
                    <table cellpadding="5" cellspacing="5" width="100%">
                        <tr>
                            <td class="lblCaption1">Anesthesia Type 
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpAnesthesiaType" runat="server" CssClass="label" Width="150px"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">General Condition 
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpPTCondition" runat="server" CssClass="label" Width="150px"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>


                        </tr>
                        <tr>
                            <td class="lblCaption1">Ventilation/ O2 Therapy

                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpVentilation" runat="server" CssClass="label" Width="150px"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Protective Airway Reflexes (Swallow, Gag, Cough) 

                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpProtective" runat="server" CssClass="label" Width="150px">
                                            <asp:ListItem Text="--- Select ---" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Intact" Value="Intact"></asp:ListItem>
                                            <asp:ListItem Text="Weak" Value="Weak"></asp:ListItem>
                                            <asp:ListItem Text="Absent" Value="Absent"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                        <tr>
                            <td class="lblCaption1">Airway Obstruction Signs 

                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpAirwayObstSigns" runat="server" CssClass="label" Width="150px">
                                            <asp:ListItem Text="--- Select ---" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Stridor" Value="Stridor"></asp:ListItem>
                                            <asp:ListItem Text="Retraction" Value="Retraction"></asp:ListItem>

                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Artificial Airway Device 

                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel8" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpArtiAirwayDevice" runat="server" CssClass="label" Width="150px">
                                            <asp:ListItem Text="--- Select ---" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Oral Airway" Value="Oral Airway"></asp:ListItem>
                                            <asp:ListItem Text="Nasal Airway" Value="Nasal Airway"></asp:ListItem>
                                            <asp:ListItem Text="ETT" Value="ETT"></asp:ListItem>
                                            <asp:ListItem Text="LMA" Value="LMA"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                        <tr>
                            <td class="lblCaption1">Vascular Cases 

                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpVascularCases" runat="server" CssClass="label" Width="150px"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Drainages 

                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel10" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpDrainage" runat="server" CssClass="label" Width="150px"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>


                        </tr>
                        <tr>
                            <td class="lblCaption1">Special Events 

                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpPTSpecialEvents" runat="server" CssClass="label" Width="150px"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">RR Procedures 

                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="UpdatePanel12" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpRRProcedures" runat="server" CssClass="label" Width="150px">
                                            <asp:ListItem Text="--- Select ---" Selected="True"></asp:ListItem>
                                            <asp:ListItem Text="Dressing Change" Value="Dressing Change"></asp:ListItem>
                                            <asp:ListItem Text="ETT Suction" Value="ETT Suction"></asp:ListItem>
                                            <asp:ListItem Text="Re-Positioning" Value="Re-Positioning"></asp:ListItem>
                                            <asp:ListItem Text="Pain Therapy" Value="Pain Therapy"></asp:ListItem>
                                            <asp:ListItem Text="Drain Evacuation" Value="Drain Evacuation"></asp:ListItem>
                                            <asp:ListItem Text="Warming" Value="Warming"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </fieldset>

            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel1" HeaderText="Monitoring" Width="100%">
            <ContentTemplate>
                <fieldset style="width: 95%;">
                    <legend class="lblCaption1">Monitoring in Recovery Room</legend>
                    <table cellpadding="5" cellspacing="5" width="100%">
                        <tr>
                            <td class="lblCaption1">Time 
                    
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel15" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtMonitoringDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender3" runat="server"
                                            Enabled="True" TargetControlID="txtMonitoringDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                          <asp:DropDownList ID="drpMoniHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                          <asp:DropDownList ID="drpMoniMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>


                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Respiration</td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="updatePanel16" runat="server">
                                    <ContentTemplate>
                                        <input type="text" name="txtRespiration" id="txtRespiration" runat="server" class="small numeric" onkeypress="return OnlyNumeric(event);" style="width: 70px;" />
                                        /m
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>


                        </tr>
                        <tr>
                            <td class="lblCaption1">Pulse 
                    
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="updatePanel17" runat="server">
                                    <ContentTemplate>
                                        <input id="txtPulse" runat="server" class="small numeric" style="width: 70px;" />&nbsp;Beats/Min
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>

                            <td class="lblCaption1">
                                <label for="BpSystolic">BP:Systolic</label></td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="updatePanel18" runat="server">
                                    <ContentTemplate>
                                        <input type="text" name="txtBpSystolic" id="txtBpSystolic" runat="server" class="small numeric" onkeypress="return OnlyNumeric(event);" style="width: 70px;" />
                                        mm/hg
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>


                        </tr>
                        <tr>
                            <td class="lblCaption1">
                                <label for="txtTemperatureF">Temperature</label></td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="updatePanel19" runat="server">
                                    <ContentTemplate>
                                        <input id="txtTemperatureF" runat="server" class="small numeric" style="width: 70px;" onkeypress="return OnlyNumeric(event);" onkeyup="FtoC();" />&nbsp;F 
                       <input id="txtTemperatureC" runat="server" class="small numeric" style="width: 70px;" onkeypress="return OnlyNumeric(event);" onkeyup="CtoF();" />&deg;C
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">
                                <label for="BpDiastolic">BP:Diastolic</label></td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="updatePanel20" runat="server">
                                    <ContentTemplate>
                                        <input type="text" name="txtBpDiastolic" id="txtBpDiastolic" runat="server" class="small numeric" onkeypress="return OnlyNumeric(event);" style="width: 70px;" />
                                        mm/hg
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>

                        <tr>
                            <td class="lblCaption1">SP02</td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="updatePanel21" runat="server">
                                    <ContentTemplate>
                                        <input type="text" name="txtSp02" id="txtSp02" runat="server" style="width: 70px;" class="small" onkeypress="return OnlyNumeric(event);" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Motor Block/ Sensory Level</td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="updatePanel22" runat="server">
                                    <ContentTemplate>
                                        <input type="text" name="txtMotorBlock" id="txtMotorBlock" runat="server" style="width: 70px;" class="small" onkeypress="return OnlyNumeric(event);" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>

                        <tr>
                            <td class="lblCaption1">Pain Score</td>
                            <td>
                                <input type="text" name="txtPainScore" id="txtPainScore" runat="server" style="width: 70px;" class="small" onkeypress="return OnlyNumeric(event);" />
                            </td>
                            <td class="lblCaption1">Comment</td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel ID="updatePanel23" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtMonitoringComment" runat="server" CssClass="label" Height="50px" Width="95%" TextMode="MultiLine"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:UpdatePanel ID="updatePanel1MonAdd" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnMonitorAdd" runat="server" CssClass="button orange small" Text="Add" OnClick="btnMonitorAdd_Click" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                    </table>
                    <asp:UpdatePanel ID="updatePanel24" runat="server">
                        <ContentTemplate>

                            <asp:GridView ID="gvMonitoring" runat="server" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="100%" GridLines="none">
                                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Date & Tome">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCode" runat="server" OnClick="Monitoring_Click">
                                                <asp:Label ID="lblMonID" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_MON_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblDate" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_TIMEDateDesc") %>'></asp:Label>
                                                <asp:Label ID="lblTime" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_TIMETimeDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BP:Systolic">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnksys" runat="server" OnClick="Monitoring_Click">
                                                <asp:Label ID="Label1" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_BP_SYSTOLIC") %>'></asp:Label>
                                                &nbsp;  mm/Hg
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="BP:DiaStolic">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkdis" runat="server" OnClick="Monitoring_Click">
                                                <asp:Label ID="Label2" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_BP_DIASTOLIC") %>'></asp:Label>
                                                &nbsp;  mm/Hg 
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Pulse">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPul" runat="server" OnClick="Monitoring_Click">
                                                <asp:Label ID="lblgvPulse" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_PULSE") %>'></asp:Label>
                                                &nbsp;Beats/Min
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SPO2">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkspo" runat="server" OnClick="Monitoring_Click">
                                                <asp:Label ID="lblgvSpo2" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_SPO2") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Respiration">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkRes" runat="server" OnClick="Monitoring_Click">
                                                <asp:Label ID="lblgvRespiration" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_RESPIRATION") %>'></asp:Label>
                                                &nbsp; /m
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Temprature">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkTemp" runat="server" OnClick="Monitoring_Click">
                                                <asp:Label ID="lblgvTemperatureC" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_TEMP_C") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblgvTemperatureF" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_TEMP") %>'></asp:Label>
                                                &nbsp; F
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>




                                    <asp:TemplateField HeaderText="Pain Score">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPain" runat="server" OnClick="Monitoring_Click">
                                                <asp:Label ID="lblgvPainScore" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_PAIN_SCORE") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Motor Block">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkMot" runat="server" OnClick="Monitoring_Click">
                                                <asp:Label ID="lblgvMotorBlock" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_MOTOR_BLOCK") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Comment">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCome" runat="server" OnClick="Monitoring_Click">
                                                <asp:Label ID="lblgvComment" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_COMMENT") %>'></asp:Label>
                                            </asp:LinkButton>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>


                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </fieldset>


            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel4" HeaderText="Medication" Width="100%">
            <ContentTemplate>
                <fieldset style="width: 95%;">
                    <legend class="lblCaption1">Medication</legend>
                    <table cellpadding="5" cellspacing="5" width="90%">
                        <tr>
                            <td class="lblCaption1">Solution
                    
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel26">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtSolution" runat="server" CssClass="label" Width="505px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Volume
                    
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel27">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtVolume" runat="server" CssClass="label" Width="70px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Started
                    
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel runat="server" ID="updatePanel28">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtMedicationStarted" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender4" runat="server"
                                            Enabled="True" TargetControlID="txtMedicationStarted" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                           <asp:DropDownList ID="drpMedStHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                          <asp:DropDownList ID="drpMedStMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                        Finished
                                 <asp:TextBox ID="txtMedicationSFinished" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender5" runat="server"
                                            Enabled="True" TargetControlID="txtMedicationSFinished" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                           <asp:DropDownList ID="drpMedEndHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                          <asp:DropDownList ID="drpMedEndMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Time Given
                            </td>
                            <td class="lblCaption1">
                                <asp:UpdatePanel runat="server" ID="updatePanel29">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtMedicationSGivenTimeDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender6" runat="server"
                                            Enabled="True" TargetControlID="txtMedicationSGivenTimeDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                          <asp:DropDownList ID="drpMedGivenHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                          <asp:DropDownList ID="drpMedGivenMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>


                                    </ContentTemplate>
                                </asp:UpdatePanel>


                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Drug Name
                    
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanelDrug">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtServCode" runat="server" Width="100px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" Enabled="false"></asp:TextBox>
                                        <asp:TextBox ID="txtServName" runat="server" Width="400px" CssClass="label" BorderWidth="1px" BorderColor="#cccccc" onblur="return ServNameSelected()"></asp:TextBox>

                                        <div id="divwidth" style="visibility: hidden;"></div>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="Server" TargetControlID="txtServCode" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                        </asp:AutoCompleteExtender>
                                        <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="Server" TargetControlID="txtServName" MinimumPrefixLength="1" ServiceMethod="GetServicessList"
                                            CompletionListCssClass="AutoExtender" CompletionListItemCssClass="AutoExtenderList" CompletionInterval="10" CompletionListHighlightedItemCssClass="AutoExtenderHighlight" CompletionListElementID="divwidth">
                                        </asp:AutoCompleteExtender>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                            <td class="lblCaption1">Given By
                    
                            </td>

                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanel30">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpGivenBy" runat="server" CssClass="label" Width="155px" BorderColor="#cccccc"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Dosage
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanelDosage">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtDossage1" runat="server" CssClass="label" Width="260px" TextMode="MultiLine" Height="30px"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                            </td>
                            <td class="lblCaption1">Route
                            </td>
                            <td>
                                <asp:UpdatePanel runat="server" ID="updatePanelRoute">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpRoute" CssClass="label" runat="server" Width="155px" BorderColor="#cccccc">
                                        </asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                        <tr>
                            <td class="lblCaption1">Intake
                            </td>
                            <td colspan="3" class="lblCaption1">
                                <asp:TextBox ID="txtIntake" runat="server" Width="100px" CssClass="label"></asp:TextBox>&nbsp;
                                IV Fluids &nbsp;
                                <asp:TextBox ID="txtIVFluids" runat="server" Width="100px" CssClass="label"></asp:TextBox>&nbsp;
                                Blood Products &nbsp;
                                  <asp:TextBox ID="txtBloodProducts" runat="server" Width="100px" CssClass="label"></asp:TextBox>&nbsp;
                                Other&nbsp;
                                 <asp:TextBox ID="txtOther" runat="server" Width="100px" CssClass="label"></asp:TextBox>&nbsp;
                                Total&nbsp;
                                <asp:TextBox ID="txtTotal" runat="server" Width="100px" CssClass="label"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:UpdatePanel ID="updatePanelMedAdd" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnMedicationAdd" runat="server" CssClass="button orange small" Text="Add" OnClick="btnMedicationAdd_Click" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                    </table>
                    <asp:UpdatePanel ID="updatePanel25" runat="server">
                        <ContentTemplate>

                            <asp:GridView ID="gvMedication" runat="server" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="100%" GridLines="none">
                                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Solution">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkCode" runat="server" OnClick="Medication_Click">
                                                <asp:Label ID="lblMedID" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_MED_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblSolu" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_SOLUTION") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Volume">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnksys" runat="server" OnClick="Medication_Click">
                                                <asp:Label ID="Label1" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_VOLUME") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Started">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkdis" runat="server" OnClick="Medication_Click">
                                                <asp:Label ID="lblgvStarted" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_STARTEDDateDesc") %>'></asp:Label>
                                                <asp:Label ID="lblgvStartedTime" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_STARTEDTimeDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Finished">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPul" runat="server" OnClick="Medication_Click">
                                                <asp:Label ID="lblgvFinished" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_FINISHEDDateDesc") %>'></asp:Label>
                                                <asp:Label ID="lblgvFinishedTime" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_FINISHEDTimeDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Drug">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkspo" runat="server" OnClick="Medication_Click">
                                                <asp:Label ID="lblgvDrugCode" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_DRUG_CODE") %>'></asp:Label>
                                                <asp:Label ID="lblgvDrugName" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_DRUG_NAME") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Dose">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkRes" runat="server" OnClick="Medication_Click">
                                                <asp:Label ID="lblgvDose" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_DOSE") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Route">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkTemp" runat="server" OnClick="Medication_Click">
                                                <asp:Label ID="lblgvRoute" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_ROUTE") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>




                                    <asp:TemplateField HeaderText="Given By">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkPain" runat="server" OnClick="Medication_Click">
                                                <asp:Label ID="lblgvGivenBy" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_GIVEN_BY") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Time Given">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkMot" runat="server" OnClick="Medication_Click">
                                                <asp:Label ID="lblgvGiven" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_TIME_GIVENDateDesc") %>'></asp:Label>
                                                <asp:Label ID="lblgvGivenTime" CssClass="GridRow" runat="server" Text='<%# Bind("IRRM_TIME_GIVENTimeDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>


                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </fieldset>


            </ContentTemplate>
        </asp:TabPanel>

        <asp:TabPanel runat="server" ID="TabPanel2" HeaderText="Nurse Notes" Width="100%">
            <ContentTemplate>

                <fieldset>
                    <legend class="lblCaption1">Recovery Nurse Notes</legend>
                    <table cellpadding="5" cellspacing="5" width="100%">
                        <tr>
                            <td class="lblCaption1" style="width: 100px">Date & Time 
                    
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel31" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtNurseNotesDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                        <asp:CalendarExtender ID="Calendarextender1" runat="server"
                                            Enabled="True" TargetControlID="txtNurseNotesDate" Format="dd/MM/yyyy">
                                        </asp:CalendarExtender>
                                           <asp:DropDownList ID="drpNurNoteHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                          <asp:DropDownList ID="drpNurNoteMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td class="lblCaption1">Comment</td>
                            <td class="lblCaption1" colspan="3">
                                <asp:UpdatePanel ID="updatePanel32" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox ID="txtNurseNotesRemarks" runat="server" CssClass="label" Height="50px" Width="95%" TextMode="MultiLine"></asp:TextBox>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:UpdatePanel ID="updatePanel33" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnNurseNotesAdd" runat="server" CssClass="button orange small" Text="Add" OnClick="btnNurseNotesAdd_Click" />
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                    </table>

                    <asp:UpdatePanel ID="updatePanel34" runat="server">
                        <ContentTemplate>

                            <asp:GridView ID="gvNurseNotes" runat="server" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="100%" GridLines="none">
                                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText=" Date & Time" HeaderStyle-Width="100px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkgvNurseNoteseTime" runat="server" OnClick="NurseNotes_Click">
                                                <asp:Label ID="lblNurseID" CssClass="GridRow" runat="server" Text='<%# Bind("IRNN_NOTES_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblgvNurseNotesDate" CssClass="GridRow" runat="server" Text='<%# Bind("IRNN_TIMEDateDesc") %>'></asp:Label>
                                                <asp:Label ID="lblgvNurseNotesTime" CssClass="GridRow" runat="server" Text='<%# Bind("IRNN_TIMETimeDesc") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remarks">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnksys" runat="server" OnClick="NurseNotes_Click">
                                                <asp:Label ID="gvNurseNotesRemarks" CssClass="GridRow" runat="server" Text='<%# Bind("IRNN_REMARKS") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>




                                </Columns>


                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </fieldset>

            </ContentTemplate>
        </asp:TabPanel>
        <asp:TabPanel runat="server" ID="TabPanel3" HeaderText="Discharge Criteria" Width="100%">
            <ContentTemplate>

                <fieldset>
                    <legend class="lblCaption1">Criteria for Discharge from Recovery Room (Modified Aldrete Score)</legend>
                    <table cellpadding="5" cellspacing="5" width="100%">
                        <tr>
                            <td class="lblCaption1">Issue Type
                    
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel35" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpDisIssueType" runat="server" CssClass="label" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="drpDisIssueType_SelectedIndexChanged"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Issue Sub Type
                    
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel36" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpDisIssueSubType" runat="server" CssClass="label" Width="200px"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Assessment Time
                    
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel37" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpDisAssTime" runat="server" CssClass="label" Width="150px"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:UpdatePanel ID="updatePanel41" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnDischargeAdd" runat="server" CssClass="button orange small" Text="Add" OnClick="btnDischargeAdd_Click" />
                                        <asp:Button ID="btnDischargeClear" runat="server" CssClass="button orange small" Text="Clear" OnClick="btnDischargeClear_Click" />

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                    </table>
                    <asp:UpdatePanel ID="updatePanel42" runat="server">
                        <ContentTemplate>

                            <asp:GridView ID="gvDischargeRoom" runat="server" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="100%" GridLines="none">
                                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText=" Issue Type" HeaderStyle-Width="150px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkgvIssueType" runat="server" OnClick="DischargeRoom_Click">
                                                <asp:Label ID="lblStatusID" CssClass="GridRow" runat="server" Text='<%# Bind("IRRS_STATUS_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblgvIssueType" CssClass="GridRow" runat="server" Text='<%# Bind("IRRS_ISSUE_TYPE") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Issue Sub Type">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkgvIssueSubType" runat="server" OnClick="DischargeRoom_Click">
                                                <asp:Label ID="lblgvIssueSubType" CssClass="GridRow" runat="server" Text='<%# Bind("IRRS_ISSUE_SUB_TYPE") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Assessment Time">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkgvAssessmentTime" runat="server" OnClick="DischargeRoom_Click">
                                                <asp:Label ID="lblgvAssessmentTime" CssClass="GridRow" runat="server" Text='<%# Bind("IRRS_ISSUE_TIMING") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Score">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkgvScore" runat="server" OnClick="DischargeRoom_Click">
                                                <asp:Label ID="lblgvScore" CssClass="GridRow" runat="server" Text='<%# Bind("IRRS_SCORE") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>


                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </fieldset>
                <fieldset>
                    <legend class="lblCaption1">Additional Criteria for Home Discharge</legend>
                    <table cellpadding="5" cellspacing="5" width="100%">
                        <tr>
                            <td class="lblCaption1">Issue Type
                    
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel38" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpHmDisIssueType" runat="server" CssClass="label" Width="150px" AutoPostBack="true" OnSelectedIndexChanged="drpHmDisIssueType_SelectedIndexChanged"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Issue Sub Type
                    
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel39" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpHmDisIssueSubType" runat="server" CssClass="label" Width="200px"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                            <td class="lblCaption1">Assessment Time
                    
                            </td>
                            <td>
                                <asp:UpdatePanel ID="updatePanel40" runat="server">
                                    <ContentTemplate>
                                        <asp:DropDownList ID="drpHmDisAssTime" runat="server" CssClass="label" Width="150px"></asp:DropDownList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:UpdatePanel ID="updatePanel43" runat="server">
                                    <ContentTemplate>
                                        <asp:Button ID="btnHMDischargeAdd" runat="server" CssClass="button orange small" Text="Add" OnClick="btnHMDischargeAdd_Click" />
                                        <asp:Button ID="btnHMDischargeClear" runat="server" CssClass="button orange small" Text="Clear" OnClick="btnHMDischargeClear_Click" />

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>

                        </tr>
                    </table>
                    <asp:UpdatePanel ID="updatePanel44" runat="server">
                        <ContentTemplate>

                            <asp:GridView ID="gvDischargeHome" runat="server" AutoGenerateColumns="False"
                                EnableModelValidation="True" Width="100%" GridLines="none">
                                <HeaderStyle CssClass="GridHeader_Blue" Font-Bold="true" />
                                <RowStyle CssClass="GridRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText=" Issue Type" HeaderStyle-Width="150px">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkgvHmIssueType" runat="server" OnClick="DischargeHome_Click">
                                                <asp:Label ID="lblHmStatusID" CssClass="GridRow" runat="server" Text='<%# Bind("IRRS_STATUS_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblgvHmIssueType" CssClass="GridRow" runat="server" Text='<%# Bind("IRRS_ISSUE_TYPE") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Issue Sub Type">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkgvHmIssueSubType" runat="server" OnClick="DischargeHome_Click">
                                                <asp:Label ID="lblgvHmIssueSubType" CssClass="GridRow" runat="server" Text='<%# Bind("IRRS_ISSUE_SUB_TYPE") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Assessment Time">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkgvHmAssessmentTime" runat="server" OnClick="DischargeHome_Click">
                                                <asp:Label ID="lblgvHmAssessmentTime" CssClass="GridRow" runat="server" Text='<%# Bind("IRRS_ISSUE_TIMING") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Score">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkgvHmScore" runat="server" OnClick="DischargeHome_Click">
                                                <asp:Label ID="lblgvHmScore" CssClass="GridRow" runat="server" Text='<%# Bind("IRRS_SCORE") %>'></asp:Label>

                                            </asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                </Columns>


                            </asp:GridView>

                        </ContentTemplate>
                    </asp:UpdatePanel>
                </fieldset>

            </ContentTemplate>
        </asp:TabPanel>
         <asp:TabPanel runat="server" ID="TabPanel5" HeaderText="Home Discharge" Width="100%">
            <ContentTemplate>
                 <table cellpadding="=5" cellspacing="5" style="width: 100%">
                    <tr>
                        <td style="width: 50%" valign="top">
                            <fieldset>
                                <legend class="lblCaption1" style="font-weight: bold;">Pain Score</legend>
                                <table style="width: 50%">

                                    <tr>
                                        <td>
                                            <label for="PainValue0" class="lblCaption1">No Pain</label></td>
                                        <td class="lblCaption1">
                                            <input type="radio" name="PainValue" id="PainValue0" value="0" class="lblCaption1" runat="server" />
                                            0<br />
                                            <input type="radio" name="PainValue" id="PainValue1" value="1" class="lblCaption1" runat="server" />
                                            1
                                        </td>
                                        <td>
                                            <img src="../Images/smilie-1.png" alt="Pain Score" class="pain-smilie" width="60px" height="55px"  /></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="PainValue2" class="lblCaption1">Mild Pain</label></td>
                                        <td class="lblCaption1">
                                            <input type="radio" name="PainValue" id="PainValue2" value="2" class="lblCaption1" runat="server" />
                                            2<br />
                                            <input type="radio" name="PainValue" id="PainValue3" value="3" class="lblCaption1" runat="server" />
                                            3
                                        </td>
                                        <td>
                                            <img src="../images/smilie-2.png" alt="Pain Score" class="pain-smilie" width="60px" height="55px" /></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="PainValue4" class="lblCaption1">Moderate   </label>
                                        </td>
                                        <td class="lblCaption1">
                                            <input type="radio" name="PainValue" id="PainValue4" value="4" class="lblCaption1" runat="server" />
                                            4<br />
                                            <input type="radio" name="PainValue" id="PainValue5" value="5" class="lblCaption1" runat="server" />
                                            5
                                        </td>
                                        <td>
                                            <img src="../images/smilie-3.png" alt="Pain Score" class="pain-smilie" width="60px" height="55px" /></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="PainValue6" class="lblCaption1">Quite a Lot </label>
                                        </td>
                                        <td class="lblCaption1">
                                            <input type="radio" name="PainValue" id="PainValue6" value="6" class="lblCaption1" runat="server" />
                                            6<br />
                                            <input type="radio" name="PainValue" id="PainValue7" value="7" class="lblCaption1" runat="server" />
                                            7
                                        </td>
                                        <td>
                                            <img src="../images/smilie-4.png" alt="Pain Score" class="pain-smilie" width="60px" height="55px" /></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="PainValue8" class="lblCaption1">Very Bad   </label>
                                        </td>
                                        <td class="lblCaption1">
                                            <input type="radio" name="PainValue" id="PainValue8" value="8" class="lblCaption1" runat="server" />
                                            8<br />
                                            <input type="radio" name="PainValue" id="PainValue9" value="9" class="lblCaption1" runat="server" />
                                            9
                                        </td>
                                        <td>
                                            <img src="../images/smilie-5.png" alt="Pain Score" class="pain-smilie"  width="60px" height="55px"/></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="PainValue10" class="lblCaption1">Worst Pain</label></td>
                                        <td class="lblCaption1">
                                            <input type="radio" name="PainValue" id="PainValue10" value="10" class="lblCaption1" runat="server" />
                                            10<br />
                                        </td>
                                        <td>
                                            <img src="../images/smilie-6.png" alt="Pain Score" class="pain-smilie" width="60px" height="55px" /></td>
                                    </tr>

                                </table>
                            </fieldset>

                        </td>
                        <td style="width: 50%" valign="top">
                            <fieldset>
                                <legend class="lblCaption1" style="font-weight: bold;">The Pt. is ready for home discharge when:</legend>
                                <table cellpadding="=5" cellspacing="5" style="width: 100%">
                                    <tr>
                                        <td class="lblCaption1">Motor Block Intensity
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpMotorBlockIntensity" runat="server" CssClass="label" Width="300px">
                                                <asp:ListItem Text="--- Select ---" Value="" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Free movement of the legs and feet" Value="Free movement of the legs and feet"></asp:ListItem>
                                                <asp:ListItem Text="Just able to flex knees with free movement of the feet" Value="Just able to flex knees with free movement of the feet"></asp:ListItem>
                                                <asp:ListItem Text="Unable to flex knees, but freemovement of legs and feet" Value="Unable to flex knees, but freemovement of legs and feet"> </asp:ListItem>
                                                <asp:ListItem  Text="Unable to move legs and feet" Value="Unable to move legs and feet"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Sedation and Agitation Score
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpSedationAgitationScore" runat="server" CssClass="label" Width="300px">
                                                 <asp:ListItem Text="--- Select ---" Value="" Selected="True"></asp:ListItem>
                                                 <asp:ListItem Text="Dangerous Agitation" Value="Dangerous Agitation"></asp:ListItem>
                                                <asp:ListItem Text="Very Agitated" Value="Very Agitated"></asp:ListItem>
                                                <asp:ListItem Text="Agitated" Value="Agitated"></asp:ListItem>
                                                <asp:ListItem Text="Calm & Cooperative" Value="Calm & Cooperative"></asp:ListItem>
                                                <asp:ListItem Text="Sedated" Value="Sedated"></asp:ListItem>
                                                <asp:ListItem Text="Very Sedated" Value="Very Sedated"></asp:ListItem>
                                                <asp:ListItem Text="Unarousable" Value="Unarousable"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Patient Teaching
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpPatientTeaching" runat="server" CssClass="label" Width="300px">
                                                  <asp:ListItem Text="--- Select ---" Value="" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="Pre-printed self care instructions provided to the patient" Value="Pre-printed self care instructions provided to the patient"></asp:ListItem>
                                                <asp:ListItem Text="Patient/ Family are able to verbalize and understand the instructions" Value="Patient/ Family are able to verbalize and understand the instructions"></asp:ListItem>
                                                <asp:ListItem Text="Other topics discussed" Value="Other topics discussed"></asp:ListItem>
                                                <asp:ListItem Text="Competent escort is available" Value="Competent escort is available"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="lblCaption1">Discharged With
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpDischargedWith" runat="server" CssClass="label" Width="300px">
                                                 <asp:ListItem Text="--- Select ---" Value="" Selected="True"></asp:ListItem>
                                                 <asp:ListItem Text="Personal belongings" Value="Personal belongings" ></asp:ListItem>
                                                 <asp:ListItem Text="Medications" Value="Medications" ></asp:ListItem>
                                                 <asp:ListItem Text="Prescriptions" Value="Prescriptions" ></asp:ListItem>
                                                 <asp:ListItem Text="Equipment or material" Value="Equipment or material" ></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td class="lblCaption1">Discharged to
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="drpDischargedTo" runat="server" CssClass="label" Width="300px">
                                                 <asp:ListItem Text="--- Select ---" Value="" Selected="True"></asp:ListItem>
                                                 <asp:ListItem Text="Ward" Value="Ward" ></asp:ListItem>
                                                 <asp:ListItem Text="ICU/ NICU" Value="ICU/ NICU" ></asp:ListItem>
                                                 <asp:ListItem Text="Another Hospital" Value="Another Hospital" ></asp:ListItem>
                                                 <asp:ListItem Text="Home" Value="Home" ></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>

                                    </tr>

                                </table>
                            </fieldset>
                            <fieldset>
                                <legend class="lblCaption1" style="font-weight: bold;">The Pt. is ready for home discharge when:</legend>
                                <table width="100%">
                                    <tr>
                                        <td class="lblCaption1">Comments and Post-op Instructions<br />
                                            <asp:TextBox ID="txtPostOpInstComment" runat="server" CssClass="label" Height="140px" Width="100%" TextMode="MultiLine"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
                 <table width="100%" style="padding-left:10px;">
                    <tr>
                        <td class="lblCaption1">RR Nurse
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel45">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpRRNurse" runat="server" CssClass="label" Width="155px" BorderColor="#cccccc"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Anesthesiologist
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel46">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpAnesthesiologist" runat="server" CssClass="label" Width="155px" BorderColor="#cccccc"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                        <td class="lblCaption1">Ward Nurse
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel47">
                                <ContentTemplate>
                                    <asp:DropDownList ID="drpWardNurse" runat="server" CssClass="label" Width="155px" BorderColor="#cccccc"></asp:DropDownList>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td class="lblCaption1">Ward Call Time
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel48">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtWardCallDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
`                                         <asp:DropDownList ID="drpWardCallHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                          <asp:DropDownList ID="drpWardCallMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>



                                    <asp:CalendarExtender ID="Calendarextender7" runat="server"
                                        Enabled="True" TargetControlID="txtWardCallDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                         <td class="lblCaption1">Discharge Date & Time
                        </td>
                        <td>
                            <asp:UpdatePanel runat="server" ID="updatePanel49">
                                <ContentTemplate>
                                    <asp:TextBox ID="txtDischargeDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                                    <asp:DropDownList ID="drpDischargeHour" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>
                                     <asp:DropDownList ID="drpDischargeMin" Style="font-size: 11px;" CssClass="label" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="48px"></asp:DropDownList>


                                    <asp:CalendarExtender ID="Calendarextender8" runat="server"
                                        Enabled="True" TargetControlID="txtDischargeDate" Format="dd/MM/yyyy">
                                    </asp:CalendarExtender>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:TabPanel>
    </asp:TabContainer>

   

    <br />
    <br />
</asp:Content>
