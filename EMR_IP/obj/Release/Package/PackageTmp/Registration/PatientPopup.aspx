﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PatientPopup.aspx.cs" Inherits="EMR_IP.Registration.PatientPopup" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />
    <title>Lookup for patients</title>

    <script language="javascript" type="text/javascript">

        function ClosePatientList() {
            /*   opener.location.href = "PatientWaitingList.aspx";
              opener.location.reload();
              opener.focus();
              self.close();
              */
            opener.location.href = "PatientWaitingList.aspx"; //opener.location.href;
            window.parent.close();
            window.parent.parent.close();
            window.close();

        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
            </asp:ToolkitScriptManager>
            <table>
                <tr>
                    <td class="PageHeader">Lookup for patients
                    </td>
                </tr>
            </table>
            <div style="padding-top: 5px;">
                <asp:HiddenField ID="hidLocalDate" runat="server" />
                <asp:HiddenField ID="hidPageName" runat="server" />
                <table width="100%">
                    <tr>
                        <td class="style2">
                            <asp:Label ID="Label18" runat="server" CssClass="lblCaption1"
                                Text="Search"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSearch" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="100px"></asp:TextBox>&nbsp;
         <asp:Button ID="btnSearch" runat="server" CssClass="button orange small" OnClick="btnSearch_Click" Text=" Find " />
                        </td>

                    </tr>
                </table>
            </div>


            <div style="padding-top: 0px; width: 98%; height: 375px; overflow: auto; border: thin; border-color: #CCCCCC; border-style: groove;">
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:GridView ID="gvPTList" runat="server" AllowPaging="True"
                            AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvPTList_Sorting"
                            EnableModelValidation="True" Width="1050px" OnPageIndexChanging="gvPTList_PageIndexChanging" PageSize="100" GridLines="None">
                            <HeaderStyle CssClass="GridHeader_Blue" />
                            <RowStyle CssClass="GridRow" />
                            <Columns>
                                <asp:TemplateField HeaderText="File No" SortExpression="HPM_PT_ID">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEmailId" CssClass="label" runat="server" Text='<%# Bind("HPM_EMAIL") %>' Visible="false"></asp:Label>
                                        <asp:LinkButton ID="lnkServiceId" runat="server" OnClick="Select_Click">
                                            <asp:Label ID="lblPatientId" CssClass="label" runat="server" Text='<%# Bind("HPM_PT_ID") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" SortExpression="HPM_PT_FNAME">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkFullName" runat="server" OnClick="Select_Click">
                                            <asp:Label ID="lblPatientName" CssClass="label" runat="server" Text='<%# Bind("FullName") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DOB" SortExpression="HPM_DOBDesc">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDOB" runat="server" OnClick="Select_Click">
                                            <asp:Label ID="lbldbo" CssClass="label" runat="server" Text='<%# Bind("HPM_DOBDesc") %>'></asp:Label>
                                        </asp:LinkButton>

                                    </ItemTemplate>

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phone No" SortExpression="HPM_PHONE1">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkPhone" runat="server" OnClick="Select_Click">
                                            <asp:Label ID="lblPhone" CssClass="label" runat="server" Text='<%# Bind("HPM_PHONE1") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mobile No" SortExpression="HPM_MOBILE">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkMobile" runat="server" OnClick="Select_Click">
                                            <asp:Label ID="lblMobile" CssClass="label" runat="server" Text='<%# Bind("HPM_MOBILE") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="PT Type" SortExpression="HPM_PT_TYPEEDesc">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="inlPTType" runat="server" OnClick="Select_Click">
                                            <asp:Label ID="lblPTType" CssClass="label" runat="server" Text='<%# Bind("HPM_PT_TYPEEDesc") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Company" SortExpression="HPM_PT_TYPEEDesc">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkCompany" runat="server" OnClick="Select_Click">
                                            <asp:Label ID="lblCompName" CssClass="label" runat="server" Text='<%# Bind("HPM_INS_COMP_NAME") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Policy No." SortExpression="HPM_POLICY_NO">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkgvPolicyNo" runat="server" OnClick="Select_Click">
                                            <asp:Label ID="lblgvPolicyNo" CssClass="label" runat="server" Text='<%# Bind("HPM_POLICY_NO") %>'></asp:Label>
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                            </Columns>

                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>

            <div>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table width="98%">
                            <tr>
                                <td align="left">
                                    <asp:Label ID="Label9" runat="server" CssClass="label" Font-Bold="true"
                                        Text="Selected Records :"></asp:Label>
                                    &nbsp;
                                    <asp:Label ID="lblTotal" runat="server" CssClass="label" Font-Bold="true"></asp:Label>
                                </td>
                                <td align="right">
                                    <asp:Button ID="btnAddWaiting" runat="server" CssClass="button blue small" OnClick="btnAddWaiting_Click" Text=" Add To Waiting " />&nbsp;
                                   <input type="button" id="btnDone" class="button blue small" value=" Done " onclick="ClosePatientList()" />
                                </td>
                            </tr>

                        </table>

                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </div>
    </form>
</body>
</html>
