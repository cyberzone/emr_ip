﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="PatientWaitingList.aspx.cs" Inherits="EMR_IP.Registration.PatientWaitingList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">


        function OnlyNumeric(evt) {
            var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
            if (chCode >= 48 && chCode <= 57 ||
                 chCode == 46) {
                return true;
            }
            else

                return false;
        }

     
    </script>

    <script language="javascript" type="text/javascript">
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var hh = today.getHours();
        var min = today.getMinutes();
        var sec = today.getSeconds();

        var yyyy = today.getFullYear();
        if (dd < 10) { dd = '0' + dd } if (mm < 10) { mm = '0' + mm } var today = yyyy + '-' + mm + '-' + dd + ' ' + hh + ':' + min + ':' + sec;

        jQuery(document).ready(function () {
            document.getElementById('<%=hidLocalDate.ClientID%>').value = today;
            // alert(today);
        });
    </script>

 <script language="javascript" type="text/javascript">
     function ShowDrWiseIncome() {
         var Report = "";
         var dtFrom = document.getElementById('<%=txtFromDate.ClientID%>').value
         var arrFromDate = dtFrom.split('/');
         var Date1;
         if (arrFromDate.length > 1) {

             Date1 = arrFromDate[2] + "-" + arrFromDate[1] + "-" + arrFromDate[0];
         }


         var dtTo = document.getElementById('<%=txtToDate.ClientID%>').value
         var arrToDate = dtTo.split('/');
         var Date2;
         if (arrToDate.length > 1) {

             Date2 = arrToDate[2] + "-" + arrToDate[1] + "-" + arrToDate[0];
         }


         var DrId = '<%= Convert.ToString(Session["User_Code"])%>'

            Report = "HmsStaffwiseIncome.rpt";


            var Criteria = " 1=1 ";


            if (DrId != "") {

                Criteria += ' AND {HMS_STAFF_WISE_INCOME.HIM_DR_CODE}=\'' + DrId + '\'';
            }


            Criteria += ' AND  {HMS_STAFF_WISE_INCOME.HIM_DATE}>=date(\'' + Date1 + '\') AND  {HMS_STAFF_WISE_INCOME.HIM_DATE}<=date(\'' + Date2 + '\')'


            var win = window.open('../CReports/ReportViewer.aspx?ReportName=' + Report + '&SelectionFormula=' + Criteria, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')



        }

        function ShowAppointment() {


            var DrId = '<%= Convert.ToString(Session["User_Code"])%>'


         var win = window.open('AppointmentEMRDr.aspx?PageName=AppointmentEMRDr&DrID=' + DrId, '_new', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

     }

     function ShowReports() {

         var win = window.open('../MedicalReport/Index.aspx?PageName=WaitingLIst', 'MedicalReport', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

     }

     function ShowResults() {

         var win = window.open('../VisitDetails/ResultsViewPopup.aspx?PageName=WaitingLIst', 'VisitDetails', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

     }

     function ShowMasterAllocation() {

         var win = window.open('../Masters/MasterAllocation.aspx?PageName=WaitingLIst', 'MastersAllocation', 'menubar=no,left=100,top=80,height=850,width=1075,scrollbars=1')

     }

     function ShowPatientList() {


         var win = window.open('PatientPopup.aspx?PageName=WaitingLIst', 'PatientList', 'menubar=no,left=100,top=80,height=550,width=1075,scrollbars=1')

     }

     function ShowIPRegistration() {


         var win = window.open('IPRegistrationPopup.aspx?PageName=WaitingLIst', 'IPRegistration', 'menubar=no,left=100,top=80,height=700,width=1075,scrollbars=1')

     }

 </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="hidPermission" runat="server" value="9" />
    <asp:HiddenField ID="hidLocalDate" runat="server" />

  <input type="hidden" id="hidSrtProc" runat="server" value="false" />
     <table width="100%">
        <tr>
            <td align="left" >
                <asp:LinkButton ID="lnkEMR" runat="server" Text="Out Patient" CssClass="lblCaption1" Font-Bold="true" OnClick="lnkEMR_Click"></asp:LinkButton>

            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td class="lblCaption1" style="width:100px">
                Date
            </td>
            <td>
                <asp:TextBox ID="txtFromDate" runat="server" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="TextBox1_CalendarExtender" runat="server"
                    Enabled="True" TargetControlID="txtFromDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender1" runat="server" Enabled="true" TargetControlID="txtFromDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>

            </td>
            <td class="lblCaption1">
                 To Date

            </td>
            <td>
                <asp:TextBox ID="txtToDate" runat="server" Width="150px" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender1" runat="server"
                    Enabled="True" TargetControlID="txtToDate" Format="dd/MM/yyyy">
                </asp:CalendarExtender>
                <asp:MaskedEditExtender ID="MaskedEditExtender2" runat="server" Enabled="true" TargetControlID="txtToDate" Mask="99/99/9999" MaskType="Date"></asp:MaskedEditExtender>


            </td>
            <td class="style2">
                <asp:DropDownList ID="drpStatus" runat="server" CssClass="label" >
                    <asp:ListItem Text="New Admission" Value="NA"></asp:ListItem>
                    <asp:ListItem Text="Admission" Value="A"></asp:ListItem>
                    <asp:ListItem Text="Discharged" Value="D"></asp:ListItem>
                    <asp:ListItem Text="Approved" Value="AP"></asp:ListItem>
                     <asp:ListItem Text="All" Value=""  Selected="True"></asp:ListItem>
                 
                </asp:DropDownList>
            </td>
            <td >

                <asp:Button ID="btnRefresh" runat="server" CssClass="button orange small" Width="80px"
                    Text="Refresh" OnClick="btnRefresh_Click" />

            </td>
        </tr>
        <tr>
            <td class="lblCaption1">File Number
            </td>
            <td>
                <asp:TextBox ID="txtSrcFileNo" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px" MaxLength="10" CssClass="label"></asp:TextBox>
            </td>
            <td class="lblCaption1">Patient Name
            </td>
            <td>
                <asp:TextBox ID="txtSrcName" runat="server" BorderWidth="1px" BorderColor="#cccccc" Width="150px" CssClass="label"></asp:TextBox>
            </td>
              <td class="lblCaption1">Mobile Number
            </td>
            <td>
             <asp:textbox id="txtSrcMobile1" runat="server" BorderWidth="1px" BorderColor="#cccccc" width="150px" CssClass="label"></asp:textbox>
            </td>
        </tr>
        <tr>
            <td  class="lblCaption1">
                 Patient Type
            </td>
            <td>
                 <asp:DropDownList ID="drpPatientType" runat="server" CssClass="label" Width="150px" BorderWidth="1px" BorderColor="#CCCCCC"  >
                     <asp:ListItem Value="" Selected="true">--- All ---</asp:ListItem>
                    <asp:ListItem Value="CA">Cash</asp:ListItem>
                    <asp:ListItem Value="CR">Credit</asp:ListItem>
                 </asp:DropDownList>
            </td>
              <td  class="lblCaption1">
                  Company 
             </td>
             <td>
                  <asp:dropdownlist id="drpSrcCompany"  BorderWidth="1px" BorderColor="#cccccc" cssclass="label" runat="server" width="150px"    >
                   </asp:dropdownlist>

             </td>
        </tr>
    </table>
     <table width="100%">
        <tr>
            <td class="lblCaption1" >
                  <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                     <ContentTemplate>
                <asp:GridView ID="gvGridView" runat="server" AllowPaging="True" Width="100%" ShowFooter="true"
                    AllowSorting="True" AutoGenerateColumns="False" OnSorting="gvGridView_Sorting"  
                    EnableModelValidation="True" OnPageIndexChanging="gvGridView_PageIndexChanging" PageSize="200" GridLines="None" >
                    <HeaderStyle CssClass="GridHeader_Blue" />
                    <FooterStyle   CssClass="GridHeader_Blue"  />
                    <RowStyle CssClass="GridRow" />
                    <AlternatingRowStyle CssClass="GridAlterRow" />
                    <Columns>
                        <asp:TemplateField HeaderText="File No." SortExpression="IAS_PT_Id" FooterText="File No"  HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPTId" runat="server"  OnClick="Edit_Click"       >
                                      <asp:Label ID="lblEMR_ID" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("IAS_EMR_ID") %>'></asp:Label>
                                      <asp:Label ID="lblIPID" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("IAS_IP_ID") %>'></asp:Label>
                                    <asp:Label ID="lblDeptName" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("IAS_DEPARTMENT") %>'></asp:Label>
                                    <asp:Label ID="lblRefDrCode" CssClass="GridRow" runat="server" Visible="false" Text='<%# Bind("IAS_REFERRAL_PHYSICIAN") %>'></asp:Label>

                                     <asp:Label ID="lblCompID" CssClass="lblCaption1"  runat="server" Text='<%# Bind("IAS_INS_COMP_ID") %>'  Visible="false" ></asp:Label>
                                    <asp:Label ID="lblCompName" CssClass="lblCaption1"  runat="server" Text='<%# Bind("IAS_COMP_NAME") %>' Visible="false"></asp:Label>
                                     <asp:Label ID="lblVisitDate" CssClass="lblCaption1" runat="server" Text='<%# Bind("IAS_DateDesc") %>'   Visible="false"  ></asp:Label>
                                      <%--<asp:Label ID="lblVisitType" CssClass="lblCaption1"  runat="server" Text='<%# Bind("HPV_VISIT_TYPEDesc") %>'  Visible="false" ></asp:Label>--%>

                                    <asp:Label ID="lblPatientId" CssClass="lblCaption1"  runat="server" Text='<%# Bind("IAS_PT_ID") %>'  Visible="true" ></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>
                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Patient Name" SortExpression="IAS_PT_NAME" FooterText="Patient Name" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkPTName" CssClass="lblCaption1"  runat="server"  OnClick="Edit_Click"    >
                                    <asp:Label ID="lblPTName" CssClass="lblCaption1"  runat="server" Text='<%# Bind("IAS_PT_NAME") %>'></asp:Label>
                                </asp:LinkButton>
                            </ItemTemplate>

                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Sex"   FooterText="Sex"  SortExpression="IAS_SEX" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                
                                    <asp:Label ID="lblSex" CssClass="lblCaption1"  runat="server" Text='<%# Bind("IAS_SEX") %>'></asp:Label>
                               
                            </ItemTemplate>

                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Age"  FooterText="Age"  SortExpression="IAS_AGE" HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                               
                                    <asp:Label ID="lblAge" CssClass="lblCaption1" runat="server" Text='<%# Bind("IAS_AGE") %>'></asp:Label>
                               
                            </ItemTemplate>

                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Nationality" FooterText="Nationality" SortExpression="IAS_NATIONALITY"   HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                               
                                    <asp:Label ID="Label2" CssClass="lblCaption1" runat="server" Text='<%# Bind("IAS_NATIONALITY") %>'></asp:Label>
                                   
                               
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Room No" FooterText="Room No" SortExpression="IAS_ADMISSION_NO"   HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                               
                                    <asp:Label ID="lblRoomNo" CssClass="lblCaption1" runat="server" Text='<%# Bind("IAS_ROOM_NO") %>'></asp:Label>
                                   
                               
                            </ItemTemplate>

                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Admission ID" FooterText="Admission ID" SortExpression="IAS_ADMISSION_NO"   HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                               
                                    <asp:Label ID="lblAdmissionNo" CssClass="lblCaption1" runat="server" Text='<%# Bind("IAS_ADMISSION_NO") %>'></asp:Label>
                                   
                               
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Admission Date & Time" FooterText="Admission Date & Time" SortExpression="AdmissionDateDesc"   HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                               
                                    <asp:Label ID="lblAdmissionDate" CssClass="lblCaption1" runat="server" Text='<%# Bind("AdmissionDateDesc") %>'></asp:Label>
                                    <asp:Label ID="lblAdmissionTime" CssClass="lblCaption1" runat="server" Text='<%# Bind("AdmissionDateTimeDesc") %>'></asp:Label>
                               
                            </ItemTemplate>

                        </asp:TemplateField>

                         <asp:TemplateField HeaderText="Doctor" Visible="false" FooterText="Doctor" SortExpression="IAS_DR_NAME"   HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                               
                                   <asp:Label ID="lblDrID" CssClass="lblCaption1"  runat="server" Text='<%# Bind("IAS_DR_ID") %>'  Visible="false" ></asp:Label>
                                   <asp:Label ID="lblDrName" CssClass="lblCaption1"  runat="server" Text='<%# Bind("IAS_DR_NAME") %>' ></asp:Label>

                                   
                               
                            </ItemTemplate>

                        </asp:TemplateField>


                          <asp:TemplateField HeaderText="Status" FooterText="Status" SortExpression="IAS_STATUSDesc"   HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                               
                                    <asp:Label ID="lblIASStatus" CssClass="lblCaption1" runat="server" Text='<%# Bind("IAS_STATUSDesc") %>'></asp:Label>
                                   
                               
                            </ItemTemplate>

                        </asp:TemplateField>
                          
 
                         
                    </Columns>
                    
                  
                </asp:GridView>

                    </ContentTemplate>
        </asp:UpdatePanel>
           </td>
        </tr>

    </table>
     <table style="width:100%;">
        <tr>
          
            <td style="width:10%;">
                  
               <asp:Button ID="btnIPRegistration" runat="server" Text="IP Registration" Width="140px"  style="display:block;" CssClass="button orange small"  OnClientClick="return ShowIPRegistration();"  Visible="false"/>

          </td>
            <td style="width:10%;">
              <asp:Button ID="btnMasterAllo" runat="server" Text="Master Allocation" Width="140px" style="display:block;" CssClass="button orange small"  OnClientClick="return ShowMasterAllocation();"   Visible="false"/>

            </td>
              <td style="width:10%;">
             
                <asp:Button ID="btnPatientList" runat="server" Text="Patient List" Width="140px" style="display:block;" CssClass="button orange small"  OnClientClick="return ShowPatientList();" Visible="false" />

            </td>
            <td style="width:70%;"></td>
        </tr>
    </table>

</asp:Content>
