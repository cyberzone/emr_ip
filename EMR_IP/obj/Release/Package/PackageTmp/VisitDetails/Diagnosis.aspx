﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Diagnosis.aspx.cs" Inherits="EMR_IP.VisitDetails.Diagnosis1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
 <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

    <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.7.1.js" type="text/javascript"></script>
  <script src="../Scripts/jquery.caret.1.02.min.js" type="text/javascript"></script>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
<script type="text/javascript" >

    function __DoPostBack(Code, Desc) {
        alert(Code);
        alert(Desc);

    }

    function ShowAlertMessage() {

        alert('Principal diagnosis already added');
    }


    function SaveEMRS() {
        var EMRS = {
            BranchID: $("#BranchID").val(),
            PatientMasterID: $("#PatientMasterID").val(),
            ExaminationType: $("#ExaminationType").val(),
            PageFrom: 'PatientDashboard'
        };
        $.ajax({
            url: getBaseURL() + 'EMRS/SaveEMRS',
            data: JSON.stringify(EMRS),
            type: 'POST',
            success: function (result) {
                if (result == "success") {
                    console.log("EMRS saved successfully...");
                } else {
                    console.log("Problem in saving emrs, due to result = " + result);
                }
            }
        });
    }

    function refreshPanel(strValue) {
        // alert(strValue)
        __doPostBack('<%= updatePanel.UniqueID %>', "TextData");

        //  SetCaretAtEnd();
        // var elem = document.getElementById("<%=txtSearch.ClientID%>")

        // var elemLen = elem.value.length;
        // alert(elemLen);
        // setCursor(elem, elemLen, elemLen)

        // $('#<%=txtSearch.ClientID%>').setCursorToTextEnd();
    }

    function SetCaretAtEnd() {

        var elem = document.getElementById("<%=txtSearch.ClientID%>")

        var elemLen = elem.value.length;

        // For IE Only
        if (document.selection) {
            // Set focus
            elem.focus();
            // Use IE Ranges
            var oSel = document.selection.createRange();
            // Reset position to 0 & then set at end
            oSel.moveStart('character', -elemLen);
            oSel.moveStart('character', elemLen);
            oSel.moveEnd('character', 0);
            oSel.select();
        }
        else if (elem.selectionStart || elem.selectionStart == '0') {
            // Firefox/Chrome
            // alert(elem.selectionStart);

            elem.selectionStart = elemLen;
            elem.selectionEnd = elemLen;
            elem.focus();


            //alert(elem.selectionStart);
            // alert(elem.selectionEnd);
        } // if
    } // SetCaretAtEnd(


    (function ($) {
        $.fn.setCursorToTextEnd = function () {
            var $initialVal = this.val();
            this.val($initialVal);
        };
    })(jQuery);



</script>
<script type="text/javascript">
    window.onload = function () {
        //  setCursor(document.getElementById('input1'), 13, 13)
        //setCursor(document.getElementById("<%=txtSearch.ClientID%>"), 6, 6)

    }

    function setCursor(el, st, end) {
        if (el.setSelectionRange) {
            el.focus();
            el.setSelectionRange(st, end);
        } else {
            if (el.createTextRange) {
                range = el.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', st);
                range.select();
            }
        }
    }

    function ShowSearch() {
        //  SetCaretAtEnd();

        var elem = document.getElementById("<%=txtSearch.ClientID%>")

        var elemLen = elem.value.length;
        //alert(elemLen);
        setCursor(elem, elemLen, elemLen)
    }
</script>
 


 <style type="text/css">
     .WebContainer{
    width: 100%;
    height: auto;
}
 </style>
</head>
<body>
    <form id="form1" runat="server">
         <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>
    <div>
         <input type="hidden" id="hidPermission" runat="server" value="9" />
    <input type="hidden" id="hidSeleCode" runat="server" />
<input type="hidden" id="hidSeleDesc" runat="server" />


<table style="width: 50%">
    <tr>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel5" >
                      <ContentTemplate>
            <asp:Button ID="btnAddFav" runat="server" CssClass="orange" style="width:70px;border-radius:5px 5px 5px 5px"  Text="Add Fav."  OnClick="btnAddFav_Click"/>
             <asp:Button ID="btnDeleteFav" runat="server" CssClass="orange" style="width:70px;border-radius:5px 5px 5px 5px"   Text="Delete Fav." OnClick="btnDeleteFav_Click" Visible="false" />
                    </ContentTemplate>
            </asp:UpdatePanel>

                          
        </td>
        <td>
              <asp:UpdatePanel runat="server" ID="updatePanel4" >
                      <ContentTemplate>
            <asp:DropDownList ID="drpSrcType" runat="server" CssClass="lblCaption1" AutoPostBack="true" OnSelectedIndexChanged="drpSrcType_SelectedIndexChanged">
                <asp:ListItem Selected="True">Full List</asp:ListItem>
                <asp:ListItem>Favorite Only</asp:ListItem>
            </asp:DropDownList>
               </ContentTemplate>
              </asp:UpdatePanel>
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel">
                <ContentTemplate>
            <asp:TextBox ID="txtSearch" runat="server" CssClass="lblCaption1" OnKeyUp="refreshPanel(this.value);"  onfocus="ShowSearch();"  ></asp:TextBox>
                   
                    </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="txtSearch" />
                   
                </Triggers>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
<table style="width:100%">
    <tr>
        <td style="width: 50%; vertical-align: top;">

            <div >
                <table style="width:100%">

                    <tr>
                        <td>
                             <div style="padding-top: 0px; width:100%; height:600px;  overflow: scroll; border: thin; border-color: #f6f6f6; border-style: groove;">
                           <asp:UpdatePanel runat="server" ID="updatePanel1" >
                                                 <ContentTemplate>
                            <asp:GridView ID="gvServicess" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"   
                                EnableModelValidation="True"     OnPageIndexChanging="gvServicess_PageIndexChanging"  GridLines="None"    >
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />
                                 <FooterStyle CssClass="FooterStyle" />
                                <Columns>
                                   
                                    <asp:TemplateField HeaderText=""  HeaderStyle-Width="5%">
                                        <ItemTemplate>
                                           
                                                        <asp:ImageButton ID="lnkAdd" runat="server" ToolTip="Add" ImageUrl="~/Images/AddButton.jpg" Height="18" Width="18"
                                                                                                        OnClick="Add_Click" />&nbsp;&nbsp;
                                               
                                        </ItemTemplate>
                                           <HeaderStyle Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                             <asp:LinkButton ID="lnkCode" runat="server" OnClick="Edit_Click"  >
                                                    <asp:Label ID="lblPrice" CssClass="label" BorderStyle="None" Visible="false" runat="server" Font-Size="11px" Width="70px" Style="text-align: right; padding-right: 5px;" Text='<%# Bind("Price") %>'></asp:Label>

                                                <asp:Label ID="lblCode" CssClass="label" BorderStyle="None"  runat="server" Font-Size="11px" Text='<%# Bind("Code") %>' Width="100px"></asp:Label>
                                             </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" HeaderStyle-Width="85%" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDesc" runat="server" OnClick="Edit_Click"  >
                                                <asp:Label ID="lblDesc" CssClass="label" BorderStyle="None" runat="server" Font-Size="11px" Text='<%# Bind("Description") %>'  Width="95%"   ></asp:Label>
                                                
                                                
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                   
                                </Columns>
                                 <PagerStyle CssClass="PagerStyle"  /> 

                               
                                
                            </asp:GridView>
                                     </ContentTemplate>
                                            </asp:UpdatePanel>
                    
                                
                             </div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>

        

        <td style="width: 50%; vertical-align: top;">
            <div>
                <table style="width:100%">
                    <tr>
                        <td class="lblCaption1">
                              <asp:UpdatePanel runat="server" ID="updatePanel3" >
                                  <ContentTemplate>
                                     <asp:Label ID="lblMessage" runat="server" ForeColor="Red" CssClass="label" ></asp:Label>
                                  </ContentTemplate>
                              </asp:UpdatePanel>
                            Remarks<br />
                              <asp:UpdatePanel runat="server" ID="updatePanel6" >
                                  <ContentTemplate>
                                         <asp:TextBox ID="txtRemarks" runat="server" CssClass="lblCaption1" Width="65%"  ></asp:TextBox>
&nbsp;
                            <asp:dropdownlist id="drpDiagType" runat="server" cssclass="lblCaption1" width="30%" borderwidth="1px" bordercolor="#cccccc" >
                                            <asp:ListItem Value="Principal" selected="True">Principal</asp:ListItem>
                                            <asp:ListItem Value="Secondary">Secondary</asp:ListItem>
                                            <asp:ListItem Value="Admitting">Admitting</asp:ListItem>
                                           
                       </asp:dropdownlist>

                                 </ContentTemplate>
                             </asp:UpdatePanel>
                           
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <div style="padding-top: 0px; width: 100%;  height:400px;  overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                            <asp:UpdatePanel runat="server" ID="updatePanel2">
                <ContentTemplate>
                                  <asp:GridView ID="gvDiagnosis" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                EnableModelValidation="True"    OnRowDataBound="gvDiagnosis_RowDataBound"  GridLines="None"   >
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />

                                <Columns>
                                     <asp:TemplateField HeaderText=""  HeaderStyle-Width="50px" >
                                        <ItemTemplate>
                                            <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="18" Width="18"
                                                OnClick="DeleteeDiag_Click" />&nbsp;&nbsp;
                                                
                                        </ItemTemplate>
                                        
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="200px" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                              <asp:Label ID="lblDiagID" CssClass="label" Font-Size="11px"  runat="server" Text='<%# Bind("IPD_DIAG_ID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblDiagType" CssClass="label" Font-Size="11px"  Visible="false"  runat="server" Text='<%# Bind("IPD_TYPE") %>'></asp:Label>
                                            <asp:Button ID="btnColor" runat="server" Width="10px" Enabled="false" BorderStyle="None" />&nbsp
                                            <asp:Label ID="lblDiagCode" CssClass="label" Font-Size="11px"   Width="100px" runat="server" Text='<%# Bind("IPD_DIAG_CODE") %>'> </asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" HeaderStyle-Width="800px" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDiagDesc" CssClass="label" Font-Size="11px" Width="100%"  runat="server" Text='<%# Bind("IPD_DIAG_NAME") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                </Columns>
                            </asp:GridView>
                  </ContentTemplate>
                                </asp:UpdatePanel>
                    
                             </div>
                        </td>
                    </tr>
                </table>
            </div>

        </td>
    </tr>
</table>

    </div>
    </form>
</body>
</html>
