﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Laboratory.aspx.cs" Inherits="EMR_IP.VisitDetails.Laboratory1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<link href="../Styles/Maincontrols.css" type="text/css" rel="stylesheet" />
  <script src="../Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-1.4.1.js" type="text/javascript"></script>

    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

     <script language="javascript" type="text/javascript">


         function OnlyNumeric(evt) {
             var chCode = evt.keyCode ? evt.keyCode : evt.charCode ? evt.charCode : evt.which;
             if (chCode >= 48 && chCode <= 57 ||
                  chCode == 46) {
                 return true;
             }
             else

                 return false;
         }
    </script>

 <script language="javascript" type="text/javascript">
     function refreshPanelLab(strValue) {
         // alert(strValue)
         __doPostBack('<%= updatePanelLab.UniqueID %>', "Laboratory");

     }

     function setCursorLab(el, st, end) {
         if (el.setSelectionRange) {
             el.focus();
             el.setSelectionRange(st, end);
         } else {
             if (el.createTextRange) {
                 range = el.createTextRange();
                 range.collapse(true);
                 range.moveEnd('character', end);
                 range.moveStart('character', st);
                 range.select();
             }
         }
     }

     function ShowSearchLab() {
         //  SetCaretAtEnd();

         var elem = document.getElementById("<%=txtSearch.ClientID%>")

         var elemLen = elem.value.length;
         //alert(elemLen);
         setCursorLab(elem, elemLen, elemLen)
     }
    </script>



</head>
<body>
    <form id="form1" runat="server">
         <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"></asp:ToolkitScriptManager>


    <div>
         <input type="hidden" id="hidPermission" runat="server" value="9"   />

    <table style="width: 50%">
    <tr>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel4">
                <ContentTemplate>
            <asp:Button ID="btnAddFav" runat="server" CssClass="orange" style="width:70px;border-radius:5px 5px 5px 5px" Text="Add Fav."  OnClick="btnAddFav_Click"/>
             <asp:Button ID="btnDeleteFav" runat="server" CssClass="orange" style="width:70px;border-radius:5px 5px 5px 5px" Text="Delete Fav." OnClick="btnDeleteFav_Click" Visible="false" />
                    </ContentTemplate>
                </asp:UpdatePanel>
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanel5">
                <ContentTemplate>
            <asp:DropDownList ID="drpSrcType" runat="server" CssClass="lblCaption1" AutoPostBack="true" OnSelectedIndexChanged="drpSrcType_SelectedIndexChanged">
                <asp:ListItem Selected="True">Full List</asp:ListItem>
                <asp:ListItem>Favorite Only</asp:ListItem>
            </asp:DropDownList>
                    </ContentTemplate>
                </asp:UpdatePanel>
        </td>
        <td>
            <asp:UpdatePanel runat="server" ID="updatePanelLab">
                <ContentTemplate>
            <asp:TextBox ID="txtSearch" runat="server" CssClass="lblCaption1" OnKeyUp="refreshPanelLab(this.value);"  onfocus="ShowSearchLab();"  ></asp:TextBox>
                   
                    </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="txtSearch" />
                   
                </Triggers>
            </asp:UpdatePanel>
        </td>
    </tr>
   
</table>
<table>
    <tr>
        <td style="width: 50%; vertical-align: top;">

            <div style="float: left;">
                <table  style="width:100%">

                    <tr>
                        <td colspan="3">
                               <div style="padding-top: 0px; width: 100%;height:600px;   overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                     <asp:UpdatePanel runat="server" ID="updatePanel1">
                                    <ContentTemplate>
                            <asp:GridView ID="gvServicess" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                EnableModelValidation="True"   OnPageIndexChanging="gvServicess_PageIndexChanging"  GridLines="None"   >
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText=""  HeaderStyle-Width="5%">
                                        <ItemTemplate>
                                             <asp:ImageButton ID="lnkAdd" runat="server" ToolTip="Add" ImageUrl="~/Images/AddButton.jpg" Height="18" Width="18"
                                                OnClick="Add_Click" />&nbsp;&nbsp;
                                        </ItemTemplate>
                                           <HeaderStyle Width="50px" />
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="10%" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                             <asp:LinkButton ID="lnkCode" runat="server" OnClick="Edit_Click"  >
                                                <asp:Label ID="lblPrice" CssClass="label" BorderStyle="None" Visible="false" runat="server" Font-Size="11px" Width="70px" Style="text-align: right; padding-right: 5px;" Text='<%# Bind("Price") %>'></asp:Label>

                                                <asp:Label ID="lblCode" CssClass="label" BorderStyle="None"  runat="server" Font-Size="11px" Text='<%# Bind("Code") %>' Width="100px"></asp:Label>
                                             </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description" HeaderStyle-Width="85%" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkDesc" runat="server" OnClick="Edit_Click"  >
                                                <asp:Label ID="lblDesc" CssClass="label" BorderStyle="None" Width="95%" runat="server" Font-Size="11px" Text='<%# Bind("Description") %>'></asp:Label>
                                            </asp:LinkButton>
                                        </ItemTemplate>

                                    </asp:TemplateField>
                                     
                                </Columns>
                                 <PagerStyle CssClass="PagerStyle" /> 
                            </asp:GridView>
                                         </ContentTemplate>
                                 </asp:UpdatePanel>
                             </div>
                        </td>
                    </tr>
                </table>
            </div>
        </td>

        <td style="width: 10px;"></td>

        <td style="width: 50%; vertical-align: top;">
            <div>
                <table style="width:100%">
                    <tr>
                        <td class="lblCaption1" >
                             <asp:UpdatePanel runat="server" ID="updatePanel3" >
                                  <ContentTemplate>
                                     <asp:Label ID="lblMessage" runat="server" ForeColor="Red" CssClass="label" ></asp:Label>
                                  </ContentTemplate>
                              </asp:UpdatePanel>
                            Remarks:<br />
                            <input type="hidden" runat="server" id="hidProcCode" />
                        </td>
                        <td class="lblCaption1" >
                            Qty:
                        </td>
                    </tr>
                   
                    <tr>
                        <td>
                            <asp:TextBox ID="txtRemarks" runat="server" CssClass="lblCaption1" Width="98%"  ></asp:TextBox>

                        </td>
                        <td>
                            <asp:TextBox ID="txtQty" runat="server" CssClass="lblCaption1" Width="100px" Text="1" onkeypress="return OnlyNumeric(event);"></asp:TextBox>

                        </td>
                     
                    </tr>
                  
                    <tr>
                        <td colspan="2">
                             <div style="padding-top: 0px; width: 100%;  height:400px;  overflow: auto; border: thin; border-color: #f6f6f6; border-style: groove;">
                                    <asp:UpdatePanel runat="server" ID="updatePanel2">
                                    <ContentTemplate>
                           <asp:GridView ID="gvLaboratory" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
                                EnableModelValidation="True"    GridLines="None"   >
                                <HeaderStyle CssClass="GridHeader" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" />
                                <RowStyle CssClass="GridRow" />
                                <AlternatingRowStyle CssClass="GridAlterRow" />

                                <Columns>
                                     <asp:TemplateField HeaderText=""  HeaderStyle-Width="5%">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="DeleteeDiag" runat="server" ToolTip="Delete" ImageUrl="~/Images/icon_delete.jpg" Height="18" Width="18"
                                                OnClick="DeleteeDiag_Click" />&nbsp;&nbsp;
                                                
                                        </ItemTemplate>
                                         
                                        <ItemStyle HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Code"  HeaderStyle-Width="10%"  HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSOTransID" CssClass="label" Font-Size="11px"  runat="server" Text='<%# Bind("IPL_SO_TRANS_ID") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblLabID" CssClass="label" Font-Size="11px"  runat="server" Text='<%# Bind("IPL_LAB_ID") %>' Visible="false"></asp:Label>
                                                <asp:Label ID="lblDiagCode" CssClass="label" Font-Size="11px"  runat="server" Text='<%# Bind("IPL_LAB_CODE") %>'></asp:Label>
                                               
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description"   HeaderStyle-Width="75%"  HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            
                                                 <asp:Label ID="lblDiagDesc" CssClass="label" Font-Size="11px"  Width="100%" runat="server" Text='<%# Bind("IPL_LAB_NAME") %>'></asp:Label>
                                              
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                   
                                    <asp:TemplateField HeaderText="Qty" HeaderStyle-Width="10%"   HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            
                                                  <asp:Label ID="lblDiagQTY" CssClass="label" Font-Size="11px"  runat="server" Text='<%# Bind("IPL_QTY") %>'></asp:Label>
                                             
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                  

                                </Columns>
                            </asp:GridView>
                                  </ContentTemplate>
                                 </asp:UpdatePanel>
                            </div>
                        </td>
                        
                    </tr>
                </table>
            </div>

        </td>
    </tr>
</table>
    </div>
    </form>
</body>
</html>
