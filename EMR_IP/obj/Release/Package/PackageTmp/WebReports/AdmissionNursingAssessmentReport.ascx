﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AdmissionNursingAssessmentReport.ascx.cs" Inherits="EMR_IP.WebReports.AdmissionNursingAssessmentReport" %>

<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1">Admission Nursing Assessment</span>
<br />

<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Route of Admission
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtAdmissRoute" runat="server" CssClass="label" Width="200px" Style="resize: none;"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Ward No
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="drpWardNo" runat="server" CssClass="label" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="drpWardNo_SelectedIndexChanged"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Room No
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="drpRoomNo" runat="server" CssClass="label" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="drpRoomNo_SelectedIndexChanged"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Bed No
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="drpBed" runat="server" CssClass="label" Width="100px"></asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Arrival time to ward
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtArrivalDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Admission Mode
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="drpAdmissionMode" runat="server" CssClass="label" Width="100px">
              
            </asp:Label>


        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Info. obtained from
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle" colspan="3">
            <asp:Label ID="drpInfObtFrom" runat="server" CssClass="label" Width="100px">
             
            </asp:Label>


        </td>


    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Date of Admission
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle" colspan="7">

            <asp:Label ID="txtAdmiDate" runat="server" Width="70px" CssClass="label" MaxLength="10" onkeypress="return OnlyNumeric(event);"></asp:Label>


        </td>
    </tr>

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Diagnosis
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle" colspan="7">

            <asp:Label ID="txtDiag" runat="server" CssClass="label" TextMode="MultiLine" Width="100%" Height="50px" Style="resize: none;"></asp:Label>


        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Weight
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtWeight" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);"></asp:Label>


        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Height
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtHeight" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);"></asp:Label>


        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Patient Pain Score
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle" colspan="3">

            <asp:Label ID="txtPTPainScore" runat="server" CssClass="label" Width="100px" onkeypress="return OnlyNumeric(event);"></asp:Label>


        </td>

    </tr>
    <tr>

        <td class="lblCaption1 BoldStyle BorderStyle" colspan="8">

            <span class="lblCaption1">Orientation to the Unit</span>


            <asp:CheckBoxList ID="chkOrientationUnit" runat="server" CssClass="lblCaption1" Width="100%" RepeatDirection="Horizontal"></asp:CheckBoxList>

        </td>

    </tr>
    <tr>
        <td class="lblCaption1" colspan="3">Valuables brought to hospital by patient at the time of admission   
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle" colspan="7">

            <asp:RadioButtonList ID="radValuables" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="300px" Enabled="false">
                <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
                <asp:ListItem Text="No" Value="No"></asp:ListItem>
                <asp:ListItem Text="Kept with patient" Value="Kept with patient"></asp:ListItem>
            </asp:RadioButtonList>

        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Sent home with  
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:Label ID="txtPTSenthWith" runat="server" CssClass="label" Width="200px"></asp:Label>
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Given to PRO  by
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:Label ID="txtProGivenBy" runat="server" CssClass="label" Width="200px"></asp:Label>

        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">Living situation
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle" colspan="3">
            <asp:Label ID="drpLivingWith" runat="server" CssClass="lblCaption1" Width="200px">
             
            </asp:Label>

        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Profession
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="drpProfession" runat="server" CssClass="lblCaption1" Width="200px">
            
            </asp:Label>

        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">Living in UAE  
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:Label ID="textLivingYears" runat="server" CssClass="label" Width="50px" onkeypress="return OnlyNumeric(event);"></asp:Label>
            Yrs
                
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Married</td>
        <td class="lblCaption1 BoldStyle BorderStyle" colspan="3">

            <asp:RadioButtonList ID="radMarried" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Enabled="false">
                <asp:ListItem Text="Yes" Value="True"></asp:ListItem>
                <asp:ListItem Text="No" Value="False"></asp:ListItem>

            </asp:RadioButtonList>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Smoker
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">


            <input type="radio" id="radSmokeYes" name="radSmokeYes" runat="server" class="lblCaption1" value="True" disabled="disabled" /><label for="radSmokeYes">Yes</label>
            <input type="radio" id="radSmokeNo" name="radSmokeYes" runat="server" class="lblCaption1" value="False" disabled="disabled" /><label for="radSmokeNo">No</label>
            <asp:Label ID="CigsCount" runat="server" Width="50px" CssClass="label" onkeypress="return OnlyNumeric(event);"></asp:Label>
            Cigs/Day
                

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Quit Smoking
        </td>
        <td class="lblCaption1  BorderStyle" colspan="5">

            <asp:Label ID="txtSmokeQuitYears" runat="server" Width="30px" CssClass="label" onkeypress="return OnlyNumeric(event);"></asp:Label>
            Yrs
                                     <asp:Label ID="txtSmokeQuitMonth" runat="server" Width="30px" CssClass="label" onkeypress="return OnlyNumeric(event);"></asp:Label>
            Month
                

        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Alcohol
        </td>
        <td class="lblCaption1  BorderStyle">


            <input type="radio" id="radAlcoholYes" name="radAlcoholYes" runat="server" class="lblCaption1" value="True" /><label for="radAlcoholYes">Yes</label>
            <input type="radio" id="radAlcoholNo" name="radAlcoholYes" runat="server" class="lblCaption1" value="False" /><label for="radAlcoholNo">No</label>


        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Quit Drinking
        </td>
        <td class="lblCaption1  BorderStyle" colspan="5">

            <asp:Label ID="txtDrinkQuitYears" runat="server" Width="30px" CssClass="label" onkeypress="return OnlyNumeric(event);"></asp:Label>
            Yrs
                                      <asp:Label ID="txtDrinkQuitMonth" runat="server" Width="30px" CssClass="label" onkeypress="return OnlyNumeric(event);"></asp:Label>
            Month
               

        </td>
    </tr>
</table>



<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:CheckBoxList ID="chkPresPastHealthProb" runat="server" CssClass="lblCaption1" RepeatLayout="Flow" Width="900px" RepeatDirection="Horizontal"></asp:CheckBoxList>

        </td>
    </tr>
</table>
<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" style="width: 200px;">If any problem identified (Please specify)
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:Label ID="txtProbIdentified" runat="server"></asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Previous Hospitalizations / If any Surgeries write
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:Label ID="txtPrevHostSurgeries" runat="server"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" valign="top">Alergies
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle" valign="top">

            <input type="radio" id="radAlergiesYes" name="radAlergiesYes" runat="server" class="lblCaption1" value="True" disabled="disabled" /><label for="radAlergiesYes">Yes</label>
            <input type="radio" id="radAlergiesNo" name="radAlergiesYes" runat="server" class="lblCaption1" value="False" /><label for="radAlergiesNo">No</label>
            &nbsp; &nbsp;
            <asp:Label ID="txtAlergies" runat="server" CssClass="label"></asp:Label>


        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" valign="top">Type of Reaction
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle" valign="top">

            <asp:Label ID="txtReactionType" runat="server" CssClass="label"></asp:Label>


        </td>
    </tr>
</table>


<span class="lblCaption1">Medication History</span>
<asp:GridView ID="gvPharmacy" runat="server" AutoGenerateColumns="false" Width="100%"
    EnableModelValidation="True">

    <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
    <RowStyle CssClass="lblCaption1" HorizontalAlign="Center" />

    <Columns>

        <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="10%">
            <ItemTemplate>

                <asp:Label ID="lblPhyCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_PHY_CODE") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Description" HeaderStyle-Width="50%" HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>
                <asp:Label ID="lblPhyDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_PHY_NAME") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Dosage" HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>
                <asp:Label ID="lblPhyDosage" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_DOSAGE") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Route" HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>
                <asp:Label ID="lblPhyRoute" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_ROUTE") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Frequency" HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>

                <asp:Label ID="lblPhyFrequency" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_FREQUENCY") %>' Visible="false"></asp:Label>&nbsp;
                                                            <asp:Label ID="lblPhyFrequencyTypeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_FREQUENCYTYPEDesc") %>'></asp:Label>
                <asp:Label ID="lblPhyFrequencyType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_FREQUENCYTYPE") %>' Visible="false"></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Last dose" HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>
                <asp:Label ID="lblLastDoseDate" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_LAST_DOSAGE_DateDesc") %>'></asp:Label>&nbsp;
                <asp:Label ID="lblLastDoseTime" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IANM_LAST_DOSAGE_TimeDesc") %>'></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>


    </Columns>
</asp:GridView>

<span class="lblCaption1">Disposition of Medicine </span>
<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:CheckBox ID="chkMedicineBrought" runat="server" CssClass="label" Text="No medicine brought to the hospital" />
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:CheckBox ID="chkVarifiedPhysician" runat="server" CssClass="label" Text="To be varified by Physician for use in the nursing unit" />
        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Sent home with
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">
            <asp:Label ID="txtSentHomeWith" runat="server" Width="200px" CssClass="label"></asp:Label>
        </td>
    </tr>
</table>
<span class="lblCaption1">Pain Assessment</span>
<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Pain Score
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtPainScore" runat="server" Width="50px" CssClass="label" onkeypress="return OnlyNumeric(event);"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:RadioButtonList ID="PainScoreType" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Enabled="false">
                <asp:ListItem Text="Numbers" Value="Numbers"></asp:ListItem>
                <asp:ListItem Text="Faces" Value="Faces"></asp:ListItem>
                <asp:ListItem Text="Flacc" Value="Flacc"></asp:ListItem>
            </asp:RadioButtonList>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Location
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtPainLocation" runat="server" Width="200px" CssClass="label"></asp:Label>

        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">Onset
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtPainOnset" runat="server" Width="200px" CssClass="label"></asp:Label>

        </td>


    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Variations
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtPainVariations" runat="server" Width="200px" CssClass="label"></asp:Label>

        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Medications
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="txtMedications" runat="server" Width="200px" CssClass="label"></asp:Label>

        </td>


    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Aggravates
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="drpPainAggravates" runat="server" CssClass="label" Width="200px">
                                       
            </asp:Label>

        </td>

        <td class="lblCaption1 BoldStyle BorderStyle">Relieves
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">

            <asp:Label ID="drpPainRelieves" runat="server" CssClass="label" Width="200px">
                                        
            </asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Effects of pain
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle" colspan="3">

            <asp:Label ID="drpPainEffects" runat="server" CssClass="label" Width="200px">
                                       
            </asp:Label>

        </td>
    </tr>


</table>

<span class="lblCaption1">Physical Assessment</span>
<table style="width: 100%">
    <tr>
        <td style="width: 50%; vertical-align: top;">
            <asp:PlaceHolder ID="plhoPhyAssessmentLeft" runat="server"></asp:PlaceHolder>
        </td>
        <td style="width: 50%; vertical-align: top;">
            <asp:PlaceHolder ID="plhoPhyAssessmentRight" runat="server"></asp:PlaceHolder>
        </td>
    </tr>
</table>
<span class="lblCaption1">Other Assessment</span>
<table style="width: 100%">
    <tr>
        <td style="width: 50%; vertical-align: top;">
            
                <span class="lblCaption1">Risk/Safety Assessment</span>
                <asp:PlaceHolder ID="plhoRiskSafetyAssessmentLeft" runat="server"></asp:PlaceHolder>

             
                <span class="lblCaption1">Educational Assessment</span>
                <asp:PlaceHolder ID="plhoEducationalAssessmentLeft" runat="server"></asp:PlaceHolder>

            
                <span class="lblCaption1">Functional Assessment  </span>
                <asp:PlaceHolder ID="plhoFunctionalAssessmentLeft" runat="server"></asp:PlaceHolder>
 

        </td>
        <td style="width: 50%; vertical-align: top;">
            
                <span class="lblCaption1">Braden Skin Risk  Assessment Scale Circle one item from each category</span>
                <asp:PlaceHolder ID="plhoBraSkinRiskAssessmentRight" runat="server"></asp:PlaceHolder>
             
        </td>
    </tr>
</table>

<span class="lblCaption1">Patient Discharge</span>
<table style="width: 100%">
    <tr>
        <td style="width: 50%; vertical-align: top;">
            <asp:PlaceHolder ID="plhoPatientDischAssessmentLeft" runat="server"></asp:PlaceHolder>
            <asp:PlaceHolder ID="plhoPatientDischSummaryLeft" runat="server"></asp:PlaceHolder>
        </td>

        <td style="width: 50%; vertical-align: top;" class="lblCaption1">
            <asp:PlaceHolder ID="plhoPatientDischAssessmentRight" runat="server"></asp:PlaceHolder>

        </td>
    </tr>

</table>
