﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebReports/Report.Master" AutoEventWireup="true" CodeBehind="AdmissionRequest.aspx.cs" Inherits="EMR_IP.WebReports.AdmissionRequest" %>

<%@ Register Src="~/WebReports/AdmissionRequestReport.ascx" TagPrefix="UC1" TagName="AdmissionRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">
     <table style="width: 100%; text-align: center; vertical-align: top;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img style="padding: 1px; height: 70px; border: none;" src="images/Report_Logo.PNG" />
            </td>
        </tr>
    </table>


    <div style="padding-bottom: 2px; border-bottom: 4px solid #92c500; float: left; font-size: 1.2em; color: #2078c0;">
        <span style="font-family: Segoe UI, Arial, Helvetica, sans-serif;">Admission Request</span>
    </div>
    <br />
    <br />



    <table style="width: 100%; border: 1px solid #dcdcdc" class="gridspacy">
        <tr>
            <td class="lblCaption1 BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">Department:
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblDept" CssClass="lblCaption1" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">Date:
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblEmrDate" CssClass="lblCaption1" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">Admission# 
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblAdmissionNo" CssClass="lblCaption1" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">Admission Type
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblAdmissionType" CssClass="lblCaption1" runat="server"></asp:Label>
            </td>

        </tr>
    </table>

    <table width="100%; border: 1px solid #dcdcdc" class="gridspacy">
        <tr>
            <td class="lblCaption1 BoldStyle" colspan="4" style="border: 1px solid #dcdcdc; height: 25px;">Patient Full Name:<asp:Label ID="lblPTFullName" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Admission Mode:<asp:Label ID="lblAddmissionMode" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>&nbsp;  
                
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">File No:<asp:Label ID="lblFileNo" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Nationality:<asp:Label ID="lblNationality" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Age:<asp:Label ID="lblAge" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>&nbsp;  
                
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Phone No:<asp:Label ID="lblMobile" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Visit No:<asp:Label ID="lblEPMID" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Sex:<asp:Label ID="lblSex" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>&nbsp;  
                 
            </td>
        </tr>

        <tr>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Ins. Co.:<asp:Label ID="lblInsCo" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Policy Type:<asp:Label ID="lblPolicyType" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Policy No:<asp:Label ID="lblPolicyNo" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>&nbsp;  
                 
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Doctor Name:<asp:Label ID="lblDrName" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
                <asp:Label ID="lblDrCode" CssClass="lblCaption1 BoldStyle" runat="server" Visible="false"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;"></td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Visit Type:<asp:Label ID="lblVisitType" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>&nbsp;  
                 
            </td>
        </tr>
    </table>
    <br />


      <UC1:AdmissionRequest ID="ucAdmissionRequestReport" runat="server"  ></UC1:AdmissionRequest>
</asp:Content>
