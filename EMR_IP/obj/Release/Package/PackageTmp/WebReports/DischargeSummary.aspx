﻿<%@ Page Title="" Language="C#" MasterPageFile="~/IPPatientHeader.Master" AutoEventWireup="true" CodeBehind="DischargeSummary.aspx.cs" Inherits="EMR_IP.WebReports.DischargeSummary" %>


<%@ Register Src="~/WebReports/IPDiagnosisReport.ascx" TagPrefix="UC1" TagName="IPDiagnosis" %>
<%@ Register Src="~/WebReports/IPRadiologyReport.ascx" TagPrefix="UC1" TagName="IPRadiology" %>
<%@ Register Src="~/WebReports/IPLaboratoryReport.ascx" TagPrefix="UC1" TagName="IPLaboratory" %>
<%@ Register Src="~/WebReports/IPProcedureReport.ascx" TagPrefix="UC1" TagName="IPProcedure" %>



<%@ Register Src="~/WebReports/IPVitalSignReport.ascx" TagPrefix="UC1" TagName="IPVitalSign" %>
<%@ Register Src="~/WebReports/IPPrescriptionsReport.ascx" TagPrefix="UC1" TagName="IPPrescriptions" %>
<%@ Register Src="~/WebReports/IVFluidReport.ascx" TagPrefix="UC1" TagName="IVFluid" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="BoxContent" runat="server">

    <input type="hidden" id="hidEMRDeptID" runat="server" />
    <input type="hidden" id="hidEMRDeptName" runat="server" />

    <table style="width: 100%; text-align: center; vertical-align: top;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <img style="padding: 1px; height: 70px; border: none;" src="images/Report_Logo.PNG" />
            </td>
        </tr>
    </table>
    <div style="padding-bottom: 2px; border-bottom: 4px solid #92c500; float: left; font-size: 1.2em; color: #2078c0;">
        <span style="font-family: Segoe UI, Arial, Helvetica, sans-serif;">Discharge Statement</span>
    </div>
    <br />
    <br />
    <table style="width: 100%; border: 1px solid #dcdcdc" class="gridspacy">
        <tr>
            <td class="lblCaption1 BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">Department:
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblDept" CssClass="lblCaption1" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">Date:
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblEmrDate" CssClass="lblCaption1" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">Admission# 
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblAdmissionNo" CssClass="lblCaption1" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" style="border: 1px solid #dcdcdc; height: 25px;">Admission Type
            </td>
            <td style="border: 1px solid #dcdcdc; height: 25px;">
                <asp:Label ID="lblAdmissionType" CssClass="lblCaption1" runat="server"></asp:Label>
            </td>

        </tr>
    </table>

    <table width="100%; border: 1px solid #dcdcdc" class="gridspacy">
        <tr>
            <td class="lblCaption1 BoldStyle" colspan="4" style="border: 1px solid #dcdcdc; height: 25px;">Patient Full Name:<asp:Label ID="lblPTFullName" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Admission Mode:<asp:Label ID="lblAddmissionMode" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>&nbsp;  
                
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">File No:<asp:Label ID="lblFileNo" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Nationality:<asp:Label ID="lblNationality" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Age:<asp:Label ID="lblAge" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>&nbsp;  
                
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Phone No:<asp:Label ID="lblMobile" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Visit No:<asp:Label ID="lblEPMID" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Sex:<asp:Label ID="lblSex" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>&nbsp;  
                 
            </td>
        </tr>

        <tr>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Ins. Co.:<asp:Label ID="lblInsCo" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Policy Type:<asp:Label ID="lblPolicyType" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Policy No:<asp:Label ID="lblPolicyNo" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>&nbsp;  
                 
            </td>
        </tr>
        <tr>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Doctor Name:<asp:Label ID="lblDrName" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>
                <asp:Label ID="lblDrCode" CssClass="lblCaption1 BoldStyle" runat="server" Visible="false"></asp:Label>
            </td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;"></td>
            <td class="lblCaption1 BoldStyle" colspan="2" style="border: 1px solid #dcdcdc; height: 25px;">Visit Type:<asp:Label ID="lblVisitType" CssClass="lblCaption1 BoldStyle" runat="server"></asp:Label>&nbsp;  
                 
            </td>
        </tr>
    </table>
    <br />
    <UC1:IPVitalSign ID="IPVitalSignReport" runat="server"></UC1:IPVitalSign>
    <br />



    <UC1:IPLaboratory ID="IPLaboratoryReport" runat="server"></UC1:IPLaboratory>
    <UC1:IPRadiology ID="IPRadiologyReport" runat="server"></UC1:IPRadiology>
    <UC1:IPDiagnosis ID="IPDiagnosisReport" runat="server"></UC1:IPDiagnosis>
    <UC1:IPProcedure ID="IPProcedureReport" runat="server"></UC1:IPProcedure>


    <UC1:IPPrescriptions ID="IPPrescriptionsReport" runat="server"></UC1:IPPrescriptions>
    <br />
    <UC1:IVFluid ID="IVFluidReportReport" runat="server"></UC1:IVFluid>
    <br />
    <fieldset>
        <legend></legend>
        <table class="table spacy">
            <tr>
                <td style="width: 227px;">
                    <label for="OtherAdmissionSummary" class="lblCaption1">Others</label>
                </td>
                <td>
                    <textarea id="OtherAdmissionSummary" runat="server" name="OtherAdmissionSummary" rows="3" cols="112" class="lblCaption1" disabled="disabled"></textarea>
                </td>
            </tr>
            <tr>
                <td style="width: 227px">
                    <label for="ProcedurePlanned" class="lblCaption1">Procedure Planned</label></td>
                <td>
                    <textarea id="ProcedurePlanned" runat="server" name="ProcedurePlanned" rows="3" cols="112" class="lblCaption1" disabled="disabled"></textarea></td>
            </tr>
            <tr>
                <td style="width: 227px">
                    <label for="Anesthesia" class="lblCaption1">Anesthesia</label></td>
                <td>
                    <input type="text" id="Anesthesia" runat="server" name="Anesthesia" style="width: 683px;" class="lblCaption1" disabled="disabled" /></td>
            </tr>
            <tr>
                <td style="width: 227px">
                    <label for="Treatment" class="lblCaption1">Treatment</label></td>
                <td>
                    <textarea id="Treatment" runat="server" name="Treatment" rows="3" cols="112" class="lblCaption1" disabled="disabled"></textarea></td>
            </tr>

            <tr>
                <td style="width: 227px">
                    <label for="ReasonforAdmission" class="lblCaption1">Reason for Admission</label></td>
                <td>
                    <textarea id="ReasonforAdmission" runat="server" name="ReasonforAdmission" rows="3" cols="112" class="lblCaption1" disabled="disabled"></textarea>
                </td>
            </tr>
            <tr>
                <td style="width: 227px">
                    <label for="Remarks" class="lblCaption1">Remarks</label></td>
                <td>
                    <textarea id="Remarks" runat="server" name="Remarks" rows="3" cols="112" class="lblCaption1" disabled="disabled"></textarea>
                </td>
            </tr>

        </table>
    </fieldset>


</asp:Content>
