﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FluidBalancechartReport.ascx.cs" Inherits="EMR_IP.WebReports.FluidBalancechartReport" %>

<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1">Fluid Balancechart</span>




<asp:GridView ID="gvFluidBalance" runat="server" AutoGenerateColumns="False"
    EnableModelValidation="True" Width="100%">
    <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
    <RowStyle CssClass="lblCaption1" HorizontalAlign="Center" />
    <Columns>
        <asp:TemplateField HeaderText="Date & Time">
            <ItemTemplate>
             
                    <asp:Label ID="lblFliidBalID" CssClass="GridRow" runat="server" Text='<%# Bind("IFBC_FLUBAL_ID") %>' Visible="false"></asp:Label>
                    <asp:Label ID="lblgvAssDate" CssClass="GridRow" runat="server" Text='<%# Bind("IFBC_DATEDesc") %>'></asp:Label>
                    <asp:Label ID="lblgvAssTime" CssClass="GridRow" runat="server" Text='<%# Bind("IFBC_DATE_TIMEDesc") %>'></asp:Label>
                
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Nature of food" HeaderStyle-Width="200px">
            <ItemTemplate>
               
                    <asp:Label ID="lblNatureoffood" CssClass="GridRow" runat="server" Text='<%# Bind("IFBC_NATURE_OF_FOOD") %>'></asp:Label>

                
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Oral" HeaderStyle-Width="100px">
            <ItemTemplate>
                
                    <asp:Label ID="lblgvOral" CssClass="GridRow" runat="server" Text='<%# Bind("IFBC_ROUTE_ORAL") %>'></asp:Label>

                 
            </ItemTemplate>

        </asp:TemplateField>

        <asp:TemplateField HeaderText="I.V." HeaderStyle-Width="100px">
            <ItemTemplate>
               
                    <asp:Label ID="lblgvIV" CssClass="GridRow" runat="server" Text='<%# Bind("IFBC_ROUTE_IV") %>'></asp:Label>
               
            </ItemTemplate>

        </asp:TemplateField>
        <asp:TemplateField HeaderText="Other Method" HeaderStyle-Width="100px">
            <ItemTemplate>
               
                    <asp:Label ID="lblgvRouteOther" CssClass="GridRow" runat="server" Text='<%# Bind("IFBC_ROUTE_OTHER") %>'></asp:Label>
                 
            </ItemTemplate>

        </asp:TemplateField>

        <asp:TemplateField HeaderText="Urine" HeaderStyle-Width="100px">
            <ItemTemplate>
                
                    <asp:Label ID="lblgvUrine" CssClass="GridRow" runat="server" Text='<%# Bind("IFBC_URINE") %>'></asp:Label>

                 
            </ItemTemplate>



        </asp:TemplateField>
        <asp:TemplateField HeaderText="Vomit" HeaderStyle-Width="100px">
            <ItemTemplate>
               
                    <asp:Label ID="lblgvVomit" CssClass="GridRow" runat="server" Text='<%# Bind("IFBC_VOMIT") %>'></asp:Label>

                
            </ItemTemplate>

        </asp:TemplateField>

        <asp:TemplateField HeaderText="N.G Aspiration" HeaderStyle-Width="100px">
            <ItemTemplate>
              
                    <asp:Label ID="lblgvNGAspiration" CssClass="GridRow" runat="server" Text='<%# Bind("IFBC_NG_ASPIRATION") %>'></asp:Label>
               
            </ItemTemplate>


        </asp:TemplateField>
        <asp:TemplateField HeaderText="Other" HeaderStyle-Width="100px">
            <ItemTemplate>
                
                    <asp:Label ID="lblgvOutputOther" CssClass="GridRow" runat="server" Text='<%# Bind("IFBC_OTHER") %>'></asp:Label>
                 
            </ItemTemplate>

        </asp:TemplateField>
        <asp:TemplateField HeaderText="Total Input" HeaderStyle-Width="100px">
            <ItemTemplate>
               
                    <asp:Label ID="lblgvTotalInput" CssClass="GridRow" runat="server" Text='<%# Bind("IFBC_TOTAL_INPUT") %>'></asp:Label>
                
            </ItemTemplate>


        </asp:TemplateField>
        <asp:TemplateField HeaderText="Total Output" HeaderStyle-Width="100px">
            <ItemTemplate>
                
                    <asp:Label ID="lblgvTotalOutput" CssClass="GridRow" runat="server" Text='<%# Bind("IFBC_TOTAL_OUTPUT") %>'></asp:Label>
                
            </ItemTemplate>


        </asp:TemplateField>
        <asp:TemplateField HeaderText="Result" HeaderStyle-Width="100px">
            <ItemTemplate>
                 
                    <asp:Label ID="lblgvResult" CssClass="GridRow" runat="server" Text='<%# Bind("IFBC_RESULT") %>'></asp:Label>
               
            </ItemTemplate>


        </asp:TemplateField>
    </Columns>


</asp:GridView>
