﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IPDiagnosisReport.ascx.cs" Inherits="EMR_IP.WebReports.IPDiagnosisReport" %>

<table class="gridspacy" style="width: 100%;">
     <tr>
         <td class="lblCaption1"  >
              Diagnosis
          </td>
      </tr>
    <tr>

        <td class="lblCaption1  BoldStyle">
            <asp:GridView ID="gvDiagnosis" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                EnableModelValidation="True" Width="100%" ShowHeader="false">
                <HeaderStyle CssClass="GridRow" Font-Bold="true" />
                <RowStyle CssClass="lblCaption1" Height="25px" />

                <Columns>
                    <asp:TemplateField HeaderText="Code" HeaderStyle-Width="10%">
                        <ItemTemplate>

                            <asp:Label ID="Label16" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPD_TYPE") %>'></asp:Label>

                        </ItemTemplate>

                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Code" HeaderStyle-Width="10%">
                        <ItemTemplate>
                            ICD:  
                            <asp:Label ID="Label16" CssClass="lblCaption1" Font-Bold="true" runat="server" Text='<%# Bind("IPD_DIAG_CODE") %>'></asp:Label>

                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description" HeaderStyle-Width="80%">
                        <ItemTemplate>

                            <asp:Label ID="Label17" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPD_DIAG_NAME") %>'></asp:Label>

                        </ItemTemplate>

                    </asp:TemplateField>
                  
                </Columns>

            </asp:GridView>

        </td>
    </tr>
</table>
