﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IPRadiologyReport.ascx.cs" Inherits="EMR_IP.WebReports.IPRadiologyReport" %>


<table style="width:100%;">
                                                     <tr>
                                      <td class="lblCaption1"  >
                                      Radiology  
                                      </td>
                                  </tr>
                                   <tr>
                                       <td class="lblCaption1  BoldStyle">
                                  <asp:GridView ID="gvRadRequest" runat="server" AllowSorting="True" AutoGenerateColumns="False"
                                            EnableModelValidation="True" Width="100%">
                                            <HeaderStyle CssClass="lblCaption1" Font-Bold="true" />
                                            <RowStyle CssClass="lblCaption1" />
                            
                                            <Columns>
                                                <asp:TemplateField HeaderText="Code"  HeaderStyle-Width="10%">
                                                    <ItemTemplate>
                                                            <asp:Label ID="Label31" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPR_RAD_CODE") %>'  ></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" HeaderStyle-Width="90%">
                                                    <ItemTemplate>
                                                            <asp:Label ID="Label32" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPR_RAD_NAME") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField HeaderText="Price" HeaderStyle-Width="10%" Visible="false">
                                                    <ItemTemplate>
                                        
                                                            <asp:Label ID="Label33" CssClass="lblCaption1" runat="server" Text="0.00"  ></asp:Label>
                                        
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                 
                                           </Columns>
                                         </asp:GridView>
                                              </td>
                               </tr>
               </table>