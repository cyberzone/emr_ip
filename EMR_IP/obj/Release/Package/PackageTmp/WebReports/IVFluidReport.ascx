﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="IVFluidReport.ascx.cs" Inherits="EMR_IP.WebReports.IVFluidReport" %>


<span class="lblCaption1" > IV Fluid</span>
<table width="100%">

    <tr>
        <td class="lblCaption1  BoldStyle">
            <asp:GridView ID="gvIVFluid" runat="server"  AutoGenerateColumns="False"
                EnableModelValidation="True" Width="100%">
                <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
                <RowStyle CssClass="lblCaption1" HorizontalAlign="Center" />

                <Columns>
                    <asp:TemplateField HeaderText="Drug Code" HeaderStyle-Width="100px">
                        <ItemTemplate>

                            <%# Eval("IPI_IV_CODE")  %>
                        </ItemTemplate>

                    </asp:TemplateField>
                   
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>

                            <%# Convert.ToString(Eval("IPI_IV_NAME")).Length > 100 ? Convert.ToString(Eval("IPI_IV_NAME")).Substring(0,100) : Eval("IPI_IV_NAME") %>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Dosage/Time/Route" HeaderStyle-Width="100px">
                        <ItemTemplate>

                            <%# Eval("IPI_DOSAGE1") %> &nbsp;
                      <%# Eval("IPI_DOSAGE") %> &nbsp; 
                      <%# Eval("IPI_ROUTEDesc") %> &nbsp;

                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Duration" HeaderStyle-Width="100px">
                        <ItemTemplate>

                            <%# Eval("IPI_DURATION") %> &nbsp; <%# Eval("IPI_DURATION_TYPEDesc") %>
                        </ItemTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Qty" HeaderStyle-Width="50px">
                        <ItemTemplate>

                            <%# Eval("IPI_QTY") %>
                        </ItemTemplate>

                    </asp:TemplateField>
                  
                    <asp:TemplateField HeaderText="Remarks" HeaderStyle-Width="200px">
                        <ItemTemplate>

                            <%# Eval("IPI_REMARKS") %>
                        </ItemTemplate>

                    </asp:TemplateField>
 
                </Columns>
            </asp:GridView>

        </td>
    </tr>
</table>
