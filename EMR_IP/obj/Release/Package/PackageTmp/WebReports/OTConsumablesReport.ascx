﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OTConsumablesReport.ascx.cs" Inherits="EMR_IP.WebReports.OTConsumablesReport" %>

<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1">OT Consumables </span>
<br />

<asp:GridView ID="gvOTConsum" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
    EnableModelValidation="True" Width="100%">
    <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
    <RowStyle CssClass="lblCaption1" HorizontalAlign="Left" />


    <Columns>


        <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>


                <asp:Label ID="lblSOTransID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPOTC_SO_TRANS_ID") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblgvOTID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPOTC_OT_ID") %>' Visible="false"></asp:Label>

                <asp:Label ID="lblgvCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPOTC_OT_CODE") %>'></asp:Label>
                <br />
                <asp:Label ID="lblgvDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPOTC_OT_NAME") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>

                <asp:Label ID="lblgvConsumableType" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPOTC_OT_TYPE") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Qty" HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>

                <asp:Label ID="lblgvQTY" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPOTC_QTY") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Price" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
            <ItemTemplate>

                <asp:Label ID="lblgvPrice" CssClass="label" Font-Size="11px" Style="text-align: right; padding-right: 5px; width: 100px;" runat="server" Text='<%# Bind("IPOTC_PRICE") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Amount" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
            <ItemTemplate>

                <asp:Label ID="lblgvAmount" CssClass="label" Font-Size="11px" Style="text-align: right; padding-right: 5px; width: 100px;" runat="server" Text='<%# Bind("IPOTC_AMOUNT") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>


    </Columns>
</asp:GridView>

<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Total Amount:&nbsp;
                          <asp:Label ID="txtTotalAmount" runat="server" CssClass="label" Width="100px" Font-Bold="true"></asp:Label>

        </td>


    </tr>
</table>
