﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PainAssessmentReport.ascx.cs" Inherits="EMR_IP.WebReports.PainAssessmentReport" %>
<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1">Pain Assessment</span>
<br />
<span class="lblCaption1">Ongoing Pain Assessment </span>
<asp:GridView ID="gvAssessment" runat="server" AutoGenerateColumns="False"
    EnableModelValidation="True" Width="100%">
    <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
    <RowStyle CssClass="lblCaption1" HorizontalAlign="Center" />
    <Columns>
        <asp:TemplateField HeaderText="Date & Tome">
            <ItemTemplate>

                <asp:Label ID="lblAssessID" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_ASS_ID") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblgvAssDate" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_DATEDesc") %>'></asp:Label>
                <asp:Label ID="lblgvAssTime" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_DATE_TIMEDesc") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Location">
            <ItemTemplate>

                <asp:Label ID="lblgvLocation" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_LOCATION") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Pain intensity">
            <ItemTemplate>

                <asp:Label ID="lblgvPainIntensity" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_PAIN_INTENSITY") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Character code">
            <ItemTemplate>

                <asp:Label ID="lblgvCharacterCode" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_CHARACTER_CODE") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Frequency">
            <ItemTemplate>

                <asp:Label ID="lblgvFrequency" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_FREQUENCY") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Duration">
            <ItemTemplate>

                <asp:Label ID="lblgvDuration" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_DURATION") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Continue treat.">
            <ItemTemplate>

                <asp:Label ID="lblgvContinuetreat" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_CONT_CURR_MED") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>




        <asp:TemplateField HeaderText="Medication">
            <ItemTemplate>

                <asp:Label ID="lblgvMedication" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_MEDICATION") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Non Medication">
            <ItemTemplate>

                <asp:Label ID="lblgvNonMedication" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_NONMEDICATION") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Other Interventions">
            <ItemTemplate>

                <asp:Label ID="lblgvOtherInter" CssClass="GridRow" runat="server" Text='<%# Bind("IPAD_OTHER_INTERVENTIONS") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>
    </Columns>


</asp:GridView>

<br />
<span class="lblCaption1">OTHER QUESTIONS TO ASK   </span>
<table cellpadding="0" cellspacing="0" style="width: 100%">

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" style="width: 200px;">Pain Radiation: 
        </td>
        <td class="lblCaption1 BorderStyle" colspan="3">

            <input type="radio" id="radPainRadiationNo" name="radWeightLoss" runat="server" class="lblCaption1" value="No" disabled="disabled" />No 
                       <input type="radio" id="radPainRadiationYes" name="radWeightLoss" runat="server" class="lblCaption1" value="Yes" disabled="disabled" />Yes
                    , specify (where?)
                 <asp:Label ID="txtCurrentMedications" runat="server" CssClass="lblCaption1" Width="100%"></asp:Label>

        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Pain Pattern
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:RadioButtonList ID="radPainPattern" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="200px" Enabled="false">
                <asp:ListItem Text="Constant" Value="Constant"></asp:ListItem>
                <asp:ListItem Text="Intermittent" Value="Intermittent"></asp:ListItem>
            </asp:RadioButtonList>
        </td>
        <td class="lblCaption1 BoldStyle BorderStyle">Pain onset
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:RadioButtonList ID="radPainOnset" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="200px" Enabled="false">
                <asp:ListItem Text="Constant" Value="Constant"></asp:ListItem>
                <asp:ListItem Text="Intermittent" Value="Intermittent"></asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">What causes your pain?
        </td>
        <td colspan="3" class="lblCaption1 BorderStyle">
            <asp:Label ID="txtPainCauses" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
    </tr>

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" colspan="4">What relieves your pain? (Heat, cold, certain position, medication, etc…) 
        </td>
    </tr>

    <tr>
        <td colspan="4" class="lblCaption1 BorderStyle">
            <asp:Label ID="txtPainRelieves" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" colspan="4">What effects does pain have on Activities of Daily Living, relationships with others and emotions?
        </td>
    </tr>
    <tr>
        <td colspan="4" class="lblCaption1 BorderStyle">
            <asp:Label ID="txtEffectsOnActivities" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
    </tr>
</table>
<span class="lblCaption1">Interventions   </span>
<table cellpadding="0" cellspacing="0" style="width: 100%">

    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle" style="width: 200px;">Medication (Type, dose, frequency): 
        </td>
        <td colspan="2" class="lblCaption1 BorderStyle">
            <asp:Label ID="txtMedication" runat="server" CssClass="lblCaption1"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Non Medication 
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:CheckBoxList ID="chkNonMedication" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="800px" Enabled="false">
                <asp:ListItem Text="Heat packs" Value="Heat packs"></asp:ListItem>
                <asp:ListItem Text="Cold packs" Value="Cold packs"></asp:ListItem>
                <asp:ListItem Text="Repositioning / turning" Value="Repositioning / turning"></asp:ListItem>
                <asp:ListItem Text="Ambulation" Value="Ambulation"></asp:ListItem>
                <asp:ListItem Text="Relaxation exercises" Value="Relaxation exercises"></asp:ListItem>
                <asp:ListItem Text="Deep Breathing" Value="Deep Breathing"></asp:ListItem>
                <asp:ListItem Text="Rhythmic Breathing" Value="Rhythmic Breathing"></asp:ListItem>
            </asp:CheckBoxList>
        </td>

    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Other Interventions
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="txtOtherInterventions" runat="server" CssClass="lblCaption1" Width="100%" Height="30px" TextMode="MultiLine"></asp:Label>
        </td>
    </tr>
    <tr>
        <td class="lblCaption1 BoldStyle BorderStyle">Intervention Time
        </td>
        <td class="lblCaption1 BorderStyle">
            <asp:Label ID="txtInterventionDate" runat="server" CssClass="lblCaption1" Width="100%" Height="30px" TextMode="MultiLine"></asp:Label>

        </td>
    </tr>

</table>
<span class="lblCaption1">Please check appropriate scale used to assess pain intensity  </span>

<table cellpadding="0" cellspacing="0" style="width: 100%">
    <tr>

        <td class="lblCaption1 BorderStyle">
            <input type="checkbox" id="chkVerbal" name="chkScale" runat="server" disabled="disabled" class="lblCaption1" value="For verbal patients explain the Faces Pain Scale and ask thepatient to rate his/her pain" />For verbal patients explain the Faces Pain Scale and ask thepatient to rate his/her pain
        </td>
        <td colspan="2" class="lblCaption1 BorderStyle">
            <input type="checkbox" id="chkNonVerbal" name="chkScale" runat="server" disabled="disabled" class="lblCaption1" value="For non-verbal / pre-verbal rate the patients` pain using the Faces Pain Scale" />For non-verbal / pre-verbal rate the patients` pain using the Faces Pain Scale
        </td>
    </tr>
    <tr>
        <td></td>
        <td class="lblCaption1 BorderStyle">
            <input type="checkbox" id="chkNeonates" name="chkScale" runat="server" disabled="disabled" class="lblCaption1" value="For Neonates use NIPS pain scoring scale" />For Neonates use NIPS pain scoring scale
        </td>
    </tr>

</table>


<br />
<span class="lblCaption1">Pain ReAssessment </span>

<asp:GridView ID="gvReAssessment" runat="server" AutoGenerateColumns="False"
    EnableModelValidation="True" Width="100%">
    <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
    <RowStyle CssClass="lblCaption1" HorizontalAlign="Center" />
    <Columns>
        <asp:TemplateField HeaderText="Date & Tome">
            <ItemTemplate>

                <asp:Label ID="lblReAssessID" CssClass="GridRow" runat="server" Text='<%# Bind("IPNAD_ASS_ID") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblgvReAssDate" CssClass="GridRow" runat="server" Text='<%# Bind("IPNAD_DATEDesc") %>'></asp:Label>
                <asp:Label ID="lblgvReAssTime" CssClass="GridRow" runat="server" Text='<%# Bind("IPNAD_DATE_TIMEDesc") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Pain intensity">
            <ItemTemplate>

                <asp:Label ID="lblgvReAssPainIntensity" CssClass="GridRow" runat="server" Text='<%# Bind("IPNAD_PAIN_INTENSITY") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>

        <asp:TemplateField HeaderText="Changes in Pain">
            <ItemTemplate>

                <asp:Label ID="lblgvReAssCharacterCode" CssClass="GridRow" runat="server" Text='<%# Bind("IPNAD_PAIN_CHANGES") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Interventions">
            <ItemTemplate>

                <asp:Label ID="lblgvReAssInterventions" CssClass="GridRow" runat="server" Text='<%# Bind("IPNAD_INTERVENTIONS") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>



    </Columns>


</asp:GridView>
