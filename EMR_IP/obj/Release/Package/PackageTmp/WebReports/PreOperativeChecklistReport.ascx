﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PreOperativeChecklistReport.ascx.cs" Inherits="EMR_IP.WebReports.PreOperativeChecklistReport" %>
<link href="../Styles/Maincontrols.css" rel="stylesheet" type="text/css" />
<link href="../Styles/Datagrid.css" rel="stylesheet" type="text/css" />
<link href="../Styles/style.css" rel="Stylesheet" type="text/css" />

<style>
    .BoldStyle
    {
        font-weight: bold;
    }

    .BorderStyle
    {
        border: 1px solid #dcdcdc;
        height: 20px;
    }
</style>

<span class="lblCaption1">Pre Operative Checklist</span>

<br />
<span class="lblCaption1">Procedure</span>
<asp:GridView ID="gvProc" runat="server" AllowSorting="True" AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
    EnableModelValidation="True" Width="100%">
    <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
    <RowStyle CssClass="lblCaption1" HorizontalAlign="Center" />


    <Columns>


        <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
            <ItemTemplate>

                <asp:Label ID="lblgvProcID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_PRO_ID") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblPhyQTY" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_QTY") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblPhyCode" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_PRO_CODE") %>'></asp:Label>
                <br />
                <asp:Label ID="lblPhyDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_PRO_NAME") %>'></asp:Label>


            </ItemTemplate>
        </asp:TemplateField>


        <asp:TemplateField HeaderText="Date & Time" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="150px">
            <ItemTemplate>

                <asp:Label ID="lblgvDateDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_DATEDesc") %>'></asp:Label>&nbsp;
                                                                        <asp:Label ID="lblgvTimeDesc" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_DATETimeDesc") %>'></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderText="Surgeon" HeaderStyle-HorizontalAlign="Left" HeaderStyle-Width="250px">
            <ItemTemplate>

                <asp:Label ID="lblgvSurgName" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_SURG_NAME") %>'></asp:Label>
                <asp:Label ID="lblgvSurgID" CssClass="label" Font-Size="11px" runat="server" Text='<%# Bind("IPP_SURG_ID") %>' Visible="false"></asp:Label>

            </ItemTemplate>
        </asp:TemplateField>


    </Columns>
</asp:GridView>

<asp:GridView ID="gvPreOperative" runat="server" AllowSorting="True" AutoGenerateColumns="False"
    EnableModelValidation="True" Width="99%" OnRowDataBound="gvPreOperative_RowDataBound">
    <HeaderStyle CssClass="lblCaption1" Height="20px" Font-Bold="true" HorizontalAlign="Center" />
    <RowStyle CssClass="lblCaption1" HorizontalAlign="Center" />
    <Columns>
        <asp:TemplateField HeaderText="Description" HeaderStyle-Width="50%">
            <ItemTemplate>
                <asp:Label ID="lblLevelType" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPSM_LEVEL") %>' Visible="false"></asp:Label>
                <asp:Label ID="lblFieldID" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPSM_FIELD_ID") %>' Visible="false"></asp:Label>

                <asp:Label ID="Label1" CssClass="lblCaption1" runat="server" Text='<%# Bind("IPSM_FIELD_NAME") %>' Width="100%"></asp:Label>

            </ItemTemplate>

        </asp:TemplateField>
        <asp:TemplateField HeaderText="Ward Nurse" HeaderStyle-Width="25%">
            <ItemTemplate>
                <asp:RadioButtonList ID="radCheck" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="200px" Enabled="false">
                    <asp:ListItem Text="Yes" Value="Yes"> </asp:ListItem>
                    <asp:ListItem Text="No" Value="No"> </asp:ListItem>
                    <asp:ListItem Text="N/A" Value="N/A"> </asp:ListItem>
                </asp:RadioButtonList>
            </ItemTemplate>

        </asp:TemplateField>
        <asp:TemplateField HeaderText="Details" HeaderStyle-Width="25%">
            <ItemTemplate>
                <asp:Label ID="txtDtls" runat="server" CssClass="lblCaption1" ></asp:Label>

            </ItemTemplate>

        </asp:TemplateField>
        <asp:TemplateField HeaderText="Countercheck by OT Nurse" HeaderStyle-Width="25%">
            <ItemTemplate>
                <asp:RadioButtonList ID="radCheckOTNurse" runat="server" CssClass="lblCaption1" RepeatDirection="Horizontal" Width="200px" Enabled="false">
                    <asp:ListItem Text="Yes" Value="Yes"> </asp:ListItem>
                    <asp:ListItem Text="No" Value="No"> </asp:ListItem>

                </asp:RadioButtonList>
            </ItemTemplate>

        </asp:TemplateField>

    </Columns>

</asp:GridView>
